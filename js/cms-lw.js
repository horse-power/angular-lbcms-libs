(function() {
  angular.module('ui.landingWidgets', ['ngAnimate', 'ui.cms', 'cms.models']);

}).call(this);
;
(function() {
  angular.module('ui.landingWidgets').directive('lwCards', ["$injector", function($injector) {
    var $models, ALLOWED_FIELDS, CmsArticle, articlesCache, getL10nObject, getLink, loadingArticleIds;
    ALLOWED_FIELDS = ['title', 'subTitle', 'text', 'price', 'template'];
    $models = null;
    CmsArticle = null;
    articlesCache = {};
    loadingArticleIds = {};
    getL10nObject = function(value) {
      var cleaned, k;
      if (!value) {
        return null;
      }
      cleaned = {};
      for (k in value) {
        if (value[k]) {
          cleaned[k] = value[k];
        }
      }
      if (angular.isEmpty(cleaned)) {
        return null;
      }
      return cleaned;
    };
    getLink = function(link) {
      var article, filter, href, k, setArticle;
      if (!link || !link.type) {
        return null;
      }
      if (!link.type || !link.data) {
        return null;
      }
      if (link.type === 'href') {
        href = getL10nObject(link.data);
        if (!href) {
          return null;
        }
        for (k in href) {
          if (href[k].startsWith('/')) {
            href[k] = "" + location.origin + href[k];
          }
          if (!href[k].startsWith('https://') && !href[k].startsWith('http://')) {
            href[k] = "https://" + href[k];
          }
        }
        return {
          type: 'href',
          data: href
        };
      }
      link = {
        type: link.type,
        data: link.data
      };
      $models = $models || $injector.get('$models');
      if (!CmsArticle && $injector.has('CmsArticle')) {
        CmsArticle = $injector.get('CmsArticle');
      }
      article = articlesCache[link.data] || $models.CmsArticle.findItemByField('id', link.data);
      if (!article || (article.category && !article.$category)) {
        setArticle = function(article) {
          link.article = article;
        };
        if (!loadingArticleIds[link.data]) {
          loadingArticleIds[link.data] = [];
          filter = {
            fields: {
              id: true,
              title: true,
              category: true,
              $category: true,
              isPublished: true
            }
          };
          CmsArticle.findById({
            id: link.data,
            filter: filter,
            expand: {
              category: true
            }
          }, function(article) {
            var cb, i, len, ref;
            if (!article.isPublished) {
              return;
            }
            article.$category = article.$category || {
              id: '-'
            };
            articlesCache[article.id] = article;
            ref = loadingArticleIds[article.id];
            for (i = 0, len = ref.length; i < len; i++) {
              cb = ref[i];
              cb(article);
            }
            loadingArticleIds[article.id] = null;
          });
        }
        loadingArticleIds[link.data].push(setArticle);
        return link;
      } else if (!article) {
        return;
      }
      link.article = article;
      return link;
    };
    return {

      /**
      *   @ngdoc directive
      *   @name ui.landingWidgets.directive:lwCards
      *   @description
      *      директива, которая может генерировать карточки от {@link AdminApp.directive:aCardsEditor}
      *   @restrict E
      *   @param {object} ngModel (ссылка) описание карточек, в формате:
      *       <ul>
      *           <li><b>cards</b> - <span class='label type-hint type-hint-array inline-block'>Array&lt;object&gt;</span> - массив карточек. Массив карточек со свойствами:
      *               <ul>
      *                   <li>
      *                      <b>imgId</b> <i>(optional)</i> - <span class='label type-hint type-hint-number  inline-block'>number</span> - id картинки из массива files
      *                   </li>
      *                   <li>
      *                       <b>faIcon</b> <i>(optional)</i> - <span class='label type-hint type-hint-string  inline-block'>string</span> - класс FontAwesome иконки
      *                   </li>
      *                   <li>
      *                       <b>title</b> <i>(optional)</i> - <span class='label type-hint type-hint-object  inline-block'>L10nObject</span> - заголовок
      *                   </li>
      *                   <li>
      *                       <b>subTitle</b> <i>(optional)</i> - <span class='label type-hint type-hint-object  inline-block'>L10nObject</span> - подзаголовок
      *                   </li>
      *                   <li>
      *                       <b>price</b> <i>(optional)</i> - <span class='label type-hint type-hint-object  inline-block'>L10nObject</span> - текст цены
      *                   </li>
      *                   <li>
      *                       <b>link</b> <i>(optional)</i> - <span class='label type-hint type-hint-object  inline-block'>object</span> - объект-описания ссылки в формате: {type: '', data: ''}. Ключ type может быть в '', 'href', 'ui-sref:app.articles.details'. Ключ data может быть как id статьи, так и L10nObject объектом полных ссылок. Если это ссылка на статью, то также будет и свойство article, содержащее в себе усеченный объект статьи
      *                   </li>
      *                   <li>
      *                       <b>text</b> <i>(optional)</i> - <span class='label type-hint type-hint-object  inline-block'>L10nObject | string</span> - html-контент подписи
      *                   </li>
      *                   <li>
      *                       <b>template</b> <i>(optional)</i> - <span class='label type-hint type-hint-object  inline-block'>L10nObject | string</span> - html-контент карточки. Если он указан, то все свойства выше должны быть проигнорированы
      *                   </li>
      *               </ul>
      *           </li>
      *           <li><b>options</b> - <span class='label type-hint type-hint-object inline-block'>object</span> - дополнительные опции
      *           </li>
      *       </ul>
      *   @param {Object=} cmsModelItem (ссылка) объект в бд, в котором есть свойство 'files' (картинки ассоциированные с карточками, которые должны отображаться).
      *   @param {number=} [lg=3] (значение) количество колонок для размеров экрана lg (целое число)
      *   @param {number=} [md=3] (значение) количество колонок для размеров экрана md (целое число)
      *   @param {number=} [sm=2] (значение) количество колонок для размеров экрана sm (целое число)
      *   @param {number=} [xs=1] (значение) количество колонок для размеров экрана xs (целое число)
      *   @example
      *       <pre>
      *           $scope.cardsBlock = { cards: [
      *               {title: {en: "Hello!"}, link: {type: 'href', data: {en: "google.com"}}},
      *               {title: {en: "Hello 2 card"}, link: {type: 'href', data: {en: "yandex.ru"}}},
      *           ]}
      *       </pre>
      *       <pre>
      *           //-pug
      *           lw-cards(ng-model="cardsBlock")
      *               .bg-primary.text-center
      *                   b {{$item.title | l10n}}
      *                   div
      *                   a.btn.btn-default(ng-href="{{$item.link.data | l10n}}", target='_blank') Link
      *       </pre>
      *       <example module="DocExampleApp">
      *           <file name="script.js">
      *               mkDocCtrl('lwCardsCtrl', {
      *                   cardsBlock: {cards: [
      *                       {title: {en: "Hello!"}, link: {type: 'href', data: {en: "google.com"}}},
      *                       {title: {en: "Hello 2 card"}, link: {type: 'href', data: {en: "yandex.ru"}}},
      *                 ]}
      *               });
      *           </file>
      *           <file name="index.html">
      *               <div ng-controller="lwCardsCtrl">
      *                   <lw-cards ng-model="cardsBlock">
      *                       <div class='bg-primary text-center' style='padding: 10px;'>
      *                           <b>{{$item.title | l10n}}</b>
      *                           <div style='padding: 5px;'></div>
      *                           <a class='btn btn-default' ng-href="{{$item.link.data | l10n}}", target='_blank'>Link</a>
      *                       </div>
      *                   </lw-cards>
      *               </div>
      *           </file>
      *       </example>
       */
      restrict: 'E',
      transclude: true,
      scope: {
        ngModel: '=',
        cmsModelItem: '=?',
        ngDisabled: '=?'
      },
      controller: ["$scope", "$element", "$attrs", "H", function($scope, $element, $attrs, H) {
        $scope.$origScope = H.findOrigScope($scope, $attrs);
        return $scope.$watchGroup(['cmsModelItem.files', 'ngModel.cards'], function() {
          var card, field, i, imgFile, j, len, len1, processedCard, ref;
          $scope.cards = [];
          ref = ($scope.ngModel || {}).cards || [];
          for (i = 0, len = ref.length; i < len; i++) {
            card = ref[i];
            processedCard = {};
            imgFile = null;
            if (card.imgId) {
              imgFile = (($scope.cmsModelItem || {}).files || []).findByProperty('id', card.imgId);
            }
            if (imgFile) {
              processedCard.img = imgFile;
            }
            if (card.faIcon) {
              processedCard.faIcon = card.faIcon;
            }
            processedCard.link = getLink(card.link);
            for (j = 0, len1 = ALLOWED_FIELDS.length; j < len1; j++) {
              field = ALLOWED_FIELDS[j];
              processedCard[field] = getL10nObject(card[field]);
            }
            $scope.cards.push(processedCard);
          }
        });
      }],
      template: function($element, $attrs) {
        return "<uic-cols-grid class='row' items=\"cards\" lg=\"" + ($attrs.lg || '3') + "\" md=\"" + ($attrs.md || '3') + "\" sm=\"" + ($attrs.sm || '2') + "\" xs=\"" + ($attrs.xs || '1') + "\">\n    <div\n        class='lw-cards-card'\n        uic-transclude=''\n        uic-transclude-bind=\"{$item: $item, $cmsModelItem: $origScope.cmsModelItem, $origScope: $origScope.$origScope}\"\n    >\n    </div>\n</uic-cols-grid>";
      }
    };
  }]);

}).call(this);
;
(function() {
  angular.module('ui.landingWidgets').directive('lwCarousel', ["$cms", "$sce", "$templateRequest", "H", function($cms, $sce, $templateRequest, H) {
    var getImageUrl, getSliderType, parseThumbSize;
    getSliderType = function($attrs) {
      var sliderType;
      sliderType = $attrs.type;
      if (!['html', 'image+html'].includes(sliderType)) {
        sliderType = 'image+html';
      }
      if (!$attrs.cmsModelItem) {
        sliderType = 'html';
      }
      return sliderType;
    };
    parseThumbSize = function(size) {
      if (size.indexOf('x') === -1) {
        size = size + "x" + size;
      }
      size = size.split('x');
      return {
        width: parseInt(size[0]),
        height: parseInt(size[1])
      };
    };
    getImageUrl = function(l10nFile, thumbSize) {
      var i, knownThumbs, len, tSize, url;
      knownThumbs = H.l10nFile.getFileThumbs(l10nFile);
      if (knownThumbs.includes(thumbSize)) {
        url = H.l10nFile.getFileThumbUrl(l10nFile, thumbSize);
      } else {
        thumbSize = parseThumbSize(thumbSize);
        for (i = 0, len = knownThumbs.length; i < len; i++) {
          tSize = knownThumbs[i];
          tSize = parseThumbSize(tSize);
          if (tSize.width >= thumbSize.width - 10 && tSize.height >= thumbSize.height - 10) {
            url = H.l10nFile.getFileThumbUrl(l10nFile, tSize.width + "x" + tSize.height);
            break;
          }
        }
        if (!url) {
          url = H.l10nFile.getFileUrl(l10nFile);
        }
      }
      return url;
    };
    return {

      /**
      *   @ngdoc directive
      *   @name ui.landingWidgets.directive:lwCarousel
      *   @description
      *      обертка над страндартной каруселью в бутстрапе, позволяет определять слайды через описание в ng-model переменную, а
      *       не через верстку.
      *   @restrict E
      *   @param {object} ngModel (ссылка) описание карусели, в формате:
      *       <ul>
      *           <li><b>slides</b> - <span class='label type-hint type-hint-array inline-block'>Array&lt;object&gt;</span> - массив слайдов. Массив объектов со свойствами:
      *               <ul>
      *                   <li>
      *                      <b>imgId</b> <i>(optional)</i> - <span class='label type-hint type-hint-number  inline-block'>number</span> - id картинки слайда из массива files
      *                   </li>
      *                   <li>
      *                       <b>template</b> <i>(optional)</i> - <span class='label type-hint type-hint-object  inline-block'>L10nObject | string</span> - контент подписи слайдов (если указан тип слайдера как 'html', то этот шаблон сам станет слайдом)
      *                   </li>
      *                   <li>
      *                       <b>templateUrl</b> <i>(optional)</i> - <span class='label type-hint type-hint-string  inline-block'>string</span> - путь к шаблону. Это свойство НЕ используется в aCorouselEditor и предназначено в основном для генерации слайдера из кода.
      *                   </li>
      *               </ul>
      *           </li>
      *           <li><b>options</b> - <span class='label type-hint type-hint-object inline-block'>object</span> - дополнительные опции
      *               <ul>
      *                   <li>
      *                      <b>active</b> <i>(optional)</i> - <span class='label type-hint type-hint-number  inline-block'>number</span> - индекс активного слайда <i>(default: 0)</i>
      *                   </li>
      *                   <li>
      *                      <b>interval</b> <i>(optional)</i> - <span class='label type-hint type-hint-number  inline-block'>number</span> - интервал меж перелистыванием слайдов (в мсек.) <i>(default: 0)</i>
      *                   </li>
      *                   <li>
      *                      <b>noPause</b> <i>(optional)</i> - <span class='label type-hint type-hint-boolean  inline-block'>boolean</span> -  the interval pauses on mouseover. Setting this to truthy, disables this pause <i>(default: false)</i>
      *                   </li>
      *                   <li>
      *                      <b>noTransition</b> <i>(optional)</i> - <span class='label type-hint type-hint-boolean  inline-block'>boolean</span> -  whether to disable the transition animation between slides. Setting this to truthy, disables this transition <i>(default: false)</i>
      *                   </li>
      *                   <li>
      *                      <b>noWrap</b> <i>(optional)</i> - <span class='label type-hint type-hint-boolean  inline-block'>boolean</span> -  disables the looping of slides <i>(default: false)</i>
      *                   </li>
      *               </ul>
      *           </li>
      *       </ul>
      *   @param {string=} [type='image+html'] (значение) тип карусели в ['html', 'image+html']. Если указан тип 'image+html', но НЕ указан cmsModelItem, то тип будет принудительно понижен до 'html'
      *   @param {Object=} cmsModelItem (ссылка) объект в бд, в котором есть свойство 'files' (картинки ассоциированные с каруселью, которые должны отображаться).
      *   @param {string=} [imgSize='1600x700,800x600'] (значение) размеры картинок, которые должны отображаться в карусели. Размеры в px, перечисляются через запятую.
      *       Первый размер - для десктопов (lg, md), второй - для мобилок (sm, xs). Если указан только первый размер, то он будет использован для всех экранов. Подробно см модуль AdminApp и директиву в нем aCorouselEditor
      *   @example
      *       <pre>
      *           # coffee
      *           $scope.carouselData = {
      *               slides:[
      *                   {template: {en: '<h1>Hello slide 1!</h1>'}, imgId: 20},
      *                   {template: {en: '<h1>Hello slide 2!</h1>'}, imgId: 10}
      *               ]
      *           }
      *           # пусть, к примеру мы взяли галерею
      *           CmsGallery.findOne (item)->
      *               $scope.gallery = item
      *       </pre>
      *       <pre>
      *           //- pug
      *           lw-carousel(ng-model="carouselData", cms-model-item="gallery", type='image+html', img-size='1000x500,800x400')
      *       </pre>
      *       <example module="DocExampleApp">
      *           <file name="script.js">
      *               mkDocCtrl('lwCarouselCtrl', {
      *                   carouselData: {slides:[ {template: {en: '<h1>Hello slide 1!</h1>'}, imgId: 20}, {template: {en: '<h1>Hello slide 2!</h1>'}, imgId: 10}]},
      *                   gallery: {images: [{id: 20, name: 'slide1.jpg'}]}
      *               });
      *           </file>
      *           <file name="index.html">
      *               <div ng-controller="lwCarouselCtrl">
      *                   <lw-carousel ng-model="carouselData" cms-model-item="gallery" type='image+html' class='bg-primary'>
      *                   </lw-carousel>
      *               </div>
      *           </file>
      *       </example>
       */
      restrict: 'E',
      scope: {
        ngModel: '=',
        cmsModelItem: '=?',
        imgSize: '@?'
      },
      controller: ["$scope", "$attrs", function($scope, $attrs) {
        var imgSize, lgPadding, xsPadding;
        $scope.$cms = $cms;
        imgSize = ($attrs.imgSize || '1600x700,800x600').split(',');
        lgPadding = H.l10nFile.calcPadding(imgSize[0]);
        xsPadding = H.l10nFile.calcPadding(imgSize[1] || imgSize[0]);
        $scope.options = {
          type: getSliderType($attrs),
          paddings: {
            lg: lgPadding,
            md: lgPadding,
            sm: xsPadding,
            xs: xsPadding
          }
        };
        $scope.$watchGroup(['cmsModelItem.files', 'ngModel.slides'], function() {
          var i, imgFile, index, len, lgImgUrl, processedSlide, ref, slide, url, xsImgUrl;
          $scope.slides = [];
          ref = ($scope.ngModel || {}).slides || [];
          for (i = 0, len = ref.length; i < len; i++) {
            slide = ref[i];
            processedSlide = {
              img: {},
              template: {}
            };
            imgFile = null;
            if ($scope.options.type === 'image+html') {
              if (slide.imgId) {
                imgFile = (($scope.cmsModelItem || {}).files || []).findByProperty('id', slide.imgId);
              }
              if (imgFile) {
                lgImgUrl = getImageUrl(imgFile, imgSize[0]);
                xsImgUrl = getImageUrl(imgFile, imgSize[1] || imgSize[0]);
                processedSlide.img.lg = lgImgUrl;
                processedSlide.img.md = lgImgUrl;
                processedSlide.img.sm = xsImgUrl;
                processedSlide.img.xs = xsImgUrl;
                processedSlide.img.title = imgFile.title;
                if (angular.isEmpty(imgFile.title)) {
                  processedSlide.img.title[CMS_DEFAULT_LANG] = H.l10nFile.getOriginalName(imgFile);
                }
              } else {
                delete processedSlide.img;
              }
            }
            if (slide.template && angular.isString(slide.template)) {
              processedSlide.template[CMS_DEFAULT_LANG] = slide.template;
            } else if (slide.template) {
              processedSlide.template = slide.template;
            }
            $scope.slides.push(processedSlide);
            if (slide.templateUrl) {
              url = $sce.getTrustedResourceUrl(slide.templateUrl);
              index = $scope.slides.length - 1;
              $templateRequest(url).then((function(index) {
                (function(data) {
                  $scope.slides[index].template = {};
                  $scope.slides[index].template[CMS_DEFAULT_LANG] = data;
                });
              })(index));
            }
          }
        });
      }],
      template: ('/client/landing_widgets_app/_directives/lwCarousel/lwCarousel.html', '<div uib-carousel="" active="ngModel.options.active" interval="ngModel.options.interval || 0" no-pause="ngModel.options.noPause" no-transition="ngModel.options.noTransition" no-wrap="ngModel.options.noWrap"><div uib-slide="" ng-repeat="slide in slides   track by $index" index="$index"><div class="lw-carousel-slide-content" style="padding-bottom: {{options.paddings[$cms.screenSize]}}%; background-image: url( {{slide.img[$cms.screenSize]}} )"><div ng-class="{\'carousel-caption\': options.type == \'image+html\'}" uic-bind-html="slide.template | l10n" bind-item="cmsModelItem"></div></div></div></div>' + '')
    };
  }]);

}).call(this);
;
(function() {
  var hasProp = {}.hasOwnProperty;

  angular.module('ui.landingWidgets').directive('lwFilterClearView', ["$compile", "$cms", "H", "$lwFilterView", function($compile, $cms, H, $lwFilterView) {
    var ATTRS, deepClear, normalizeIdName;
    ATTRS = {
      'ng-disabled': 'ngDisabled',
      items: 'items',
      'search-term': 'searchTerm',
      'force-lang': 'forceLang'
    };
    normalizeIdName = function(name) {
      name += '';
      return name.split('.').join('_').split('-').join('_');
    };
    deepClear = function(obj) {
      var cleaned, k, v;
      if (!angular.isObject(obj)) {
        return obj;
      }
      cleaned = {};
      for (k in obj) {
        if (!hasProp.call(obj, k)) continue;
        v = obj[k];
        if (!(angular.isDefined(v) && v !== null && k[0] !== '$')) {
          continue;
        }
        if (angular.isObject(v) && !angular.isArray(v)) {
          v = deepClear(v);
          if (Object.keys(v).length === 0) {
            continue;
          }
        }
        cleaned[k] = v;
      }
      return cleaned;
    };
    return {

      /**
      *   @ngdoc directive
      *   @name ui.landingWidgets.directive:lwFilterClearView
      *   @description
      *
      *   @restrict E
      *   @example
      *       <pre>
      *           $scope.
      *       </pre>
      *       <pre>
      *           //- pug
      *       </pre>
      *       <example module="DocExampleApp">
      *           <file name="script.js">
      *               mkDocCtrl('lwFilterClearViewCtrl', {
      *
      *               });
      *           </file>
      *           <file name="index.html">
      *               <div ng-controller="lwFilterClearViewCtrl">
      *
      *               </div>
      *           </file>
      *       </example>
       */
      restrict: 'E',
      require: 'ngModel',
      scope: {
        items: '=',
        forceLang: '=?',
        searchTerm: '=?'
      },
      controller: ["$scope", "$element", "$attrs", function($scope, $element, $attrs) {
        var filterDefs, ngModelCtrl, suffix, template;
        $scope.$origScope = H.findOrigScope($scope, $attrs);
        ngModelCtrl = $element.controller('ngModel');
        filterDefs = $scope.$parent.$eval($attrs.filters) || $attrs.filters;
        if (typeof filterDefs === 'string') {
          suffix = filterDefs;
          filterDefs = angular.copy($lwFilterView.getFiltersConfig(filterDefs));
        } else {
          suffix = '-';
        }
        $scope.definitions = [];
        template = (filterDefs.filters || []).map(function(definition) {
          var attrs, conf;
          if (!definition || !definition.filter) {
            return '';
          }
          conf = $lwFilterView.getRenderConfig(definition.filter);
          if (!conf.directiveClearName) {
            return '';
          }
          definition.filterId = normalizeIdName(definition.filterId || definition.field || ("auto_" + $scope.filterParams.length));
          definition.filterParams || (definition.filterParams = {});
          $scope.definitions.push(definition);
          attrs = angular.copy(ATTRS);
          attrs['header-name'] = definition.headerName;
          attrs['definition'] = "definitions[" + ($scope.definitions.length - 1) + "]";
          attrs = H.convert.objToHtmlProps(attrs);
          return "<" + conf.directiveClearName + " ng-model=\"proxy." + definition.filterId + "\" " + attrs + " orig-scope=\"$origScope\">\n</" + conf.directiveClearName + ">";
        });
        $element.html(template.join(''));
        $compile($element.contents())($scope);
        ngModelCtrl.$render = function() {
          $scope.proxy = deepClear(ngModelCtrl.$modelValue || {}) || {};
        };
        return $scope.$watch('proxy', function(proxy, oldValue) {
          var cleaned;
          cleaned = deepClear(proxy);
          if (Object.keys(cleaned).length === 0) {
            $scope.proxy = {};
            ngModelCtrl.$setViewValue(null);
          } else {
            ngModelCtrl.$setViewValue(cleaned);
          }
        }, true);
      }],
      template: ""
    };
  }]);

}).call(this);
;
(function() {
  angular.module('ui.landingWidgets').directive('lwFilterEditView', ["$compile", "$cms", "H", "$lwFilterView", function($compile, $cms, H, $lwFilterView) {
    var ATTRS, normalizeIdName;
    ATTRS = {
      'ng-disabled': 'ngDisabled',
      items: 'items',
      'search-term': 'searchTerm',
      'force-lang': 'forceLang'
    };
    normalizeIdName = function(name) {
      name += '';
      return name.split('.').join('_').split('-').join('_');
    };
    return {

      /**
      *   @ngdoc directive
      *   @name ui.landingWidgets.directive:lwFilterEditView
      *   @description
      *
      *   @restrict E
      *   @param {Array} items (ссылка) массив элементов
      *   @example
      *       <pre>
      *           $scope.
      *       </pre>
      *       <pre>
      *           //- pug
      *       </pre>
      *       <example module="DocExampleApp">
      *           <file name="script.js">
      *               mkDocCtrl('lwFilterEditViewCtrl', {
      *
      *               });
      *           </file>
      *           <file name="index.html">
      *               <div ng-controller="lwFilterEditViewCtrl">
      *
      *               </div>
      *           </file>
      *       </example>
       */
      restrict: 'E',
      require: 'ngModel',
      scope: {
        items: '=',
        forceLang: '=?',
        searchTerm: '=?'
      },
      controller: ["$scope", "$element", "$attrs", "$cms", function($scope, $element, $attrs, $cms) {
        var filterDefs, filterItems, ngModelCtrl, onFilterChange, suffix, template;
        $scope.$origScope = H.findOrigScope($scope, $attrs);
        ngModelCtrl = $element.controller('ngModel');
        filterDefs = $scope.$parent.$eval($attrs.filters) || $attrs.filters;
        if (typeof filterDefs === 'string') {
          suffix = filterDefs;
          filterDefs = angular.copy($lwFilterView.getFiltersConfig(filterDefs));
        } else {
          suffix = '-';
        }
        onFilterChange = $scope.$parent.$eval(($attrs.onFilterChange || '').split('(')[0]);
        if (!angular.isFunction(onFilterChange)) {
          onFilterChange = angular.noop;
        }
        $scope.definitions = [];
        $scope.$cms = $cms;
        template = (filterDefs.filters || []).map(function(definition) {
          var attrs, conf;
          if (!definition || !definition.filter) {
            return '';
          }
          conf = $lwFilterView.getRenderConfig(definition.filter);
          if (!conf.directiveEditName) {
            return '';
          }
          definition.filterId = normalizeIdName(definition.filterId || definition.field || ("auto_" + $scope.filterParams.length));
          definition.filterParams || (definition.filterParams = {});
          if (definition.filterParams.comparator) {
            definition.doesFilterPass = function(params, filterValue) {
              return definition.filterParams.comparator(filterValue, params.value);
            };
          } else {
            definition.doesFilterPass = conf.doesFilterPass || function() {
              return true;
            };
          }
          if (!definition.valueGetter) {
            definition.valueGetter = function(params) {
              return angular.getValue(params.data, definition.field);
            };
          }
          $scope.definitions.push(definition);
          attrs = angular.copy(ATTRS);
          attrs['header-name'] = definition.headerName;
          attrs['definition'] = "definitions[" + ($scope.definitions.length - 1) + "]";
          attrs = H.convert.objToHtmlProps(attrs);
          return "<" + conf.directiveEditName + " ng-model=\"proxy." + definition.filterId + "\" " + attrs + " orig-scope=\"$origScope\">\n</" + conf.directiveEditName + ">";
        });
        $element.html(template.join(''));
        $compile($element.contents())($scope);
        filterItems = function() {
          var filteredItems, items, proxy;
          items = $scope.items || [];
          proxy = $scope.proxy || [];
          if (!Object.keys(proxy).length || !items.length) {
            onFilterChange(items, angular.copy(proxy));
            return;
          }
          filteredItems = items.filter(function(item) {
            var definition, i, len, params, ref;
            params = {
              data: item
            };
            ref = filterDefs.filters;
            for (i = 0, len = ref.length; i < len; i++) {
              definition = ref[i];
              if (!(angular.isDefined(proxy[definition.filterId]) && proxy[definition.filterId] !== void 0)) {
                continue;
              }
              params.value = definition.valueGetter(params);
              params.filterParams = definition.filterParams || {};
              if (!definition.doesFilterPass.call(definition, params, proxy[definition.filterId])) {
                return false;
              }
            }
            return true;
          });
          return onFilterChange(filteredItems, angular.copy(proxy));
        };
        ngModelCtrl.$render = function() {
          var k, ref, v;
          $scope.proxy = {};
          ref = ngModelCtrl.$modelValue || {};
          for (k in ref) {
            v = ref[k];
            if (angular.isDefined(v) && v !== void 0 && k[0] !== '$') {
              $scope.proxy[k] = v;
            }
          }
        };
        $scope.$watch('proxy', function(proxy, oldValue) {
          var cleaned, k, ref, v;
          cleaned = {};
          ref = proxy || {};
          for (k in ref) {
            v = ref[k];
            if (angular.isDefined(v) && v !== void 0 && k[0] !== '$') {
              cleaned[k] = v;
            }
          }
          if (angular.isEmpty(cleaned)) {
            $scope.proxy = {};
            ngModelCtrl.$setViewValue(null);
          } else {
            ngModelCtrl.$setViewValue(JSON.parse(JSON.stringify(cleaned)));
          }
          filterItems();
        }, true);
        $scope.$watch('items', filterItems, true);
        return $scope.$watch('$cms.settings.$preferredCurrency', function(preferredCurrency, oldValue) {
          if (preferredCurrency && preferredCurrency !== oldValue) {
            filterItems();
          }
        });
      }],
      template: ""
    };
  }]);

}).call(this);
;
(function() {
  angular.module('ui.landingWidgets').directive('lwGallery', function() {
    var DEFAULT_GRID_COLUMNS, DEFAULT_SLIDER_HEIGHT, getCmsModelItem, getConfiguration, getFile;
    DEFAULT_SLIDER_HEIGHT = 500;
    DEFAULT_GRID_COLUMNS = {
      lg: 3,
      md: 3,
      sm: 2,
      xs: 1
    };
    getCmsModelItem = function($scope) {
      if ($scope.item) {
        return $scope.item;
      }
      return $scope.cmsModelItem;
    };
    getConfiguration = function($scope) {
      if ($scope.ngModel && $scope.ngModel.data) {
        return $scope.ngModel.data;
      }
      return $scope.ngModel || {};
    };
    getFile = function(cmsModelItem, id) {
      var images, j, l10nFile, len1;
      images = [];
      if (cmsModelItem.hasOwnProperty('gallery')) {
        images = cmsModelItem.gallery;
      }
      if (cmsModelItem.hasOwnProperty('images')) {
        images = cmsModelItem.images;
      } else if (cmsModelItem.hasOwnProperty('files')) {
        images = cmsModelItem.files;
      }
      if (!id && images.length) {
        return images[0];
      }
      for (j = 0, len1 = images.length; j < len1; j++) {
        l10nFile = images[j];
        if (l10nFile === id) {
          return l10nFile;
        }
      }
      return void 0;
    };
    return {

      /**
      *   @ngdoc directive
      *   @name ui.landingWidgets.directive:lwGallery
      *   @description
      *
      *   @restrict EA
      *   @param {object} ngModel (ссылка) модель
      *   @param {boolean=} ngDisabled (ссылка) флаг доступности тега
      *   @example
      *       <pre>
      *           $scope.
      *       </pre>
      *       <pre>
      *           //- pug
      *       </pre>
      *       <pre>
      *       </pre>
       */
      restrict: 'EA',
      scope: {
        ngModel: '=',
        item: '=?',
        cmsModelItem: '=?',
        ngDisabled: '=?'
      },
      controller: ["$scope", "$element", "$attrs", "$timeout", "$cms", "H", "lwGalleryModal", function($scope, $element, $attrs, $timeout, $cms, H, lwGalleryModal) {
        var $thumbnails, $thumbwrapper, calculateThumbsWidth, prepareGallery, thumbSmartScroll, thumbsWidth;
        $scope.$cms = $cms;
        $thumbwrapper = null;
        $thumbnails = null;
        thumbsWidth = 0;
        calculateThumbsWidth = function() {
          var elWidth, thumbsNum, visible_width, width;
          width = 0;
          visible_width = 0;
          angular.forEach($thumbnails.find('img'), function(thumb) {
            width += thumb.clientWidth;
            width += 10;
            visible_width = thumb.clientWidth + 10;
          });
          thumbsNum = 0;
          elWidth = $element[0].clientWidth;
          if (elWidth) {
            thumbsNum = parseInt(elWidth / (70 + 10));
          }
          return {
            width: width,
            visible_width: visible_width * thumbsNum
          };
        };
        thumbSmartScroll = function(index) {
          if (!$thumbwrapper || !$thumbwrapper[0]) {
            return;
          }
          $timeout(function() {
            var i, item_scroll, len, s;
            len = $scope.gallery.images.length;
            item_scroll = parseInt(thumbsWidth / len, 10);
            i = index + 1;
            s = Math.ceil(len / i);
            $thumbwrapper[0].scrollLeft = 0;
            $thumbwrapper[0].scrollLeft = i * item_scroll - s * item_scroll;
          }, 100);
        };
        prepareGallery = function() {
          var base, cmsModelItem, config, i, id, image, j, k, l, l10nFile, len1, len2, len3, ref, ref1, ref2, screenSize, v;
          cmsModelItem = getCmsModelItem($scope);
          config = getConfiguration($scope);
          $scope.config = config;
          $scope.gallery = {
            mainImg: null,
            mainImgThumbSize: config.mainImgThumbSize || '440x250',
            previewThumbSize: config.previewThumbSize || '440x250',
            images: []
          };
          if ((config.filesId || {}).length) {
            ref = config.filesId;
            for (j = 0, len1 = ref.length; j < len1; j++) {
              id = ref[j];
              if (l10nFile = getFile(cmsModelItem, id)) {
                $scope.gallery.images.push(l10nFile);
              }
            }
          } else {
            if (config.previewType === 'slider' && config.useMainImg && cmsModelItem.mainImg) {
              $scope.gallery.images.push(cmsModelItem.mainImg);
            }
            ref1 = cmsModelItem.gallery || cmsModelItem.images || cmsModelItem.files || [];
            for (k = 0, len2 = ref1.length; k < len2; k++) {
              l10nFile = ref1[k];
              $scope.gallery.images.push(l10nFile);
            }
          }
          if (config.previewType === 'card') {
            $scope.gallery.mainImg = getFile(cmsModelItem, config.mainImgId) || cmsModelItem.mainImg;
          } else if (config.previewType === 'grid') {
            $scope.gallery.gridColums = config.previewGridColumns || {};
            for (screenSize in DEFAULT_GRID_COLUMNS) {
              v = DEFAULT_GRID_COLUMNS[screenSize];
              (base = $scope.gallery.gridColums)[screenSize] || (base[screenSize] = v);
            }
          } else if (config.previewType === 'slider') {
            $scope.gallery.carouselOptions = {
              active: 0,
              interval: config.previewSliderInterval || 0,
              slides: [],
              style: {}
            };
            ref2 = $scope.gallery.images;
            for (i = l = 0, len3 = ref2.length; l < len3; i = ++l) {
              image = ref2[i];
              $scope.gallery.images[i].copyrightHtml = H.l10nFile.getCopyrightHtml(image);
            }
            if (!$scope.gallery.images.length) {
              return;
            }
            $timeout(function() {
              var calculatedWidth, thumbnailsWidth;
              $thumbwrapper = angular.element($element[0].querySelectorAll('.lw-gallery-thumbnails-wrapper'));
              $thumbnails = angular.element($element[0].querySelectorAll('.lw-gallery-thumbnails'));
              calculatedWidth = calculateThumbsWidth();
              thumbsWidth = calculatedWidth.width;
              thumbnailsWidth = calculatedWidth.width + 1;
              $thumbnails.css({
                width: thumbnailsWidth + 'px'
              });
              return thumbSmartScroll($scope.gallery.carouselOptions.active);
            }, 500);
          }
        };
        $scope.openGallery = function(index) {
          var config;
          if ($scope.ngDisabled) {
            return;
          }
          config = getConfiguration($scope);
          lwGalleryModal.open({
            images: $scope.gallery.images,
            index: index || 0,
            thumbWidth: '70x70',
            thumbsNum: config.thumbsNum || 3
          });
        };
        $scope.changeImage = function(index) {
          if ($scope.ngDisabled) {
            return;
          }
          $scope.gallery.carouselOptions.active = index;
        };
        $scope.$watch('gallery.carouselOptions.active', function(active, oldValue) {
          var config;
          config = getConfiguration($scope);
          if (config.previewType === 'slider') {
            thumbSmartScroll(active);
          }
        });
        if ($attrs.hasOwnProperty('item')) {
          console.warn('lwGallery: "item" property is deprecated! Use "cmsModelItem" instead');
          $scope.$watchGroup(['item', 'ngModel'], function(item) {
            $scope.cmsModelItem = item;
            prepareGallery();
          });
          return;
        }
        return $scope.$watchGroup(['cmsModelItem', 'ngModel'], prepareGallery);
      }],
      template: ('/client/landing_widgets_app/_directives/lwGallery/lwGallery.html', '<div ng-switch="config.previewType"><div class="preview-card" ng-switch-when="card"><div class="thumbnail" ng-click="openGallery(0)"><uic-picture file="gallery.mainImg" size="{{gallery.mainImgThumbSize}}"></uic-picture><div class="fas fa-search-plus animated zoomIn"></div></div></div><div class="preview-grid" ng-switch-when="grid"><uic-cols-grid class="row" items="gallery.images" lg="{{gallery.gridColums.lg}}" md="{{gallery.gridColums.md}}" sm="{{gallery.gridColums.sm}}" xs="{{gallery.gridColums.xs}}"><div class="thumbnail" ng-click="$origScope.openGallery(index)"><uic-picture file="$item" size="{{$origScope.gallery.previewThumbSize}}"></uic-picture><div class="fas fa-search-plus animated zoomIn"></div></div></uic-cols-grid></div><div class="preview-slider" ng-switch-when="slider"><div uib-carousel="" active="gallery.carouselOptions.active" interval="gallery.carouselOptions.interval"><div uib-slide="" ng-repeat="image in gallery.images" index="$index"><div class="preview-slider-placeholder"><div class="img-bg" uic-parallax="image" ng-click="openGallery($index)"><picture-copyright class="img-bg-copyright" uic-bind-html="image.copyrightHtml"></picture-copyright></div></div></div></div><div class="lw-gallery-thumbnails-wrapper visible"><div class="lw-gallery-thumbnails slide-left"><div ng-repeat="image in gallery.images"><uic-picture class="lw-gallery-thumbnail" file="image" size="70x70" ng-class="{active: gallery.carouselOptions.active == $index}" ng-click="changeImage($index)"></uic-picture></div></div></div></div></div>' + '')
    };
  });

}).call(this);
;
(function() {
  angular.module('ui.landingWidgets').service('lwGalleryModal', ["$uibModal", "$timeout", "$q", "H", function($uibModal, $timeout, $q, H) {
    var KEY_KODES, modalController;
    KEY_KODES = {
      enter: 13,
      left: 37,
      right: 39
    };
    modalController = function($scope, $uibModalInstance, options) {
      var $thumbnails, $thumbwrapper, calculateThumbsWidth, k, loadImage, showImage, smartScroll, thumbsWidth;
      $scope.index = options.index || 0;
      $scope.thumbsNum = 3;
      $thumbwrapper = null;
      $thumbnails = null;
      thumbsWidth = 0;
      for (k in options) {
        if (options[k] !== void 0) {
          $scope[k] = options[k];
        }
      }
      if ($scope.thumbsNum > $scope.images.length) {
        $scope.thumbsNum = $scope.images.length;
      }
      loadImage = function(index) {
        var deferred, image;
        deferred = $q.defer();
        image = new Image;
        image.onload = function() {
          $scope.loading = false;
          if (typeof this.complete === false || this.naturalWidth === 0) {
            deferred.reject();
          }
          deferred.resolve(image);
        };
        image.onerror = function() {
          deferred.reject();
        };
        $scope.loading = true;
        image.src = $scope.images[index].url || H.l10nFile.getFileUrl($scope.images[index]);
        return deferred.promise;
      };
      showImage = function(index) {
        loadImage(index).then(function(result) {
          $scope.imgUrl = result.src;
          smartScroll(index);
        });
        $scope.description = $scope.images[index].description || {};
        $scope.copyrightHtml = H.l10nFile.getCopyrightHtml($scope.images[index]);
      };
      $scope.closeGallery = function() {
        return $uibModalInstance.dismiss('cancel');
      };
      $scope.onTableClick = function(event) {
        if (event && ['button', 'img', 'picture', 'span'].includes(event.target.nodeName.toLowerCase())) {
          return;
        }
        return $scope.closeGallery();
      };
      $scope.changeImage = function(i) {
        $scope.index = i;
        showImage(i);
      };
      $scope.nextImage = function() {
        $scope.index += 1;
        if ($scope.index === $scope.images.length) {
          $scope.index = 0;
        }
        showImage($scope.index);
      };
      $scope.prevImage = function() {
        $scope.index -= 1;
        if ($scope.index < 0) {
          $scope.index = $scope.images.length - 1;
        }
        showImage($scope.index);
      };
      calculateThumbsWidth = function() {
        var error, tw, visible_width, width;
        tw = $scope.thumbWidth;
        if (typeof tw === 'string') {
          try {
            tw = parseInt(tw);
          } catch (error1) {
            error = error1;
          }
        }
        width = 0;
        visible_width = 0;
        angular.forEach($thumbnails.find('img'), function(thumb) {
          width += thumb.clientWidth || tw;
          width += 10;
          visible_width = (thumb.clientWidth || tw) + 10;
        });
        return {
          width: width,
          visible_width: visible_width * $scope.thumbsNum
        };
      };
      smartScroll = function(index) {
        if (!$thumbwrapper) {
          return;
        }
        $timeout(function() {
          var i, item_scroll, len, s, width;
          len = $scope.images.length;
          width = thumbsWidth;
          item_scroll = parseInt(width / len, 10);
          i = index + 1;
          s = Math.ceil(len / i);
          $thumbwrapper[0].scrollLeft = 0;
          $thumbwrapper[0].scrollLeft = i * item_scroll - s * item_scroll;
        }, 100);
      };
      showImage($scope.index);
      return $uibModalInstance.rendered.then(function() {
        var calculatedWidth, lwGalleryContent, lwGalleryElement, thumbnailsWidth;
        $scope.modalReady = true;
        lwGalleryElement = document.getElementsByClassName('lw-gallery-modal')[0];
        lwGalleryContent = document.getElementsByClassName('lw-gallery-content')[0];
        $thumbwrapper = angular.element(lwGalleryElement.querySelectorAll('.lw-gallery-thumbnails-wrapper'));
        $thumbnails = angular.element(lwGalleryElement.querySelectorAll('.lw-gallery-thumbnails'));
        angular.element(lwGalleryElement).bind('keydown', function(event) {
          if (event.which === KEY_KODES.right || event.which === KEY_KODES.enter) {
            $scope.nextImage();
          } else if (event.which === KEY_KODES.left) {
            $scope.prevImage();
          }
          $scope.$apply();
        });
        angular.element(lwGalleryContent).bind('click', function(event) {
          if (event.target === this) {
            $scope.closeGallery();
          }
        });
        calculatedWidth = calculateThumbsWidth();
        thumbsWidth = calculatedWidth.width;
        thumbnailsWidth = calculatedWidth.width + 1;
        $thumbnails.css({
          width: thumbnailsWidth + 'px'
        });
        $thumbwrapper.css({
          width: calculatedWidth.visible_width + 'px'
        });
        return smartScroll($scope.index);
      });
    };
    this.open = function(options) {
      var modalData;
      options = options || {};
      modalData = {
        animation: true,
        template: ('/client/landing_widgets_app/_directives/lwGallery/modal.html', '<div class="lw-gallery-content" unselectable="on" ng-swipe-left="nextImage()" ng-swipe-right="prevImage()"><div class="uil-ring-css" ng-show="loading"><div></div></div><button class="btn-close" ng-click="closeGallery()" title="{{\'Close\' | translate}}"><span class="fas fa-times"></span></button><table ng-click="onTableClick($event)"><tbody><tr><td><button class="nav-left" ng-click="prevImage()" title="{{\'Next image\' | translate}}"><span class="fas fa-angle-left"></span></button></td><td><div uic-center-block=""><picture><img class="effect lw-gallery-current-image" ondragstart="return false;" draggable="false" oncontextmenu="return false" ng-click="nextImage()" ng-src="{{imgUrl}}" ng-show="!loading"/><picture-copyright uic-bind-html="copyrightHtml"></picture-copyright></picture></div></td><td><button class="nav-right" ng-click="nextImage()" title="{{\'Previous image\' | translate}}"><span class="fas fa-angle-right"></span></button></td></tr></tbody></table><span class="info-text" ng-show="(text=(description | l10n)).length">{{ index + 1 }}/{{ images.length }}<span>- {{ text }}</span></span><div class="lw-gallery-thumbnails-wrapper" ng-class="{\'visible\': modalReady}"><div class="lw-gallery-thumbnails slide-left"><div ng-repeat="image in images"><uic-picture class="lw-gallery-thumbnail" file="image" size="70x70" ng-class="{active: index == $index}" ng-click="changeImage($index)"></uic-picture></div></div></div></div>' + ''),
        windowClass: 'lw-gallery-modal',
        controller: ['$scope', '$uibModalInstance', 'options', modalController],
        resolve: {
          options: function() {
            return options;
          }
        }
      };
      if (options.template) {
        modalData.template = options.template;
        delete options.templates;
      }
      if (options.templateUrl) {
        modalData.templateUrl = options.templateUrl;
        delete modalData.template;
        delete options.templateUrl;
      }
      return $uibModal.open(modalData).result;
    };
    return this;
  }]);

}).call(this);
;
(function() {
  angular.module('ui.landingWidgets').directive('lwHeaderScrollDownClass', ["$window", "$timeout", function($window, $timeout) {
    return {

      /**
      *   @ngdoc directive
      *   @name ui.landingWidgets.directive:lwHeaderScrollDownClass
      *   @description
      *
      *   @restrict A
      *   @param {string} lwHeaderScrollDownClass (значение) название класса для анимаций
      *   @param {number=} [fromTop=10] (значение) отступ от верха окна, с какого момента добавить или убрать класс
      *   @example
      *       <pre>
      *           //- pug
      *           div(lw-viewport-enter-class="myAnimation")
      *       </pre>
       */
      restrict: 'A',
      link: function(scope, element, attr) {
        var added_class, fromTop;
        if (!attr.lwHeaderScrollDownClass) {
          return;
        }
        fromTop = parseInt(attr.fromTop);
        if (isNaN(fromTop)) {
          fromTop = 2;
        }
        if ($window.pageYOffset < fromTop) {
          element.removeClass(attr.lwHeaderScrollDownClass);
          added_class = false;
        } else {
          element.addClass(attr.lwHeaderScrollDownClass);
        }
        return angular.element($window).on('scroll', function() {
          if ($window.pageYOffset < fromTop) {
            if (added_class) {
              element.removeClass(attr.lwHeaderScrollDownClass);
              added_class = false;
            }
          } else if (!added_class) {
            element.addClass(attr.lwHeaderScrollDownClass);
            added_class = true;
          }
        });
      }
    };
  }]);

}).call(this);
;
(function() {
  angular.module('ui.landingWidgets').directive('lwLegalInformationLinks', function() {
    return {

      /**
      *   @ngdoc directive
      *   @name ui.landingWidgets.directive:lwLegalInformationLinks
      *   @description
      *       отображает плашку, в которой находятся все ссылки на статьи для категории с identifier=='legal-information', если они доступны. Если таких статей нет, то виджет невидим
      *   @restrict E
      *   @param {string=} [srefName='app.articles.details'] (значение) название ui-router пути для рендеринга url под ссылку на статью
      *   @example
      *       <example module="DocExampleApp">
      *           <file name="script.js">
      *               mkDocCtrl('lwLegalInformationLinksCtrl', {
      *
      *               });
      *           </file>
      *           <file name="index.html">
      *               <div ng-controller="lwLegalInformationLinksCtrl">
      *
      *               </div>
      *           </file>
      *       </example>
       */
      restrict: 'E',
      scope: {
        srefName: '@?'
      },
      controller: ["$scope", "$element", "CmsArticle", "$injector", function($scope, $element, CmsArticle, $injector) {
        var fields, where;
        $scope.linkTemplate = "<a ui-sref=\"" + ($scope.srefName || 'app.articles.details') + "({urlId: (item | toUrlId), categoryId: (item.$category | toUrlId) })\" rel='nofollow'>\n    {{item.title | l10n}}\n</a>";
        fields = {
          id: true,
          title: true,
          category: true,
          $category: true
        };
        where = {
          category__identifier: 'legal-information',
          isPublished: true
        };
        return CmsArticle.find({
          filter: {
            where: where,
            fields: fields
          },
          expand: {
            category: true
          }
        }, function(items) {
          if (!items || !items.length) {
            return;
          }
          $scope.items = items;
          $element.addClass('visible');
        });
      }],
      template: ('/client/landing_widgets_app/_directives/lwLegalInformationLinks/lwLegalInformationLinks.html', '<div class="container"><div class="text-center"><span ng-repeat="item in items"><span uic-bind-html="linkTemplate"></span><span class="link-separator" ng-show="!$last"></span></span></div></div>' + '')
    };
  });

}).call(this);
;
(function() {
  angular.module('ui.landingWidgets').directive('lwLegalInformationRequest', function() {
    return {

      /**
      *   @ngdoc directive
      *   @name ui.landingWidgets.directive:lwLegalInformationRequest
      *   @description
      *
      *   @restrict E
      *   @param {string=} [requestType='info'] (значение) тип запроса в ['info', 'anonimization', 'deletion']
      *   @param {boolean=} ngDisabled (ссылка) флаг доступности тега
      *   @example
      *       <example module="DocExampleApp">
      *           <file name="script.js">
      *               mkDocCtrl('lwLegalInformationRequestCtrl', {
      *
      *               });
      *           </file>
      *           <file name="index.html">
      *               <div ng-controller="lwLegalInformationRequestCtrl">
      *
      *               </div>
      *           </file>
      *       </example>
       */
      restrict: 'E',
      scope: {
        requestType: '@?'
      },
      controller: ["$scope", "$langPicker", "$q", "$injector", "$cms", function($scope, $langPicker, $q, $injector, $cms) {
        var CmsGdprRequest;
        CmsGdprRequest = $injector.get('CmsGdprRequest');
        $scope.$cms = $cms;
        $scope.options = {
          step: 'initial',
          verifyPhoneNumber: true
        };
        $scope.item = {
          emails: [''],
          phones: []
        };
        $scope.verificationCodes = {
          emails: {},
          phones: {}
        };
        $scope.gdprRequest = {};
        $scope._requestType = {
          info: 'i',
          anonimization: 'a',
          deletion: 'd'
        }[$scope.requestType] || 'i';
        $scope.addEmail = function() {
          return $scope.item.emails.push('');
        };
        $scope.addPhone = function() {
          return $scope.item.phones.push('');
        };
        $scope.removeEmail = function(index) {
          return $scope.item.emails.remove(index);
        };
        $scope.removePhone = function(index) {
          return $scope.item.phones.remove(index);
        };
        $scope.sendVerificationCodes = function() {
          $scope.options.step = 'loading';
          CmsGdprRequest.create({
            preferredLang: $langPicker.currentLang,
            type: $scope._requestType
          }, function(data) {
            $scope.gdprRequest = data;
            $scope.options.step = 'verifyCodes';
            $scope.item.emails = $scope.item.emails.removeDuplicates().filter(function(e) {
              return !!e;
            });
            $scope.item.phones = $scope.item.phones.removeDuplicates().filter(function(p) {
              return !!p;
            });
            $scope.item.emails.forEach(function(email) {
              return CmsGdprRequest.sendVerificationCode({
                id: $scope.gdprRequest.id
              }, {
                email: email
              });
            });
            return $scope.item.phones.forEach(function(phone) {
              return CmsGdprRequest.sendVerificationCode({
                id: $scope.gdprRequest.id
              }, {
                phone: phone
              });
            });
          });
        };
        $scope.verifyCodes = function() {
          var tasks;
          $scope.options.step = 'loading';
          tasks = [];
          $scope.item.emails.forEach(function(email) {
            if (!$scope.gdprRequest.emails.includes(email)) {
              return tasks.push(CmsGdprRequest.verifyCode({
                id: $scope.gdprRequest.id
              }, {
                email: email,
                code: $scope.verificationCodes.emails[email]
              }).$promise);
            }
          });
          $scope.item.phones.forEach(function(phone) {
            if (!$scope.gdprRequest.phones.includes(phone)) {
              return tasks.push(CmsGdprRequest.verifyCode({
                id: $scope.gdprRequest.id
              }, {
                phone: phone,
                code: $scope.verificationCodes.phones[phone]
              }).$promise);
            }
          });
          return $q.all(tasks)["finally"](function() {
            return CmsGdprRequest.findById({
              id: $scope.gdprRequest.id
            }, function(data) {
              $scope.gdprRequest = data;
              if ($scope.item.emails.length === data.emails.length && $scope.item.phones.length === data.phones.length) {
                return CmsGdprRequest.sendRequest({
                  id: $scope.gdprRequest.id
                }, function() {
                  return $scope.options.step = 'done';
                });
              } else {
                return $scope.options.step = 'verifyCodes';
              }
            });
          });
        };
        return $scope.$watch('$cms.settings.serverIntegrations', function(serverIntegrations) {
          serverIntegrations || (serverIntegrations = {});
          $scope.options.verifyPhoneNumber = !!serverIntegrations.useSmsNotifications;
        });
      }],
      template: ('/client/landing_widgets_app/_directives/lwLegalInformationRequest/lwLegalInformationRequest.html', '<h4 class="text-primary" ng-switch="_requestType"><span ng-switch-when="i" translate="">Information request</span><span ng-switch-when="a" translate="">Anonimization request</span><span ng-switch-when="d" translate="">Deletion request</span></h4><fieldset ng-disabled="ngDisabled"><form class="row" name="lInfoForm"><div class="col-md-50"><p translate="">In order for us to be sure that the mailboxes and phone numbers you have specified do belong to you, you should do the following steps:</p><ol><li translate="">Specify all mail boxes for which you would like to receive information and all phone numbers (in the international format)</li><li translate="">Click on the button "Send letters and sms with verification codes". For all the mailboxes and phone numbers you specified, you will receive verification codes, which you must enter on the same page without restarting it</li><li translate="">After that, click on the "Send Request" button</li><li translate="">We will review your application within 1-30 days from the date of request and send you letters with responses to all the mail boxes that you specified</li></ol></div><div class="col-md-50" ng-switch="options.step"><div class="with-preloader" ng-switch-when="loading"><div class="preloader" style="height: 5em;"></div></div><div ng-switch-when="initial"><div ng-switch="item.emails.length==1"><div class="form-group" ng-switch-when="true"><label translate="">Your email</label><input class="form-control" ng-model="item.emails[0]" type="email" placeholder="mymail@somemail.com" required="required"/></div><div ng-switch-when="false"><div class="form-group" ng-repeat="e in item.emails track by $index"><label><span translate="">Your email</span><span> №{{$index+1}} </span><a class="text-danger" ng-click="removeEmail($index)" ng-show="$index!=0"><span class="fas fa-times"></span><span translate="">remove</span></a></label><input class="form-control" ng-model="item.emails[$index]" type="email" placeholder="mymail@somemail.com" required="required"/></div></div></div><div class="form-group"><button class="btn btn-default btn-block btn-sm" ng-click="addEmail()"><span class="fas fa-plus"></span><span translate="">Add another email</span></button></div><div class="form-group" ng-repeat="p in item.phones track by $index" ng-hide="!options.verifyPhoneNumber"><label><span translate="">Your phone</span><span> №{{$index+1}} </span><a class="text-danger" ng-click="removePhone($index)"><span class="fas fa-times"></span><span translate="">remove</span></a></label><input class="form-control" ng-model="item.phones[$index]" placeholder="+00 0000 000" required="required"/></div><div class="form-group" ng-hide="!options.verifyPhoneNumber"><button class="btn btn-default btn-block btn-sm" ng-click="addPhone()"><span ng-switch="item.phones.length == 0"><span class="fas fa-plus"></span><span ng-switch-when="true" translate="">Add phone number</span><span ng-switch-when="false" translate="">Add another phone number</span></span></button></div><button class="btn btn-primary btn-block" ng-click="sendVerificationCodes()" ng-disabled="lInfoForm.$invalid"><div class="fas fa-paper-plane"></div><span translate="">Send letters and sms with verification codes</span></button></div><div ng-switch-when="verifyCodes"><div class="form-group" ng-repeat="e in item.emails"><div class="input-group"><div class="input-group-addon"><span class="text-success fas fa-check" ng-show="gdprRequest.emails.includes(e)"></span><span>{{e}}</span></div><input class="form-control" ng-model="verificationCodes.emails[e]" required="required" placeholder="{{\'Verification code\'}}" ng-disabled="gdprRequest.emails.includes(e)"/></div></div><div class="form-group" ng-repeat="p in item.phones" ng-hide="!options.verifyPhoneNumber"><div class="input-group"><div class="input-group-addon"><span class="text-success fas fa-check" ng-show="gdprRequest.phones.includes(p)"></span><span>{{p}}</span></div><input class="form-control" ng-model="verificationCodes.emails[e]" required="required" placeholder="{{\'Verification code\'}}" ng-disabled="gdprRequest.phones.includes(p)"/></div></div><button class="btn btn-primary btn-block" ng-click="verifyCodes()" ng-disabled="lInfoForm.$invalid"><div class="fas fa-check"></div><span translate="">Verify</span></button></div><div ng-switch-when="done"><br/><br/><h4><b translate="">Thanks for your request. We will answer you as soon as possible</b></h4><br/><br/></div></div></form></fieldset>' + '')
    };
  });

}).call(this);
;
(function() {
  angular.module('ui.landingWidgets').directive('lwMap', function() {
    return {

      /**
      *   @ngdoc directive
      *   @name ui.landingWidgets.directive:lwMap
      *   @description
      *
      *   @restrict E
      *   @param {object} ngModel (ссылка) модель
      *   @example
      *       <pre>
      *           $scope.
      *       </pre>
      *       <pre>
      *           //- pug
      *       </pre>
      *       <pre>
      *       </pre>
       */
      restrict: 'E',
      scope: {
        ngModel: '='
      },
      controller: ["$scope", "$element", "$timeout", "$window", "$cms", "$langPicker", "$worldMaps", function($scope, $element, $timeout, $window, $cms, $langPicker, $worldMaps) {
        var WorldMapClass, elementMap, elementMapStatic, gm, lastSize, updateMap;
        elementMap = $element[0].children[0];
        elementMapStatic = $element[0].children[1];
        WorldMapClass = $worldMaps.getFactory();
        if (!WorldMapClass) {
          $element.addClass('ng-hide');
          return;
        }
        gm = new WorldMapClass(elementMap, $scope.ngModel);
        lastSize = {};
        $scope.showJsMap = false;
        $scope.forceShowJsMap = false;
        $scope.staticMapUrl = '';
        updateMap = function() {
          var config, size;
          size = {
            width: elementMapStatic.clientWidth,
            height: elementMapStatic.clientHeight
          };
          lastSize = size;
          $scope.ngModel = $scope.ngModel || {};
          $scope.ngModel.config = $scope.ngModel.config || {};
          config = $scope.ngModel.config;
          gm.data = $scope.ngModel;
          $scope.showJsMap = !(config.showPlaceholder || (config.showPlaceholderClickableXs && $cms.screenSize === 'xs'));
          if ($scope.showJsMap || $scope.forceShowJsMap) {
            return gm.initMap(function() {
              gm.data = $scope.ngModel;
              gm.resetMarkers(false);
              gm.updateMarkers();
              return setTimeout(function() {
                return gm.fitBounds(true);
              }, 500);
            });
          } else {
            return $scope.staticMapUrl = gm.generateStaticImgUrl(size);
          }
        };
        $scope.getMarkerFlagChar = function(marker, index) {
          return gm.getMarkerFlagChar(marker.text, index + 1);
        };
        $scope.toggleForceShowJsMap = function() {
          var config;
          config = $scope.ngModel.config || {};
          if (!config.showPlaceholder) {
            $scope.forceShowJsMap = !$scope.forceShowJsMap;
            $timeout(updateMap, 100);
          }
        };
        angular.element($window).bind('resize', function() {
          if (lastSize.width !== elementMapStatic.clientWidth || lastSize.height !== elementMapStatic.clientHeight) {
            return updateMap();
          }
        });
        $scope.$watch('ngModel', updateMap, true);
        if (Object.keys($langPicker.languageList).length) {
          $scope.$langPicker = $langPicker;
          $scope.$watch('$langPicker.currentLang', function(currentLang, oldValue) {
            if (currentLang !== oldValue) {
              return updateMap();
            }
          });
        }
        $timeout(updateMap, 100);
      }],
      template: ('/client/landing_widgets_app/_directives/lwMap/lwMap.html', '<div class="gmap" ng-show="showJsMap || forceShowJsMap"></div><div class="clearfix gmap-static" ng-show="!showJsMap &amp;&amp; !forceShowJsMap" ng-click="toggleForceShowJsMap()"><img ng-src="{{staticMapUrl}}"/><div class="caption bottom" ng-if="!showJsMap &amp;&amp; !forceShowJsMap"><small ng-repeat="marker in (ngModel.data.markers || ngModel.markers)"><div class="marker" style="background-color: {{marker.color || \'green\'}};"><b>{{getMarkerFlagChar(marker, $index)}}</b></div>- {{(marker.text | l10n) || (\'Place\' | translate) + " " + ($index+1)}}</small></div></div>' + '')
    };
  });

}).call(this);
;
(function() {
  angular.module('ui.landingWidgets').directive('lwQuoteBubble', function() {
    return {

      /**
      *   @ngdoc directive
      *   @name ui.landingWidgets.directive:lwQuoteBubble
      *   @description
      *       директива для отображения цитат в виде баллонов с подписью и аватаром внизу.
      *   @restrict E
      *   @param {string=} imgUrl (значение) прямой url к картинке с аватаром
      *   @param {object=} imgL10nFile (ссылка) L10nImage картинка от lbcms (нужно указывать либо imgUrl либо imgL10nFile)
      *   @param {string} title (значение) заголовок подписи
      *   @param {string=} subTitle (значение) позаголовок подписи
      *   @param {string=} ngHref (значение) ссылка на страницу в интернете где находится эта цитата. Будет отображена только если передан в параметрах subTitle
      *   @example
      *       <pre>
      *           //- pug
      *           lw-quote-bubble(img-url='http://somesite.com/img.jpg',
      *                   title='Some title', sub-title='Some subtitle',
      *                   ng-href='http://site2.com/bla-bla')
      *               blockquote
      *                   | My quote. Some text
      *       </pre>
       */
      restrict: 'E',
      transclude: true,
      scope: {
        imgUrl: '@?',
        imgL10nFile: '=?',
        title: '@',
        subTitle: '@?',
        ngHref: '@?'
      },
      controller: ["$scope", "H", function($scope, H) {
        return window.onImageLoadError_lwQuoteBubble = window.onImageLoadError_lwQuoteBubble || function(image) {
          image.src = H.path.resolveFilePath("/client/brand/avatar.png");
        };
      }],
      template: ('/client/landing_widgets_app/_directives/lwQuoteBubble/lwQuoteBubble.html', '<div class="quote-bubble"><div ng-transclude="ng-transclude"></div></div><div class="user-info"><span ng-switch="!!imgUrl"><uic-picture class="avatar" ng-switch-when="false" file="imgL10nFile" size="70" default-url="/client/brand/avatar.png"></uic-picture><img class="avatar" ng-switch-when="true" ng-src="{{imgUrl}}" onerror="window.onImageLoadError_lwQuoteBubble(this)"/></span><div class="info"><h4>{{title}}</h4><p ng-show="subTitle &amp;&amp; !ngHref">{{subTitle}}</p><a ng-show="subTitle &amp;&amp; ngHref" href="{{ngHref}}" rel="nofollow">{{subTitle}}</a></div></div>' + '')
    };
  });

}).call(this);
;
(function() {
  angular.module('ui.landingWidgets').factory('LwVideoParserClass', function() {
    var Parser, SOURCES;
    SOURCES = {
      youtube: {
        is: function(url) {
          return url.indexOf('youtube.com/') > -1 || url.indexOf('youtu.be/') > -1;
        },
        getId: function(url) {
          var i, len, param, search;
          if (url.indexOf('/embed/') > -1 || url.indexOf('youtu.be/') > -1) {
            url = url.split('/');
            return url[url.length - 1].split('?')[0];
          }
          search = url.split('?')[1].replace('?', '').split('&');
          for (i = 0, len = search.length; i < len; i++) {
            param = search[i];
            param = param.split('=');
            if (param[0] === 'v') {
              return param[1];
            }
          }
        },
        getThumbnailUrl: function(id, size) {
          var SIZE;
          SIZE = {
            lg: 'maxresdefault.jpg',
            sm: 'sddefault.jpg',
            xs: 'hqdefault.jpg'
          };
          return "http://i1.ytimg.com/vi/" + id + "/" + SIZE[size];
        },
        getVideoEmbedUrl: function(id, autoplay) {
          if (autoplay) {
            autoplay = 1;
          } else {
            autoplay = 0;
          }
          return "https://www.youtube.com/embed/" + id + "?autoplay=" + autoplay;
        }
      }
    };
    return Parser = (function() {
      function Parser(url1) {
        this.url = url1;
        this.type = this.getVideoType();
        this.id = this.getVideoId();
      }

      Parser.prototype.getVideoId = function() {
        if (this.type) {
          return SOURCES[this.type].getId(this.url);
        }
      };

      Parser.prototype.getVideoType = function() {
        var helpers, sourceName;
        for (sourceName in SOURCES) {
          helpers = SOURCES[sourceName];
          if (helpers.is(this.url)) {
            return sourceName;
          }
        }
      };

      Parser.prototype.getVideoEmbedUrl = function(autoplay) {
        if (this.type) {
          return SOURCES[this.type].getVideoEmbedUrl(this.id, autoplay);
        }
      };

      Parser.prototype.getThumbnailUrl = function(size) {
        if (this.type) {
          return SOURCES[this.type].getThumbnailUrl(this.id, size);
        }
      };

      return Parser;

    })();
  }).directive('lwVideo', function() {
    return {

      /**
      *   @ngdoc directive
      *   @name ui.landingWidgets.directive:lwVideo
      *   @description
      *
      *   @restrict E
      *   @param {object} ngModel (ссылка) модель
      *   @example
      *       <pre>
      *           $scope.
      *       </pre>
      *       <pre>
      *           //- pug
      *       </pre>
       */
      restrict: 'E',
      scope: {
        ngModel: '='
      },
      controller: ["$scope", "$element", "LwVideoParserClass", "lwVideoModal", function($scope, $element, LwVideoParserClass, lwVideoModal) {
        var generateHtml;
        $scope.widgetType = 'lw-video';
        generateHtml = function(url) {
          var attrs, autoplay, p, placeholderSize;
          if (!url) {
            return;
          }
          p = new LwVideoParserClass(url || $scope.ngModel.data.video.url);
          attrs = "";
          autoplay = $scope.ngModel.data.video.showPlaceholder;
          if ($scope.ngModel.data.video.allowFullscreen) {
            attrs += " allowfullscreen";
          }
          $scope.html = "<iframe class=\"embed-responsive-item\" src=\"" + (p.getVideoEmbedUrl(autoplay)) + "\" " + attrs + ">\n</iframe>";
          if ($scope.ngModel.data.video.showPlaceholder) {
            $scope.showPlaceholder = true;
            placeholderSize = $scope.ngModel.data.video.placeholderSize || 'lg';
            $scope.html = "<div class=\"placeholder\" ng-if=\"showPlaceholder\">\n    <img src=\"" + (p.getThumbnailUrl(placeholderSize)) + "\" class=\"w-100\"/>\n    <div class=\"load-video\"  ng-click=\"togglePlaceholder()\">\n        <div uic-center-block=''>\n            <h4 translate>Play</h4>\n            <div class=\"fa fa-youtube-play fa-2x\"></div>\n        </div>\n    </div>\n</div>\n<div ng-if=\"!showPlaceholder\">\n    " + $scope.html + "\n<div>";
          }
        };
        $scope.togglePlaceholder = function() {
          var attrs, p;
          if ($scope.ngModel.data.video.showInModal && $scope.showPlaceholder) {
            p = new LwVideoParserClass($scope.ngModel.data.video.url);
            attrs = '';
            if ($scope.ngModel.data.video.allowFullscreen) {
              attrs += " allowfullscreen";
            }
            lwVideoModal.open($scope.ngModel, {
              html: "<iframe class=\"embed-responsive-item\" src=\"" + (p.getVideoEmbedUrl(true)) + "\" " + attrs + ">\n</iframe>"
            });
            return;
          }
          return $scope.showPlaceholder = !$scope.showPlaceholder;
        };
        $scope.$watch('ngModel.data.video', function(video) {
          return generateHtml(video.url);
        }, true);
        if ($scope.ngModel.data) {
          return generateHtml($scope.ngModel.data.video.url);
        }
      }],
      template: ('/client/landing_widgets_app/_directives/lwVideo/lwVideo.html', '<div class="caption {{widget.cache.classes.caption}}" ng-if="ngModel.data.caption.position==\'left\' || ngModel.data.caption.position==\'top\'" style="{{widget.cache.styles.caption}}"><span uic-bind-html="ngModel.data.caption.text | l10n"></span></div><div class="{{widget.cache.classes.main}}" style="{{widget.cache.styles.main}}"><div class="embed-responsive embed-responsive-16by9" uic-bind-html="html" ng-if="ngModel.data.video.url"></div><div class="nocontent" ng-if="!ngModel.data.video.url"><div><div class="fab fa-youtube fa-2x"></div></div><span translate="">No video available</span></div></div><div class="caption {{widget.cache.classes.caption}}" ng-if="ngModel.data.caption.position==\'bottom\' || ngModel.data.caption.position==\'right\'" style="{{widget.cache.styles.caption}}"><span uic-bind-html="ngModel.data.caption.text | l10n"></span></div>' + '')
    };
  });

}).call(this);
;
(function() {
  angular.module('ui.landingWidgets').service('lwVideoModal', ["$uibModal", "LwVideoParserClass", function($uibModal, LwVideoParserClass) {
    var modalController;
    modalController = function($scope, $uibModalInstance, ngModel, options) {
      $scope.ngModel = angular.copy(ngModel);
      $scope.options = options;
      $scope.cancel = function() {
        return $uibModalInstance.dismiss('cancel');
      };
      return $scope.ok = function() {
        return $uibModalInstance.close();
      };
    };
    this.open = function(ngModel, options) {
      var attrs, p, result;
      options || (options = {});
      if (!options.html) {
        p = new LwVideoParserClass(ngModel.data.video.url);
        attrs = '';
        if (ngModel.data.video.allowFullscreen) {
          attrs += " allowfullscreen";
        }
        options.html = "<iframe class=\"embed-responsive-item\" src=\"" + (p.getVideoEmbedUrl(true)) + "\" " + attrs + ">\n</iframe>";
      }
      result = $uibModal.open({
        animation: true,
        windowClass: 'lw-video-modal',
        template: ('/client/landing_widgets_app/_directives/lwVideo/modal/modal.html', '<div class="modal-header"><button class="close" ng-click="cancel()"><span>&times;</span></button><h4 class="modal-title" ng-show="ngModel.data.caption.text | l10n">{{ngModel.data.caption.text | l10n}}</h4></div><div class="modal-body"><div class="embed-responsive embed-responsive-16by9" uic-bind-html="options.html"></div></div><div class="modal-footer"><button class="btn btn-default" ng-click="cancel()" translate="">Close</button></div>' + ''),
        controller: ['$scope', '$uibModalInstance', 'ngModel', 'options', modalController],
        size: 'md',
        resolve: {
          ngModel: function() {
            return ngModel;
          },
          options: function() {
            return options;
          }
        }
      });
      return result.result;
    };
    return this;
  }]);

}).call(this);
;
(function() {
  angular.module('ui.landingWidgets').directive('lwViewportEnterClass', ["$window", "$timeout", function($window, $timeout) {
    return {

      /**
      *   @ngdoc directive
      *   @name ui.landingWidgets.directive:lwViewportEnterClass
      *   @description
      *
      *   @restrict A
      *   @param {string} lwViewportEnterClass (значение) название класса для анимаций
      *   @param {number=} [timeout=10] (значение) задержка до начала анимации(мсек)
      *   @example
      *       <pre>
      *           //- pug
      *           div(lw-viewport-enter-class="myAnimation")
      *       </pre>
       */
      restrict: 'A',
      link: function(scope, element, attr) {
        var onScroll, timeout;
        if (!attr.lwViewportEnterClass) {
          return;
        }
        timeout = 10;
        if (attr.timeout && !isNaN(parseInt(attr.timeout))) {
          timeout = parseInt(attr.timeout);
        }
        if (element[0].getBoundingClientRect().top < $window.innerHeight) {
          $timeout(function() {
            element.addClass('animated');
            return $timeout(function() {
              return element.addClass(attr.lwViewportEnterClass + ' lw-viewport-enter-class-visible');
            }, timeout);
          }, 0);
          return;
        }
        onScroll = function() {
          if (element[0].getBoundingClientRect().top < $window.innerHeight) {
            angular.element($window).off('scroll', onScroll);
            return $timeout(function() {
              return element.addClass(attr.lwViewportEnterClass + ' lw-viewport-enter-class-visible');
            }, timeout);
          }
        };
        return angular.element($window).on('scroll', onScroll);
      }
    };
  }]);

}).call(this);
;
(function() {
  angular.module('ui.landingWidgets').directive('lwViewportEnterFunction', ["$window", "$timeout", function($window, $timeout) {
    return {

      /**
      *   @ngdoc directive
      *   @name ui.landingWidgets.directive:lwViewportEnterFunction
      *   @description
      *
      *   @restrict A
      *   @param {string} lwViewportEnterFunction (значение) название класса для анимаций
      *   @param {number=} [timeout=10] (значение) задержка до начала анимации(мсек)
      *   @example
      *       <pre>
      *           //- pug
      *           div(lw-viewport-enter-class="myAnimation")
      *       </pre>
       */
      restrict: 'A',
      link: function(scope, element, attr) {
        var onScroll, timeout;
        if (!attr.lwViewportEnterFunction) {
          return;
        }
        timeout = 10;
        if (attr.timeout && !isNaN(parseInt(attr.timeout))) {
          timeout = parseInt(attr.timeout);
        }
        if (element[0].getBoundingClientRect().top < $window.innerHeight) {
          $timeout(function() {
            element.addClass('animated');
            return $timeout(function() {
              element.addClass('lw-viewport-enter-function-visible');
              return scope.$eval(attr.lwViewportEnterFunction);
            }, timeout);
          }, 0);
          return;
        }
        onScroll = function() {
          if (element[0].getBoundingClientRect().top < $window.innerHeight) {
            angular.element($window).off('scroll', onScroll);
            return $timeout(function() {
              element.addClass('lw-viewport-enter-function-visible');
              return scope.$eval(attr.lwViewportEnterFunction);
            }, timeout);
          }
        };
        return angular.element($window).on('scroll', onScroll);
      }
    };
  }]);

}).call(this);
;
(function() {
  angular.module('ui.landingWidgets').service('lwGdprRequestModal', ["$uibModal", "$injector", function($uibModal, $injector) {
    var modalController;
    modalController = function($scope, $uibModalInstance, ngModel, responseType) {
      var CmsGdprRequest;
      CmsGdprRequest = $injector.get('CmsGdprRequest');
      $scope.ngModel = angular.copy(ngModel);
      $scope.responseType = responseType;
      $scope.sendEmail = true;
      $scope.cancel = function() {
        return $uibModalInstance.dismiss('cancel');
      };
      return $scope.ok = function() {
        var action;
        $scope.error = false;
        $scope.loading = true;
        switch ($scope.responseType) {
          case 'info':
            action = CmsGdprRequest.collectUserData;
            break;
          case 'anonimization':
            action = CmsGdprRequest.anonimizeUserData;
            break;
          case 'deletion':
            action = CmsGdprRequest.deleteUserData;
        }
        return action({
          id: ngModel.id
        }, {
          sendEmail: $scope.sendEmail
        }, $uibModalInstance.close, function(data) {
          return $scope.error = true;
        });
      };
    };
    this.open = function(ngModel, responseType) {
      var result;
      result = $uibModal.open({
        animation: true,
        backdrop: 'static',
        keyboard: false,
        windowClass: 'lw-gdpr-request-modal',
        template: ('/client/landing_widgets_app/_modals/lwGdprRequest/modal.html', '<div class="modal-header"><h4 class="modal-title" translate="">GDPR response for user</h4></div><form class="modal-body" name="form" ng-hide="loading"><uic-checkbox ng-model="sendEmail"><span translate="">Send email to user with response</span></uic-checkbox><b translate="" ng-show="error">Something went wrong. Can\'t run action and send email to user. Try again later</b></form><div class="model-body" ng-show="loading"><div class="with-preloader" style="height: 7em;"><div class="preloader"></div></div></div><div class="modal-footer" ng-hide="loading"><button class="btn btn-success" ng-click="ok()" ng-disabled="form.$invalid" ng-switch="responseType"><span ng-switch-when="info" translate="">Collect information</span><span ng-switch-when="anonimization" translate="">Anonimize</span><span ng-switch-when="deletion" translate="">Delete user data</span></button><button class="btn btn-default" ng-click="cancel()" translate="">Cancel</button></div>' + ''),
        controller: ['$scope', '$uibModalInstance', 'ngModel', 'responseType', modalController],
        size: 'sm',
        resolve: {
          ngModel: function() {
            return ngModel;
          },
          responseType: function() {
            return responseType || 'info';
          }
        }
      });
      return result.result;
    };
    return this;
  }]);

}).call(this);
;
(function() {
  angular.module('ui.landingWidgets').controller('LwBaseFilterEditDefault', ["$scope", "$element", "castToValue", "dumpValue", function($scope, $element, castToValue, dumpValue) {
    var ngModelCtrl, ref, ref1;
    ngModelCtrl = $element.controller('ngModel');
    $scope.proxy = {};
    $scope.definition.filterParams.defaultOption = $scope.definition.filterParams.defaultOption || 'inRange';
    $scope.options = {
      showGte: (ref = $scope.definition.filterParams.defaultOption) === 'inRange' || ref === 'greaterThanOrEqual',
      showLte: (ref1 = $scope.definition.filterParams.defaultOption) === 'lessThanOrEqual' || ref1 === 'inRange',
      showEquals: $scope.definition.filterParams.defaultOption === 'equals'
    };
    ngModelCtrl.$render = function() {
      var eq, gte, lte, ref2;
      $scope.proxy = {
        gte: null,
        lte: null,
        eq: null
      };
      if ((ref2 = ngModelCtrl.$modelValue) === null || ref2 === (void 0)) {
        return;
      } else if (angular.isObject(ngModelCtrl.$modelValue) && ($scope.options.showLte || $scope.options.showGte)) {
        gte = castToValue(ngModelCtrl.$modelValue.gte);
        if (gte !== null) {
          $scope.proxy.gte = gte;
        }
        lte = castToValue(ngModelCtrl.$modelValue.lte);
        if (lte !== null) {
          $scope.proxy.lte = lte;
        }
      } else if ($scope.options.showEquals) {
        eq = castToValue(ngModelCtrl.$modelValue);
        if (eq !== null) {
          $scope.proxy.eq = eq;
        }
      }
    };
    return $scope.$watch('proxy', function(proxy, oldValue) {
      var value;
      if ($scope.definition.filterParams.defaultOption === 'equals') {
        if (proxy.eq !== null) {
          ngModelCtrl.$setViewValue(dumpValue(proxy.eq));
        }
      } else {
        value = {};
        if (proxy.gte !== null) {
          value.gte = proxy.gte;
        }
        if (proxy.lte !== null) {
          value.lte = proxy.lte;
        }
        if (angular.isEmpty(value)) {
          ngModelCtrl.$setViewValue(void 0);
        } else {
          ngModelCtrl.$setViewValue(value);
        }
      }
    }, true);
  }]).controller('LwBaseFilterClearDefault', ["$scope", function($scope) {
    var ref, ref1;
    $scope.proxy = {};
    $scope.definition.filterParams.defaultOption = $scope.definition.filterParams.defaultOption || 'inRange';
    $scope.options = {
      showGte: (ref = $scope.definition.filterParams.defaultOption) === 'inRange' || ref === 'greaterThanOrEqual',
      showLte: (ref1 = $scope.definition.filterParams.defaultOption) === 'lessThanOrEqual' || ref1 === 'inRange',
      showEquals: $scope.definition.filterParams.defaultOption === 'equals'
    };
    return $scope.clear = function(prop) {
      if (prop === 'eq') {
        $scope.ngModel = void 0;
      } else {
        $scope.ngModel[prop] = null;
      }
    };
  }]);

}).call(this);
;
(function() {
  angular.module('ui.landingWidgets').config(["$lwFilterViewProvider", function($lwFilterViewProvider) {
    return $lwFilterViewProvider.registerRender('bool', {
      directiveEditName: 'lw-filter-bool',
      directiveClearName: 'lw-filter-clear-bool',
      doesFilterPass: function(params, filterValue) {
        var ref;
        if (!filterValue) {
          return true;
        }
        if (isNaN(params.value)) {
          return false;
        }
        return (ref = params.value) !== null && ref !== (void 0) && ref !== 0 && ref !== false && ref !== '';
      }
    });
  }]).directive('lwFilterBool', function() {
    return {
      restrict: 'E',
      require: 'ngModel',
      scope: {
        definition: '=',
        items: '=?',
        ngDisabled: '=?'
      },
      controller: ["$scope", "$element", "$controller", function($scope, $element, $controller) {
        var ngModelCtrl;
        ngModelCtrl = $element.controller('ngModel');
        ngModelCtrl.$render = function() {
          $scope.proxy = !!ngModelCtrl.$modelValue;
        };
        return $scope.$watchCollection('proxy', function(proxy, oldValue) {
          if (!proxy) {
            ngModelCtrl.$setViewValue(void 0);
          } else {
            ngModelCtrl.$setViewValue(true);
          }
        });
      }],
      template: ('/client/landing_widgets_app/_providers/filterDirectives/lwFilterBool/lwFilterBool.html', '<div class="form-group"><uic-checkbox ng-model="proxy"><span class="lw-filter-title-bool">{{definition.headerName | translate}}</span></uic-checkbox></div>' + '')
    };
  }).directive('lwFilterClearBool', function() {
    return {
      restrict: 'E',
      scope: {
        ngModel: '=',
        definition: '=',
        items: '=?',
        ngDisabled: '=?'
      },
      controller: ["$scope", "$controller", function($scope, $controller) {
        return $scope.clear = function(index) {
          $scope.ngModel = void 0;
        };
      }],
      template: ('/client/landing_widgets_app/_providers/filterDirectives/lwFilterBool/lwFilterClearBool.html', '<span class="lw-filter-clear-default" ng-show="ngModel"><span class="label label-primary" ng-click="clear()"><span class="lw-filter-clear"><span class="fas fa-times"></span></span><span class="lw-filter-text"><span class="lw-filter-title"><span class="text">{{definition.headerName | translate}}</span></span></span></span></span>' + '')
    };
  });

}).call(this);
;
(function() {
  angular.module('ui.landingWidgets').config(["$lwFilterViewProvider", function($lwFilterViewProvider) {
    return $lwFilterViewProvider.registerRender('custom', {
      directiveEditName: 'lw-filter-custom',
      directiveClearName: 'lw-filter-clear-custom',
      doesFilterPass: function(params, filterValue) {
        return true;
      }
    });
  }]).directive('lwFilterCustom', function() {
    return {
      restrict: 'E',
      require: 'ngModel',
      scope: {
        definition: '=',
        items: '=?',
        ngDisabled: '=?'
      },
      controller: ["$scope", "$element", "$compile", function($scope, $element, $compile) {
        var ngModelCtrl;
        ngModelCtrl = $element.controller('ngModel');
        $scope.proxy = void 0;
        ngModelCtrl.$render = function() {
          $scope.proxy = angular.copy(ngModelCtrl.$modelValue);
        };
        $scope.$watchCollection('proxy', function(proxy, oldValue) {
          ngModelCtrl.$setViewValue(proxy);
        });
        $element.html($scope.definition.template || '');
        return $compile($element.contents())($scope);
      }],
      template: ''
    };
  }).directive('lwFilterClearCustom', function() {
    return {
      restrict: 'E',
      scope: {
        ngModel: '=',
        definition: '=',
        items: '=?',
        ngDisabled: '=?'
      },
      controller: ["$scope", "$element", "$compile", function($scope, $element, $compile) {
        $scope.clear = function() {
          $scope.ngModel = void 0;
        };
        $element.html($scope.definition.cleanTemplate || '');
        return $compile($element.contents())($scope);
      }],
      template: ''
    };
  });

}).call(this);
;
(function() {
  var normaliazeDate, toDate, toDatetime;

  toDate = function(value) {
    value = new Date(value);
    value.setUTCHours(12);
    value.setUTCMinutes(0);
    value.setUTCSeconds(0);
    value.setUTCMilliseconds(0);
    return value;
  };

  toDatetime = function(value) {
    if (angular.isDate(value)) {
      return value;
    }
    return new Date(value);
  };

  normaliazeDate = function(value, valueType) {
    if (!value) {
      return void 0;
    }
    if (valueType === 'date') {
      return toDate(value);
    }
    return toDatetime(value);
  };

  angular.module('ui.landingWidgets').config(["$lwFilterViewProvider", function($lwFilterViewProvider) {
    return $lwFilterViewProvider.registerRender('date', {
      directiveEditName: 'lw-filter-date',
      directiveClearName: 'lw-filter-clear-date',
      doesFilterPass: function(params, filterValue) {
        var defaultOption, eq, gte, lte, value, valueType;
        defaultOption = this.filterParams.defaultOption || 'inRange';
        valueType = this.filterParams.valueType || 'datetime';
        value = normaliazeDate(params.value, valueType);
        if (defaultOption === 'inRange' || defaultOption === 'greaterThanOrEqual') {
          gte = normaliazeDate(filterValue.gte, valueType);
          if (angular.isDate(gte)) {
            if (value === null || value === (void 0)) {
              return false;
            }
            if (gte > params.value) {
              return false;
            }
          }
        }
        if (defaultOption === 'lessThanOrEqual' || defaultOption === 'inRange') {
          lte = normaliazeDate(filterValue.lte, valueType);
          if (angular.isDate(lte)) {
            if (value === null || value === (void 0)) {
              return false;
            }
            if (lte < params.value) {
              return false;
            }
          }
        }
        if (defaultOption === 'equals') {
          eq = normaliazeDate(filterValue, valueType);
          if (angular.isDate(eq)) {
            if (value === null || value === (void 0)) {
              return false;
            }
            return eq.getTime() === value.getTime();
          }
        }
        return true;
      }
    });
  }]).directive('lwFilterDate', function() {
    return {
      restrict: 'E',
      require: 'ngModel',
      scope: {
        definition: '=',
        items: '=?',
        ngDisabled: '=?'
      },
      controller: ["$scope", "$element", "$controller", function($scope, $element, $controller) {
        var valueType;
        valueType = $scope.definition.filterParams.valueType || 'datetime';
        return $controller('LwBaseFilterEditDefault', {
          $scope: $scope,
          $element: $element,
          castToValue: function(value) {
            return normaliazeDate(value, valueType);
          },
          dumpValue: function(value) {
            if (valueType === 'date') {
              if (!angular.isString(value)) {
                return value.toDateISOString();
              }
              return value;
            } else {
              return value.toISOString();
            }
          }
        });
      }],
      template: ('/client/landing_widgets_app/_providers/filterDirectives/lwFilterDate/lwFilterDate.html', '<h6 class="lw-filter-title" ng-show="definition.headerName">{{definition.headerName | translate}}</h6><div class="form-group" ng-if="options.showGte"><label translate="">From date:</label><uic-datepicker-input ng-model="proxy.gte" ng-model-type="date" ng-disabled="ngDisabled"></uic-datepicker-input></div><div class="form-group" ng-if="options.showLte"><label translate="">To date:</label><uic-datepicker-input ng-model="proxy.lte" ng-model-type="date" ng-disabled="ngDisabled"></uic-datepicker-input></div><div class="form-group" ng-if="options.showEquals"><uic-datepicker ng-model="proxy.eq" ng-model-type="date" ng-disabled="ngDisabled"></uic-datepicker></div>' + '')
    };
  }).directive('lwFilterClearDate', function() {
    return {
      restrict: 'E',
      scope: {
        ngModel: '=',
        definition: '=',
        items: '=?',
        ngDisabled: '=?'
      },
      controller: ["$scope", "$controller", function($scope, $controller) {
        return $controller('LwBaseFilterClearDefault', {
          $scope: $scope
        });
      }],
      template: ('/client/landing_widgets_app/_providers/filterDirectives/lwFilterDate/lwFilterClearDate.html', '<span class="lw-filter-clear-default" ng-switch="options.showEquals"><span ng-switch-when="false"><span class="label label-primary" ng-show="ngModel.gte" ng-click="clear(\'gte\')"><span class="lw-filter-clear"><span class="fas fa-times"></span></span><span class="lw-filter-text"><span class="lw-filter-title" ng-show="definition.headerName"><span class="text">{{definition.headerName | translate}}</span><span> </span></span>&ge;\n{{ngModel.gte | date: \'shortDate\'}}</span></span><span class="label label-primary" ng-show="ngModel.lte" ng-click="clear(\'lte\')"><span class="lw-filter-clear"><span class="fas fa-times"></span></span><span class="lw-filter-text"><span class="lw-filter-title" ng-show="definition.headerName"><span class="text">{{definition.headerName | translate}}</span><span> </span></span>&le;\n{{ngModel.lte | date: \'shortDate\'}}</span></span></span><span ng-switch-when="true"><span class="label label-primary" ng-show="ngModel" ng-click="clear(\'eq\')"><span class="lw-filter-clear"><span class="fas fa-times"></span></span><span class="lw-filter-text"><span class="lw-filter-title" ng-show="definition.headerName"><span class="text">{{definition.headerName | translate}}</span><span> </span></span>{{ngModel | date: \'shortDate\'}}</span></span></span></span>' + '')
    };
  });

}).call(this);
;
(function() {
  var toNumber;

  toNumber = function(value) {
    var err;
    try {
      value = parseFloat(value);
    } catch (error) {
      err = error;
    }
    if (angular.isNumber(value)) {
      return value;
    }
    return void 0;
  };

  angular.module('ui.landingWidgets').config(["$lwFilterViewProvider", function($lwFilterViewProvider) {
    return $lwFilterViewProvider.registerRender('number', {
      directiveEditName: 'lw-filter-number',
      directiveClearName: 'lw-filter-clear-number',
      doesFilterPass: function(params, filterValue) {
        var defaultOption, gte, lte;
        defaultOption = this.filterParams.defaultOption || 'inRange';
        if (defaultOption === 'inRange' || defaultOption === 'greaterThanOrEqual') {
          gte = toNumber(filterValue.gte);
          if (angular.isNumber(gte)) {
            if (!angular.isDefined(params.value)) {
              return false;
            }
            if (gte > params.value) {
              return false;
            }
          }
        }
        if (defaultOption === 'lessThanOrEqual' || defaultOption === 'inRange') {
          lte = toNumber(filterValue.lte);
          if (angular.isNumber(lte)) {
            if (!angular.isDefined(params.value)) {
              return false;
            }
            if (lte < params.value) {
              return false;
            }
          }
        }
        return true;
      }
    });
  }]).directive('lwFilterNumber', function() {
    return {
      restrict: 'E',
      require: 'ngModel',
      scope: {
        definition: '=',
        items: '=?',
        ngDisabled: '=?'
      },
      controller: ["$scope", "$element", "$controller", function($scope, $element, $controller) {
        $controller('LwBaseFilterEditDefault', {
          $scope: $scope,
          $element: $element,
          castToValue: function(value) {
            value = toNumber(value);
            if (isNaN(value)) {
              return void 0;
            }
            return value;
          },
          dumpValue: function(value) {
            return value;
          }
        });
        return $scope.options.step = ($scope.definition.filterParams || {}).step || 0.01;
      }],
      template: ('/client/landing_widgets_app/_providers/filterDirectives/lwFilterNumber/lwFilterNumber.html', '<h6 class="lw-filter-title" ng-show="definition.headerName">{{definition.headerName | translate}}</h6><div class="form-group" ng-if="options.showGte"><label translate="">Greater than or equals:</label><input class="form-control" ng-model="proxy.gte" type="number" step="{{:: step}}" ng-disabled="ngDisabled"/></div><div class="form-group" ng-if="options.showLte"><label translate="">Less than or equals:</label><input class="form-control" ng-model="proxy.lte" type="number" step="{{:: step}}" ng-disabled="ngDisabled" min="{{proxy.gte}}"/></div>' + '')
    };
  }).directive('lwFilterClearNumber', function() {
    return {
      restrict: 'E',
      scope: {
        ngModel: '=',
        definition: '=',
        items: '=?',
        ngDisabled: '=?'
      },
      controller: ["$scope", "$controller", function($scope, $controller) {
        return $controller('LwBaseFilterClearDefault', {
          $scope: $scope
        });
      }],
      template: ('/client/landing_widgets_app/_providers/filterDirectives/lwFilterNumber/lwFilterClearNumber.html', '<span class="lw-filter-clear-default" ng-switch="options.showEquals"><span ng-switch-when="false"><span class="label label-primary" ng-show="ngModel.gte != null" ng-click="clear(\'gte\')"><span class="lw-filter-clear"><span class="fas fa-times"></span></span><span class="lw-filter-text"><span class="lw-filter-title" ng-show="definition.headerName"><span class="text">{{definition.headerName | translate}}</span><span> </span></span>&ge;\n{{ngModel.gte | number: options.step}}</span></span><span class="label label-primary" ng-show="ngModel.lte != null" ng-click="clear(\'lte\')"><span class="lw-filter-clear"><span class="fas fa-times"></span></span><span class="lw-filter-text"><span class="lw-filter-title" ng-show="definition.headerName"><span class="text">{{definition.headerName | translate}}</span><span> </span></span>&le;\n{{ngModel.lte | number: options.step}}</span></span></span></span>' + '')
    };
  });

}).call(this);
;
(function() {
  angular.module('ui.landingWidgets').config(["$lwFilterViewProvider", function($lwFilterViewProvider) {
    return $lwFilterViewProvider.registerRender('set', {
      directiveEditName: 'lw-filter-set',
      directiveClearName: 'lw-filter-clear-set',
      doesFilterPass: function(params, filterValue) {
        filterValue = (filterValue || {}).inq || [];
        if (filterValue.length === 0) {
          return true;
        }
        if (angular.isArray(params.value)) {
          return params.value.some(function(v) {
            return filterValue.includes(v);
          });
        }
        return filterValue.includes(params.value);
      }
    });
  }]).controller('LwBaseFilterSet', ["$scope", "$filter", "$constants", function($scope, $filter, $constants) {
    var cache, l10n, translate;
    translate = $filter('translate');
    l10n = $filter('l10n');
    cache = {};
    if (angular.isString($scope.definition.filterParams.values)) {
      $scope.values = $constants.resolve($scope.definition.filterParams.values);
    } else if (angular.isArray($scope.definition.filterParams.values)) {
      $scope.values = $scope.definition.filterParams.values;
    } else {
      $scope.$watch('items', function(items, oldValue) {
        $scope.values = (items || []).map(function(item) {
          var value;
          value = $scope.definition.valueGetter({
            data: item
          });
          return {
            value: value,
            description: value
          };
        });
      }, true);
    }
    $scope.$watch('values', function(items, oldValue) {
      cache = {};
      items.forEach(function(o) {
        return cache[o.value] = o;
      });
    }, true);
    $scope.getText = function(id) {
      var o;
      o = cache[id] || {};
      if (o.description) {
        if (angular.isString(o.description)) {
          return translate(o.description);
        }
        return l10n(o.description);
      }
      if (angular.isDefined(o.value)) {
        return o.value;
      }
      return id;
    };
  }]).directive('lwFilterSet', function() {
    return {
      restrict: 'E',
      require: 'ngModel',
      scope: {
        definition: '=',
        items: '=?',
        ngDisabled: '=?'
      },
      controller: ["$scope", "$element", "$controller", function($scope, $element, $controller) {
        var ngModelCtrl;
        $controller('LwBaseFilterSet', {
          $scope: $scope
        });
        ngModelCtrl = $element.controller('ngModel');
        ngModelCtrl.$render = function() {
          if (!ngModelCtrl.$modelValue || angular.isEmpty(ngModelCtrl.$modelValue) || angular.isEmpty(ngModelCtrl.$modelValue.inq)) {
            $scope.proxy = [];
          } else {
            $scope.proxy = ngModelCtrl.$modelValue.inq || [];
          }
        };
        return $scope.$watchCollection('proxy', function(proxy, oldValue) {
          if (angular.isEmpty(proxy)) {
            ngModelCtrl.$setViewValue(void 0);
          } else {
            ngModelCtrl.$setViewValue({
              inq: proxy
            });
          }
        });
      }],
      template: ('/client/landing_widgets_app/_providers/filterDirectives/lwFilterSet/lwFilterSet.html', '<h6 class="lw-filter-title" ng-show="definition.headerName">{{definition.headerName | translate}}</h6><div class="form-group"><uic-constant ng-model="$parent.proxy" checkbox="values" ng-if="values.length" ng-model-type="array"></uic-constant></div>' + '')
    };
  }).directive('lwFilterClearSet', function() {
    return {
      restrict: 'E',
      scope: {
        ngModel: '=',
        definition: '=',
        items: '=?',
        ngDisabled: '=?'
      },
      controller: ["$scope", "$controller", function($scope, $controller) {
        $controller('LwBaseFilterSet', {
          $scope: $scope
        });
        return $scope.clear = function(index) {
          var base;
          (base = $scope.ngModel).inq || (base.inq = []);
          $scope.ngModel.inq.remove(index);
          if ($scope.ngModel.inq.length === 0) {
            $scope.ngModel = void 0;
          } else {
            $scope.ngModel = angular.copy($scope.ngModel);
          }
        };
      }],
      template: ('/client/landing_widgets_app/_providers/filterDirectives/lwFilterSet/lwFilterClearSet.html', '<span class="lw-filter-clear-default" ng-show="ngModel.inq.length"><span class="label label-primary" ng-repeat="id in ngModel.inq" ng-click="clear($index)"><span class="lw-filter-clear"><span class="fas fa-times"></span></span><span class="lw-filter-text"><span class="lw-filter-title" ng-show="definition.headerName"><span class="text">{{definition.headerName | translate}}</span><span> </span></span>{{getText(id)}}</span></span></span>' + '')
    };
  });

}).call(this);
;

/**
*   @ngdoc object
*   @name ui.landingWidgets.$lwFilterViewProvider
*   @header ui.landingWidgets.$lwFilterViewProvider
*   @description провайдер для определения фильтровочных объектов
 */

(function() {
  angular.module('ui.landingWidgets').provider('$lwFilterView', function() {
    var filtersConfig, renders;
    renders = {};
    filtersConfig = {};
    this.registerRender = function(filterName, config) {
      renders[filterName] = config || {};
    };
    this.setFiltersConfig = function(columnsName, config) {
      filtersConfig[columnsName] = config;
    };
    this.$get = [
      function() {
        return {
          getFiltersConfig: function(columnsName) {
            return angular.copy(filtersConfig[columnsName]) || {};
          },
          getRenderConfig: function(name) {
            if (!renders[name] || !renders[name].doesFilterPass || !renders[name].directiveEditName || !renders[name].directiveClearName) {
              console.warn("$lwFilterView: render '" + name + "' doesn't has all properties");
            }
            return renders[name] || {};
          }
        };
      }
    ];
    return this;
  });

}).call(this);
;
(function() {


}).call(this);
;
angular.module('ui.cms').run(['gettextCatalog', function (gettextCatalog) {
/* jshint -W100 */
    gettextCatalog.setStrings('cz', {"Add another email":"Přidat další e-mail","Add another phone number":"Přidat další telefonní číslo","Add phone number":"Přidat telefoní číslo","After that, click on the \"Send Request\" button":"Poté klikněte na tlačítko \"Odeslat požadavek\"","Anonimization request":"Žádost o anonymizaci","Anonimize":"Anonymizovat","Cancel":"Zrušit","Click on the button \"Send letters and sms with verification codes\". For all the mailboxes and phone numbers you specified, you will receive verification codes, which you must enter on the same page without restarting it":"Klikněte na tlačítko \"Odeslat e-zpravý a sms s ověřovacími kódy\". Pro všechny e-mailové adresy a telefonní čísla obdržíte ověřovací kódy, které musíte zadat na stejnou stránku bez nutnosti restartování","Close":"Zavřít","Collect information":"Shromažďovat informace","Delete user data":"Smazat uživatelská data","Deletion request":"Požadavek na smazání","From date":"Od data","GDPR response for user":"GDPR odpověď pro uživatele","Greater than or equals":"Větší nebo rovno","In order for us to be sure that the mailboxes and phone numbers you have specified do belong to you, you should do the following steps":"Je důležité, abyste potvrdili, že e-mailové schránky a telefonní čísla jsou Vaše, a proto byste měli provést následující kroky","Information request":"Žádost o informace","Less than or equals":"Méně nebo rovno","Load video":"Nahrát video","No image available":"K dispozici není žádný obrázek","No video available":"K dispozici není žádné video","Send email to user with response":"Odeslat e-mail uživateli s odpovědí","Send letters and sms with verification codes":"Poslat emaily a sms s ověřovacími kódy","Something went wrong. Can't run action and send email to user. Try again later":"Něco se pokazilo. Nelze spustit akci a odeslat uživateli e-mail. Zkuste to prosim později","Specify all mail boxes for which you would like to receive information and all phone numbers (in the international format)":"Zadejte všechny e-mailové adresy, u kterých chcete přijímat informace, a všechna telefonní čísla (v mezinárodním formátu)","Thanks for your request. We will answer you as soon as possible":"Děkujeme za Vaši žádost. Odpovíme Vám co nejdříve","To date":"K datu","Verify":"Potvrdit","We will review your application within 1-30 days from the date of request and send you letters with responses to all the mail boxes that you specified":"Vaši žádost zkontrolujeme do 1-30 dnů ode dne podání žádosti a pošleme Vám zpravy s odpověďmi na všechny určené poštovní schránky","Your email":"Vaše emailová adresa","Your phone":"Vaše telefonní číslo","remove":"odstranit"});
    gettextCatalog.setStrings('ru', {"Add another email":"Добавить дополнительный email","Add another phone number":"Добавить дополнительный телефонный номер","Add phone number":"Добавить телефонный номер","After that, click on the \"Send Request\" button":"После этого нажмите на кнопку \"Отослать запрос\"","Anonimization request":"Запрос на анонимизацию","Anonimize":"Анонимизировать","Cancel":"Отменить","Click on the button \"Send letters and sms with verification codes\". For all the mailboxes and phone numbers you specified, you will receive verification codes, which you must enter on the same page without restarting it":"Нажмите на кнопку \"Отослать письма и смс с кодами подтверждений\". На все указанные вами почтовые ящики и номера телефонов придут коды подтверждения, которые вы должны ввести на этой же странице не перезагружая ее","Close":"Закрыть","Collect information":"Собрать информацию","Delete user data":"Удалить пользовательские данные","Deletion request":"Запрос на удаление","From date":"До даты","GDPR response for user":"GDRP-ответы для пользователей","Greater than or equals":"Больше чем или равно","In order for us to be sure that the mailboxes and phone numbers you have specified do belong to you, you should do the following steps":"Для того, чтоб мы были уверены, что указанные вами почтовые ящики и номера телефонов действительно принадлежат вам, вы должны сделать следующее","Information request":"Информационный запрос","Less than or equals":"Менее или равно","Load video":"Загрузить видео","Next image":"Следующая картинка","No video available":"Нет видео","Previous image":"Предыдущая картинка","Send email to user with response":"Отослать письмо пользователю с ответом","Send letters and sms with verification codes":"Отослать письма и смс с кодами подтверждений","Something went wrong. Can't run action and send email to user. Try again later":"Что-то пошло не так. Не получилось выполнить действие и отослать почту пользователю. Попытайтесь позже","Specify all mail boxes for which you would like to receive information and all phone numbers (in the international format)":"Укажите все почтовые ящики, для которых вы хотите получить информацию и все телефонные номера (в международном формате)","Thanks for your request. We will answer you as soon as possible":"Спасибо за ваш запрос. Мы постараемся быстро ответить вам","To date":"До даты","Verify":"Подтвердить","We will review your application within 1-30 days from the date of request and send you letters with responses to all the mail boxes that you specified":"Мы рассмотрим вашу заявку в течении 1-30 дней с момента запроса и отошлем письма с ответами на все почтовые ящики которые вы указали","Your email":"Ваш email","Your phone":"Ваш телефон","remove":"удалить"});
/* jshint +W100 */
}]);