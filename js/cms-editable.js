
/**
*    @ngdoc overview
*    @name ui.cms.editable
*    @description модуль содержит директивы для базового редактирования объектов
*       cms, директивы для логина/разлогина пользователя, и т.п.
*       <br/>
*       Его стоит подключать только если нужна админка или редактирование объектов.
*       На обычных сайтах хватит с головой и {@link ui.cms ui.cms} с cms.models.
*
 */

(function() {
  angular.module('ui.cms.editable', ['gettext', 'ui.router', 'ui.cms']);

}).call(this);
;
(function() {
  angular.module('ui.cms.editable').directive('uiceAvatarManager', function() {
    return {

      /**
      *   @ngdoc directive
      *   @name ui.cms.editable.directive:uiceAvatarManager
      *   @description виджет для загрузки/редактирования/удаления аватарки mainImg у объекта CmsUser
      *   @restrict E
      *   @param {object} ngModel (ссылка) модель объекта CmsUser, которая содержит поле mainImg
      *   @param {boolean=} [disableDelete=false] (значение) возможность отключения удаления картинки
      *   @param {boolean=} ngDisabled (ссылка) флаг доступности тега
      *   @example
      *       <pre>
      *           id = 1 # mongodb objectId or int(for django app)
      *           $scope.user = CmsUser.findById({id})
      *       </pre>
      *       <pre>
      *           //- pug
      *           uice-avatar-manager(ng-model="user")
      *           // если нужен аватар в круге
      *           uice-avatar-manager.circle(ng-model="user")
      *           // малый размер круга
      *           uice-avatar-manager.circle.sm(ng-model="user")
      *       </pre>
      *       <example module="DocExampleApp">
      *           <file name="index.html">
      *               <p class='text-danger'>Загрузка файлов из документации не работает!</p>
      *               <div class="row" ng-controller="defaultCtrl">
      *                   <div class="col-sm-50">
      *                       <p>Обычный вид</p>
      *                       <uice-avatar-manager ng-model="user">
      *                       </uice-avatar-manager>
      *                   </div>
      *                   <div class="col-sm-50">
      *                   </div>
      *               </div>
      *               <div class="row" ng-controller="defaultCtrl">
      *                   <div class="col-sm-50">
      *                       <p>Вид в круге</p>
      *                       <uice-avatar-manager class='circle' ng-model="user">
      *                       </uice-avatar-manager>
      *                   </div>
      *                   <div class="col-sm-50">
      *                       <p>Вид в малом круге</p>
      *                       <uice-avatar-manager class='circle sm' ng-model="user">
      *                       </uice-avatar-manager>
      *                   </div>
      *               </div>
      *           </file>
      *       </example>
       */
      restrict: 'E',
      transclude: {
        noImgPlaceholder: '?noImgPlaceholder'
      },
      scope: {
        ngModel: '=',
        ngDisabled: '=?',
        disableDelete: '@?'
      },
      controller: ["$scope", "$attrs", "$currentUser", "CmsUser", "uiceL10nFileEditorModal", function($scope, $attrs, $currentUser, CmsUser, uiceL10nFileEditorModal) {
        var ref;
        $scope._disableDelete = (ref = $scope.disableDelete) === true || ref === 'true';
        $scope.manageMainImg = function() {
          var options;
          if ($scope.ngDisabled) {
            return;
          }
          options = {
            maxSize: '13 MB',
            accept: 'image/jpeg,image/png,image/gif',
            forModelField: 'mainImg'
          };
          return uiceL10nFileEditorModal.edit($scope.ngModel, options).then(function(mainImg) {
            $currentUser.setData({
              id: $currentUser.id,
              mainImg: mainImg
            });
            $scope.ngModel.mainImg = mainImg;
          });
        };
        return $scope.removeMainImg = function() {
          var options;
          if ($scope.ngDisabled) {
            return;
          }
          options = {
            title: angular.tr('Delete avatar?'),
            forModelField: 'mainImg'
          };
          return uiceL10nFileEditorModal["delete"]($scope.ngModel, options).then(function() {
            if ($scope.ngModel.id === $currentUser.id) {
              $currentUser.setData({
                id: $currentUser.id,
                mainImg: null
              });
            }
            $scope.ngModel.mainImg = null;
          });
        };
      }],
      template: ('/client/cms_editable_app/_directives/uiceAvatarManager/uiceAvatarManager.html', '<div ng-switch="!!ngModel.mainImg"><div class="text-center" ng-switch-when="false"><div class="no-img-placeholder" ng-transclude="noImgPlaceholder"><div class="thumbnail nomargin"><div class="fas fa-user"></div></div></div><button class="btn btn-sm btn-success btn-upload" ng-click="manageMainImg()" style="margin: 1em auto;" ng-disabled="ngDisabled"><div class="fas fa-cloud-upload-alt"></div><span class="btn-text-default" translate="">Upload image</span><span class="btn-text-sm" translate="">Upload</span></button></div><div class="thumbnail nomargin" ng-switch-when="true"><uic-picture class="thumbnail-content" file="ngModel.mainImg" size="350" ng-click="manageMainImg()"></uic-picture><div class="thumbnail-actions"><div class="btn-group"><button class="btn btn-primary" ng-click="manageMainImg()" ng-disabled="ngDisabled"><span class="fas fa-pencil-alt"></span><span translate="">Edit</span></button><button class="btn btn-danger" ng-click="removeMainImg()" ng-disabled="ngDisabled" title="{{\'Delete\' | translate}}" ng-if="!_disableDelete"><span class="fas fa-trash-alt nopadding"></span></button></div></div></div></div>' + '')
    };
  });

}).call(this);
;
(function() {
  angular.module('ui.cms.editable').directive('uiceColorPicker', ["H", "gettextCatalog", function(H, gettextCatalog) {
    var getInputType, hexToRgb, isHtml5ColorAvailable, rgbToHex;
    isHtml5ColorAvailable = (function() {
      var tag;
      tag = document.createElement("input");
      tag.setAttribute("type", "color");
      return tag.type === "color";
    })();
    getInputType = function(t) {
      var type;
      type = t || 'hex';
      if (!['rgba', 'rgb', 'hex'].includes(type)) {
        return 'hex';
      }
      return type;
    };
    rgbToHex = function(rgb) {
      var b, g, r;
      if (!rgb) {
        return null;
      }
      rgb = rgb.split(',');
      if (rgb.length !== 3) {
        return null;
      }
      r = parseInt(rgb[0].split('(')[1]);
      g = parseInt(rgb[1]);
      b = parseInt(rgb[2].split(')')[0]);
      return "#" + ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1);
    };
    hexToRgb = function(hex) {
      var b, g, r, result, shorthandRegex;
      if (!hex) {
        return null;
      }
      shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
      hex = hex.replace(shorthandRegex, function(m, r, g, b) {
        return r + r + g + g + b + b;
      });
      result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
      if (result) {
        r = parseInt(result[1], 16);
        g = parseInt(result[2], 16);
        b = parseInt(result[3], 16);
        return "rgb(" + r + ", " + g + ", " + b + ")";
      }
      return null;
    };
    return {

      /**
      *   @ngdoc directive
      *   @name ui.cms.editable.directive:uiceColorPicker
      *   @description выбирает цвета в hex. Для браузеров, которые не поддерживают input[type='color']
      *       директива подсовывает пикер от https://github.com/Simonwep/pickr
      *   @restrict E
      *   @param {string} ngModel (ссылка) цвет
      *   @param {string=} [type='hex'] (значение) тип цвета. Возможные значения - hex, rgb, rgba. Если указан тип rgba, то будет использован
      *       пикер на js. Если указан тип rgb, и есть возможность использовать стандартный инпут html, то он будет использован вместо пикера на js
      *   @param {string=} inputClass (значение) css-класс, который будет добавлен к input-у
      *   @param {boolean=} [withoutHtml5=false] (значение) использовать ли js-пикер вместо встроенного в браузер
      *   @param {boolean=} [showText=false] (значение) отображать ли текстовый инпут с цветом рядом с пикером
      *   @param {string=} placeholder (значение) текст, который отображается на месте пустого ngModel. Доступен только при showText == true
      *   @param {boolean=} ngDisabled (ссылка) флаг доступности тега
      *   @example
      *       <pre>
      *           $scope.color = "#045735"
      *           $scope.rgbColor = "rgb(10, 15, 100)"
      *           $scope.rgbaColor = "rgba(0, 200, 50, 0.6)"
      *       </pre>
      *       <pre>
      *           //- pug
      *           // пикер по умолчанию для hex-цвета
      *           uice-color-picker(ng-model="color")
      *           // пикер для hex, использующий js реализацию вместо встроенной в браузер
      *           uice-color-picker(ng-model="color", without-html5='true')
      *           // пикер уменьшенного размера
      *           uice-color-picker(ng-model="color", without-html5='true', input-class='input-sm')
      *           // пикер с текстовым значением цвета в соседнем текстовом инпуте
      *           uice-color-picker(ng-model="color", show-text='true', without-html5='true')
      *           // пикер для rgb на html5 input
      *           uice-color-picker(ng-model="rgbColor", type='rgb')
      *           // пикер для rgb на js
      *           uice-color-picker(ng-model="rgbColor", type='rgb', without-html5='true')
      *           // пикер для rgba
      *           uice-color-picker(ng-model="rgbaColor", type='rgba')
      *       </pre>
      *       <example module="DocExampleApp">
      *           <file name="script.js">
      *               mkDocCtrl('uiceColorPickerCtrl', {
      *                   color: "#045735",
      *                   rgbColor: "rgb(10, 15, 100)",
      *                   rgbaColor: "rgba(0, 200, 50, 0.6)"
      *               });
      *           </file>
      *           <file name="index.html">
      *               <div ng-controller="uiceColorPickerCtrl">
      *                   <p>Color = <b>{{color}}</b></p>
      *                   <p>RgbColor = <b>{{rgbColor}}</b></p>
      *                   <p>RgbaColor = <b>{{rgbaColor}}</b></p>
      *                   <hr/>
      *                   <div class="row">
      *                       <div class="col-md-50">
      *                           <div class="form-group">
      *                               <label>Пикер по умолчанию для hex-цвета</label>
      *                               <uice-color-picker ng-model="color"></uice-color-picker>
      *                           </div>
      *                           <div class="form-group">
      *                               <label>Пикер для hex, использующий js реализацию вместо встроенной в браузер</label>
      *                               <uice-color-picker ng-model="color" without-html5='true'></uice-color-picker>
      *                           </div>
      *                           <div class="form-group">
      *                               <label>Пикер уменьшенного размера</label>
      *                               <uice-color-picker ng-model="color" input-class='input-sm' without-html5='true'></uice-color-picker>
      *                           </div>
      *                           <div class="form-group">
      *                               <label>Пикер с текстовым значением цвета в соседнем текстовом инпуте</label>
      *                               <uice-color-picker ng-model="color" without-html5='true' show-text='true'></uice-color-picker>
      *                           </div>
      *                       </div>
      *                       <div class="col-md-50">
      *                           <div class="form-group">
      *                               <label>Пикер для rgb на html5 input</label>
      *                               <uice-color-picker ng-model="rgbColor" type='rgb'></uice-color-picker>
      *                           </div>
      *                           <div class="form-group">
      *                               <label>Пикер для rgb на js</label>
      *                               <uice-color-picker ng-model="rgbColor" type='rgb' without-html5='true'></uice-color-picker>
      *                           </div>
      *                           <div class="form-group">
      *                               <label>Пикер для rgba</label>
      *                               <uice-color-picker ng-model="rgbaColor" type='rgba'></uice-color-picker>
      *                           </div>
      *                       </div>
      *                   </div>
      *               </div>
      *           </file>
      *       </example>
       */
      restrict: 'E',
      require: 'ngModel',
      scope: {
        ngDisabled: '=?',
        placeholder: '@?'
      },
      link: function($scope, $element, $attrs, ngModelCtrl) {
        var pickr, type, useHtml5Input;
        type = getInputType($attrs.type);
        useHtml5Input = isHtml5ColorAvailable && !$attrs.withoutHtml5 && ['rgb', 'hex'].includes(type);
        $scope.proxy = null;
        pickr = null;
        ngModelCtrl.$render = function() {
          if (type === 'rgb' && useHtml5Input) {
            $scope.proxy = rgbToHex(ngModelCtrl.$modelValue);
          } else {
            $scope.proxy = ngModelCtrl.$modelValue;
          }
          if (!useHtml5Input && pickr) {
            setTimeout(function() {
              pickr.setColor(ngModelCtrl.$modelValue);
            });
          }
        };
        $scope.$watch('proxy', function(proxy, oldValue) {
          if (type === 'rgb' && useHtml5Input) {
            ngModelCtrl.$setViewValue(hexToRgb(proxy));
          } else {
            ngModelCtrl.$setViewValue(proxy);
          }
        });
        $scope.onTextColorClick = function() {
          var inputs;
          if (!$scope.proxy) {
            inputs = $element.find('input');
            inputs[0].click();
          }
        };
        if (!useHtml5Input) {
          return H.loadOnce.js("https://cdn.jsdelivr.net/npm/@simonwep/pickr@1.5.1/dist/pickr.es5.min.js", function() {
            var clear, ref;
            clear = true;
            if ($attrs.hasOwnProperty('required') && ((ref = $attrs.required) !== 'false' && ref !== false)) {
              clear = false;
            }
            pickr = Pickr.create({
              el: $element.find('input')[0],
              theme: 'nano',
              useAsButton: true,
              components: {
                opacity: type === 'rgba',
                preview: true,
                hue: true,
                interaction: {
                  clear: clear
                }
              },
              strings: {
                clear: gettextCatalog.getString('Clear color value')
              }
            });
            pickr.setColor(ngModelCtrl.$modelValue || '');
            pickr.on('change', function(color) {
              if (type === 'hex') {
                color = '#' + color.toHEXA().join('');
              } else if (type === 'rgb') {
                color = hexToRgb('#' + color.toHEXA().join(''));
              } else if (type === 'rgba') {
                color = color.toRGBA();
                color[0] = color[0].toFixed(0);
                color[1] = color[1].toFixed(0);
                color[2] = color[2].toFixed(0);
                color[3] = color[3].toFixed(2);
                color = "rgba(" + color.join(', ') + ")";
              }
              $scope.$apply(function() {
                $scope.proxy = color;
              });
            });
            pickr.on('clear', function() {
              return $scope.$apply(function() {
                $scope.proxy = null;
              });
            });
          });
        }
      },
      template: function($element, $attrs) {
        var inputClass, ref, ref1, type, useHtml5Input;
        type = getInputType($attrs.type);
        useHtml5Input = isHtml5ColorAvailable && !$attrs.withoutHtml5 && ['rgb', 'hex'].includes(type);
        inputClass = $attrs.inputClass || '';
        if (useHtml5Input) {
          if ((ref = $attrs.showText) === true || ref === 'true') {
            return "<div class=\"input-group\">\n    <div class=\"input-group-addon nopadding\">\n        <input class='form-control " + inputClass + "'\n            ng-model='proxy'\n            type='color'\n            ng-disabled='ngDisabled'\n        >\n    </div>\n    <input type='text' ng-model=\"proxy\"\n        class=\"form-control " + inputClass + "\"\n        ng-click=\"onTextColorClick()\"\n        placeholder=\"{{placeholder || ('Select color' | translate)}}\"\n    >\n</div>";
          }
          return "<input class='form-control " + inputClass + "'\n    ng-model='proxy'\n    type='color'\n    ng-disabled='ngDisabled'\n>";
        }
        H.loadOnce.css("https://cdn.jsdelivr.net/npm/@simonwep/pickr@1.5.1/dist/themes/nano.min.css");
        if ((ref1 = $attrs.showText) === true || ref1 === 'true') {
          return "<div class=\"input-group\">\n    <div class=\"input-group-addon nopadding\">\n        <input class='form-control uice-color-picker-input  " + inputClass + "'\n            ng-model='proxy'\n            type='text'\n            ng-disabled='ngDisabled'\n            style=\"background: {{proxy}};\"\n            spellcheck='false'\n        >\n    </div>\n    <input type='text' ng-model=\"proxy\"\n        class=\"form-control " + inputClass + "\"\n        ng-click=\"onTextColorClick()\"\n        placeholder=\"{{placeholder || ('Select color' | translate)}}\"\n    >\n</div>";
        }
        return "<input class='form-control uice-color-picker-input " + inputClass + "'\n    ng-model='proxy'\n    type='text'\n    ng-disabled='ngDisabled'\n    style=\"background: {{proxy}};\"\n    spellcheck='false'\n>";
      }
    };
  }]);

}).call(this);
;
(function() {
  angular.module('ui.cms.editable').service('contactEditModal', ["$uibModal", function($uibModal) {
    var modalController;
    modalController = function($scope, $uibModalInstance, ngModel, options) {
      $scope.ngModel = angular.copy(ngModel);
      $scope.options = options;
      $scope.cancel = function() {
        return $uibModalInstance.dismiss('cancel');
      };
      return $scope.ok = function() {
        return $uibModalInstance.close($scope.ngModel);
      };
    };
    this.open = function(ngModel, options) {
      var result;
      result = $uibModal.open({
        animation: true,
        template: ('/client/cms_editable_app/_directives/uiceContactsEditor/contactEditModal/modal.html', '<div class="modal-body"><uice-contact-item-editor ng-model="ngModel"></uice-contact-item-editor></div><div class="modal-footer"><button class="btn btn-success" ng-click="ok()">OK</button><button class="btn btn-default" ng-click="cancel()" translate="">Cancel</button></div>' + ''),
        controller: ['$scope', '$uibModalInstance', 'ngModel', 'options', modalController],
        size: 'sm',
        resolve: {
          ngModel: function() {
            return ngModel;
          },
          options: function() {
            return options || {};
          }
        }
      });
      return result.result;
    };
    return this;
  }]);

}).call(this);
;
(function() {
  angular.module('ui.cms.editable').factory('ContactValidator', function() {
    return {
      isEmail: function(email) {
        var re;
        re = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
        return re.test(email);
      },
      isPhone: function(phone) {
        var re;
        re = /^([ \.]?[0-9 ()+-])+$/i;
        return re.test(phone);
      }
    };
  }).directive('uiceContactsEditor', ["ContactValidator", "contactEditModal", function(ContactValidator, contactEditModal) {
    return {

      /**
      *   @ngdoc directive
      *   @name ui.cms.editable.directive:uiceContactsEditor
      *   @description
      *       Редактор контактов для SiteSettings
      *   @restrict E
      *   @param {array<Object>} ngModel (ссылка) массив моделей. Описание модели {@link ui.cms.directive:uicContact см. тут}
      *   @param {boolean=} ngDisabled (ссылка) флаг доступности тега
      *   @example
      *       <pre>
      *           $scope.contacts = [
      *               {type: 'email', value: 'hello@kitty.com', description: {ru: 'Мое мыло', en: 'my email'}},
      *               {type: 'tel', value: '+000 000 000'}
      *           ]
      *       </pre>
      *       <pre>
      *           //- pug
      *           uice-contacts-editor(ng-model="contacts")
      *       </pre>
      *       <example module="DocExampleApp">
      *           <file name="script.js">
      *               mkDocCtrl('uiceContactsEditorCtrl', {
      *                   contacts:[
      *                       {type: 'email', value: 'hello@kitty.com', description: {ru: 'Мое мыло', en: 'my email'}},
      *                       {type: 'tel', value: '+000 000 000'}
      *                   ]
      *               });
      *           </file>
      *           <file name="index.html">
      *               <div ng-controller="uiceContactsEditorCtrl">
      *                 <uice-contacts-editor ng-model="contacts"></uice-contacts-editor>
      *               </div>
      *           </file>
      *       </example>
       */
      restrict: 'E',
      scope: {
        ngModel: '=',
        ngDisabled: '=?'
      },
      controller: ["$scope", function($scope) {
        $scope.newString = '';
        $scope.add = function() {
          var contact;
          contact = {
            value: angular.copy($scope.newString)
          };
          if (ContactValidator.isEmail(contact.value)) {
            contact.type = 'email';
          } else if (ContactValidator.isPhone(contact.value)) {
            contact.type = 'whatsapp-tel';
          } else {
            contact.type = 'skype';
          }
          if (!$scope.ngModel) {
            $scope.ngModel = [];
          }
          $scope.ngModel.push(contact);
          return $scope.newString = '';
        };
        $scope.edit = function(index) {
          return contactEditModal.open($scope.ngModel[index]).then(function(data) {
            var k, results, v;
            results = [];
            for (k in data) {
              v = data[k];
              results.push($scope.ngModel[index][k] = v);
            }
            return results;
          });
        };
        return $scope.remove = function(index) {
          return $scope.ngModel.splice(index, 1);
        };
      }],
      template: ('/client/cms_editable_app/_directives/uiceContactsEditor/uiceContactsEditor.html', '<div class="input-group"><input class="form-control" type="text" ng-model="newString" uic-enter-press="add()" placeholder="{{\'Type your contact (email, phone, skype, fax)\' | translate}}" ng-disabled="ngDisabled"/><div class="input-group-btn"><button class="btn btn-success" ng-disabled="!newString || ngDisabled" ng-click="add()"><span class="fas fa-plus fa-nopadding-xs"></span><span class="hidden-xxs" translate="">Add contact</span></button></div></div><table><tbody><tr ng-repeat="item in ngModel"><td ng-click="edit($index)"><uic-contact ng-model="item"></uic-contact></td><td><div class="btn-group"><button class="btn btn-xs btn-default hidden-xs" ng-click="edit($index)"><span class="fas fa-pencil-alt"></span><span translate="">Edit</span></button><button class="btn btn-xs btn-danger" ng-click="remove($index)"><span class="fas fa-trash-alt"></span><span translate="">Remove</span></button></div></td></tr></tbody></table>' + '')
    };
  }]).directive('uiceContactItemEditor', function() {
    return {
      restrict: 'E',
      scope: {
        ngModel: '='
      },
      controller: ["$scope", function($scope) {
        var TYPES;
        TYPES = {
          email: 'Email',
          tel: 'Phone',
          skype: 'Skype',
          fax: 'Fax'
        };
        $scope.lang = CMS_DEFAULT_LANG;
        $scope.getTypeTitle = function(v) {
          return TYPES[v];
        };
        return $scope.setType = function(t) {
          return $scope.ngModel.type = t;
        };
      }],
      template: ('/client/cms_editable_app/_directives/uiceContactsEditor/uiceContactItemEditor.html', '<div class="form-group"><label translate="translate">Contact type</label><select class="form-control" ng-model="ngModel.type"><option value="email" translate="">Email</option><option value="tel" translate="">Phone</option><option value="whatsapp" translate="">WhatsUp/Viber</option><option value="whatsapp-tel" translate="">Phone + WhatsUp/Viber</option><option value="skype" translate="">Skype</option><option value="fax" translate="">Fax</option></select></div><div class="form-group"><label translate="translate">Contact data</label><input class="form-control" ng-model="ngModel.value" type="{{ngModel.type}}"/></div><div class="form-group"><label translate="">Description</label><div class="input-group"><input class="form-control" ng-model="ngModel.description[lang]"/><div class="input-group-btn"><uic-lang-picker ng-model="lang" display="dropdown"></uic-lang-picker></div></div></div>' + '')
    };
  });

}).call(this);
;
(function() {
  angular.module('ui.cms.editable').directive('uiceDbModelList', ["$constants", function($constants) {
    var TEMPLATE;
    TEMPLATE = "<table class=\"table table-condensed\">\n  <tbody>\n    <tr class=\"noborder-top\">\n      <td class=\"nopadding-top nopadding-lr noborder-top\" colspan=\"3\">\n        <div class=\"form-group nomargin\">\n          <div class=\"input-group-hack\">[[MODEL_SELECT_DIRECTIVE]]\n            <div class=\"input-group-btn\">\n              <button class=\"btn btn-success\" ng-click=\"_onCreateClick()\" ng-disabled=\"ngDisabledCreate || ngDisabled\" title=\"{{'Create' | translate}}\"><span class=\"fas fa-plus\"></span><span translate=\"\">Create</span></button>\n            </div>\n          </div>\n        </div>\n      </td>\n    </tr>\n    <tr ng-if=\"ngModel.length==0 || !ngModel\">\n      <td colspan=\"3\" uic-transclude=\"modelEmpty\" uic-transclude-bind=\"{$origScope: $origScope}\">\n        <div class=\"text-center\" style=\"padding: 1em 0;\"><i translate=\"\">Please select an object from the dropdown list in order to add it to the list</i></div>\n      </td>\n    </tr>\n    <tr ng-repeat=\"$itemId in ngModel track by $index\" ng-show=\"itemsCache[$itemId]\">\n      <td class=\"text-left nopadding-left\">[[TEMPLATE_FOR_ITEM]]</td>\n      <td class=\"text-right td-special-actions\" uic-transclude=\"specialActions\" uic-transclude-bind=\"{$origScope: $origScope, $itemId: $itemId, $item: itemsCache[$itemId]}\"></td>\n      <td class=\"text-right td-default-actions nopadding-right\" ng-switch=\"disableMoveActions == 'true'\">\n        <div class=\"btn-group\" ng-switch-when=\"false\">\n          <button class=\"btn btn-default btn-xs\" ng-click=\"move($index, -1)\" ng-disabled=\"$first\">\n            <div class=\"fas fa-arrow-up nopadding\"></div>\n          </button>\n          <button class=\"btn btn-default btn-xs\" ng-click=\"move($index, 1)\" ng-disabled=\"$last\">\n            <div class=\"fas fa-arrow-down nopadding\"></div>\n          </button>\n          <button class=\"btn btn-danger btn-xs\" ng-click=\"remove($index)\" ng-disabled=\"ngDisabled\" title=\"{{'Remove from list' | translate}}\"><span class=\"fas fa-trash-alt\"></span><span translate=\"translate\">Remove</span></button>\n        </div>\n        <button class=\"btn btn-danger btn-xs\" ng-switch-when=\"true\" ng-click=\"remove($index)\" ng-disabled=\"ngDisabled\" title=\"{{'Remove from list' | translate}}\"><span class=\"fas fa-times\"></span><span translate=\"translate\">Remove</span></button>\n      </td>\n    </tr>\n  </tbody>\n</table>";
    return {

      /**
      *   @ngdoc directive
      *   @name ui.cms.editable.directive:uiceDbModelList
      *   @description директива, которая позволяет выбирать id объектов моделей из бд в массив. При этом можно менять позиции id в массиве, создавать новые элементы из бд (отрисовывается кнопка +).
      *       <b>Директива не позволяет держать дубликаты id в одном массиве ngModel</b>
      *   @restrict E
      *   @param {object} ngModel (ссылка) массив id
      *   @param {string} modelName (значение) название модели из БД
      *   @param {Array=} items (ссылка) массив данных из бд (если нет ни одного элемента, то будет отображена плашка. если хотите переопределить верстку этой плашки внутри директивы добавьте тег model-empty и расположите верстку там)
      *   @param {boolean=} [disableMoveActions=false] (значение)
      *   @param {string=} uiSrefCreate (значение) ui-sref название вьюхи в приложении через которую можно создать новый объект в бд. По умолчанию будет: "app.modelEditors.#{$attrs.modelName}.add"
      *   @param {string=} uiSrefEdit (значение) ui-sref название вьюхи в приложении через которую можно редактировать объект в бд. По умолчанию будет пустое значение.
      *   @param {function=} onCreateClick (ссылка) функция, которая будет вызвана при нажатии на кнопку создания нового объекта. Тогда ссылка из uiSrefCreate будет проигнорирована
      *   @param {boolean=} ngDisabled (ссылка) флаг доступности тега
      *   @example
      *       <pre>
      *           # coffee
      *           $scope.myIdsArray = [4,3,1]
      *           $scope.items = [
      *               {id: 1, title: "Test 1"},
      *               {id: 2, title: "Test 2"},
      *               {id: 3, title: "Test 3"},
      *               {id: 4, title: "Test 4"}
      *           ]
      *       </pre>
      *       <pre>
      *           //- pug
      *           //- В документации нельзя создать новые объекты
      *           p Выбранные элементы: {{myIdsArray | json}}
      *           uice-db-model-list(ng-model="myIdsArray", items="items", model-name='CmsArticle')
      *       </pre>
      *       <example module="DocExampleApp">
      *           <file name="script.js">
      *               mkDocCtrl('uiceDbModelListCtrl', {
      *                   myIdsArray: [4,3,1],
      *                   items:[
      *                       {id: 1, title: "Test 1"},
      *                       {id: 2, title: "Test 2"},
      *                       {id: 3, title: "Test 3"},
      *                       {id: 4, title: "Test 4"}
      *                   ]
      *               }, function($scope, $injector){
      *                   var $constants = $injector.get('$constants');
      *                   $constants.update('$REPRESENTATION', {CmsArticle: {html: "{{$item.title}}"}});
      *               });
      *           </file>
      *           <file name="index.html">
      *               <div ng-controller="uiceDbModelListCtrl">
      *                   <p>Выбранные элементы: <b>{{myIdsArray | json}}</b></p>
      *                   <uice-db-model-list ng-model="myIdsArray" items="items" model-name='CmsArticle' ng-if="items.length">
      *                   </uice-db-model-list>
      *                   <hr/>
      *               </div>
      *           </file>
      *       </example>
       */
      restrict: 'E',
      scope: {
        ngModel: '=',
        items: '=?',
        disableMoveActions: '@?',
        allowDuplicates: '@?',
        onCreateClick: '&?',
        onEditClick: '&?',
        ngDisabled: '=?',
        ngDisabledCreate: '=?',
        ngDisabledEdit: '=?'
      },
      transclude: {
        modelEmpty: '?modelEmpty',
        specialActions: '?specialActions'
      },
      controller: ["$scope", "$element", "$attrs", "$state", "$stateParams", "H", function($scope, $element, $attrs, $state, $stateParams, H) {
        var ids;
        ids = [];
        $scope.newId = null;
        $scope.$origScope = H.findOrigScope($scope, $attrs);
        $scope.itemsCache = {};
        $scope.remove = function(index) {
          $scope.ngModel.remove(index);
        };
        $scope.move = function(index, diff) {
          $scope.ngModel.move(index, index + diff);
          if ($scope.allowDuplicates === 'true') {
            $scope.ngModel = angular.copy($scope.ngModel);
          }
        };
        $scope._onCreateClick = function() {
          if ($scope.onDisabledCreate || $scope.ngDisabled) {
            return;
          }
          if ($scope.onCreateClick) {
            $scope.onCreateClick();
            return;
          }
          $state.go($scope.uiSrefCreate || ("app.modelEditors." + $attrs.modelName + ".add"), {
            returnTo: $state.current.name,
            returnToParams: angular.copy($stateParams)
          });
        };
        $scope._onEditClick = function(item) {
          if ($scope.onDisabledEdit || $scope.ngDisabled) {
            return;
          }
          if ($scope.onEditClick) {
            $scope.onEditClick({
              item: item
            });
            return;
          }
          $state.go($scope.uiSrefEdit, {
            id: item.id,
            returnTo: $state.current.name,
            returnToParams: angular.copy($stateParams)
          });
        };
        $scope.$watchCollection('items', function(items, oldValue) {
          var i, item, len, ref;
          ids = [];
          ref = items || [];
          for (i = 0, len = ref.length; i < len; i++) {
            item = ref[i];
            ids.push(item.id);
            $scope.itemsCache[item.id] = item;
          }
        });
        return $scope.$watch('newId', function(newId, oldValue) {
          if (newId === null || !ids.includes(newId)) {
            return;
          }
          if (!angular.isArray($scope.ngModel)) {
            $scope.ngModel = [];
          }
          if ($scope.ngModel.includes(newId) && $scope.allowDuplicates !== 'true') {
            $scope.newId = null;
            return;
          }
          $scope.ngModel.push(newId);
          $scope.newId = null;
        });
      }],
      template: function($element, $attrs) {
        var directive, r, templateForItem;
        r = $constants.resolve("$REPRESENTATION." + $attrs.modelName) || {};
        templateForItem = $attrs.templateForItem || r.html || '{{$item.id}}';
        directive = "<uic-db-model\n    ng-model='newId'\n    placeholder='{{\"" + (r.placeholder || '') + "\" | translate}}'\n    template-for-item=\"" + templateForItem + "\"\n    select=\"items\">\n</uic-db-model>";
        templateForItem = templateForItem.replaceAll("$item", "itemsCache[$itemId]");
        if ($attrs.uiSrefEdit) {
          templateForItem = "<a ui-sref=\"" + $attrs.uiSrefEdit + "({id: $itemId})\" ng-disabled=\"ngDisabledEdit || ngDisabled\" target='_blank' title=\"{{'Edit' | translate}}\">\n    <span class='fas fa-pencil-alt'></span>\n    " + templateForItem + "\n</a>";
        }
        if ($attrs.onEditClick) {
          templateForItem = "<ul class='list-table nopadding nomargin vertical-align-middle w-auto link' ng-click=\"_onEditClick(itemsCache[$itemId]); $event.preventDefault()\" ng-disabled=\"ngDisabledEdit || ngDisabled\" role='button' title=\"{{'Edit' | translate}}\">\n    <li>\n        <span class='fas fa-pencil-alt'></span>\n    </li>\n    <li>" + templateForItem + "</li>\n</span>";
        }
        return TEMPLATE.replaceAll('[[MODEL_SELECT_DIRECTIVE]]', directive).replaceAll('[[TEMPLATE_FOR_ITEM]]', templateForItem);
      }
    };
  }]);

}).call(this);
;
(function() {
  angular.module('ui.cms.editable').directive('uiceDurationpicker', function() {
    return {

      /**
      *   @ngdoc directive
      *   @name ui.cms.editable.directive:uiceDurationpicker
      *   @description
      *
      *   @restrict E
      *   @param {string} ngModel (ссылка) модель в формате: '[DD] [HH:[MM:]]ss[.uuuuuu]'  Подробнее см: https://docs.djangoproject.com/en/2.0/_modules/django/db/models/fields/#DurationField
      *   @param {string=} [format='d,h,m'] (значение) тип отображения, где d - дни, h - часы, m - минуты, s -секунды
      *   @param {number=} [minuteStep=5] (значение) шаг минут (1...30)
      *   @param {number=} [secondStep=1] (значение) шаг секунд (1...30)
      *   @param {string=} inputClass (значение) дополнительный класс для select-ов в верстке
      *   @param {boolean=} ngDisabled (ссылка) флаг доступности тега
      *   @example
      *       <pre>
      *           # coffee
      *           $scope.duration = '5 10:35:10'
      *       </pre>
      *       <pre>
      *           //- pug
      *           p Duration = {{duration | json}}
      *           uice-durationpicker(ng-model="duration")
      *       </pre>
      *       <example module="DocExampleApp">
      *           <file name="script.js">
      *               mkDocCtrl('uiceDurationpickerCtrl', {
      *                   duration: '5 10:35:10'
      *               });
      *           </file>
      *           <file name="index.html">
      *               <div ng-controller="uiceDurationpickerCtrl">
      *                   <p>Duration = {{duration | json}}</p>
      *                   <uice-durationpicker ng-model="duration"></uice-durationpicker>
      *               </div>
      *           </file>
      *       </example>
       */
      restrict: 'E',
      require: 'ngModel',
      scope: {
        minuteStep: '@?',
        secondStep: '@?',
        format: '@?',
        inputClass: '@?',
        ngDisabled: '=?'
      },
      controller: ["$scope", "$element", "$attrs", "H", function($scope, $element, $attrs, H) {
        var format, i, minuteStep, ngModelCtrl, required, secondStep;
        ngModelCtrl = $element.controller('ngModel');
        if ($attrs.hasOwnProperty('required')) {
          required = true;
        }
        minuteStep = H.convert.toInt($scope.minuteStep, 0);
        secondStep = H.convert.toInt($scope.secondStep, 0);
        if (!minuteStep) {
          minuteStep = 5;
        }
        if (minuteStep > 30) {
          minuteStep = 5;
        }
        if (!secondStep) {
          secondStep = 5;
        }
        if (secondStep > 30) {
          secondStep = 5;
        }
        $scope.DAYS_LIST = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14];
        $scope.HOURS_LIST = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23];
        $scope.MINUTES_LIST = (function() {
          var j, ref, results;
          results = [];
          for (i = j = 0, ref = minuteStep; j <= 59; i = j += ref) {
            results.push(i);
          }
          return results;
        })();
        $scope.SECONDS_LIST = (function() {
          var j, ref, results;
          results = [];
          for (i = j = 0, ref = secondStep; j <= 59; i = j += ref) {
            results.push(i);
          }
          return results;
        })();
        $scope.proxy = {
          days: 0,
          hours: 0,
          minutes: 0,
          seconds: 0
        };
        format = ($scope.format || "d,h,m").split(',');
        $scope.display = {
          days: false,
          hours: false,
          minutes: false,
          seconds: false
        };
        if (format.includes('s')) {
          $scope.display.seconds = true;
        }
        if (format.includes('m')) {
          $scope.display.minutes = true;
        }
        if (format.includes('h')) {
          $scope.display.hours = true;
        }
        if (format.includes('d')) {
          $scope.display.days = true;
        }
        ngModelCtrl.$render = function() {
          $scope.proxy = H.duration.parse(ngModelCtrl.$modelValue);
        };
        return $scope.$watchCollection('proxy', function(proxy, oldValue) {
          if (required && !proxy.milliseconds && !proxy.seconds && !proxy.minutes && !proxy.hours && !proxy.days) {
            ngModelCtrl.$setViewValue(null);
            return;
          }
          ngModelCtrl.$setViewValue(H.duration.dump(proxy));
        });
      }],
      template: ('/client/cms_editable_app/_directives/uiceDurationpicker/uiceDurationpicker.html', '<ul class="list-table nomargin-bottom xxs-block"><li ng-if="display.days"><div class="input-group"><div class="input-group-btn"><select class="form-control w-5em {{:: inputClass}}" ng-model="proxy.days" ng-options="d for d in DAYS_LIST"></select></div><div class="input-group-addon" translate="translate" translate-n="proxy.days" translate-plural="days" translate-context="medium duration text">day</div></div></li><li ng-if="display.hours"><div class="input-group"><div class="input-group-btn"><select class="form-control w-5em {{:: inputClass}}" ng-model="proxy.hours" ng-options="h for h in HOURS_LIST"></select></div><div class="input-group-addon" translate="translate" translate-n="proxy.hours" translate-plural="hours" translate-context="medium duration text">hour</div></div></li><li ng-if="display.minutes"><div class="input-group"><div class="input-group-btn"><select class="form-control w-5em {{:: inputClass}}" ng-model="proxy.minutes" ng-options="m for m in MINUTES_LIST"></select></div><div class="input-group-addon" translate="translate" translate-n="proxy.minutes" translate-plural="minutes" translate-context="medium duration text">minute</div></div></li><li ng-if="display.seconds"><div class="input-group"><div class="input-group-btn"><select class="form-control w-5em {{:: inputClass}}" ng-model="proxy.seconds" ng-options="m for m in SECONDS_LIST"></select></div><div class="input-group-addon" translate="translate" translate-n="proxy.seconds" translate-plural="seconds" translate-context="medium duration text">second</div></div></li></ul>' + '')
    };
  });

}).call(this);
;
(function() {
  angular.module('ui.cms.editable').directive('uiceFaIconPicker', function() {
    return {

      /**
      *   @ngdoc directive
      *   @name ui.cms.editable.directive:uiceFaIconPicker
      *   @description директива для выбора иконок из сета FontAwesome
      *
      *   @restrict E
      *   @param {object} ngModel (ссылка) модель
      *   @param {string=} [type='html'] (значение) тип ngModel: html - в виде тега, class - в виде css класса
      *   @param {boolean=} ngDisabled (ссылка) флаг доступности тега
      *   @example
      *       <pre>
      *           $scope.fa = "fas fa-plus"
      *           $scope.faHtml = "<span class='fas fa-plus'></span>"
      *       </pre>
      *       <pre>
      *           //- pug
      *           uice-fa-icon-picker(ng-model="fa", type='class')
      *           uice-fa-icon-picker(ng-model="faHtml")
      *       </pre>
      *       <example module="DocExampleApp">
      *           <file name="script.js">
      *               mkDocCtrl('uiceFaIconPickerrCtrl', {
      *                   fa: 'fas fa-plus',
      *                   faHtml: "<span class='fas fa-plus'></span>"
      *               });
      *           </file>
      *           <file name="index.html">
      *               <div ng-controller="uiceFaIconPickerrCtrl">
      *                   <p>Icon class = {{fa | json}}, icon html = {{faHtml | json}}</p>
      *                   <div class='row'>
      *                       <div class='col-md-50'>
      *                          <uice-fa-icon-picker ng-model="fa" type='class'></uice-fa-icon-picker>
      *                       </div>
      *                       <div class='col-md-50'>
      *                           <uice-fa-icon-picker ng-model="faHtml"></uice-fa-icon-picker>
      *                       </div>
      *                   </div>
      *               </div>
      *           </file>
      *       </example>
       */
      restrict: 'E',
      scope: {
        ngModel: '=',
        type: '@?',
        ngDisabled: '=?'
      },
      controller: ["$scope", "uiceFaIconPickerModal", function($scope, uiceFaIconPickerModal) {
        var dump, parse;
        if (!$scope.type) {
          $scope.type = 'html';
        }
        parse = function(data) {
          var i;
          if (!data) {
            return '';
          }
          if ($scope.type === 'html') {
            data = data.split('class="')[1].split('"')[0];
            i = data.indexOf('fa ');
            if (i > -1) {
              return data.substr(i);
            }
            return data;
          }
          return data;
        };
        dump = function(icon) {
          if ($scope.type === 'html') {
            return "<span class=\"" + icon + "\"></span>";
          }
          return icon;
        };
        $scope.change = function() {
          uiceFaIconPickerModal.open(parse($scope.ngModel)).then(function(icon) {
            $scope.ngModel = dump(icon);
          });
        };
        return $scope.remove = function() {
          return $scope.ngModel = '';
        };
      }],
      template: ('/client/cms_editable_app/_directives/uiceFaIconPicker/uiceFaIconPicker.html', '<div class="btn-group"><div class="btn btn-default btn-sm" ng-click="change()" ng-disabled="ngDisabled"><span ng-if="type==\'html\'" uic-bind-html="ngModel"></span><span class="{{ngModel}}" ng-if="type==\'class\'"></span><span translate="" ng-if="ngModel">Change icon</span><span translate="" ng-if="!ngModel">Set icon</span></div><div class="btn btn-danger btn-sm" ng-click="remove()" ng-disabled="!ngModel || ngDisabled"><div class="fas fa-trash-alt"></div><span translate="">Delete icon</span></div></div>' + '')
    };
  });

}).call(this);
;
(function() {
  angular.module('ui.cms.editable').directive('uiceFileDropzone', ["$parse", function($parse) {
    return {

      /**
      *   @ngdoc directive
      *   @name ui.cms.editable.directive:uiceFileDropzone
      *   @description
      *       виджет для поддержки зоны дропдауна файла. Виджет ничего не загружает на сервер,
      *       позволяет только перехватывать файлы, которые пользователь бросает в область виджета.
      *   @restrict AE
      *   @param {function} onUpload (функция) функция, которая будет вызвана при подтверждении пользователем загрузки из виджета
      *   @param {number=} maxCount (значение) сколько всего можно бросать файлов в область. Если пользователь бросил больше файлов чем указано в параметре, то их количество обрезается до нужного
      *   @example
      *       <pre>
      *           $scope.myCb = (files)->
      *               console.log 'Files to upload', files
      *       </pre>
      *       <pre>
      *           //- pug
      *           div(uice-file-dropzone='', on-upload="myCb(files)")
      *               dropzone-content
      *                  // код внутри зоны для бросания файлов
      *               dropzone-alert(ng-if="myerror")
      *                  // код из классов .alert для разных уведомлений пользователю
      *                  // обязательно использовать ng-if для этого трансклюда
      *       </pre>
      *       <example module="DocExampleApp">
      *           <file name="script.js">
      *               mkDocCtrl('uiceFileDropzoneCtrl', {
      *
      *               });
      *           </file>
      *           <file name="index.html">
      *               <div ng-controller="uiceFileDropzoneCtrl">
      *
      *               </div>
      *           </file>
      *       </example>
       */
      restrict: 'AE',
      transclude: {
        dropzoneContent: 'dropzoneContent',
        dropzoneAlert: '?dropzoneAlert'
      },
      controller: ["$scope", "$element", "$attrs", function($scope, $element, $attrs) {
        var _onUpload, multiple;
        if ($attrs.hasOwnProperty('uiceFileDropzone') && $attrs.uiceFileDropzone === 'false') {
          return;
        }
        multiple = $attrs.multiple === 'true';
        _onUpload = $scope.$eval($attrs.onUpload.split('(')[0]);
        if (!angular.isFunction(_onUpload)) {
          throw "uiceFileDropzone: provide function for 'on-upload' attribute!";
        }
        $scope.$$dropzone = $scope.$$dropzone || {};
        $scope.getDroppedFileNames = function() {
          return ($scope.$$dropzone.files || []).map(function(f) {
            return f.name;
          }).join(', ');
        };
        $scope.$$dropzone.onUpload = function() {
          if (multiple) {
            _onUpload($scope.$$dropzone.files);
          } else {
            _onUpload($scope.$$dropzone.files[0]);
          }
          $scope.$$dropzone.reset();
        };
        $scope.$$dropzone.reset = function() {
          $scope.$$dropzone.active = false;
          $scope.$$dropzone.dropped = false;
          return $scope.$$dropzone.file = null;
        };
        $element.on('dragleave', function(event) {
          if (event.target !== $element[0]) {
            return;
          }
          if ($scope.$$dropzone.active) {
            $scope.$apply(function() {
              $scope.$$dropzone.active = false;
              $scope.$$dropzone.dropped = false;
            });
          }
        });
        $element.on('dragover', function(event) {
          event.preventDefault();
          if (!$scope.$$dropzone.active) {
            $scope.$apply(function() {
              $scope.$$dropzone.active = true;
            });
          }
        });
        return $element.on('drop', function(event) {
          event.preventDefault();
          return $scope.$apply(function() {
            if (event.dataTransfer.files && event.dataTransfer.files.length) {
              $scope.$$dropzone.active = false;
              $scope.$$dropzone.dropped = true;
              $scope.$$dropzone.files = event.dataTransfer.files;
              if (!multiple) {
                $scope.$$dropzone.files = [event.dataTransfer.files[0]];
              }
            } else {
              $scope.$$dropzone.reset();
            }
          });
        });
      }],
      template: ('/client/cms_editable_app/_directives/uiceFileDropzone/uiceFileDropzone.html', '<div class="clearfix"></div><div class="uice-dropzone-content" ng-class="{\'invisible noevents minheight\': $$dropzone.active || $$dropzone.dropped}" uic-transclude="dropzoneContent"></div><div uic-transclude="dropzoneAlert" ng-class="{noevents: $$dropzone.active}"></div><div class="uice-dropzone-alert" ng-class="{visible: $$dropzone.active || $$dropzone.dropped, noevents: $$dropzone.active}"><div class="alert alert-info alert-info-helper noevents" ng-show="$$dropzone.active"><div class="alert-content"><div class="alert alert-info"><b class="alert-content"><div class="fas fa-file"></div><span translate="">Drop file here</span></b></div></div></div><div class="alert alert-info" ng-show="$$dropzone.dropped &amp;&amp; !uploading.error"><div class="alert-content"><span translate="" translate-n="$$dropzone.files.length" translate-plural="Upload files">Upload file</span><b class="uice-dropzone-file-names"> {{getDroppedFileNames()}}</b>?<div class="text-center"><div class="btn-group uice-dropzone-btn-group"><button class="btn btn-info btn-xs" ng-click="$$dropzone.onUpload()"><div class="fas fa-cloud-upload-alt"></div><span translate="">Upload</span></button><button class="btn btn-default btn-xs" translate="" ng-click="$$dropzone.reset()">Cancel</button></div></div></div></div></div><div class="clearfix"></div>' + '')
    };
  }]);

}).call(this);
;
(function() {
  angular.module('ui.cms.editable').directive('uiceFileIdPicker', ["$injector", "$q", function($injector, $q) {
    var uploadFileToList;
    uploadFileToList = function(cmsModelItem, file, options) {
      var ModelRerource, data, defer, lastIndex, params, response;
      lastIndex = (cmsModelItem.files || []).length;
      ModelRerource = $injector.get(cmsModelItem.__proto__.constructor.modelName);
      params = {
        id: cmsModelItem.id,
        forModelField: options.forModelField || 'files'
      };
      data = {
        file: file,
        options_thumbSizes: options.thumbSizes,
        $asFormData: true
      };
      if (data.options_thumbSizes.indexOf('440x250') === -1) {
        data.options_thumbSizes.push('440x250');
      }
      defer = $q.defer();
      response = ModelRerource.uploadFileToList(params, data);
      response.$promise.then(function(l10nFiles) {
        defer.resolve(l10nFiles[lastIndex]);
      }, defer.reject);
      return {
        $promise: defer.promise,
        $cancelRequest: response.$cancelRequest
      };
    };
    return {

      /**
      *   @ngdoc directive
      *   @name ui.cms.editable.directive:uiceFileIdPicker
      *   @description
      *
      *   @restrict E
      *   @param {string} ngModel (ссылка) id файла из item.files списка файлов
      *   @param {object} cmsModelItem (ссылка) потомок Resource-фабрики Cms* модели
      *   @param {string=} [accept='*'] (значение) список mime-типов файлов(через запятую. поддерживаются маски)
      *   @param {boolean=} [allowPreview=false] (значение) отображать ли превью файла в виджите
      *   @param {string=} inputClass css-класс для внутренней верстки в ['input-sm', 'input-lg'].
      *   @param {boolean=} ngDisabled (ссылка) флаг доступности тега
      *   @example
      *       <pre>
      *           $scope.myId = null
      *           CmsArticle.findOne (article)->
      *               $scope.article = article
      *       </pre>
      *       <pre>
      *           //- pug
      *           uice-file-id-picker(ng-model="myId", cms-model-item="article", accept='image/*,video/*', input-size='sm')
      *       </pre>
      *       <pre>
      *       </pre>
       */
      restrict: 'E',
      transclude: true,
      scope: {
        ngModel: '=',
        cmsModelItem: '=',
        accept: '@?',
        acceptThumbSizes: '@?',
        allowPreview: '@?',
        allowDnd: '@?',
        ngDisabled: '=?'
      },
      controller: ["$scope", "$element", "H", "uiceL10nFileEditorModal", function($scope, $element, H, uiceL10nFileEditorModal) {
        var acceptThumbSizes, fixNgModel, getFiles;
        $scope.inputSize = ($scope.inputClass || '').split('-')[1];
        $scope.files = [];
        acceptThumbSizes = ($scope.acceptThumbSizes || '').replaceAll('"', '').replaceAll("'", '');
        if (!acceptThumbSizes.length) {
          acceptThumbSizes = [];
        } else {
          acceptThumbSizes = acceptThumbSizes.split(',');
        }
        if ($scope.allowDnd === 'false') {
          $scope._allowDnd = false;
        } else {
          $scope._allowDnd = true;
        }
        getFiles = function(files) {
          var _type, file, filteredFiles, j, len, ref;
          filteredFiles = [
            {
              id: void 0,
              meta: {
                originalName: '---'
              }
            }
          ];
          ref = files || [];
          for (j = 0, len = ref.length; j < len; j++) {
            file = ref[j];
            _type = H.l10nFile.getFileType(file);
            if (!H.l10nFile.isValidFileType(_type, $scope.accept)) {
              continue;
            }
            if (!acceptThumbSizes.length) {
              filteredFiles.push(file);
            } else if (H.l10nFile.isFileHasThumbs(file, acceptThumbSizes)) {
              filteredFiles.push(file);
            }
          }
          return filteredFiles;
        };
        fixNgModel = function(ngModel) {
          var num;
          num = parseInt(ngModel);
          if (angular.isString(ngModel) && angular.isNumber(num) && !isNaN(num)) {
            return num;
          }
          return ngModel;
        };
        $scope.getFileTitle = function(file) {
          return H.l10nFile.getOriginalName(file);
        };
        $scope.reset = function() {
          $scope.uploading = {
            error: '',
            file: null,
            loading: false
          };
        };
        $scope.cancelRequest = function() {
          $scope.uploading.cancelRequest();
        };
        $scope.uploadFile = function(file) {
          var accept, maxsize, res;
          maxsize = H.l10nFile.humanFileSizeToBytes($scope.maxSize || '15 MB');
          accept = $scope.accept || '*';
          if (file.size > maxsize) {
            $scope.uploading.error = 'maxsize';
            return;
          }
          if (!H.l10nFile.isValidFileType(file.type, accept)) {
            $scope.uploading.error = 'accept-type';
            return;
          }
          res = uploadFileToList($scope.cmsModelItem, file, {
            thumbSizes: acceptThumbSizes
          });
          $scope.uploading.loading = true;
          $scope.uploading.cancelRequest = res.$cancelRequest;
          res.$promise.then(function(data) {
            $scope.ngModel = data.id;
            $scope.cmsModelItem.files = $scope.cmsModelItem.files || [];
            $scope.cmsModelItem.files.push(data);
            return $scope.reset();
          }, function(data) {
            return $scope.reset();
          });
        };
        $scope.editFile = function() {
          var i, index, j, l10nFile, len, ref;
          index = -1;
          ref = $scope.cmsModelItem.files;
          for (i = j = 0, len = ref.length; j < len; i = ++j) {
            l10nFile = ref[i];
            if (!(l10nFile.id === $scope.previewFile.id)) {
              continue;
            }
            index = i;
            break;
          }
          uiceL10nFileEditorModal.edit($scope.cmsModelItem, {
            forModelField: 'files',
            allowEditTitleDescription: true,
            accept: $scope.accept || '*',
            maxSize: $scope.maxSize || '15 MB',
            index: index
          });
        };
        $scope.$watchCollection('cmsModelItem.files', function(files) {
          $scope.files = getFiles(files);
          return $scope.previewFile = ($scope.files || []).findByProperty('id', fixNgModel($scope.ngModel));
        }, true);
        $scope.$watch('uploading.file', function(file, oldValue) {
          if (file && file !== oldValue) {
            $scope.uploadFile(file);
          }
        });
        $scope.$watch('ngModel', function(ngModel) {
          $scope.previewFile = null;
          if ($scope.ngModel) {
            $scope.ngModel = fixNgModel(ngModel);
            if ($scope.allowPreview === 'true') {
              $scope.previewFile = ($scope.files || []).findByProperty('id', $scope.ngModel);
            }
          }
        });
        return $scope.reset();
      }],
      template: ('/client/cms_editable_app/_directives/uiceFileIdPicker/uiceFileIdPicker.html', '<div uice-file-dropzone="{{_allowDnd}}" on-upload="uploadFile(file)" max-count="1"><dropzone-content ng-hide="!_allowDnd &amp;&amp; uploading.error"><div class="thumbnail preview" ng-if="allowPreview==\'true\' &amp;&amp; previewFile.id"><uic-file-thumb file="previewFile" size="440x250"></uic-file-thumb></div><div class="input-group"><select class="form-control input-{{:: inputSize}}" ng-model="ngModel" ng-options="file.id as getFileTitle(file)   for file in files" ng-disabled="ngDisabled || uploading.loading"></select><div class="input-group-btn" ng-switch="uploading.loading"><div ng-switch-when="false"><button class="btn btn-default" ng-click="editFile()" title="{{\'Edit\' | translate}}" ng-if="previewFile.id"><span class="fas fa-pencil-alt nopadding"></span></button><uic-input-file class="btn btn-success btn-{{:: inputSize}}" ng-model="uploading.file" accept="{{accept || \'*\'}}" max-size="{{maxSize || \'15 MB\'}}" ng-disabled="ngDisabled"><span class="fas fa-plus"></span><span translate="">Upload file</span></uic-input-file></div><button class="btn btn-danger btn-{{:: inputSize}}" ng-switch-when="true" ng-click="cancelRequest()"><span class="fas fa-times"></span><span translate="">Cancel</span></button></div></div></dropzone-content><dropzone-alert ng-if="uploading.error" ng-class="{\'position-initial\': !_allowDnd}"><div class="alert alert-danger"><div class="alert-content" ng-click="reset()"><div ng-switch="uploading.error"><b ng-switch-when="maxsize"><span translate="">File is too big. Max size is</span><span style="padding-left:3px;">{{maxSize || \'15 MB\'}}</span></b><b ng-switch-when="accept-type"><span translate="">Invalid file type. Allowed are:</span><b style="padding-left:4px;">{{accept || \'*\'}}</b></b></div><button class="btn btn-danger btn-xs"><span style="padding:0 3px;">Ok</span></button></div></div></dropzone-alert></div>' + '')
    };
  }]);

}).call(this);
;
(function() {
  angular.module('ui.cms.editable').controller('UiceFileManagerCtrl', ["$scope", "$injector", "H", "uiceL10nFileEditorModal", function($scope, $injector, H, uiceL10nFileEditorModal) {
    var getNgResource, getSaveFunction, isImage, ref, ref1, saveFn, setMainImgFromFiles;
    if ($scope.type === 'panel' && ((ref = $scope.manageMainImg) !== false && ref !== 'false')) {
      $scope.manageMainImg = true;
    }
    getSaveFunction = function(scope) {
      if (scope.id === 1 || !scope) {
        return null;
      }
      if (angular.isFunction(scope.save)) {
        return scope.save;
      }
      return getSaveFunction(scope.$parent);
    };
    isImage = function(l10nFile) {
      if (!l10nFile) {
        return false;
      }
      return H.l10nFile.getFileType(l10nFile).includes('image/');
    };
    getNgResource = function() {
      var c;
      if ($scope.ngModel) {
        c = $scope.ngModel.__proto__.constructor || {};
        if (!c.modelName) {
          throw "uiceFileManager: Strange ngModel __proto__.constructor value";
        }
        return $injector.get(c.modelName);
      }
      throw "uiceFileManager: Can't detect model resource! Please provide ngModel value";
    };
    setMainImgFromFiles = function(files, index) {
      var ModelResource, data, file, i, len, params;
      index = index || 0;
      files = files || [];
      params = {
        id: $scope.ngModel.id,
        forModelField: 'mainImg'
      };
      data = {
        $asFormData: true
      };
      if (isImage(files[index])) {
        data.linkTo = {
          field: $scope.field,
          id: files[index].id
        };
      } else {
        for (i = 0, len = files.length; i < len; i++) {
          file = files[i];
          if (!(isImage(file))) {
            continue;
          }
          data.linkTo = {
            field: $scope.field,
            id: file.id
          };
          break;
        }
      }
      if (data.linkTo) {
        ModelResource = getNgResource();
        ModelResource.uploadFile(params, data, function(data) {
          return $scope.ngModel.mainImg = data;
        });
      }
    };
    $scope.manageFile = function(index) {
      var k, options, ref1, v;
      options = {
        maxSize: '15 MB',
        accept: $scope.accept,
        forModelField: $scope.field,
        allowEditTitleDescription: true,
        index: index
      };
      ref1 = $scope.options || {};
      for (k in ref1) {
        v = ref1[k];
        if (k !== 'index') {
          options[k] = v;
        }
      }
      uiceL10nFileEditorModal.edit($scope.ngModel, options).then(function(l10nFiles) {
        $scope.ngModel[$scope.field] = l10nFiles;
      });
    };
    $scope.viewFile = function(index) {
      uiceL10nFileEditorModal.view($scope.ngModel, {
        forModelField: $scope.field,
        index: index
      });
    };
    $scope.getDownloadLink = function(index) {
      var file;
      file = $scope.ngModel[$scope.field][index];
      if (!file) {
        return;
      }
      return H.l10nFile.getFileUrl(file);
    };
    $scope.getOriginalFileName = function(index) {
      var file;
      file = $scope.ngModel[$scope.field][index];
      if (!file) {
        return;
      }
      return H.l10nFile.getOriginalName(file);
    };
    $scope.deleteFile = function(index) {
      var file, options;
      options = {
        index: index,
        forModelField: $scope.field
      };
      file = $scope.ngModel[$scope.field][index];
      return uiceL10nFileEditorModal["delete"]($scope.ngModel, options).then(function(l10nFiles) {
        var ref1;
        $scope.ngModel[$scope.field] = l10nFiles;
        if (((ref1 = $scope.manageMainImg) === 'true' || ref1 === true) && $scope.ngModel.mainImg) {
          if ($scope.ngModel.mainImg.id === file.id) {
            $scope.ngModel.mainImg = null;
            setMainImgFromFiles(l10nFiles);
          }
        }
      });
    };
    $scope.isImage = function(index) {
      return isImage($scope.ngModel[$scope.field][index]);
    };
    $scope.setAsMainImg = function(index) {
      return setMainImgFromFiles($scope.ngModel[$scope.field], index);
    };
    if ((ref1 = $scope.manageMainImg) === 'true' || ref1 === true) {
      $scope.$watch('ngModel.mainImg', function(mainImg, oldValue) {
        if ((mainImg || {}).id === (oldValue || {}).id) {
          return;
        }
        if (!mainImg && $scope.ngModel) {
          setMainImgFromFiles($scope.ngModel[$scope.field]);
        }
      });
    }
    saveFn = getSaveFunction($scope.$parent);
    if (saveFn) {
      $scope.saveNgModel = function() {
        return saveFn(void 0, true);
      };
    }
  }]).directive('uiceFileManager', function() {
    return {

      /**
      *   @ngdoc directive
      *   @name ui.cms.editable.directive:uiceFileManager
      *   @description виджет для загрузки/редактирования/удаления файлов из списка
      *   @restrict E
      *   @param {object} ngModel (ссылка) модель (например, потомок ресурса CmsArticle)
      *   @param {string} field (значение) название поля, внутри переданной модели, в котором хранятся L10nAnyFile объекты
      *   @param {string=} [accept='*'] (значение) mime-типы разрешенных файлов(через запятую)
      *   @param {boolean=} [manageMainImg=false] (значение) управлять ли также и главной картинкой из модели - поле в модели <b>mainImg</b> (если да, то если в файлах есть картинки, то у каждой в дропдауне редактирования появится пункт "Set as main image")
      *   @param {string=} [addFilesLabel="Upload"] (значение) текст, который отображается на кнопке загрузки файлов
      *   @param {string=} [filesLabel="Files"] (значение) текст, который является заголовком виджета
      *   @param {string=} type (значение) тип виджета. Если не указано, то стандартный. Если 'panel', то будет отрендерен как bootstrap-panel
      *   @example
      *       <pre>
      *           CmsArticle.finOne (item)->
      *               $scope.item = item
      *       </pre>
      *       <pre>
      *           //- pug
      *           p Стандартный вид
      *           uice-file-manager(ng-model="item", field='files', manage-main-img='true', files-label='Files for article')
      *           p Вид панели
      *           uice-file-manager(ng-model="item", field='files', manage-main-img='true', type='panel')
      *       </pre>
      *       <example module="DocExampleApp">
      *           <file name="script.js">
      *               mkDocCtrl('uiceFileManagerDocCtrl', {
      *                   item: {
      *                       id: 1
      *                   }
      *               });
      *           </file>
      *           <file name="index.html">
      *               <div ng-controller="uiceFileManagerDocCtrl" class='row'>
      *                   <div class='col-md-50'>
      *                       <p>Стандартный вид</p>
      *                       <uice-file-manager ng-model="item" field='files' manage-main-img='true' files-label='Files for article'></uice-file-manager>
      *                   </div>
      *                   <div class='col-md-50'>
      *                       <p>Вид панели</p>
      *                       <uice-file-manager ng-model="item" field='files' manage-main-img='true' type='panel'></uice-file-manager>
      *                   </div>
      *               </div>
      *           </file>
      *       </example>
       */
      restrict: 'E',
      scope: {
        ngModel: '=',
        field: '@',
        accept: '@?',
        manageMainImg: '@?',
        addFilesLabel: '@?',
        filesLabel: '@?',
        options: '=?',
        type: '@?'
      },
      controller: 'UiceFileManagerCtrl',
      template: ('/client/cms_editable_app/_directives/uiceFileManager/uiceFileManager.html', '<div ng-switch="type==\'panel\'"><div class="old-version" ng-switch-when="false" ng-switch="!!ngModel.id"><div ng-switch-when="true"><label style="margin-bottom:0.7em;"><span ng-switch="!!filesLabel"><span ng-switch-when="false" translate="">Files</span><span ng-switch-when="true">{{filesLabel}}</span></span><button class="btn btn-success" ng-click="manageFile(-1)" style="margin-left:1em;"><span class="fas fa-plus"></span><span ng-switch="!!addFilesLabel"><span ng-switch-when="false" translate="">Add files</span><span ng-switch-when="true">{{addFilesLabel}}</span></span></button></label><div class="row"><div class="col-lg-25 col-md-25 col-sm-50 col-xs-100" ng-repeat="l10nFile in ngModel[field]"><div class="thumbnail"><uic-file-thumb file="l10nFile" size="440x250" ng-click="manageFile($index)"></uic-file-thumb><div class="btn-group" uib-dropdown=""><button class="btn btn-default btn-xs" uib-dropdown-toggle=""><span translate="">Actions</span><span class="caret" style="margin-left:3px;"></span></button><ul class="dropdown-menu pull-right" uib-dropdown-menu=""><li ng-show="isImage($index) &amp;&amp; manageMainImg"><a ng-click="setAsMainImg($index)"><span translate="">Set as main image</span></a></li><li><a ng-click="manageFile($index)"><div class="fas fa-pencil-alt"></div><span translate="">Edit</span></a></li><li><a ng-href="{{getDownloadLink($index)}}" target="_blank" download="{{getOriginalFileName($index)}}"><div class="fas fa-cloud-download-alt"></div><span translate="">Download</span></a></li><li><a ng-click="viewFile($index)"><div class="fas fa-eye"></div><span translate="">View file</span></a></li><li><a ng-click="deleteFile($index)"><div class="fas fa-trash-alt"></div><span translate="">Delete</span></a></li></ul></div></div></div></div></div><div class="text-center no-ng-model" ng-switch-when="false"><div translate="">To upload linked files, you must first save the document</div><button class="btn btn-sm btn-primary" ng-click="saveNgModel()" ng-if="saveNgModel" style="margin:0.5em auto;"><div class="fas fa-save"></div><span translate="">Save</span></button></div></div><div class="panel panel-default panel-sm" ng-switch-when="true"><div class="panel-heading"><div class="panel-title" ng-switch="!!filesLabel"><span ng-switch-when="false" translate="">Files</span><span ng-switch-when="true">{{filesLabel}}</span></div><div class="pull-right" ng-if="ngModel.id &amp;&amp; ngModel[field].length"><button class="btn btn-success btn-xs" ng-click="manageFile(-1)" ng-disabled="ngDisabled"><div class="fas fa-cloud-upload-alt"></div><span ng-switch="!!addFilesLabel"><span ng-switch-when="false" translate="">Upload</span><span ng-switch-when="true">{{addFilesLabel}}</span></span></button></div></div><div class="panel-body nopadding-bottom" ng-switch="!!ngModel.id"><div class="row" ng-switch-when="true"><div class="col-xs-100 text-center" ng-if="!ngModel[field].length"><button class="btn btn-sm btn-success" ng-click="manageFile(-1)" style="margin-bottom:1em;margin-top:1em;" ng-disabled="ngDisabled"><div class="fas fa-cloud-upload-alt"></div><span translate="">Upload</span></button></div><div class="col-xs-50" ng-repeat="l10nFile in ngModel[field]"><div class="thumbnail"><uic-file-thumb class="nomargin thumbnail-content" file="l10nFile" size="440x250" ng-click="manageFile($index)"></uic-file-thumb><div class="thumbnail-actions"><div class="btn-group"><div class="btn-group dropup" uib-dropdown=""><button class="btn btn-default" uib-dropdown-toggle=""><span translate="">Actions</span><span class="dropup"><span class="caret" style="margin-left:3px;"></span></span></button><ul class="dropdown-menu" uib-dropdown-menu="" ng-class="{\'pull-right\': $index%2 == 1}"><li ng-show="isImage($index) &amp;&amp; manageMainImg"><a ng-click="setAsMainImg($index)"><span translate="">Set as main image</span></a></li><li><a ng-href="{{getDownloadLink($index)}}" target="_blank" download="{{getOriginalFileName($index)}}"><div class="fas fa-cloud-download-alt"></div><span translate="">Download</span></a></li><li><a ng-click="viewFile($index)"><div class="fas fa-eye"></div><span translate="">View file</span></a></li><li><a ng-click="manageFile($index)"><div class="fas fa-pencil-alt"></div><span translate="">Edit</span></a></li></ul></div><button class="btn btn-danger" ng-click="deleteFile($index)" title="{{\'Delete\' | translate}}"><div class="fas fa-trash-alt nopadding"></div></button></div></div></div></div></div><div class="text-center no-ng-model" ng-switch-when="false"><div translate="">To upload linked files, you must first save the document</div><button class="btn btn-sm btn-primary" ng-click="saveNgModel()" ng-if="saveNgModel" style="margin:0.5em auto;"><div class="fas fa-save"></div><span translate="">Save</span></button></div></div></div></div>' + '')
    };
  });

}).call(this);
;
(function() {
  angular.module('ui.cms.editable').config(["$directiveOverrideProvider", function($directiveOverrideProvider) {
    $directiveOverrideProvider.duplicate('uicHtmlEditor', 'uiceHtmlEditor');
  }]);

}).call(this);
;
(function() {
  angular.module('ui.cms.editable').directive('uiceItemIdListEditor', function() {
    return {

      /**
      *   @ngdoc directive
      *   @name ui.cms.editable.directive:uiceItemIdListEditor
      *   @description редактор массива id элементов
      *
      *   @restrict E
      *   @param {array<>} ngModel (ссылка) модель array. Напр.: [1, 2].
      *   @param {array<object>} items (ссылка) список объектов. Напр: [{id: 1}, {id: 2}]
      *   @param {boolean=} [disableMoveActions=false] (значение)
      *   @param {string=} addButtonText (значение) текст на кнопке добавления значения
      *   @param {function=} onChange (функция) функция которая вызывается при изменении модели ngModel
      *   @param {boolean=} ngDisabled (ссылка) флаг доступности тега
       */
      restrict: 'E',
      transclude: true,
      scope: {
        ngModel: '=',
        items: '=',
        options: '=?',
        disableMoveActions: '@?',
        addButtonText: '@',
        onChange: '&?',
        ngDisabled: '=?'
      },
      controller: ["$scope", "$filter", function($scope, $filter) {
        var l10n;
        $scope.newId = '';
        l10n = $filter('l10n');
        $scope.add = function() {
          var i;
          if (!($scope.ngModel instanceof Array)) {
            $scope.ngModel = [];
          }
          i = $scope.ngModel.indexOf($scope.newId);
          if (i > -1) {
            return;
          }
          $scope.ngModel.push($scope.newId);
          $scope.newId = '';
          if ($scope.onChange) {
            return $scope.onChange($scope.ngModel);
          }
        };
        $scope.remove = function(index) {
          $scope.ngModel.splice(index, 1);
          if ($scope.onChange) {
            return $scope.onChange($scope.ngModel);
          }
        };
        $scope.move = function(index, diff) {
          $scope.ngModel.move(index, index + diff);
          if ($scope.onChange) {
            return $scope.onChange($scope.ngModel);
          }
        };
        return $scope.toTitle = function(item) {
          var toTitle;
          if ($scope.options && $scope.options.toTitle) {
            toTitle = $scope.options.toTitle;
          } else {
            toTitle = function(item) {
              return l10n(item.title);
            };
          }
          return toTitle(item);
        };
      }],
      template: ('/client/cms_editable_app/_directives/uiceItemIdListEditor/uiceItemIdListEditor.html', '<form name="uiceItemIdListForm"><div class="input-group"><select class="form-control" ng-options="item.id as toTitle(item)   for item in items" ng-model="newId"></select><div class="input-group-btn"><button class="btn btn-success" ng-disabled="!newId || uiceItemIdListForm.$invalid || ngDisabled" ng-click="add()"><span class="fas fa-plus fa-nopadding-xs"></span><span class="hidden-xxs" ng-switch="!!addButtonText"><span ng-switch-when="true">{{addButtonText}}</span><span ng-switch-when="false" translate="">Add</span></span></button></div></div><table class="w-100"><tbody><tr ng-repeat="id in ngModel   track by $index"><td><span uic-transclude="" uic-transclude-bind="{$origScope: $parent.$parent.$parent}"></span></td><td class="text-right" ng-switch="disableMoveActions == \'true\'"><div class="btn-group" ng-switch-when="false"><button class="btn btn-default btn-xs" ng-click="move($index, -1)" ng-disabled="$first"><span class="fas fa-arrow-up nopadding"></span></button><button class="btn btn-default btn-xs" ng-click="move($index, 1)" ng-disabled="$last"><span class="fas fa-arrow-down"></span></button><button class="btn btn-danger btn-xs" ng-click="remove($index)" ng-disabled="ngDisabled"><span class="fas fa-trash-alt"></span><span translate="translate">Remove</span></button></div><button class="btn btn-danger btn-xs" ng-switch-when="true" ng-click="remove($index)" ng-disabled="ngDisabled"><span class="fas fa-trash-alt"></span><span translate="translate">Remove</span></button></td></tr></tbody></table></form>' + '')
    };
  });

}).call(this);
;
(function() {
  angular.module('ui.cms.editable').directive('uiceL10nInputSourceText', ["$filter", function($filter) {
    var cutFilter;
    cutFilter = $filter('cut');
    return {
      restrict: 'A',
      controller: ["$scope", "$attrs", function($scope, $attrs) {
        var begin, text;
        $scope.$isVisibleAllSourceText = false;
        $scope.$showAllSourceText = function() {
          return $scope.$isVisibleAllSourceText = true;
        };
        text = $scope.$eval($attrs.uiceL10nInputSourceText);
        if (!text) {
          return;
        }
        if (text.length > 380) {
          begin = cutFilter(text, true, 350, '');
          $scope.$sourceText = {
            begin: begin,
            end: text.replace(begin, '')
          };
        } else {
          $scope.$sourceText = {
            begin: text
          };
        }
      }],
      template: "<span>{{$sourceText.begin}}</span>\n<span ng-show=\"!$isVisibleAllSourceText && $sourceText.end\">\n    ...\n    <a ng-click=\"$showAllSourceText()\"  translate='' style='padding-left:2px;'>\n        view all\n    </a>\n</span>\n<span ng-show=\"$isVisibleAllSourceText\">{{$sourceText.end}}</span>"
    };
  }]).directive('uiceL10nInput', ["$templateCache", "$translationService", function($templateCache, $translationService) {
    var ALLOWED_INPUT_TYPES, PLACEHOLDERS, TRANSLATE_INPUT_TEMPLATES, attrsToString, isOnlyOneLangForSite;
    ALLOWED_INPUT_TYPES = ['html', 'text', 'textarea', 'price', 'password', 'number', 'email', 'url'];
    PLACEHOLDERS = {
      text: 'Text',
      number: 'Number',
      password: 'Password',
      email: 'Email. Ex.: some@mail.com',
      url: 'Paste link to page',
      textarea: 'Text',
      price: 'Price value'
    };
    TRANSLATE_INPUT_TEMPLATES = {
      "default": "{{INPUT_HTML}}",
      html: "{{INPUT_HTML}}",
      textarea: "{{INPUT_HTML}}",
      text: "{{INPUT_HTML}}"
    };
    if ($translationService.provider === 'google') {
      TRANSLATE_INPUT_TEMPLATES.textarea = "<div class='uice-l10n-input-textarea-with-btn'>\n    <div ng-if=\"lang!='" + CMS_DEFAULT_LANG + "'\"\n            uib-popover-template=\"popoverTemplateUrl\"\n            popover-title=\"{{'Translation suggested by' | translate}}\"\n            popover-placement='top-right'\n            popover-trigger=\"'outsideClick'\"\n            popover-is-open=\"$parent.popoverIsOpen\" >\n        <button class=\"btn btn-default {{TRANSLATE_BTN_CLASS}}\" title=\"{{'Translation suggested by Google Translate' | translate}}\">\n            <span class=\"fas fa-language nopadding\"></span>\n        </button>\n    </div>\n    {{INPUT_HTML}}\n</div>";
      TRANSLATE_INPUT_TEMPLATES.text = "<div ng-class=\"{'input-group': lang!='" + CMS_DEFAULT_LANG + "'}\" class=\"{{translation.popoverClass}}\">\n    {{INPUT_HTML}}\n    <span class=\"input-group-btn\" ng-if=\"lang!='" + CMS_DEFAULT_LANG + "'\"\n            uib-popover-template=\"popoverTemplateUrl\"\n            popover-title=\"{{'Translation suggested by' | translate}}\"\n            popover-placement='top-right'\n            popover-trigger=\"'outsideClick'\"\n            popover-is-open=\"$parent.popoverIsOpen\">\n        <button class=\"btn btn-default {{TRANSLATE_BTN_CLASS}}\" title=\"{{'Translation suggested by Google Translate' | translate}}\">\n            <span class=\"fas fa-language nopadding\"></span>\n        </button>\n    </span>\n</div>";
    }
    isOnlyOneLangForSite = function() {
      var e;
      try {
        return CMS_DEFAULT_LANG === CMS_ALLOWED_LANGS;
      } catch (error) {
        e = error;
      }
      return false;
    };
    attrsToString = function(attrs) {
      var _str, k, ref, v;
      _str = "";
      ref = attrs || {};
      for (k in ref) {
        v = ref[k];
        _str = _str + (" " + k + "='" + v + "'");
      }
      return _str;
    };
    $templateCache.put('/cache/uiceL10nInput/popover.html', "<span ng-switch=\"translation.error\">\n    <div ng-switch-when='false' class='clearfix'>\n        <span class='tr-link'  ng-click=\"setTranslation()\" title=\"{{'Apply translation' | translate}}\">\n            {{translation.result | cut:true:450}}\n        </span>\n        <a href=\"{{translation.href}}\" target='_blank' style='padding-left:2px;'>[google]</a>\n    </div>\n    <span ng-switch-when='true' class='text-danger'>{{'Error on text translation' | translate}}=(</span>\n</span>");
    return {

      /**
      *   @ngdoc directive
      *   @name ui.cms.editable.directive:uiceL10nInput
      *   @description
      *       директива для ввода значений в l10nObject, string, number переменные.
      *       Для l10nObject она отображает оригинальный текст для языка CMS_DEFAULT_LANG(
      *       если значение ngModel[CMS_DEFAULT_LANG] было определено).
      *   @restrict E
      *   @param {string | number | object} ngModel (ссылка) модель
      *   @param {string=} lang (ссылка) редактируемый язык. Для того, чтоб передавать
      *         этот параметр, ngModel должен быть l10nObject-ом
      *   @param {string=} [type='text'] (значение) тип тега, который будет использоваться['text', 'number', 'email', 'password', 'url', 'textarea']
      *   @param {boolean=} ngDisabled (ссылка) флаг доступности тега
      *   @param {string=} [inputClass='form-control'] (значение) класс, который применяется для input-a
      *   @param {string=} * обычные значения атрибутов доступные для input-тегов вроде ['name', 'required', 'min', 'max', 'minlength', 'maxlength']
      *   @example
      *       <pre>
      *           $scope.hello = {
      *                ru: 'Привет'
      *                en: 'Hello'
      *           }
      *           $scope.lang = 'ru'
      *       </pre>
      *       <pre>
      *           //- pug
      *           uice-l10n-input(ng-model="hello", lang="lang", type='textarea')
      *               span-label(translate='') With lang
      *           uice-l10n-input(ng-model="hello.ru", maxlength='10', required)
      *               span-label(translate='') Without lang
      *           uice-l10n-input(ng-model="noModel", type='number', min='3',max='10', input-class='form-control input-sm')
      *       </pre>
      *       <example module="DocExampleApp">
      *           <file name="script.js">
      *               mkDocCtrl('uiceL10nInputCtrl', {
      *                   hello: {
      *                       ru: 'Привет',
      *                       en: 'Hello'
      *                   },
      *                   lang: 'ru'
      *               });
      *           </file>
      *           <file name="index.html">
      *               <div ng-controller="uiceL10nInputCtrl">
      *                    <uice-l10n-input ng-model="hello" lang="lang" input-class="form-control input-sm" type="textarea">
      *                      <span-label translate="">With lang</span-label>
      *                    </uice-l10n-input>
      *                    <uice-l10n-input ng-model="hello.ru" maxlength="10" required>
      *                      <span-label translate="">Without lang</span-label>
      *                    </uice-l10n-input>
      *                    <uice-l10n-input ng-model="noModel" type="number" min="3" max="10" input-class='form-control input-sm'></uice-l10n-input>
      *               </div>
      *           </file>
      *       </example>
       */
      restrict: 'E',
      transclude: {
        label: '?spanLabel',
        validationArea: '?validationArea'
      },
      scope: {
        ngModel: '=',
        lang: '=?',
        ngDisabled: '=?',
        max: '@?',
        min: '@?',
        maxlength: '@?',
        minlength: '@?',
        name: '@?',
        placeholder: '@?'
      },
      template: function($element, $attrs) {
        var _attrs, classes, label_info_template, label_template, template;
        if (!ALLOWED_INPUT_TYPES.includes($attrs.type)) {
          $attrs.type = 'text';
        }
        _attrs = {
          name: "{{::name}}",
          type: $attrs.type,
          "class": $attrs.inputClass || 'form-control',
          'ng-disabled': "ngDisabled",
          placeholder: "{{placeholder || (_placeholder | translate)}}"
        };
        if (_attrs.type === 'price') {
          _attrs.type = 'number';
        }

        /*
                собираем все атрибуты для инпута
         */
        if ($attrs.lang) {
          _attrs['ng-model'] = "ngModel[lang]";
        } else {
          _attrs['ng-model'] = "ngModel";
        }
        if ($attrs.hasOwnProperty('required')) {
          _attrs.required = 'true';
        }
        classes = $attrs["class"] || '';
        classes = classes.split(' ');
        if (classes.includes('xs') || classes.includes('sm')) {
          _attrs["class"] += ' input-sm';
        }
        switch ($attrs.type) {
          case 'html':
            _attrs["class"] = _attrs["class"].replace('form-control', '');
            if ($attrs.minHeight) {
              _attrs['min-height'] = $attrs.minHeight;
            }
            if ($attrs.toolbar) {
              _attrs['toolbar'] = $attrs.toolbar;
            }
            if ($attrs.cmsModelItem) {
              _attrs['cms-model-item'] = "$parent." + $attrs.cmsModelItem;
            }
            break;
          case 'price':
            if ($attrs.max) {
              _attrs.max = "{{max}}";
            }
            if ($attrs.min) {
              _attrs.min = "{{min}}";
            } else {
              _attrs.min = '0';
            }
            if ($attrs.step) {
              _attrs.step = $attrs.step;
            } else {
              _attrs.step = '0.01';
            }
            break;
          case 'number':
            _attrs.max = "{{max}}";
            _attrs.min = "{{min}}";
            if ($attrs.step) {
              _attrs.step = $attrs.step;
            }
            break;
          default:
            _attrs.maxlength = '{{maxlength}}';
            _attrs.minlength = '{{minlength}}';
            if ($attrs.type === 'textarea' && $attrs.minHeight) {
              _attrs.style = "min-height:" + $attrs.minHeight + ";";
            }
        }

        /*
                собираем html для директивы
         */
        template = TRANSLATE_INPUT_TEMPLATES[$attrs.type] || TRANSLATE_INPUT_TEMPLATES["default"];
        label_info_template = "";
        if (isOnlyOneLangForSite()) {
          $attrs.lang = null;
        }
        if ($attrs.lang) {
          label_info_template += "<span class='fas fa-language' style='font-size:15px;margin:0 3px;' title=\"{{'This field is multilingual' | translate}}\"></span>";
        }
        switch ($attrs.type) {
          case 'html':
            template = template.replace('{{INPUT_HTML}}', "<uic-html-editor " + (attrsToString(_attrs)) + "></uic-html-editor>");
            if ($attrs.lang) {
              label_info_template = label_info_template + "\n<a ng-click='openGoogleTranslateUrl()' ng-show=\"lang!='" + CMS_DEFAULT_LANG + "'\">\n    [google translate]\n</a>";
            }
            break;
          case 'textarea':
            template = template.replace('{{INPUT_HTML}}', "<textarea " + (attrsToString(_attrs)) + "></textarea>");
            break;
          default:
            template = template.replace('{{INPUT_HTML}}', "<input " + (attrsToString(_attrs)) + " />");
        }
        if (_attrs["class"].indexOf('input-sm') > -1) {
          template = template.replace('{{TRANSLATE_BTN_CLASS}}', 'btn-sm');
        } else {
          template = template.replace('{{TRANSLATE_BTN_CLASS}}', '');
        }
        if (_attrs.required) {
          label_info_template += "<span class='text-danger'> *</span>";
        }
        if ($attrs.label) {
          if ($attrs.label.indexOf('{{') > -1) {
            label_template = "<span>" + $attrs.label + "</span>";
          } else {
            label_template = "<span translate=''>" + $attrs.label + "</span>";
          }
        } else {
          label_template = "<span ng-transclude=\"label\"></span>";
        }
        if ($attrs.lang) {
          label_template = "<label class='uice-l10n-input-label'>\n    <span title=\"{{'This field is multilingual' | translate}}\">\n        " + label_template + "\n    </span>\n    " + label_info_template + "\n</label>";
        } else {
          label_template = "<label class='uice-l10n-input-label'>\n    " + label_template + "\n    " + label_info_template + "\n</label>";
        }
        if ($attrs.type === 'html') {
          return label_template + "<div ng-transclude='validationArea' class='uice-l10n-input-validation-area'></div>" + template;
        }
        return label_template + "\n<p ng-if=\"originalValue\" style='text-indent:0;margin-bottom:3px;'>\n    <small translate=\"\">Source text</small>:\n    <small uice-l10n-input-source-text=\"originalValue\">\n    </small>\n</p>\n<div ng-transclude='validationArea' class='uice-l10n-input-validation-area'></div>\n" + template;
      },
      controller: ["$scope", "$attrs", function($scope, $attrs) {
        var ref;
        if (!ALLOWED_INPUT_TYPES.includes($attrs.type)) {
          $attrs.type = 'text';
        }
        $scope._placeholder = PLACEHOLDERS[$attrs.type] || 'Text';
        $scope.popoverTemplateUrl = "/cache/uiceL10nInput/popover.html";
        $scope.popoverIsOpen = false;
        $scope.translation = {
          result: '',
          error: false
        };
        if (isOnlyOneLangForSite()) {
          $scope.lang = angular.copy(CMS_DEFAULT_LANG);
        }
        if ($attrs.lang && ((ref = $attrs.type) === 'text' || ref === 'textarea')) {

          /*
                  поодержка popover-a с переводами текстов
                  --------------------------------------------
                  для uic-html-editor не нужно генерировать кнопку с
                  переводами. для него нужна только ссылка на
                  гугль-транслейт
           */
          $scope.$watch('ngModel', function(ngModel, oldValue) {
            if (!angular.isObject(ngModel)) {
              $scope.ngModel = {};
              return;
            }
            if ($scope.lang && $scope.lang !== CMS_DEFAULT_LANG && ngModel) {
              $scope.originalValue = ngModel[CMS_DEFAULT_LANG];
              $scope._placeholder = 'Type translation for text';
            } else {
              $scope.originalValue = '';
              $scope._placeholder = PLACEHOLDERS[$attrs.type];
            }
          }, true);
          $scope.$watch('lang', function(lang, oldValue) {
            if (lang && lang !== CMS_DEFAULT_LANG && $scope.ngModel) {
              $scope.originalValue = $scope.ngModel[CMS_DEFAULT_LANG];
              $scope._placeholder = 'Type translation for text';
            } else {
              $scope.originalValue = '';
              $scope._placeholder = PLACEHOLDERS[$attrs.type];
            }
          });
          $scope.$watch('popoverIsOpen', function(popoverIsOpen, oldValue) {
            if (popoverIsOpen) {
              $scope.translation = {
                result: '',
                popoverClass: '',
                error: false
              };
              $translationService.translate(CMS_DEFAULT_LANG, $scope.lang, [$scope.originalValue]).then(function(data) {
                $scope.translation.result = data[$scope.originalValue] || $scope.originalValue;
                $scope.translation.href = $translationService.getTranslateUrl(CMS_DEFAULT_LANG, $scope.lang, $scope.originalValue);
                if ($scope.translation.result.length <= 40) {
                  $scope.translation.popoverClass = 'popover-sm';
                }
              }, function(data) {
                $scope.translation.error = true;
              });
            }
          });
          $scope.setTranslation = function() {
            $scope.ngModel[$scope.lang] = $scope.translation.result || '';
            $scope.popoverIsOpen = false;
          };
        }
        $scope.openGoogleTranslateUrl = function() {
          var ngModel, text;
          ngModel = $scope.ngModel || {};
          text = ngModel[CMS_DEFAULT_LANG] || '';
          if (!text) {
            return;
          }
          text = text.replace(/<(?:.|\n)*?>/gm, '');
          text = text.replaceAll('&nbsp;', ' ').replaceAll('\t', '');
          text = text.replaceAll('\n\n', '\n').replaceAll('\n ', '\n').replaceAll('\n\n', '\n');
          window.open($translationService.getTranslateUrl(CMS_DEFAULT_LANG, $scope.lang, text), '_blank');
        };
      }]
    };
  }]);

}).call(this);
;
(function() {
  var hasProp = {}.hasOwnProperty;

  angular.module('ui.cms.editable').directive('uiceListEditor', function() {
    var DIRECTIVE_TMPL, INPUT_TMPLT;
    INPUT_TMPLT = "<span class=\"error\" ng-if=\"uiceListForm[[formId]].input.$error[inputType]\">[invalid {{inputType || 'text'}}]</span>\n<div class=\"form-group nomargin\">\n  <div class=\"input-group\">\n    <input class=\"form-control\" type=\"{{inputType || 'text'}}\" ng-model=\"ctrl.newData\" uic-enter-press=\"ctrl.add()\" placeholder=\"{{ctrl.inputPlaceholder}}\" ng-disabled=\"ng-disabled\" name=\"input\"/>\n    <div class=\"input-group-btn\">\n      <button class=\"btn btn-success\" ng-disabled=\"!ctrl.newData || uiceListForm[[formId]].$invalid || ctrl.ngDisabled\" ng-click=\"ctrl.add()\"><span class=\"fas fa-plus fa-nopadding-xs\"></span><span class=\"hidden-xxs\" ng-switch=\"!!ctrl.addButtonText\"><span ng-switch-when=\"true\">{{ctrl.addButtonText}}</span><span ng-switch-when=\"false\" translate=\"\" translate-context=\"append\">Add</span></span></button>\n    </div>\n  </div>\n</div>";
    DIRECTIVE_TMPL = "<form name=\"uiceListForm[[formId]]\">\n  <div uic-transclude=\"listEmpty\" ng-if=\"ctrl.ngModel.length==0\" uic-transclude-bind=\"{$origScope: $origScope}\"></div>[[inputPositionTop]]\n  <div class=\"well well-sm\" ng-repeat=\"$item in ctrl.ngModel   track by $index\">\n    <div>\n      <div class=\"well-actions\">\n        <div class=\"btn-group\" ng-if=\"!ctrl.disableMoveActions\">\n          <button class=\"btn btn-default btn-xs\" ng-click=\"ctrl.move($index, -1)\" ng-disabled=\"$first\"><span class=\"fas fa-arrow-up nopadding\"></span></button>\n          <button class=\"btn btn-default btn-xs\" ng-click=\"ctrl.move($index, 1)\" ng-disabled=\"$last\"><span class=\"fas fa-arrow-down nopadding\"></span></button>\n          <button class=\"btn btn-danger btn-xs\" ng-click=\"ctrl.remove($index)\" ng-disabled=\"ctrl.ngDisabled\"><span class=\"fas fa-trash-alt\"></span><span translate=\"translate\">Remove</span></button>\n        </div>\n        <button class=\"btn btn-danger btn-xs\" ng-click=\"ctrl.remove($index)\" ng-disabled=\"ctrl.ngDisabled\" ng-if=\"ctrl.disableMoveActions\"><span class=\"fas fa-trash-alt\"></span><span translate=\"translate\">Remove</span></button>\n      </div><span uic-transclude=\"itemTitle\" uic-transclude-bind=\"{$origScope: $origScope}\">\n        <label><span translate=\"\">Element</span> {{$index+1}}</label></span>\n    </div>\n    <div></div>\n    <div uic-transclude=\"itemBody\" uic-transclude-bind=\"{$origScope: $origScope}\"></div>\n  </div>[[inputPositionBottom]]\n</form>";
    return {

      /**
      *   @ngdoc directive
      *   @name ui.cms.editable.directive:uiceListEditor
      *   @description
      *
      *   @restrict E
      *   @param {Array<Object>} ngModel (ссылка) массив элементов
      *   @param {function} onAdd (значение) функция, которая вызывается при добавлении элемента в массив
      *   @param {string=} [inputPosition='top'] (значение) где отображать инпут ввода нового значения (top, bottom, none)
      *   @param {string=} [inputType='text'] (значение) тип вводимого значения внизу редактора
      *   @param {string=} inputPlaceholder (значение) placeholder для вводимого значения внизу редактора
      *   @param {string=} [addButtonText='Add'] (значение) текст кнопки для кнопки добавления вводимого значения внизу редактора
      *   @param {boolean=} [disableMoveActions=false] (значение)
      *   @param {boolean=} ngDisabled (ссылка) флаг доступности тега
      *   @example
      *       <pre>
      *           $scope.myArr = [
      *               {title: 'Elem 1'},
      *               {title: 'Elem 2'},
      *               {title: 'Elem Hello'},
      *           ]
      *           $scope.onAdd = (value)->
      *               $scope.myArr.push({title: value})
      *       </pre>
      *       <pre>
      *           //- pug
      *           uice-list-editor(ng-model="myArr", on-add="onAdd(value)")
      *               item-body
      *                   .form-group
      *                       label Title
      *                       input.form-control(ng-model="$item.title", required='')
      *       </pre>
      *       <example module="DocExampleApp">
      *           <file name="script.js">
      *               mkDocCtrl('uiceListEditorCtrl', {
      *                   myArr: [
      *                       {title: 'Elem 1'},
      *                       {title: 'Elem 2'},
      *                       {title: 'Elem Hello'},
      *                   ],
      *                   onAdd: function(value){
      *                       this.myArr.push(value);
      *                    }
      *               });
      *           </file>
      *           <file name="index.html">
      *               <div ng-controller="uiceListEditorCtrl">
      *                   <uice-list-editor ng-model="myArr" on-add="onAdd(value)">
      *                       <item-body>
      *                           <div class='form-group'>
      *                               <label>Title</label>
      *                               <input ng-model="$item.title" class='form-control' />
      *                           </div>
      *                       </item-body>
      *                   </uice-list-editor>
      *               </div>
      *           </file>
      *       </example>
       */
      restrict: 'E',
      transclude: {
        itemTitle: '?itemTitle',
        itemBody: '?itemBody',
        listEmpty: '?listEmpty'
      },
      scope: {},
      bindToController: {
        ngModel: '=',
        inputType: '@?',
        inputPlaceholder: '@?',
        disableMoveActions: '@?',
        addButtonText: '@',
        onAdd: '&?',
        ngDisabled: '=?'
      },
      controllerAs: 'ctrl',
      controller: ["$scope", "$cms", "$attrs", "$models", "H", function($scope, $cms, $attrs, $models, H) {
        var ctrl, k, ref, setInitialData, v;
        $scope.$cms = $cms;
        $scope.$models = $models;
        $scope.$origScope = H.findOrigScope($scope, $attrs);
        ctrl = this;
        ref = $scope.$parent;
        for (k in ref) {
          if (!hasProp.call(ref, k)) continue;
          v = ref[k];
          if (k.indexOf('$') === -1) {
            if (!ctrl.hasOwnProperty(k)) {
              $scope[k] = v;
            }
          }
        }
        setInitialData = function() {
          var ref1;
          if (!ctrl.inputType || ((ref1 = ctrl.inputType) === 'text' || ref1 === 'email' || ref1 === 'url')) {
            ctrl.newData = '';
          }
          if (ctrl.inputType === 'number') {
            return ctrl.newData = 0;
          }
        };
        this.add = function() {
          if (!(ctrl.ngModel instanceof Array)) {
            ctrl.ngModel = [];
          }
          if (ctrl.onAdd) {
            ctrl.onAdd({
              value: ctrl.newData
            });
          }
          return setInitialData();
        };
        this.remove = function(index) {
          return ctrl.ngModel.splice(index, 1);
        };
        this.move = function(index, diff) {
          return ctrl.ngModel.move(index, index + diff);
        };
        return this;
      }],
      template: function($element, $attrs) {
        var context, k, tmplt, v;
        context = {
          formId: new Date().getTime(),
          inputPositionTop: '',
          inputPositionBottom: ''
        };
        if (!$attrs.inputPosition || $attrs.inputPosition === 'top') {
          context.inputPositionTop = INPUT_TMPLT.replaceAll('[[formId]]', context.formId);
          context.inputPositionTop += '<hr/>';
        } else if ($attrs.inputPosition !== 'none') {
          context.inputPositionBottom = INPUT_TMPLT.replaceAll('[[formId]]', context.formId);
          context.inputPositionBottom = '<hr/>' + context.inputPositionBottom;
        }
        tmplt = DIRECTIVE_TMPL;
        for (k in context) {
          v = context[k];
          tmplt = tmplt.replaceAll('[[' + k + ']]', v);
        }
        return tmplt;
      }
    };
  });

}).call(this);
;
(function() {
  angular.module('ui.cms.editable').directive('uiceMainImgManager', function() {
    var getSaveFunction;
    getSaveFunction = function($scope) {
      if (!$scope || $scope.id === 1) {
        return null;
      }
      if (angular.isFunction($scope.save)) {
        return $scope.save;
      }
      return getSaveFunction($scope.$parent);
    };
    return {

      /**
      *   @ngdoc directive
      *   @name ui.cms.editable.directive:uiceMainImgManager
      *   @description виджет для загрузки/редактирования/удаления поля mainImg(или любого другого указаного в атрибуте field) у объекта
      *   @restrict E
      *   @param {object} ngModel (ссылка) модель объекта, которая содержит поле mainImg
      *   @param {string=} [field='mainImg'] (значение) название поля, внутри переданной модели, в котором хранится L10nImageFile объект
      *   @param {string=} [heading='Main image'] (значение) заголовок редактора картинки
      *   @param {boolean=} [disableDelete=false] (значение) возможность отключения удаления картинки
      *   @param {boolean=} [disableView=false] (значение) возможность отключения просмотра картинки
      *   @param {boolean=} ngDisabled (ссылка) флаг доступности тега
      *   @example
      *       <pre>
      *           id = 1 # mongodb objectId or int(for django app)
      *           $scope.item = CmsArticle.findById({id})
      *       </pre>
      *       <pre>
      *           //- pug
      *           uice-main-img-manager(ng-model="item")
      *       </pre>
       */
      restrict: 'E',
      transclude: {
        noImgPlaceholder: '?noImgPlaceholder'
      },
      scope: {
        ngModel: '=',
        field: '@?',
        heading: '@?',
        ngDisabled: '=?',
        disableDelete: '@?',
        disableView: '@?',
        options: '=?'
      },
      controller: ["$scope", "$attrs", "uiceL10nFileEditorModal", function($scope, $attrs, uiceL10nFileEditorModal) {
        var ref, ref1, saveFn;
        $scope.field = $scope.field || 'mainImg';
        $scope._disableDelete = (ref = $scope.disableDelete) === true || ref === 'true';
        $scope._disableView = (ref1 = $scope.disableView) === true || ref1 === 'true';
        $scope.manageMainImg = function() {
          var k, options, ref2, v;
          if ($scope.ngDisabled) {
            return;
          }
          options = {
            maxSize: '13 MB',
            accept: 'image/jpeg,image/png,image/gif',
            forModelField: $scope.field,
            allowEditTitleDescription: true
          };
          ref2 = $scope.options || {};
          for (k in ref2) {
            v = ref2[k];
            options[k] = v;
          }
          return uiceL10nFileEditorModal.edit($scope.ngModel, options).then(function(l10nFile) {
            $scope.ngModel[$scope.field] = l10nFile;
          });
        };
        $scope.viewMainImg = function() {
          if ($scope.ngModel[$scope.field] && !$scope._disableView) {
            return uiceL10nFileEditorModal.view($scope.ngModel, {
              forModelField: $scope.field
            });
          }
          return $scope.manageMainImg();
        };
        $scope.removeMainImg = function() {
          var options;
          if ($scope.ngDisabled) {
            return;
          }
          options = {
            title: angular.tr('Delete main image?'),
            forModelField: $scope.field
          };
          if ($scope.field !== 'mainImg') {
            options.title = angular.tr('Delete image?');
          }
          return uiceL10nFileEditorModal["delete"]($scope.ngModel, options).then(function() {
            $scope.ngModel[$scope.field] = null;
          });
        };
        saveFn = getSaveFunction($scope.$parent);
        if (saveFn) {
          $scope.saveNgModel = function() {
            return saveFn(void 0, true);
          };
        }
      }],
      template: ('/client/cms_editable_app/_directives/uiceMainImgManager/uiceMainImgManager.html', '<label ng-switch="!!heading"><span ng-switch-when="true">{{heading}}</span><span ng-switch-when="false" translate="">Main image:</span></label><div class="col-md-100 col-md-offset-0 col-sm-60 col-sm-offset-20 col-xs-80 col-xs-offset-10 nopadding" ng-switch="!!ngModel.id"><div ng-switch-when="true"><div class="text-center" ng-if="!ngModel[field]"><div ng-transclude="noImgPlaceholder"></div><button class="btn btn-sm btn-success" ng-click="manageMainImg()" style="margin: 1em auto;" ng-disabled="ngDisabled"><div class="fas fa-cloud-upload-alt"></div><span translate="">Upload image</span></button></div><div class="thumbnail nomargin" ng-if="ngModel[field]"><uic-picture class="nomargin thumbnail-content" file="ngModel[field]" ng-click="viewMainImg()" force-cms-storage-type="{{options.forceCmsStorageType}}"></uic-picture><div class="thumbnail-actions"><div class="btn-group"><button class="btn btn-primary" ng-click="manageMainImg()" ng-disabled="ngDisabled"><span class="fas fa-pencil-alt"></span><span translate="">Edit</span></button><button class="btn btn-danger" ng-click="removeMainImg()" ng-disabled="ngDisabled" title="{{\'Delete\' | translate}}" ng-if="!_disableDelete"><span class="fas fa-trash-alt nopadding"></span></button></div></div></div></div><div class="text-center" ng-switch-when="false"><div translate="">To assign the main image, you must first save the document</div><button class="btn btn-sm btn-primary" ng-click="saveNgModel()" ng-if="saveNgModel" style="margin:0.5em auto;"><div class="fas fa-save"></div><span translate="">Save</span></button></div></div>' + '')
    };
  });

}).call(this);
;
(function() {
  var isTitleEmpty;

  isTitleEmpty = function(title) {
    var empty, k, v;
    if (!Object.keys(title || {}).length) {
      return true;
    }
    empty = true;
    for (k in title) {
      v = title[k];
      if (v) {
        empty = false;
        break;
      }
    }
    return empty;
  };

  angular.module('ui.cms.editable').directive('uiceNavbarNavEditor', ["$cms", "$filter", "$models", function($cms, $filter, $models) {
    return {

      /**
      *   @ngdoc directive
      *   @name ui.cms.editable.directive:uiceNavbarNavEditor
      *   @description редактор меню
      *
      *   @restrict E
      *   @param {object} ngModel (ссылка) модель
      *   @example
      *       <pre>
      *           $scope.nav = {
      *                right: [
      *                    {
      *                        type: 'href',
      *                        data: 'www.google.com',
      *                        title: {ru:"Ссылка 1", en:"Link 1"},
      *                        attrs: {target: '_blank'}
      *                    },
      *                    {type: 'ui-sref', data: 'app.index', title:{en: "Index"}},
      *                    {type: 'directive', data: 'ui-lang-picker-for-navbar'},
      *                    {
      *                        title: {en:'Menu', ru:'Меню'},
      *                        items:[
      *                            {
      *                               type: 'href',
      *                               data: 'www.yandex.ru',
      *                               title: {ru:"Ссылка 2", en:"Link 2"}
      *                            },
      *                            {
      *                               type: 'ui-sref',
      *                               data: 'app.index',
      *                               title: {ru:"Основная страница", en:"Index page"}
      *                           }
      *                        ]
      *                    }
      *                ]
      *            }
      *       </pre>
      *       <pre>
      *           //- pug
      *           uice-navbar-nav-editor(ng-model="nav")
      *       </pre>
      *       <example module="DocExampleApp">
      *           <file name="script.js">
      *               mkDocCtrl('uiceNavbarNavEditorCtrl', {
      *                 nav: {
      *                   right: [
      *                        {
      *                            type: 'href',
      *                            data: 'www.google.com',
      *                            title: {ru:"Ссылка 1", en:"Link 1"},
      *                            attrs: {target: '_blank'}
      *                       },
      *                        {type: 'ui-sref', data: 'app.index', title:{en: "Index"}},
      *                        {type: 'directive', data: 'ui-lang-picker-for-navbar'},
      *                        {
      *                            title: {en:'Menu', ru:'Меню'},
      *                            items:[
      *                                {
      *                                   type: 'href',
      *                                  data: 'www.yandex.ru',
      *                                   title: {ru:"Ссылка 2", en:"Link 2"}
      *                                },
      *                                {
      *                                   type: 'ui-sref',
      *                                   data: 'app.index',
      *                                   title: {ru:"Основная страница", en:"Index page"}
      *                               }
      *                           ]
      *                        }
      *                    ]
      *                   }
      *               });
      *           </file>
      *           <file name="index.html">
      *               <div ng-controller="uiceNavbarNavEditorCtrl">
      *                   <uice-navbar-nav-editor ng-model="nav"></uice-navbar-nav-editor>
      *               </div>
      *           </file>
      *       </example>
       */
      restrict: 'E',
      scope: {
        ngModel: '=',
        cmsModelItem: '=?',
        disableItemTypes: '@?',
        staticPages: '=?'
      },
      controller: ["$scope", function($scope) {
        var error, l10n, proxyToModel, skip_add_to_history, translate;
        translate = $filter('translate');
        l10n = $filter('l10n');
        skip_add_to_history = false;
        $scope.tab = 0;
        $scope.$cms = $cms;
        $scope.proxyModel = angular.copy($scope.ngModel);
        $scope.align = 'right';
        $scope.lang = CMS_DEFAULT_LANG;
        try {
          $scope.LANGS = CMS_ALLOWED_LANGS;
        } catch (error1) {
          error = error1;
          $scope.LANGS = ['ru', 'en', 'cz'];
        }

        /*
                ф-ции работы с историей
         */
        $scope.history = [];
        $scope.revision = 0;
        $scope.originalModel = angular.copy($scope.ngModel);
        $scope.undo = function() {
          skip_add_to_history = true;
          $scope.revision -= 1;
          $scope.ngModel = $scope.history[$scope.revision];
          $scope.proxyModel = angular.copy($scope.ngModel);
          return $scope.history.splice($scope.revision, 0);
        };
        $scope.redo = function() {
          var r;
          r = $scope.revision + 1;
          if (r > $scope.history.length - 1) {
            return;
          }
          skip_add_to_history = true;
          $scope.ngModel = $scope.history[r];
          $scope.proxyModel = angular.copy($scope.ngModel);
          return $scope.revision = r;
        };
        $scope.resetModel = function() {
          $scope.proxyModel = angular.copy($scope.originalModel);
          $scope.ngModel = angular.copy($scope.originalModel);
          $scope.history = [];
          $scope.revision = 0;
          return skip_add_to_history = true;
        };

        /*
         */
        $scope.setActiveTab = function(index) {
          return $scope.tab = index;
        };
        $scope.getTitle = function(item) {
          var obj;
          if (item.type === 'directive') {
            return translate('Site directive');
          }
          if (item.type === 'ui-sref:generator:article-category') {
            if (isTitleEmpty(item.title)) {
              obj = $models.CmsCategoryForArticle.findItemByField('id', item.data) || $models.CmsCategoryForArticle.findItemByField('identifier', item.data);
              if (obj) {
                return l10n(obj.title);
              }
            } else {
              return l10n(item.title);
            }
            return translate('!Noname!');
          }
          if (item.type === 'ui-sref:app.articles.details') {
            if (isTitleEmpty(item.title)) {
              obj = $models.CmsArticle.findItemByField('id', item.data);
              if (obj) {
                return l10n(obj.title);
              }
            } else {
              return l10n(item.title);
            }
            return translate('!Noname!');
          }
          if (item.type === 'ui-sref:app.galleries.details') {
            if (isTitleEmpty(item.title)) {
              obj = $models.CmsPictureAlbum.findItemByField('id', item.data);
              if (obj) {
                return l10n(obj.title);
              }
            } else {
              return l10n(item.title);
            }
            return translate('!Noname!');
          }
          if (item.type === 'contacts') {
            return translate('Contacts');
          }
          return l10n(item.title) || translate('!Noname!');
        };
        $scope.moveItem = function(index, i) {
          return $scope.proxyModel[$scope.align].move(index, index + i);
        };
        $scope.removeItem = function(index) {
          return $scope.proxyModel[$scope.align].splice(index, 1);
        };
        $scope.addItem = function(index, i) {
          i = index + i;
          $scope.proxyModel[$scope.align] = $scope.proxyModel[$scope.align] || [];
          return $scope.proxyModel[$scope.align].insert(i, {
            type: 'href',
            data: '',
            title: {},
            attrs: {
              target: '_blank'
            },
            html: {}
          });
        };
        proxyToModel = function(proxyModel) {
          var _item, item, j, k, l, len, len1, new_items, ngModel, proxy, ref, v;
          proxy = angular.copy(proxyModel);
          ngModel = {};
          for (k in proxy) {
            v = proxy[k];
            if (k !== "left" && k !== "right" && k !== "center") {
              ngModel[k] = v;
            }
            if (v === void 0 || v === null) {
              ngModel[k] = v;
              continue;
            }
            ngModel[k] = [];
            for (j = 0, len = v.length; j < len; j++) {
              item = v[j];
              if (!item.data && !item.items && !item.type === 'contacts') {
                continue;
              }
              if (!item.items) {
                ngModel[k].push(item);
                continue;
              }
              new_items = [];
              ref = item.items;
              for (l = 0, len1 = ref.length; l < len1; l++) {
                _item = ref[l];
                if (!_item.data) {
                  continue;
                }
                new_items.push(_item);
              }
              item.items = new_items;
              ngModel[k].push(item);
            }
          }
          return ngModel;
        };
        $scope.$watch('proxyModel', function(pM) {
          $scope.ngModel = angular.copy(proxyToModel(pM));
          if (!skip_add_to_history) {
            $scope.history.splice($scope.revision, 0);
            $scope.history.push(angular.copy($scope.ngModel));
            $scope.revision = $scope.history.length - 1;
          }
          return skip_add_to_history = false;
        }, true);
        return $scope.$watch('ngModel', function(ngModel) {
          if (!$scope.proxyModel) {
            $scope.proxyModel = angular.copy(ngModel);
            return $scope.originalModel = angular.copy(ngModel);
          }
        }, true);
      }],
      template: ('/client/cms_editable_app/_directives/uiceNavbarNavEditor/uiceNavbarNavEditor.html', '<div class="toolbar text-right"><div class="btn-group"><button class="btn btn-sm btn-danger" ng-disabled="revision==0" ng-click="undo()"><div class="fas fa-undo"></div></button><button class="btn btn-sm btn-default" ng-disabled="history.length==revision+1 || !history.length" ng-click="redo()"><div class="fas fa-redo"></div></button><button class="btn btn-sm btn-success" ng-click="addItem(0,0)" ng-hide="proxyModel[align].length"><div class="fas fa-plus"></div><span translate="translate">Add menu item</span></button><button class="btn btn-sm btn-info" ng-click="resetModel()"><div class="fas fa-circle"></div><span translate="translate">Reset to default</span></button></div></div><div class="menu-item-editor" ng-repeat="item in proxyModel[align]"><div class="header"><div class="text-left title" ng-click="setActiveTab($index)"><div class="fas fa-pencil-alt" ng-hide="tab!=$index"></div>{{getTitle(item)}}</div><div class="text-right btn-control"><div class="btn-group"><button class="btn btn-xs btn-default" ng-disabled="$last" ng-click="moveItem($index, 1)"><div class="fas fa-arrow-down"></div></button><button class="btn btn-xs btn-default" ng-disabled="$first" ng-click="moveItem($index, -1)"><div class="fas fa-arrow-up"></div></button><div class="btn-group" uib-dropdown="uib-dropdown"><button class="btn btn-xs btn-success" uib-dropdown-toggle="uib-dropdown-toggle"><span translate="translate">Add item</span><span> </span><span class="caret"></span></button><ul class="dropdown-menu pull-right" uib-dropdown-menu="uib-dropdown-menu"><li><a ng-click="addItem($index, 0)" translate="translate" style="cursor:pointer;">to top</a></li><li><a ng-click="addItem($index, 1)" translate="translate" style="cursor:pointer;">to bottom</a></li></ul></div><button class="btn btn-xs btn-danger" ng-disabled="proxyModel[align].length&lt;2" ng-click="removeItem($index)"><div class="fas fa-trash-alt"></div></button></div></div></div><div class="content" uib-collapse="tab!=$index"><uice-navbar-nav-item-editor ng-model="item" allow-subitems="{{allowSubitems || \'true\'}}" disable-item-types="{{disableItemTypes}}" static-pages="staticPages" cms-model-item="cmsModelItem"></uice-navbar-nav-item-editor></div></div>' + '')
    };
  }]).directive('uiceNavbarNavItemEditor', ["$models", "$cms", "$filter", function($models, $cms, $filter) {
    return {
      restrict: 'E',
      scope: {
        ngModel: '=',
        cmsModelItem: '=?',
        allowSubitems: '@?',
        disableItemTypes: '@?',
        staticPages: '=?'
      },
      controller: ["$scope", "$injector", "uiceNavbarNavItemEditorModal", function($scope, $injector, uiceNavbarNavItemEditorModal) {
        var l10n, translate;
        translate = $filter('translate');
        l10n = $filter('l10n');
        $scope.$cms = $cms;
        $scope.$models = $models;
        $scope.lang = CMS_DEFAULT_LANG;
        $scope.options = {
          customSiteActions: $cms.customSiteActions,
          customSiteAnchors: $cms.customSiteAnchors,
          CMS_ALLOWED_LANGS: CMS_ALLOWED_LANGS
        };
        if (angular.isEmpty($scope.options.customSiteActions)) {
          $scope.options.customSiteActions = null;
        }
        if (angular.isEmpty($scope.options.customSiteAnchors)) {
          $scope.options.customSiteAnchors = null;
        }
        if ($injector.has('AdminAppConfig')) {
          $scope.visibleModels = $injector.get('AdminAppConfig').visibleModels;
        }
        $scope.getTitle = function(item) {
          var obj;
          if (!isTitleEmpty(item.title)) {
            return l10n(item.title);
          }
          if (item.type === 'directive') {
            return translate('Site Directive');
          }
          if (item.type === 'ui-sref:app.articles.details') {
            if (isTitleEmpty(item.title)) {
              obj = $models.CmsArticle.findItemByField('id', item.data);
              if (obj) {
                return l10n(obj.title) || translate('!Noname!');
              }
            }
            return translate('!Noname!');
          }
          if (item.type === 'ui-sref:app.galleries.details') {
            if (isTitleEmpty(item.title)) {
              obj = $models.CmsPictureAlbum.findItemByField('id', item.data);
              if (obj) {
                return l10n(obj.title) || translate('!Noname!');
              }
            }
            return translate('!Noname!');
          }
          return translate('!Noname!');
        };
        $scope.getTitleForAction = function(k, v) {
          var title;
          title = k;
          if (v.description) {
            title = k + ": " + v.description;
          }
          return title;
        };
        $scope.onItemEditClick = function(item, index) {
          var options;
          options = {
            staticPages: $scope.staticPages,
            cmsModelItem: $scope.cmsModelItem
          };
          uiceNavbarNavItemEditorModal.open(item, options).then(function(data) {
            $scope.ngModel.items[index] = data;
          });
        };
        $scope.onItemDeleteClick = function(index) {
          return $scope.ngModel.items.splice(index, 1);
        };
        $scope.onMoveItemClick = function(index, i) {
          return $scope.ngModel.items.move(index, index + i);
        };
        $scope.addItem = function() {
          if ($scope.ngModel.type !== '') {
            return;
          }
          if (!$scope.ngModel.items) {
            $scope.ngModel.items = [];
          }
          return $scope.ngModel.items.push({
            title: {},
            type: 'href',
            data: '',
            attrs: {
              target: '_blank'
            }
          });
        };
        return $scope.$watch('ngModel.type', function(newVal, oldVal) {
          if (oldVal !== newVal) {
            $scope.ngModel.data = '';
            if (newVal === 'file') {
              $scope.ngModel.data = void 0;
            }
          }
          if (newVal === 'ui-sref:anchor') {
            $scope.ngModel.data = Object.keys($scope.options.customSiteAnchors)[0];
          }
        });
      }],
      template: ('/client/cms_editable_app/_directives/uiceNavbarNavEditor/uiceNavbarNavItemEditor.html', '<div class="form-inline"><div class="form-group"><label translate="translate">Menu item type:</label><select class="form-control input-sm" ng-model="ngModel.type"><option value="" ng-if="allowSubitems==\'true\'" translate="">Submenu</option><option value="href" translate=""> Link to external site</option><option value="ui-sref:generator:article-category" ng-if="allowSubitems==\'true\' &amp;&amp; disableItemTypes!=\'article_category\'" translate="">Article category</option><option value="ui-sref:app.articles.details" translate=""> Link to article</option><option value="ui-sref:app.galleries.details" ng-if="visibleModels.CmsPictureAlbum" translate=""> Link to picture album</option><option value="ui-sref:anchor" ng-if="options.customSiteAnchors" translate="">Link to anchor on page</option><option value="file" ng-if="cmsModelItem" translate=""> Link to file</option><option value="contacts" translate="">Public contacts</option><option value="action" ng-if="options.customSiteActions" translate="">Site action</option><option value="directive" translate="">Site directive</option><option value="ui-sref" translate="">View/Route</option></select></div></div><div class="form-inline"><div class="form-group"><label translate="">Hide menu item for languages:</label><label class="checkbox-inline checkbox-sm" ng-repeat="l_code in options.CMS_ALLOWED_LANGS"><input ng-model="ngModel.hideForLangs[l_code]" type="checkbox"/>{{$cms.LANG_MAP[l_code]}}</label></div></div><div class="form-inline"><div class="form-group"><label translate="">Hide menu item for screen size:</label><div class="checkbox-inline checkbox-sm"><label><input ng-model="ngModel.hideForScreenSize.xs" type="checkbox"/><span translate="" translate-context="screen size">Mobile</span><span> (xs)</span></label></div><label class="checkbox-inline checkbox-sm"><input ng-model="ngModel.hideForScreenSize.sm" type="checkbox"/><span translate="" translate-context="screen size">Mobile</span><span> (sm)</span></label><label class="checkbox-inline checkbox-sm"><input ng-model="ngModel.hideForScreenSize.md" type="checkbox"/><span translate="" translate-context="screen size">Tablet</span><span> (md)</span></label><label class="checkbox-inline checkbox-sm"><input ng-model="ngModel.hideForScreenSize.lg" type="checkbox"/><span translate="" translate-context="screen size">Desktop</span><span> (lg)</span></label></div></div><div class="settings well well-sm"><div ng-switch="ngModel.type"><div class="form-group" ng-switch-when="contacts"><label translate="">Show contacts</label><select class="form-control input-sm" ng-model="ngModel.data"><option value="" translate="">All</option><option value="tel" translate="">Phones only</option><option value="email" translate="">Emails only</option><option value="tel-skype-whatsapp" translate="">Phones, WhatsApp/Viber, Skype</option></select></div><div class="form-group" ng-switch-when="directive"><label translate="translate">Custom Directive</label><select class="form-control input-sm" ng-model="ngModel.data"><option value="ui-lang-picker-for-navbar" translate=""> Language picker</option><option value="uic-currency-picker-for-navbar" translate="">Currency picker</option><option value="uice-logout-for-navbar" translate="">Logout button</option><option ng-repeat="(directiveName, conf) in $cms.navbarCustomDirectives" value="{{directiveName}}">{{conf.description || directiveName}}</option></select></div><div class="form-group" ng-switch-when="href"><label translate="translate">Link</label><input class="form-control input-sm" ng-model="ngModel.data"/></div><div class="form-group" ng-switch-when="action"><label translate="translate">Site action</label><select class="form-control input-sm" ng-model="ngModel.data" ng-options="key as getTitleForAction(key,val) for (key,val) in options.customSiteActions"></select></div><div class="form-group" ng-switch-when="ui-sref:generator:article-category"><label translate="translate">Select category</label><select class="form-control input-sm" ng-model="ngModel.data" ng-options="ac_item.id as (ac_item.title | l10n) for ac_item in $models.CmsCategoryForArticle.items"></select></div><div class="form-group" ng-switch-when="ui-sref:app.articles.details"><label translate="translate">Select article</label><select class="form-control input-sm" ng-model="ngModel.data" ng-options="a_item.id as (a_item.title | l10n) for a_item in $models.CmsArticle.items | filter: {isPublished:true}"></select></div><div class="form-group" ng-switch-when="ui-sref:app.galleries.details"><label translate="translate">Select picture album</label><select class="form-control input-sm" ng-model="ngModel.data" ng-options="a_item.id as (a_item.title | l10n) for a_item in $models.CmsPictureAlbum.items | filter: {isPublished:true}"></select></div><div class="form-group" ng-switch-when="ui-sref:anchor"><label translate="">Select anchor</label><select class="form-control input-sm" ng-model="ngModel.data" ng-options="key as getTitleForAction(key,val) for (key,val) in options.customSiteAnchors"></select></div><div class="form-group" ng-switch-when="ui-sref"><div ng-hide="staticPages.length"><label translate="translate">Type angular ui.route name</label><input class="form-control input-sm" ng-model="ngModel.data"/></div><div ng-show="staticPages.length"><label translate="">Select static page</label><select class="form-control input-sm" ng-model="ngModel.data" ng-options="page.data as page.label   for page in staticPages"></select></div></div><div class="form-group" ng-switch-when="file"><div class="form-group"><label translate="">Select file</label><uice-file-id-picker ng-model="ngModel.data" cms-model-item="cmsModelItem" input-size="sm"></uice-file-id-picker></div></div></div><div class="form-group" ng-show="ngModel.type==\'ui-sref\' || ngModel.type==\'ui-sref:app.articles.details\' || ngModel.type==\'ui-sref:app.galleries.details\' || ngModel.type==\'href\'"><div class="checkbox"><label><input type="checkbox" ng-model="ngModel.attrs.target" ng-true-value="\'_blank\'" ng-false-value="\'\'"/><span translate="translate">Open in a new window or tab</span></label></div></div><div ng-show="ngModel.type!=\'directive\' &amp;&amp; ngModel.type!=\'contacts\'"><div class="form-group"><label translate="translate">Title</label><div class="input-group"><input class="form-control input-sm" ng-model="ngModel.title[lang]"/><div class="input-group-btn"><uic-lang-picker ng-model="lang" display="dropdown" size="sm"></uic-lang-picker></div></div></div><div class="form-group"><label translate="translate">Icon</label><uice-fa-icon-picker ng-model="ngModel.html.pre"></uice-fa-icon-picker></div></div></div><div class="settings well well-sm" ng-show="allowSubitems==\'true\' &amp;&amp; (!ngModel.type || ngModel.type==\'\')"><label translate="translate">Items</label><ul class="list-group"><li class="list-group-item" ng-repeat="item in ngModel.items"><span class="title">{{getTitle(item)}}</span><div class="actions"><div class="btn-group"><button class="btn btn-xs btn-default" ng-disabled="$last" ng-click="onMoveItemClick($index, 1)"><div class="fas fa-arrow-down"></div></button><button class="btn btn-xs btn-default" ng-disabled="$first" ng-click="onMoveItemClick($index, -1)"><div class="fas fa-arrow-up"></div></button><button class="btn btn-xs btn-default" ng-click="onItemEditClick(item, $index)"><span class="fas fa-pencil-alt"></span><span translate="translate">Edit</span></button><button class="btn btn-xs btn-danger" ng-click="onItemDeleteClick($index)"><span class="fas fa-trash-alt"></span></button></div></div></li><li class="list-group-item list-group-item-success" ng-click="addItem()" style="cursor:pointer;"><span class="fas fa-plus"></span><span translate="translate">Add new item</span></li></ul></div>' + '')
    };
  }]);

}).call(this);
;
(function() {
  angular.module('ui.cms.editable').service('uiceNavbarNavItemEditorModal', ["$uibModal", function($uibModal) {
    var modalController;
    modalController = function($scope, $uibModalInstance, item, options) {
      $scope.item = angular.copy(item);
      $scope.options = options;
      $scope.cancel = function() {
        return $uibModalInstance.dismiss('cancel');
      };
      return $scope.ok = function() {
        return $uibModalInstance.close($scope.item);
      };
    };
    this.open = function(item, options) {
      var result;
      result = $uibModal.open({
        animation: true,
        template: ('/client/cms_editable_app/_directives/uiceNavbarNavEditor/itemEditModal.html', '<div class="modal-header"><h4 translate="translate">Menu item</h4></div><div class="modal-body"><uice-navbar-nav-item-editor ng-model="item" static-pages="options.staticPages" cms-model-item="options.cmsModelItem"></uice-navbar-nav-item-editor></div><div class="modal-footer"><button class="btn btn-success" ng-click="ok()" translate="translate">OK</button><button class="btn btn-default" ng-click="cancel()" translate="translate">Cancel</button></div>' + ''),
        controller: ['$scope', '$uibModalInstance', 'item', 'options', modalController],
        size: 'md',
        resolve: {
          item: function() {
            return item;
          },
          options: function() {
            return options;
          }
        }
      });
      return result.result;
    };
    return this;
  }]);

}).call(this);
;
(function() {
  angular.module('ui.cms.editable').directive('uiceNoItems', function() {
    return {

      /**
      *   @ngdoc directive
      *   @name ui.cms.editable.directive:uiceNoItems
      *   @description плашка с указанием о том, что объектов нет, или о том, что
      *       поиск по запросу/фильтрации не дал результатов.
      *       Предпологается, что плашка отображается когда действительно объектов нет.
      *       Варианты отображения:
      *       <ul>
      *           <li><b>searchTerm и isFiltered пустые или false</b> - уведомление о том, что объектов нет</li>
      *           <li><b>searchTerm не пустой, а isFiltered == false</b> - уведомление о том, что поиск по словам не дал результатов</li>
      *           <li><b>searchTerm не пустой, а isFiltered == true, или searchTerm пустой, а isFiltered == true</b> - уведомление о том, что поиск не дал результатов</li>
      *       </ul>
      *       Укажите функцию для сброса фильтра, и виджет отрисует кнопку сброса.
      *   @restrict E
      *   @param {string=} searchTerm (ссылка) текст по которому производится поиск
      *   @param {boolean=} isFiltered (ссылка) отфильтрованы ли объекты по каким-то другим критериям
      *   @param {function=} resetFilter (ссылка) функция, по которой можно проводить сброс фильтров
      *   @param {boolean=} ngDisabled (ссылка) флаг доступности тега
      *   @example
      *       <pre>
      *           $scope.mySearchTerm = ""
      *           $scope.mySearchTerm2 = "hello"
      *           $scope.myIsFiltered = false
      *           $scope.reset = ()->
      *               alert('Reset filter!')
      *       </pre>
      *       <pre>
      *           //- pug
      *           uice-no-items(search-term="mySearchTerm", is-filtered="myIsFiltered")
      *           uice-no-items(search-term="mySearchTerm2", is-filtered="myIsFiltered", reset-filter="reset()")
      *       </pre>
      *       <example module="DocExampleApp">
      *           <file name="script.js">
      *               mkDocCtrl('uiceNoItemsCtrl', {
      *                   mySearchTerm: '',
      *                   mySearchTerm2: 'hello',
      *                   myIsFiltered: false,
      *                   reset: function(){
      *                       alert('Reset filter!')
      *                   }
      *               });
      *           </file>
      *           <file name="index.html">
      *               <div ng-controller="uiceNoItemsCtrl">
      *                   <div class='row'>
      *                       <uice-no-items search-term='mySearchTerm', is-filtered="isFiltered" class='col-md-50'>
      *                       </uice-no-items>
      *                       <uice-no-items search-term='mySearchTerm2', is-filtered="isFiltered" reset-filter="reset()" class='col-md-50'>
      *                       </uice-no-items>
      *                   </div>
      *               </div>
      *           </file>
      *       </example>
       */
      restrict: 'E',
      scope: {
        searchTerm: '=?',
        isFiltered: '=?',
        resetFilter: '&?',
        ngDisabled: '=?'
      },
      controller: ["$scope", function($scope) {}],
      template: ('/client/cms_editable_app/_directives/uiceNoItems/uiceNoItems.html', '<div ng-switch="!searchTerm &amp;&amp; !isFiltered"><div class="uice-no-items-content" ng-switch-when="true"><div class="fas fa-info-circle"></div><h3 translate="">There\'s nothing in here</h3></div><div class="uice-no-items-content" ng-switch-when="false"><div class="fas fa-frown"></div><h3 ng-switch="!!(searchTerm &amp;&amp; !isFiltered)"><span ng-switch-when="true" translate="">Your search - <b>"{{searchTerm}}"</b> - did not match any documents</span><span ng-switch-when="false" translate="">Your search did not match any documents</span></h3><div class="text-center" ng-show="resetFilter"><button class="btn btn-default" style="pointer-events: all;" ng-click="resetFilter()" ng-disabled="ngDisabled" translate="">Reset filter</button></div></div></div>' + '')
    };
  });

}).call(this);
;
(function() {
  angular.module('ui.cms.editable').directive('uiceSocialLinksEditor', function() {
    return {

      /**
      *   @ngdoc directive
      *   @name ui.cms.editable.directive:uiceSocialLinksEditor
      *   @description редактор массива ссылок на социальные сети
      *
      *   @restrict E
      *   @param {array<string>} ngModel (ссылка) модель array. Напр.: ['vk.com/fake', 'facebook.com/ddd'].
      *   @param {function=} onChange (функция) функция которая вызывается при изменении модели
       */
      restrict: 'E',
      scope: {
        ngModel: '=',
        onChange: '&?'
      },
      controller: ["$scope", function($scope) {
        return $scope.getUrl = function(url) {
          if (url.indexOf('http://') !== 0 && url.indexOf('https://') !== 0) {
            return "http://" + url;
          }
          return url;
        };
      }],
      template: ('/client/cms_editable_app/_directives/uiceSocialLinksEditor/uiceSocialLinksEditor.html', '<uice-string-list-editor class="no-list-style" ng-model="ngModel" input-type="text" input-placeholder="{{\'Type link to your social media page\' | translate}}" add-button-text="{{\'Add link\' | translate}}" on-change="onChange()"><uic-site-brand-a href="{{$origScope.getUrl($item)}}" target="_blank" show-text="true"></uic-site-brand-a></uice-string-list-editor>' + '')
    };
  });

}).call(this);
;
(function() {
  angular.module('ui.cms.editable').directive('uiceStringListEditor', function() {
    return {

      /**
      *   @ngdoc directive
      *   @name ui.cms.editable.directive:uiceStringListEditor
      *   @description редактор массива строк(или других данных)
      *
      *   @restrict E
      *   @param {array<>} ngModel (ссылка) модель array. Напр.: ['hello', 'world'].
      *   @param {string=} inputType (значение) тип данных (number, text, email)
      *   @param {string} inputPlaceholder (значение)
      *   @param {boolean=} [disableMoveActions=false] (значение)
      *   @param {string=} addButtonText (значение) текст на кнопке добавления значения
      *   @param {function=} onChange (функция) функция которая вызывается при изменении модели
      *   @param {number=} maxlength (значение) максимальное количество символов нового добавляемого элемента
      *   @param {boolean=} ngDisabled (ссылка) флаг доступности тега
       */
      restrict: 'E',
      transclude: true,
      scope: {
        ngModel: '=',
        hideInput: '@?',
        inputType: '@?',
        inputPlaceholder: '@?',
        disableMoveActions: '@?',
        disableRemoveAction: '@?',
        addButtonText: '@',
        onChange: '&?',
        ngDisabled: '=?',
        maxlength: '@?'
      },
      controller: ["$scope", "$attrs", "H", function($scope, $attrs, H) {
        var setInitialData;
        $scope.$origScope = H.findOrigScope($scope, $attrs);
        if ($scope.disableRemoveAction === 'true') {
          $scope._disableRemoveAction = true;
        } else {
          $scope._disableRemoveAction = false;
        }
        setInitialData = function() {
          if (!$scope.inputType || $scope.inputType === 'text' || $scope.inputType === 'email') {
            $scope.newData = '';
          }
          if ($scope.inputType === 'number') {
            return $scope.newData = 0;
          }
        };
        $scope.add = function() {
          if (!($scope.ngModel instanceof Array)) {
            $scope.ngModel = [];
          }
          $scope.ngModel.push($scope.newData);
          setInitialData();
          if ($scope.onChange) {
            return $scope.onChange($scope.ngModel);
          }
        };
        $scope.remove = function(index) {
          $scope.ngModel.splice(index, 1);
          if ($scope.onChange) {
            return $scope.onChange($scope.ngModel);
          }
        };
        return $scope.move = function(index, diff) {
          $scope.ngModel.move(index, index + diff);
          if ($scope.onChange) {
            return $scope.onChange($scope.ngModel);
          }
        };
      }],
      template: ('/client/cms_editable_app/_directives/uiceStringListEditor/uiceStringListEditor.html', '<form name="uiceStringListForm"><div class="form-group nomargin"><span class="error" ng-show="uiceStringListForm.input.$error[inputType] &amp;&amp; !hideInput">[invalid {{inputType || \'text\'}}]</span><div class="input-group" ng-show="!hideInput"><input class="form-control" type="{{inputType || \'text\'}}" ng-model="newData" uic-enter-press="add()" placeholder="{{inputPlaceholder}}" ng-disabled="ng-disabled" maxlength="{{maxlength || -1}}" name="input"/><div class="input-group-btn"><button class="btn btn-success" ng-disabled="!newData || uiceStringListForm.$invalid || ngDisabled" ng-click="add()"><span class="fas fa-plus fa-nopadding-xs"></span><span class="hidden-xxs" ng-switch="!!addButtonText"><span ng-switch-when="true">{{addButtonText}}</span><span ng-switch-when="false" translate="" translate-context="append">Add</span></span></button></div></div></div><table><tbody><tr ng-repeat="$item in ngModel   track by $index"><td uic-transclude="" uic-transclude-bind="{item:$item, $origScope: $origScope}"></td><td class="text-right td-actions"><div class="btn-group" ng-if="!disableMoveActions"><button class="btn btn-default btn-xs" ng-click="move($index, -1)" ng-disabled="$first"><div class="fas fa-arrow-up nopadding"></div></button><button class="btn btn-default btn-xs" ng-click="move($index, 1)" ng-disabled="$last"><div class="fas fa-arrow-down nopadding"></div></button><button class="btn btn-danger btn-xs" ng-click="remove($index)" ng-disabled="ngDisabled" ng-if="!_disableRemoveAction"><span class="fas fa-trash-alt"></span><span translate="translate">Remove</span></button></div><button class="btn btn-danger btn-xs" ng-click="remove($index)" ng-disabled="ngDisabled" ng-if="disableMoveActions &amp;&amp; !_disableRemoveAction"><span class="fas fa-trash-alt"></span><span translate="translate">Remove</span></button></td></tr></tbody></table></form>' + '')
    };
  });

}).call(this);
;
(function() {
  angular.module('ui.cms.editable').directive('uiceTableView', ["$compile", "$window", "$timeout", "$cms", "H", "$tableView", "$currentUser", function($compile, $window, $timeout, $cms, H, $tableView, $currentUser) {
    var ATTRS;
    ATTRS = {
      'ng-disabled': 'ngDisabled',
      items: '$parent.items',
      options: 'options',
      "class": 'ag-bootstrap',
      'search-term': '$parent.searchTerm',
      'on-item-click': 'onItemClick',
      'on-items-select': 'onItemsSelect',
      'sort-field': '{{sortField}}',
      'force-lang': '$parent.forceLang'
    };
    return {

      /**
      *   @ngdoc directive
      *   @name ui.cms.editable.directive:uiceTableView
      *   @description
      *
      *   @restrict E
      *   @param {Array} items (ссылка) массив элементов
      *   @example
      *       <pre>
      *           $scope.
      *       </pre>
      *       <pre>
      *           //- pug
      *       </pre>
      *       <example module="DocExampleApp">
      *           <file name="script.js">
      *               mkDocCtrl('uiceTableViewCtrl', {
      *
      *               });
      *           </file>
      *           <file name="index.html">
      *               <div ng-controller="uiceTableViewCtrl">
      *
      *               </div>
      *           </file>
      *       </example>
       */
      restrict: 'E',
      scope: {
        items: '=',
        forceLang: '=?',
        searchTerm: '=?',
        onItemClick: '&?',
        onItemsSelect: '&?',
        itemsPerPage: '@?',
        queryFilter: '=?',
        sortField: '@?'
      },
      link: function($scope, $element, $attrs) {
        var attrs, columnDefs, container, countItemsFn, fillTElementHeight, fillViewport, getDefaultAttrs, getItemsPerPage, hasIsEnabled, html, i, j, k, len, len1, loadItemsFn, options, ref, screen, screens, serverPagination, suffix, t, tElement, template;
        columnDefs = $scope.$parent.$eval($attrs.columns) || $attrs.columns;
        if (typeof columnDefs === 'string') {
          suffix = columnDefs;
          columnDefs = angular.copy($tableView.getColumnsConfig(columnDefs));
        } else {
          suffix = $attrs.columns || '-';
        }
        if (!columnDefs.lg) {
          throw "uiceTableView: Can't find 'lg' columns definition for table in columns attribute=(";
        }

        /*
            поддержка серверной пагинации
         */
        if ($attrs.loadItemsFn) {
          loadItemsFn = $scope.$parent.$eval($attrs.loadItemsFn.split('(')[0]);
          if (!angular.isFunction(loadItemsFn)) {
            loadItemsFn = null;
          }
        }
        if ($attrs.countItemsFn) {
          countItemsFn = $scope.$parent.$eval($attrs.countItemsFn.split('(')[0]);
          if (!angular.isFunction(countItemsFn)) {
            countItemsFn = null;
          }
        }
        if ((loadItemsFn && !countItemsFn) || (!loadItemsFn && countItemsFn)) {
          throw "uiceTableView: you should set loadItemsFn and countItemsFn as a function (or don't use it)! ";
        }
        if (loadItemsFn && countItemsFn) {
          $scope.loadItemsFn = loadItemsFn;
          $scope.countItemsFn = countItemsFn;
          getDefaultAttrs = function() {
            var attrs;
            attrs = angular.copy(ATTRS);
            attrs.suffix = suffix;
            attrs['load-items-fn'] = 'loadItemsFn';
            attrs['count-items-fn'] = 'countItemsFn';
            attrs['query-filter'] = '$parent.queryFilter';
            delete attrs.items;
            if (!$scope.onItemClick) {
              delete attrs['on-item-click'];
            }
            return attrs;
          };
        } else {
          getDefaultAttrs = function() {
            var attrs;
            attrs = angular.copy(ATTRS);
            attrs.suffix = suffix;
            if (!$scope.onItemClick) {
              delete attrs['on-item-click'];
            }
            return attrs;
          };
        }
        getItemsPerPage = function(defaultValue) {
          var itemsPerPage;
          if (!$scope.itemsPerPage && defaultValue) {
            return defaultValue;
          }
          itemsPerPage = JSON.parse($scope.itemsPerPage);
          if (angular.isArray(itemsPerPage)) {
            return itemsPerPage;
          }
          if (isNaN(itemsPerPage) || itemsPerPage < 1) {
            itemsPerPage = 15;
          }
          return itemsPerPage;
        };
        template = [];
        $scope.columnsDef = [];
        $scope.toolbarsDef = [];
        $scope.optionsDef = [];
        $scope.$cms = $cms;
        $scope.$currentUser = $currentUser;
        $scope.$origScope = H.findOrigScope($scope, $attrs);
        serverPagination = !!loadItemsFn && !!countItemsFn;
        fillViewport = {};
        ref = ['lg', 'md', 'sm', 'xs'];
        for (i = j = 0, len = ref.length; j < len; i = ++j) {
          screen = ref[i];
          if (columnDefs[screen]) {
            attrs = getDefaultAttrs();
            if (!$attrs.sortField) {
              attrs['sort-field'] = columnDefs[screen].sortField;
            }
            attrs['items-per-page'] = getItemsPerPage(columnDefs[screen].itemsPerPage);
            $scope.columnsDef.push(columnDefs[screen].columns);
            $scope.toolbarsDef.push(columnDefs[screen].toolbar);
            options = angular.copy(columnDefs[screen].options || {});
            options.serverPagination = serverPagination;
            $scope.optionsDef.push(options);
            if (options.fillViewport) {
              attrs["class"] = (attrs["class"] || '') + ' h-100';
              fillViewport[screen] = true;
            }
            attrs.columns = "columnsDef[" + ($scope.columnsDef.length - 1) + "]";
            attrs.toolbar = "toolbarsDef[" + ($scope.toolbarsDef.length - 1) + "]";
            attrs.options = "optionsDef[" + ($scope.optionsDef.length - 1) + "]";
            template.push({
              screen: [screen],
              render: columnDefs[screen].render || $tableView.getDefaultRenderNameForScreenSize(screen),
              attrs: H.convert.objToHtmlProps(attrs)
            });
          } else {
            template[template.length - 1].screen.push(screen);
            if ($scope.optionsDef[$scope.optionsDef.length - 1].fillViewport) {
              fillViewport[screen] = true;
            }
          }
        }
        html = "";
        for (k = 0, len1 = template.length; k < len1; k++) {
          t = template[k];
          screens = t.screen.map(function(size) {
            return "$cms.screenSize=='" + size + "'";
          });
          html += "<div ng-if=\"" + (screens.join(' || ')) + "\" table-view-wrapper-" + t.render + " " + t.attrs + ">\n</div>";
        }
        $element.html(html);
        $compile($element.contents())($scope);
        if (angular.isEmpty(fillViewport)) {
          return;
        }
        container = document.body.getElementsByTagName('ui-view-animation-container');
        tElement = $element[0];
        fillTElementHeight = function() {
          var e, w;
          if (fillViewport[$cms.screenSize]) {
            e = tElement.getBoundingClientRect();
            w = H.info.viewport.getSize();
            tElement.style.height = (w.height - e.top - 15) + "px";
          } else {
            tElement.style.height = 'auto';
          }
        };
        angular.element($window).on('resize', fillTElementHeight);
        $scope.$on('$destroy', function() {
          angular.element($window).off('resize', fillTElementHeight);
          if (container) {
            return container.removeClass('fill-viewport');
          }
        });
        $timeout(fillTElementHeight, 0);
        $timeout(fillTElementHeight, 1000);
        if (container) {
          container = angular.element(container);
          $scope.$watch('$cms.screenSize', function(screenSize, oldValue) {
            if (fillViewport[screenSize]) {
              container.addClass('fill-viewport');
            } else {
              container.removeClass('fill-viewport');
            }
          });
        }
        hasIsEnabled = $scope.columnsDef.some(function(_columns) {
          if (!_columns || !angular.isFunction(_columns.some)) {
            return false;
          }
          return _columns.some(function(c) {
            return angular.isFunction(c.isEnabled);
          });
        });
        if (!hasIsEnabled) {
          hasIsEnabled = $scope.toolbarsDef.some(function(_columns) {
            if (!_columns || !angular.isFunction(_columns.some)) {
              return false;
            }
            return _columns.some(function(c) {
              return angular.isFunction(c.isEnabled);
            });
          });
        }
        if (hasIsEnabled) {
          return $scope.$watch('$currentUser.id', function(id, oldValue) {
            if (id === oldValue) {
              return;
            }
            $element.html("");
            $compile($element.contents())($scope);
            $timeout(function() {
              $element.html(html);
              return $compile($element.contents())($scope);
            }, 0);
          });
        }
      },
      template: ""
    };
  }]);

}).call(this);
;
(function() {
  angular.module('ui.cms.editable').config(["$injector", "$directiveOverrideProvider", function($injector, $directiveOverrideProvider) {

    /**
    *   @ngdoc directive
    *   @name ui.cms.editable.directive:uiceTabset
    *   @description Директива подобная uib-tabset, также содержит
    *       такие же директивы для задание табов, заголовков и т.п.
    *       Разница меж uibTabset и uiceTabset в том, что в uiceTabset
    *       табы распологаются слева, а для мобильных экранов эти табы вообще
    *       сворачиваются в кнопку-дропдаун
    *   @restrict E
    *   @param {number} active (ссылка) активный таб
    *   @example
    *       <pre>
    *           $scope.tabs = [
    *               { title:'Dynamic Title 1', content:'Dynamic content 1' },
    *               { title:'Dynamic Title 2', content:'Dynamic content 2', disabled: true }
    *           ]
    *       </pre>
    *       <pre>
    *           //- pug
    *           uice-tabset.sm(active="active")
    *               uice-tabset-tab(index="0", heading="Static title") Static content
    *               uice-tabset-tab(index="$index + 1", ng-repeat="tab in tabs", heading="{{tab.title}}", disable="tab.disabled")
    *                   | {{tab.content}}
    *               uice-tabset-tab(index="3", select="alertMe()")
    *                   uice-tabset-tab-heading
    *                       i.fas.fa-car
    *                       |  Alert! {{1+2}}
    *                   |     I've got an HTML heading, and a select callback. Pretty cool!
    *       </pre>
    *       <example module="DocExampleApp">
    *           <file name="script.js">
    *               mkDocCtrl('uiceTabsetCtrl', {
    *                   tabs: [
    *                       { title:'Dynamic Title 1', content:'Dynamic content 1' },
    *                       { title:'Dynamic Title 2', content:'Dynamic content 2', disabled: true }
    *                   ],
    *                   alertMe: function(){alert();}
    *               });
    *           </file>
    *           <file name="index.html">
    *               <div ng-controller="uiceTabsetCtrl">
    *                    <uice-tabset class="sm" active="active">
    *                      <uice-tabset-tab index="0" heading="Static title">Static content</uice-tabset-tab>
    *                      <uice-tabset-tab index="$index + 1" ng-repeat="tab in tabs" heading="{{tab.title}}" disable="tab.disabled">{{tab.content}}</uice-tabset-tab>
    *                      <uice-tabset-tab index="3" select="alertMe()">
    *                        <uice-tabset-tab-heading><i class="fas fa-car"></i> Alert! {{1+2}}</uice-tabset-tab-heading>    I've got an HTML heading, and a select callback. Pretty cool!
    *                      </uice-tabset-tab>
    *                    </uice-tabset>
    *               </div>
    *           </file>
    *       </example>
     */
    return $directiveOverrideProvider.duplicate('uibTab', 'uiceTabsetTab', {
      require: '^uiceTabset',
      transclude: true,
      template: ('/client/cms_editable_app/_directives/uiceTabset/uiceTabsetTab.html', '<li class="uib-tab nav-item" ng-class="[{active: active, disabled: disabled}, classes]"><a class="nav-link" href="" ng-click="select($event)" uice-tabset-tab-heading-transclude="">{{heading}}</a></li>' + '')
    }).duplicate('uibTabHeadingTransclude', 'uiceTabsetTabHeadingTransclude', {
      require: '^uiceTabsetTab'
    }).duplicate('uibTabset', 'uiceTabset', {
      require: null,
      bindToController: {
        active: '=?',
        tabsize: '@?',
        dropdownBtnClass: '@?'
      },
      controller: [
        '$scope', '$controller', '$element', '$compile', '$timeout', function($scope, $controller, $element, $compile, $timeout) {
          var ctrl, dropdown, getHeadingHtml, renderDropdownElement;
          dropdown = null;
          ctrl = $controller('UibTabsetController', {
            $scope: $scope
          });
          if (!ctrl.tabsize) {
            ctrl.tabsize = '200px';
          }
          ctrl._select = function(index, evt, disabled) {
            if (!disabled) {
              return ctrl.select(index, evt);
            }
          };
          getHeadingHtml = function(tab) {
            var text;
            text = tab.tab.heading;
            if (!text && tab.tab.headingElement) {
              text = tab.tab.headingElement.innerHTML;
            }
            return text || '';
          };
          renderDropdownElement = function(tabs) {
            var html, i, j, len, ref, tab, text;
            if (!$element && !$element.length) {
              return;
            }
            if (!dropdown) {
              dropdown = $element[0].getElementsByClassName('uice-tabset-dropdown-menu');
              if (!dropdown.length) {
                return;
              }
              dropdown = dropdown[0];
            }
            html = "";
            ref = tabs || [];
            for (i = j = 0, len = ref.length; j < len; i = ++j) {
              tab = ref[i];
              text = getHeadingHtml(tab);
              if (!text) {
                continue;
              }
              html += "<li ng-class='[{active: tabset.tabs[" + i + "].tab.active, disabled: tabset.tabs[" + i + "].tab.disabled}, classes]'>\n    <a href='' ng-click='tabset._select(" + i + ", $event, tabset.tabs[" + i + "].tab.disabled)'>\n        " + text + "\n    </a>\n</li>";
            }
            dropdown.innerHTML = html;
            $compile(dropdown)($scope);
            ctrl.contentMinHeight = $element[0].getElementsByClassName('uice-tabset-navs')[0].offsetHeight;
          };
          $scope.$watchCollection('tabset.tabs', function(tabs, oldValue) {
            $timeout(function() {
              return renderDropdownElement(ctrl.tabs);
            }, 0);
          });
          $scope.$watch('tabset.active', function(active, oldValue) {
            if (ctrl.tabs && ctrl.tabs.length) {
              $scope.activeHeadingHtml = getHeadingHtml(ctrl.tabs[ctrl.active]);
              if (!$scope.activeHeadingHtml) {
                $timeout(function() {
                  return $scope.activeHeadingHtml = getHeadingHtml(ctrl.tabs[ctrl.active]);
                }, 0);
              }
            }
          });
          return ctrl;
        }
      ],
      template: ('/client/cms_editable_app/_directives/uiceTabset/uiceTabset.html', '<div class="uice-tabset"><div class="uice-tabset-dropdown visible-xs-block" uib-dropdown=""><button class="btn btn-{{tabset.dropdownBtnClass || \'default\'}}" uib-dropdown-toggle=""><span uic-bind-html="activeHeadingHtml"></span><span> </span><span class="caret"></span></button><ul class="dropdown-menu uice-tabset-dropdown-menu" dropdown-menu=""></ul></div><div><ul class="nav nav-tabs nav-stacked nav-tabs-left uice-tabset-navs" ng-transclude="" uib-dropdown-menu="" style="width: {{tabset.tabsize}};"></ul><div class="tab-content" style="margin-left: {{tabset.tabsize}};"><div class="tab-pane" ng-repeat="tab in tabset.tabs" ng-class="{active: tabset.active === tab.index}" style="min-height:{{tabset.contentMinHeight}}px;" uice-tabset-tab-content-transclude="tab"></div></div></div></div>' + '')
    });
  }]).directive('uiceTabsetTabContentTransclude', function() {
    var isTabHeading;
    isTabHeading = function(node) {
      return node.tagName && (node.hasAttribute('uib-tab-heading') || node.hasAttribute('data-uib-tab-heading') || node.hasAttribute('x-uib-tab-heading') || node.tagName.toLowerCase() === 'uice-tabset-tab-heading' || node.tagName.toLowerCase() === 'data-uib-tab-heading' || node.tagName.toLowerCase() === 'x-uib-tab-heading' || node.tagName.toLowerCase() === 'uib:tab-heading');
    };
    return {
      restrict: 'A',
      require: '^uiceTabset',
      link: function(scope, elm, attrs) {
        var tab;
        tab = scope.$eval(attrs.uiceTabsetTabContentTransclude).tab;
        return tab.$transcludeFn(tab.$parent, function(contents) {
          return angular.forEach(contents, function(node) {
            if (isTabHeading(node)) {
              return tab.headingElement = node;
            } else {
              return elm.append(node);
            }
          });
        });
      }
    };
  });

}).call(this);
;
(function() {
  angular.module('ui.cms.editable').factory('$uiceUserHelpers', ["$q", "$location", "$uibModal", "$cms", "CmsUser", function($q, $location, $uibModal, $cms, CmsUser) {
    var confirmUser, forceSetPassword, forceSetPasswordModalController, generateNewPassword, registerUser, resetPasswordByEmail, resetPasswordModalController;
    registerUser = function(userData, options) {
      var defer, exists, fn, params, where;
      userData = angular.copy(userData);
      defer = $q.defer();
      exists = CmsUser.querysetExists || CmsUser.count;
      where = [
        {
          email: userData.email
        }, {
          username: userData.username
        }
      ];
      if (!options) {
        options = {};
      }
      if (!options.verifyHref) {
        options.verifyHref = location.origin + '/confirm/email';
      }
      if (!options.redirectHref) {
        options.redirectHref = '/';
      }
      if (CmsUser.count) {
        fn = CmsUser.count;
        params = {
          where: {
            or: where
          }
        };
      }
      if (CmsUser.querysetExists) {
        fn = CmsUser.querysetExists;
        params = {
          filter: {
            where: {
              or: where
            }
          }
        };
      }
      if (userData.password2) {
        delete userData.password2;
      }
      fn(params, function(data) {
        var err;
        if (data.exists || data.count) {
          err = {
            status: 422,
            statusCode: 422,
            data: {
              error: {
                name: 'ValidationError',
                status: 422,
                statusCode: 422,
                message: "User with this email or username already exists"
              }
            }
          };
          defer.reject(err);
          return;
        }
        CmsUser.create(options, userData, defer.resolve, defer.reject);
      }, defer.reject);
      return defer.promise;
    };
    confirmUser = function(stateParams) {
      var params;
      stateParams = stateParams || $location.search();
      params = {
        uid: stateParams.userId,
        redirect: stateParams.redirectHref || location.origin,
        token: stateParams.verificationToken
      };
      return CmsUser.confirm(params).$promise;
    };
    resetPasswordModalController = function($scope, $uibModalInstance, options) {
      $scope.options = options;
      $scope.cancel = function() {
        return $uibModalInstance.dismiss('cancel');
      };
      return $scope.ok = function() {
        $scope.error = null;
        CmsUser.reset({
          email: $scope.options.email
        }, function() {
          return $scope.sended = true;
        }, function() {
          return $scope.error = 'no-such-email';
        });
      };
    };
    resetPasswordByEmail = function(options) {
      var result;
      result = $uibModal.open({
        animation: true,
        backdrop: 'static',
        keyboard: false,
        windowClass: 'uice-user-helpers-reset-password-by-email',
        template: ('/client/cms_editable_app/_helpers/resetPasswordByEmail-modal.html', '<div class="modal-header"><h4 class="modal-title" translate="">Reset password</h4></div><form class="modal-body" name="form" ng-switch="!!sended"><div ng-switch-when="false"><div class="form-group"><label translate="">Your email</label><input class="form-control" ng-model="options.email" type="email" required="required" autofocus=""/></div><uic-alert type="danger" ng-show="error == \'no-such-email\'"><b translate="">We can\'t find {{options.email}} in our database. Please, give us email that associated with your account on this site</b></uic-alert></div><div ng-switch-when="true"><label ng-switch="options.loginProtection == \'s\'"><span ng-switch-when="false" translate="">We\'ve sent you email with instructions on resetting your password</span><span ng-switch-when="true" translate="">We\'ve sent you email with instructions on resetting your password and changing the phone numbers</span></label><div class="text-center"><button class="btn btn-primary" translate="" ng-click="cancel()" style="min-width: 4em;">Ok</button></div></div></form><div class="modal-footer" ng-hide="sended"><button class="btn btn-primary" ng-click="ok()" translate="" ng-disabled="form.$invalid">Reset</button><button class="btn btn-default" ng-click="cancel()" translate="">Cancel</button></div>' + ''),
        controller: ['$scope', '$uibModalInstance', 'options', resetPasswordModalController],
        size: 'sm',
        resolve: {
          options: function() {
            return options || {};
          }
        }
      });
      return result.result;
    };
    forceSetPasswordModalController = function($scope, $uibModalInstance, ngModel, options) {
      $scope.ngModel = ngModel;
      $scope.options = options;
      $scope.options.passwordType = 'password';
      $scope.toggleShowPassword = function() {
        if ($scope.options.passwordType === 'password') {
          $scope.options.passwordType = 'text';
        } else {
          $scope.options.passwordType = 'password';
        }
      };
      $scope.generatePassword = function() {
        $scope.password = generateNewPassword();
        $scope.password2 = $scope.password + '';
      };
      $scope.cancel = function() {
        return $uibModalInstance.dismiss('cancel');
      };
      return $scope.ok = function() {
        $scope.options.loading = true;
        CmsUser.forceSetPassword({
          id: ngModel.id
        }, {
          password: $scope.password
        }, function() {
          $cms.showNotification('New password is set');
          $uibModalInstance.close();
        }, function() {
          $scope.options.error = true;
          $scope.options.loading = false;
        });
      };
    };
    forceSetPassword = function(ngModel, options) {
      var result;
      result = $uibModal.open({
        animation: true,
        backdrop: 'static',
        keyboard: false,
        template: ('/client/cms_editable_app/_helpers/forceSetPassword-modal.html', '<div class="modal-header"><h4 class="modal-title" translate="">New password</h4></div><div class="modal-body"><form name="form" ng-show="!options.loading"><div class="form-group"><label><span translate="translate">Password</span><span class="required">*</span></label><div class="input-group"><input class="form-control" type="{{options.passwordType}}" name="new_password" required="" ng-model="password"/><div class="input-group-btn"><button class="btn btn-default" ng-click="toggleShowPassword()" title="{{\'Toggle password visibility\' | translate}}" ng-class="{active: options.passwordType == \'password\'}"><span class="fas nopadding" ng-class="{\'fa-eye\': options.passwordType == \'text\', \'fa-eye-slash\': options.passwordType == \'password\'}"></span></button><button class="btn btn-warning" ng-click="generatePassword()" title="{{\'Generate password\' | translate}}"><span class="fas fa-unlock-alt nopadding"></span></button></div></div></div><div class="form-group"><label><span translate="translate">Repeat password</span><span class="required">*</span><small class="error" ng-show="form.new_password2.$error.compareEqual" translate="">Password doesn\'t match</small></label><input class="form-control" type="{{options.passwordType}}" name="new_password2" required="" ng-model="password2" compare-equal="password"/></div><p class="text-danger" ng-show="options.error" translate="">Can\'t set user password. Try again later</p></form><div class="with-preloader" ng-show="options.loading" style="height: 8em;"><div class="preloader"></div></div></div><div class="modal-footer" ng-hide="options.loading"><button class="btn btn-primary" ng-click="ok()" translate="" ng-disabled="form.$invalid" translate-context="verb">Set</button><button class="btn btn-default" ng-click="cancel()" translate="">Cancel</button></div>' + ''),
        windowClass: 'uice-user-helpers-force-set-password',
        controller: ['$scope', '$uibModalInstance', 'ngModel', 'options', forceSetPasswordModalController],
        size: 'sm',
        resolve: {
          ngModel: function() {
            return ngModel;
          },
          options: function() {
            return options || {};
          }
        }
      });
      return result.result;
    };
    generateNewPassword = function(length) {
      var charset, i, n, retVal;
      length = length || 8;
      charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789@#!?{}$";
      retVal = "";
      i = 0;
      n = charset.length;
      while (i < length) {
        retVal += charset.charAt(Math.floor(Math.random() * n));
        ++i;
      }
      return retVal;
    };
    return {
      registerUser: registerUser,
      confirmUser: confirmUser,
      resetPasswordByEmail: resetPasswordByEmail,
      forceSetPassword: forceSetPassword,
      generateNewPassword: generateNewPassword
    };
  }]);

}).call(this);
;
(function() {
  angular.module('ui.cms.editable').service('uiceConfirmModal', ["$uibModal", function($uibModal) {
    var modalController;
    modalController = function($scope, $uibModalInstance, options) {
      $scope.options = options;
      $scope.cancel = function() {
        return $uibModalInstance.dismiss('cancel');
      };
      return $scope.confirm = function() {
        return $uibModalInstance.close();
      };
    };
    this.open = function(options) {
      var result;
      result = $uibModal.open({
        animation: true,
        template: ('/client/cms_editable_app/_modals/uiceConfirmModal/modal.html', '<div class="modal-body"><p uic-bind-html="options.title"></p><div uic-bind-html="options.body"></div></div><div class="modal-footer"><button class="btn {{options.btnClass}}" ng-click="confirm()">{{options.confirmBtnText | translate}}</button><button class="btn btn-default" ng-click="cancel()" translate="translate">Cancel</button></div>' + ''),
        controller: ['$scope', '$uibModalInstance', 'options', modalController],
        size: 'sm',
        resolve: {
          options: function() {
            return options || {};
          }
        }
      });
      return result.result;
    };
    this["delete"] = (function(_this) {
      return function(options) {
        if (options == null) {
          options = {};
        }
        options.btnClass = options.btnClass || 'btn-danger';
        options.confirmBtnText = options.confirmBtnText || 'Delete';
        return _this.open(options);
      };
    })(this);
    return this;
  }]);

}).call(this);
;
(function() {
  angular.module('ui.cms.editable').service('uiceFaIconPickerModal', ["$uibModal", function($uibModal) {
    var modalController;
    modalController = function($scope, $uibModalInstance, $uicIntegrations, H, ngModel, options) {
      var fa4Json;
      $scope.ngModel = ngModel + '';
      $scope.options = options;
      fa4Json = $uicIntegrations.getThirdPartyLibrary('fontawesome5-json');
      fa4Json.load().then(function(data) {
        $scope.faIcons = data[0];
      });
      $scope.cancel = function() {
        return $uibModalInstance.dismiss('cancel');
      };
      return $scope.select = function(icon) {
        return $uibModalInstance.close(icon);
      };
    };
    this.open = function(ngModel, options) {
      var result;
      result = $uibModal.open({
        animation: true,
        windowClass: 'uice-fa-icon-picker-modal',
        template: ('/client/cms_editable_app/_modals/uiceFaIconPicker/modal.html', '<div class="modal-header"><button class="close" ng-click="cancel()"><span>&times;</span></button><h4 class="modal-title" translate="">Select icon</h4></div><form class="modal-body" name="form"><div class="form-group"><uic-search-input ng-model="searchTerm"></uic-search-input></div><div class="icons" ng-repeat="(category, icons) in faIcons"><h5 class="text-success" ng-show="fIcons.length"><b>{{category | translate}}</b></h5><button class="btn btn-default" ng-repeat="icon in fIcons = (icons | filter:searchTerm)" ng-click="select(icon)" ng-class="{\'active btn-primary\': ngModel==icon}"><div class="fa {{ :: icon}}"></div></button></div></form><div class="modal-footer"><button class="btn btn-default" ng-click="cancel()" translate="">Close</button></div>' + ''),
        controller: ['$scope', '$uibModalInstance', '$uicIntegrations', 'H', 'ngModel', 'options', modalController],
        size: 'lg',
        resolve: {
          ngModel: function() {
            return ngModel;
          },
          options: function() {
            return options || {};
          }
        }
      });
      return result.result;
    };
    return this;
  }]);

}).call(this);
;
(function() {
  angular.module('ui.cms.editable').service('uiceFileViewModal', ["$uibModal", "H", "$sce", "$cms", function($uibModal, H, $sce, $cms) {
    var LANGS, modalController;
    LANGS = {
      ru: 'Russian',
      en: 'English',
      cz: 'Czech'
    };
    modalController = function($scope, $uibModalInstance, ngModel, options) {
      var urlParams;
      $scope.ngModel = ngModel;
      $scope.options = options;
      $scope.$cms = $cms;
      if (angular.isObject(ngModel)) {
        $scope.fileType = H.l10nFile.getFileType(ngModel);
        $scope.options.fileUrl = H.l10nFile.getFileUrl(ngModel);
        if ($scope.fileType.includes('image/')) {
          $scope.fileType = 'image';
        } else if ($scope.fileType === 'application/pdf') {
          $scope.fileType = 'pdf';
          if ($scope.options.fileUrl[0] === '/') {
            $scope.options.fileUrl = location.origin + $scope.options.fileUrl;
          }
        }
        $scope.title = ngModel.title;
        $scope.description = ngModel.description;
        if (angular.isEmpty($scope.title)) {
          $scope.title = H.l10nFile.getOriginalName(ngModel);
        }
      } else if (angular.isString(ngModel)) {
        if (ngModel.includes("/render_pdf/")) {
          $scope.fileType = 'pdf';
          $scope.options.fileUrl = ngModel;
          if ($scope.options.fileUrl[0] === '/') {
            $scope.options.fileUrl = location.origin + $scope.options.fileUrl;
          }
          $scope.title = options.title || angular.tr('PDF file');
          $scope.description = options.description || '';
        }
      }
      if ((options.allowedLangs || {}).length) {
        urlParams = H.urlParams.parse($scope.options.fileUrl);
        $scope.options.forceLang = (urlParams.findByProperty('0', 'preferredLang') || {})[1];
        $scope.options.LANGS = options.allowedLangs.map(function(code) {
          return {
            value: code,
            description: LANGS[code]
          };
        });
        $scope.$watch('options.forceLang', function(forceLang, oldValue) {
          var params;
          params = urlParams.map(function(param) {
            if (param[0] === 'preferredLang') {
              param[1] = forceLang;
            }
            return param;
          });
          $scope.options.fileUrl = $scope.options.fileUrl.split('?')[0] + '?' + H.urlParams.dump(params);
        });
      }
      $scope.cancel = function() {
        return $uibModalInstance.dismiss('cancel');
      };
      return $scope.ok = function() {
        return $uibModalInstance.close();
      };
    };
    this.open = function(ngModel, options) {
      var result;
      result = $uibModal.open({
        animation: true,
        template: ('/client/cms_editable_app/_modals/uiceFileView/modal.html', '<div class="modal-header"><button class="close" ng-click="cancel()"><span>&times;</span></button><h4 class="modal-title"><span>{{:: title | l10n | translate}}</span><small ng-if="description | l10n"> ({{:: description | l10n}})</small></h4></div><div class="modal-body" ng-switch="fileType"><img class="thumbnail nomargin" ng-switch-when="image" ng-src="{{options.fileUrl}}" style="width:100%;"/><div ng-switch-when="pdf"><uic-pdf-viewer ng-model="options.fileUrl" height="520px"></uic-pdf-viewer></div><div class="text-center" ng-switch-default=""><br/><br/><br/><h4 translate="">File is not available for preview</h4><br/><br/><br/></div></div><div class="modal-footer"><ul class="list-table nomargin vertical-align-middle"><li><ul class="list-table w-auto vertical-align-middle" ng-show="options.allowedLangs.length"><li><label class="nomargin" translate="">Language:</label></li><li><div class="form-group nomargin" ng-switch="options.LANGS.length &lt;= 2 &amp;&amp; $cms.screenSize == \'lg\'"><uic-constant ng-model="options.forceLang" radio="options.LANGS" ng-switch-when="true" input-class="inline"></uic-constant><uic-constant ng-model="options.forceLang" select="options.LANGS" ng-switch-when="false" input-class="input-sm"></uic-constant></div></li></ul></li><li><a class="btn btn-primary" href="{{options.fileUrl}}" target="_blank" download=""><span class="fas fa-cloud-download-alt"></span><span translate="">Download file</span></a></li></ul></div>' + ''),
        controller: ['$scope', '$uibModalInstance', 'ngModel', 'options', modalController],
        size: 'lg',
        windowClass: 'uice-file-view-modal',
        resolve: {
          ngModel: function() {
            return ngModel;
          },
          options: function() {
            return options || {};
          }
        }
      });
      return result.result;
    };
    return this;
  }]);

}).call(this);
;
(function() {
  angular.module('ui.cms.editable').provider('uiceItemEditor', function() {
    var defaultModelConfigs, defaultWindowClass, getService, uiceItemEditor;
    defaultWindowClass = '';
    defaultModelConfigs = {};
    this.setDefaultWindowClass = function(cssName) {
      defaultWindowClass = cssName || '';
      return this;
    };
    this.setDefaultConfigForModel = function(modelName, config) {
      defaultModelConfigs[modelName] = config;
      return this;
    };
    getService = function($uibModal, $injector, $q, gettextCatalog, $cms, $constants, H) {
      var $currentUser, modalController, modalDeleteController, modalDeleteMultipleController, service;
      service = {};
      $currentUser = $injector.get('$currentUser');
      modalController = function($scope, $uibModalInstance, ngModel, options) {
        $scope.ngModel = angular.copy(ngModel) || {};
        $scope.options = options;
        $scope.forceLang = options.forceLang || CMS_DEFAULT_LANG;
        if ($scope.ngModel.id) {
          options.model.findById({
            id: ngModel.id
          }, function(data) {
            $scope.ngModel = data;
          });
        }
        $scope.cancel = function() {
          return $uibModalInstance.dismiss('cancel');
        };
        $scope.ok = function() {
          var onError, onOk;
          $scope.loading = true;
          onOk = function(data) {
            var k, ref, tasks, v;
            tasks = [];
            if (options.model.uploadFile) {
              ref = $scope.ngModel;
              for (k in ref) {
                v = ref[k];
                if (v instanceof File) {
                  tasks.push((function(k, v) {
                    var postData;
                    postData = {
                      file: v,
                      $asFormData: true
                    };
                    return options.model.uploadFile({
                      id: data.id,
                      forModelField: k.replace('$', '')
                    }, postData).$promise;
                  })(k, v));
                }
              }
            }
            if (!tasks.length) {
              $uibModalInstance.close(data);
              return;
            }
            $q.all(tasks).then(function() {
              options.model.findById({
                id: data.id
              }, $uibModalInstance.close, function() {
                return $uibModalInstance.close(data);
              });
            }, function() {
              $uibModalInstance.close(data);
            });
          };
          onError = function(data) {
            $cms.showNotification('Error on save', null, 'error');
            $scope.loading = false;
          };
          if (!$scope.ngModel.id) {
            options.model.create({}, $scope.ngModel, onOk, onError);
          } else {
            options.model.updateAttributes({
              id: ngModel.id
            }, $scope.ngModel, onOk, onError);
          }
        };
        $scope["delete"] = function() {
          uiceItemEditor["delete"](ngModel, {
            modelName: options.modelName
          }).then($uibModalInstance.close);
        };
        $scope.save = $scope.ok;
      };
      modalDeleteController = function($scope, $uibModalInstance, ngModel, options) {
        var representation;
        options.title = options.title || gettextCatalog.getString("Delete?");
        if (!options.representationHtml) {
          representation = $constants.resolve("$REPRESENTATION." + options.modelName) || {};
          options.representationHtml = representation.html;
        }
        options.representationHtml = options.representationHtml || "{{$item.name || ($item.title | l10n)}}";
        $scope.options = options;
        $scope.$item = ngModel;
        $scope.cancel = function() {
          return $uibModalInstance.dismiss('cancel');
        };
        return $scope.ok = function() {
          $scope.loading = true;
          options.model.deleteById({
            id: ngModel.id
          }, function(data) {
            $uibModalInstance.close(data);
          }, function(data) {
            $scope.error = data;
            $scope.loading = false;
          });
        };
      };
      modalDeleteMultipleController = function($scope, $uibModalInstance, items, options) {
        var representation;
        options.title = options.title || gettextCatalog.getString("Delete all selected objects?");
        if (!options.representationHtml) {
          representation = $constants.resolve("$REPRESENTATION." + options.modelName) || {};
          options.representationHtml = representation.html;
        }
        options.representationHtml = options.representationHtml || "{{$item.name || ($item.title | l10n)}}";
        $scope.options = options;
        $scope.items = items;
        $scope.cancel = function() {
          return $uibModalInstance.dismiss('cancel');
        };
        return $scope.ok = function() {
          var tasks;
          $scope.loading = true;
          tasks = items.map(function(item) {
            return (function() {
              return options.model.deleteById({
                id: item.id
              }).$promise;
            })();
          });
          $q.waterfall(tasks).then(function(data) {
            $uibModalInstance.close(data);
          }, function(data) {
            $scope.error = data;
            $scope.loading = false;
          });
        };
      };
      service.generateNewItem = function(modelName) {
        var item;
        if (angular.isFunction((defaultModelConfigs[modelName] || {}).getNewItem)) {
          item = defaultModelConfigs[modelName].getNewItem($currentUser, $injector);
        }
        return item || {};
      };
      service.edit = function(ngModel, options) {
        var dashedModelName, defaultConfig, editorTag, representation, result;
        options = options || {};
        ngModel = ngModel || {};
        if (!ngModel.id && !options.modelName) {
          throw "aDbItemEditor.edit: provide ngModel or options.modelName!";
        }
        options.modelName = options.modelName || ngModel.__proto__.constructor.modelName;
        options.model = options.model || $injector.get(options.modelName);
        defaultConfig = defaultModelConfigs[options.modelName] || {};
        if (Object.keys(ngModel).length === 0) {
          ngModel = service.generateNewItem(options.modelName);
        }
        if (!ngModel.hasOwnProperty('isPublished')) {
          ngModel.isPublished = true;
          ngModel.publicationDate = new Date;
        }
        dashedModelName = H.convert.camelCaseToDash(options.modelName);
        if (!options.title) {
          representation = $constants.resolve("$REPRESENTATION." + options.modelName) || {};
          options.title = representation.name || dashedModelName.split('-').join(' ');
        }
        if (options.modelName && $injector.has("oa" + options.modelName + "EditorDirective")) {
          editorTag = "oa-" + dashedModelName + "-editor";
        } else if (options.modelName && $injector.has("a" + options.modelName + "EditorDirective")) {
          editorTag = "a-" + dashedModelName + "-editor";
        }
        if (editorTag) {
          options.directiveEditorHtml = "<" + editorTag + " ng-model=\"ngModel\" force-lang=\"forceLang\" form=\"form\" modal='true'></" + editorTag + ">";
        }
        options.directiveEditorHtml || (options.directiveEditorHtml = "<div class='alert alert-danger'>\n    <b>Can't find directive for '" + options.modelName + "'. Create 'a" + options.modelName + "Editor' or 'oa" + options.modelName + "Editor' directive</b>\n</div>");
        result = $uibModal.open({
          animation: true,
          windowClass: "uice-item-editor-modal " + defaultWindowClass,
          template: ('/client/cms_editable_app/_modals/uiceItemEditor/edit.html', '<div class="modal-header"><button class="close" ng-click="cancel()"><span>&times;</span></button><h4 class="modal-title">{{options.title | l10n | translate}}</h4></div><form class="modal-body" name="form"><div class="with-preloader" ng-show="loading" style="height: 7em;"><div class="preloader"></div></div><div ng-switch="!!options.directiveEditorHtml" ng-hide="loading"><div uic-bind-html="options.directiveEditorHtml"></div></div></form><div class="modal-footer"><button class="btn btn-primary" ng-click="ok()" ng-disabled="form.$invalid || loading" ng-switch="!!ngModel.id"><span class="fas fa-save"></span><span ng-switch-when="false" translate="">Create</span><span ng-switch-when="true" translate="">Update</span></button><button class="btn btn-danger btn-delete" ng-click="delete()" ng-class="{visible: ngModel.id &amp;&amp; options.allowDelete}"><span class="fas fa-trash-alt"></span><span translate="">Delete</span></button><button class="btn btn-default" ng-click="cancel()" translate="" ng-disabled="loading">Cancel</button></div>' + ''),
          controller: ['$scope', '$uibModalInstance', 'ngModel', 'options', modalController],
          size: options.modalSize || defaultConfig.editModalSize || 'md',
          resolve: {
            ngModel: function() {
              return ngModel;
            },
            options: function() {
              return options;
            }
          }
        });
        return result.result;
      };
      service["delete"] = function(ngModel, options) {
        var result;
        if (!(ngModel || {}).id) {
          console.warn("aDbItemEditor.delete: ngModel.id required!");
          return;
        }
        options = options || {};
        options.modelName = options.modelName || ngModel.__proto__.constructor.modelName;
        options.model = options.model || $injector.get(options.modelName);
        result = $uibModal.open({
          animation: true,
          windowClass: "uice-item-editor-modal delete " + defaultWindowClass,
          template: ('/client/cms_editable_app/_modals/uiceItemEditor/delete.html', '<div class="modal-header"><button class="close" ng-click="cancel()"><span>&times;</span></button><h4 class="modal-title">{{options.title | l10n | translate}}</h4></div><div class="modal-body"><div class="with-preloader" ng-show="loading" style="height: 7em;"><div class="preloader"></div></div><div ng-hide="loading"><div class="thumbnail" ng-if="$item.mainImg"><uic-picture file="$item.mainImg"></uic-picture></div><p><span translate="">"<span uic-bind-html="options.representationHtml"></span>" will be deleted immediately. </span><span> </span><b translate="" translate-context="delete object">You can\'t undo this action.</b></p></div><uic-alert type="danger" ng-show="error &amp;&amp; !loading"><b translate="">Can\'t delete object. Try again later.</b></uic-alert></div><div class="modal-footer"><button class="btn btn-danger" ng-click="ok()" ng-disabled="loading"><span class="fas fa-trash-alt"></span><span translate="">Delete </span></button><button class="btn btn-default" ng-click="cancel()" translate="">Cancel</button></div>' + ''),
          controller: ['$scope', '$uibModalInstance', 'ngModel', 'options', modalDeleteController],
          size: 'sm',
          resolve: {
            ngModel: function() {
              return ngModel;
            },
            options: function() {
              return options;
            }
          }
        });
        return result.result;
      };
      service.deleteMultiple = function(items, options) {
        var _items, i, item, len, result;
        options = options || {};
        _items = [];
        for (i = 0, len = items.length; i < len; i++) {
          item = items[i];
          if (item.id) {
            _items.push(item);
          }
          options.modelName = options.modelName || item.__proto__.constructor.modelName;
        }
        if (!_items.length) {
          console.warn("aDbItemEditor.deleteMultiple: items with 'id' property required!");
          return;
        }
        if (!options.modelName) {
          throw "aDbItemEditor.deleteMultiple: provide options.modelName!";
        }
        options.model = options.model || $injector.get(options.modelName);
        result = $uibModal.open({
          animation: true,
          windowClass: "uice-item-editor-modal delete-multiple " + defaultWindowClass,
          template: ('/client/cms_editable_app/_modals/uiceItemEditor/deleteMultiple.html', '<div class="modal-header"><button class="close" ng-click="cancel()"><span>&times;</span></button><h4 class="modal-title">{{options.title | l10n | translate}}</h4></div><div class="modal-body"><div class="with-preloader" ng-show="loading" style="height: 7em;"><div class="preloader"></div></div><div ng-hide="loading"><p translate="">This items will be deleted immediately:</p><ul class="items-list"><li ng-repeat="$item in items"><div uic-bind-html="options.representationHtml"></div></li></ul><b translate="" translate-context="delete object">You can\'t undo this action.</b></div><uic-alert type="danger" ng-show="error &amp;&amp; !loading"><b translate="">Can\'t delete objects. Try again later.</b></uic-alert></div><div class="modal-footer"><button class="btn btn-danger" ng-click="ok()" ng-disabled="loading"><span class="fas fa-trash-alt"></span><span translate="">Delete </span></button><button class="btn btn-default" ng-click="cancel()" translate="">Cancel</button></div>' + ''),
          controller: ['$scope', '$uibModalInstance', 'items', 'options', modalDeleteMultipleController],
          size: 'sm',
          resolve: {
            items: function() {
              return _items;
            },
            options: function() {
              return options;
            }
          }
        });
        return result.result;
      };
      return service;
    };
    uiceItemEditor = null;
    this.$get = [
      '$uibModal', '$injector', '$q', 'gettextCatalog', '$cms', '$constants', 'H', function($uibModal, $injector, $q, gettextCatalog, $cms, $constants, H) {
        uiceItemEditor = uiceItemEditor || getService($uibModal, $injector, $q, gettextCatalog, $cms, $constants, H);
        return uiceItemEditor;
      }
    ];
    return this;
  });

}).call(this);
;
(function() {
  angular.module('ui.cms.editable').provider('uiceItemsExportModal', function() {
    var MAX_COUNT, modalController;
    MAX_COUNT = 1000;
    this.setMinItemsCountForBackgroundExport = function(count) {
      if (angular.isNumber(count) && count >= 0) {
        MAX_COUNT = count;
      }
    };
    modalController = function($scope, $uibModalInstance, $rootScope, $injector, modelName, options) {
      var $cms, queryFilter, r;
      r = $injector.get(modelName);
      $cms = $injector.get('$cms');
      $scope.fileFormat = 'xlsx';
      $scope.MAX_COUNT = MAX_COUNT;
      queryFilter = options.queryFilter || {};
      $scope.count = r.count(queryFilter);
      $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
      };
      return $scope.ok = function() {
        var $state, url;
        $state = $injector.get('$state');
        if ($scope.count.count <= MAX_COUNT) {
          url = r.$actionToUrl('export', {
            filter: queryFilter,
            fileFormat: $scope.fileFormat,
            fileName: options.fileName || $cms.seoService.pageTitle,
            stateName: $state.current.name,
            stateParams: $state.current.params || {},
            params: options.params || {}
          });
          location.href = url;
        } else {
          r["export"]({
            filter: queryFilter,
            fileFormat: $scope.fileFormat,
            fileName: options.fileName || $cms.seoService.pageTitle,
            stateName: $state.current.name,
            stateParams: $state.current.params || {},
            params: options.params || {}
          });
        }
        $uibModalInstance.close();
      };
    };
    this.$get = [
      '$uibModal', function($uibModal) {
        this.open = function(modelName, options) {
          var result;
          result = $uibModal.open({
            animation: true,
            backdrop: 'static',
            keyboard: false,
            template: ('/client/cms_editable_app/_modals/uiceItemsExport/modal.html', '<div class="modal-header"><h4 class="modal-title" translate="">Settings</h4></div><div class="modal-body"><div class="form-group"><label translate="">File format</label><select class="form-control" ng-model="fileFormat"><option value="csv" translate="">Csv text file</option><option value="xlsx" translate="">Excel 2007</option></select></div><div ng-show="count.count &gt; MAX_COUNT"><b class="text-danger" translate="">There are too many data rows selected to be exported. Data will be exported in the background and the file will be sent to your email.</b></div></div><div class="modal-footer"><button class="btn btn-primary" ng-click="ok()" translate="">Export</button><button class="btn btn-default" ng-click="cancel()" translate="">Cancel</button></div>' + ''),
            controller: ['$scope', '$uibModalInstance', '$rootScope', '$injector', 'modelName', 'options', modalController],
            size: 'sm',
            resolve: {
              modelName: function() {
                return modelName;
              },
              options: function() {
                return options || {};
              }
            }
          });
          return result.result;
        };
        return this;
      }
    ];
  });

}).call(this);
;

/**
*   @ngdoc service
*   @name ui.cms.editable.service:uiceL10nFileEditorModal
*   @description сервис дает возможность вызывать модальное окно для загрузки
*       L10nFile-файлов на сервер, редактирования уже заргуженных L10nFile-файлов
*       <br>
 */

(function() {
  angular.module('ui.cms.editable').provider('uiceL10nFileEditorModal', function() {
    var getService, isTitleDescriptionVisible, uiceL10nFileEditorModal;
    isTitleDescriptionVisible = true;
    this.setTitleDescriptionVisibility = function(isVisible) {
      isTitleDescriptionVisible = !!isVisible;
      return this;
    };
    getService = function($uibModal, $injector) {
      var getNgResource, getNgResourceName, modalDeleteController, modalEditController, modalUploadMultiple, service;
      service = {};
      getNgResource = function(item, options) {
        if ((item.__proto__.constructor || {}).modelName) {
          return $injector.get(item.__proto__.constructor.modelName);
        }
        if (options.cmsModelName) {
          return $injector.get(options.cmsModelName);
        }
        console.warn(item);
        throw "uiceL10nFileEditorModal: cant find ng resource for item";
      };
      getNgResourceName = function(item, options) {
        if ((item.__proto__.constructor || {}).modelName && $injector.has(item.__proto__.constructor.modelName)) {
          return item.__proto__.constructor.modelName;
        }
        return options.cmsModelName;
      };

      /**
      *   @ngdoc property
      *   @name ui.cms.editable.service:uiceL10nFileEditorModal#view
      *   @methodOf ui.cms.editable.service:uiceL10nFileEditorModal
      *   @description
      *        открывает модальное окно для просмотра L10nFile
      *   @param {object} item объет из бд (типа CmsArticle, CmsKeyword и т.п. из модуля lbServices)
      *   @param {object} options параметры
      *       - `forModelField` - `string` - (required) название поля внутри item, для которого нужен просмотр (напр.: mainImg, avatarImg)
      *       - `title` - `string` - заголовок, который будет у модального окна. По умолчанию название файла
      *   @returns {promise} экземпляр {@link https://docs.angularjs.org/api/ng/service/$q $q.promise}. Можно на нем использовать then. В successCb придет обновленное значение поля `forModelField` с сервера
      *   @example
      *       <pre>
      *           $scope.item = ...// где-то выше определен или получен с сервера(напр. объект модели CmsArticle)
      *           options = {
      *                forModelField: 'mainImg'
      *                accept: 'video/*,audio/*'
      *                maxSize: '10 MB'
      *           }
      *
      *           uiceL10nFileEditorModal.edit($scope.item, options).then (mainImg)->
      *                $scope.item.mainImg = mainImg
      *       </pre>
       */
      service.view = function(item, options) {
        var uiceFileViewModal;
        if (!item || !item.id) {
          throw "uiceL10nFileEditorModal: you cant view empty model!";
        }
        options = options || {};
        if (!options.forModelField) {
          throw "uiceL10nFileEditorModal: you should specify 'forModelField' in options!";
        }
        if (angular.isArray(item[options.forModelField])) {
          item = item[options.forModelField][options.index];
        } else {
          item = item[options.forModelField];
        }
        if (!item || !item.id) {
          throw "uiceL10nFileEditorModal: you cant view empty L10nFile!";
        }
        uiceFileViewModal = $injector.get('uiceFileViewModal');
        return uiceFileViewModal.open(item, {
          title: options.title
        });
      };
      modalEditController = function($scope, $uibModalInstance, item, ModelResource, options) {
        var fieldIsArray, updateTitleAndDescription, uploadFile, value;
        $scope.options = options;
        value = item[options.forModelField];
        fieldIsArray = angular.isArray(value);
        if (fieldIsArray) {
          $scope.ngModel = value[options.index] || {};
        } else {
          $scope.ngModel = value || {};
        }
        uploadFile = function(file, onOk) {
          var data, params;
          params = {
            id: item.id,
            forModelField: options.forModelField
          };
          data = {
            file: file,
            title: $scope.ngModel.title || {},
            description: $scope.ngModel.description || {},
            copyright: $scope.ngModel.copyright || {},
            $asFormData: true
          };
          if (options.forceCmsStorageType === 'localStorage') {
            data.options_saveToLocalStorage = true;
          }
          if (!fieldIsArray) {
            ModelResource.uploadFile(params, data, onOk, function() {
              $scope.error = 1;
              return $scope.loading = false;
            });
            return;
          }
          if (options.index !== -1) {
            data.options_forIdInList = $scope.ngModel.id;
          }
          ModelResource.uploadFileToList(params, data, onOk, function() {
            $scope.error = 1;
            return $scope.loading = false;
          });
        };
        updateTitleAndDescription = function(l10nFile, onOk) {
          var data, params;
          if (!options.allowEditTitleDescription) {
            return onOk(l10nFile);
          }
          params = {
            id: item.id,
            forModelField: options.forModelField
          };
          data = {
            id: $scope.ngModel.id,
            title: $scope.ngModel.title,
            description: $scope.ngModel.description,
            copyright: $scope.ngModel.copyright || {}
          };
          if (!fieldIsArray) {
            ModelResource.updateFile(params, data, onOk, function() {
              $scope.error = 2;
              return $scope.loading = false;
            });
            return;
          }
          ModelResource.updateFilesInsideList(params, [data], onOk, function() {
            $scope.error = 1;
            return $scope.loading = false;
          });
        };
        $scope.cancel = function() {
          return $uibModalInstance.dismiss('cancel');
        };
        $scope.ok = function() {
          $scope.loading = true;
          $scope.error = null;
          if (!$scope.localFile) {
            updateTitleAndDescription($scope.ngModel, $uibModalInstance.close);
            return;
          }
          uploadFile($scope.localFile, $uibModalInstance.close);
        };
        if (!fieldIsArray && !options.allowEditTitleDescription) {
          $scope.$watch('localFile', function(localFile, oldValue) {
            if (!localFile) {
              return;
            }
            $scope.ok();
          });
        }
      };
      modalUploadMultiple = function($scope, $uibModalInstance, item, ModelResource, options) {
        var $cms, $q, gettextCatalog;
        $scope.options = options;
        $scope.localFileList = [];
        $scope.proxyFileList = [];
        $q = $injector.get('$q');
        $cms = $injector.get('$cms');
        gettextCatalog = $injector.get('gettextCatalog');
        $scope.toggleProxyUpload = function(index) {
          $scope.proxyFileList[index].upload = !$scope.proxyFileList[index].upload;
        };
        $scope.cancel = function() {
          return $uibModalInstance.dismiss('cancel');
        };
        $scope.ok = function() {
          var fileNames, j, len, pFile, params, ref, tasks;
          $scope.loading = true;
          $scope.error = null;
          params = {
            id: item.id,
            forModelField: options.forModelField
          };
          tasks = [];
          fileNames = [];
          ref = $scope.proxyFileList;
          for (j = 0, len = ref.length; j < len; j++) {
            pFile = ref[j];
            if (!pFile.upload) {
              continue;
            }
            fileNames.push(pFile.name);
            tasks.push((function(pFile) {
              var data;
              data = {
                file: pFile.file,
                title: pFile.title,
                description: pFile.description,
                $asFormData: true
              };
              params = {
                id: item.id,
                forModelField: options.forModelField
              };
              return ModelResource.uploadFileToList(params, data).$promise;
            })(pFile));
          }
          $q.waterfall(tasks, true).then(function(data) {
            var e, html, i, k, len1, ref1;
            if (data.$errors) {
              html = "<ul>";
              ref1 = data.$errors;
              for (i = k = 0, len1 = ref1.length; k < len1; i = ++k) {
                e = ref1[i];
                if (e) {
                  html += "<li>" + fileNames[i] + "</li>";
                }
              }
              html += "</ul>";
              $cms.showNotification(gettextCatalog.getString('Error on file upload:') + html, null, 'error');
            }
            params = {
              id: item.id,
              filter: {
                fields: {}
              }
            };
            params.filter.fields[options.forModelField] = true;
            ModelResource.findById(params, function(data) {
              return $uibModalInstance.close(data[options.forModelField]);
            });
          }, function(data) {
            $scope.loading = false;
            return $scope.error = 1;
          });
        };
        return $scope.$watch('localFileList', function(localFileList, oldValue) {
          var file, j, len, pFile, proxyFileList, ref;
          proxyFileList = [];
          proxyFileList.isEmpty = function() {
            var j, len, pFile, ref;
            ref = this;
            for (j = 0, len = ref.length; j < len; j++) {
              pFile = ref[j];
              if (pFile.upload) {
                return false;
              }
            }
            return true;
          };
          ref = localFileList || [];
          for (j = 0, len = ref.length; j < len; j++) {
            file = ref[j];
            pFile = $scope.proxyFileList.findByProperty('name', file.name) || {
              name: file.name,
              upload: true,
              title: {},
              description: {}
            };
            pFile.file = file;
            proxyFileList.push(pFile);
          }
          $scope.proxyFileList = proxyFileList;
        });
      };

      /**
      *   @ngdoc property
      *   @name ui.cms.editable.service:uiceL10nFileEditorModal#edit
      *   @methodOf ui.cms.editable.service:uiceL10nFileEditorModal
      *   @description
      *        открывает модальное окно для редактирования L10nFile
      *   @param {object} item объет из бд (типа CmsArticle, CmsKeyword и т.п. из модуля lbServices)
      *   @param {object} options параметры
      *       - `forModelField` - `string` - (required) название поля внутри item, для которого происходит загрузка/редактирование (напр.: mainImg, avatarImg)
      *       - `allowEditTitleDescription` - `boolean` - можно ли редактировать поля заголовка и описания картинки. По умолчанию false
      *       - `accept` - `string` -  маска MIME типов файлов для выбора файлов с компютера (через запятую). По умолчанию - '*'
      *       - `maxSize` - `string` - максимальный размер файла в ['b', 'kb', 'mb']
      *   @returns {promise} экземпляр {@link https://docs.angularjs.org/api/ng/service/$q $q.promise}. Можно на нем использовать then. В successCb придет обновленное значение поля `forModelField` с сервера
      *   @example
      *       <pre>
      *           $scope.item = ...// где-то выше определен или получен с сервера(напр. объект модели CmsArticle)
      *           options = {
      *                forModelField: 'mainImg'
      *                accept: 'video/*,audio/*'
      *                maxSize: '10 MB'
      *           }
      *
      *           uiceL10nFileEditorModal.edit($scope.item, options).then (mainImg)->
      *                $scope.item.mainImg = mainImg
      *       </pre>
       */
      service.edit = function(item, options) {
        var AdminAppConfig, ModelResource, ModelResourceName, modalSize, result;
        if (!item || !item.id) {
          throw "uiceL10nFileEditorModal: you cant edit empty model!";
        }
        ModelResource = getNgResource(item, options);
        ModelResourceName = getNgResourceName(item, options);
        options = options || {};
        if (!isTitleDescriptionVisible) {
          options.allowEditTitleDescription = false;
        }
        if (!options.forModelField) {
          throw "uiceL10nFileEditorModal: you should specify 'forModelField' in options!";
        }
        options.accept = options.accept || '*';
        options.maxSize = options.maxSize || '15 MB';
        options.lang = options.lang || CMS_DEFAULT_LANG;
        options.singleLang = CMS_ALLOWED_LANGS.length === 1 && CMS_ALLOWED_LANGS[0] === CMS_DEFAULT_LANG;
        if (!options.allowEditCopyrightMetadata && $injector.has('AdminAppConfig')) {
          AdminAppConfig = $injector.get('AdminAppConfig');
          options.allowEditCopyrightMetadata = AdminAppConfig.copyrightMetadataEditorForModels[ModelResourceName] === '*' || (AdminAppConfig.copyrightMetadataEditorForModels[ModelResourceName] || []).includes(options.forModelField);
          options.copyrightMetadataFields = AdminAppConfig.copyrightMetadataFields || [];
        }
        modalSize = 'md';
        if (!options.allowEditTitleDescription) {
          modalSize = 'sm';
        }
        if (angular.isArray(item[options.forModelField]) && !item[options.forModelField][options.index]) {
          options.index = -1;
        }
        if (options.index === -1) {
          result = $uibModal.open({
            animation: true,
            windowClass: 'uice-l10n-file-editor-modal',
            template: ('/client/cms_editable_app/_modals/uiceL10nFileEditor/editMultiple.html', '<div class="modal-header"><button class="close" ng-click="cancel()"><span>&times;</span></button><h4 class="modal-title" translate="">Upload files</h4></div><form class="modal-body" name="form"><div class="table-file-upload-container" ng-hide="loading || error"><div class="pull-right hidden-xs" ng-hide="options.singleLang || !proxyFileList.length"><ul class="list-table w-auto vertical-align-middle"><li translate="">Edit properties for language:</li><li><uic-lang-picker ng-model="options.lang" size="sm"></uic-lang-picker></li></ul></div><div class="clearfix"></div><table class="table-file-upload"><tbody><tr ng-repeat="file in proxyFileList"><td class="td-checkbox" ng-click="toggleProxyUpload($index)"><label class="checkbox"><input type="checkbox" ng-checked="proxyFileList[$index].upload"/></label></td><td class="td-thumbnail col-sm-35" ng-click="toggleProxyUpload($index)"><uic-local-file-picker ng-model="file.file"></uic-local-file-picker></td><td class="td-data hidden-xs" ng-hide="!options.allowEditTitleDescription"><uice-l10n-input class="form-group" ng-model="proxyFileList[$index].title" lang="options.lang" label="Title" ng-disabled="form.$invalid"></uice-l10n-input><uice-l10n-input class="form-group" ng-model="proxyFileList[$index].description" type="textarea" lang="options.lang" label="Description" ng-disabled="form.$invalid"></uice-l10n-input><div ng-if="options.allowEditCopyrightMetadata"><div class="form-group" ng-if="options.copyrightMetadataFields.includes(\'creator\')"><label><span translate="" translate-context="copyright">Creator</span><span> </span><small translate="">(copyright metadata)</small></label><input class="form-control" ng-model="proxyFileList[$index].copyright.creator"/></div><div class="form-group" ng-if="options.copyrightMetadataFields.includes(\'creatorUrl\')"><label><span translate="" translate-context="copyright">Creator\'s url</span><span> </span><small translate="">(copyright metadata)</small></label><input class="form-control" ng-model="proxyFileList[$index].copyright.creatorUrl"/></div><div class="form-group" ng-if="options.copyrightMetadataFields.includes(\'creditLine\')"><label><span translate="" translate-context="copyright">Credit line</span><span> </span><small translate="">(copyright metadata)</small></label><input class="form-control" ng-model="proxyFileList[$index].copyright.creditLine"/></div><div class="form-group" ng-if="options.copyrightMetadataFields.includes(\'copyrightNotice\')"><label><span translate="" translate-context="copyright">Copyright notice</span><span> </span><small translate="">(copyright metadata)</small></label><input class="form-control" ng-model="proxyFileList[$index].copyright.copyrightNotice"/></div></div></td></tr></tbody></table><div class="text-left" ng-show="!proxyFileList.length"><h5 translate="">Choose files from your computer</h5><p translate="" ng-show="options.accept != \'*\'">Allowed file types: <b>{{options.accept}}</b></p><p translate="" ng-show="options.maxSize">Allowed max file size <b>{{options.maxSize}}</b></p></div></div><div class="with-preloader" ng-show="loading" style="height: 10em;"><div class="preloader"></div></div><div class="text-center" ng-show="error"><h3 class="text-danger" translate="">Oopps!</h3><p class="text-danger" ng-switch="error"><span ng-switch-when="1" translate="translate">Can\'t upload file to server=(</span></p></div></form><fieldset class="modal-footer" ng-disabled="loading"><uic-input-file class="btn btn-success" ng-model="localFileList" accept="{{options.accept}}" max-size="{{options.maxSize}}" multiple="true"></uic-input-file><button class="btn btn-primary" ng-click="ok()" ng-disabled="form.$invalid || proxyFileList.isEmpty()" ng-switch="!!ngModel.id" ng-hide="!proxyFileList.length || error"><span class="fas fa-save"></span><span ng-switch-when="false" translate="">Upload</span><span ng-switch-when="true" translate="">Save</span></button><button class="btn btn-primary" ng-click="ok()" translate="" ng-show="error">Retry</button><button class="btn btn-default hidden-xs" ng-click="cancel()" translate="">Cancel</button></fieldset>' + ''),
            controller: ['$scope', '$uibModalInstance', 'item', 'ModelResource', 'options', modalUploadMultiple],
            size: modalSize,
            resolve: {
              item: function() {
                return item;
              },
              ModelResource: function() {
                return ModelResource;
              },
              options: function() {
                return options;
              }
            }
          });
        } else {
          result = $uibModal.open({
            animation: true,
            windowClass: 'uice-l10n-file-editor-modal',
            template: ('/client/cms_editable_app/_modals/uiceL10nFileEditor/edit.html', '<div class="modal-header"><button class="close" ng-click="cancel()"><span>&times;</span></button><h4 class="modal-title" ng-switch="!!ngModel.id"><span ng-switch-when="false" translate="">Upload file</span><span ng-switch-when="true" translate="">Edit file</span></h4></div><form class="modal-body" name="form"><div class="row" ng-hide="loading || error"><div class="form-group" ng-class="{\'col-sm-40\': options.allowEditTitleDescription, \'col-sm-100 nomargin\': !options.allowEditTitleDescription}"><uic-local-file-picker ng-model="localFile" old-l10n-file="ngModel" force-cms-storage-type="{{options.forceCmsStorageType}}" accept="{{options.accept}}" max-size="{{options.maxSize}}" name="file" ng-required="!ngModel.id"></uic-local-file-picker><div class="help-block" ng-if="form.file.$error.acceptType"><small class="text-danger" translate="">Invalid file type. Allowed are: <b>{{options.accept}}</b></small></div><div class="help-block" ng-if="form.file.$error.maxsize"><small class="text-danger" translate="">File is too big. Size should be &lt;= {{options.maxSize}}</small></div></div><div class="col-sm-60" ng-show="options.allowEditTitleDescription"><ul class="list-table w-auto vertical-align-middle" ng-hide="options.singleLang"><li translate="">Edit properties for language:</li><li><uic-lang-picker ng-model="options.lang" size="sm"></uic-lang-picker></li></ul><div class="clearfix"></div><uice-l10n-input class="form-group" ng-model="ngModel.title" lang="options.lang" label="Title" ng-disabled="form.$invalid"></uice-l10n-input><uice-l10n-input class="form-group" ng-model="ngModel.description" type="textarea" lang="options.lang" label="Description" ng-disabled="form.$invalid"></uice-l10n-input></div><div class="col-md-100" ng-if="options.allowEditCopyrightMetadata"><h4 class="text-primary" translate="">Copyright metadata</h4><div class="form-group" ng-if="options.copyrightMetadataFields.includes(\'creator\')"><label translate="" translate-context="copyright">Creator</label><input class="form-control input-sm" ng-model="ngModel.copyright.creator"/></div><div class="form-group" ng-if="options.copyrightMetadataFields.includes(\'creatorUrl\')"><label translate="" translate-context="copyright">Creator\'s url</label><input class="form-control input-sm" ng-model="ngModel.copyright.creatorUrl"/></div><div class="form-group" ng-if="options.copyrightMetadataFields.includes(\'creditLine\')"><label translate="" translate-context="copyright">Credit line</label><input class="form-control input-sm" ng-model="ngModel.copyright.creditLine"/></div><div class="form-group" ng-if="options.copyrightMetadataFields.includes(\'copyrightNotice\')"><label translate="" translate-context="copyright">Copyright notice</label><input class="form-control input-sm" ng-model="ngModel.copyright.copyrightNotice"/></div></div></div><div class="with-preloader" ng-show="loading" style="height: 10em;"><div class="preloader"></div></div><div class="text-center" ng-show="error"><h3 class="text-danger" translate="">Oopps!</h3><p class="text-danger" ng-switch="error"><span ng-switch-when="1" translate="translate">Can\'t upload file to server=(</span><span ng-switch-when="2" translate="translate">Can\'t update title and description for images=(</span></p></div></form><fieldset class="modal-footer" ng-show="options.allowEditTitleDescription || error" ng-disabled="loading"><button class="btn btn-primary" ng-click="ok()" ng-disabled="form.$invalid" ng-switch="!!ngModel.id" ng-hide="error"><span class="fas fa-save"></span><span ng-switch-when="false" translate="">Upload</span><span ng-switch-when="true" translate="">Save</span></button><button class="btn btn-primary" ng-click="ok()" translate="" ng-show="error">Retry</button><button class="btn btn-default hidden-xs" ng-click="cancel()" translate="">Cancel</button></fieldset>' + ''),
            controller: ['$scope', '$uibModalInstance', 'item', 'ModelResource', 'options', modalEditController],
            size: modalSize,
            resolve: {
              item: function() {
                return item;
              },
              ModelResource: function() {
                return ModelResource;
              },
              options: function() {
                return options;
              }
            }
          });
        }
        return result.result;
      };

      /*
       */
      modalDeleteController = function($scope, $uibModalInstance, gettextCatalog, item, ModelResource, options) {
        var deleteFile, fieldIsArray, value;
        $scope.options = options;
        $scope.options.title = options.title || gettextCatalog.getString('Delete file?');
        value = item[options.forModelField];
        fieldIsArray = angular.isArray(value);
        if (fieldIsArray) {
          $scope.ngModel = value[options.index];
        } else {
          $scope.ngModel = value;
        }
        deleteFile = function(onOk) {
          var data;
          if (!fieldIsArray) {
            ModelResource.deleteFile({
              id: item.id,
              forModelField: options.forModelField
            }, onOk, function() {
              return $scope.error = 1;
            });
            return;
          }
          data = {
            id: $scope.ngModel.id
          };
          ModelResource.deleteFilesInsideList({
            id: item.id,
            forModelField: options.forModelField
          }, [data], onOk, function() {
            return $scope.error = 1;
          });
        };
        $scope.cancel = function() {
          return $uibModalInstance.dismiss('cancel');
        };
        return $scope.ok = function() {
          $scope.loading = true;
          $scope.error = null;
          deleteFile($uibModalInstance.close);
        };
      };

      /**
      *   @ngdoc property
      *   @name ui.cms.editable.service:uiceL10nFileEditorModal#delete
      *   @methodOf ui.cms.editable.service:uiceL10nFileEditorModal
      *   @description
      *        открывает модальное окно для удаления L10nFile
      *   @param {object} item объет из бд (типа CmsArticle, CmsKeyword и т.п. из модуля lbServices)
      *   @param {object} options параметры
      *       - `forModelField` - `string` - (required) название поля внутри item, для которого происходит загрузка/редактирование (напр.: mainImg, avatarImg)
      *       - `title` - `string` - заголовок, который будет у модального окна. По умолчанию "Delete file?"
      *   @returns {promise} экземпляр {@link https://docs.angularjs.org/api/ng/service/$q $q.promise}. Можно на нем использовать then. В successCb придет null (что значит, что файл <b>удален на сервере</b>)
      *   @example
      *       <pre>
      *           $scope.item = ...// где-то выше определен или получен с сервера(напр. объект модели CmsArticle)
      *           options = {
      *                forModelField: 'mainImg'
      *                title: 'Delete this?'
      *           }
      *
      *           uiceL10nFileEditorModal.delete($scope.item, options).then ()->
      *                $scope.item.mainImg = null
      *       </pre>
       */
      service["delete"] = function(item, options) {
        var ModelResource, result;
        if (!item || !item.id) {
          throw "uiceL10nFileEditorModal: you cant edit empty model!";
        }
        options = options || {};
        if (!options.forModelField) {
          throw "uiceL10nFileEditorModal: you should specify 'forModelField' in options!";
        }
        ModelResource = getNgResource(item, options);
        result = $uibModal.open({
          animation: true,
          windowClass: 'uice-l10n-file-editor-modal',
          template: ('/client/cms_editable_app/_modals/uiceL10nFileEditor/delete.html', '<div class="modal-header"><button class="close" ng-click="cancel()"><span>&times;</span></button><h4 class="modal-title">{{options.title | l10n | translate}}</h4></div><form class="modal-body"><div class="with-preloader" ng-show="loading" style="height: 10em;"><div class="preloader"></div></div><div class="text-center" ng-show="error"><h3 class="text-danger" translate="">Oopps!</h3><p class="text-danger" ng-switch="error"><span ng-switch-when="1" translate="">Unable to delete file</span></p></div><uic-file-thumb ng-hide="loading" file="ngModel" size="440x250" force-cms-storage-type="{{options.forceCmsStorageType}}"></uic-file-thumb></form><fieldset class="modal-footer" ng-disabled="loading"><button class="btn btn-danger" ng-click="ok()" ng-switch="!!error"><span class="fas fa-trash-alt"></span><span ng-switch-when="false" translate="">Delete</span><span ng-switch-when="true" translate="">Retry</span></button><button class="btn btn-default" ng-click="cancel()" translate="">Cancel</button></fieldset>' + ''),
          controller: ['$scope', '$uibModalInstance', 'gettextCatalog', 'item', 'ModelResource', 'options', modalDeleteController],
          size: 'sm',
          resolve: {
            item: function() {
              return item;
            },
            ModelResource: function() {
              return ModelResource;
            },
            options: function() {
              return options;
            }
          }
        });
        return result.result;
      };
      return service;
    };
    uiceL10nFileEditorModal = null;
    this.$get = [
      '$uibModal', '$injector', function($uibModal, $injector) {
        uiceL10nFileEditorModal = uiceL10nFileEditorModal || getService($uibModal, $injector);
        return uiceL10nFileEditorModal;
      }
    ];
    return this;
  });

}).call(this);
;

/**
*   @ngdoc object
*   @name ui.cms.editable.$stateOverrideProvider
*   @header ui.cms.editable.$stateOverrideProvider
*   @description провайдер для переопределения объявления views из ui.router.
*       Доступен только в блоках config.
*       <b>Приложение, которое "переопределяет" родительское приложение
*       должно быть подключено в html ПОСЛЕ родительского, но подключено к тегу html.body ДО родительского.</b>
*       Переопределять можно части stateConfig, а не весь. Подробнее см. примеры.
 */

(function() {
  angular.module('ui.cms.editable').provider('$stateOverride', ["$injector", function($injector) {
    var $stateProvider, overridedStates;
    $stateProvider = $injector.get('$stateProvider');
    overridedStates = {};
    $stateProvider.decorator('views', function(state, parent) {
      var config, name, stateName, views;
      views = parent(state);
      for (name in views) {
        config = views[name];
        stateName = config.self.name;
        if (overridedStates.hasOwnProperty(stateName)) {
          if (overridedStates[stateName].template || overridedStates[stateName].templateUrl) {
            delete views[name].self.template;
            delete views[name].self.templateUrl;
          }
          views[name].self = angular.extend(views[name].self, overridedStates[stateName]);
          break;
        }
      }
      return views;
    });
    this.override = (function(_this) {
      return function(stateName, stateConfig) {

        /**
        * @ngdoc method
        * @name ui.cms.editable.$stateOverrideProvider#override
        * @methodOf ui.cms.editable.$stateOverrideProvider
        * @param {string} stateName уникальное имя состояния зарегестрированного через $stateProvider.state,
        *       напр.: "app.login" или "app.model.list".
        * @param {object} stateConfig объект состояния. Подробнее можно прочитать в
        *       {@link http://angular-ui.github.io/ui-router/site/#/api/ui.router.state.$stateProvider }
        *       Необязательно переопределять все параметры.
        * @returns {self} ссылка на самого себя для возможности регистрации переопределений "цепочкой" вызовов
        * @description Переопределяет <b>позже зарегистрированный через $stateProvider.state</b> state приложения.
        *       Не создает новых состояний для ui.router!
        * @example
        *   Переопределяем для "app.login" только шаблон, а для "app.news.list" шаблон и контроллер.
        *   <pre>
        *       angular.module('OverrideMyApp')
        *
        *       .controller 'OverrideNewsListCtrl', ($scope, $controller)->
        *           // магия наследования базового контроллера.
        *           // делать это не обязательно
        *           $controller('NewsListCtrl', { $scope })
        *           // теперь все $scope переменные базового контроллера
        *           // доступны тут.
        *           $scope.myNewFunc = ()->
        *               return 'Hello'
        *
        *       .config ($stateOverrideProvider)->
        *            $stateOverrideProvider
        *                .override('app.login', {
        *                    templateUrl: 'overrideLoginView.html'
        *                })
        *                .override('app.news.list', {
        *                    controller: 'OverrideNewsListCtrl'
        *                    templateUrl: 'overrideNewsList.html'
        *                })
        *       // далее где-то должно быть зарегистрировано приложение MyApp
        *       // с состояниями "app.login" и "app.news.list"
        *
        *
        *       // последнее: подключение обоих модулей к html.body
        *       // переопределяемое приложение должно подключаться ПОСЛЕДНИМ
        *       angular.element(document).ready \
        *           ()-> angular.bootstrap(document, ['OverrideMyApp', 'MyApp'])
        *   </pre>
         */
        overridedStates[stateName] = stateConfig;
        return _this;
      };
    })(this);
    this.$get = [
      function() {
        return null;
      }
    ];
    return this;
  }]);

}).call(this);
;

/**
*   @ngdoc object
*   @name ui.cms.editable.$tableViewProvider
*   @header ui.cms.editable.$tableViewProvider
*   @description провайдер для переопределения aggrid или других рендеров таблиц
 */

(function() {
  angular.module('ui.cms.editable').provider('$tableView', function() {
    var columnsConfig, renders, rendersForScreensize;
    columnsConfig = {};
    renders = {};
    rendersForScreensize = {
      lg: 'table',
      md: 'table',
      sm: 'list',
      xs: 'list'
    };
    this.registerRender = function(renderName, config) {
      renders[renderName] = config || {};
    };
    this.setRenderCdn = function(renderName, cdn) {
      throw "$tableView.setRenderCdn: Deprecated function. Use $uicIntegrationsProvider.registerThirdPartyLibrary('" + renderName + "', ['" + cdn + "'])";
    };
    this.setColumnsConfig = function(columnsName, config) {
      columnsConfig[columnsName] = config;
    };
    this.setDefaultRendersForScreenSize = function(screenSize, renderName) {
      if (screenSize !== 'lg' && screenSize !== 'md' && screenSize !== 'sm' && screenSize !== 'xs') {
        throw "setDefaultRendersForScreenSize: Invalid screenSize '" + screenSize + "'. Allowed are: 'lg', 'md', 'sm', 'xs'";
      }
      if (!renders[renderName]) {
        throw "setDefaultRendersForScreenSize: No such render '" + renderName + "' in $tableView";
      }
      rendersForScreensize[screenSize] = renderName;
    };
    this.$get = [
      function() {
        return {
          getColumnsConfig: function(columnsName) {
            return angular.copy(columnsConfig[columnsName]) || {};
          },
          getRenderConfig: function(name) {
            if (renders[name]) {
              return angular.copy(renders[name]) || {};
            }
            return {};
          },
          getDefaultRenderNameForScreenSize: function(screenSize) {
            return rendersForScreensize[screenSize] || 'table';
          }
        };
      }
    ];
    return this;
  });

}).call(this);
;
(function() {
  angular.module('ui.cms.editable').service('uiceAggridCellEditorModal', ["$uibModal", "$controller", "$sce", "$templateRequest", function($uibModal, $controller, $sce, $templateRequest) {
    var modalController;
    modalController = function($scope, $uibModalInstance, ngModel, options) {
      $scope.ngModel = angular.copy(ngModel);
      $scope.options = options;
      $scope.cancel = function() {
        return $uibModalInstance.dismiss('cancel');
      };
      $scope.ok = function() {
        return $uibModalInstance.close($scope.ngModel);
      };
      if (options.controller) {
        return $controller(options.controller, {
          $scope: $scope
        });
      }
    };
    this.open = function(ngModel, options) {
      var result, url;
      options = options || {};
      if (options.templateUrl) {
        url = $sce.getTrustedResourceUrl(options.templateUrl);
        $templateRequest(url).then(function(data) {
          options.template = data;
        });
      }
      result = $uibModal.open({
        animation: true,
        windowClass: 'uice-aggrid-cell-editor-modal',
        template: ('/client/cms_editable_app/_providers/tableViews/_directives/tableViewWrapperAggrid/_modals/uiceAggridCellEditor/modal.html', '<div class="modal-header"><button class="close" ng-click="cancel()"><span>&times;</span></button><h4 class="modal-title">{{options.modalHeaderName | translate}}</h4></div><div class="modal-body" name="form"><div uic-bind-html="options.template"></div><p class="help-text" ng-show="options.valueParams.required"><span class="text-danger">*</span><span> - </span><span translate="">required fields</span></p></div><div class="modal-footer"><button class="btn btn-success" ng-click="ok()" translate="" ng-disabled="form.$invalid">Ok</button><button class="btn btn-default" ng-click="cancel()" translate="">Cancel</button></div>' + ''),
        controller: ['$scope', '$uibModalInstance', 'ngModel', 'options', modalController],
        size: options.modalSize || 'sm',
        resolve: {
          ngModel: function() {
            return ngModel;
          },
          options: function() {
            return options;
          }
        }
      });
      return result.result;
    };
    return this;
  }]);

}).call(this);
;
(function() {
  angular.module('ui.cms.editable').service('uiceAggridColumnsEditorModal', ["$uibModal", "$filter", function($uibModal, $filter) {
    var modalController;
    modalController = function($scope, $uibModalInstance, ngModel) {
      $scope.ngModel = $filter('orderBy')(angular.copy(ngModel), 'pinned');
      $scope.cancel = function() {
        return $uibModalInstance.dismiss('cancel');
      };
      $scope.reset = function() {
        return $uibModalInstance.close(null);
      };
      return $scope.ok = function() {
        return $uibModalInstance.close($scope.ngModel);
      };
    };
    this.open = function(ngModel, origNgModel) {
      var result;
      console.log(ngModel);
      result = $uibModal.open({
        animation: true,
        template: ('/client/cms_editable_app/_providers/tableViews/_directives/tableViewWrapperAggrid/_modals/uiceAggridColumnsEditor/modal.html', '<div class="modal-header"><button class="close" ng-click="cancel()"><span>&times;</span></button><h4 class="modal-title" translate="">Visible columns</h4></div><div class="modal-body"><div class="checkbox" ng-repeat="col in ngModel" ng-class="{\'nomargin-top\': $first, \'nomargin-bottom\': $last}"><label><input ng-model="ngModel[$index].visible" ng-disabled="col.pinned" type="checkbox"/>{{col.colDef.headerName}}</label></div></div><div class="modal-footer"><button class="btn btn-success" ng-click="ok()" translate="">Ok</button><button class="btn btn-danger" ng-click="reset()" translate="">Reset</button><button class="btn btn-default" ng-click="cancel()" translate="">Cancel</button></div>' + ''),
        controller: ['$scope', '$uibModalInstance', 'ngModel', modalController],
        size: 'sm',
        resolve: {
          ngModel: function() {
            return ngModel;
          }
        }
      });
      return result.result;
    };
    return this;
  }]);

}).call(this);
;
(function() {
  angular.module('ui.cms.editable').directive('tableViewWrapperAggrid', ["$injector", "$rootScope", function($injector, $rootScope) {
    var $cms, $compile, $countries, $currentUser, $document, $filter, $gdprStorage, $interpolate, $langPicker, $models, $state, $tableView, $timeout, $window, AggridHelpers, H, NAMED_COLUMNS, aggridLib, agridFiltersGenerator, columnHelpers, findHtmlElementsByClass, generateCategoryTitleColumnDef, ngFilter, uiceAggridColumnsEditorModal, uiceItemsExportModal;
    $tableView = null;
    $window = null;
    $document = null;
    $filter = null;
    $langPicker = null;
    $compile = null;
    $cms = null;
    $models = null;
    $timeout = null;
    $interpolate = null;
    $state = null;
    $currentUser = null;
    $countries = null;
    $gdprStorage = null;
    H = null;
    AggridHelpers = null;
    uiceItemsExportModal = null;
    uiceAggridColumnsEditorModal = null;
    ngFilter = null;
    agridFiltersGenerator = null;
    columnHelpers = null;
    aggridLib = null;
    generateCategoryTitleColumnDef = function(modelName) {
      return {
        headerName: 'Category',
        width: 200,
        field: 'category',
        valueGetter: function(params, $scope, helpers) {
          var category, node;
          node = params.data;
          if (!node.data.category) {
            return '';
          }
          if ((node.data.$category || {}).title) {
            return columnHelpers.filters.l10n(node.data.$category.title, helpers.forceLang) || '';
          }
          category = $models[modelName].findItemByField('id', node.data.category);
          if (category) {
            return columnHelpers.filters.l10n(category.title, helpers.forceLang) || '';
          }
          return '';
        }
      };
    };
    findHtmlElementsByClass = function(jqParent, tagName, className) {
      var child, len, m, ref, res;
      res = [];
      ref = jqParent.find(tagName);
      for (m = 0, len = ref.length; m < len; m++) {
        child = ref[m];
        if ((child.className || '').includes(className)) {
          res.push(child);
        }
      }
      return res;
    };
    NAMED_COLUMNS = {
      $select: {
        headerName: '',
        pinned: 'left',
        checkboxSelection: true,
        suppressFilter: true,
        suppressSorting: true,
        width: 22
      },
      id: {
        headerName: "Id",
        field: 'id',
        width: 80
      },
      '$select+id': {
        headerName: 'Id',
        field: 'id',
        pinned: 'left',
        checkboxSelection: true,
        suppressFilter: true,
        width: 80
      },
      name: {
        headerName: 'Name',
        field: 'name',
        width: 280
      },
      title: {
        headerName: "Title",
        field: 'title',
        l10n: true,
        width: 250
      },
      state: {
        headerName: 'State',
        field: 'isPublished',
        width: 57,
        suppressFilter: true,
        suppressSorting: true,
        cellClass: 'text-center',
        cellRenderer: function(node) {
          if (node.data.isPublished) {
            return '<span class="far fa-check-circle text-success"></span>';
          }
          return '<span class="far fa-times-circle text-danger"></span>';
        }
      },
      created: {
        headerName: 'Created',
        field: 'created',
        width: 122,
        filter: 'date',
        valueFormatter: function(params, $scope, h) {
          if (!params.value) {
            return '';
          }
          return h.filters.date(params.value, 'short');
        }
      },
      publicationDate: {
        headerName: 'Published',
        field: 'publicationDate',
        filter: 'date',
        width: 148,
        valueFormatter: function(params, $scope, h) {
          if (!params.value) {
            return '';
          }
          return h.filters.date(params.value, 'short');
        }
      },
      categoryTitleForCmsProduct: generateCategoryTitleColumnDef('CmsCategoryForProduct'),
      price: {
        headerName: 'Price',
        field: 'price',
        width: 90,
        filter: 'number',
        valueGetter: function(params) {
          return params.data.price || 0;
        },
        cellRenderer: function(node) {
          return "{{" + node.value + " | humanCurrency}}";
        }
      },
      totalPrice: {
        headerName: 'Price',
        field: 'totalPrice',
        width: 90,
        filter: 'number',
        valueGetter: function(params) {
          return params.data.totalPrice || 0;
        },
        cellRenderer: function(node) {
          return "{{" + node.value + " | humanCurrency}}";
        }
      },
      owner: {
        headerName: 'Owner',
        field: 'owner',
        width: 250,
        valueGetter: function(params, scope, h) {
          var ownerId;
          ownerId = params.data.owner || params.data.ownerId;
          if (!ownerId) {
            return '';
          }
          if (!params.data.$owner) {
            params.data.$owner = h.$models.CmsUser.findItemByField('id', ownerId);
          }
          if (params.data.$owner) {
            return params.data.$owner.name || params.data.$owner.username || '';
          }
          return '';
        }
      }
    };
    return {
      restrict: 'A',
      scope: {
        items: '=?',
        forceLang: '=?',
        searchTerm: '=?',
        onItemClick: '=?',
        onItemsSelect: '=?',
        columns: '=?',
        toolbar: '=?',
        options: '=?',
        sort: '@?',
        queryFilter: '=?',
        loadItemsFn: '=?',
        countItemsFn: '=?'
      },
      link: function($scope, $element, $attrs, ctrls) {
        var LoadingOverlayComponent, NoRowsOverlayComponent, aggridElement, aggridFooterElement, base, btn_attrs, c, columnDefs, countItems, decorator, defaultColumnDefs, footerColumnDefs, getStateParams, gridOptions, gridOptionsFooter, gridParams, i, isAgGridInitialized, isLoadingItemsFromServer, item, itemsPerPage, lastRestFilter, len, len1, len2, len3, len4, loadItemsFromServer, m, n, name, onItemClick, onItemClickFn, onItemsSelectFn, options, p, paginationElement, q, ref, rowHeight, s, saveStateParams, setColumnsWidth, setGridHeight, setResourceModelName, skipRowSelect, stateParams, t, tEl, template, toolbar, toolbarElements, toolbar_template, translateColumnsHeaders, treedata, wrapped;
        if (!$tableView) {
          $tableView = $injector.get('$tableView');
          $window = $injector.get('$window');
          $document = $injector.get('$document');
          $filter = $injector.get('$filter');
          $langPicker = $injector.get('$langPicker');
          $compile = $injector.get('$compile');
          $cms = $injector.get('$cms');
          $models = $injector.get('$models');
          $timeout = $injector.get('$timeout');
          $interpolate = $injector.get('$interpolate');
          $state = $injector.get('$state');
          $currentUser = $injector.get('$currentUser');
          $countries = $injector.get('$countries');
          $gdprStorage = $injector.get('$gdprStorage');
          H = $injector.get('H');
          AggridHelpers = $injector.get('AggridHelpers');
          uiceItemsExportModal = $injector.get('uiceItemsExportModal');
          uiceAggridColumnsEditorModal = $injector.get('uiceAggridColumnsEditorModal');
          ngFilter = $filter('filter');
          agridFiltersGenerator = $injector.get('agridFiltersGenerator');
          aggridLib = $injector.get('$uicIntegrations').getThirdPartyLibrary('aggrid');
          columnHelpers = {
            $models: $models,
            $filter: $filter,
            $state: $state,
            $currentUser: $currentUser,
            $countries: $countries,
            $injector: $injector,
            filters: {
              translate: $filter('translate'),
              l10n: $filter('l10n'),
              date: $filter('date'),
              time: $filter('time'),
              number: $filter('number'),
              humanCurrency: $filter('humanCurrency'),
              toYesNo: $filter('toYesNo'),
              constantToText: $filter('constantToText')
            }
          };
        }
        toolbarElements = findHtmlElementsByClass($element, 'ul', 'aggrid-toolbar');
        paginationElement = $element.find('uic-pagination')[0];
        aggridElement = findHtmlElementsByClass($element, 'div', 'ag-body-main')[0];
        aggridFooterElement = findHtmlElementsByClass($element, 'div', 'ag-footer-aggregation')[0];
        if (!aggridFooterElement || !aggridElement || !toolbarElements.length) {
          throw "tableViewWrapperAggrid: Can't initialize directive. Please provide div with classes 'ag-body-main', 'ag-footer-aggregation' and ul with class 'aggrid-toolbar'";
        }
        options = $scope.options || {};
        options.themeName || (options.themeName = 'ag-theme-default');
        options.idField || (options.idField = 'id');
        rowHeight = options.rowHeight || 30;
        isAgGridInitialized = false;
        skipRowSelect = false;
        onItemClickFn = $scope.$parent.$parent.$eval($attrs.onItemClick);
        onItemsSelectFn = $scope.$parent.$parent.$eval($attrs.onItemsSelect);
        if ($attrs.itemsPerPage.indexOf(',') > -1) {
          itemsPerPage = $attrs.itemsPerPage.split(',').map(function(o) {
            if (o === 'Infinity') {
              return 2e308;
            }
            return parseInt(o);
          });
        } else {
          if ($attrs.itemsPerPage === 'Infinity') {
            itemsPerPage = 2e308;
          } else {
            itemsPerPage = parseInt($attrs.itemsPerPage);
          }
        }
        if (angular.isArray(itemsPerPage)) {
          $scope.ITEMS_PER_PAGE = itemsPerPage;
          itemsPerPage = $scope.ITEMS_PER_PAGE[0];
        }
        options.onItemClickAllowSingleClick = true;
        if (!angular.isDefined(options.stateParamsNames)) {
          options.stateParamsNames = {};
        }
        $scope.$origScope = H.findOrigScope($scope.$parent);
        $scope.themeName = options.themeName;
        $scope.$cms = $cms;
        $scope.$langPicker = $langPicker;
        $scope.$currentUser = $currentUser;
        $scope.isFiltered = false;
        $scope.selectedItems = [];
        $scope.pagination = {
          current: 1,
          total: 1
        };
        if (options.allowGdprStorageForState) {
          $gdprStorage.tableViewAggridColumnsFilterData || ($gdprStorage.tableViewAggridColumnsFilterData = {});
          (base = $gdprStorage.tableViewAggridColumnsFilterData)[name = $attrs.suffix] || (base[name] = {});
          if ($gdprStorage.tableViewAggridColumnsFilterData[$attrs.suffix].isActive) {
            options.$storageForState = 'gdprStorage';
          }
        }
        saveStateParams = function(stateParams) {
          stateParams = AggridHelpers.saveStateParams(stateParams, options.stateParamsNames);
          if (options.$storageForState === 'gdprStorage') {
            $gdprStorage.tableViewAggridColumnsFilterData[$attrs.suffix].stateParams = angular.copy(stateParams);
          }
          return stateParams;
        };
        getStateParams = function() {
          var stateParams, urlParams;
          if (options.$storageForState === 'gdprStorage') {
            stateParams = $gdprStorage.tableViewAggridColumnsFilterData[$attrs.suffix].stateParams || {};
            stateParams = angular.copy(stateParams);
            stateParams.filter || (stateParams.filter = {});
            stateParams.selectedItems || (stateParams.selectedItems = []);
            stateParams.sort || (stateParams.sort = {});
            urlParams = AggridHelpers.getStateParams(options.stateParamsNames);
            stateParams.page = urlParams.page || 1;
            return stateParams;
          }
          return AggridHelpers.getStateParams(options.stateParamsNames);
        };
        toolbar = $scope.toolbar || [];
        $scope._toolbar = {
          isVisible: !angular.isEmpty(toolbar),
          itemsPerPage: toolbar.includes('itemsPerPage') && ($scope.ITEMS_PER_PAGE && $scope.ITEMS_PER_PAGE.length),
          selectAllBtnIsVisible: toolbar.includes('selectAll') || !angular.isEmpty(toolbar),
          exportXlsBtnIsVisible: toolbar.includes('exportXls'),
          createNewIsVisible: toolbar.includes('createNew'),
          reloadDataIsVisible: toolbar.includes('reloadData') && options.serverPagination,
          resetFilterBtnIsVisible: toolbar.includes('resetFilter'),
          tableSettings: toolbar.includes('tableSettings')
        };
        for (m = 0, len = toolbar.length; m < len; m++) {
          t = toolbar[m];
          if (!(angular.isString(t) && t.includes('exportXls'))) {
            continue;
          }
          $scope._toolbar.exportXlsBtnIsVisible = true;
          $scope._toolbar.exportXlsModelName = t.split(':')[1];
          break;
        }
        if ($scope._toolbar.isVisible) {
          toolbar_template = [];
          $scope._toolbar.$onClick = {};
          decorator = function(fn) {
            return function() {
              skipRowSelect = true;
              fn(gridOptions, $scope);
              skipRowSelect = false;
            };
          };
          for (i = n = 0, len1 = toolbar.length; n < len1; i = ++n) {
            item = toolbar[i];
            if (!(angular.isObject(item))) {
              continue;
            }
            template = '';
            if (!AggridHelpers.isColumnEnabled(item, $scope.$origScope, columnHelpers)) {
              continue;
            }
            if (angular.isFunction(item.template)) {
              template = item.template();
            } else if (angular.isString(item.template)) {
              template = item.template;
            }
            btn_attrs = "";
            if (item["if"]) {
              btn_attrs = "ng-if='" + item["if"] + "'";
            }
            if (angular.isFunction(item.onClick) && template) {
              $scope._toolbar.$onClick['cb' + i] = decorator(item.onClick);
              toolbar_template.push("<li ng-click=\"_toolbar.$onClick.cb" + i + "()\" " + btn_attrs + ">\n    " + template + "\n</li>");
            } else if (template) {
              toolbar_template.push("<li " + btn_attrs + ">" + template + "</li>");
            }
          }
          if (toolbar_template.length) {
            for (p = 0, len2 = toolbarElements.length; p < len2; p++) {
              tEl = toolbarElements[p];
              $compile(angular.element(tEl).append(toolbar_template.join('')))($scope);
            }
          }
        }
        columnDefs = [];
        ref = angular.copy($scope.columns) || [];
        for (i = q = 0, len3 = ref.length; q < len3; i = ++q) {
          c = ref[i];
          if (angular.isString(c) && NAMED_COLUMNS[c]) {
            c = angular.copy(NAMED_COLUMNS[c]);
          }
          if (typeof c !== 'object') {
            continue;
          }
          if (!AggridHelpers.isColumnEnabled(c, $scope.$origScope, columnHelpers)) {
            continue;
          }
          if (!c.field) {
            c.field = "AUTOFIELD_NAME_" + i;
          } else if (c.field && !c.restField) {
            c.restField = c.field;
          }
          c.colId = c.colId || c.field;
          c._headerTooltip = c.headerTooltip || null;
          delete c.headerTooltip;
          columnDefs.push(c);
        }
        defaultColumnDefs = angular.copy(columnDefs);
        wrapped = AggridHelpers.wrapColumnDefs(columnDefs, options, $scope, columnHelpers);
        options = wrapped.options;
        columnDefs = AggridHelpers.loadColumnDefs(wrapped.columnDefs, $attrs.suffix);
        options.allowAddColumnWidthPadding = !columnDefs.saved.length;
        itemsPerPage = AggridHelpers.loadItemsPerPage($attrs.suffix) || itemsPerPage;
        $scope.itemsPerPage = itemsPerPage;
        if (options.allowTreeData && !options.treeDataParams.hasCellRenderer) {
          for (i = s = 0, len4 = columnDefs.length; s < len4; i = ++s) {
            c = columnDefs[i];
            if (!(c.field === options.idField)) {
              continue;
            }
            columnDefs[i].cellRenderer = 'group';
            options.treeDataParams.hasCellRenderer = true;
            break;
          }
          if (!options.treeDataParams.hasCellRenderer) {
            columnDefs[0].cellRenderer = 'group';
          }
        }
        stateParams = getStateParams();
        if ($attrs.sortField && (!stateParams.sort || !stateParams.sort.length)) {
          stateParams.sort = [{}];
          if ($attrs.sortField[0] === '-') {
            stateParams.sort[0].colId = $attrs.sortField.substr(1);
            stateParams.sort[0].sort = 'desc';
          } else {
            stateParams.sort[0].sort = 'asc';
            stateParams.sort[0].colId = $attrs.sortField;
          }
          saveStateParams(stateParams);
        }
        NoRowsOverlayComponent = (function() {
          function NoRowsOverlayComponent() {}

          NoRowsOverlayComponent.prototype.init = function() {
            this.eGui = document.createElement('div');
            this.eGui.innerHTML = "<uice-no-items search-term=\"searchTerm\"\n               is-filtered=\"isFiltered\"\n               reset-filter=\"resetAggridFilter()\"\n               >\n</uice-no-items>";
            $compile(this.eGui)($scope);
          };

          NoRowsOverlayComponent.prototype.getGui = function() {
            return this.eGui;
          };

          return NoRowsOverlayComponent;

        })();
        LoadingOverlayComponent = (function() {
          function LoadingOverlayComponent() {}

          LoadingOverlayComponent.prototype.init = function() {
            this.eGui = document.createElement('div');
            this.eGui.style.position = 'relative';
            this.eGui.style.height = '5em';
            this.eGui.innerHTML = "<div class='preloader'\n     style='position: absolute; background: transparent; background-color: transparent;'\n     >\n</div>";
          };

          LoadingOverlayComponent.prototype.getGui = function() {
            return this.eGui;
          };

          return LoadingOverlayComponent;

        })();
        countItems = function(items) {
          var count;
          if (options.allowTreeData) {
            count = 0;
            (items || []).forEach(function(item) {
              var l;
              count += 1;
              if (!item) {
                return;
              }
              l = (item[options.treeDataParams.childrenField] || []).length;
              if (angular.isNumber(l) && !isNaN(l)) {
                count += l;
              }
            });
            return count;
          }
          return (items || []).length;
        };
        gridParams = {
          $scope: $scope,
          $compile: $compile,
          angularCompileHeaders: true
        };
        gridOptions = {
          columnDefs: columnDefs,
          rowData: [],
          rowHeight: rowHeight,
          pagination: true,
          paginationPageSize: itemsPerPage,
          rowSelection: 'multiple',
          enableSorting: true,
          enableColResize: true,
          unSortIcon: true,
          enableFilter: true,
          suppressCellSelection: true,
          suppressDragLeaveHidesColumns: true,
          suppressMenuHide: true,
          suppressPaginationPanel: true,
          angularCompileFilters: true,
          angularCompileRows: true,
          angularCompileHeaders: true,
          isExternalFilterPresent: function() {
            return true;
          },
          doesExternalFilterPass: function(node) {
            var l;
            if ($scope.searchTerm && !options.serverPagination) {
              l = ngFilter([node.data], $scope.searchTerm);
              return !!l.length;
            }
            return true;
          },
          noRowsOverlayComponent: NoRowsOverlayComponent,
          loadingOverlayComponent: LoadingOverlayComponent,
          onGridReady: function() {
            gridOptions.api.setHeaderHeight(rowHeight);
            return $timeout(function() {
              var el_width, table_width;
              gridOptions.api.setFilterModel(stateParams.filter);
              gridOptions.api.setSortModel(stateParams.sort);
              isAgGridInitialized = true;
              $scope.$apply(function() {
                $scope.isFiltered = AggridHelpers.isAgridFiltered(stateParams.filter);
                if (options.serverPagination) {
                  return loadItemsFromServer();
                }
              });
              el_width = aggridElement.offsetWidth;
              if (!el_width) {
                return;
              }
              table_width = 0;
              gridOptions.columnApi.getAllDisplayedColumns().forEach(function(c) {
                table_width += c.actualWidth || 0;
              });
              if (el_width > table_width) {
                return gridOptions.api.sizeColumnsToFit();
              }
            }, 0);
          },
          onRowDoubleClicked: function(node) {
            return onItemClick(node.data);
          },
          onRowSelected: function(node) {
            if (skipRowSelect) {
              return;
            }
            node = node.node;
            return $scope.$apply(function() {
              var _node, child_ids, childs, len5, len6, len7, len8, model, ref1, ref2, ref3, ref4, u, w, x, y;
              if (node.selected) {
                if (options.allowTreeData) {
                  childs = node.data[options.treeDataParams.childrenField] || [];
                  child_ids = childs.map(function(o) {
                    return o[options.idField];
                  });
                  $scope.selectedItems = $scope.selectedItems.filter(function(o) {
                    return !child_ids.includes(o[options.idField]);
                  });
                  $scope.selectedItems = $scope.selectedItems.concat(childs);
                  child_ids = childs.map(function(o) {
                    return o[options.idField];
                  });
                  model = gridOptions.api.getModel();
                  skipRowSelect = true;
                  ref1 = model.rowsAfterFilter || model.rowsToDisplay;
                  for (u = 0, len5 = ref1.length; u < len5; u++) {
                    _node = ref1[u];
                    if (child_ids.includes(_node.data[options.idField])) {
                      _node.setSelected(true);
                    }
                  }
                  skipRowSelect = false;
                }
                $scope.selectedItems.push(node.data);
              } else {
                ref2 = $scope.selectedItems;
                for (i = w = 0, len6 = ref2.length; w < len6; i = ++w) {
                  item = ref2[i];
                  if (item[options.idField] === node.data[options.idField]) {
                    $scope.selectedItems.splice(i, 1);
                    break;
                  }
                }
                if (options.allowTreeData) {
                  childs = node.data[options.treeDataParams.childrenField] || [];
                  child_ids = childs.map(function(o) {
                    return o[options.idField];
                  });
                  $scope.selectedItems = $scope.selectedItems.filter(function(o) {
                    return !child_ids.includes(o[options.idField]);
                  });
                  model = gridOptions.api.getModel();
                  skipRowSelect = true;
                  ref3 = model.rowsAfterFilter || model.rowsToDisplay;
                  for (x = 0, len7 = ref3.length; x < len7; x++) {
                    _node = ref3[x];
                    if (child_ids.includes(_node.data[options.idField])) {
                      _node.setSelected(false);
                    }
                  }
                  skipRowSelect = false;
                  if (!node.data[options.treeDataParams.childrenField] && !node.data[options.treeDataParams.groupField]) {
                    model = gridOptions.api.getModel();
                    skipRowSelect = true;
                    ref4 = model.rowsAfterFilter || model.rowsToDisplay;
                    for (y = 0, len8 = ref4.length; y < len8; y++) {
                      _node = ref4[y];
                      if (!_node.data[options.treeDataParams.childrenField] && !node.data[options.treeDataParams.groupField]) {
                        continue;
                      }
                      if (_node.data[options.treeDataParams.childrenField].findByProperty(options.idField, node.data[options.idField])) {
                        $scope.selectedItems = $scope.selectedItems.filter(function(item) {
                          return item[options.idField] !== _node.data[options.idField];
                        });
                        _node.setSelected(false);
                        break;
                      }
                    }
                    skipRowSelect = false;
                  }
                }
              }
              setResourceModelName($scope.selectedItems[0]);
            });
          },
          saveStateParams: function() {
            var page;
            if (isAgGridInitialized) {
              if (stateParams.page > $scope.pagination.current) {
                page = stateParams.page;
              } else {
                page = $scope.pagination.current;
              }
              stateParams = saveStateParams({
                filter: gridOptions.api.getFilterModel(),
                sort: gridOptions.api.getSortModel(),
                page: page
              });
              $scope.isFiltered = AggridHelpers.isAgridFiltered(stateParams.filter);
            }
          },
          onFilterChanged: function(skipLoadFromServer) {
            gridOptions._onAfterFilterChanged(skipLoadFromServer === true);
          },
          onAfterFilterChanged: function(skipLoadFromServer) {
            gridOptions._onAfterFilterChanged(skipLoadFromServer === true);
          },
          _onAfterFilterChanged: function(skipLoadFromServer) {
            var filteredItems;
            if (options.serverPagination && !skipLoadFromServer) {
              loadItemsFromServer();
            }
            filteredItems = gridOptions.api.inMemoryRowModel.rowsToDisplay;
            if (!filteredItems.length) {
              setGridHeight('18em');
              if (!isLoadingItemsFromServer) {
                gridOptions.api.showNoRowsOverlay();
              }
            } else {
              gridOptions.api.hideOverlay();
              setGridHeight();
            }
            gridOptions.saveStateParams();
            if (angular.isFunction(options.onAfterFilterChanged)) {
              options.onAfterFilterChanged(gridOptions, $scope, $origScope);
            }
          },
          _onAfterSortChanged: function(skipLoadFromServer) {
            if (options.serverPagination && !skipLoadFromServer) {
              loadItemsFromServer();
            }
            gridOptions.saveStateParams();
          },
          onSortChanged: function() {
            gridOptions._onAfterSortChanged();
          }
        };
        ['onColumnEverythingChanged', 'onColumnResized', 'onColumnMoved'].forEach(function(ev) {
          return gridOptions[ev] = function() {
            AggridHelpers.saveColumnDefs(gridOptions, $attrs.suffix);
          };
        });
        if (angular.isFunction(options.getRowHeight)) {
          gridOptions.getRowHeight = function(params) {
            params.context = params.context || {};
            params.context.rowHeight = rowHeight;
            return options.getRowHeight(params, $scope.$origScope, columnHelpers) || rowHeight;
          };
        }
        if (angular.isFunction(options.getRowClass)) {
          gridOptions.getRowClass = function(params) {
            return options.getRowClass(params, $scope.$origScope, columnHelpers) || '';
          };
        }
        if (options.onItemClickAllowSingleClick && !$scope._toolbar.isVisible) {
          gridOptions.onRowSelected = function(node) {
            onItemClick(node.node.data);
          };
        }
        if (!options.serverPagination) {
          gridOptions.onPaginationChanged = function() {
            $scope.pagination = {
              current: gridOptions.api.paginationGetCurrentPage() + 1,
              total: gridOptions.api.paginationGetTotalPages() || 1
            };
            if (options.fillViewport && $scope.pagination.total > 1) {
              setGridHeight();
            }
          };
        }
        if (options.allowTreeData) {
          treedata = options.treeDataParams || {};
          treedata.groupField = treedata.groupField || 'groupName';
          treedata.childrenField = treedata.childrenField || 'children';
          if (!angular.isDefined(treedata.expanded)) {
            treedata.expanded = false;
          }
          treedata.expanded = !!treedata.expanded;
          gridOptions.getNodeChildDetails = function(item) {
            if (item[treedata.childrenField] && item[treedata.childrenField].length) {
              return {
                group: true,
                expanded: treedata.expanded,
                children: item[treedata.childrenField],
                key: item[treedata.groupField]
              };
            }
            return null;
          };
        }
        translateColumnsHeaders = function(lang) {
          var len5, ref1, u;
          ref1 = gridOptions.columnDefs;
          for (i = u = 0, len5 = ref1.length; u < len5; i = ++u) {
            c = ref1[i];
            if (angular.isString(c._headerName)) {
              gridOptions.columnDefs[i].headerName = columnHelpers.filters.translate(c._headerName);
            } else {
              gridOptions.columnDefs[i].headerName = columnHelpers.filters.l10n(c._headerName);
            }
            if (options.autogeneratedTooltips && !c._headerTooltip) {
              gridOptions.columnDefs[i].headerTooltip = gridOptions.columnDefs[i].headerName;
            } else if (c._headerTooltip) {
              c.headerTooltip = c._headerTooltip;
            }
          }
        };
        setColumnsWidth = function() {

          /*
              функция для определения ширины колонок. часто темы добавляют паддинги,
              для этого создаем кусок html-кода с нужными классами и находим их
           */
          var fakeAggridTableElement, fakeCellElement, l, len5, len6, paddingLeft, paddingRight, r, ref1, ref2, u, w;
          if (!options.allowAddColumnWidthPadding) {
            return;
          }
          paddingLeft = 0;
          paddingRight = 0;
          if ($window.getComputedStyle) {
            fakeAggridTableElement = angular.element("<div class=\"ag-bootstrap\" style='display: none;'>\n  <div class=\"" + options.themeName + "\">\n    <div class=\"ag-header\">\n      <div class=\"ag-header-viewport\">\n        <div class=\"ag-header-container\">\n          <div class=\"ag-header-row\">\n            <ag-fake-cell class=\"ag-header-cell\" style='display: block;'></ag-fake-cell>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>");
            $document[0].body.appendChild(fakeAggridTableElement[0]);
            fakeCellElement = fakeAggridTableElement.find('ag-fake-cell')[0];
            r = $window.getComputedStyle(fakeCellElement, null).getPropertyValue('padding-right') || '';
            paddingRight = H.convert.toInt(r.replace('px', ''), 0);
            l = $window.getComputedStyle(fakeCellElement, null).getPropertyValue('padding-left') || '';
            paddingLeft = H.convert.toInt(l.replace('px', ''), 0);
            fakeAggridTableElement[0].parentNode.removeChild(fakeAggridTableElement[0]);
          }
          ref1 = gridOptions.columnDefs;
          for (u = 0, len5 = ref1.length; u < len5; u++) {
            c = ref1[u];
            if (c.width) {
              c.width = paddingLeft + c.width + paddingRight;
            }
          }
          ref2 = (gridOptionsFooter || {}).columnDefs || [];
          for (i = w = 0, len6 = ref2.length; w < len6; i = ++w) {
            c = ref2[i];
            if (c.width) {
              c.width = paddingLeft + c.width + paddingRight;
            }
          }
        };
        setGridHeight = function(h) {
          var count, pagination;
          if (options.fillViewport) {
            setTimeout(function() {
              var elementRect, len5, paginationRect, toolbarRect, u;
              elementRect = $element[0].getBoundingClientRect();
              paginationRect = paginationElement.getBoundingClientRect();
              toolbarRect = {
                height: 0
              };
              for (u = 0, len5 = toolbarElements.length; u < len5; u++) {
                tEl = toolbarElements[u];
                if (tEl.className.includes('dropdown-menu')) {
                  toolbarRect.height += tEl.parentElement.getBoundingClientRect().height;
                } else {
                  toolbarRect.height += tEl.getBoundingClientRect().height;
                }
              }
              h = elementRect.height - toolbarRect.height - paginationRect.height;
              if (options.allowFooterAggregation) {
                h -= rowHeight;
              }
              aggridElement.style.height = h + 'px';
            }, 100);
            return;
          }
          if (angular.isString(h)) {
            return aggridElement.style.height = h;
          }
          pagination = gridOptions.api.paginationService || gridOptions.api.paginationProxy;
          count = pagination.bottomRowIndex - pagination.topRowIndex + 1;
          if (count < 8) {
            return aggridElement.style.height = '18em';
          }
          h = count * rowHeight + 30 + rowHeight;
          aggridElement.style.height = h + 'px';
        };
        onItemClick = function(item) {
          var ids, len5, node, ref1, res, u;
          if ($scope.ngDisabled) {
            return;
          }
          if ($scope.onItemClick) {
            ids = [];
            ref1 = gridOptions.api.inMemoryRowModel.rowsToDisplay || [];
            for (u = 0, len5 = ref1.length; u < len5; u++) {
              node = ref1[u];
              if (node.data && node.data[options.idField]) {
                ids.push(node.data[options.idField]);
              }
            }
            res = onItemClickFn({
              item: item,
              ids: ids
            });
            if (res && res.then) {
              return res.then(function() {
                if (gridOptions.api) {
                  return gridOptions.api.refreshView();
                }
              });
            }
          }
        };
        setResourceModelName = function(item) {
          if (!item) {
            $scope.MODEL_NAME = '';
            return;
          }
          if (item.__proto__.constructor) {
            $scope.MODEL_NAME = item.__proto__.constructor.modelName;
          }
        };
        if (options.allowFooterAggregation) {
          footerColumnDefs = columnDefs.map(function(c, i) {
            var coldef;
            coldef = {
              field: c.field,
              valueGetter: function(params) {
                return params.data[i];
              }
            };
            if (c.pinned) {
              coldef.pinned = c.pinned;
            }
            if (c.width) {
              coldef.width = c.width;
            }
            if (c.footerAggFormatter) {
              coldef.valueFormatter = function(params) {
                return c.footerAggFormatter(params, $scope.$origScope, columnHelpers);
              };
            }
            return coldef;
          });
          gridOptionsFooter = {
            rowHeight: rowHeight,
            rowData: [],
            headerHeight: 0,
            suppressHorizontalScroll: true,
            suppressCellSelection: true,
            suppressDragLeaveHidesColumns: true,
            suppressMenuHide: true,
            suppressPaginationPanel: true,
            suppressRowClickSelection: true,
            alignedGrids: [gridOptions],
            columnDefs: footerColumnDefs
          };
          gridOptions.alignedGrids = [gridOptionsFooter];
          gridOptions.onFilterChanged = function(skipLoadFromServer) {
            var j, len5, summary, summaryValues, u;
            gridOptions._onAfterFilterChanged(skipLoadFromServer === true);
            summaryValues = {};
            this.api.forEachNodeAfterFilter(function(node, i) {
              var j, len5, u, valueGetter;
              for (j = u = 0, len5 = columnDefs.length; u < len5; j = ++u) {
                c = columnDefs[j];
                if (!(c.footerAggFunc && c.visible)) {
                  continue;
                }
                valueGetter = c.valueGetter || function(params) {
                  return angular.getValue(params.data, c.field);
                };
                if (c.footerAggFuncIgnoreChilds && !node.group) {
                  valueGetter = function() {
                    return 0;
                  };
                }
                summaryValues[j] = summaryValues[j] || [];
                summaryValues[j].push(valueGetter(node));
              }
            });
            if (angular.isEmpty(summaryValues)) {
              gridOptionsFooter.api.setRowData([]);
              aggridFooterElement.style.height = "0px";
              return;
            }
            summary = {};
            for (j = u = 0, len5 = columnDefs.length; u < len5; j = ++u) {
              c = columnDefs[j];
              if (c.footerAggFunc && c.visible) {
                summary[j] = c.footerAggFunc(summaryValues[j]);
              }
            }
            gridOptionsFooter.api.setRowData([summary]);
            aggridFooterElement.style.height = rowHeight + "px";
          };
        }
        lastRestFilter = null;
        isLoadingItemsFromServer = false;
        loadItemsFromServer = function(forceLoadFromServer) {
          var onFail, queryFilter, queryFilterDump;
          onFail = function() {
            if (gridOptions.api) {
              gridOptions.api.hideOverlay();
            }
            $scope.items = [];
            $cms.showNotification("Can't download items", {}, 'error');
            isLoadingItemsFromServer = false;
          };
          queryFilter = AggridHelpers.generateRestFilter(columnDefs, options, $scope.queryFilter, gridOptions.api.getFilterModel(), gridOptions.api.getSortModel());
          queryFilter.quickFilter = $scope.searchTerm || '';
          queryFilterDump = JSON.stringify(queryFilter);
          if (lastRestFilter === queryFilterDump && !forceLoadFromServer) {
            return;
          }
          lastRestFilter = JSON.stringify(queryFilter);
          gridOptions.api.showLoadingOverlay();
          isLoadingItemsFromServer = true;
          return $scope.countItemsFn(queryFilter.where, queryFilter.quickFilter).then(function(data) {
            var count;
            if (lastRestFilter !== queryFilterDump) {
              return;
            }
            if (data && data.hasOwnProperty('count')) {
              count = data.count;
            } else {
              count = data;
            }
            if (!count) {
              $scope.items = [];
              $scope.selectedItems = [];
              $scope.pagination = {
                current: 1,
                total: 1
              };
              if (gridOptions.api) {
                gridOptions.api.hideOverlay();
                gridOptions.api.showNoRowsOverlay();
              }
              isLoadingItemsFromServer = false;
              return;
            }
            $scope.pagination.total = (count - count % itemsPerPage) / itemsPerPage;
            if (count % itemsPerPage > 0) {
              $scope.pagination.total += 1;
            }
            if ($scope.pagination.current > $scope.pagination.total) {
              $scope.pagination.current = $scope.pagination.total;
            }
            queryFilter.skip = ($scope.pagination.current - 1) * itemsPerPage;
            queryFilter.limit = itemsPerPage;
            return $scope.loadItemsFn(queryFilter, queryFilter.quickFilter).then(function(data) {
              if (lastRestFilter !== queryFilterDump) {
                return;
              }
              isLoadingItemsFromServer = false;
              $scope.items = data;
              $scope.selectedItems = [];
              if (gridOptions.api) {
                gridOptions.api.hideOverlay();
                return gridOptions.api.paginationSetPageSize(countItems(data));
              }
            }, onFail);
          }, onFail);
        };

        /*
                загруза самого aggrid и подключение его к dom
         */
        return aggridLib.load().then(function() {
          var col, e, grid, gridFooter, len5, len6, len7, ref1, u, w, x;
          ref1 = [aggridElement, aggridFooterElement];
          for (u = 0, len5 = ref1.length; u < len5; u++) {
            e = ref1[u];
            e.className = e.className.replace("{{:: themeName}}", options.themeName).replace("{{::themeName}}", options.themeName).replace("{{ themeName }}", options.themeName).replace("{{themeName}}", options.themeName);
          }
          translateColumnsHeaders();
          setColumnsWidth();
          grid = new agGrid.Grid(aggridElement, gridOptions, gridParams);
          for (w = 0, len6 = columnDefs.length; w < len6; w++) {
            col = columnDefs[w];
            if (!col.visible) {
              gridOptions.columnApi.setColumnVisible(col.colId, false);
            }
          }
          if (gridOptionsFooter) {
            gridFooter = new agGrid.Grid(aggridFooterElement, gridOptionsFooter, gridParams);
            for (x = 0, len7 = columnDefs.length; x < len7; x++) {
              col = columnDefs[x];
              if (!col.visible) {
                gridOptionsFooter.columnApi.setColumnVisible(col.colId, false);
              }
            }
          }
          if (options.fillViewport) {
            angular.element($window).on('resize', setGridHeight);
          }
          $scope.$on('$destroy', function() {
            grid.destroy();
            if (options.fillViewport) {
              angular.element($window).off('resize', setGridHeight);
            }
            if (gridFooter) {
              gridFooter.destroy();
            }
          });
          $scope.$watch('items', function(items, oldValue) {
            var gridIds, newIds, oldIds, page;
            items || (items = []);
            oldValue || (oldValue = []);
            if (items && items[0]) {
              setResourceModelName(items[0]);
            }
            newIds = (items.map(function(o) {
              return (o || {})[options.idField];
            })).filter(angular.isDefined);
            oldIds = (oldValue.map(function(o) {
              return (o || {})[options.idField];
            })).filter(angular.isDefined);
            gridIds = [];
            gridOptions.api.forEachNode(function(o) {
              return gridIds.push((o.data || {})[options.idField]);
            });
            gridIds = gridIds.filter(angular.isDefined);
            if (newIds.length && gridIds.length && angular.equals(newIds, oldIds)) {
              gridOptions.api.forEachNode(function(node) {
                var child, childrens, data, len8, y;
                data = items.findByProperty(options.idField, node.data[options.idField]);
                if (data) {
                  node.setData(data);
                } else if (!data && options.allowTreeData) {
                  for (y = 0, len8 = items.length; y < len8; y++) {
                    item = items[y];
                    if (!(item[options.treeDataParams.childrenField] || []).length) {
                      continue;
                    }
                    childrens = item[options.treeDataParams.childrenField] || [];
                    child = childrens.findByProperty(options.idField, node.data[options.idField]);
                    if (child) {
                      node.setData(child);
                      break;
                    }
                  }
                }
              });
              gridOptions.api.refreshView();
            } else {
              gridOptions.api.setRowData(items);
            }
            gridOptions.visible = true;
            if (options.serverPagination) {
              $scope.pagination.current = stateParams.page || 1;
            } else {
              gridOptions.api.setFilterModel(stateParams.filter);
              gridOptions.api.setSortModel(stateParams.sort);
              if (items && items.length) {
                page = stateParams.page || 1;
                gridOptions.api.paginationGoToPage(page - 1);
              }
              setGridHeight();
            }
            if (options.allowFooterAggregation || options.serverPagination) {
              gridOptions.onFilterChanged(true);
            }
          }, true);
          $scope.$watch('forceLang', function(forceLang, oldValue) {
            if (forceLang === oldValue) {
              return;
            }
            if (options.serverPagination) {
              loadItemsFromServer(true);
              return;
            }
            gridOptions.api.refreshView();
          });
          $scope.$watch('pagination.current', function(current, oldValue) {
            if (current === oldValue) {
              return;
            }
            if (options.serverPagination) {
              loadItemsFromServer(true);
            } else {
              gridOptions.api.paginationGoToPage(current - 1);
            }
            stateParams.page = current;
            saveStateParams(stateParams);
            window.smoothScrollTo(0);
          });
          $scope.$watch('searchTerm', function(searchTerm, oldValue) {
            gridOptions.api.onFilterChanged();
          });
          $scope.$watch('$langPicker.currentLang', function(currentLang, oldValue) {
            if (currentLang === oldValue) {
              return;
            }
            translateColumnsHeaders();
            gridOptions.api.setColumnDefs(gridOptions.columnDefs);
          });
          if (options.serverPagination) {
            $scope.$watch('queryFilter', function(queryFilter, oldValue) {
              return loadItemsFromServer();
            }, true);
          }
          if (onItemsSelectFn) {
            $scope.$watchCollection('selectedItems', function(selectedItems, oldValue) {
              onItemsSelectFn({
                items: selectedItems || []
              });
            });
          }
          $scope.forceReloadData = function() {
            if (!options.serverPagination) {
              return;
            }
            loadItemsFromServer(true);
          };
          $scope.getFilteredItems = function() {
            var fitems;
            fitems = [];
            gridOptions.api.forEachNodeAfterFilterAndSort(function(node) {
              return fitems.push(node.data);
            });
            return fitems;
          };
          $scope.setItemsPerPage = function(i) {
            $scope.itemsPerPage = i;
            itemsPerPage = i;
            AggridHelpers.saveItemsPerPage(itemsPerPage, $attrs.suffix);
            gridOptions.api.paginationSetPageSize(itemsPerPage);
            if (options.serverPagination) {
              loadItemsFromServer(true);
            } else {
              gridOptions.api.paginationGoToPage(1);
            }
          };
          $scope.getMaxSize = function() {
            if ($cms.screenSize === 'xs') {
              return 1;
            }
            return 10;
          };
          $scope.onOpenColumnsEditModal = function() {
            var defs;
            defs = gridOptions.api.columnController.primaryColumns.map(function(c) {
              var _c, len8, y;
              _c = {
                colId: c.colId,
                colDef: c.colDef,
                visible: c.visible
              };
              for (y = 0, len8 = defaultColumnDefs.length; y < len8; y++) {
                col = defaultColumnDefs[y];
                if (!(col.field === c.colDef.field)) {
                  continue;
                }
                _c.pinned = col.pinned;
                break;
              }
              return _c;
            });
            return uiceAggridColumnsEditorModal.open(defs).then(function(data) {
              if (!data) {
                return gridOptions.columnApi.resetColumnState();
              }
              data.forEach(function(col) {
                gridOptions.columnApi.setColumnVisible(col.colId, col.visible);
                if (gridFooter) {
                  return gridOptionsFooter.columnApi.setColumnVisible(col.colId, col.visible);
                }
              });
              AggridHelpers.saveColumnDefs(gridOptions, $attrs.suffix);
            });
          };
          $scope.resetAggridFilter = function() {
            stateParams = saveStateParams({});
            gridOptions.api.setFilterModel(null);
            gridOptions.api.setSortModel(null);
            $scope.isFiltered = false;
            $scope.searchTerm = '';
          };
          $scope.selectAll = function(fn) {
            var len8, model, node, ref2, selectedItems, y;
            fn = fn || function() {
              return true;
            };
            skipRowSelect = true;
            model = gridOptions.api.getModel();
            selectedItems = [];
            ref2 = model.rowsAfterFilter || model.rowsToDisplay;
            for (y = 0, len8 = ref2.length; y < len8; y++) {
              node = ref2[y];
              if (!(fn(node.data))) {
                continue;
              }
              node.setSelected(true);
              selectedItems.push(node.data);
            }
            $timeout(function() {
              skipRowSelect = false;
              $scope.selectedItems = selectedItems;
              return setResourceModelName(selectedItems[0]);
            }, 0);
          };
          $scope.deselectAll = function() {
            var model;
            skipRowSelect = true;
            model = gridOptions.api.getModel();
            (model.rowsToDisplay || []).forEach(function(node) {
              node.setSelected(false);
            });
            $timeout(function() {
              skipRowSelect = false;
              return $scope.selectedItems = [];
            }, 0);
          };
          $scope.exportToXls = function(type) {
            var aggridClientFilter, fileName, k, modelName, queryFilter, ref2, v;
            if (type === 'selected') {
              if (!$scope.selectedItems.length) {
                return;
              }
              queryFilter = AggridHelpers.generateRestFilter(columnDefs, options, $scope.queryFilter || {}, gridOptions.api.getFilterModel(), gridOptions.api.getSortModel());
              queryFilter.where || (queryFilter.where = {});
              queryFilter.where[options.idField] = {
                inq: $scope.selectedItems.map(function(o) {
                  return o[options.idField];
                })
              };
            } else {
              if (!$scope.items.length) {
                return;
              }
              queryFilter = $scope.queryFilter || {};
              if (!options.serverPagination) {
                queryFilter = {
                  where: {}
                };
                queryFilter.where[options.idField] = {
                  inq: $scope.items.map(function(o) {
                    return o[options.idField];
                  })
                };
              }
              queryFilter = AggridHelpers.generateRestFilter(columnDefs, options, queryFilter, gridOptions.api.getFilterModel(), gridOptions.api.getSortModel());
              if (type === 'page') {
                queryFilter.skip = ($scope.pagination.current - 1) * itemsPerPage;
                queryFilter.limit = itemsPerPage;
              }
            }
            modelName = $scope._toolbar.exportXlsModelName || $scope.MODEL_NAME;
            if (queryFilter && $injector.has(modelName)) {
              aggridClientFilter = {};
              ref2 = gridOptions.api.getFilterModel();
              for (k in ref2) {
                v = ref2[k];
                c = AggridHelpers.findColumn(columnDefs, k);
                if (c && c.filterParams && (c.filterParams.serverPagination === false || c.filterParams.filterStrategy === 'both' || c.filterParams.filterStrategy === 'client')) {
                  aggridClientFilter[c.field] = v;
                }
              }
            }
            fileName = null;
            if (angular.isFunction(options.fileNameForExport)) {
              fileName = options.fileNameForExport($cms.seoService.pageTitle);
            }
            uiceItemsExportModal.open(modelName, {
              queryFilter: queryFilter,
              params: {
                aggridClientFilter: aggridClientFilter
              },
              fileName: fileName
            });
          };
          $scope.isInfinity = function(value) {
            return value === 2e308;
          };
          $scope.toggleStorageForState = function() {
            if (options.$storageForState === 'gdprStorage') {
              options.$storageForState = 'url';
              $gdprStorage.tableViewAggridColumnsFilterData[$attrs.suffix].isActive = false;
            } else {
              options.$storageForState = 'gdprStorage';
              $gdprStorage.tableViewAggridColumnsFilterData[$attrs.suffix].isActive = true;
              gridOptions.saveStateParams();
            }
          };
          if (angular.isFunction($scope.$origScope.onItemAdd)) {
            $scope.createNewItem = $scope.$origScope.onItemAdd;
          }
          return $scope.getFullQueryFilter = function() {
            return AggridHelpers.generateRestFilter(columnDefs, options, $scope.queryFilter, gridOptions.api.getFilterModel(), gridOptions.api.getSortModel());
          };
        });
      },
      template: ('/client/cms_editable_app/_providers/tableViews/_directives/tableViewWrapperAggrid/tableViewWrapperAggrid.html', '<ul class="list-inline aggrid-toolbar" ng-show="_toolbar.isVisible"><li ng-if="_toolbar.selectAllBtnIsVisible"><div class="btn-group"><button class="btn btn-xs btn-info" ng-click="selectAll()"><div class="far fa-check-square"></div><span translate="">Select all</span></button><button class="btn btn-xs btn-default" ng-click="deselectAll()" title="{{\'Select none\' | translate}}"><div class="far fa-square nopadding"></div></button></div></li><li ng-if="_toolbar.tableSettings"><div uib-dropdown=""><button class="btn btn-xs btn-default" uib-dropdown-toggle=""><span translate="">Table</span><span> </span><span class="caret"></span></button><ul class="dropdown-menu dropdown-menu-sm" uib-dropdown-menu=""><li title="{{\'Click to change the way of saving filter parameters\' | translate}}" ng-show="options.allowGdprStorageForState"><a ng-click="toggleStorageForState()"><span class="far text-primary" ng-class="{\'fa-square\': options.$storageForState != \'gdprStorage\', \'fa-check-square\': options.$storageForState == \'gdprStorage\'}"></span><span translate="">Save filtering preferences in the browser</span></a></li><li><a ng-click="resetAggridFilter()"><span class="fas fa-filter"></span><span translate="">Reset filter</span></a></li><li><a ng-click="onOpenColumnsEditModal()"><span class="fas fa-columns"></span><span translate="">Columns</span></a></li></ul></div></li><li ng-if="_toolbar.itemsPerPage"><div uib-dropdown=""><button class="btn btn-xs btn-default" uib-dropdown-toggle=""><span translate="">Items on page</span><span> - {{itemsPerPage}} </span><span class="caret"></span></button><ul class="dropdown-menu dropdown-menu-sm" uib-dropdown-menu=""><li ng-repeat="i in ITEMS_PER_PAGE" ng-class="{active: i == itemsPerPage}"><a ng-click="setItemsPerPage(i)">{{i}}</a></li></ul></div></li><li ng-if="_toolbar.resetFilterBtnIsVisible"><button class="btn btn-xs btn-default" ng-click="resetAggridFilter()"><span class="fas fa-filter"></span><span translate="">Reset filter</span></button></li><li ng-if="_toolbar.reloadDataIsVisible"><button class="btn btn-xs btn-default" ng-click="forceReloadData()"><span class="fas fa-sync"></span><span translate="">Refresh</span></button></li><li ng-if="_toolbar.createNewIsVisible &amp;&amp; createNewItem"><button class="btn btn-xs btn-success" ng-click="createNewItem()"><span class="fas fa-plus"></span><span translate="">Add new</span></button></li><li ng-if="_toolbar.exportXlsBtnIsVisible"><div uib-dropdown=""><button class="btn btn-xs btn-success" uib-dropdown-toggle="" ng-disabled="!items.length"><span class="fas fa-file-excel"></span><span translate="">Export</span><span> </span><span class="caret"></span></button><ul class="dropdown-menu dropdown-menu-sm" uib-dropdown-menu=""><li ng-hide="!selectedItems.length"><a ng-click="exportToXls(\'selected\')"><span translate="">Selected rows</span></a></li><li ng-hide="isInfinity($parent.itemsPerPage)"><a ng-click="exportToXls(\'page\')"><span translate="">All on page</span></a></li><li><a ng-click="exportToXls(\'all\')"><span translate="">All</span></a></li></ul></div></li></ul><div class="ag-body-main {{:: themeName}}"></div><div class="ag-footer-aggregation {{:: themeName}}"></div><uic-pagination ng-model="pagination.current" total-pages="pagination.total"></uic-pagination>' + '')
    };
  }]);

}).call(this);
;
(function() {
  var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty,
    indexOf = [].indexOf || function(item) { for (var i = 0, l = this.length; i < l; i++) { if (i in this && this[i] === item) return i; } return -1; };

  (function() {
    var error, k, ref, tableViewAggridColumns, v;
    tableViewAggridColumns = {};
    ref = localStorage || {};
    for (k in ref) {
      v = ref[k];
      if (!(k && k.indexOf('$tableView-aggrid-') === 0)) {
        continue;
      }
      try {
        v = JSON.parse(v);
      } catch (error1) {
        error = error1;
        v = [];
      }
      tableViewAggridColumns[k.replace('$tableView-aggrid-', '')] = v;
      localStorage.removeItem(k);
    }
    if (!angular.isEmpty(tableViewAggridColumns)) {
      return localStorage.setItem('$lb$tableViewAggridColumns', JSON.stringify(tableViewAggridColumns));
    }
  })();

  angular.module('storage.gdpr').config(["$gdprStorageProvider", function($gdprStorageProvider) {
    $gdprStorageProvider.registerKey('tableViewAggridColumns', {
      purpose: angular.tr('Columns configuration for aggrid tables')
    });
    $gdprStorageProvider.registerKey('tableViewAggridItemsPerPage', {
      purpose: angular.tr('Items per page count for aggrid tables')
    });
    $gdprStorageProvider.registerKey('tableViewAggridColumnsFilterData', {
      purpose: angular.tr('Columns filter configuration for aggrid tables')
    });
  }]);

  angular.module('ui.cms.editable').factory('AggridHelpers', ["TableViewHelpers", "agridFiltersGenerator", "$state", "$interpolate", "$models", "$filter", "$gdprStorage", function(TableViewHelpers, agridFiltersGenerator, $state, $interpolate, $models, $filter, $gdprStorage) {
    var cellTooltipRenderer, footerAggregationFuncs, l10nFilter, toNumber;
    l10nFilter = $filter('l10n');
    toNumber = function(value, default_value) {
      var e;
      if (angular.isNumber(value)) {
        return value;
      }
      try {
        value = parseFloat(value);
        if (isNaN(value)) {
          return default_value;
        }
        return value;
      } catch (error1) {
        e = error1;
        return default_value;
      }
    };
    footerAggregationFuncs = {
      sum: function(values) {
        var j, len1, sum, v;
        sum = 0;
        for (j = 0, len1 = values.length; j < len1; j++) {
          v = values[j];
          if (v) {
            sum += toNumber(v, 0);
          }
        }
        return sum;
      },
      '+sum': function(values) {
        return footerAggregationFuncs.sum(values);
      },
      '-sum': function(values) {
        var j, len1, sum, v;
        sum = 0;
        for (j = 0, len1 = values.length; j < len1; j++) {
          v = values[j];
          if (v) {
            sum -= toNumber(v, 0);
          }
        }
        return sum;
      },
      count: function(values) {
        return values.length;
      }
    };
    TableViewHelpers.isAgridFiltered = function(agridModel) {
      var f, k, v;
      f = false;
      for (k in agridModel) {
        v = agridModel[k];
        if (typeof v === 'object') {
          if (Object.keys(v).length) {
            f = true;
            break;
          }
          continue;
        }
        if (v) {
          f = true;
          break;
        }
      }
      return f;
    };
    TableViewHelpers.saveColumnDefs = function(gridOptions, suffix) {
      var cols;
      cols = gridOptions.api.columnController.getAllGridColumns().map(function(agColumn) {
        var col, k, v;
        col = {
          colId: agColumn.colId
        };
        for (k in agColumn) {
          v = agColumn[k];
          if (typeof v === 'boolean') {
            col[k] = v;
          }
          if (k === 'pinned' && v) {
            col[k] = v;
          }
          if (k === 'actualWidth') {
            col.width = v;
          }
          if (k === 'sort') {
            col.sort = v;
          }
        }
        return col;
      });
      $gdprStorage.tableViewAggridColumns = $gdprStorage.tableViewAggridColumns || {};
      $gdprStorage.tableViewAggridColumns[$state.current.name + "-" + suffix] = cols;
    };
    TableViewHelpers.loadColumnDefs = function(columnDefs, suffix) {
      var c, defs, id, j, l, len1, len2, merge, newColumnDefs, s, saved;
      $gdprStorage.tableViewAggridColumns = $gdprStorage.tableViewAggridColumns || {};
      saved = $gdprStorage.tableViewAggridColumns[$state.current.name + "-" + suffix];
      if (!angular.isArray(saved)) {
        saved = [];
      } else {
        saved = angular.copy(saved);
      }
      merge = function(s) {
        var col, j, len1;
        for (j = 0, len1 = columnDefs.length; j < len1; j++) {
          col = columnDefs[j];
          if (s.colId === col.colId) {
            return angular.extend({}, col, s);
          }
          if (s.colId === col.field) {
            return angular.extend({}, col, s);
          }
        }
      };
      newColumnDefs = {
        saved: [],
        savedIds: [],
        other: []
      };
      for (j = 0, len1 = saved.length; j < len1; j++) {
        c = saved[j];
        s = merge(c);
        if (s) {
          newColumnDefs.saved.push(s);
          newColumnDefs.savedIds.push(c.colId);
        }
      }
      for (l = 0, len2 = columnDefs.length; l < len2; l++) {
        c = columnDefs[l];
        id = c.colId || c.field;
        if (newColumnDefs.savedIds.indexOf(id) === -1) {
          c.visible = true;
          newColumnDefs.other.push(c);
        }
      }
      defs = newColumnDefs.saved.concat(newColumnDefs.other);
      defs.saved = newColumnDefs.saved;
      return defs;
    };
    TableViewHelpers.saveItemsPerPage = function(itemsPerPage, suffix) {
      $gdprStorage.tableViewAggridItemsPerPage = $gdprStorage.tableViewAggridItemsPerPage || {};
      $gdprStorage.tableViewAggridItemsPerPage[$state.current.name + "-" + suffix] = itemsPerPage;
    };
    TableViewHelpers.loadItemsPerPage = function(suffix) {
      var e, itemsPerPage;
      $gdprStorage.tableViewAggridItemsPerPage = $gdprStorage.tableViewAggridItemsPerPage || {};
      itemsPerPage = $gdprStorage.tableViewAggridItemsPerPage[$state.current.name + "-" + suffix];
      try {
        return Math.abs(parseInt(itemsPerPage) || 0);
      } catch (error1) {
        e = error1;
        return 0;
      }
    };
    cellTooltipRenderer = function(params) {
      if (!angular.isDefined(params.valueFormatted) || params.valueFormatted === null) {
        return '';
      }
      if ((params.valueFormatted + '').length < 4) {
        return params.valueFormatted + '';
      }
      return "<span title='" + params.valueFormatted + "'>" + params.valueFormatted + "</span>";
    };
    TableViewHelpers.wrapColumnDefs = function(columnDefs, options, $scope, helpers) {
      var c, filterGeneneratorFn, filterParams, i, j, len1, ref, ref1, ref2;
      helpers = helpers || {};
      options = options || {};
      for (i = j = 0, len1 = columnDefs.length; j < len1; i = ++j) {
        c = columnDefs[i];
        columnDefs[i]._headerName = columnDefs[i].headerName;

        /*
            заменяем строковые названия фукнций для агрегации значений
            в футере таблицы
         */
        if (angular.isObject(c.footerAggFunc)) {
          c.footerAggFuncIgnoreChilds = !!c.footerAggFunc.ignoreChildsFromTreeData;
          c.footerAggFunc = c.footerAggFunc["function"];
        }
        if (angular.isString(c.footerAggFunc)) {
          if (!footerAggregationFuncs[c.footerAggFunc]) {
            throw "uice-table-view: функции '" + c.footerAggFunc + "' для агрегации в футере нет. Доступны: " + (Object.keys(footerAggregationFuncs)) + ". Также вы можете объявить функцию напрямую";
          }
          c.footerAggFunc = footerAggregationFuncs[c.footerAggFunc];
        }

        /*
            параметры генерации таблицы
         */
        if (angular.isFunction(c.footerAggFunc)) {
          options.allowFooterAggregation = true;
        } else {
          delete c.footerAggFunc;
        }
        if (c.checkboxSelection) {
          options.onItemClickAllowSingleClick = false;
        }
        if (options.allowTreeData && c.cellRenderer === 'group') {
          options.treeDataParams = options.treeDataParams || {};
          options.treeDataParams.hasCellRenderer = true;
        }

        /*
            добавляем фильтры для грида
         */
        if (!c.suppressFilter) {
          filterGeneneratorFn = agridFiltersGenerator[columnDefs[i].filter] || agridFiltersGenerator["default"];
          filterParams = columnDefs[i].filterParams || {};
          if (options.serverPagination) {
            filterParams.applyButton = true;
            if ((ref = filterParams.serverPagination) !== false && ref !== 'both') {
              filterParams.serverPagination = true;
            }
          }
          if ((ref1 = filterParams.filterStrategy) !== 'client' && ref1 !== 'server' && ref1 !== 'both') {
            if (filterParams.serverPagination) {
              if ((ref2 = options.filterStrategy) !== 'client' && ref2 !== 'both') {
                filterParams.filterStrategy = 'server';
              }
            } else {
              filterParams.filterStrategy = 'client';
            }
          }
          columnDefs[i].filter = filterGeneneratorFn(columnDefs[i].field, filterParams);
        }

        /*
            оборачиваем valueGetter
         */
        if (angular.isFunction(c.valueGetter)) {
          c._valueGetter = c.valueGetter;
          c.valueGetter = (function(c) {
            return function(params) {
              return c._valueGetter(params, $scope.$origScope, helpers);
            };
          })(c);
        } else if (angular.isArray(c.valueGetter)) {
          c._valueGetter = c.valueGetter;
          if (c.valueGetter.length <= 2) {
            c.valueGetter = (function(c) {
              return function(params) {
                return angular.getValue.call(null, params.data, c._valueGetter[0], c._valueGetter[1]);
              };
            })(c);
          } else {
            c.valueGetter = (function(c) {
              var vgetter_args;
              vgetter_args = c._valueGetter;
              vgetter_args.insert(0, null);
              return function(params) {
                vgetter_args[0] = params.data;
                return angular.getValueOr.apply(null, vgetter_args);
              };
            })(c);
          }
        }

        /*
            оборачиваем valueFormatter
         */
        if (c.valueFormatter) {
          c._valueFormatter = c.valueFormatter;
          c.valueFormatter = (function(c) {
            return function(params) {
              return c._valueFormatter(params, $scope.$origScope, helpers);
            };
          })(c);
          c.cellFormatter = c.valueFormatter;
        }
        if (c.cellTemplate) {
          c.compiledTemplate = $interpolate(c.cellTemplate);
          if (!c.cellTemplate.includes('<') && !c.cellTemplate.includes('/>')) {
            c.valueFormatter = (function(c) {
              return function(params) {
                var res;
                res = c.compiledTemplate({
                  item: params.data,
                  $item: params.data,
                  forceLang: $scope.forceLang,
                  $models: $models
                });
                if (options.autogeneratedTooltips) {
                  return "<span title='" + res + "'>" + res + "</span>";
                }
                return res;
              };
            })(c);
          } else {
            console.warn("uice-table-view: для aggrid в cellTemplate (для поля '" + c.field + "') можно указывать только верстку, которая не содержит директивы ангулара, т.к. шаблон обрабатывается $interpolate, а не $compile");
            c._field = c.field;
            c.field = '$$field_' + i;
            c.cellRenderer = (function(c) {
              return function(params) {
                params.node.data[c.field] = c.compiledTemplate({
                  item: params.data,
                  $item: params.data,
                  forceLang: $scope.forceLang,
                  $models: $models
                });
                return params.node.data[c.field];
              };
            })(c);
          }
          continue;
        }

        /*
            оборачиваем cellRenderer
         */
        if (c.cellRenderer && !angular.isString(c.cellRenderer)) {
          c._cellRenderer = c.cellRenderer;
          if (c.valueGetter) {
            c.cellRenderer = (function(c) {
              return function(node) {
                node.value = angular.getValue(node.data, c.field);
                return c._cellRenderer(node, $scope.$origScope, helpers);
              };
            })(c);
            continue;
          }
          c._field = c.field;
          c.field = '$$field_' + i;
          c.cellRenderer = (function(c) {
            return function(node) {
              node.value = angular.getValue(node.data, c._field);
              node.data[c.field] = c._cellRenderer(node, $scope.$origScope, helpers);
              return node.data[c.field];
            };
          })(c);
        }

        /*
            особое поведение для l10n-столбцов
         */
        if (c.l10n && !c.cellRenderer && !c.valueGetter) {
          c.valueGetter = function(params) {
            var value;
            value = l10nFilter(angular.getValue(params.data, params.colDef.field), $scope.forceLang);
            if (!angular.isDefined(value)) {
              return '';
            }
            return value;
          };
        }
        if (!c.cellRenderer && options.autogeneratedTooltips) {
          c.cellRenderer = cellTooltipRenderer;
        }
      }
      return {
        columnDefs: columnDefs,
        options: options
      };
    };
    return TableViewHelpers;
  }]).factory('AggridBaseTableFilter', function() {
    var AggridBaseTableFilter;
    AggridBaseTableFilter = (function() {
      function AggridBaseTableFilter(filterTemplate, filterParams) {
        this.filterTemplate = filterTemplate;
        this.filterName = 'base';
        this.filterParams = filterParams || {};
        this.hidePopup = angular.noop;
      }

      AggridBaseTableFilter.prototype.getGui = function() {
        return this.eGui;
      };

      AggridBaseTableFilter.prototype.resetFilterModel = function() {};

      AggridBaseTableFilter.prototype.afterGuiAttached = function(params) {
        var egui, input, j, len1, ref;
        this.hidePopup = params.hidePopup;
        egui = angular.element(this.eGui);
        ref = egui.find('input');
        for (j = 0, len1 = ref.length; j < len1; j++) {
          input = ref[j];
          if (input.hasAttribute('autofocus')) {
            input.focus();
            break;
          }
        }
      };

      AggridBaseTableFilter.prototype.init = function(params) {
        var self;
        self = this;
        params.$scope.self = self;
        this.filterChangedCallback = params.filterChangedCallback;
        this.valueGetter = params.valueGetter;
        if (this.filterParams.applyButton && !this.actionsTemplate) {
          this.actionsTemplate = "<div class='btn-group'>\n    <button class='btn btn-xs btn-primary'\n            ng-click=\"applyFilter()\"\n            ng-disabled=\"!isValid()\"\n            translate=''>\n        Apply\n    </button>\n    <button class='btn btn-xs btn-default'\n            ng-click=\"resetFilter()\"\n            translate=''>\n        Reset\n    </button>\n</div>";
        } else if (!this.actionsTemplate) {
          this.actionsTemplate = "<button class='btn btn-xs btn-default'\n        ng-click=\"resetFilter()\"\n        translate=''>\n    Reset\n</button>";
        }
        this.eGui = document.createElement('div');
        this.eGui.innerHTML = "<div class=\"agfilter-box agfilter-box-" + (this.filterName || 'base') + "\">\n    " + this.filterTemplate + "\n    <div class=\"text-right\">\n        " + this.actionsTemplate + "\n    </div>\n</div>";
        this.resetFilterModel();
        this.onEnterPress = function() {
          if (this.filterParams.applyButton) {
            params.$scope.applyFilter();
          }
        };
        params.$scope.isValid = function() {
          return self.isFilterActive();
        };
        params.$scope.applyFilter = function() {
          self.filterChangedCallback();
          if (self.filterParams.applyButton) {
            return self.hidePopup();
          }
        };
        return params.$scope.resetFilter = function() {
          self.resetFilterModel();
          return self.filterChangedCallback();
        };
      };

      return AggridBaseTableFilter;

    })();
    return AggridBaseTableFilter;
  }).factory('agridFiltersGenerator', ["$filter", "$countries", "$constants", "$models", "$langPicker", "AggridBaseTableFilter", function($filter, $countries, $constants, $models, $langPicker, AggridBaseTableFilter) {
    var agridFiltersGenerator, cutFilter, generateForNumber, generateForSet, l10nFilter, translateFilter;
    cutFilter = $filter('cut');
    l10nFilter = $filter('l10n');
    translateFilter = $filter('translate');
    agridFiltersGenerator = {};
    generateForNumber = function(fieldName, filterParams, filterName, step) {
      var NumberFilter;
      step = step || 0.01;
      return NumberFilter = (function(superClass) {
        extend(NumberFilter, superClass);

        function NumberFilter() {
          this.filterName = filterName;
          this.filterParams = filterParams || {};
          this.filterTemplate = "<div class='form-group'>\n    <label translate=''>Greater than or equals:</label>\n    <input class='form-control input-sm'\n            type='number' step='" + step + "'\n            placeholder=\"{{'Value' | translate}}\"\n            uic-enter-press=\"self.onEnterPress()\"\n            ng-model=\"self.searchTerm.gte\"\n            ng-model-options=\"{ debounce: 250 }\"\n        />\n</div>\n<div class='form-group'>\n    <label translate=''>Less than or equals:</label>\n    <input class='form-control input-sm'\n            type='number' step='" + step + "'\n            placeholder=\"{{'Value' | translate}}\"\n            uic-enter-press=\"self.onEnterPress()\"\n            min=\"{{self.searchTerm.gte}}\"\n            ng-model=\"self.searchTerm.lte\"\n            ng-model-options=\"{ debounce: 250 }\"\n        />\n</div>";
        }

        NumberFilter.prototype.resetFilterModel = function() {
          this.searchTerm = {
            gte: null,
            lte: null
          };
        };

        NumberFilter.prototype.init = function(params) {
          var self;
          NumberFilter.__super__.init.call(this, params);
          self = this;
          params.$scope.isValid = function() {
            return !(self.isValueEmpty(self.searchTerm.gte) && self.isValueEmpty(self.searchTerm.lte));
          };
          if (!this.filterParams.applyButton) {
            params.$scope.$watchGroup(['self.searchTerm.gte', 'self.searchTerm.lte'], params.$scope.applyFilter);
          }
        };

        NumberFilter.prototype.isValueEmpty = function(val) {
          return val === '' || val === null || !angular.isDefined(val);
        };

        NumberFilter.prototype.doesFilterPass = function(params) {
          var error, val;
          if (this.filterParams.filterStrategy === 'server') {
            return true;
          }
          if (this.isValueEmpty(this.searchTerm.gte) && this.isValueEmpty(this.searchTerm.lte)) {
            return true;
          }
          val = this.valueGetter(params);
          if (this.isValueEmpty(val)) {
            return false;
          }
          if (!angular.isNumber(val)) {
            try {
              val = parseFloat(val);
              if (isNaN(val)) {
                return false;
              }
            } catch (error1) {
              error = error1;
            }
          }
          if (!this.isValueEmpty(this.searchTerm.gte) && !this.isValueEmpty(this.searchTerm.lte)) {
            return (this.searchTerm.gte <= val && val <= this.searchTerm.lte);
          }
          if (!this.isValueEmpty(this.searchTerm.gte)) {
            return this.searchTerm.gte <= val;
          }
          if (!this.isValueEmpty(this.searchTerm.lte)) {
            return val <= this.searchTerm.lte;
          }
        };

        NumberFilter.prototype.isFilterActive = function() {
          return this.searchTerm && (!this.isValueEmpty(this.searchTerm.lte) || !this.isValueEmpty(this.searchTerm.gte));
        };

        NumberFilter.prototype.getModel = function() {
          var v;
          if (this.isValueEmpty(this.searchTerm.gte) && this.isValueEmpty(this.searchTerm.lte)) {
            return null;
          }
          v = {};
          if (this.searchTerm.gte !== null) {
            v.gte = this.searchTerm.gte;
          }
          if (this.searchTerm.lte !== null) {
            v.lte = this.searchTerm.lte;
          }
          return v;
        };

        NumberFilter.prototype.setModel = function(model) {
          if (!model) {
            return this.resetFilterModel();
          }
          this.searchTerm = {
            gte: model.gte,
            lte: model.lte
          };
        };

        return NumberFilter;

      })(AggridBaseTableFilter);
    };
    agridFiltersGenerator.number = function(fieldName, filterParams) {
      return generateForNumber(fieldName, filterParams, 'number', 0.01);
    };
    agridFiltersGenerator.float = function(fieldName, filterParams) {
      return generateForNumber(fieldName, filterParams, 'float', 0.01);
    };
    agridFiltersGenerator.int = function(fieldName, filterParams) {
      return generateForNumber(fieldName, filterParams, 'int', 1);
    };
    agridFiltersGenerator.string = agridFiltersGenerator["default"] = function(fieldName, filterParams) {
      var StringFilter;
      return StringFilter = (function(superClass) {
        extend(StringFilter, superClass);

        function StringFilter() {
          var base, filterType;
          this.filterName = 'string';
          this.filterParams = filterParams || {};
          (base = this.filterParams).filterType || (base.filterType = 'like,nlike');
          filterType = this.filterParams.filterType.split(',');
          if (indexOf.call(filterType, 'like') >= 0 && indexOf.call(filterType, 'nlike') >= 0) {
            this.filterTemplate = "<div class='form-group'>\n    <select ng-model=\"self.searchTerm.type\" class='form-control input-sm'>\n        <option value='like' translate=''>\n            Contains\n        </option>\n        <option value='nlike' translate=''>\n            Not contains\n        </option>\n        <!--\n        <option value='starts-with' translate=''>\n            Starts with\n        </option>\n        <option value='ends-with' translate=''>\n            Ends with\n        </option>\n        -->\n    </select>\n</div>\n<div class='form-group'>\n    <input class='form-control input-sm'\n        ng-model=\"self.searchTerm.value\"\n        ng-model-options=\"{ debounce: 250 }\"\n        placeholder=\"{{'Text' | translate}}...\"\n        uic-enter-press=\"self.onEnterPress()\"\n        autofocus\n        />\n</div>";
          } else if (indexOf.call(filterType, 'like') >= 0) {
            this.filterTemplate = "<div class='form-group'>\n    <input class='form-control input-sm'\n        ng-model=\"self.searchTerm.value\"\n        ng-model-options=\"{ debounce: 250 }\"\n        placeholder=\"{{'Contains' | translate}}...\"\n        uic-enter-press=\"self.onEnterPress()\"\n        autofocus\n        />\n</div>";
          } else if (indexOf.call(filterType, 'nlike') >= 0) {
            this.filterTemplate = "<div class='form-group'>\n    <input class='form-control input-sm'\n        ng-model=\"self.searchTerm.value\"\n        ng-model-options=\"{ debounce: 250 }\"\n        placeholder=\"{{'Not contains' | translate}}...\"\n        uic-enter-press=\"self.onEnterPress()\"\n        autofocus\n        />\n</div>";
          }
        }

        StringFilter.prototype.resetFilterModel = function($scope) {
          this.searchTerm = {
            type: 'like',
            value: ''
          };
          if (indexOf.call(this.filterParams.filterType.split(','), 'like') < 0) {
            this.searchTerm.type = 'nlike';
          }
        };

        StringFilter.prototype.init = function(params) {
          StringFilter.__super__.init.call(this, params);
          if (!this.filterParams.applyButton) {
            params.$scope.$watchGroup(['self.searchTerm.value', 'self.searchTerm.type'], params.$scope.applyFilter);
          }
        };

        StringFilter.prototype.doesFilterPass = function(params) {
          var val;
          if (this.filterParams.filterStrategy === 'server') {
            return true;
          }
          if (!this.searchTerm.value) {
            return true;
          }
          val = (this.valueGetter(params) || '') + '';
          if (this.searchTerm.type === 'like') {
            return val.indexOf(this.searchTerm.value) > -1 || val.toLowerCase().indexOf(this.searchTerm.value.toLowerCase()) > -1;
          }
          if (this.searchTerm.type === 'nlike') {
            return val.indexOf(this.searchTerm.value) === -1 || val.toLowerCase().indexOf(this.searchTerm.value.toLowerCase()) === -1;
          }

          /*
          if @searchTerm.type == 'starts-with'
              return val.indexOf(@searchTerm.value) == 0 or val.toLowerCase().indexOf(@searchTerm.value.toLowerCase()) == 0
          if @searchTerm.type == 'ends-with'
              val = val.substr(val.length - @searchTerm.value.length)
              if !val then return false
              return val == @searchTerm.value or val.toLowerCase() == @searchTerm.value.toLowerCase()
           */
        };

        StringFilter.prototype.isFilterActive = function() {
          return this.searchTerm && this.searchTerm.value;
        };

        StringFilter.prototype.getModel = function() {
          var v;
          if (!this.searchTerm.value) {
            return null;
          }
          v = {};
          v[this.searchTerm.type + "_i"] = this.searchTerm.value;
          return v;
        };

        StringFilter.prototype.setModel = function(model) {
          var k, v;
          this.resetFilterModel();
          if (!model) {
            return;
          }
          for (k in model) {
            v = model[k];
            if (!(['like_i', 'nlike_i'].includes(k))) {
              continue;
            }
            this.searchTerm.type = k.split('_')[0];
            this.searchTerm.value = v;
            break;
          }
        };

        return StringFilter;

      })(AggridBaseTableFilter);
    };
    agridFiltersGenerator.date = function(fieldName, filterParams) {
      var DateFilter;
      return DateFilter = (function(superClass) {
        extend(DateFilter, superClass);

        function DateFilter() {
          var ngModelType, ref;
          this.filterName = 'date';
          this.filterParams = filterParams || {};
          if ((ref = this.filterParams.valueType) !== 'datetime' && ref !== 'date') {
            this.filterParams.valueType = 'datetime';
          }
          ngModelType = 'string';
          if (this.filterParams.valueType === 'datetime') {
            ngModelType = 'date';
          }
          this.filterTemplate = "<div class=\"row\">\n    <div class=\"col-md-50\">\n        <label translate=''>From date:</label>\n        <div>\n            <uic-datepicker ng-model=\"self.dateFrom\" class=\"xs\" datepicker-options=\"dtOptionsFrom\" ng-model-type=\"" + ngModelType + "\">\n            </uic-datepicker>\n        </div>\n    </div>\n    <div class=\"col-md-50\">\n        <label translate=''>Till date:</label>\n        <div>\n            <uic-datepicker ng-model=\"self.dateTo\" min-date=\"self.dateFrom\" class=\"xs\"  datepicker-options=\"dtOptionsTo\" ng-model-type=\"" + ngModelType + "\">\n            </uic-datepicker>\n        </div>\n    </div>\n</div>";
        }

        DateFilter.prototype.datetimeToDate = function(d) {
          if (!d) {
            return null;
          }
          d = new Date(d);
          d.setUTCHours(12);
          d.setUTCMinutes(0);
          d.setUTCSeconds(0);
          d.setUTCMilliseconds(0);
          return d;
        };

        DateFilter.prototype.normaliazeDate = function(d) {
          if (this.filterParams.valueType === 'date') {
            return this.datetimeToDate(d);
          }
          if (!d) {
            return null;
          }
          return new Date(d);
        };

        DateFilter.prototype.dumpDate = function(d, type) {
          if (this.filterParams.valueType === 'date') {
            if (!angular.isString(d)) {
              return d.toDateISOString();
            }
            return d;
          } else {
            return d[type]('day').toISOString();
          }
        };

        DateFilter.prototype.resetFilterModel = function() {
          this.dateFrom = null;
          this.dateTo = null;
        };

        DateFilter.prototype.init = function(params) {
          DateFilter.__super__.init.call(this, params);
          params.$scope.dtOptionsFrom = {
            maxMode: 'day',
            formatYear: 'yyyy',
            startingDay: 1,
            showWeeks: false
          };
          params.$scope.dtOptionsTo = {
            maxMode: 'day',
            formatYear: 'yyyy',
            startingDay: 1,
            showWeeks: false
          };
          params.$scope.isValid = function() {
            return params.$scope.self.dateTo || params.$scope.self.dateFrom;
          };
          if (!this.filterParams.applyButton) {
            params.$scope.$watchGroup(['self.dateFrom', 'self.dateTo'], params.$scope.applyFilter);
          }
        };

        DateFilter.prototype.doesFilterPass = function(params) {
          var d, dateFrom, dateTo;
          if (this.filterParams.filterStrategy === 'server') {
            return true;
          }
          d = this.valueGetter(params);
          d = this.normaliazeDate(d);
          if (!d) {
            return false;
          }
          dateFrom = this.normaliazeDate(this.dateFrom);
          dateTo = this.normaliazeDate(this.dateTo);
          if (dateFrom && dateFrom.getTime() > d.getTime()) {
            return false;
          }
          if (dateTo && dateTo.getTime() < d.getTime()) {
            return false;
          }
          return true;
        };

        DateFilter.prototype.isFilterActive = function() {
          return this.dateTo || this.dateFrom;
        };

        DateFilter.prototype.getModel = function() {
          var v;
          if (!this.dateTo && !this.dateFrom) {
            return null;
          }
          v = {};
          if (this.dateFrom) {
            v.gte = this.dumpDate(this.dateFrom, 'startOf');
          }
          if (this.dateTo) {
            v.lte = this.dumpDate(this.dateTo, 'endOf');
          }
          return v;
        };

        DateFilter.prototype.setModel = function(model) {
          var error;
          this.resetFilterModel();
          if (!model) {
            return;
          }
          if (model.gte) {
            try {
              this.dateFrom = new Date(Date.parse(model.gte));
              if (this.filterParams.valueType === 'date') {
                this.dateFrom = this.datetimeToDate(this.dateFrom);
              }
            } catch (error1) {
              error = error1;
            }
          }
          if (model.lte) {
            try {
              this.dateTo = new Date(Date.parse(model.lte));
              if (this.filterParams.valueType === 'date') {
                this.dateTo = this.datetimeToDate(this.dateTo);
              }
            } catch (error1) {
              error = error1;
            }
          }
        };

        return DateFilter;

      })(AggridBaseTableFilter);
    };
    generateForSet = function(fieldName, filterParams) {
      var SetFilter;
      return SetFilter = (function(superClass) {
        extend(SetFilter, superClass);

        function SetFilter() {
          this.filterName = 'set';
          this.filterParams = filterParams || {};
          this.filterTemplate = "<div class=\"overflow-box\">\n    <div class=\"form-group nomargin\">\n        <div class=\"checkbox checkbox-sm\">\n            <label>\n                <input type=\"checkbox\" ng-model=\"self.selectAll\">\n                <span translate=''>Select all</span>\n            </label>\n        </div>\n        <hr ng-show=\"self.filterValues.length\" />\n        <div class='form-inline' ng-show=\"self.filterValues.length > 7\">\n            <div class='form-group'>\n                <uic-search-input ng-model=\"self.searchTerm\" placeholder=\"{{'Search items...' | translate}}\">\n                </uic-search-input>\n            </div>\n        </div>\n        <div class=\"checkbox checkbox-sm\" ng-repeat=\"data in self.filterValues | filter: self.searchTerm\">\n            <label title=\"{{data.data}}\">\n                <input type=\"checkbox\"\n                     value=\"{{data.value}}\"\n                     ng-checked=\"self.filterValuesSelected.indexOf(data.value) > -1\"\n                     ng-click=\"toggleSelection(data.value)\">\n                {{getText(data)}}\n            </label>\n        </div>\n    </div>\n</div>";
        }

        SetFilter.prototype.resetFilterModel = function() {
          this.selectAll = true;
          this.filterValuesSelected = [];
        };

        SetFilter.prototype.init = function(params) {
          var skip_watch_selectall;
          SetFilter.__super__.init.call(this, params);
          this.filterValues = [];
          this.selectAll = true;
          this.gridOptionsWrapper = params.column.gridOptionsWrapper;
          skip_watch_selectall = false;
          params.$scope.toggleSelection = function(value) {
            var i;
            if (!params.$scope.self.filterValuesSelected) {
              params.$scope.self.filterValuesSelected = [];
            }
            i = params.$scope.self.filterValuesSelected.indexOf(value);
            if (i > -1) {
              params.$scope.self.filterValuesSelected.splice(i, 1);
            } else {
              params.$scope.self.filterValuesSelected.push(value);
            }
            if (params.$scope.self.filterValuesSelected.length === params.$scope.self.filterValues.length) {
              if (!params.$scope.self.selectAll) {
                skip_watch_selectall = true;
              }
              params.$scope.self.selectAll = true;
            } else {
              if (params.$scope.self.selectAll) {
                skip_watch_selectall = true;
              }
              params.$scope.self.selectAll = false;
            }
          };
          params.$scope.getText = function(o) {
            if (o.description) {
              if (angular.isString(o.description)) {
                return translateFilter(o.description);
              }
              return l10nFilter(o.description);
            }
            if (angular.isDefined(o.value)) {
              return o.value;
            }
            return '';
          };
          params.$scope.$watch('self.selectAll', (function(_this) {
            return function(selectAll, oldValue) {
              if (skip_watch_selectall || selectAll === oldValue) {
                skip_watch_selectall = false;
                return;
              }
              if (selectAll) {
                params.$scope.self.filterValuesSelected = params.$scope.self.filterValues.map(function(obj) {
                  return obj.value;
                });
              } else {
                params.$scope.self.filterValuesSelected = [];
              }
              if (!_this.filterParams.applyButton) {
                params.$scope.applyFilter();
              }
            };
          })(this));
          if (!this.filterParams.applyButton) {
            params.$scope.$watchCollection('self.filterValuesSelected', params.$scope.applyFilter);
          }
        };

        SetFilter.prototype.cleanValues = function(items) {
          var skip;
          items = items || [];
          skip = this.filterParams.skipValues || null;
          if (angular.isFunction(skip)) {
            return items.filter(function(item) {
              return !skip(item);
            });
          }
          if (!skip || !skip.length) {
            return items;
          }
          return items.filter(function(o) {
            return !skip.includes(o.value);
          });
        };

        SetFilter.prototype.getGui = function() {
          var collectedValues, filterValues, self, transformValues;
          if (angular.isString(this.filterParams.values)) {
            if (this.filterParams.values.indexOf('$models') === 0) {
              transformValues = this.filterParams.transformValues || angular.identity;
              if (!angular.isFunction(transformValues)) {
                transformValues = angular.identity;
              }
              filterValues = angular.getValue($models, this.filterParams.values.replace('$models.', '')) || [];
              this.filterValues = this.cleanValues(filterValues.map(transformValues));
            } else {
              this.filterValues = this.cleanValues($constants.resolve(this.filterParams.values));
            }
            return SetFilter.__super__.getGui.call(this);
          }
          if (angular.isArray(this.filterParams.values)) {
            this.filterValues = this.cleanValues(angular.copy(this.filterParams.values));
            return SetFilter.__super__.getGui.call(this);
          }
          self = this;
          collectedValues = [];
          this.gridOptionsWrapper.gridOptions.api.forEachNode(function(node) {
            var j, len1, ref, value, values;
            value = self.valueGetter(node);
            if (!angular.isDefined(value)) {
              return;
            }
            if (self.filterParams.valueType === 'array') {
              values = value;
            } else {
              values = [value];
            }
            ref = values || [];
            for (j = 0, len1 = ref.length; j < len1; j++) {
              value = ref[j];
              if (self.filterParams.l10n) {
                value = l10nFilter(value);
              }
              if (collectedValues.includes(value)) {
                return;
              }
              collectedValues.push(value);
            }
          });
          collectedValues.sort();
          this.filterValues = this.cleanValues(collectedValues.map(function(value) {
            return {
              value: value,
              description: cutFilter(value + "", false, 40)
            };
          }));
          return SetFilter.__super__.getGui.call(this);
        };

        SetFilter.prototype.doesFilterPass = function(params) {
          var d;
          if (this.filterParams.filterStrategy === 'server') {
            return true;
          }
          if (this.selectAll || !this.filterValuesSelected.length) {
            return true;
          }
          d = this.valueGetter(params);
          if (!angular.isDefined(d)) {
            return false;
          }
          if (this.filterParams.l10n) {
            d = l10nFilter(d);
          }
          return this.filterValuesSelected.includes(d);
        };

        SetFilter.prototype.isFilterActive = function() {
          if (!this.filterValuesSelected) {
            this.filterValuesSelected = [];
          }
          if (this.filterValuesSelected.length) {
            return true;
          }
          if (!this.selectAll) {
            return true;
          }
          return false;
        };

        SetFilter.prototype.getModel = function() {
          if (this.selectAll) {
            return null;
          }
          return {
            inq: this.filterValuesSelected
          };
        };

        SetFilter.prototype.setModel = function(model) {
          this.resetFilterModel();
          if (!model) {
            return;
          }
          if (model.inq && model.inq.length) {
            this.filterValuesSelected = model.inq;
            this.selectAll = false;
          }
        };

        return SetFilter;

      })(AggridBaseTableFilter);
    };
    agridFiltersGenerator.set = function(fieldName, filterParams) {
      return generateForSet(fieldName, filterParams);
    };
    agridFiltersGenerator.countriesSet = function(fieldName, filterParams) {
      var CountriesSetFilter, SetFilter;
      SetFilter = generateForSet(fieldName, filterParams);
      return CountriesSetFilter = (function(superClass) {
        extend(CountriesSetFilter, superClass);

        function CountriesSetFilter() {
          CountriesSetFilter.__super__.constructor.call(this);
          this.filterTemplate = "<div class=\"overflow-box\">\n    <div class=\"form-group nomargin\">\n        <div class=\"checkbox checkbox-sm\">\n            <label>\n                <input type=\"checkbox\" ng-model=\"self.selectAll\">\n                <span translate=''>Select all</span>\n            </label>\n        </div>\n        <div class=\"checkbox checkbox-sm\">\n            <label>\n                <input type=\"checkbox\"\n                     ng-model='self.onlyOneCountry'\n                     ng-disabled=\"self.filterValuesSelected.length > 1\">\n                <span translate=''>Only one country</span>\n            </label>\n        </div>\n        <hr ng-show=\"self.filterValues.length\" />\n        <div class='form-inline' ng-show=\"self.filterValues.length > 7\">\n            <div class='form-group'>\n                <uic-search-input ng-model=\"self.searchTerm\" placeholder=\"{{'Search items...' | translate}}\">\n                </uic-search-input>\n            </div>\n        </div>\n        <div class=\"checkbox checkbox-sm\" ng-repeat=\"data in self.filterValues | filter: self.searchTerm\">\n            <label title=\"{{data.data}}\">\n                <input type=\"checkbox\"\n                     value=\"{{data.value}}\"\n                     ng-checked=\"self.filterValuesSelected.indexOf(data.value) > -1\"\n                     ng-click=\"toggleSelection(data.value)\">\n                {{getText(data)}}\n            </label>\n        </div>\n    </div>\n</div>";
        }

        CountriesSetFilter.prototype.init = function(params) {
          CountriesSetFilter.__super__.init.call(this, params);
          params.$scope.$watch('self.filterValuesSelected.length', function(len) {
            if (len > 1 || len === 0) {
              params.$scope.self.onlyOneCountry = false;
            }
          });
        };

        CountriesSetFilter.prototype.getGui = function() {
          var _values, j, len1, ref, v, values;
          if (angular.isString(this.filterParams.values)) {
            if (this.filterParams.values.indexOf('$models') === 0) {
              values = angular.getValue($models, this.filterParams.values.replace('$models.', '')) || [];
            } else if (this.filterParams.values.indexOf('$countries') === 0) {
              values = [];
              ref = this.filterParams.values.split('+');
              for (j = 0, len1 = ref.length; j < len1; j++) {
                v = ref[j];
                _values = angular.getValue($countries, v.trim().replace("$countries.", '')) || [];
                values = values.concat(_values.map(function(c) {
                  return c[0];
                }));
              }
            }
            this.filterValues = (values || []).map(function(code) {
              return {
                value: code,
                description: $countries.codeToText(code, $langPicker.currentLang)
              };
            });
            this.filterValues.sort(function(a, b) {
              if (a.description < b.description) {
                return -1;
              }
              if (a.description > b.description) {
                return 1;
              }
              return 0;
            });
            return this.eGui;
          }
          if (angular.isArray(this.filterParams.values)) {
            this.filterValues = this.cleanValues(angular.copy(this.filterParams.values));
          }
          return this.eGui;
        };

        CountriesSetFilter.prototype.afterGuiAttached = function(params) {
          var r;
          r = CountriesSetFilter.__super__.afterGuiAttached.call(this, params);
          this.getGui();
          return r;
        };

        CountriesSetFilter.prototype.doesFilterPass = function(params) {
          var d;
          if (this.filterParams.filterStrategy === 'server') {
            return true;
          }
          if (this.selectAll || !this.filterValuesSelected.length) {
            return true;
          }
          d = this.valueGetter(params);
          if (!angular.isDefined(d)) {
            return false;
          }
          if (this.filterParams.l10n) {
            d = l10nFilter(d);
          }
          if (this.onlyOneCountry) {
            return this.filterValuesSelected[0] === d;
          }
          return this.filterValuesSelected.includes(d);
        };

        CountriesSetFilter.prototype.getModel = function() {
          var model;
          if (this.selectAll) {
            return null;
          }
          model = {
            inq: this.filterValuesSelected
          };
          if (this.onlyOneCountry) {
            model.count = 1;
          }
          return model;
        };

        CountriesSetFilter.prototype.setModel = function(model) {
          this.resetFilterModel();
          if (!model) {
            return;
          }
          if (model.inq && model.inq.length) {
            this.filterValuesSelected = model.inq;
            this.selectAll = false;
            if (model.count) {
              this.onlyOneCountry = true;
            }
          }
        };

        return CountriesSetFilter;

      })(SetFilter);
    };
    return agridFiltersGenerator;
  }]);

}).call(this);
;
(function() {
  angular.module('ui.cms.editable').directive('tableViewWrapperGrid', ["$injector", function($injector) {
    var $cms, $compile, BOOTSTRAP_GRID_SIZE, DEFAULT_ITEMS_COUNT_FOR_SCREEN, H, TableViewHelpers, chunkArray;
    TableViewHelpers = null;
    $compile = null;
    $cms = null;
    H = null;
    DEFAULT_ITEMS_COUNT_FOR_SCREEN = {
      lg: 4,
      md: 3,
      sm: 2,
      xs: 1
    };
    BOOTSTRAP_GRID_SIZE = {
      1: '100',
      2: '50',
      3: '33-3',
      4: '25',
      5: '20'
    };
    chunkArray = function(arr, len) {
      var chunks, i, j, total;
      chunks = [];
      total = arr.length;
      i = 0;
      j = -1;
      while (i < total) {
        if (i % len === 0) {
          j += 1;
          chunks[j] = [];
        }
        chunks[j].push(arr[i]);
        i += 1;
      }
      return chunks;
    };
    return {
      restrict: 'A',
      transclude: true,
      scope: {
        items: '=?',
        forceLang: '=?',
        searchTerm: '=?',
        onItemClick: '&?',
        columns: '=?',
        options: '=?',
        queryFilter: '=?',
        loadItemsFn: '=?',
        countItemsFn: '=?'
      },
      link: function($scope, $element, $attrs) {
        var c, colsForScreen, columnDefs, column_class, divElement, item_html, linkHelper, ngClick, screenSize;
        if (!TableViewHelpers) {
          TableViewHelpers = $injector.get('TableViewHelpers');
          $compile = $injector.get('$compile');
          $cms = $injector.get('$cms');
          H = $injector.get('H');
        }
        $scope.$cms = $cms;
        $scope.$origScope = H.findOrigScope($scope.$parent);
        columnDefs = angular.copy($scope.columns || {});
        divElement = angular.element($element.children()[0]);
        linkHelper = new TableViewHelpers.TableViewLink($scope, $element, $attrs, $scope.options || {});
        linkHelper.init();
        column_class = '';
        colsForScreen = columnDefs.itemsOnScreen || {};
        for (screenSize in DEFAULT_ITEMS_COUNT_FOR_SCREEN) {
          c = DEFAULT_ITEMS_COUNT_FOR_SCREEN[screenSize];
          colsForScreen[screenSize] = colsForScreen[screenSize] || c;
          column_class += " col-" + screenSize + "-" + BOOTSTRAP_GRID_SIZE[colsForScreen[screenSize]];
        }
        columnDefs.cellDirective = columnDefs.cellDirective || 'no-name-directive';
        item_html = columnDefs.cellTemplate || ("<" + columnDefs.cellDirective + " item=\"$item\"><" + columnDefs.cellDirective + "/>");
        linkHelper.saveStateParams();
        ngClick = "ng-click='_onItemClick($item, $index)'";
        if (!$scope.onItemClick || columnDefs.suppressCellClick) {
          ngClick = "";
        }
        divElement.html("<div ng-repeat=\"cachedItems in cache[$cms.screenSize]\">\n    <div clas='clearfix'>\n        <div ng-repeat=\"$item in cachedItems\"\n            " + ngClick + "\n            class='" + column_class + "'>\n            " + item_html + "\n        </div>\n    </div>\n    <div class='clearfix'>\n    </div>\n</div>");
        $compile(divElement.contents())($scope);
        linkHelper.runWatchers();
        return $scope.$watchCollection('itemsOnPage', function(items, oldValue) {
          var k, v;
          $scope.cache = {};
          if (angular.isEmpty(items)) {
            return;
          }
          for (k in colsForScreen) {
            v = colsForScreen[k];
            $scope.cache[k] = chunkArray(items, v);
          }
        });
      },
      template: ('/client/cms_editable_app/_providers/tableViews/_directives/tableViewWrapperGrid/tableViewWrapperGrid.html', '<div class="row" ng-show="itemsOnPage.length &amp;&amp; items.length"></div><div ng-if="!$tableViewLoading &amp;&amp; (!itemsOnPage.length || !items.length)"><uice-no-items search-term="searchTerm" is-filtered="isFiltered" reset-filter="resetFilter()"></uice-no-items></div><uic-pagination ng-model="stateParams.page" total-pages="totalPages"></uic-pagination><div class="uice-table-view-loading preloader" ng-class="{visible: $tableViewLoading}"></div>' + '')
    };
  }]);

}).call(this);
;
(function() {
  angular.module('ui.cms.editable').directive('tableViewWrapperList', ["$injector", function($injector) {
    var $compile, H, TableViewHelpers;
    TableViewHelpers = null;
    $compile = null;
    H = null;
    return {

      /*
      {
          iconType: 'div' ['img', 'none', 'html', 'empty']
          iconClass: ''
          iconTemplate: ''
          iconShowIsPublished: true
          cellTemplate: ""
          secondaryActions: [
              {}
          ]
      }
       */
      restrict: 'A',
      scope: {
        items: '=?',
        forceLang: '=?',
        searchTerm: '=?',
        onItemClick: '=?',
        columns: '=?',
        options: '=?',
        queryFilter: '=?',
        loadItemsFn: '=?',
        countItemsFn: '=?',
        sortField: '@?'
      },
      link: function($scope, $element, $attrs) {
        var body_item, columnDefs, linkHelper, ngClick, tableElement;
        if (!TableViewHelpers) {
          TableViewHelpers = $injector.get('TableViewHelpers');
          $compile = $injector.get('$compile');
          H = $injector.get('H');
        }
        $scope.$origScope = H.findOrigScope($scope.$parent);
        columnDefs = angular.copy($scope.columns || {});
        tableElement = angular.element($element.children()[0]);
        linkHelper = new TableViewHelpers.TableViewLink($scope, $element, $attrs, $scope.options || {});
        linkHelper.init();
        columnDefs.iconType = columnDefs.iconType || 'img';
        columnDefs.iconTemplate = columnDefs.iconTemplate || '';
        columnDefs.iconClass = columnDefs.iconClass || '';
        columnDefs.cellTemplate = columnDefs.cellTemplate || "<h4>{{$item.title | l10n: forceLang | cut:false:40}}</h4>";
        if (columnDefs.cellTemplateShowIsPublishedCreated !== false) {
          columnDefs.cellTemplateShowIsPublishedCreated = true;
        }
        if (columnDefs.cellTemplateShowPublishedCreated) {
          columnDefs.cellTemplate += "<p ng-if=\"$item.isPublished && $item.publicationDate\">\n    <span class='text-success' translate='' style='padding-right:3px;'>Published:</span>\n    {{$item.publicationDate | date: 'shortDate'}}\n</p>\n<p ng-if=\"!$item.isPublished && $item.created\">\n    <span translate='' style='padding-right:3px;'>Created:</span>\n    {{$item.created | date: 'shortDate'}}\n</p>";
        }
        body_item = "";
        switch (columnDefs.iconType) {
          case 'img':
            body_item += "<td class='uic-list-icon' ng-switch=\"!!$item.mainImg\">\n    <uic-picture class='" + columnDefs.iconClass + "' file=\"$item.mainImg\" size='70' ng-switch-when='true'>\n    </uic-picture>\n    <div class='uic-list-icon-placeholder " + columnDefs.iconClass + "' ng-switch-when='false'>\n        " + columnDefs.iconTemplate + "\n    </div>\n</td>";
            break;
          case 'html':
            body_item += "<td class='uic-list-icon'>\n    " + columnDefs.iconTemplate + "\n</td>";
            break;
          case 'placeholder':
            body_item += "<td class='uic-list-icon'>\n    <div class='uic-list-icon-placeholder " + columnDefs.iconClass + "'>\n        " + columnDefs.iconTemplate + "\n    </div>\n</td>";
        }
        body_item += "<td class='uic-list-primary'>\n    " + columnDefs.cellTemplate + "\n</td>";
        linkHelper.saveStateParams();
        ngClick = "ng-click='_onItemClick($item, $index)'";
        if (!$scope.onItemClick || columnDefs.suppressCellClick) {
          ngClick = "";
        }
        tableElement.html("<tbody>\n    <tr ng-repeat=\"$item in itemsOnPage\" " + ngClick + ">\n        " + body_item + "\n    </tr>\n</tbody>");
        $compile(tableElement.contents())($scope);
        return linkHelper.runWatchers();
      },
      template: ('/client/cms_editable_app/_providers/tableViews/_directives/tableViewWrapperList/tableViewWrapperList.html', '<table ng-show="itemsOnPage.length &amp;&amp; items.length"></table><div ng-if="!$tableViewLoading &amp;&amp; (!itemsOnPage.length || !items.length)"><uice-no-items search-term="searchTerm" is-filtered="isFiltered" reset-filter="resetFilter()"></uice-no-items></div><uic-pagination ng-model="stateParams.page" total-pages="totalPages"></uic-pagination><div class="uice-table-view-loading preloader" ng-class="{visible: $tableViewLoading}"></div>' + '')
    };
  }]);

}).call(this);
;
(function() {
  angular.module('ui.cms.editable').filter('getCategoryTitleForCmsObject', ["$models", "$injector", function($models, $injector) {
    var l10n;
    l10n = null;
    return function(item, modelName, forceLang) {
      var $filter, category;
      if (!item.category || !modelName) {
        return '';
      }
      if (!l10n) {
        $filter = $injector.get('$filter');
        l10n = $filter('l10n');
      }
      if ((item.$category || {}).title) {
        return l10n(item.$category.title, forceLang) || '';
      }
      category = $models[modelName].findItemByField('id', item.category);
      if (category) {
        return l10n(category.title, forceLang) || '';
      }
      return '';
    };
  }]).directive('tableViewWrapperTable', ["$injector", function($injector) {
    var $compile, H, NAMED_COLUMNS, TableViewHelpers, generateCategoryTitleColumnDef;
    TableViewHelpers = null;
    $compile = null;
    H = null;
    generateCategoryTitleColumnDef = function(modelName) {
      return {
        headerName: 'Category',
        field: 'categoryId',
        cellTemplate: "{{$item | getCategoryTitleForCmsObject:'" + modelName + "':forceLang}}"
      };
    };
    NAMED_COLUMNS = {
      id: {
        headerName: 'Id',
        field: 'id',
        cellTemplate: "{{$item.id}}"
      },
      '$select+id': {
        headerName: 'Id',
        field: 'id',
        checkboxSelection: true,
        cellTemplate: "{{$item.id}}"
      },
      name: {
        headerName: "Name",
        field: 'name',
        cellTemplate: "{{$item.name}}"
      },
      title: {
        headerName: "Title",
        field: 'title',
        cellTemplate: "{{$item.title | l10n: forceLang}}",
        l10n: true
      },
      state: {
        headerName: 'State',
        field: 'isPublished',
        cellClass: 'text-center',
        cellTemplate: "<uic-state state='$item.isPublished'></uic-state>"
      },
      created: {
        headerName: 'Created',
        field: 'created',
        cellTemplate: "{{$item.created | date: 'short'}}"
      },
      publicationDate: {
        headerName: 'Published',
        field: 'publicationDate',
        cellTemplate: "{{$item.publicationDate | date: 'short'}}"
      },
      categoryTitleForCmsProduct: generateCategoryTitleColumnDef('CmsCategoryForProduct'),
      price: {
        headerName: 'Price',
        field: 'price',
        cellTemplate: "{{$item.price | humanCurrency}}"
      },
      totalPrice: {
        headerName: 'Price',
        field: 'totalPrice',
        cellTemplate: "{{$item.totalPrice | humanCurrency}}"
      },
      owner: {
        headerName: 'Owner',
        field: 'owner',
        cellTemplate: "{{getOwnerName($item)}}"
      }
    };
    return {
      restrict: 'A',
      scope: {
        items: '=?',
        forceLang: '=?',
        searchTerm: '=?',
        onItemClick: '=?',
        columns: '=?',
        toolbar: '=?',
        options: '=?',
        queryFilter: '=?',
        loadItemsFn: '=?',
        countItemsFn: '=?'
      },
      link: function($scope, $element, $attrs) {
        var body_item, c, className, columnDefs, has_sort_field, head, i, j, l10nFields, len, linkHelper, ngClick, tableElement, toolbar;
        if (!TableViewHelpers) {
          TableViewHelpers = $injector.get('TableViewHelpers');
          $compile = $injector.get('$compile');
          H = $injector.get('H');
        }
        $scope.$origScope = H.findOrigScope($scope.$parent);
        l10nFields = [];
        columnDefs = angular.copy($scope.columns);
        tableElement = angular.element(angular.element($element.children()[0]).children()[1]);
        linkHelper = new TableViewHelpers.TableViewLink($scope, $element, $attrs, $scope.options || {});
        linkHelper.init();
        toolbar = $scope.toolbar || [];
        $scope._toolbar = {
          isVisible: !angular.isEmpty(toolbar),
          selectAllBtnIsVisible: toolbar.includes('selectAll') || !angular.isEmpty(toolbar),
          createNewIsVisible: toolbar.includes('createNew')
        };
        head = "";
        body_item = "";
        has_sort_field = false;
        for (i = j = 0, len = columnDefs.length; j < len; i = ++j) {
          c = columnDefs[i];
          if (angular.isString(c) && NAMED_COLUMNS[c]) {
            columnDefs[i] = angular.copy(NAMED_COLUMNS[c]);
            c = columnDefs[i];
          }
          if (typeof c !== 'object') {
            continue;
          }
          if (!TableViewHelpers.isColumnEnabled(c, $scope.$origScope)) {
            continue;
          }
          if (!c.field) {
            с.field = "$autoname_" + i;
            c.suppressSorting = true;
            if (!c.cellTemplate) {
              throw "tableViewWrapperTable: provide 'cellTemplate' attribute for " + i + " column!";
            }
          }
          if (c.l10n) {
            l10nFields.push(c.field);
          }
          className = c.field.replaceAll('.', '-').toLowerCase();
          if (c.field === $scope.stateParams.sort.field) {
            has_sort_field = true;
          }
          if (c.suppressSorting) {
            head += "<th class=\"th-" + className + " cursor-default\">\n    <span translate=''>" + c.headerName + "</span>\n</th>";
          } else {
            head += "<th class=\"th-" + className + "\" ng-click=\"stateParams.sort.field='" + c.field + "'; stateParams.sort.reverse=!stateParams.sort.reverse;\">\n    <span translate=''>" + c.headerName + "</span>\n    <uic-caret direction=\"stateParams.sort.reverse\" ng-if=\"stateParams.sort.field=='" + c.field + "'\"></uic-caret>\n</th>";
          }
          if (!c.cellTemplate && c.l10n) {
            c.cellTemplate = "{{$item." + c.field + " | l10n}}";
          } else if (!c.cellTemplate) {
            c.cellTemplate = "{{$item." + c.field + "}}";
          }
          if (c.checkboxSelection && !linkHelper.options.hasMultiSelection) {
            linkHelper.options.hasMultiSelection = true;
          } else {
            c.checkboxSelection = false;
          }
          if (c.suppressCellClick) {
            ngClick = '';
          } else if (linkHelper.options.hasMultiSelection) {
            ngClick = 'ng-dblclick="_onItemClick($item, $index)" ng-click="_onItemSelect($item, $index, $event)"';
          } else {
            ngClick = 'ng-click="_onItemClick($item, $index)"';
          }
          if (c.checkboxSelection) {
            c.cellTemplate = "<ul class='list-table nopadding nomargin w-auto'>\n    <li>\n        <span class='far uice-table-btn-checkbox'\n              ng-click=\"_onItemSelect($item, $index, $event, 'add'); $event.stopPropagation()\"\n              ng-class=\"{'fa-square': !isItemSelected($item), 'fa-check-square text-primary': isItemSelected($item)}\"\n              >\n        </span>\n    </li>\n    <li>\n        " + c.cellTemplate + "\n    </li>\n</ul>";
          }
          body_item += "<td class=\"td-" + className + " " + (c.cellClass || '') + "\" " + ngClick + ">\n    " + c.cellTemplate + "\n</td>";
        }
        if (!has_sort_field) {
          $scope.stateParams.sort.field = columnDefs[0].field;
        }
        linkHelper.setL10nFields(l10nFields);
        linkHelper.saveStateParams();
        tableElement.html("<thead>\n    <tr>\n        " + head + "\n    </tr>\n</thead>\n<tbody>\n    <tr ng-repeat=\"$item in itemsOnPage\" ng-class=\"{active: isItemSelected($item)}\">\n        " + body_item + "\n    </tr>\n</tbody>");
        $compile(tableElement.contents())($scope);
        return linkHelper.runWatchers();
      },
      template: ('/client/cms_editable_app/_providers/tableViews/_directives/tableViewWrapperTable/tableViewWrapperTable.html', '<div ng-show="itemsOnPage.length &amp;&amp; items.length"><ul class="list-inline table-toolbar" ng-show="_toolbar.isVisible"><li ng-if="_toolbar.selectAllBtnIsVisible"><div class="btn-group"><button class="btn btn-xs btn-info" ng-click="selectAll()"><div class="far fa-check-square"></div><span translate="">Select all</span></button><button class="btn btn-xs btn-default" ng-click="deselectAll()" title="{{\'Select none\' | translate}}"><div class="far fa-square nopadding"></div></button></div></li><li ng-if="_toolbar.createNewIsVisible &amp;&amp; createNewItem"><button class="btn btn-xs btn-success" ng-click="createNewItem()"><span class="fas fa-plus"></span><span translate="">Add new</span></button></li></ul><table class="table table-bordered table-hover"></table></div><div ng-if="!$tableViewLoading &amp;&amp; (!itemsOnPage.length || !items.length)"><uice-no-items search-term="searchTerm" is-filtered="isFiltered" reset-filter="resetFilter()"></uice-no-items></div><uic-pagination ng-model="stateParams.page" total-pages="totalPages"></uic-pagination><div class="uice-table-view-loading preloader" ng-class="{visible: $tableViewLoading}"></div>' + '')
    };
  }]);

}).call(this);
;
(function() {
  angular.module('ui.cms.editable').config(["$tableViewProvider", function($tableViewProvider) {
    $tableViewProvider.registerRender('table');
    $tableViewProvider.registerRender('grid');
    $tableViewProvider.registerRender('list');
    $tableViewProvider.registerRender('aggrid');
  }]);

}).call(this);
;
(function() {
  angular.module('ui.cms.editable').service('TableViewHelpers', ["$state", "$injector", "$models", function($state, $injector, $models) {
    var $cms, $compile, $currentUser, $q, $tableView, $timeout, H, TableViewLink, filterFilter, filterLimitTo, filterOrderBy, self;
    $tableView = null;
    $compile = null;
    $timeout = null;
    $cms = null;
    $q = null;
    $currentUser = null;
    H = null;
    filterFilter = null;
    filterOrderBy = null;
    filterLimitTo = null;
    this.saveStateParams = function(stateParams, nameAliases) {
      var params;
      if (nameAliases === false) {
        return {
          sort: stateParams.sort || {},
          filter: stateParams.filter || {},
          page: stateParams.page || 1
        };
      }
      nameAliases = nameAliases || {};
      nameAliases.filter = nameAliases.filter || 'filter';
      nameAliases.sort = nameAliases.sort || 'sort';
      nameAliases.page = nameAliases.page || 'page';
      nameAliases.selectedItems = nameAliases.selectedItems || 'selectedItems';
      params = $state.params;
      if (angular.isEmpty(stateParams.filter)) {
        params[nameAliases.filter] = void 0;
      } else {
        params[nameAliases.filter] = JSON.stringify(stateParams.filter);
      }
      if (angular.isEmpty(stateParams.sort)) {
        params[nameAliases.sort] = void 0;
      } else {
        params[nameAliases.sort] = JSON.stringify(stateParams.sort);
      }
      if (stateParams.selectedItems && stateParams.selectedItems.length) {
        params[nameAliases.selectedItems] = stateParams.selectedItems.join(',');
      } else {
        params[nameAliases.selectedItems] = void 0;
      }
      if (!stateParams.page || stateParams.page === 1) {
        params[nameAliases.page] = void 0;
      } else {
        params[nameAliases.page] = stateParams.page;
      }
      $state.go('.', params, {
        notify: false
      });
      return {
        sort: stateParams.sort || {},
        filter: stateParams.filter || {},
        page: stateParams.page || 1
      };
    };
    this.getStateParams = function(nameAliases) {
      var error, filter, page, selectedItems, sort;
      if (nameAliases === false) {
        return {
          sort: {},
          filter: {},
          page: 1
        };
      }
      nameAliases = nameAliases || {};
      nameAliases.filter = nameAliases.filter || 'filter';
      nameAliases.sort = nameAliases.sort || 'sort';
      nameAliases.page = nameAliases.page || 'page';
      nameAliases.selectedItems = nameAliases.selectedItems || 'selectedItems';
      filter = $state.params[nameAliases.filter] || "";
      sort = $state.params[nameAliases.sort] || "";
      page = $state.params[nameAliases.page] || "";
      selectedItems = $state.params[nameAliases.selectedItems] || "";
      try {
        filter = JSON.parse(filter) || {};
      } catch (error1) {
        error = error1;
        filter = {};
      }
      try {
        sort = JSON.parse(sort) || {};
      } catch (error1) {
        error = error1;
        sort = {};
      }
      try {
        page = parseInt(page);
      } catch (error1) {
        error = error1;
        page = 1;
      }
      if (isNaN(page)) {
        page = 1;
      }
      return {
        sort: sort,
        filter: filter,
        page: page || 1,
        selectedItems: selectedItems.split(',')
      };
    };
    this.findColumn = function(columnDefs, id) {
      var c, j, len;
      for (j = 0, len = columnDefs.length; j < len; j++) {
        c = columnDefs[j];
        if (c.colId === id || c.field === id) {
          return c;
        }
      }
      return null;
    };
    this.isColumnEnabled = function(columnDef, origScope, columnHelpers) {
      var isEnabled;
      isEnabled = true;
      if (angular.isFunction(columnDef.isEnabled)) {
        isEnabled = columnDef.isEnabled(origScope, columnHelpers || {});
      } else {
        if (!columnDef.hasOwnProperty('isEnabled')) {
          isEnabled = true;
        } else {
          isEnabled = !!columnDef.isEnabled;
        }
      }
      return isEnabled;
    };
    this.generateRestFilter = function(columnDefs, options, queryFilter, whereFilter, sortFilter) {
      var c, fixRestField, k, kk, ref, ref1, res, sortField, v, vv;
      queryFilter = angular.copy(queryFilter || {});
      queryFilter.where = queryFilter.where || {};
      sortFilter = sortFilter || "";
      fixRestField = function(field) {
        return field.replaceAll('$', '').replaceAll('.', '__');
      };
      ref = whereFilter || {};
      for (k in ref) {
        v = ref[k];
        c = this.findColumn(columnDefs, k);
        if (c) {
          if (c.filterParams && c.filterParams.serverPagination === false) {
            continue;
          }
          k = c.restField || c.field;
        }
        if (angular.isFunction(k)) {
          res = k(v, whereFilter || {});
          if (res.hasOwnProperty('key') && res.hasOwnProperty('value') && Object.keys(res).length === 2) {
            queryFilter.where[res.key] = res.value;
          } else {
            for (kk in res) {
              vv = res[kk];
              if (queryFilter.where[kk]) {
                console.warn("TableViewHelpers.generateRestFilter: Вы пытаетесь перезаписать rest-поле для запроса через where! Проверьте описание по полям в $tableViewProvider для таблицы. Проблемное поле: '" + kk + "'. Текущее значение where: " + (JSON.stringify(queryFilter.where)));
              }
              queryFilter.where[kk] = vv;
            }
          }
          continue;
        }
        k = fixRestField(k);
        queryFilter.where[k] = queryFilter.where[k] || {};
        if (!angular.isObject(v)) {
          queryFilter.where[k] = v;
        } else {
          ref1 = v || {};
          for (kk in ref1) {
            vv = ref1[kk];
            kk = kk.split('_');
            queryFilter.where[k][kk[0]] = vv;
            if (kk.length === 2) {
              queryFilter.where[k].options = kk[1];
            }
          }
        }
      }
      if (angular.isArray(sortFilter) && sortFilter[0]) {
        c = this.findColumn(columnDefs, sortFilter[0].colId);
        if (c) {
          sortField = c.restField || c.field;
        } else {
          sortField = sortFilter[0].colId;
        }
        queryFilter.order = fixRestField(sortField) + ' ' + sortFilter[0].sort.toUpperCase();
      } else if (angular.isObject(sortFilter) && sortFilter.field) {
        if (sortFilter.reverse) {
          queryFilter.order = fixRestField(sortFilter.field) + ' DESC';
        } else {
          queryFilter.order = fixRestField(sortFilter.field) + ' ASC';
        }
      }
      return queryFilter;
    };
    self = this;
    TableViewLink = (function() {
      function TableViewLink($scope, $element, $attrs, options) {
        var $filter;
        this.$scope = $scope;
        this.$element = $element;
        this.$attrs = $attrs;
        if (!$tableView) {
          $tableView = $injector.get('$tableView');
          $filter = $injector.get('$filter');
          $compile = $injector.get('$compile');
          $timeout = $injector.get('$timeout');
          $cms = $injector.get('$cms');
          $q = $injector.get('$q');
          $currentUser = $injector.get('$currentUser');
          H = $injector.get('H');
          filterFilter = $filter('filter');
          filterOrderBy = $filter('orderBy');
          filterLimitTo = $filter('limitTo');
        }
        this.options = options || {};
        this.options.l10nFields = this.options.l10nFields || [];
        this.$scope.$currentUser = $currentUser;
        this.$scope.selectedItems = [];
        this.$scope._selectedItemsIds = {};
        this.$scope.filteredItems = [];
        this.$scope.itemsOnPage = [];
        this.itemsPerPage = parseInt(this.$attrs.itemsPerPage);
        this.lastRestFilter = null;
        this.lastQuickFilter = null;
        this.$scope.resetFilter = (function(_this) {
          return function() {
            _this.$scope.searchTerm = '';
          };
        })(this);
        this.$scope.getMaxSize = function() {
          if ($cms.screenSize === 'xs') {
            return 3;
          }
          return 10;
        };
        this.$scope.isItemSelected = (function(_this) {
          return function(item) {
            item || (item = {});
            return !!_this.$scope._selectedItemsIds[item.id];
          };
        })(this);
        this.$scope._onItemClick = (function(_this) {
          return function(item, index) {
            var _item, base, base1, ids, j, len, ref;
            if (_this.$scope.ngDisabled) {
              return;
            }
            if (_this.options.hasMultiSelection) {
              (base = _this.$scope).selectedItems || (base.selectedItems = []);
              (base1 = _this.$scope)._selectedItemsIds || (base1._selectedItemsIds = {});
            } else {
              _this.$scope.selectedItems = [];
              _this.$scope._selectedItemsIds = {};
            }
            if (!_this.$scope._selectedItemsIds[item.id]) {
              _this.$scope.selectedItems.push(item);
              _this.$scope._selectedItemsIds[item.id] = true;
            }
            ids = [];
            ref = _this.$scope.filteredItems || [];
            for (j = 0, len = ref.length; j < len; j++) {
              _item = ref[j];
              if (_item.id) {
                ids.push(_item.id);
              }
            }
            if (_this.$scope.onItemClick) {
              _this.$scope.onItemClick({
                item: item,
                ids: ids
              });
            }
          };
        })(this);
        this.$scope._onItemSelect = (function(_this) {
          return function(item, index, event, strategy) {
            var base, base1, i, j, len, o, ref;
            if (_this.$scope.ngDisabled) {
              return;
            }
            if (_this.options.hasMultiSelection && (strategy === 'add' || event.ctrlKey)) {
              (base = _this.$scope).selectedItems || (base.selectedItems = []);
              (base1 = _this.$scope)._selectedItemsIds || (base1._selectedItemsIds = {});
            } else {
              _this.$scope.selectedItems = [];
              _this.$scope._selectedItemsIds = {};
            }
            if (!_this.$scope._selectedItemsIds[item.id]) {
              _this.$scope.selectedItems.push(item);
              _this.$scope._selectedItemsIds[item.id] = true;
            } else {
              ref = _this.$scope.selectedItems;
              for (i = j = 0, len = ref.length; j < len; i = ++j) {
                o = ref[i];
                if (!(o.id === item.id)) {
                  continue;
                }
                _this.$scope.selectedItems.remove(i);
                break;
              }
              _this.$scope._selectedItemsIds[item.id] = false;
            }
          };
        })(this);
        this.$scope.deselectAll = (function(_this) {
          return function() {
            _this.$scope.selectedItems = [];
            _this.$scope._selectedItemsIds = {};
          };
        })(this);
        this.$scope.selectAll = (function(_this) {
          return function() {
            var item, j, len, ref;
            _this.$scope.deselectAll();
            ref = _this.$scope.itemsOnPage || [];
            for (j = 0, len = ref.length; j < len; j++) {
              item = ref[j];
              _this.$scope.selectedItems.push(item);
              _this.$scope._selectedItemsIds[item.id] = true;
            }
          };
        })(this);
        this.$scope.getOwnerName = function(item) {
          var ownerId;
          ownerId = item.owner || item.ownerId;
          if (!ownerId) {
            return '';
          }
          if (!item.$owner) {
            item.$owner = $models.CmsUser.findItemByField('id', ownerId);
          }
          if (item.$owner) {
            return item.$owner.name || item.$owner.username || '';
          }
          return '';
        };
      }

      TableViewLink.prototype.setL10nFields = function(l10nFields) {
        this.options.l10nFields = l10nFields || [];
      };

      TableViewLink.prototype.calculatePagesForItems = function(items) {
        var l, pages;
        pages = items.length / this.itemsPerPage;
        l = pages % 1;
        if (l > 0) {
          pages = pages - l + 1;
        }
        return pages;
      };

      TableViewLink.prototype.saveStateParams = function() {
        self.saveStateParams(this.$scope.stateParams);
      };

      TableViewLink.prototype.loadingOverlay = function(isVisible) {
        this.$scope.$tableViewLoading = !!isVisible;
        if (!!isVisible) {
          this.$element.addClass('with-preloader');
        } else {
          $timeout((function(_this) {
            return function() {
              return _this.$element.removeClass('with-preloader');
            };
          })(this), 0);
        }
      };

      TableViewLink.prototype.normalizeSelectedItems = function(itemsOnPage) {
        var item, j, len, selectedItems;
        itemsOnPage = itemsOnPage || [];
        selectedItems = this.$scope.selectedItems.filter(function(item) {
          return !!itemsOnPage.findItemByProperty('id', item.id);
        });
        this.$scope.selectedItems = selectedItems;
        this.$scope._selectedItemsIds = {};
        for (j = 0, len = selectedItems.length; j < len; j++) {
          item = selectedItems[j];
          this.$scope._selectedItemsIds[item.id] = true;
        }
      };

      TableViewLink.prototype.loadItemsFromServer = function(forceLoadFromServer) {
        var defer, onFail, queryFilter, quickFilter;
        defer = $q.defer();
        onFail = function() {
          $cms.showNotification("Can't download items", {}, 'error');
          defer.resolve([]);
        };
        queryFilter = self.generateRestFilter([], this.options, this.$scope.queryFilter, [], this.$scope.stateParams.sort || {});
        quickFilter = this.$scope.searchTerm || '';
        if (this.lastRestFilter === JSON.stringify(queryFilter) && this.lastQuickFilter === quickFilter && !forceLoadFromServer) {
          defer.resolve(this.$scope.items);
          return defer.promise;
        }
        this.lastRestFilter = JSON.stringify(queryFilter);
        this.lastQuickFilter = quickFilter + '';
        this.loadingOverlay(true);
        this.$scope.countItemsFn(queryFilter.where, quickFilter).then((function(_this) {
          return function(data) {
            var count;
            if (data && data.hasOwnProperty('count')) {
              count = data.count;
            } else {
              count = data;
            }
            if (!count) {
              _this.$scope.totalPages = 1;
              _this.$scope.stateParams.page = 1;
              defer.resolve([]);
              return;
            }
            _this.$scope.totalPages = (count - count % _this.itemsPerPage) / _this.itemsPerPage;
            if (count % _this.itemsPerPage > 0) {
              _this.$scope.totalPages += 1;
            }
            if (_this.$scope.stateParams.page > _this.$scope.totalPages) {
              _this.$scope.stateParams.page = _this.$scope.totalPages;
              defer.resolve(_this.$scope.items);
              return;
            }
            queryFilter.skip = (_this.$scope.stateParams.page - 1) * _this.itemsPerPage;
            queryFilter.limit = _this.itemsPerPage;
            queryFilter.quickFilter = quickFilter;
            return _this.$scope.loadItemsFn(queryFilter).then(function(data) {
              var ids, item, j, len, ref;
              if (_this.$scope.stateParams.selectedItems && _this.$scope.stateParams.selectedItems.length) {
                ids = _this.$scope.stateParams.selectedItems.map(function(o) {
                  return o + '';
                });
                _this.$scope.selectedItems = data.filter(function(item) {
                  return ids.includes((item || {}).id + '');
                });
                _this.$scope._selectedItemsIds = {};
                ref = _this.$scope.selectedItems;
                for (j = 0, len = ref.length; j < len; j++) {
                  item = ref[j];
                  _this.$scope._selectedItemsIds[item.id] = true;
                }
                _this.$scope.stateParams.selectedItems = [];
              }
              defer.resolve(data);
            }, onFail);
          };
        })(this), onFail);
        return defer.promise;
      };

      TableViewLink.prototype.serverFilterItems = function(forceLoadFromServer) {
        this.loadingOverlay(true);
        this.loadItemsFromServer(forceLoadFromServer).then((function(_this) {
          return function(items) {
            _this.$scope.isFiltered = !!_this.$scope.searchTerm;
            _this.$scope.itemsOnPage = _this.$scope.filteredItems = _this.$scope.items = items;
            _this.loadingOverlay(false);
          };
        })(this), (function(_this) {
          return function() {
            _this.$scope.isFiltered = !!_this.$scope.searchTerm;
            _this.$scope.itemsOnPage = _this.$scope.filteredItems = _this.$scope.items = [];
            _this.loadingOverlay(false);
          };
        })(this));
      };

      TableViewLink.prototype.filterItems = function(items) {
        var _items, sortField;
        if (!items || !items.length) {
          this.normalizeSelectedItems([]);
          return items;
        }
        if (this.$scope.searchTerm) {
          _items = filterFilter(items, this.$scope.searchTerm);
        } else {
          _items = items;
        }
        if (this.options.l10nFields.indexOf(this.$scope.stateParams.sort.field) > -1) {
          sortField = this.$scope.stateParams.sort.field + "." + this.$scope.forceLang;
        } else {
          sortField = this.$scope.stateParams.sort.field;
        }
        _items = filterOrderBy(_items, sortField, this.$scope.stateParams.sort.reverse);
        this.$scope.filteredItems = _items;
        this.$scope.totalPages = this.calculatePagesForItems(_items);
        if (this.$scope.stateParams.page && this.$scope.stateParams.page > 1) {
          if (this.$scope.stateParams.page > this.$scope.totalPages) {
            this.$scope.stateParams.page = this.$scope.totalPages;
          }
        }
        this.$scope.isFiltered = _items.length !== items.length;
        _items = filterLimitTo(_items, this.itemsPerPage, this.itemsPerPage * (this.$scope.stateParams.page - 1));
        this.normalizeSelectedItems(_items);
        return _items;
      };

      TableViewLink.prototype.init = function() {
        this.$scope.stateParams = self.getStateParams();
        if (!this.$scope.stateParams.sort || !this.$scope.stateParams.sort.field) {
          if (this.$attrs.sortField && this.$attrs.sortField !== 'undefined') {
            if (this.$attrs.sortField[0] === '-') {
              this.$scope.stateParams.sort = {
                reverse: true,
                field: this.$attrs.sortField.substr(1)
              };
            } else {
              this.$scope.stateParams.sort = {
                reverse: false,
                field: this.$attrs.sortField
              };
            }
          }
        }
        if (!this.$scope.stateParams.sort || !this.$scope.stateParams.sort.field) {
          this.$scope.stateParams.sort = {
            field: 'created',
            reverse: true
          };
        }
        this.$scope.forceLang = CMS_DEFAULT_LANG;
        this.$scope.totalPages = 1;
      };

      TableViewLink.prototype.runWatchers = function() {
        if (this.options.serverPagination) {
          this.serverFilterItems();
          this.$scope.$watch('searchTerm', (function(_this) {
            return function(searchTerm, oldValue) {
              if (searchTerm !== oldValue) {
                _this.$scope.stateParams.page = 1;
                self.saveStateParams(_this.$scope.stateParams);
                _this.serverFilterItems();
              }
            };
          })(this));
          this.$scope.$watch('stateParams', (function(_this) {
            return function(stateParams, oldValue) {
              stateParams = stateParams || {};
              oldValue = oldValue || {};
              if (stateParams.page !== oldValue.page) {
                _this.serverFilterItems(true);
                self.saveStateParams(_this.$scope.stateParams);
                window.smoothScrollTo(0);
                return;
              }
              stateParams.sort = stateParams.sort || {};
              oldValue.sort = oldValue.sort || {};
              if (stateParams.sort.field !== oldValue.sort.field || stateParams.sort.reverse !== oldValue.sort.reverse) {
                _this.serverFilterItems();
                self.saveStateParams(_this.$scope.stateParams);
              }
            };
          })(this), true);
          this.$scope.$watch('queryFilter', (function(_this) {
            return function(oldValue, newValue) {
              if (!angular.equals(oldValue, newValue)) {
                _this.serverFilterItems(true);
              }
            };
          })(this), true);
        } else {
          this.$scope.$watchCollection('items', (function(_this) {
            return function(items, oldValue) {
              if (items) {
                _this.$scope.itemsOnPage = _this.filterItems(items);
              } else {
                _this.$scope.itemsOnPage = _this.filterItems([]);
              }
            };
          })(this), true);
          this.$scope.$watch('searchTerm', (function(_this) {
            return function(searchTerm, oldValue) {
              if (searchTerm !== oldValue) {
                _this.$scope.stateParams.page = 1;
                _this.$scope.itemsOnPage = _this.filterItems(_this.$scope.items || []);
                self.saveStateParams(_this.$scope.stateParams);
              }
            };
          })(this));

          /*
          @$scope.$watch 'sortField', (sortField)=>
              sortField = sortField or ''
              if sortField[0] == '-'
                  @$scope.stateParams.sort = {
                      reverse: true
                      field: sortField.substr(1)
                  }
              else
                  @$scope.stateParams.sort = {
                      reverse: false
                      field: sortField
                  }
              return
           */
          this.$scope.$watch('stateParams', (function(_this) {
            return function(stateParams, oldValue) {
              stateParams = stateParams || {};
              oldValue = oldValue || {};
              if (stateParams.page !== oldValue.page) {
                _this.$scope.itemsOnPage = _this.filterItems(_this.$scope.items || []);
                self.saveStateParams(_this.$scope.stateParams);
                window.smoothScrollTo(0);
                return;
              }
              stateParams.sort = stateParams.sort || {};
              oldValue.sort = oldValue.sort || {};
              if (stateParams.sort.field !== oldValue.sort.field || stateParams.sort.reverse !== oldValue.sort.reverse) {
                _this.$scope.itemsOnPage = _this.filterItems(_this.$scope.items || []);
                self.saveStateParams(_this.$scope.stateParams);
              }
            };
          })(this), true);
        }
      };

      return TableViewLink;

    })();
    this.TableViewLink = TableViewLink;
    return this;
  }]);

}).call(this);
;

/**
*   @ngdoc object
*   @name ui.cms.editable.$translationService
*   @header ui.cms.editable.$translationService
*   @description
*       Сервис для получения переводов с google translate. Кеширует переводы
*       в localStorage(или sessionStorage), дает возможность получать ссылки на переводы
 */

(function() {
  angular.module('ui.cms.editable').service('$translationService', ["$q", "$http", "$injector", "$langPicker", "CmsSettings", function($q, $http, $injector, $langPicker, CmsSettings) {
    var $gdprStorage, SafeGoogleTranslateString, error, findTranslationsInStorage, fixLangCode, ref, saveTranslationsToStorage;
    $gdprStorage = null;
    fixLangCode = function(lang) {
      if (lang === 'cz') {
        return 'cs';
      }
      if (lang === 'ua') {
        return 'uk';
      }
      return lang;
    };
    findTranslationsInStorage = function(fromTo, text_list) {
      var j, len1, no_tr, text, tr;
      $gdprStorage = $gdprStorage || $injector.get('$gdprStorage');
      if (!$gdprStorage.uiCmsGoogleTranslate) {
        $gdprStorage.uiCmsGoogleTranslate = {};
        $gdprStorage.uiCmsGoogleTranslate[fromTo] = {};
        return;
      }
      if (!$gdprStorage.uiCmsGoogleTranslate[fromTo]) {
        $gdprStorage.uiCmsGoogleTranslate[fromTo] = {};
        return;
      }
      tr = {};
      no_tr = [];
      for (j = 0, len1 = text_list.length; j < len1; j++) {
        text = text_list[j];
        if ($gdprStorage.uiCmsGoogleTranslate[fromTo][text]) {
          tr[text] = $gdprStorage.uiCmsGoogleTranslate[fromTo][text];
        } else {
          no_tr.push(text);
        }
      }
      return {
        tr: tr,
        noTr: no_tr
      };
    };
    saveTranslationsToStorage = function(fromTo, fromTextList, toGtranslate, sgt) {
      var i, isUpperCase, j, len1, text, tr, unsafe_translation;
      if (!toGtranslate.data || !toGtranslate.data.translations) {
        return;
      }
      isUpperCase = function(letter) {
        return letter === letter.toUpperCase();
      };
      tr = {};
      $gdprStorage = $gdprStorage || $inject.get('$gdprStorage');
      for (i = j = 0, len1 = fromTextList.length; j < len1; i = ++j) {
        text = fromTextList[i];
        unsafe_translation = sgt.makeUnsafe(text, toGtranslate.data.translations[i].translatedText);
        if (isUpperCase(text[0])) {
          unsafe_translation = unsafe_translation[0].toUpperCase() + unsafe_translation.substr(1);
        }
        tr[text] = unsafe_translation;
        $gdprStorage.uiCmsGoogleTranslate[fromTo][text] = unsafe_translation;
      }
      return tr;
    };
    SafeGoogleTranslateString = (function() {
      function SafeGoogleTranslateString() {
        this.regExp = /\{\{([A-Za-z0-9_\s+\(+\)+\$+\.+]+)\}\}/g;
        this.regExp = /\{\{([^\}]+)\}\}/g;
        this.strings = {};
      }

      SafeGoogleTranslateString.prototype.makeSafe = function(text) {
        var j, len1, match, matches, ng_exprs, safe_expr, safe_text;
        matches = text.matchAll(this.regExp);
        if (!matches) {
          return text;
        }
        ng_exprs = {};
        safe_text = angular.copy(text);
        for (j = 0, len1 = matches.length; j < len1; j++) {
          match = matches[j];
          safe_expr = match[0].replaceAll("{{", "<span class='notranslate'>{{").replaceAll("}}", '}}</span>');
          if (!ng_exprs[safe_expr]) {
            ng_exprs[safe_expr] = match[0];
            safe_text = safe_text.replaceAll(match[0], safe_expr);
          }
        }
        this.strings[text] = ng_exprs;
        return encodeURIComponent(safe_text);
      };

      SafeGoogleTranslateString.prototype.makeUnsafe = function(keyText, text) {
        var expr, ref, value;
        if (!this.strings[keyText]) {
          return text;
        }
        ref = this.strings[keyText];
        for (expr in ref) {
          value = ref[expr];
          text = text.replaceAll(expr, value);
        }
        return text;
      };

      return SafeGoogleTranslateString;

    })();
    this.provider = null;
    try {
      this.provider = CMS_SPECIAL_INTEGRATIONS.translations || null;
      if ((ref = this.provider) !== null && ref !== 'google') {
        throw "$translationService: unknown provider '" + this.provider + "' in CMS_SPECIAL_INTEGRATIONS.translations!";
      }
    } catch (error1) {
      error = error1;
    }
    this.translate = function(sourceLang, targetLang, textList) {
      var _textList, chunk_size, chunks, deferred, i, j, len, len1, origChunks, res, safeTextList, sgt, t, tasks, translated, url;
      sourceLang = fixLangCode(sourceLang);
      targetLang = fixLangCode(targetLang);
      deferred = $q.defer();
      res = findTranslationsInStorage(sourceLang + "-" + targetLang, textList);
      if (!res || angular.isEmpty(res.tr)) {
        _textList = textList;
      } else {
        _textList = res.noTr;
      }
      _textList = textList;
      if (!_textList.length && res) {
        deferred.resolve(res.tr);
        return deferred.promise;
      }
      sgt = new SafeGoogleTranslateString();
      url = CmsSettings.$actionToUrl('translateText', {
        source: sourceLang,
        target: targetLang
      });
      safeTextList = [];
      len = 0;
      for (j = 0, len1 = _textList.length; j < len1; j++) {
        t = _textList[j];
        t = sgt.makeSafe(t);
        len += encodeURIComponent(t).length + 3;
        safeTextList.push(t);
      }
      chunks = [];
      origChunks = [];
      if (len > 4990 || safeTextList.length > 120) {
        chunk_size = parseInt(len / 4500) + 1;
        chunk_size = parseInt(safeTextList.length / chunk_size);
        if (chunk_size > 120) {
          chunk_size = 120;
        }
        i = 0;
        while (i < safeTextList.length) {
          chunks.push(safeTextList.slice(i, i + chunk_size));
          origChunks.push(_textList.slice(i, i + chunk_size));
          i += chunk_size;
        }
      } else {
        chunks = [safeTextList];
        origChunks = [_textList];
      }
      if (res && res.tr) {
        translated = res.tr;
      } else {
        translated = {};
      }
      tasks = chunks.filter(function(c) {
        return c.length;
      }).map(function(chunk, i) {
        var churl, d;
        d = $q.defer();
        churl = url;
        chunk.forEach(function(t) {
          churl += "&q=" + t;
        });
        $http.get(churl).then(function(data) {
          var tr;
          if (data.errors) {
            return d.reject(data.errors);
          }
          tr = saveTranslationsToStorage(sourceLang + "-" + targetLang, origChunks[i], data.data, sgt);
          translated = angular.extend(translated, tr);
          d.resolve();
        }, function(data) {
          return d.reject();
        });
        return d.promise;
      });
      $q.waterfall(tasks).then(function() {
        return deferred.resolve(translated);
      }, function() {
        return deferred.reject();
      });
      return deferred.promise;
    };
    this.getTranslateUrl = function(sourceLang, targetLang, text) {
      sourceLang = fixLangCode(sourceLang);
      targetLang = fixLangCode(targetLang);
      return ("https://translate.google.com.ua/?hl=" + $langPicker.currentLang + "#") + (sourceLang + "/" + targetLang + "/" + (encodeURIComponent(text)));
    };
    return this;
  }]);

}).call(this);
;
(function() {
  angular.module('ui.cms.editable').directive('compareEqual', function() {
    return {
      require: 'ngModel',
      link: function(scope, elem, attrs, ngModel) {
        ngModel.$parsers.unshift(function(value) {
          var isValid;
          isValid = scope.$eval(attrs.compareEqual) === value;
          ngModel.$setValidity('compareEqual', isValid);
          if (isValid) {
            return value;
          } else {
            return void 0;
          }
        });
        ngModel.$formatters.unshift(function(value) {
          var isValid;
          isValid = scope.$eval(attrs.compareEqual) === value;
          ngModel.$setValidity('compareEqual', isValid);
          return value;
        });
        scope.$watch(attrs.compareEqual, function(value) {
          var isValid;
          isValid = ngModel.$modelValue === value;
          ngModel.$setValidity('compareEqual', isValid);
        });
      }
    };
  });

}).call(this);
;
(function() {
  angular.module('ui.cms.editable').directive('emailAvailable', ["$q", "CmsUser", function($q, CmsUser) {
    return {
      restrict: 'AE',
      require: 'ngModel',
      link: function(scope, elm, attr, model) {
        model.$asyncValidators.emailAvailable = function(modelValue, viewValue) {
          var defer, fn, params;
          if (model.$isEmpty(modelValue)) {
            return $q.when();
          }
          defer = $q.defer();
          if (CmsUser.count) {
            fn = CmsUser.count;
            params = {
              where: {
                email: modelValue
              }
            };
          }
          if (CmsUser.querysetExists) {
            fn = CmsUser.querysetExists;
            params = {
              filter: {
                where: {
                  email: modelValue
                }
              }
            };
          }
          fn(params, function(data) {
            if (data.exists || data.count) {
              return defer.reject();
            } else {
              return defer.resolve();
            }
          }, function(data) {
            return defer.reject();
          });
          return defer.promise;
        };
      }
    };
  }]).directive('emailForUsernameAvailable', ["$q", "CmsUser", function($q, CmsUser) {
    return {
      restrict: 'AE',
      require: 'ngModel',
      link: function(scope, elm, attrs, model) {
        model.$asyncValidators.emailForUsernameAvailable = function(modelValue, viewValue) {
          var defer, fn, params, username;
          if (model.$isEmpty(modelValue)) {
            return $q.when();
          }
          defer = $q.defer();
          username = scope.$eval(attrs.emailForUsernameAvailable);
          if (CmsUser.count) {
            fn = CmsUser.count;
            params = {
              where: {
                email: modelValue,
                username: {
                  neq: username
                }
              }
            };
          }
          if (CmsUser.querysetExists) {
            fn = CmsUser.querysetExists;
            params = {
              filter: {
                where: {
                  email: modelValue,
                  username: {
                    neq: username
                  }
                }
              }
            };
          }
          fn(params, function(data) {
            if (data.exists || data.count) {
              return defer.reject();
            } else {
              return defer.resolve();
            }
          }, function(data) {
            return defer.reject();
          });
          return defer.promise;
        };
      }
    };
  }]);

}).call(this);
;
(function() {
  angular.module('ui.cms.editable').directive('uniqueIdInList', function() {
    return {
      require: 'ngModel',
      link: function(scope, elem, attr, ngModel) {
        var isValid;
        isValid = function(text) {
          var error, i, index, item, items, j, len, valid;
          valid = true;
          items = attr.uniqueIdInList;
          index = parseInt(attr.index);
          try {
            items = JSON.parse(items);
          } catch (error1) {
            error = error1;
            items = [];
          }
          for (i = j = 0, len = items.length; j < len; i = ++j) {
            item = items[i];
            if (item.id === text && i !== index) {
              valid = false;
              break;
            }
          }
          return valid;
        };
        ngModel.$parsers.unshift(function(value) {
          var valid;
          valid = isValid(value);
          ngModel.$setValidity('uniqueIdInList', valid);
          if (valid) {
            return value;
          } else {
            return void 0;
          }
        });
        ngModel.$formatters.unshift(function(value) {
          ngModel.$setValidity('uniqueIdInList', isValid(value));
          return value;
        });
      }
    };
  }).directive('valueNotInList', function() {
    return {
      require: 'ngModel',
      link: function(scope, elem, attr, ngModel) {
        var isValid;
        isValid = function(value) {
          var list;
          list = scope.$eval(attr.valueNotInList) || [];
          console.log(list, value);
          return list.indexOf(value) === -1;
        };
        ngModel.$parsers.unshift(function(value) {
          var valid;
          valid = isValid(value);
          ngModel.$setValidity('valueNotInList', valid);
          if (valid) {
            return value;
          } else {
            return void 0;
          }
        });
        ngModel.$formatters.unshift(function(value) {
          ngModel.$setValidity('valueNotInList', isValid(value));
          return value;
        });
      }
    };
  });

}).call(this);
;
(function() {
  angular.module('ui.cms.editable').directive('usernameAvailable', ["$q", "CmsUser", function($q, CmsUser) {
    return {
      restrict: 'AE',
      require: 'ngModel',
      link: function(scope, elm, attr, model) {
        model.$asyncValidators.usernameAvailable = function(modelValue, viewValue) {
          var defer, fn, params;
          if (model.$isEmpty(modelValue)) {
            return $q.when();
          }
          defer = $q.defer();
          if (CmsUser.count) {
            fn = CmsUser.count;
            params = {
              where: {
                username: modelValue
              }
            };
          }
          if (CmsUser.querysetExists) {
            fn = CmsUser.querysetExists;
            params = {
              filter: {
                where: {
                  username: modelValue
                }
              }
            };
          }
          fn(params, function(data) {
            if (data.exists || data.count) {
              return defer.reject();
            } else {
              return defer.resolve();
            }
          }, function(data) {
            return defer.reject();
          });
          return defer.promise;
        };
      }
    };
  }]).directive('usernameChars', ["$timeout", function($timeout) {
    return {
      require: 'ngModel',
      link: function(scope, elem, attr, ngModel) {
        var allow_space, usernameIsValid;
        allow_space = attr.usernameCharsAllowSpace || attr.hasOwnProperty('usernameCharsAllowSpace');
        usernameIsValid = function(username) {
          if (allow_space) {
            return /^[0-9a-zA-Z_ .-]+$/.test(username);
          }
          return /^[0-9a-zA-Z_.-]+$/.test(username);
        };
        ngModel.$parsers.unshift(function(value) {
          var valid;
          if (!value) {
            ngModel.$setValidity('usernameChars', true);
            return value;
          }
          valid = usernameIsValid(value);
          ngModel.$setValidity('usernameChars', valid);
          if (valid) {
            return value;
          } else {
            return void 0;
          }
        });
        ngModel.$formatters.unshift(function(value) {
          ngModel.$setValidity('usernameChars', usernameIsValid(value));
          return value;
        });
        if (!ngModel.$viewValue && !ngModel.$modelValue) {
          $timeout(function() {
            return ngModel.$setValidity('usernameChars', true);
          }, 0);
        }
      }
    };
  }]);

}).call(this);
;

/**
*   @ngdoc directive
*   @name ui.cms.editable.directive:uiceLoginForm
*   @description директива формы логина пользователя. Поддерживает
*       горизонтальное и вертикальное расположение элементов формы.
*   @restrict E
*   @param {boolean=} [showPlaceholder=false] (ссылка) если showPlaceholder == true,
*        то будут скрыты labels и отображены placeholders внутри input-ов.
*   @param {boolean=} [showRememberMe=false] (ссылка) показывать ли галку для "запоминания" логина
*   @param {boolean=} [showResetPassword=false] (ссылка) показывать ли кнопку для сброса пароля
*   @param {string=} formClass (значение) класс формы. Напр.: formClass = 'form-inline'
*   @param {function=} onLogin (функция) функция которая будет выполнена при успешном
*       логине на стороне сервера
*   @param {function=} onError (функция) функция которая будет выполнена при ошибках
*       логина
*   @param {boolean=} ngDisabled (ссылка) флаг доступности input-ов в форме
*   @example
*       <pre>
*           //- pug
*           .col-lg-offset-35.col-lg-30
*                h2 Cms Admin
*                uice-login-form(on-login="onLogin()")
*       </pre>
 */

(function() {
  angular.module('ui.cms.editable').directive('uiceLoginForm', ["CmsUser", "LoopBackAuth", function(CmsUser, LoopBackAuth) {
    return {
      restrict: 'E',
      scope: {
        showPlaceholder: '=?',
        showRememberMe: '=?',
        showResetPassword: '=?',
        formClass: '@?',
        onLogin: '&?',
        onError: '&?',
        ngDisabled: '=?'
      },
      controller: ["$scope", "$currentUser", "$uiceUserHelpers", function($scope, $currentUser, $uiceUserHelpers) {
        var protection;
        if (CMS_DB_BUCKET === 'demo_site') {
          $scope.use_demo = true;
        }
        $scope.cr = {
          rememberMe: true
        };
        $scope.options = {
          passwordType: 'password'
        };
        $scope.error = null;
        $scope.loginProtection = null;
        protection = {
          sms: null
        };
        $scope.login = function() {
          var cr, params;
          $scope.error = null;
          params = {
            rememberMe: $scope.cr.rememberMe
          };
          cr = JSON.parse(JSON.stringify($scope.cr));
          LoopBackAuth.clearUser();
          LoopBackAuth.clearStorage();
          return CmsUser.login(params, cr, function(data) {
            $currentUser.setData(data.user);
            if ($scope.onLogin) {
              return $scope.onLogin({
                data: data
              });
            }
          }, function(data) {
            var ref;
            if (data.status === 400) {
              if ((data.data || {}).protection === 'sms') {
                $scope.error = 'invalid-sms-code';
                $scope.cr.smsCode = '';
                protection.sms = data.data.username;
                $scope.loginProtection = 's';
              } else {
                $scope.error = 'invalid-credentials';
                $scope.cr.password = '';
                $scope.cr.smsCode = '';
              }
            }
            if (data.status === 406) {
              protection.sms = data.data.username;
              $scope.loginProtection = 's';
              $scope.cr.smsCode = '';
            }
            if ((ref = data.status) === 401 || ref === 505) {
              $scope.error = 'invalid-credentials';
              $scope.cr.password = '';
            }
            if ($scope.onError) {
              return $scope.onError(data);
            }
          });
        };
        $scope.resetPassword = function() {
          $uiceUserHelpers.resetPasswordByEmail({
            loginProtection: $scope.loginProtection
          });
        };
        $scope.toggleShowPassword = function() {
          if ($scope.options.passwordType === 'password') {
            $scope.options.passwordType = 'text';
          } else {
            $scope.options.passwordType = 'password';
          }
        };
        return $scope.$watch('cr.username', function(username, oldValue) {
          if (username !== protection.sms) {
            $scope.loginProtection = null;
          }
        });
      }],
      template: ('/client/cms_editable_app/cmsUser/_directives/uiceLoginForm/uiceLoginForm.html', '<form class="{{formClass}}" ng-if="showPlaceholder" name="form"><div class="uice-login-form-inputs"><p class="text-center" translate="" ng-if="use_demo" style="text-indent:0px;">Use login \'demo\' and password \'1\'</p><label class="ng-invalid" ng-if="error==\'invalid-credentials\' &amp;&amp; formClass" translate="translate">Invalid username or password</label><label class="ng-invalid" ng-if="error==\'invalid-sms-code\' &amp;&amp; formClass" translate="translate">Invalid SMS code</label><div class="text-center"><label class="text-danger" ng-if="error==\'invalid-credentials\' &amp;&amp; !formClass" translate="translate">Invalid username or password</label><label class="text-danger" ng-if="error==\'invalid-sms-code\' &amp;&amp; !formClass" translate="translate">Invalid SMS code</label></div><div class="form-group"><input class="form-control" type="text" placeholder="{{\'Username or email\' | translate }}" ng-disabled="ngDisabled" required="required" ng-model="cr.username" name="username" autofocus=""/></div><div class="form-group"><div class="input-group"><input class="form-control" type="{{options.passwordType}}" placeholder="{{\'Password\' | translate}}" ng-disabled="ngDisabled" required="required" ng-model="cr.password" name="password" uic-enter-press="login()"/><div class="input-group-btn"><button class="btn btn-default" ng-click="toggleShowPassword()" title="{{\'Toggle password visibility\' | translate}}" ng-class="{active: options.passwordType == \'password\'}"><span class="fas nopadding" ng-class="{\'fa-eye\': options.passwordType == \'text\', \'fa-eye-slash\': options.passwordType == \'password\'}"></span></button></div></div></div><div class="form-group" ng-if="loginProtection == \'s\'"><label translate="">Enter the four-digit code here (the code is valid for only 3 minute), that has been sent to the phone numbers that are indicated in your profile as main ones</label><input class="form-control" placeholder="{{\'Confirmation code from SMS message\' | translate}}" ng-model="cr.smsCode" name="smsCode" ng-disabled="ngDisabled" required="required"/></div><uic-checkbox ng-model="cr.rememberMe" ng-show="showRememberMe"><span translate="translate">Remember me</span></uic-checkbox><div class="form-group" ng-if="showResetPassword"><a class="uice-login-form-reset-password" ng-click="resetPassword()" ng-switch="error == \'invalid-sms-code\'"><span ng-switch-when="true" translate="">Reset password and phone numbers</span><span ng-switch-when="false" translate="">Reset password</span></a></div></div><div class="uice-login-form-submit"><button class="btn btn-primary" ng-class="{\'btn-block\': !formClass}" translate="translate" ng-click="login()" ng-disabled="form.$invalid || ngDisabled">Login</button></div></form><form class="{{formClass}}" ng-if="!showPlaceholder" name="form"><div class="uice-login-form-inputs"><p class="text-center" translate="" ng-if="use_demo" style="text-indent:0px;">Use login \'demo\' and password \'1\'</p><label class="ng-invalid" ng-if="error==\'invalid-credentials\' &amp;&amp; formClass" translate="translate">Invalid username or password</label><label class="ng-invalid" ng-if="error==\'invalid-sms-code\' &amp;&amp; formClass" translate="translate">Invalid SMS code</label><div class="text-center"><label class="text-danger" ng-if="error==\'invalid-credentials\' &amp;&amp; !formClass" translate="translate">Invalid username or password</label><label class="text-danger" ng-if="error==\'invalid-sms-code\' &amp;&amp; !formClass" translate="translate">Invalid SMS code</label></div><div class="form-group"><label translate="translate">Username or email</label><input class="form-control" type="text" ng-disabled="ngDisabled" required="required" ng-model="cr.username" name="username" autofocus=""/></div><div class="form-group"><label translate="translate">Password</label><div class="input-group"><input class="form-control" type="{{options.passwordType}}" ng-disabled="ngDisabled" required="required" ng-model="cr.password" name="password" uic-enter-press="login()"/><div class="input-group-btn"><button class="btn btn-default" ng-click="toggleShowPassword()" title="{{\'Toggle password visibility\' | translate}}" ng-class="{active: options.passwordType == \'password\'}"><span class="fas nopadding" ng-class="{\'fa-eye\': options.passwordType == \'text\', \'fa-eye-slash\': options.passwordType == \'password\'}"></span></button></div></div></div><div class="form-group" ng-if="loginProtection == \'s\'"><label><span translate="">Confirmation code from SMS message</span><span class="text-danger"> *</span></label><input class="form-control" ng-model="cr.smsCode" name="smsCode" ng-disabled="ngDisabled" required="required"/><p class="help-block"><span>* - </span><span translate="">enter the four-digit code here (the code is valid for only 3 minute), that has been sent to the phone numbers that are indicated in your profile as main ones</span></p></div><uic-checkbox ng-model="cr.rememberMe" ng-show="showRememberMe"><span translate="translate">Remember me</span></uic-checkbox><div class="form-group" ng-if="showResetPassword"><a class="uice-login-form-reset-password" ng-click="resetPassword()" ng-switch="error == \'invalid-sms-code\'"><span ng-switch-when="true" translate="">Reset password and phone numbers</span><span ng-switch-when="false" translate="">Reset password</span></a></div></div><div class="uice-login-form-submit"><button class="btn btn-primary" ng-class="{\'btn-block\': !formClass}" translate="translate" ng-click="login()" ng-disabled="form.$invalid || ngDisabled">Login</button></div></form>' + '')
    };
  }]);

}).call(this);
;

/**
*   @ngdoc directive
*   @name ui.cms.editable.directive:uiceLogout
*   @description директива кнопки разлогина пользователя.
*   @restrict E
*   @param {function=} onLogout (функция) функция которая будет выполнена при успешном
*       разлогине на стороне сервера
*   @param {function=} onError (функция) функция которая будет выполнена при ошибках
*       разлогина
*   @param {boolean=} ngDisabled (ссылка) флаг доступности кнопки
*   @example
*       <pre>
*           //- pug
*           uice-logout(on-logout="onLogout()")
*       </pre>
 */

(function() {
  angular.module('ui.cms.editable').directive('uiceLogout', function() {
    return {
      restrict: 'E',
      scope: {
        onLogout: '&?',
        onError: '&?',
        ngDisabled: '=?'
      },
      controller: ["$scope", "$state", "$injector", "CmsUser", function($scope, $state, $injector, CmsUser) {
        var gotoLoginView, loginMogal;
        if ($injector.has('loginMogal')) {
          loginMogal = $injector.get('loginMogal');
        }
        gotoLoginView = function() {
          if (loginMogal) {
            return loginMogal.open();
          }
          $state.go('app.login', {
            returnTo: $state.current.name,
            returnToParams: $state.params
          });
        };
        return $scope.logout = function() {
          return CmsUser.logout({}, function(data) {
            if ($scope.onLogout) {
              return $scope.onLogout();
            }
            gotoLoginView();
          }, function(data) {
            if ($scope.onError) {
              return $scope.onError();
            }
            gotoLoginView();
          });
        };
      }],
      template: ('/client/cms_editable_app/cmsUser/_directives/uiceLogout/uiceLogout.html', '<span ng-click="logout()"><span class="fas fa-sign-out-alt"></span><span class="text" translate="">Logout</span></span>' + '')
    };
  }).directive('uiceLogoutForNavbar', ["CmsUser", "$state", "$injector", function(CmsUser, $state, $injector) {
    return {
      restrict: 'A',
      replace: true,
      link: function($scope, $element, attrs) {
        var gotoLoginView, loginMogal;
        $scope.attrs = attrs;
        if ($injector.has('loginMogal')) {
          loginMogal = $injector.get('loginMogal');
        }
        gotoLoginView = function() {
          if (loginMogal) {
            return loginMogal.open();
          }
          $state.go('app.login', {
            returnTo: $state.current.name,
            returnToParams: $state.params
          });
        };
        return $scope.logout = function() {
          CmsUser.logout({}, gotoLoginView, gotoLoginView);
        };
      },
      template: ('/client/cms_editable_app/cmsUser/_directives/uiceLogout/uiceLogoutForNavbar.html', '<li><a style="cursor:pointer;" ng-disabled="attrs.disabled" ng-click="logout()"><span translate="">Logout</span></a></li>' + '')
    };
  }]);

}).call(this);
;
(function() {
  angular.module('ui.cms.editable').directive('uiceRegisterForm', function() {
    return {

      /**
      *   @ngdoc directive
      *   @name ui.cms.editable.directive:uiceRegisterForm
      *   @description
      *
      *   @restrict E
      *   @param {object} ngModel (ссылка) модель
      *   @param {boolean=} ngDisabled (ссылка) флаг доступности тега
      *   @example
      *       <pre>
      *           $scope.
      *       </pre>
      *       <pre>
      *           //- pug
      *       </pre>
      *       <example module="DocExampleApp">
      *           <file name="script.js">
      *               mkDocCtrl('uiceRegisterFormCtrl', {
      *
      *               });
      *           </file>
      *           <file name="index.html">
      *               <div ng-controller="uiceRegisterFormCtrl">
      *
      *               </div>
      *           </file>
      *       </example>
       */
      restrict: 'E',
      transclude: true,
      scope: {
        showPlaceholder: '=?',
        defaultUser: '=?',
        options: '=?',
        formClass: '@?',
        onRegister: '&?',
        onError: '&?',
        ngDisabled: '=?'
      },
      controller: ["$scope", "$uiceUserHelpers", function($scope, $uiceUserHelpers) {
        var i, k, len, ref;
        $scope.error = 0;
        $scope.user = angular.copy($scope.defaultUser || {});
        if (!$scope.options) {
          $scope.options = {};
        }
        if (!$scope.options.fields) {
          $scope.options.fields = ['username', 'email', 'password', 'password2'];
        }
        if (!$scope.options.hasOwnProperty('emailVerified')) {
          $scope.options.emailVerified = false;
        }
        $scope.isVisibleField = {};
        ref = $scope.options.fields;
        for (i = 0, len = ref.length; i < len; i++) {
          k = ref[i];
          $scope.isVisibleField[k] = true;
        }
        return $scope.register = function() {
          return $uiceUserHelpers.registerUser($scope.user, $scope.options).then(function(data) {
            if ($scope.onRegister) {
              return $scope.onRegister(data);
            }
          }, function(data) {
            $scope.error = data.status || 422;
            if ($scope.onError) {
              return $scope.onError(data);
            }
          });
        };
      }],
      template: ('/client/cms_editable_app/cmsUser/_directives/uiceRegisterForm/uiceRegisterForm.html', '<label class="ng-invalid" ng-if="error &amp;&amp; error!=422 &amp;&amp; formClass" translate="">Can\'t register user. Try again later</label><label class="ng-invalid" ng-if="error==422 &amp;&amp; formClass" translate="">User with this email or username already exists</label><form class="{{formClass}}" ng-if="showPlaceholder" name="form"><div class="form-group" ng-if="isVisibleField.username"><label ng-show="form.username.$error"><small class="error" ng-show="form.username.$error.usernameAvailable" translate="">Username alredy taken</small><small class="text-success" ng-show="form.username.$pending">please wait...</small><small class="error" ng-show="form.username.$error.usernameChars" translate="">Invalid chars in username</small></label><input class="form-control" type="text" name="username" ng-disabled="ngDisabled" required="required" username-available="" username-chars="" ng-model="user.username" placeholder="{{\'Username\' | translate}}. {{\'Only a-z, 0-9, and -_. allowed\' | translate}}" maxlength="150"/></div><div class="form-group" ng-if="isVisibleField.email"><label ng-show="form.email.$error"><small class="error" ng-show="form.email.$error.emailAvailable" translate="">Email alredy registered</small><small class="text-success" ng-show="form.email.$pending">please wait...</small></label><input class="form-control" type="email" name="email" ng-disabled="ngDisabled" required="" email-available="" placeholder="{{\'Email\' | translate}}" ng-model="user.email"/></div><div class="form-group" ng-if="isVisibleField.password"><input class="form-control" type="password" name="password" ng-disabled="ngDisabled" required="" placeholder="{{\'Password\' | translate}}" ng-model="user.password"/></div><div class="form-group" ng-if="isVisibleField.password2"><label ng-show="form.password2.$error"><small class="error" ng-show="form.password2.$error.compareEqual" translate=""> Password doesn\'t match</small></label><input class="form-control" type="password" name="password2" ng-disabled="ngDisabled" required="" ng-model="user.password2" placeholder="{\'Repeat password\' | translate}" compare-equal="user.password"/></div><div class="text-center" ng-show="error &amp;&amp; !formClass"><label class="text-danger" ng-if="error &amp;&amp; error!=422" translate="">Can\'t register user. Try again later</label><label class="text-danger" ng-if="error==422" translate="">User with this email or username already exists</label></div><button class="btn btn-primary" ng-class="{\'btn-block\': !formClass}" translate="translate" ng-click="register()" ng-disabled="form.$invalid || ngDisabled">Register</button></form><form class="{{formClass}}" ng-if="!showPlaceholder" name="form"><div class="form-group" ng-if="isVisibleField.username"><label><span translate="">Username</span><span class="required">*</span><small class="error" ng-show="form.username.$error.usernameAvailable" translate="">Username alredy taken</small><small class="text-success" ng-show="form.username.$pending">please wait...</small><small class="error" ng-show="form.username.$error.usernameChars" translate="">Invalid chars in username</small></label><input class="form-control" type="text" name="username" ng-disabled="ngDisabled" required="required" username-available="" username-chars="" ng-model="user.username" placeholder="{{\'Only a-z, 0-9, and -_. allowed\' | translate}}" maxlength="150"/></div><div class="form-group" ng-if="isVisibleField.email"><label><span translate="">Email</span><span class="required">*</span><small class="error" ng-show="form.email.$error.emailAvailable" translate="">Email alredy registered</small><small class="text-success" ng-show="form.email.$pending">please wait...</small></label><input class="form-control" type="email" name="email" ng-disabled="ngDisabled" required="" email-available="" ng-model="user.email"/></div><div class="form-group" ng-if="isVisibleField.password"><label><span translate="translate">Password</span><span class="required">*</span></label><input class="form-control" type="password" name="password" ng-disabled="ngDisabled" required="" ng-model="user.password"/></div><div class="form-group" ng-if="isVisibleField.password2"><label><span translate="translate">Repeat password</span><span class="required">*</span><small class="error" ng-show="form.password2.$error.compareEqual" translate=""> Password doesn\'t match</small></label><input class="form-control" type="password" name="password2" ng-disabled="ngDisabled" required="" ng-model="user.password2" compare-equal="user.password"/></div><div class="text-center" ng-show="error &amp;&amp; !formClass"><label class="text-danger" ng-if="error &amp;&amp; error!=422" translate="">Can\'t register user. Try again later</label><label class="text-danger" ng-if="error==422" translate="">User with this email or username already exists</label></div><button class="btn btn-primary" ng-class="{\'btn-block\': !formClass}" translate="translate" ng-click="register()" ng-disabled="form.$invalid || ngDisabled">Register</button></form>' + '')
    };
  });

}).call(this);
;
angular.module('ui.cms').run(['gettextCatalog', function (gettextCatalog) {
/* jshint -W100 */
    gettextCatalog.setStrings('cz', {"<span uic-bind-html=\"options.representationHtml\"></span>\" will be deleted immediately":"<span uic-bind-html=\"options.representationHtml\"></span>\" bude okamžitě smazán","Accessibility Icons":"Ikony Dostupnosti","Actions":"Akce","Add":{"$$noContext":"Přidat","append":"Připojit"},"Add contact":"Přidat kontakt","Add files":"Přidat soubor","Add item":"Přidat položku","Add link":"Přidat odkaz","Add menu item":"Přidat položku v menu","Add new":"Přidat novou","Add new item":"Přidat novou položku","All":"Vše","All on page":"Vše na stránce","Allowed file types: <b>{{options.accept}}</b>":"Povolené typy souborů: <b>{{options.accept}}</b>","Allowed max file size <b>{{options.maxSize}}</b>":"Povolená maximální velikost souboru <b>{{options.maxSize}}</b>","Apply":"Aplikovat","Apply translation":"Použít překlad","Article category":"Kategorie článku","Brand Icons":"Ikony Značek","Can't delete object. Try again later":"Objekt nelze odstranit. Zkuste to později znovu","Can't delete objects. Try again later":"Objekt nelze odstranit. Zkuste to později znovu","Can't register user. Try again later":"Není možné zaregistrovat uživatele. Zkuste to později","Can't set user password. Try again later":"Nelze nastavit heslo uživatele. Zkuste to později znovu","Can't update title and description for images=(":"Nelze aktualizovat název a popis obrázku=(","Can't upload file to server=(":"Nelze nahrát soubor na server =(","Cancel":"Zrušit","Change icon":"Změnit ikonu","Chart Icons":"Ikonky grafu","Choose files from your computer":"Vyberte soubory z počítače","Click to change the way of saving filter parameters":"Kliknutím změníte způsob ukládání parametrů filtru","Close":"Zavřít","Columns":"Sloupce","Confirmation code from SMS message":"Potvrzovací kód z SMS zprávy","Contact data":"Kontaktní údaje","Contact type":"Typ kontaktních údajů","Contacts":"Kontakty","Contains":"Obsahuje","Content language":"Jazyk obsahu","Create":"Vytvořit","Created":"Vytvořeno","Csv text file":"Textový soubor CSV","Currency Icons":"Ikonky Měn","Currency picker":"Přepínač měn pro uživatele","Custom Directive":"Uživatelská direktiva","Czech":"Čeština","Delete":"Smazat","Delete all selected objects?":"Smazat všechny vybrané objekty?","Delete avatar?":"Smazat avatar?","Delete file?":"Smazat?","Delete icon":"Smazat ikonku","Delete main image?":"Chcete smazat hlavní obrázek?","Delete?":"Smazat?","Description":"Popis","Desktop":{"screen size":"Desktop"},"Directional Icons":"Směrové ikony","Download":"Stáhnout","Download file":"Stáhnout soubor","Drop file here":"Přesunout sem soubor","Edit":{"$$noContext":"Upravit","small":"Upravit"},"Edit file":"Upravit soubor","Edit properties for language":"Upravte vlastnosti jazyka","Editor":"Editor","Element":"Prvek","Email":"Email","Email alredy registered":"Email už je zaregistrován","Email. Ex.: some@mail.com":"Email. Napr.: some@mail.com","Emails only":"Pouze email adresy","English":"English","Enter the four-digit code here (the code is valid for only 3 minute), that has been sent to the phone numbers that are indicated in your profile as main ones":"Prosim zadejte čtyřmístný kód zde (kód je platný pouze 3 minuty) z SMSky, která byla odeslána na telefonní čísla, které jsou uvedeny ve vašem profilu jako hlavní","Error on text translation":"Chyba při překladu textu","Excel 2007":"Excel 2007","Export":"Export","Fax":"Fax","File Type Icons":"Ikonky typů souborů","File format":"Formát souboru","File is not available for preview":"Soubor není k dispozici pro náhled","File is too big. Max size is":"Soubor je příliš velký. Maximální velikost je","File is too big. Size should be &lt;= {{options.maxSize}}":"Soubor je příliš velký. Velikost by měla být &lt;= {{options.maxSize}}","Files":"Soubory","Form Control Icons":"Ikony Form Control","From date":"Od datumu","Gender Icons":"Ikonky pohlaví","Generate password":"Generovat heslo","Greater than or equals":"Větší nebo rovno","Hand Icons":"Ikonky Rukou","Hide menu item for languages":"Skrýt položku nabídky pro jazyky","Hide menu item for screen size":"Skrýt položku nabídky pro velikost obrazovky","Icon":"Ikonka","Invalid SMS code":"Neplatný kód SMS","Invalid chars in username":"Nedostupné symboly be jméně uživateke","Invalid file type. Allowed are":"Neplatný typ souboru. Povolené jsou","Invalid file type. Allowed are: <b>{{options.accept}}</b>":"Neplatný typ souboru. Povolené jsou: <b>{{options.accept}}</b>","Invalid username or password":"Nesprávné jméno či heslo","Items":"Prvky","Items on page":"Položek na stránce","Language":"Jazyk","Language picker":"Přepínač jazyků","Less than or equals":"Méně nebo rovno","Link":"Odkaz","Link to anchor on page":"Odkaz na kotvu na stránce","Link to article":"Odkaz na článek","Link to external site":"Odkaz na jiný web","Link to file":"Odkaz na soubor","Link to picture album":"Odkaz na album obrázků","Login":"Přihlásit se","Logout":"Odhlásit se","Main image":"Hlavní obrázek","Medical Icons":"Lékařské ikonky","Menu item":"Položka nabídky","Menu item type":"Typ položky nabídky","Mobile":{"screen size":"Mobile"},"Name":"Název","New password":"Nové heslo","New password is set":"Nové heslo je nastaveno","Noname":"Bez jména","Not contains":"Neobsahuje","Number":"Číslo","OK":"ОК","Ok":"Ok","Only a-z, 0-9, and -_. allowed":"Přípustné pouze a-z, 0-9,  -_. symboly","Only one country":"Pouze jedna země","Oopps":"Ups","Open in a new window or tab":"Otevírat v novém okně nebo kartě","Password":"Heslo","Password doesn't match":"Hesla se neshodují","Paste link to page":"Vložte odkaz na stránku","Payment Icons":"Ikony platebních systémů","Phone":"Mobilní číslo","Phone + WhatsUp/Viber":"Mobilní číslo + WhatsUp/Viber","Phones only":"Pouze mobilní číslo","Phones, WhatsApp/Viber, Skype":"Mobilní číslo, WhatsApp/Viber, Skype","Please select an object from the dropdown list in order to add it to the list":"Prosím, vyberte objekt z dropdown-seznamu, aby jej přidat do seznamu","Price":"Cena","Public contacts":"Veřejné kontakty","Published":"Opublikováno","Refresh":"Obnovit","Register":"Zaregistrovat se","Regular Icons":"Regular Icons","Remember me":"Pamatovat si mě","Remove":"Smazat","Remove from list":"Odstranit ze seznamu","Repeat password":"Zopakujte heslo","Reset":"Obnovit","Reset filter":"Obnovit filtr","Reset password":"Obnovit heslo","Reset password and phone numbers":"Resetujte heslo a telefonní čísla","Reset to default":"Resetovat","Retry":"Zkusit to znovu","Russian":"Rusky","Save":"Uložit","Save filtering preferences in the browser":"Uložit předvolby filtrování v prohlížeči","Search items":"Vyhledat položky","Select all":"Vybrat vše","Select anchor":"Vybrat kotvu","Select article":"Vybrat článek","Select category":"Vybrat kategorii","Select color":"Vybrat barvu","Select file":"Vybrat soubor","Select icon":"Vybrat ikonku","Select none":"Vybrat žádnou","Select picture album":"Vyberte album obrázků","Select static page":"Vybrat statickou stránku","Selected rows":"Vybrané řádky","Set":{"verb":"Nastavit"},"Set as main image":"Nastavit jako hlavní snímek","Set icon":"Nastavit ikonku","Settings":"Nastavení","Show contacts":"Zobrazit kontakty","Site action":"Webové akce","Site directive":"Upravení webu","Skype":"Skype","Spinner Icons":"Inkonky načítání","Submenu":"Podnabídka","Table":"Tabulka","Tablet":{"screen size":"Tablet"},"Text Editor Icons":"Ikonky textových redaktorů","There are too many data rows selected to be exported. Data will be exported in the background and the file will be sent to your email":"Existuje příliš mnoho řádků dat, které mají být exportovány. Data budou exportována na pozadí a soubor bude zaslán na váš e-mail","There's nothing in here":"Zde nic není","This field is multilingual":"Toto pole je vícejazyčné","This items will be deleted immediately":"Tyto položky budou okamžitě smazány","Till date":"Do","Title":"Záhlaví","To assign the main image, you must first save the document":"Chcete-li přiřadit hlavní obrázek, musíte nejprve dokument uložit","To upload linked files, you must first save the document":"Chcete-li stáhnout související soubory, musíte nejprve dokument uložit","Toggle password visibility":"Přepnout viditelnost hesla","Translation suggested by":"Překlad navrhl","Translation suggested by Google Translate":"Překlad navrhl Překladač Google","Transportation Icons":"Dopravní Ikony","Type angular ui.route name":"Zadejte název angular ui.route","Type link to your social media page":"Vložte odkaz na sociální síť zde","Type your contact (email, phone, skype, fax)":"Vaše kontaktní údaje(pošta, telefon, skype, fax)","Unable to delete file":"Soubor nelze smazat","Unique identifier":"Unikátní identifikátor","Update":"Aktualizovat","Upload":"Stáhnout","Upload file":["Nahrát soubor","Uložit soubory","Uložit soubory"],"Upload files":"Uložit složku","Upload image":"Stáhnout obrázek","Use login 'demo' and password '1":"Používejte login 'demo' a heslo '1' pro vstup","User with this email or username already exists":"Uživatel se stejným jménem nebo emailem už je zaregistrován","Username":"Login uživatele","Username alredy taken":"Jméno uživatele už je použito","Username or email":"Login a email","Value":"Hodnota","Video Player Icons":"Ikonky videopřehrávače","View file":"Prohlédnout soubor","View/Route":"Představení/Cesta(angular)","Visible columns":"Viditelné sloupce","We can't find {{options.email}} in our database. Please, give us email that associated with your account on this site":"E-mailová adresa {{options.email}} se v naší databázi nenachází. Uveďte prosim e-mailovou adresu, která je spojena s Vaším účtem","We've sent you email with instructions on resetting your password":"Poslali jsme vám e-mail s pokyny k obnovení hesla","We've sent you email with instructions on resetting your password and changing the phone numbers":"Poslali jsme Vám e-mail s pokyny k resetování hesla a změně telefonních čísel","Web Application Icons":"Ikony web aplikací","WhatsUp/Viber":"WhatsUp/Viber","You can't undo this action":{"delete object":"Tuto akci nelze vrátit zpět"},"Your email":"Vaše emailová adresa","Your search - <b>\"{{searchTerm}}\"</b> - did not match any documents":"Vašemu vyhledávání - <b>\"{{searchTerm}}\"</b> - neodpovídají žádné dokumenty","Your search did not match any documents":"Na vyžádání nebylo nalezeno nic","day":{"medium duration text":["den","dne","dní"]},"enter the four-digit code here (the code is valid for only 3 minute), that has been sent to the phone numbers that are indicated in your profile as main ones":"prosim zadejte čtyřmístný kód zde (kód je platný pouze 3 minuty) z SMSky, která byla odeslána na telefonní čísla, které jsou uvedeny ve vašem profilu jako hlavní","hour":{"medium duration text":["hodina","hodiny","hodin"]},"minute":{"medium duration text":["minuta","minuty","minut"]},"required fields":"povinná pole","second":{"medium duration text":["sekunda","sekundy","sekund"]},"to bottom":"dolu","to top":"nahoru","view all":"zobrazit vše"});
    gettextCatalog.setStrings('de', {"Logout":"Ausloggen"});
    gettextCatalog.setStrings('ru', {"(copyright metadata)":"(метаданные копирайта)","<span uic-bind-html=\"options.representationHtml\"></span>\" will be deleted immediately":"<span uic-bind-html=\"options.representationHtml\"></span>\" будет удален","Accessibility Icons":"Иконки доступности","Actions":"Действия","Add":{"$$noContext":"Добавить","append":"Добавить"},"Add contact":"Добавить контакт","Add files":"Добавить файлы","Add item":"Добавить пункт","Add link":"Добавить ссылку","Add menu item":"Добавить пункт в меню","Add new":"Создать новый","Add new item":"Добавить новый пункт","All":"Все","All on page":"Все на странице","Allowed file types: <b>{{options.accept}}</b>":"Разрешены типы файлов: <b>{{options.accept}}</b>","Allowed max file size <b>{{options.maxSize}}</b>":"Максимальный разрешенный размер файла - <b>{{options.maxSize}}</b>","Apply":"Применить","Apply translation":"Применить перевод","Article category":"Категория со статьями","Brand Icons":"Иконки брендов","Can't delete object. Try again later":"Не получилось удалить объект. Попытайтесь позже","Can't delete objects. Try again later":"Не получилось удалить объекты. Попытайтесь позже","Can't register user. Try again later":"Невозможно зарегистрировать пользователя. Попытайтесь позже","Can't set user password. Try again later":"Не получилось изменить пароль пользователя. Попытайтесь позже","Can't update title and description for images=(":"Обновить заголовок и описание для изобажения не получилось=(","Can't upload file to server=(":"Ошибка при загрузке файла на сервер=(","Cancel":"Отменить","Change icon":"Сменить иконку","Chart Icons":"Иконки диаграмм","Choose files from your computer":"Выберите файлы с вашего компьютера","Click to change the way of saving filter parameters":"Нажмите чтоб изменить стратегию сохранения параметров фильтрации таблицы","Close":"Закрыть","Columns":"Колонки","Confirmation code from SMS message":"Код-подтверждение из смс","Contact data":"Контактные данные","Contact type":"Тип контактных данных","Contacts":"Контакты","Contains":"Содержит","Content language":"Язык контента","Copyright metadata":"Метаданные копирайта","Create":"Создать","Created":"Создано","Creator":{"copyright":"Создатель/фотограф"},"Creator's url":{"copyright":"Ссылка на создателя/фотографа"},"Csv text file":"Csv текстовый файл","Currency Icons":"Иконки валют","Currency picker":"Переключатель валют для пользователя","Custom Directive":"Пользовательская директива","Czech":"Чешский","Delete":"Удалить","Delete all selected objects?":"Удалить выбранные объекты?","Delete avatar?":"Удалить аватар?","Delete file?":"Удалить файл?","Delete icon":"Удалить иконку","Delete main image?":"Удалить основное изображение?","Delete?":"Удалить?","Description":"Описание","Desktop":{"screen size":"Десктоп"},"Directional Icons":"Иконки направлений","Download":"Скачать","Download file":"Скачать файл","Drop file here":"Бросьте файл сюда","Edit":{"$$noContext":"Редактировать","small":"Ред."},"Edit file":"Редактировать файл","Edit properties for language":"Редактировать свойства для языка","Editor":"Редактир","Element":"Элемент","Email":"Email","Email alredy registered":"Email уже зарегистрирован","Email. Ex.: some@mail.com":"Email. Например: some@mail.com","Emails only":"Только почтовые ящики","English":"Английский","Enter the four-digit code here (the code is valid for only 3 minute), that has been sent to the phone numbers that are indicated in your profile as main ones":"Введите четырехзначный код в это поле (код доступен для ввода 3 минуты), который был отправлен на ваши номера телефонов указанные в профиле как основные","Error on text translation":"Ошибка при переводе текста","Export":"Экспортировать","Fax":"Fax","File Type Icons":"Иконки типов файлов","File format":"Формат файла","File is not available for preview":"Файл не доступен для превью","File is too big. Max size is":"Файл слишком большой. Максимальный размер должен быть","File is too big. Size should be &lt;= {{options.maxSize}}":"Файл слишком большой. Разрешен размер не более {{options.maxSize}}","Files":"Файлы","Form Control Icons":"Иконки форм","From date":"С даты","Gender Icons":"Иконки полов","Generate password":"Сгенерировать пароль","Greater than or equals":"Больше чем или равно","Hand Icons":"Иконки рук","Hide menu item for languages":"Скрывать пункт меню для языков","Hide menu item for screen size":"Скрывать пункт меню для размеров экрана","Icon":"Иконка","Invalid SMS code":"Неверный смс-код","Invalid chars in username":"Недопустимые символы в имени пользователя","Invalid file type. Allowed are":"Неправильный тип файла. Разрешены только ","Invalid file type. Allowed are: <b>{{options.accept}}</b>":"Неверный тип файла. Разрешены только <b>{{options.accept}}</b>","Invalid username or password":"Неверное имя пользователя или пароль","Items":"Элементы","Items on page":"Эл-в на странице","Language":"Язык","Language picker":"Переключалка языков","Less than or equals":"Меньше чем или равно","Link":"Ссылка","Link to anchor on page":"Ссылка на якорь на странице","Link to article":"Ссылка на статью","Link to external site":"Ссылка на другой сайт","Link to file":"Ссылка на файл","Link to picture album":"Ссылка на альбом картинок","Login":"Войти","Logout":"Выйти","Logout button":"Кнопка выхода","Main image":"Основное изображение","Medical Icons":"Медицинские иконки","Menu item":"Пункт меню","Menu item type":"Тип пункта меню","Mobile":{"screen size":"Мобильные телефоны"},"Name":"Название","New password":"Новый пароль","New password is set":"Новый пароль установлен","Noname":"Без имени","Not contains":"Не содержит","Number":"Число","OK":"ОК","Ok":"Ок","Only a-z, 0-9, and -_. allowed":"Допустимы только a-z, 0-9,  -_. символы","Only one country":"Только одна страна","Oopps":"Ооой","Open in a new window or tab":"Открывать в новом окне или вкладке","Owner":"Владелец","Password":"Пароль","Password doesn't match":"Пароли не совпадают","Paste link to page":"Вставьте ссылку на страницу","Payment Icons":"Иконки платежных систем","Phone":"Телефон","Phone + WhatsUp/Viber":"Телефон + WhatsUp/Viber","Phones only":"Только телефоны","Phones, WhatsApp/Viber, Skype":"Телефоны, WhatsApp/Viber, Skype","Please select an object from the dropdown list in order to add it to the list":"Выберите объект из дропдауна сверху для того, чтоб добавить его в список","Price":"Цена","Public contacts":"Публичные контакты","Published":"Опубликовано","Refresh":"Обновить","Register":"Зарегистрироваться","Regular Icons":"Стандартные иконки","Remember me":"Запомнить меня","Remove":"Удалить","Remove from list":"Убрать из списка","Repeat password":"Повторите пароль","Reset":"Сбросить","Reset filter":"Сбросить фильтр","Reset password":"Сбросить пароль","Reset password and phone numbers":"Сбросить пароль и телефонные номера","Reset to default":"Сбросить","Retry":"Попытаться снова","Russian":"Русский","Save":"Сохранить","Save filtering preferences in the browser":"Сохранять параметры фильтрации в браузере","Search items":"Поиск элементов","Select all":"Выбрать все","Select anchor":"Выберите якорь","Select article":"Выбрите статью","Select category":"Выберите категорию","Select color":"Выберите цвет","Select file":"Выбрать файл","Select icon":"Установить иконку","Select none":"Снять выделение","Select picture album":"Выберите альбом картинок","Select static page":"Выберите страницу","Selected rows":"Выбранные строки","Set":{"verb":"Назначить"},"Set as main image":"Сделать осн. изображением","Set icon":"Установить иконку","Settings":"Настройки","Show contacts":"Показывать контакты","Site action":"Специальные действия сайта","Site directive":"Директива сайта","Skype":"Skype","Spinner Icons":"Иконки загрузок","Submenu":"Подменю","Table":"Таблица","Tablet":{"screen size":"Планшет"},"Text Editor Icons":"Иконки текстовых редакторов","There are too many data rows selected to be exported. Data will be exported in the background and the file will be sent to your email":"Слишком много объектов необходимо экспортировать. Данные будут экспортированы в бэкграунде, после чего файл будет отправлен вам на почту","There's nothing in here":"Объектов нет","This field is multilingual":"Это поле - мультиязычное","This items will be deleted immediately":"Следующие элементы будут удалены","Till date":"По дату","Title":"Заголовок","To assign the main image, you must first save the document":"Для назначения основного изображения нужно сначала сохранить документ","To upload linked files, you must first save the document":"Для загрузки связанных файлов нужно сначала сохранить документ","Toggle password visibility":"Переключить видимость пароля","Translation suggested by":"Перевод предложенный","Translation suggested by Google Translate":"Перевод предложенный Google Translate","Transportation Icons":"Иконки транспорта","Type angular ui.route name":"Напишите название angular ui.route представления","Type link to your social media page":"Вставьте ссылку на социальный сайт сюда","Type your contact (email, phone, skype, fax)":"Ваши контактные данные(почта, телефон, skype, факс)","Unable to delete file":"Не получилось удалить файл","Unique identifier":"Уникальный идентификатор","Update":"Обновить","Upload":"Загрузить","Upload file":["Загрузить файл","Загрузить файлы","Загрузить файлы"],"Upload files":"Загрузить файлы","Upload image":"Загрузить изображение","Use login 'demo' and password '1":"Испольуйте логин 'demo' и пароль '1' для входа","User with this email or username already exists":"Пользователь с таким логином или почтой уже зарегистрирован","Username":"Логин пользователя","Username alredy taken":"Имя пользователя уже занято","Username or email":"Логин или почта","Value":"Значение","Video Player Icons":"Иконки видео-плеера","View file":"Просмотреть файл","View/Route":"Представление/Путь(angular)","Visible columns":"Видимые колонки","We can't find {{options.email}} in our database. Please, give us email that associated with your account on this site":"Мы не можем найти {{options.email}} в нашей базе данных. Введите почтовый адрес, который привязан к профилю на этом сайте","We've sent you email with instructions on resetting your password":"Мы отправили вам email-сообщение с инструкциями как изменить пароль","We've sent you email with instructions on resetting your password and changing the phone numbers":"Мы отправили вам email-сообщение с инструкциями как изменить пароли и номера телефонов","Web Application Icons":"Иконки веб-приложений","You can't undo this action":{"delete object":"Это действие нельзя будет откатить"},"Your email":"Ваш email","Your search - <b>\"{{searchTerm}}\"</b> - did not match any documents":"По запросу - <b>\"{{searchTerm}}\"</b> -  ничего не найдено","Your search did not match any documents":"По запросу ничего не найдено","day":{"medium duration text":["день","дня","дней"]},"enter the four-digit code here (the code is valid for only 3 minute), that has been sent to the phone numbers that are indicated in your profile as main ones":"введите четырехзначный код в это поле (код доступен для ввода 3 минуты), который был отправлен на ваши номера телефонов указанные в профиле как основные","hour":{"medium duration text":["час","часа","часов"]},"minute":{"medium duration text":["минута","минуты","минут"]},"required fields":"обязательные поля","second":{"medium duration text":["секунда","секунды","секунд"]},"to bottom":"вниз","to top":"наверх","view all":"отобразить все"});
/* jshint +W100 */
}]);