(function(root, factory) {
  'use strict';
  if (typeof define === 'function' && define.amd) {
    define(['angular'], factory);
  } else if (typeof exports === 'object') {
    module.exports = factory(require('angular'));
  } else {
    factory(root.angular);
  }
})(this, function(angular) {
//-----------------------------------------
'use strict';
var BaseFallbackStorage, GdprException, STORAGE_PREFIX, generateStorageFactory, getStorageKey, ngGdprStorage, ngLocalStorage, ngSessionStorage, rmNgGdprStorageWatch, rmNgLocalStorageWatch, rmNgSessionStorageWatch,
  extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty,
  indexOf = [].indexOf || function(item) { for (var i = 0, l = this.length; i < l; i++) { if (i in this && this[i] === item) return i; } return -1; };

STORAGE_PREFIX = 'ngStorage-';

BaseFallbackStorage = (function() {
  function BaseFallbackStorage() {
    this.length = void 0;
    if (Object.defineProperty) {
      Object.defineProperty(this, 'length', {
        configurable: false,
        get: function() {
          return this.__length();
        }
      });
    }
    return;
  }

  BaseFallbackStorage.prototype.getItem = function(name) {
    if (arguments.length < 1) {
      throw "Failed to execute 'getItem' on sessionStorage fallback: 1 argument required, but only " + arguments.length + " present";
    }
    return this.__getItem(name);
  };

  BaseFallbackStorage.prototype.setItem = function(name, value) {
    if (arguments.length < 2) {
      throw "Failed to execute 'setItem' on sessionStorage fallback: 2 arguments required, but only " + arguments.length + " present";
    }
    this.__setItem(name + '', value + '');
  };

  BaseFallbackStorage.prototype.removeItem = function(name) {
    throw "Failed to execute 'removeItem' on sessionStorage fallback: 1 argument required, but only " + arguments.length + " present";
    this.__removeItem(name + '');
  };

  BaseFallbackStorage.prototype.clear = function() {
    this.__clear();
  };

  BaseFallbackStorage.prototype.key = function(i) {
    if (arguments.length < 1) {
      throw "Failed to execute 'key' on sessionStorage fallback: 1 argument required, but only " + arguments.length + " present";
    }
    return this.__key(i);
  };

  return BaseFallbackStorage;

})();

GdprException = function(message) {
  this.name = 'GdprException';
  this.message = message || '';
  return this;
};

GdprException.prototype = new Error();

GdprException.prototype.constructor = GdprException;

getStorageKey = function(key) {
  return key.slice(STORAGE_PREFIX.length);
};

generateStorageFactory = function($rootScope, $window, $log, $timeout, storageType, preferredStorageType, allowedKeysForGdpr) {
  var $storage, _debounce, _last$storage, fromJson, getStorage, memoryStorage, onStorageChangeEvent, signalState, toJson;
  _debounce = null;
  _last$storage = null;
  allowedKeysForGdpr = allowedKeysForGdpr || {};
  toJson = angular.toJson;
  fromJson = function(data) {
    var error, error1;
    try {
      return angular.fromJson(data);
    } catch (error1) {
      error = error1;
      return void 0;
    }
  };
  memoryStorage = (function() {
    var BANNED_PROPS, MemoryStorage, storage;
    BANNED_PROPS = ['length'];
    MemoryStorage = (function(superClass) {
      extend(MemoryStorage, superClass);

      function MemoryStorage() {
        return MemoryStorage.__super__.constructor.apply(this, arguments);
      }

      MemoryStorage.prototype.__getItem = function(name) {
        return this[name + ''] || void 0;
      };

      MemoryStorage.prototype.__setItem = function(name, value) {
        if (angular.isFunction(this[name])) {
          throw "Failed to execute 'setItem' on sessionStorage fallback: " + name + " is not allowed";
        }
        if (this[name] === value) {
          return;
        }
        this[name] = value;
      };

      MemoryStorage.prototype.__removeItem = function(name) {
        delete this[name];
      };

      MemoryStorage.prototype.__clear = function() {
        var k;
        for (k in this) {
          if (!hasProp.call(this, k)) continue;
          if (indexOf.call(BANNED_PROPS, k) < 0 && !angular.isFunction(this[k])) {
            delete this[k];
          }
        }
      };

      MemoryStorage.prototype.__key = function(i) {
        var k, keys;
        keys = [];
        for (k in this) {
          if (!hasProp.call(this, k)) continue;
          if (indexOf.call(BANNED_PROPS, k) < 0 && !angular.isFunction(this[k])) {
            keys.push(k);
          }
        }
        keys.sort();
        return keys[i];
      };

      MemoryStorage.prototype.__length = function() {
        var k, length;
        length = 0;
        for (k in this) {
          if (!hasProp.call(this, k)) continue;
          if (indexOf.call(BANNED_PROPS, k) < 0 && !angular.isFunction(this[k])) {
            length += 1;
          }
        }
        return length;
      };

      return MemoryStorage;

    })(BaseFallbackStorage);
    storage = new MemoryStorage();
    if (!angular.isDefined($window.Proxy)) {
      return storage;
    }
    return new Proxy(storage, {
      set: function(obj, prop, value) {
        var event;
        if (obj[prop] === value) {
          return value;
        }
        event = new CustomEvent('storage', {});
        event.key = prop;
        event.oldValue = obj[prop];
        event.isMemoryStorage = true;
        obj.setItem(prop, value);
        event.newValue = obj[prop];
        $window.dispatchEvent(event);
        return value;
      },
      get: function(obj, prop) {
        return obj[prop];
      }
    });
  })();
  getStorage = (function() {
    var err, error1, error2, error3, key, localStorageSupported, sessionStorageSupported;
    try {
      sessionStorageSupported = $window['sessionStorage'];
    } catch (error1) {
      err = error1;
      sessionStorageSupported = false;
    }
    try {
      localStorageSupported = $window['localStorage'];
    } catch (error2) {
      err = error2;
      localStorageSupported = false;
    }
    if (localStorageSupported) {
      key = '__' + Math.round(Math.random() * 1e7);
      try {
        localStorageSupported.setItem(key, key);
        localStorageSupported.removeItem(key);
      } catch (error3) {
        err = error3;
        localStorageSupported = false;
      }
    }
    if (!localStorageSupported && !sessionStorageSupported) {
      $log.warn('This browser does not support Web Storage!');
    }
    return function(storageType) {
      var storage;
      if (storageType === 'gdprStorage') {
        storageType = preferredStorageType;
      }
      storage = null;
      if (storageType === 'localStorage' && localStorageSupported) {
        storage = localStorageSupported;
      } else if (storageType === 'sessionStorage' && sessionStorageSupported) {
        storage = sessionStorageSupported;
      }
      return storage || memoryStorage;
    };
  })();
  $storage = {
    $default: function(items) {
      var k, v;
      for (k in items) {
        v = items[k];
        if (!angular.isDefined($storage[k])) {
          $storage[k] = v;
        }
      }
      $storage.$sync();
      return $storage;
    },
    $reset: function(items) {
      var k, webStorage;
      webStorage = getStorage(storageType);
      for (k in $storage) {
        if (!(k[0] !== '$')) {
          continue;
        }
        delete $storage[k];
        webStorage.removeItem(STORAGE_PREFIX + k);
      }
      return $storage.$default(items);
    },
    $sync: function() {
      var $storageKey, k, value, webStorage;
      webStorage = getStorage(storageType);
      for (k in webStorage) {
        if (!(k && k.indexOf(STORAGE_PREFIX) === 0)) {
          continue;
        }
        $storageKey = getStorageKey(k);
        if (storageType === 'gdprStorage' && !allowedKeysForGdpr[$storageKey]) {
          continue;
        }
        value = fromJson(webStorage.getItem(k));
        if (storageType === 'gdprStorage' && $storageKey === 'gdprPermission') {
          $storage.$setPermission(value);
        } else {
          if (angular.isDefined(value)) {
            $storage[$storageKey] = value;
          } else {
            delete $storage[$storageKey];
          }
        }
      }
    },
    $apply: function(force) {
      var k, temp$storage, v, webStorage;
      _debounce = null;
      temp$storage = void 0;
      webStorage = getStorage(storageType);
      if (!angular.equals($storage, _last$storage) || force) {
        temp$storage = angular.copy(_last$storage);
        for (k in $storage) {
          v = $storage[k];
          if (!(angular.isDefined(v) && k[0] !== '$')) {
            continue;
          }
          if (storageType === 'gdprStorage' && !allowedKeysForGdpr[k]) {
            throw new GdprException("You can't assign key '" + k + "' for $gdprStorage! Please register key '" + k + "' inside config block with this: $gdprStorageProvider.registerKey('" + k + "')");
          }
          webStorage.setItem(STORAGE_PREFIX + k, toJson(v));
          delete temp$storage[k];
        }
        for (k in temp$storage) {
          webStorage.removeItem(STORAGE_PREFIX + k);
        }
        _last$storage = angular.copy($storage);
      }
    }
  };
  if (storageType === 'gdprStorage') {
    $storage.gdprPermission = {
      app: false
    };
    $storage.$getAllowedKeys = function() {
      return angular.copy(allowedKeysForGdpr);
    };
    signalState = 'none';
    $storage.$setPermission = function(permission) {
      var $storageKey, k, newStorage, oldStorage, sType, v, value;
      permission = permission || {};
      permission.app = !!permission.app;
      if (permission.app) {
        sType = 'localStorage';
      } else {
        sType = 'sessionStorage';
      }
      $storage.gdprPermission = permission;
      if (signalState === 'none') {
        $rootScope.$emit("storage.gdpr.init");
        signalState = 'init';
      } else {
        signalState = 'update';
        $rootScope.$emit("storage.gdpr.permissionUpdate");
      }
      oldStorage = getStorage(preferredStorageType);
      newStorage = getStorage(sType);
      newStorage.setItem(STORAGE_PREFIX + 'gdprPermission', toJson(permission));
      if (sType === preferredStorageType) {
        return;
      }
      if (oldStorage) {
        for (k in oldStorage) {
          v = oldStorage[k];
          if (!(k && k.indexOf(STORAGE_PREFIX) === 0)) {
            continue;
          }
          if (k !== STORAGE_PREFIX + 'gdprPermission') {
            newStorage.setItem(k, v);
          }
          oldStorage.removeItem(k);
        }
      }
      for (k in newStorage) {
        if (!(k && k.indexOf(STORAGE_PREFIX) === 0)) {
          continue;
        }
        $storageKey = getStorageKey(k);
        value = fromJson(newStorage.getItem(k));
        if (angular.isDefined(value)) {
          $storage[$storageKey] = value;
        }
      }
      preferredStorageType = sType;
    };
  }
  $storage.$sync();
  _last$storage = angular.copy($storage);
  onStorageChangeEvent = function(event) {
    var $storageKey, value;
    if (!event || !event.key) {
      return;
    }
    $storageKey = getStorageKey(event.key);
    if (event.key !== (STORAGE_PREFIX + $storageKey) || (!event.isMemoryStorage && event.storageArea !== getStorage(storageType))) {
      return;
    }
    value = fromJson(event.newValue);
    if (angular.isDefined(value)) {
      $storage[$storageKey] = value;
    } else {
      delete $storage[$storageKey];
    }
    _last$storage = angular.copy($storage);
    if (!$rootScope.$$phase) {
      $rootScope.$apply();
    }
  };
  $window.addEventListener('storage', onStorageChangeEvent);
  $window.addEventListener('beforeunload', function(event) {
    $window.removeEventListener('storage', onStorageChangeEvent);
    $storage.$apply();
  }, false);
  return $storage;
};

ngLocalStorage = null;

ngSessionStorage = null;

ngGdprStorage = null;

rmNgLocalStorageWatch = null;

rmNgSessionStorageWatch = null;

rmNgGdprStorageWatch = null;


/**
 * @ngdoc overview
 * @name gdpr.storage
 */

angular.module('storage.gdpr', []).provider('storageSettings', function() {
  var storageSettings, thirdPartyLiterals;
  storageSettings = null;
  thirdPartyLiterals = {};
  this.setPrefix = function(prefix) {
    STORAGE_PREFIX = prefix || '';
  };
  this.registerThirdPartyServiceLiteral = function(literalName, config) {
    thirdPartyLiterals[literalName] = config || [];
  };
  this.$get = function() {
    if (!storageSettings) {
      storageSettings = {
        defaultAllowAll: false,
        isBannerVisible: false,
        thirdPartyServices: [],
        getPrefix: function() {
          return STORAGE_PREFIX;
        },
        setBannerVisibility: function(visibility) {
          storageSettings.isBannerVisible = visibility === true || visibility === 'visible';
        },
        getThirdPartyLiterals: function() {
          return angular.copy(thirdPartyLiterals);
        }
      };
    }
    return storageSettings;
  };
  return this;
}).service('$localStorage', ["$rootScope", "$window", "$log", "$timeout", function($rootScope, $window, $log, $timeout) {
  var _debounce;
  _debounce = null;
  if (!ngLocalStorage) {
    ngLocalStorage = generateStorageFactory($rootScope, $window, $log, $timeout, 'localStorage');
  } else {
    ngLocalStorage.$sync();
  }
  if (rmNgLocalStorageWatch) {
    rmNgLocalStorageWatch();
  }
  rmNgLocalStorageWatch = $rootScope.$watch(function() {
    if (!_debounce) {
      _debounce = $timeout(function() {
        return ngLocalStorage.$apply();
      }, 100, false);
    }
    return _debounce;
  });
  return ngLocalStorage;
}]).service('$sessionStorage', ["$rootScope", "$window", "$log", "$timeout", function($rootScope, $window, $log, $timeout) {
  var _debounce;
  _debounce = null;
  if (!ngSessionStorage) {
    ngSessionStorage = generateStorageFactory($rootScope, $window, $log, $timeout, 'sessionStorage');
  } else {
    ngSessionStorage.$sync();
  }
  if (rmNgSessionStorageWatch) {
    rmNgSessionStorageWatch();
  }
  rmNgSessionStorageWatch = $rootScope.$watch(function() {
    if (!_debounce) {
      _debounce = $timeout(function() {
        return ngSessionStorage.$apply();
      }, 100, false);
    }
    return _debounce;
  });
  return ngSessionStorage;
}]).provider('$gdprStorage', function() {
  var ALLOWED_KEYS;
  ALLOWED_KEYS = {};
  this.registerKey = function(key, options) {
    options = options || {};
    ALLOWED_KEYS[key] = options;
  };
  this.$get = ["$rootScope", "$window", "$log", "$timeout", function($rootScope, $window, $log, $timeout) {
    var _debounce, k, permission, preferredStorageType, value;
    _debounce = null;
    if (!ngGdprStorage) {
      if (!ngSessionStorage) {
        ngSessionStorage = generateStorageFactory($rootScope, $window, $log, $timeout, 'sessionStorage');
      } else {
        ngSessionStorage.$sync();
      }
      if (!ngLocalStorage) {
        ngLocalStorage = generateStorageFactory($rootScope, $window, $log, $timeout, 'localStorage');
      } else {
        ngLocalStorage.$sync();
      }
      permission = {
        app: false
      };
      if (angular.isObject(ngLocalStorage.gdprPermission)) {
        permission = ngLocalStorage.gdprPermission;
      } else if (angular.isObject(ngSessionStorage.gdprPermission)) {
        permission = ngSessionStorage.gdprPermission;
      }
      for (k in ALLOWED_KEYS) {
        value = '';
        if (ngSessionStorage[k]) {
          value = ngSessionStorage[k];
        }
        if (ngLocalStorage && ngLocalStorage[k]) {
          value = ngLocalStorage[k];
        }
        if (value === '') {
          continue;
        }
        if (permission.app) {
          ngLocalStorage[k] = value;
        } else {
          ngSessionStorage[k] = value;
        }
      }
      if (permission.app) {
        ngLocalStorage.$apply();
        preferredStorageType = 'localStorage';
      } else {
        ngSessionStorage.$apply();
        preferredStorageType = 'sessionStorage';
      }
      ngGdprStorage = generateStorageFactory($rootScope, $window, $log, $timeout, 'gdprStorage', preferredStorageType, ALLOWED_KEYS);
      ngGdprStorage.$setPermission(permission);
    } else {
      ngGdprStorage.$sync();
    }
    if (rmNgGdprStorageWatch) {
      rmNgGdprStorageWatch();
    }
    rmNgGdprStorageWatch = $rootScope.$watch(function() {
      if (!_debounce) {
        _debounce = $timeout(function() {
          return ngGdprStorage.$apply();
        }, 100, false);
      }
      return _debounce;
    });
    return ngGdprStorage;
  }];
  return this;
}).config(["$gdprStorageProvider", function($gdprStorageProvider) {
  return $gdprStorageProvider.registerKey('gdprPermission', {
    description: 'GDPR permission data for site'
  });
}]);

angular.module('storage.gdpr').directive('gdprRequestPermissionBanner', function() {
  return {
    restrict: 'E',
    scope: {
      defaultAllowAll: '@?'
    },
    controller: ["$scope", "$element", "$gdprStorage", "storageSettings", function($scope, $element, $gdprStorage, storageSettings) {
      $scope.storageSettings = storageSettings;
      if (!$gdprStorage.gdprPermission.app) {
        storageSettings.setBannerVisibility(true);
      }
      storageSettings.defaultAllowAll = $scope.defaultAllowAll === 'true';
      return $scope.$watch('storageSettings.isBannerVisible', function(isVisible) {
        if (isVisible) {
          $element.addClass('visible');
        } else {
          $element.removeClass('visible');
        }
      });
    }],
    template: ('/src/gdprRequestPermissionBanner.html', '\n<div class="container" ng-if="storageSettings.isBannerVisible">\n  <gdpr-request-permission-area></gdpr-request-permission-area>\n</div>' + '')
  };
}).directive('gdprRequestPermissionArea', function() {
  return {
    restrict: 'E',
    scope: {},
    controller: ["$scope", "$rootScope", "$timeout", "$gdprStorage", "storageSettings", function($scope, $rootScope, $timeout, $gdprStorage, storageSettings) {
      var appCookies, key, options, ref;
      $scope.storageSettings = storageSettings;
      $scope.options = {
        permission: angular.copy($gdprStorage.gdprPermission),
        isDetailsVisible: false,
        activeTab: 0
      };
      appCookies = [];
      ref = $gdprStorage.$getAllowedKeys();
      for (key in ref) {
        options = ref[key];
        appCookies.push({
          key: key,
          provider: options.provider || 'This site',
          purpose: options.purpose || 'Settings',
          expiry: options.expiry || 'Never',
          type: options.type || 'Value in localStorage'
        });
      }
      $scope.$watchCollection('storageSettings.thirdPartyServices', function(thirdPartyServices, oldValue) {
        var i, j, len, len1, literals, ref1, ref2, s;
        $scope.SERVICES = [
          {
            type: 'app',
            name: 'Site settings',
            cookies: appCookies
          }
        ];
        literals = storageSettings.getThirdPartyLiterals();
        ref1 = thirdPartyServices || [];
        for (i = 0, len = ref1.length; i < len; i++) {
          s = ref1[i];
          if (angular.isString(s) && literals[s]) {
            $scope.SERVICES.push(literals[s]);
          } else if (angular.isObject(s)) {
            $scope.SERVICES.push(s);
          }
        }
        if (storageSettings.defaultAllowAll) {
          ref2 = $scope.SERVICES;
          for (j = 0, len1 = ref2.length; j < len1; j++) {
            s = ref2[j];
            $scope.options.permission[s.type] = true;
          }
        }
      });
      $rootScope.$on('storage.gdpr.update', function() {
        $scope.options.permission = angular.copy($gdprStorage.gdprPermission);
      });
      $scope.onCancel = function() {
        var k;
        for (k in $scope.options.permission) {
          $scope.options.permission[k] = false;
        }
        $gdprStorage.$setPermission($scope.options.permission);
        storageSettings.setBannerVisibility(false);
      };
      $scope.onAccept = function() {
        $gdprStorage.$setPermission($scope.options.permission);
        storageSettings.setBannerVisibility(false);
        $rootScope.$emit('storage.gdpr.userPermissionAccept');
      };
      $scope.onAcceptAll = function() {
        var i, len, ref1, service;
        ref1 = $scope.SERVICES || [];
        for (i = 0, len = ref1.length; i < len; i++) {
          service = ref1[i];
          $scope.options.permission[service.type] = true;
        }
        $timeout($scope.onAccept, 900);
      };
      $scope.toggleDetails = function() {
        return $scope.options.isDetailsVisible = !$scope.options.isDetailsVisible;
      };
    }],
    template: ('/src/gdprRequestPermissionArea.html', '\n<table class="gdpr-request-permission-area-content">\n  <tbody>\n    <tr>\n      <td>\n        <h5><b translate="">This website uses cookies and local storage</b></h5>\n        <p class="gdpr-text"><span translate="">We use cookies to personalise content and ads, to provide social media features and to analyse our traffic. You should consent to our cookies if you continue to use our website.</span><span> </span><a ng-click="toggleDetails()" ng-switch="options.isDetailsVisible"><span translate="" ng-switch-when="true">Hide details</span><span translate="" ng-switch-when="false">Show details about cookies and local storage</span></a></p>\n        <div class="gdpr-request-permission-area-content-checkboxes">\n          <div class="checkbox-inline" ng-repeat="service in SERVICES">\n            <label>\n              <input type="checkbox" ng-model="options.permission[service.type]"/><span> </span><span>{{service.name | translate}}</span>\n            </label>\n          </div>\n        </div>\n        <div class="gdpr-request-permission-area-content-details" ng-if="options.isDetailsVisible">\n          <uib-tabset class="uib-tabset-sm" active="options.activeTab">\n            <uib-tab ng-repeat="service in SERVICES" index="$index" heading="{{service.name | translate}}">\n              <p class="gdpr-text" ng-if="service.description" uic-bind-html="service.description | translate" ng-class="{nomargin: !service.cookies.length}"></p>\n              <table class="table table-striped table-bordered table-condensed" ng-if="service.cookies.length">\n                <thead>\n                  <tr>\n                    <th class="gdpr-text" translate="">Name</th>\n                    <th class="gdpr-text" translate="">Provider</th>\n                    <th class="gdpr-text" translate="">Purpose</th>\n                    <th class="gdpr-text" translate="">Expiry</th>\n                    <th class="gdpr-text" translate="">Type</th>\n                  </tr>\n                </thead>\n                <tbody>\n                  <tr ng-repeat="cookie in service.cookies">\n                    <td class="gdpr-text">{{cookie.key}}</td>\n                    <td class="gdpr-text">{{cookie.provider | translate}}</td>\n                    <td class="gdpr-text">{{cookie.purpose | translate}}</td>\n                    <td class="gdpr-text">{{cookie.expiry | translate}}</td>\n                    <td class="gdpr-text">{{cookie.type | translate}}</td>\n                  </tr>\n                </tbody>\n              </table>\n            </uib-tab>\n          </uib-tabset>\n        </div>\n      </td>\n      <td class="gdpr-request-permission-area-content-btns hidden-xs hidden-sm" ng-switch="!!storageSettings.defaultAllowAll">\n        <table ng-switch-when="false">\n          <body>\n            <tr>\n              <td>\n                <button class="btn btn-default btn-xs btn-block" ng-click="onAccept()"><span translate="" translate-context="Accept cookies">Accept</span></button>\n              </td>\n              <td class="btn-separator"></td>\n              <td>\n                <button class="btn btn-default btn-xs btn-block" ng-click="onCancel()"><span class="fa fa-times"></span><span translate="" translate-context="Deny cookies">Deny</span></button>\n              </td>\n            </tr>\n            <tr>\n              <td colspan="3">\n                <button class="btn btn-success btn-xs btn-block" ng-click="onAcceptAll()"><span class="fa fa-check"></span><span translate="" translate-context="Accept all cookies">Accept all</span>\n                  <body ng-switch-when="false"></body>\n                </button>\n              </td>\n            </tr>\n          </body>\n        </table>\n        <table ng-switch-when="true">\n          <body>\n            <tr>\n              <td>\n                <button class="btn btn-success btn-xs btn-block" ng-click="onAccept()"><span class="fa fa-check"></span><span translate="" translate-context="Accept cookies">Accept</span></button>\n              </td>\n            </tr>\n            <tr>\n              <td>\n                <button class="btn btn-default btn-xs btn-block" ng-click="onCancel()"><span class="fa fa-times"></span><span translate="" translate-context="Deny cookies">Deny</span></button>\n              </td>\n            </tr>\n          </body>\n        </table>\n      </td>\n    </tr>\n    <tr class="gdpr-request-permission-area-content-btns visible-xs visible-sm">\n      <td class="text-center" colspan="2">\n        <div class="btn-group">\n          <button class="btn btn-default btn-xs" ng-click="onAccept()" ng-class="{\'btn-success\': storageSettings.defaultAllowAll}"><span class="fa fa-check" ng-show="storageSettings.defaultAllowAll"></span><span translate="" translate-context="Accept cookies">Accept</span></button>\n          <button class="btn btn-success btn-xs" ng-click="onAcceptAll()" ng-hide="storageSettings.defaultAllowAll"><span class="fa fa-check"></span><span translate="" translate-context="Accept all cookies">Accept all</span></button>\n          <button class="btn btn-default btn-xs" ng-click="onCancel()"><span class="fa fa-times"></span><span translate="" translate-context="Deny cookies">Deny</span></button>\n        </div>\n      </td>\n    </tr>\n  </tbody>\n</table>' + '')
  };
});

angular.module('storage.gdpr').config(["storageSettingsProvider", function(storageSettingsProvider) {
  var GA, jivosite, yandexMetrica;
  GA = [
    {
      key: '_ga',
      provider: 'google.com',
      purpose: 'Used to distinguish users',
      expiry: '2 years',
      type: 'HTTP cookie'
    }, {
      key: '_gid',
      provider: 'google.com',
      purpose: '24 hours',
      expiry: 'Used to distinguish users',
      type: 'HTTP cookie'
    }, {
      key: '_gat',
      provider: 'google.com',
      purpose: 'Used to throttle request rate. If Google Analytics is deployed via Google Tag Manager, this cookie will be named _dc_gtm_<property-id>',
      expiry: '1 minute',
      type: 'HTTP cookie'
    }, {
      key: 'AMP_TOKEN',
      provider: 'google.com',
      purpose: 'Contains a token that can be used to retrieve a Client ID from AMP Client ID service. Other possible values indicate opt-out, inflight request or an error retrieving a Client ID from AMP Client ID service',
      expiry: '30 seconds to 1 year',
      type: 'HTTP cookie'
    }, {
      key: '_gac_<property-id>',
      provider: 'google.com',
      purpose: 'Contains campaign related information for the user',
      expiry: '90 days',
      type: 'HTTP cookie'
    }, {
      key: '__utma',
      provider: 'google.com',
      purpose: 'Used to distinguish users and sessions. The cookie is created when the javascript library executes and no existing __utma cookies exists. The cookie is updated every time data is sent to Google Analytics',
      expiry: '2 years from set/update',
      type: 'HTTP cookie'
    }, {
      key: '__utmt',
      provider: 'google.com',
      purpose: 'Used to throttle request rate',
      expiry: '10 minutes',
      type: 'HTTP cookie'
    }, {
      key: '__utmb',
      provider: 'google.com',
      purpose: 'Used to determine new sessions/visits. The cookie is created when the javascript library executes and no existing __utmb cookies exists. The cookie is updated every time data is sent to Google Analytics',
      expiry: '30 mins from set/update',
      type: 'HTTP cookie'
    }, {
      key: '__utmz',
      provider: 'google.com',
      purpose: 'Stores the traffic source or campaign that explains how the user reached your site. The cookie is created when the javascript library executes and is updated every time data is sent to Google Analytics',
      expiry: '6 months from set/update',
      type: 'HTTP cookie'
    }, {
      key: '__utmv',
      provider: 'google.com',
      purpose: 'Used to store visitor-level custom variable data',
      expiry: '2 years from set/update',
      type: 'HTTP cookie'
    }, {
      key: '__utmx',
      provider: 'google.com',
      purpose: "Used to determine a user's inclusion in an experiment",
      expiry: '18 months',
      type: 'HTTP cookie'
    }, {
      key: '__utmxx',
      provider: 'google.com',
      purpose: 'Used to determine the expiry of experiments a user has been included in',
      expiry: '18 months',
      type: 'HTTP cookie'
    }, {
      key: '_gaexp',
      provider: 'google.com',
      purpose: "Used to determine a user's inclusion in an experiment and the expiry of experiments a user has been included in",
      expiry: 'Depends on the length of the experiment but typically 90 days',
      type: 'HTTP cookie'
    }
  ];
  jivosite = [
    {
      key: 'jv_enter_ts_${jivo_visitor_id}',
      provider: 'jivochat.com',
      purpose: 'First visit to the website',
      expiry: '12 hours from set/update',
      type: 'HTTP cookie'
    }, {
      key: 'jv_visits_count_${jivo_visitor_id}',
      provider: 'jivochat.com',
      purpose: 'Number of visits to the website',
      expiry: '12 hours from set/update',
      type: 'HTTP cookie'
    }, {
      key: 'jv_pages_count_${jivo_visitor_id}',
      provider: 'jivochat.com',
      purpose: 'Number of pages viewed',
      expiry: '12 hours from set/update',
      type: 'HTTP cookie'
    }, {
      key: 'jv_refer_${jivo_visitor_id}',
      provider: 'jivochat.com',
      purpose: 'Source of entry to the website',
      expiry: '12 hours from set/update',
      type: 'HTTP cookie'
    }, {
      key: 'jv_utm_${jivo_visitor_id}',
      provider: 'jivochat.com',
      purpose: 'ID of the marketing campaign of the visit',
      expiry: '12 hours from set/update',
      type: 'HTTP cookie'
    }, {
      key: 'jv_invitation_time_${jivo_visitor_id}',
      provider: 'jivochat.com',
      purpose: 'Time of appearance of proactive invitation',
      expiry: '12 hours from set/update',
      type: 'HTTP cookie'
    }, {
      key: 'jv_country_${jivo_visitor_id}',
      provider: 'jivochat.com',
      purpose: 'Country of the visitor',
      expiry: '12 hours from set/update',
      type: 'HTTP cookie'
    }, {
      key: 'jv_city_${jivo_visitor_id}',
      provider: 'jivochat.com',
      purpose: 'City of the visitor',
      expiry: '12 hours from set/update',
      type: 'HTTP cookie'
    }, {
      key: 'jv_close_time_${jivo_visitor_id}',
      provider: 'jivochat.com',
      purpose: 'Time the widget was closed',
      expiry: '12 hours from set/update',
      type: 'HTTP cookie'
    }, {
      key: 'jv_cw_timer_${jivo_visitor_id}',
      provider: 'jivochat.com',
      purpose: 'Time the callback request was initiated',
      expiry: '12 hours from set/update',
      type: 'HTTP cookie'
    }, {
      key: 'jv_callback_ping_response_${jivo_visitor_id}',
      provider: 'jivochat.com',
      purpose: 'Cash of callback backend script response',
      expiry: '12 hours from set/update',
      type: 'HTTP cookie'
    }, {
      key: 'jv_use_lp_${jivo_visitor_id}',
      provider: 'jivochat.com',
      purpose: 'Flag that enables long_pooling',
      expiry: '12 hours from set/update',
      type: 'HTTP cookie'
    }, {
      key: 'jv_mframe_protected_${jivo_visitor_id}',
      provider: 'jivochat.com',
      purpose: 'Indication if iframe is forbidden',
      expiry: '12 hours from set/update',
      type: 'HTTP cookie'
    }, {
      key: 'jv_ab_test_group_${jivo_visitor_id}',
      provider: 'jivochat.com',
      purpose: 'a/b testing group flag',
      expiry: '12 hours from set/update',
      type: 'HTTP cookie'
    }
  ];
  yandexMetrica = [
    {
      key: '_ym_isad',
      provider: 'yandex.com',
      purpose: 'Used to determine if a visitor has ad blockers',
      expiry: '2 days',
      type: 'HTTP cookie'
    }, {
      key: '_ym_uid',
      provider: 'yandex.com',
      purpose: 'Used for identifying site users',
      expiry: '1 year',
      type: 'HTTP cookie'
    }, {
      key: '_ym_d',
      provider: 'yandex.com',
      purpose: "Date of the user's first site session",
      expiry: '1 year',
      type: 'HTTP cookie'
    }, {
      key: 'yabs-sid',
      provider: 'yandex.com',
      purpose: 'Session ID',
      expiry: 'Until the session ends',
      type: 'HTTP cookie'
    }, {
      key: '_ym_debug',
      provider: 'yandex.com',
      purpose: 'Indicates that debug mode is active',
      expiry: 'Until the session ends',
      type: 'HTTP cookie'
    }, {
      key: '_ym_mp2_substs',
      provider: 'yandex.com',
      purpose: 'Used for Target Call',
      expiry: 'Until the session ends',
      type: 'HTTP cookie'
    }, {
      key: 'i',
      provider: 'yandex.com',
      purpose: 'Used for identifying site users',
      expiry: '1 year',
      type: 'HTTP cookie'
    }, {
      key: 'yandexuid',
      provider: 'yandex.com',
      purpose: 'Used for identifying site users',
      expiry: '1 year',
      type: 'HTTP cookie'
    }, {
      key: 'usst',
      provider: 'yandex.com',
      purpose: 'Stores auxiliary information for syncing site user IDs between different Yandex domains',
      expiry: '1 year',
      type: 'HTTP cookie'
    }, {
      key: '_ym_visorc_*',
      provider: 'yandex.com',
      purpose: 'Allows Session Replay to function correctly',
      expiry: '30 minutes',
      type: 'HTTP cookie'
    }, {
      key: '_ym_hostIndex',
      provider: 'yandex.com',
      purpose: 'Limits the number of requests',
      expiry: '1 day',
      type: 'HTTP cookie'
    }, {
      key: '_ym_mp2_track',
      provider: 'yandex.com',
      purpose: 'Used for Target Call',
      expiry: '30 days',
      type: 'HTTP cookie'
    }, {
      key: 'zz',
      provider: 'yandex.com',
      purpose: 'Used for identifying site users',
      expiry: '90 days',
      type: 'HTTP cookie'
    }
  ];
  storageSettingsProvider.registerThirdPartyServiceLiteral('googleAnalytics', {
    type: 'googleAnalytics',
    name: 'Google Analytics',
    cookies: GA
  });
  storageSettingsProvider.registerThirdPartyServiceLiteral('googleTagManager', {
    type: 'googleTagManager',
    name: 'Google Tag Manager',
    cookies: GA
  });
  storageSettingsProvider.registerThirdPartyServiceLiteral('jivosite', {
    type: 'jivosite',
    name: 'Jivochat',
    cookies: jivosite
  });
  storageSettingsProvider.registerThirdPartyServiceLiteral('yandexMetrica', {
    type: 'yandexMetrica',
    name: 'Yandex Metrica',
    cookies: yandexMetrica
  });
  storageSettingsProvider.registerThirdPartyServiceLiteral('facebookPixel', {
    type: 'facebookPixel',
    name: 'Facebook Pixel',
    description: "Unfortunately, facebook.com does not provide comprehensive information about the created and tracked cookies and keys in localStorage. For general information, you can follow this <a href='https://www.facebook.com/policies/cookies/' target='_blank'>link</a>."
  });
  storageSettingsProvider.registerThirdPartyServiceLiteral('chatflow', {
    type: 'chatflow',
    name: 'chatflow.io',
    description: "Unfortunately, chatflow.io does not provide comprehensive information about the created and tracked cookies and keys in localStorage. For general information, you can follow this <a href='https://chatflow.io/' target='_blank'>link</a>."
  });
  storageSettingsProvider.registerThirdPartyServiceLiteral('disqus', {
    type: 'disqus',
    name: 'Disqus',
    description: "Unfortunately, disqus.com does not provide comprehensive information about the created and tracked cookies and keys in localStorage. For general information, you can follow this <a href='https://help.disqus.com/en/collections/191787-terms-and-policies' target='_blank'>link</a>."
  });
  storageSettingsProvider.registerThirdPartyServiceLiteral('googleAdSense', {
    type: 'googleAdSense',
    name: 'Adsense',
    description: "Unfortunately, Google AdSense does not provide comprehensive information about the created and tracked cookies and keys in localStorage. For general information, you can follow this <a href=\"https://policies.google.com/technologies/cookies?hl=en-US#types-of-cookies\" target=\"_blank\">link</a>."
  });
}]);

angular.module('storage.gdpr').run(['gettextCatalog', function (gettextCatalog) {
/* jshint -W100 */
    gettextCatalog.setStrings('cz', {"Accept":{"Accept cookies":"Akceptovat"},"Accept all":{"Accept all cookies":"Přijmout vše"},"Deny":{"Deny cookies":"Zrušit cookies"},"Expiry":"Uplynutí","GDPR permission data for site":"Údaje o oprávnění GDPR pro web","Hide details":"Skrýt detaily","Local storage":"Místní (local) úložiště","Name":"Jméno","Never":"Nikdy","Provider":"Poskytovatel","Purpose":"Účel","Settings":"Nastavení","Show details about cookies and local storage":"Zobrazit podrobnosti o souborech cookie a místním úložišti","Site settings":"Nastavení webu","This site":"Tento web","This website uses cookies and local storage":"Tento web používá soubory cookie a místní úložiště","Type":"Typ","Value in localStorage":"Hodnota v localStorage","We use cookies to personalise content and ads, to provide social media features and to analyse our traffic. You should consent to our cookies if you continue to use our website":"Cookies používáme k přizpůsobení obsahu a reklam, k poskytování funkcí sociálních médií a k analýze provozu. Pokud budete pokračovat v používání našich webových stránek, měli byste souhlasit s našimi soubory cookie"});
    gettextCatalog.setStrings('ru', {"Accept":{"Accept cookies":"Принять"},"Accept all":{"Accept all cookies":"Принять все"},"Deny":{"Deny cookies":"Отказаться"},"Expiry":"Истечение срока","GDPR permission data for site":"GDPR разрешение для сайта","Hide details":"Скрыть детали","Local storage":"Местное хранилище (localStorage)","Name":"Название","Never":"Никогда","Provider":"Провайдер","Purpose":"Назначение","Settings":"Настройки","Show details about cookies and local storage":"Показать детали о cookies и localStorage","Site settings":"Настройки сайта","This site":"Этот сайт","This website uses cookies and local storage":"Этот веб-сайт использует cookies и localStorage","Type":"Тип","Value in localStorage":"Значение в localStorage","We use cookies to personalise content and ads, to provide social media features and to analyse our traffic. You should consent to our cookies if you continue to use our website":"Мы используем файлы cookie для персонализации контента и рекламы, предоставления функций социальных сетей и анализа трафика. Вы должны согласиться на использование cookie, если вы желаете использовать наш веб-сайт"});
    gettextCatalog.setStrings('cz', {"1 minute":"1 minuta","10 minutes":"10 minut","12 hours from set/update":"12 hodin od nastavení / aktualizace","18 months":"8 měsíců","2 years":"2 roky","2 years from set/update":"2 roky od nastavení / aktualizace","24 hours":"24 hodiny","30 mins from set/update":"30 minut from set/update","30 seconds to 1 year":"30 sekund až 1 rok","6 months from set/update":"6 měsíců od nastavení / aktualizace","90 days":"90 dní","Cash of callback backend script response":"Cash callback odpovědi backend skriptu","City of the visitor":"Město návštěvníka","Contains a token that can be used to retrieve a Client ID from AMP Client ID service. Other possible values indicate opt-out, inflight request or an error retrieving a Client ID from AMP Client ID service":"Obsahuje token, který lze použít k načtení ID klienta z služby AMP Client ID. Jiné možné hodnoty označují opt-out, inflight request nebo chybu načítání ID klienta z služby AMP Client ID","Contains campaign related information for the user":"Obsahuje informace o kampani pro uživatele","Country of the visitor":"Země návštěvníka","Depends on the length of the experiment but typically 90 days":"Závisí na délce experimentu, ale obvykle 90 dnů","First visit to the website":"První návštěva webové stránky","Flag that enables long_pooling":"Flag, který umožňuje long_pooling","ID of the marketing campaign of the visit":"ID marketingové kampaně","Indication if iframe is forbidden":"Údaj, zda iframe je zakázán","Number of pages viewed":"Počet navštívených stránek","Number of visits to the website":"Počet návštěv na webu","Source of entry to the website":"Zdroj vstupu na web","Stores the traffic source or campaign that explains how the user reached your site. The cookie is created when the javascript library executes and is updated every time data is sent to Google Analytics":"Uchovává zdroj nebo kampaň, která vysvětluje, jak se uživatel dostal na váš web. Soubor cookie je vytvořen při spuštění knihovny javascript a je aktualizován při každém odeslání dat do služby Google Analytics","Time of appearance of proactive invitation":"Čas vzniku proaktivní pozvánky","Time the callback request was initiated":"Čas, kdy byl spuštěn callback request","Time the widget was closed":"Čas, kdy byl widget uzavřen","Used to determine a user's inclusion in an experiment":"Používá se k určení zařazení uživatele do experimentu","Used to determine a user's inclusion in an experiment and the expiry of experiments a user has been included in":"Používá se k určení zařazení uživatele do experimentu a ukončení experimentů, do kterých byl uživatel zařazen","Used to determine new sessions/visits. The cookie is created when the javascript library executes and no existing __utmb cookies exists. The cookie is updated every time data is sent to Google Analytics":"Používá se k určení nových sessions / návštěv. Soubor cookie je vytvořen při spuštění knihovny javascript a neexistují žádné existující cookies __utmb. Soubor cookie je aktualizován při každém odeslání dat do služby Google Analytics","Used to determine the expiry of experiments a user has been included in":"Používá se k určení ukončení experimentů, do kterých byl uživatel zařazen","Used to distinguish users":"Slouží k rozlišení uživatelů","Used to distinguish users and sessions. The cookie is created when the javascript library executes and no existing __utma cookies exists. The cookie is updated every time data is sent to Google Analytics":"Slouží k rozlišení uživatelů a relací. Soubor cookie je vytvořen při spuštění knihovny javascript a neexistují žádné existující cookies __utma. Soubor cookie je aktualizován při každém odeslání dat do služby Google Analytics","Used to store visitor-level custom variable data":"Slouží k ukládání vlastních proměnných na úrovni návštěvníka","Used to throttle request rate":"Omezuje frekvenci požadavků","Used to throttle request rate. If Google Analytics is deployed via Google Tag Manager, this cookie will be named _dc_gtm_%property-id%":"Omezuje frekvenci požadavků, pokud je služba Google Analytics nasazena prostřednictvím Google Tag Manager, tento soubor cookie bude pojmenován _dc_gtm_%property-id%","a/b testing group flag":"flag a/b testovací skupiny"});
    gettextCatalog.setStrings('ru', {"1 day":"1 день","1 minute":"1 минута","1 year":"1 год","10 minutes":"10 минут","12 hours from set/update":"12 часов с момента установки или обновления","18 months":"18 месяцев","2 days":"2 дня","2 years":"2 года","2 years from set/update":"2 года с момента установки или обновления","24 hours":"24 часа","30 days":"30 дней","30 mins from set/update":"30 минут с момента установки или обновления","30 minutes":"30 минут","30 seconds to 1 year":"от 30 сек до 1 года","6 months from set/update":"6 месяцев с момента установки или обновления","90 days":"90 дней","Cash of callback backend script response":"Кеш ответа callback-скрипта","City of the visitor":"Город посетителя","Contains a token that can be used to retrieve a Client ID from AMP Client ID service. Other possible values indicate opt-out, inflight request or an error retrieving a Client ID from AMP Client ID service":"Содержит токен, с помощью которого можно получить Client ID от сервиса AMP. Другие возможные значения: отключение функции, активный запрос или ошибка получения Client ID от сервиса AMP","Contains campaign related information for the user":"Содержит информацию о кампании для пользователя","Country of the visitor":"Страна посетителя","Depends on the length of the experiment but typically 90 days":"Зависит от длительности эксперимента (обычно составляет 90 дней)","First visit to the website":"Первый визит на сайт","Flag that enables long_pooling":"Флажок, который включает long_pooling","ID of the marketing campaign of the visit":"ID маркетинговой кампании посещения","Indication if iframe is forbidden":"Индикатор запрета iframe","Number of pages viewed":"Количество просмотренных страниц","Number of visits to the website":"Количество посещений сайта","Source of entry to the website":"Источник входа на сайт","Stores the traffic source or campaign that explains how the user reached your site. The cookie is created when the javascript library executes and is updated every time data is sent to Google Analytics":"Сохраняет информацию об источнике трафика или кампании, позволяющую понять, откуда пользователь пришел на ваш сайт. Создается при выполнении библиотеки и обновляется при каждой отправке данных в Google Analytics","Time of appearance of proactive invitation":"Время появления активного приглашения","Time the callback request was initiated":"Время инициирования callback запроса","Time the widget was closed":"Время закрытия виджета","Unfortunately, Google AdSense does not provide comprehensive information about the created and tracked cookies and keys in localStorage. For general information, you can follow this <a href=\"https://policies.google.com/technologies/cookies?hl=en-US#types-of-cookies\" target=\"_blank\">link</a>":"К сожалению, Google AdSense не предоставляет исчерпывающей информации про создаваемые и отслеживаемые cookies и ключи в localStorage. Для получения общей информации вы можете пройти по этой <a href=\"https://policies.google.com/technologies/cookies?hl=en-US#types-of-cookies\" target=\"_blank\">ссылке</a>","Unfortunately, chatflow.io does not provide comprehensive information about the created and tracked cookies and keys in localStorage. For general information, you can follow this <a href=\"https://chatflow.io/\" target=\"_blank\">link</a>":{"chatflow":"К сожалению, chatflow.io не предоставляет исчерпывающей информации про создаваемые и отслеживаемые cookies и ключи в localStorage. Для получения общей информации вы можете пройти по этой <a href=\"ttps://chatflow.io/\" target=\"_blank\">ссылке</a>"},"Unfortunately, facebook.com does not provide comprehensive information about the created and tracked cookies and keys in localStorage. For general information, you can follow this <a href=\"https://www.facebook.com/policies/cookies/\" target=\"_blank\">link</a>":{"facebook pixel":"К сожалению, facebook.com не предоставляет исчерпывающей информации про создаваемые и отслеживаемые cookies и ключи в localStorage. Для получения общей информации вы можете пройти по этой <a href=\"https://www.facebook.com/policies/cookies/\" target=\"_blank\">ссылке</a>"},"Used to determine a user's inclusion in an experiment":"Определяет, принимал ли пользователь участие в эксперименте","Used to determine a user's inclusion in an experiment and the expiry of experiments a user has been included in":"Определяет, когда истекает срок эксперимента и принимал ли пользователь в нем участие","Used to determine new sessions/visits. The cookie is created when the javascript library executes and no existing __utmb cookies exists. The cookie is updated every time data is sent to Google Analytics":"Используется для определения новых сеансов или посещений. Создается при выполнении библиотеки JavaScript, если нет существующих файлов cookie __utmb. Обновляется при каждой отправке данных в Google Analytics","Used to determine the expiry of experiments a user has been included in":"Определяет, когда истекает срок действия эксперимента, в котором участвовал пользователь","Used to distinguish users":"Позволяет различать пользователей","Used to distinguish users and sessions. The cookie is created when the javascript library executes and no existing __utma cookies exists. The cookie is updated every time data is sent to Google Analytics":"Позволяет различать пользователей и сеансы. Создается при выполнении библиотеки JavaScript, если нет существующих файлов cookie __utma. Обновляется при каждой отправке данных в Google Analytics","Used to store visitor-level custom variable data":"Сохраняет данные о пользовательской переменной уровня посетителя","Used to throttle request rate":"Ограничивает частоту запросов","Used to throttle request rate. If Google Analytics is deployed via Google Tag Manager, this cookie will be named _dc_gtm_%property-id%":"Ограничивает частоту запросов. Если поддержка Google Analytics реализована с помощью Диспетчера тегов Google, файлу будет присвоено название _dc_gtm_%property-id%","a/b testing group flag":"флаг группы тестирования a/b"});
/* jshint +W100 */
}]);
//-----------------------------------------
});;

/**
*    @ngdoc overview
*    @name ui.cms
*    @description
*    Основной модуль cms. Дает обертки над функциями ангулара,
*    компоненты для работы с картинками(отображение, загрузка) и
*    файлами.
*    <br/>
*    <br/>
*
*    <b>ВАЖНО: в html стороннего сайта до загрузки ui.cms должны быть определены
*    такие глобальные переменные:</b>
*    <pre>
*        var CMS_DB_BUCKET = "my_super_bucket"; // bucket который привязан к сайту который подключает этот модуль
*        var CMS_STORAGE = {
*            type: "amazon",     // где хранятся загруженные файлы ('amazon', 'google', 'digitaloceanSpaces', 'local')
*            server: "http://url-to-server.com"  // путь к серверу где хранится статика. необязательно если cms запущена на localhost
*        }
*        var CMS_DEFAULT_LANG = "en"; // главный язык сайта(fallback-язык для отображения cms-объектов)
*        var CMS_ALLOWED_LANGS = ["en", "ru"]; // разрешенные языки на сайте
*    </pre>
*
 */

(function() {
  
try {
    CMS_VERSION = CMS_VERSION || 31;
} catch (e) {
    CMS_VERSION = 31;
}
;
  angular.module('ui.cms', ['lbServices', 'storage.gdpr', 'ui.gettext.langPicker']);


  /**
  *    @ngdoc overview
  *    @name GLOBAL
  *    @description
  *    в этом разделе описаны функции, которые добавлены в базовые js объекты.
  *    Т.е. их не нужно как-то инжектить в контроллеры/сервисы и т.п. Они доступны везде. Подробнее см примеры
   */

}).call(this);
;

/**
*   @ngdoc object
*   @name window.String
*   @description дополнительные функции для работы со строками
 */


/**
*   @ngdoc function
*   @methodOf window.String
*   @name window.String#matchAll
*   @description версия по мотивам String.prototype.match, только эта функция всегда возвращает массив с индексами вхождения строки, вместо неинформационного просто возвращения массива со строками
*   @param {RegExp} regexp глобальный регексп
*   @returns {Array} массив массивов или null если вхождения не найдены. см пример
*   @example
*   <pre>
*       > test = "hello hello hel world"
*       > test.match(/hello/g)
*       ["hello", "hello"]
*
*       > test.matchAll(/hello/g)
*       [
*           {0:"hello", index: 0, input: "hello hello hel world"},
*           {0:"hello", index: 6, input: "hello hello hel world"}
*       ]
*   </pre>
 */

(function() {
  var arrayRemoveDuplicates, dateEndOf, dateStartOf, dateToDateISOString, dateToTimeISOString, isDocument, isElement, normalizeWeekDay, objectArrayMap, padLeft, resolveValue, unwrapElement;

  String.prototype.matchAll = function(regexp) {
    var matches;
    matches = [];
    this.replace(regexp, function() {
      var arr, extras;
      arr = [].slice.call(arguments, 0);
      extras = arr.splice(-2);
      arr.index = extras[0];
      arr.input = extras[1];
      matches.push(arr);
    });
    if (matches.length) {
      return matches;
    } else {
      return null;
    }
  };


  /**
  *   @ngdoc function
  *   @methodOf window.String
  *   @name window.String#replaceAll
  *   @description функция для замены всех вхождений строки на другую. Поддерживается как замена по регекспу, так и без него
  *   @param {RegExp|string} search строка или регексп для замены
  *   @param {string} replacement строка на которую заменяются вхождения search
  *   @returns {string} новая строка в которой произведена замена
  *   @example
  *   <pre>
  *       > test = "hello hello hel world"
  *       > test.replaceAll(/he(l+)o/g, "what?")
  *       "what? what? hel world"
  *
  *       > test.replaceAll("hello", "whoa!")
  *       "whoa! whoa! hel world"
  *   </pre>
   */

  String.prototype.replaceAll = function(search, replacement) {
    var target;
    target = this;
    if (search instanceof RegExp) {
      target = target.replace(new RegExp(search, 'g'), replacement);
    } else {
      search = angular.copy(search) + '';
      target = target.split(search).join(replacement);
    }
    return target;
  };


  /**
  *   @ngdoc object
  *   @name window.Object
  *   @description дополнительные функции для работы со Object
   */


  /**
  *   @ngdoc function
  *   @methodOf window.Object
  *   @name window.Object#arrayMap
  *   @description функция для создания нового массива с результатом вызова указанной функции для каждой пары КЛЮЧ/ЗНАЧЕНИЕ объекта.
  *       Функция подобна {@link https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Global_Objects/Array/map }
  *   @param {function} callback функция, создающая элемент в новом массиве, принимает три аргумента:
  *       <ul>
  *           <li><b>key</b> - ключ из объекта</li>
  *           <li><b>value</b> - значение ключа из объекта</li>
  *           <li><b>object</b> - объект, по которому осуществляется проход</li>
  *       </ul>
  *   @param {any=} thisArg значение, используемое в качестве this при вызове функции callback
  *   @returns {Array} массив, где каждый элемент является результатом callback функции
  *   @example
  *   <pre>
  *       > test = {hello: 'world', other_key: 'other value'}
  *       // преобразовываем объект в массив объединенных ключей+значений
  *       > test.arrayMap (k,v)-> "#{k} = #{v}"
  *       [
  *           'hello = world',
  *           'other_key = other value'
  *       ]
  *   </pre>
   */

  objectArrayMap = function(callback, thisArg) {
    var A, O, T, i, k, key, len, value;
    T = void 0;
    A = void 0;
    k = void 0;
    if (this === null) {
      throw new TypeError('this is null or not defined');
    }
    O = Object(this);
    len = O.length >>> 0;
    if (typeof callback !== 'function') {
      throw new TypeError(callback + ' is not a function');
    }
    if (arguments.length > 1) {
      T = thisArg;
    }
    A = new Array(len);
    i = 0;
    for (key in O) {
      value = O[key];
      A[i] = callback.call(T, key, value, O);
      i += 1;
    }
    return A;
  };

  if (!Object.prototype.arrayMap) {
    if (Object.defineProperty) {
      Object.defineProperty(Object.prototype, 'arrayMap', {
        value: objectArrayMap
      });
    } else {
      Object.prototype.arrayMap = objectArrayMap;
    }
  }

  padLeft = function(value) {
    if (value < 10) {
      return "0" + value;
    }
    return "" + value;
  };


  /**
  *   @ngdoc object
  *   @name window.Date
  *   @description дополнительные функции для работы c датами
   */


  /**
  *   @ngdoc function
  *   @methodOf window.Date
  *   @name window.Date#toDateISOString
  *   @description возвращает строковое предстваление объекта Date, в iso-дату, т.е. в строку без времени и часового пояса. Вывод зависит от часового пояса!
  *   @returns {string} iso-date строка. см пример
  *   @example
  *   <pre>
  *       > test = new Date('1990-02-10T10:10')
  *       > test.toDateISOString()
  *       "1990-02-10"
  *   </pre>
   */

  dateToDateISOString = function() {
    if (!this) {
      return null;
    }
    if (isNaN(this.getTime())) {
      return null;
    }
    return (this.getFullYear()) + "-" + (padLeft(this.getMonth() + 1)) + "-" + (padLeft(this.getDate()));
  };


  /**
  *   @ngdoc function
  *   @methodOf window.Date
  *   @name window.Date#toTimeISOString
  *   @description возвращает строковое предстваление объекта Date, в iso-время, т.е. в строку без даты и часового пояса. Вывод зависит от часового пояса!
  *   @returns {string} iso-time строка. см пример
  *   @example
  *   <pre>
  *       > test = new Date('1990-02-10T10:10:40')
  *       > test.toTimeISOString()
  *       "10:10:40"
  *   </pre>
   */

  dateToTimeISOString = function() {
    if (!this) {
      return null;
    }
    if (isNaN(this.getTime())) {
      return null;
    }
    return (padLeft(this.getHours())) + ":" + (padLeft(this.getMinutes())) + ":" + (padLeft(this.getSeconds()));
  };

  normalizeWeekDay = function(d) {
    if (d === 0) {
      return 7;
    }
    return d;
  };


  /**
  *   @ngdoc function
  *   @methodOf window.Date
  *   @name window.Date#startOf
  *   @description по мотивам библиотеки moment.js и ее функции startOf - возвращает в зависимости от типа: начало дня, недели, месяца у даты
  *   @param {string=} [type='day'] тип выравнивания даты - 'day', 'week', 'month'
  *   @returns {Date} см пример
  *   @example
  *   <pre>
  *       > test = new Date('1990-02-10T10:10:40')
  *       > test.startOf('month')
  *       Thu Feb 01 1990 00:00:00 GMT+0200 (MSK)
  *       > test.startOf('day')
  *       Sat Feb 10 1990 00:00:00 GMT+0200 (MSK)
  *   </pre>
   */

  dateStartOf = function(type) {
    var d, weekday;
    if (!['day', 'week', 'month'].includes(type)) {
      type = 'day';
    }
    d = new Date(this);
    switch (type) {
      case 'day':
        d.setHours(0, 0, 0, 0);
        break;
      case 'week':
        weekday = normalizeWeekDay(d.getDay());
        if (weekday !== 1) {
          d.setDate(d.getDate() - weekday + 1);
        }
        d.setHours(0, 0, 0, 0);
        break;
      case 'month':
        d = new Date(d.getFullYear(), d.getMonth(), 1);
        d.setHours(0, 0, 0, 0);
    }
    return d;
  };


  /**
  *   @ngdoc function
  *   @methodOf window.Date
  *   @name window.Date#endOf
  *   @description по мотивам библиотеки moment.js и ее функции endOf - возвращает в зависимости от типа: конец дня, недели, месяца у даты
  *   @param {string=} [type='day'] тип выравнивания даты - 'day', 'week', 'month'
  *   @returns {Date} см пример
  *   @example
  *   <pre>
  *       > test = new Date('1990-02-10T10:10:40')
  *       > test.endOf('month')
  *       Wed Feb 28 1990 23:59:59 GMT+0200 (MSK)
  *       > test.endOf('week')
  *       Sun Feb 11 1990 23:59:59 GMT+0200 (MSK)
  *   </pre>
   */

  dateEndOf = function(type) {
    var d, weekday;
    if (!['day', 'week', 'month'].includes(type)) {
      type = 'day';
    }
    d = new Date(this);
    switch (type) {
      case 'day':
        d.setHours(23, 59, 59, 999);
        break;
      case 'week':
        weekday = normalizeWeekDay(d.getDay());
        if (weekday !== 7) {
          d.setDate(d.getDate() + 7 - weekday);
        }
        d.setHours(23, 59, 59, 999);
        break;
      case 'month':
        d = new Date(d.getFullYear(), d.getMonth() + 1, 0);
        d.setHours(23, 59, 59, 999);
    }
    return d;
  };

  if (Object.defineProperty) {
    Object.defineProperty(Date.prototype, 'toDateISOString', {
      value: dateToDateISOString
    });
    Object.defineProperty(Date.prototype, 'toTimeISOString', {
      value: dateToTimeISOString
    });
    Object.defineProperty(Date.prototype, 'startOf', {
      value: dateStartOf
    });
    Object.defineProperty(Date.prototype, 'endOf', {
      value: dateEndOf
    });
  } else {
    Date.prototype.toDateISOString = dateToDateISOString;
    Date.prototype.toTimeISOString = dateToTimeISOString;
    Date.prototype.startOf = startOf;
    Date.prototype.endOf = endOf;
  }


  /**
  *   @ngdoc object
  *   @name window.Array
  *   @description дополнительные функции для работы с массивами
   */


  /**
  *   @ngdoc function
  *   @methodOf window.Array
  *   @name window.Array#removeDuplicates
  *   @description функция для удаления дубликатов из массивов. Работает только либо с простыми типами, либо с теми, которые поддерживают сравнение по ===
  *   @returns {Array} очищенный массив
  *   @example
  *   <pre>
  *       > arr = [1, 2, 1,"hello", null, undefined, NaN, 1,2, "world", NaN]
  *       > arr.removeDuplicates()
  *       [1, 2, "hello", null, undefined, NaN, "world"]
  *   </pre>
   */

  arrayRemoveDuplicates = function() {
    var cleaned, item, l, len1, nan_added, ref;
    cleaned = [];
    nan_added = false;
    ref = this || [];
    for (l = 0, len1 = ref.length; l < len1; l++) {
      item = ref[l];
      if (angular.isNumber(item) && isNaN(item)) {
        if (!nan_added) {
          cleaned.push(0/0);
          nan_added = true;
        }
        continue;
      }
      if (cleaned.indexOf(item) === -1) {
        cleaned.push(item);
      }
    }
    return cleaned;
  };

  if (Object.defineProperty) {
    Object.defineProperty(Array.prototype, 'removeDuplicates', {
      value: arrayRemoveDuplicates
    });
  } else {
    Array.prototype.removeDuplicates = arrayRemoveDuplicates;
  }


  /**
  *   @ngdoc function
  *   @methodOf window.Array
  *   @name window.Array#findByProperty
  *   @description функция для нахождения в массиве объекта по его свойству. Возвращает первый найденный объект или undefined
  *   @param {string} property название свойства по которому производится поиск
  *   @param {any} value значение свойства
  *   @returns {any} найденный объект или undefined
  *   @example
  *   <pre>
  *       > arr = [{id: 1}, {id: 2, text: 'hello'}, {}, null, NaN, {id: 3}]
  *       > arr.findByProperty('id', 3)
  *       {id: 3}
  *
  *       > arr.findByProperty('text', 'hello')
  *       {id: 2, text: 'hello'}
  *
  *       > arr.findByProperty('id', 4)
  *       undefined
  *   </pre>
   */

  Array.prototype.findByProperty = function(property, value) {
    var item, l, len1, ref;
    ref = this || [];
    for (l = 0, len1 = ref.length; l < len1; l++) {
      item = ref[l];
      if (angular.isDefined(item) && item !== null) {
        if (item[property] === value) {
          return item;
        }
      }
    }
    return void 0;
  };


  /**
  *   @ngdoc function
  *   @methodOf window.Array
  *   @name window.Array#move
  *   @description функция для перемещения элемента с индекса на другое место. <b>Функция модифицирует исходный массив</b>
  *   @param {number} from индекс элемента который нужно переместить
  *   @param {number} to индекс, на который нужно поместить элемент
  *   @example
  *   <pre>
  *       > arr = [{id: 1}, {id: 2, text: 'hello'}, {}, null, NaN, {id: 3}]
  *       > arr.move(1,3)
  *       > console.log(arr)
  *       [{id: 1}, {}, null, {id: 2, text: 'hello'}, NaN, {id: 3}]
  *
  *       > arr = [{id: 1}, {id: 2, text: 'hello'}, {}, null, NaN, {id: 3}]
  *       > arr.move(1,0)
  *       > console.log(arr)
  *       [{id: 2, text: 'hello'}, {id: 1}, {}, null, NaN, {id: 3}]
  *
  *   </pre>
   */

  Array.prototype.move = function(from, to) {
    this.splice(to, 0, this.splice(from, 1)[0]);
  };


  /**
  *   @ngdoc function
  *   @methodOf window.Array
  *   @name window.Array#insert
  *   @description функция для вставки элемента в массив под индексом. <b>Функция модифицирует исходный массив</b>
  *   @param {number} index индекс, на который нужно поместить элемент
  *   @param {any} item элемент
  *   @example
  *   <pre>
  *       > arr = [{id: 1}, {id: 2, text: 'hello'}, {}, null, NaN, {id: 3}]
  *       > arr.insert(1, "WORLD")
  *       > console.log(arr)
  *       [{id: 1}, "WORLD", {id: 2, text: 'hello'}, {}, null, NaN, {id: 3}]
  *   </pre>
   */

  Array.prototype.insert = function(index, item) {
    this.splice(index, 0, item);
  };


  /**
  *   @ngdoc function
  *   @methodOf window.Array
  *   @name window.Array#remove
  *   @description функция для удаления элемента из массива под индексом. <b>Функция модифицирует исходный массив</b>
  *   @param {number} index индекс элемента
  *   @returns {any} удаленный элемент
  *   @example
  *   <pre>
  *       > arr = [{id: 1}, {id: 2, text: 'hello'}, {}, null, NaN, {id: 3}]
  *       > arr.remove(1)
  *       {id: 2, text: 'hello'}
  *       > console.log(arr)
  *       [{id: 1}, {}, null, NaN, {id: 3}]
  *
  *       > arr = [{id: 1}, {id: 2, text: 'hello'}, {}, null, NaN, {id: 3}]
  *       > arr.remove(3)
  *       null
  *       > console.log(arr)
  *       [{id: 1}, {id: 2, text: 'hello'}, {}, NaN, {id: 3}]
  *   </pre>
   */

  Array.prototype.remove = function(index) {
    return this.splice(index, 1)[0];
  };


  /**
  *   @ngdoc function
  *   @methodOf window.Array
  *   @name window.Array#removeValue
  *   @description функция для удаления элемента из массива по значению. <b>Функция модифицирует исходный массив</b>
  *   @param {any} value значение элемента
  *   @returns {any} удаленный элемент
  *   @example
  *   <pre>
  *       > arr = [{id: 1}, {id: 2, text: 'hello'}, {}, null, NaN, {id: 3}]
  *       > arr.removeValue({id: 2, text: 'hello'})
  *       {id: 2, text: 'hello'}
  *       > console.log(arr)
  *       [{id: 1}, {}, null, NaN, {id: 3}]
  *
  *       > arr = [{id: 1}, null, {id: 2, text: 'hello'}, {}, null, NaN, {id: 3}]
  *       > arr.removeValue(null)
  *       null
  *       > console.log(arr)
  *       [{id: 1}, {id: 2, text: 'hello'}, {}, NaN, {id: 3}]
  *   </pre>
   */

  Array.prototype.removeValue = function(value) {
    var index, l, ref, removedValue, v;
    removedValue = void 0;
    for (index = l = ref = this.length - 1; l >= 0; index = l += -1) {
      v = this[index];
      if (angular.equals(value, v)) {
        removedValue = this.splice(index, 1)[0];
      }
    }
    return removedValue;
  };

  if (!Array.prototype.includes) {
    Array.prototype.includes = function(searchElement, fromIndex) {
      var k, len, n, o, sameValueZero;
      if (this === null) {
        throw new TypeError('"this" is null or not defined');
      }
      sameValueZero = function(x, y) {
        return x === y || typeof x === 'number' && typeof y === 'number' && isNaN(x) && isNaN(y);
      };
      o = Object(this);
      len = o.length >>> 0;
      if (len === 0) {
        return false;
      }
      n = fromIndex | 0;
      if (n >= 0) {
        k = Math.max(n, 0);
      } else {
        k = Math.max(len - Math.abs(n), 0);
      }
      while (k < len) {
        if (sameValueZero(o[k], searchElement)) {
          return true;
        }
        k++;
      }
      return false;
    };
  }

  if (!Array.prototype.some) {
    Array.prototype.some = function(fun) {
      var i, len, t, thisArg;
      if (this === null) {
        throw new TypeError('Array.prototype.some called on null or undefined');
      }
      if (typeof fun !== 'function') {
        throw new TypeError;
      }
      t = Object(this);
      len = t.length >>> 0;
      thisArg = arguments.length >= 2 ? arguments[1] : void 0;
      i = 0;
      while (i < len) {
        if (i in t && fun.call(thisArg, t[i], i, t)) {
          return true;
        }
        i++;
      }
      return false;
    };
  }


  /**
  *   @ngdoc function
  *   @name angular.isEmpty
  *   @description проверяет является ли переданный объект пустым.
  *   В основном предназначен для определения является ли пустой передаваемая строка, является ли объект пустым(без свойств), пустой ли массив.
  *   <div></div>
  *   <b>Когда объект считается пустым:</b>
  *   <ul>
  *       <li>когда он равен <i>null</i> или <i>undefined</i></li>
  *       <li>когда свойство объекта <i>length</i> равно нулю</li>
  *       <li>когда Object.keys(obj).length == 0</li>
  *   </ul>
  *   <div></div>
  *   <b>Когда объект считается НЕ пустым:</b>
  *   <ul>
  *       <li>когда тип объекта не равен object (т.е., число, функция)</li>
  *       <li>остальные случаи не оговоренные выше</li>
  *   </ul>
  *   @param {Any=} obj объект для проверки
  *   @returns {boolean} true/false
  *   @example
  *   <pre>
  *       > angular.isEmpty(null)
  *       true
  *
  *       > angular.isEmpty(undefined)
  *       true
  *
  *       > angular.isEmpty(false)
  *       false
  *
  *       > angular.isEmpty(0)
  *       false
  *
  *       > angular.isEmpty('')
  *       true
  *
  *       > angular.isEmpty(NaN)
  *       false
  *
  *       > angular.isEmpty({})
  *       true
  *
  *       > angular.isEmpty([])
  *       true
  *
  *       > arr = new Array(8)
  *       > angular.isEmpty(arr)
  *       false // массив забит undefined-ами, но свойство length больше нуля
  *
  *       > angular.isEmpty("Hello")
  *       false
  *
  *       > angular.isEmpty({test: "Hello"})
  *       false
  *
  *       > angular.isEmpty(["test"])
  *       false
  *   </pre>
   */

  angular.isEmpty = angular.isEmpty || function(obj) {
    if (obj === null || obj === void 0) {
      return true;
    }
    if (obj.length > 0) {
      return false;
    }
    if (obj.length === 0) {
      return true;
    }
    if (typeof obj !== 'object') {
      return false;
    }
    return Object.keys(obj).length === 0;
  };


  /**
  *   @ngdoc function
  *   @name angular.tr
  *   @description функция, которая предназначена для маркирования строковых литералов
  *   в исходном коде для последующего перевода под gettext. <b>В верстке, потом эти строки должны все равно переводиться!</b>
  *   <style>.usage, h2#usage{display: none;}</style>
  *   @param {string=} text текст
  *   @returns {string} тот же самый текст который подавался на вход в функцию
  *   @example
  *   <pre>
  *       > angular.tr("My text")
  *       "My text"
  *   </pre>
   */

  angular.tr = angular.tr || function(text) {
    return text || '';
  };

  resolveValue = function(value, default_value) {
    if (!angular.isDefined(default_value)) {
      return value;
    }
    if (!angular.isDefined(value)) {
      return default_value;
    }
    if (angular.isString(value) && angular.isString(default_value)) {
      if (value.length > 0) {
        return value;
      }
      return default_value;
    }
    return value;
  };


  /**
  *   @ngdoc function
  *   @name angular.getValue
  *   @description возвращает значение пути к полю объекта
  *   @param {object|Array|Any} obj объект у которого нужно считать свойство
  *   @param {string} path путь к свойству. Напр: 'prop1.prop2[0].prop3'
  *   @param {Any=} [default=undefined] значение поля, если считать его из объекта не получилось.
  *   @returns {Any} значение
  *   @example
  *   <pre>
  *       > myObject = {
  *           prop1:{
  *               text: 'Hello world'
  *               array: [
  *                   'hi!',
  *                   {id: -1, text: 'custom object'}
  *               ]
  *           }
  *       }
  *       > angular.getValue(myObject, 'prop1.text', 'No text')
  *       'Hello world'
  *
  *       > angular.getValue(myObject, 'prop2.text', 'No text')
  *       'No text'
  *
  *       > angular.getValue(myObject, 'prop1.array[1].text')
  *       'custom object'
  *   </pre>
   */

  if (!angular.getValue) {
    angular.getValue = function(obj, _path, default_value) {
      var _p, l, len1, ref, res;
      if (obj === null || typeof obj === 'undefined' || !_path) {
        return default_value;
      }
      if (_path.indexOf('.') === -1 && _path.indexOf('[') === -1) {
        res = obj[_path];
        return resolveValue(res, default_value);
      }
      _path = _path.replace(/\[(\w+)\]/g, '.$1');
      _path = _path.replace(/^\./, '');
      res = obj;
      ref = _path.split('.');
      for (l = 0, len1 = ref.length; l < len1; l++) {
        _p = ref[l];
        if (res === null) {
          return default_value || res;
        }
        if (typeof res[_p] !== 'undefined') {
          res = res[_p];
        } else {
          return default_value;
        }
      }
      return resolveValue(res, default_value);
    };
  }


  /**
  *   @ngdoc function
  *   @name angular.getValueOr
  *   @description похожая на angular.getValue функция, только 1 обязательный аргумент - объект с которого считываются свойства,
  *       последний обязательный аргумент - значение по умолчанию если ни одно свойство не прочитано.
  *       В середине аргументов можно указывать любое количестов(минимум 2) путей к свойству. Таким образом
  *       минимальное количество аргументов для функции - 4
  *   @returns {Any} значение
  *   @example
  *   <pre>
  *       > myObject = {
  *           prop1:{
  *               text: 'Hello world'
  *               array: [
  *                   'hi!',
  *                   {id: -1, text: 'custom object'}
  *               ]
  *           }
  *       }
  *       > angular.getValueOr(myObject, 'prop1.text', 'prop2.text' 'No text')
  *       'Hello world'
  *
  *       > angular.getValueOr(myObject, 'prop3.text', 'prop2.text' 'No text')
  *       'No text'
  *
  *       > angular.getValueOr(myObject, 'prop3.text', 'pro1p1.text' 'No text')
  *       'Hello world'
  *   </pre>
   */

  if (!angular.getValueOr) {
    angular.getValueOr = function(obj) {
      var _p, _paths, default_value, l, len1, str_values, v;
      if (arguments.length < 4) {
        throw "Expected 4 arguments min for this function. Got " + arguments.length;
      }
      _paths = Array.prototype.slice.call(arguments, 1, arguments.length - 1);
      default_value = arguments[arguments.length - 1];
      str_values = [];
      for (l = 0, len1 = _paths.length; l < len1; l++) {
        _p = _paths[l];
        v = angular.getValue(obj, _p);
        if (!angular.isDefined(v)) {
          continue;
        }
        if (angular.isString(default_value)) {
          if (!angular.isString(v) && v !== null) {
            return v;
          }
          if (v && v.length) {
            str_values.push(v);
          }
          continue;
        }
        if (v === null) {
          return default_value || v;
        }
        return v;
      }
      return str_values[0] || default_value;
    };
  }


  /**
  *   @ngdoc function
  *   @name angular.setValue
  *   @description добавляет значение свойства в объект согласно пути к свойству
  *   @example
  *   <pre>
  *       > myObject = {
  *           prop1:{
  *               text: 'Hello world'
  *               array: [
  *                   'hi!',
  *                   {id: -1, text: 'custom object'}
  *               ]
  *           }
  *       }
  *       > angular.setValue(myObject, 'prop1.text', 'Other text')
  *       > console.log(myObject.prop1.text)
  *       'Other text'
  *
  *       > angular.setValue(myObject, 'prop1.array[1].text', 'Other text')
  *       > console.log(myObject.prop1.array[1].text)
  *       'Other text'
  *   </pre>
   */

  if (!angular.setValue) {
    angular.setValue = function(obj, _path, value) {
      var j, nP, normalizeP, parent;
      if (obj === null || typeof obj === 'undefined' || !_path) {
        return;
      }
      if (_path.indexOf('.') === -1 && _path.indexOf('[') === -1) {
        obj[_path] = value;
        return;
      }
      parent = obj;
      _path = _path.replace(/^\./, '');
      _path = _path.split('.');
      normalizeP = function(p) {
        if (p[0] === '[' && p[p.length - 1] === ']') {
          p = p.replace('[', '').replace(']', '');
          return {
            p: p,
            index: parseInt(p),
            isArray: true
          };
        }
        return {
          p: p
        };
      };
      j = 0;
      while (j < _path.length - 1) {
        nP = normalizeP(_path[j]);
        if (parent.hasOwnProperty(nP.p)) {
          parent = parent[nP.p];
        } else {
          if (_path[j + 1][0] === '[') {
            parent[nP.p] = [];
          } else {
            parent[nP.p] = {};
          }
          parent = parent[nP.p];
        }
        j += 1;
      }
      nP = normalizeP(_path[_path.length - 1]);
      parent[nP.p] = value;
    };
  }

  isDocument = function(el) {
    return (typeof HTMLDocument !== 'undefined' && el instanceof HTMLDocument) || (el.nodeType && el.nodeType === el.DOCUMENT_NODE);
  };

  isElement = function(el) {
    return (typeof HTMLElement !== 'undefined' && el instanceof HTMLElement) || (el.nodeType && el.nodeType === el.ELEMENT_NODE);
  };

  unwrapElement = function(el) {
    if (!el) {
      return null;
    }
    if (isElement(el) || isDocument(el)) {
      return el;
    }
    return el[0];
  };


  /**
  *   @ngdoc function
  *   @name angular.element.prototype.contains
  *   @description добавляет к jqLite метод определения, принадлежит ли HTML элемент родителю <style>.usage, h2#usage{display: none;}</style>
  *   @example
  *   <pre>
  *       > el = angular.element($window)
  *       > el.contains(document.getElementById('hello'))
  *       true
  *   </pre>
   */

  angular.element.prototype.contains = function(contained) {
    var container;
    container = unwrapElement(this);
    contained = unwrapElement(contained);
    if (!container || !contained) {
      return false;
    }
    return container.contains(contained);
  };

}).call(this);
;

/*
            Strings
 */

(function() {
  var arrayFind, arrayIncludes, arrayMap, stringEndsWith, stringIncludes, stringStartsWith,
    indexOf = [].indexOf || function(item) { for (var i = 0, l = this.length; i < l; i++) { if (i in this && this[i] === item) return i; } return -1; };

  stringIncludes = function(searchStr, fromIndex) {
    if (!this || !this.length) {
      return false;
    }
    if (typeof fromIndex !== 'number') {
      fromIndex = 0;
    }
    if (fromIndex + searchStr.length > this.length) {
      return false;
    }
    return this.indexOf(searchStr, fromIndex) !== -1;
  };

  if (!String.prototype.includes) {
    if (Object.defineProperty) {
      Object.defineProperty(String.prototype, 'includes', {
        value: stringIncludes
      });
    } else {
      String.prototype.includes = stringIncludes;
    }
  }

  stringEndsWith = function(searchString, position) {
    var lastIndex, subjectString;
    subjectString = this.toString();
    if (position === void 0 || position > subjectString.length) {
      position = subjectString.length;
    }
    position -= searchString.length;
    lastIndex = subjectString.indexOf(searchString, position);
    return lastIndex !== -1 && lastIndex === position;
  };

  if (!String.prototype.endsWith) {
    if (Object.defineProperty) {
      Object.defineProperty(String.prototype, 'endsWith', {
        value: stringEndsWith
      });
    } else {
      String.prototype.endsWith = stringEndsWith;
    }
  }

  stringStartsWith = function(searchString, position) {
    position = position || 0;
    return this.indexOf(searchString, position) === position;
  };

  if (!String.prototype.startsWith) {
    if (Object.defineProperty) {
      Object.defineProperty(String.prototype, 'startsWith', {
        value: stringStartsWith
      });
    } else {
      String.prototype.endsWith = stringStartsWith;
    }
  }


  /*
              Arrays
   */

  arrayIncludes = function(searchElement, fromIndex) {
    var k, len, n, o, sameValueZero;
    if (this === null) {
      throw new TypeError('Array.prototype.includes: "this" is null or not defined');
    }
    o = Object(this);
    len = o.length || 0;
    if (len === 0) {
      return false;
    }
    if (!fromIndex && this.indexOf(searchElement) > -1) {
      return true;
    }
    n = parseInt(fromIndex) || 0;
    if (n >= 0) {
      k = Math.max(n, 0);
    } else {
      k = Math.max(len - Math.abs(n), 0);
    }
    sameValueZero = function(x, y) {
      return x === y || (typeof x === 'number' && typeof y === 'number' && isNaN(x) && isNaN(y));
    };
    while (k < len) {
      if (sameValueZero(o[k], searchElement)) {
        return true;
      }
      k++;
    }
    return false;
  };

  arrayMap = function(callback, thisArg) {
    var A, O, T, k, kValue, len, mappedValue;
    T = void 0;
    A = void 0;
    k = void 0;
    if (this === null) {
      throw new TypeError(' this is null or not defined');
    }
    O = Object(this);
    len = O.length >>> 0;
    if (typeof callback !== 'function') {
      throw new TypeError(callback + ' is not a function');
    }
    if (arguments.length > 1) {
      T = thisArg;
    }
    A = new Array(len);
    k = 0;
    while (k < len) {
      kValue = void 0;
      mappedValue = void 0;
      if (indexOf.call(O, k) >= 0) {
        kValue = O[k];
        mappedValue = callback.call(T, kValue, k, O);
        A[k] = mappedValue;
      }
      k++;
    }
    return A;
  };

  arrayFind = function(predicate) {
    var i, length, list, thisArg, value;
    if (this === null) {
      throw new TypeError('Array.prototype.find called on null or undefined');
    }
    if (typeof predicate !== 'function') {
      throw new TypeError('predicate must be a function');
    }
    if (this instanceof Array) {
      list = this;
    } else {
      list = Object(this);
    }
    length = list.length >>> 0;
    thisArg = arguments[1];
    value = void 0;
    i = 0;
    while (i < length) {
      value = list[i];
      if (predicate.call(thisArg, value, i, list)) {
        return value;
      }
      i++;
    }
    return void 0;
  };

  if (!Array.prototype.includes) {
    if (Object.defineProperty) {
      Object.defineProperty(Array.prototype, 'includes', {
        value: arrayIncludes
      });
    } else {
      Array.prototype.includes = arrayIncludes;
    }
  }

  if (!Array.prototype.map) {
    if (Object.defineProperty) {
      Object.defineProperty(Array.prototype, 'map', {
        value: arrayMap
      });
    } else {
      Array.prototype.map = arrayMap;
    }
  }

  if (!Array.prototype.find) {
    if (Object.defineProperty) {
      Object.defineProperty(Array.prototype, 'find', {
        value: arrayFind
      });
    } else {
      Array.prototype.find = arrayFind;
    }
  }

}).call(this);
;

/**
*    @ngdoc service
*    @name ui.cms.service:UicGoogleMapClass
*    @description UicGoogleMapClass фабрика которая возвращает класс для работы с google maps. Конструктор
*       принимает следующие параметры:
*    @param {jqLite | HTMLElement} element елемент внутри которого будет отрендерена карта
*    @param {Object=} data параметры
*     - `config` – <i>(opional)</i> <span class='label type-hint type-hint-object' style='display: inline-block;'>object</span> – параметры карты
*     - `config.zoom` – <i>(opional)</i> <span class='label type-hint type-hint-number' style='display: inline-block;'>number</span> – зум карты <i>(default: 15)</i>
*     - `config.center` – <i>(opional)</i> <span class='label type-hint type-hint-object' style='display: inline-block;'>object</span> <span class='label type-hint type-hint-array' style='display: inline-block;'>Array</span> – точка центра карты (если не заданы маркеры, иначе центр карты будет меж маркерами вычислен самой библиотекой)
*     - `config.useInfoBox` – <i>(opional)</i> <span class='label type-hint type-hint-boolean' style='display: inline-block;'>boolean</span> – загружать ли и использовать infoBox вместо стандарных тултипов от гуглекарт <i>(default: false)</i>
*     - `config.hideMarkerTooltipOnInit` – <i>(opional)</i> <span class='label type-hint type-hint-boolean' style='display: inline-block;'>boolean</span> – скрывать ли тултипы от маркеров на карте. По умолчанию при загрузке они будут отображены. иначе пользоветелю придется кликать по маркеру <i>(default: false)</i>
*     - `markers` – <i>(opional)</i> <span class='label type-hint type-hint-array' style='display: inline-block;'>Array</span> – массив маркеров для карты. Формат маркера описан в {@link ui.cms.service:UicGoogleMapClass#setMarker}
*    @returns {instanceOf} UicGoogleMapClass
 */

(function() {
  var GOOGLEMAP_CACHE, GOOGLEMAP_MAX_ALLOWED_WAYPOINTS;

  GOOGLEMAP_MAX_ALLOWED_WAYPOINTS = 23;

  GOOGLEMAP_CACHE = {
    geocoder: {},
    autocomplete: {}
  };

  angular.module('ui.cms').factory('UicGoogleMapClass', ["__uicGoogleMap", "$rootScope", "$langPicker", "$filter", "$q", "$timeout", "$location", "$gdprStorage", "$uicIntegrations", "H", function(__uicGoogleMap, $rootScope, $langPicker, $filter, $q, $timeout, $location, $gdprStorage, $uicIntegrations, H) {
    var UicGoogleMapAutocompleteHelper, UicGoogleMapClass, UicGoogleMapGeocoderHelper, findRoute, getCacheValue, getDistanceMatrixDirect, getMarkerFlagChar, l10n, markerToUrlParam, numberFilter, setValueToCache;
    l10n = $filter('l10n');
    numberFilter = $filter('number');
    getCacheValue = function(type, id) {
      var base;
      $gdprStorage.googleMapsCache || ($gdprStorage.googleMapsCache = {});
      (base = $gdprStorage.googleMapsCache)[type] || (base[type] = {});
      id = id.toLowerCase().trim();
      if (!id) {
        return;
      }
      if (GOOGLEMAP_CACHE[type][id]) {
        return angular.copy(GOOGLEMAP_CACHE[type][id]);
      }
      if ($gdprStorage.googleMapsCache[type][id]) {
        return angular.copy($gdprStorage.googleMapsCache[type][id]);
      }
    };
    setValueToCache = function(type, id, value) {
      var len;
      id = id.toLowerCase().trim();
      if (!id) {
        return;
      }
      value = JSON.parse(JSON.stringify(value));
      GOOGLEMAP_CACHE[type][id] = value;
      len = JSON.stringify($gdprStorage.googleMapsCache).length * 2 / 1048576;
      if (len > 5) {
        $gdprStorage.googleMapsCache = {
          geocoder: {},
          autocomplete: {}
        };
      }
      $gdprStorage.googleMapsCache[type][id] = value;
    };
    getMarkerFlagChar = function(l10nText, i) {
      var allowed, text;
      text = l10n(l10nText) || "M";
      text = text[0].toUpperCase();
      allowed = /^[A-Z0-9]*$/;
      if (!allowed.test(text)) {
        text = i + '';
      }
      return text;
    };
    markerToUrlParam = function(marker, i) {
      var m;
      m = {
        color: marker.color || 'green',
        label: getMarkerFlagChar(marker.text, i)
      };
      if (marker.coordinate instanceof Array) {
        m.latLng = marker.coordinate;
      } else {
        m.latLng = [marker.coordinate.lat, marker.coordinate.lng];
      }
      return "markers=color:" + m.color + "|label:" + m.label + "|" + m.latLng[0] + "," + m.latLng[1];
    };
    findRoute = function(gmap, origin, destination, waypoints, index) {
      var defer, params;
      defer = $q.defer();
      params = {
        origin: gmap.getLatLng(origin),
        destination: gmap.getLatLng(destination),
        waypoints: (waypoints || []).map(function(p) {
          return {
            location: gmap.getLatLng(p),
            stopover: true
          };
        }),
        optimizeWaypoints: false,
        travelMode: google.maps.TravelMode.DRIVING
      };
      gmap.directionsService.route(params, function(response, status) {
        if (status !== google.maps.DirectionsStatus.OK) {
          return defer.reject();
        }
        return defer.resolve({
          response: response,
          index: index,
          params: params
        });
      });
      return defer.promise;
    };
    getDistanceMatrixDirect = function(options) {
      var R, a, c, dLat, dLon, distance, distanceText, end, start, toRad;
      start = options.origins[0];
      end = options.destinations[0];
      if (angular.isString(start) || angular.isString(end)) {
        throw "UicGoogleMapClass: cant get getDistanceMatrix for text points. Provide lat/lng coordinates";
      }
      toRad = function(value) {
        return value * Math.PI / 180;
      };
      R = 6371;
      dLat = toRad(end.lat - start.lat);
      dLon = toRad(end.lng - start.lng);
      start.lat = toRad(start.lat);
      end.lat = toRad(end.lat);
      a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(start.lat) * Math.cos(end.lat);
      c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
      distance = R * c;
      if (options.unitSystem === google.maps.UnitSystem.IMPERIAL) {
        distance = distance / 1.609;
        distanceText = numberFilter(distance) + ' ml';
      } else {
        distanceText = numberFilter(distance) + ' km';
      }
      return {
        originAddresses: [start.text || start],
        destinationAddresses: [end.text || end],
        rows: [
          {
            elements: [
              {
                distance: {
                  value: distance,
                  text: distanceText
                },
                status: 'OK'
              }
            ]
          }
        ]
      };
    };
    UicGoogleMapGeocoderHelper = (function() {
      function UicGoogleMapGeocoderHelper(geocoder) {
        this.geocoder = geocoder;
      }

      UicGoogleMapGeocoderHelper.prototype.addressStrListToGmapData = function(addressStrList, onOk, onFail) {
        var decoded, error, processed, self, toDecode;
        decoded = [];
        toDecode = [];
        if (!angular.isFunction(onOk)) {
          onOk = angular.noop;
        }
        if (!angular.isFunction(onFail)) {
          onFail = angular.noop;
        }
        addressStrList.forEach(function(address, i) {
          var cachedValue;
          if (!angular.isString(address)) {
            throw "UicGoogleMapGeocoderHelper: object " + address + " is not a string";
          }
          cachedValue = getCacheValue('geocoder', address);
          if (cachedValue) {
            decoded[i] = cachedValue;
            decoded[i].$originalAddress = address;
          } else {
            toDecode.push({
              index: i,
              address: address
            });
          }
        });
        if (!toDecode.length) {
          return onOk(decoded);
        }
        self = this;
        error = false;
        processed = 0;
        toDecode.forEach(function(params) {
          var decode;
          decode = function() {
            self.geocoder.geocode({
              address: params.address
            }, function(results, status) {
              if (status === 'OK' || status === 'ZERO_RESULTS') {
                results.$originalAddress = params.address;
                setValueToCache('geocoder', params.address, results);
                decoded[params.index] = results;
              } else {
                params.failIterator || (params.failIterator = 0);
                params.failIterator += 1;
                if (params.failIterator <= 100) {
                  $timeout(decode, 300 + params.index * 2);
                  return;
                }
                error = status;
              }
              processed += 1;
              if (toDecode.length === processed) {
                if (error) {
                  onFail(error);
                  return;
                }
                onOk(decoded);
              }
            });
          };
          return decode();
        });
      };

      return UicGoogleMapGeocoderHelper;

    })();
    UicGoogleMapAutocompleteHelper = (function() {
      function UicGoogleMapAutocompleteHelper(autocomplete) {
        this.autocomplete = autocomplete;
      }

      UicGoogleMapAutocompleteHelper.prototype.addressToGmapPredictions = function(options, onOk, onFail) {
        var cachedValue, decode, error, input;
        input = options.address;
        if (angular.isString(options)) {
          input = options;
          options = {};
        } else {
          delete options.address;
        }
        if (!angular.isString(input)) {
          throw "UicGoogleMapAutocompleteHelper: object " + input + " is not a string";
        }
        options.input = input.toLowerCase();
        if (!angular.isFunction(onOk)) {
          onOk = angular.noop;
        }
        if (!angular.isFunction(onFail)) {
          onFail = angular.noop;
        }
        cachedValue = getCacheValue('autocomplete', options.input);
        if (cachedValue) {
          onOk(cachedValue);
          return;
        }
        error = false;
        decode = (function(_this) {
          return function() {
            _this.autocomplete.getPlacePredictions(options, function(results, status) {
              if (status === 'OK' || status === 'ZERO_RESULTS') {
                setValueToCache('autocomplete', options.input, results);
              } else {
                params.failIterator || (params.failIterator = 0);
                params.failIterator += 1;
                if (params.failIterator <= 100) {
                  $timeout(decode, 300);
                  return;
                }
                error = status;
              }
              if (error) {
                onFail(error);
              } else {
                onOk(getCacheValue('autocomplete', options.input) || []);
              }
            });
          };
        })(this);
        decode();
      };

      return UicGoogleMapAutocompleteHelper;

    })();
    return UicGoogleMapClass = (function() {
      function UicGoogleMapClass(element, data1, options1) {
        var base;
        this.element = element;
        this.data = data1;
        this.options = options1;
        this.map = null;
        this.geocoder = null;
        this.directionsService = null;
        this.directionsDisplay = null;
        this.mapMarkers = [];
        this.mapTooltips = {};
        this._callbacks = {};
        this.data || (this.data = {});
        (base = this.data).config || (base.config = {});
      }

      UicGoogleMapClass.prototype.getMarkerFlagChar = getMarkerFlagChar;

      UicGoogleMapClass.prototype.getLatLng = function(coordinate) {

        /**
        *   @ngdoc property
        *   @name ui.cms.service:UicGoogleMapClass#getLatLng
        *   @methodOf ui.cms.service:UicGoogleMapClass
        *   @description функция, которая нормализирует координаты в потомок google.maps.LatLng
        *   @param {object|Array|google.maps.LatLng} coordinate координата, которую надо преобразовать
        *   @returns {object} экземпляр google.maps.LatLng с координатами
        *   @example
        *   <pre>
        *       > // gm - экземпляр от класса UicGoogleMapClass
        *       > gm.getLatLng([10, 20]) // когда координата - массив из lat, lng
        *       FIXME: дописать пример
        *       > gm.getLatLng({lat: 10, lng: 20}) // когда координата - объект с ключами (стандартное и желательное поведение)
        *       > gm.getLatLng(c)
        *   </pre>
         */
        if (coordinate instanceof google.maps.LatLng) {
          return coordinate;
        }
        if (coordinate && coordinate.coordinate) {
          coordinate = coordinate.coordinate;
        }
        if (coordinate instanceof Array) {
          return new google.maps.LatLng(coordinate[0], coordinate[1]);
        } else {
          return new google.maps.LatLng(coordinate.lat, coordinate.lng);
        }
      };

      UicGoogleMapClass.prototype.initMap = function(done) {

        /**
        *   @ngdoc property
        *   @name ui.cms.service:UicGoogleMapClass#initMap
        *   @methodOf ui.cms.service:UicGoogleMapClass
        *   @description
        *   @param {function=} done функция, которая вызывается при полной загрузке js библиотек google maps
         */
        var postInit;
        postInit = (function(_this) {
          return function() {
            var coordinate, error, lastZoom, listener, mapOptions, zoom;
            if (_this.data.markers) {
              coordinate = _this.data.markers[0].coordinate;
            } else {
              coordinate = [0, 0];
            }
            zoom = _this.data.config.zoom || 'default';
            if (zoom === 'default') {
              zoom = 15;
            }
            try {
              zoom = parseInt(zoom);
            } catch (error1) {
              error = error1;
              zoom = 15;
            }
            mapOptions = {
              center: _this.getLatLng(_this.data.config.center || coordinate),
              zoom: zoom,
              zoomControl: true,
              mapTypeId: google.maps.MapTypeId.ROADMAP,
              scrollwheel: false
            };
            if (!_this.map && _this.element) {
              _this.map = new google.maps.Map(_this.element, mapOptions);
            } else if (_this.map && _this.map.setOptions) {
              _this.map.setOptions(mapOptions);
            }
            _this.geocoder = new google.maps.Geocoder();
            _this.geocoderHelper = new UicGoogleMapGeocoderHelper(_this.geocoder);
            _this.autocomplete = new google.maps.places.AutocompleteService();
            _this.autocompleteHelper = new UicGoogleMapAutocompleteHelper(_this.autocomplete);
            if (!_this.element) {
              if (done) {
                done();
              }
              return;
            }
            if (zoom !== 0) {
              lastZoom = zoom;
              listener = _this.map.addListener('zoom_changed', function() {
                if (_this.map.zoom === 0 && lastZoom !== 1) {
                  listener.remove();
                  _this.map.setZoom(zoom);
                }
                lastZoom = _this.map.zoom;
              });
            }
            if (done) {
              return done();
            }
          };
        })(this);
        return __uicGoogleMap.load(this.data.config.forceLang, (function(_this) {
          return function() {
            var infoBoxLib;
            if (_this.data.config && _this.data.config.useInfoBox) {
              infoBoxLib = $uicIntegrations.getThirdPartyLibrary('googlemaps-infobox');
              return infoBoxLib.load(postInit);
            }
            return postInit();
          };
        })(this));
      };

      UicGoogleMapClass.prototype.createMarkerTooltip = function(mapMarker, text) {
        var infoWindow;
        if (this.data.config && this.data.config.useInfoBox) {
          if (this.infoBox) {
            this.infoBox.close();
          }
          this.infoBox = new InfoBox({
            content: text,
            disableAutoPan: false,
            maxWidth: 0,
            pixelOffset: new google.maps.Size(10, 125),
            closeBoxMargin: '5px -20px 2px 2px',
            closeBoxURL: 'http://www.google.com/intl/en_us/mapfiles/close.gif',
            isHidden: false,
            alignBottom: true,
            pane: 'floatPane',
            enableEventPropagation: true
          });
          this.infoBox.open(this.map, mapMarker);
          return;
        }
        if (this.mapTooltips[mapMarker._id]) {
          this.mapTooltips[mapMarker._id].close();
        }
        if (!text) {
          return;
        }
        infoWindow = new google.maps.InfoWindow({
          content: text
        });
        this.mapTooltips[mapMarker._id] = infoWindow;
        return infoWindow.open(this.map, mapMarker);
      };

      UicGoogleMapClass.prototype.setMarker = function(marker, options) {

        /**
        *   @ngdoc property
        *   @name ui.cms.service:UicGoogleMapClass#setMarker
        *   @methodOf ui.cms.service:UicGoogleMapClass
        *   @description устанавливает маркер на карте
        *   @param {object} marker описание маркера в формате:
        *     - `marker.coordinate` – <span class='label type-hint type-hint-object' style='display: inline-block;'>object</span> <span class='label type-hint type-hint-array' style='display: inline-block;'>Array</span> <span class='label type-hint type-hint-object' style='display: inline-block;'>google.maps.LatLng</span> – координата
        *     - `marker.text` – <i>(opional)</i> <span class='label type-hint type-hint-object' style='display: inline-block;'>L10nString</span> – переводимая строка-объект
        *     - `marker.color` – <i>(opional)</i> <span class='label type-hint type-hint-string' style='display: inline-block;'>string</span> – цвет маркера <i>(default: 'green')</i>
        *     - `marker.icon` – <i>(opional)</i> <span class='label type-hint type-hint-string' style='display: inline-block;'>string</span> – полный путь к картинке-маркеру, если вы хотите использовать картинку вместо стандарных цветных маркеров (тогда свойство color будет проигнорировано)
        *     - `marker.template` – <i>(opional)</i> <span class='label type-hint type-hint-string' style='display: inline-block;'>string</span> – шаблон верстки тултипа (при указании этого свойства будет проигнорировано свойство text), имеет смысл использовать только при загрузке карты с InfoBox
        *   @param {Object=} options дополнительные параметры, которые вы хотите передать маркеру (см документацию по google.maps.Marker)
        *   @returns {instanceOf} google.maps.Marker экземпляр маркера
        *   @example
        *   <pre>
        *       > // gm - экземпляр от класса UicGoogleMapClass
        *       > marker = {
        *           coordinate: {lat: 10, lng: 20},
        *           color: 'red',
        *           text: {
        *               ru: 'Заголовок для маркер',
        *               en: 'Marker title'
        *           }
        *        }
        *       > gm.setMarker(marker)
        *       FIXME: дописать пример
        *   </pre>
         */
        var base, color, inMarkers, index, k, l, len1, m, mapMarker, markerOptions, ref, ref1, text, v;
        color = marker.color || 'green';
        if (typeof marker.text === 'string') {
          text = marker.text;
        }
        if (typeof marker.coordinate.text === 'string') {
          text = marker.coordinate.text;
        } else {
          text = l10n(marker.text) || '';
        }
        if (marker.template) {
          text = marker.template;
        }
        (base = this.data).markers || (base.markers = []);
        inMarkers = false;
        ref = this.data.markers;
        for (l = 0, len1 = ref.length; l < len1; l++) {
          m = ref[l];
          if (!(angular.equals(m, marker))) {
            continue;
          }
          inMarkers = true;
          break;
        }
        if (!inMarkers) {
          this.data.markers.push(marker);
        }
        markerOptions = {
          position: this.getLatLng(marker.coordinate),
          map: this.map,
          title: text,
          icon: "https://maps.google.com/mapfiles/ms/icons/" + color + "-dot.png"
        };
        if (marker.icon) {
          if (marker.icon[0] === '/' && $location.host() !== 'localhost') {
            markerOptions.icon = H.path.resolveFilePath(marker.icon.substr(1));
          } else {
            markerOptions.icon = marker.icon;
          }
        }
        ref1 = options || {};
        for (k in ref1) {
          v = ref1[k];
          markerOptions[k] = v;
        }
        mapMarker = new google.maps.Marker(markerOptions);
        mapMarker.setIconColor = function(color) {
          return this.setIcon("https://maps.google.com/mapfiles/ms/icons/" + color + "-dot.png");
        };
        mapMarker._id = new Date().getTime() + color;
        index = this.mapMarkers.length;
        this.mapMarkers.push(mapMarker);
        google.maps.event.addListener(mapMarker, 'click', (function(_this) {
          return function() {
            _this.createMarkerTooltip(mapMarker, text);
            return _this.emit('markerClick', mapMarker, marker, index);
          };
        })(this));
        if (!this.data.config || !this.data.config.hideMarkerTooltipOnInit) {
          this.createMarkerTooltip(mapMarker, text);
        }
        return mapMarker;
      };

      UicGoogleMapClass.prototype.updateMarkers = function() {

        /**
        *   @ngdoc property
        *   @name ui.cms.service:UicGoogleMapClass#updateMarkers
        *   @methodOf ui.cms.service:UicGoogleMapClass
        *   @description перерисовка всех маркеров, котрые находятся в self.data.markers (установлены через конструктор UicGoogleMapClass или добавлены через setMarker)
         */
        (this.data.markers || []).forEach((function(_this) {
          return function(m) {
            _this.setMarker(m);
          };
        })(this));
      };

      UicGoogleMapClass.prototype.resetMarkers = function(force) {

        /**
        *   @ngdoc property
        *   @name ui.cms.service:gUicGoogleMapClass#updateMarkers
        *   @methodOf ui.cms.service:UicGoogleMapClass
        *   @description удаление всех маркеров, тултиров и InfoBox-ов с карты
         */
        var iW, id, ref;
        if (force === void 0) {
          force = true;
        }
        (this.mapMarkers || []).forEach(function(m) {
          m.setMap(null);
        });
        ref = this.mapTooltips;
        for (id in ref) {
          iW = ref[id];
          iW.close();
        }
        this.mapMarkers = [];
        this.mapTooltips = {};
        if (force) {
          this.data.markers = [];
        }
      };

      UicGoogleMapClass.prototype.fitBounds = function(force) {

        /**
        *   @ngdoc property
        *   @name ui.cms.service:UicGoogleMapClass#fitBounds
        *   @methodOf ui.cms.service:UicGoogleMapClass
        *   @description центрирование карты так, чтоб были видны все маркера или если маркеров нет, то чтоб карта была с центром из координаты что была передана в конструктором (зум будет применен такой, чтоб было видны все маркера или виден центр)
        *   @param {boolean=} [force=false] если на карте маркеров всего 1 или 0, то центрирование не будет выполнено. Если нужно чтоб центрирование выполнилось все равно - передайте true
         */
        var bounds, center, coordinate, l, len1, m, ref, routes;
        if (this.mapMarkers.length < 2 && !force) {
          return;
        }
        center = this.data.config.center;
        routes = ((this.directionsDisplay || {}).directions || {}).routes || [];
        if (this.mapMarkers.length <= 1 && !routes.length) {
          if (center) {
            coordinate = this.getLatLng(center);
          }
          if (!coordinate && this.mapMarkers.length) {
            coordinate = this.mapMarkers[0].position;
          }
          if (coordinate) {
            this.map.setCenter(coordinate);
          }
        } else {
          if (routes.length) {
            bounds = routes[routes.length - 1].bounds;
          } else {
            bounds = new google.maps.LatLngBounds();
          }
          ref = this.mapMarkers;
          for (l = 0, len1 = ref.length; l < len1; l++) {
            m = ref[l];
            bounds.extend(m.position);
          }
          this.map.fitBounds(bounds);
        }
        center = center || {};
        if (center.offsetX || center.offsetY) {
          this.map.panBy(center.offsetX || 0, center.offsetY || 0);
        }
      };

      UicGoogleMapClass.prototype.getDistanceMatrix = function(markers, options, done) {

        /**
        *   @ngdoc property
        *   @name ui.cms.service:UicGoogleMapClass#getDistanceMatrix
        *   @methodOf ui.cms.service:UicGoogleMapClass
        *   @description рассчитывает расстояние меж маркеров как дороги. Обертка над  google.maps.DistanceMatrixService которая использует следующие параметры: `{travelMode: google.maps.TravelMode.DRIVING, unitSystem: google.maps.UnitSystem.METRIC, avoidHighways: false, avoidTolls: false}`
        *   @param {array=} markers маркеры. Если массив не передан, то будут взяты маркеры с карты (в порядке добавления)
        *   @returns {Promise} обещание. Используйте `.then (response)->` чтоб получить результаты. respone может быть undefined если маркеров меньше 2.
         */
        var defer, response, service, unwrap;
        if (angular.isFunction(options)) {
          done = options;
          options = {};
        }
        if (angular.isFunction(markers)) {
          done = markers;
          options = {};
          markers = null;
        }
        if (!angular.isFunction(done)) {
          done = angular.noop;
        }
        defer = $q.defer();
        markers = markers || this.data.markers || [];
        if (markers.length < 2) {
          defer.resolve();
          done();
          return defer.promise;
        }
        unwrap = function(coordinate) {
          if (coordinate.coordinate) {
            coordinate = coordinate.coordinate;
          }
          if (options.travelMode === 'DIRECT') {
            return coordinate;
          }
          if (angular.isNumber(coordinate.lat) && angular.isNumber(coordinate.lng)) {
            return {
              lat: coordinate.lat,
              lng: coordinate.lng
            };
          }
          return {
            lat: coordinate.lat(),
            lng: coordinate.lng()
          };
        };
        options.origins = [unwrap(markers[0])];
        options.destinations = markers.slice(1).map(unwrap);
        options.travelMode || (options.travelMode = google.maps.TravelMode.DRIVING);
        options.unitSystem || (options.unitSystem = google.maps.UnitSystem.METRIC);
        if (!options.hasOwnProperty('avoidHighways')) {
          options.avoidHighways = false;
        }
        if (!options.hasOwnProperty('avoidTolls')) {
          options.avoidTolls = false;
        }
        if (options.travelMode === 'DIRECT') {
          response = getDistanceMatrixDirect(options);
          defer.resolve(response);
          done(response);
          return defer.promise;
        }
        service = new google.maps.DistanceMatrixService();
        service.getDistanceMatrix(options, function(response, status) {
          if (status !== google.maps.DistanceMatrixStatus.OK) {
            return defer.reject();
          }
          defer.resolve(response);
          done(response);
        });
        return defer.promise;
      };

      UicGoogleMapClass.prototype.findAndDisplayRoute = function(markers, done) {

        /**
        *   @ngdoc property
        *   @name ui.cms.service:UicGoogleMapClass#findAndDisplayRoute
        *   @methodOf ui.cms.service:UicGoogleMapClass
        *   @description находит маршрут меж маркеров и отображает его на карте. Обертка над  google.maps.DirectionsService и google.maps.DirectionsRenderer которая использует следующие параметры: `{optimizeWaypoints: false, travelMode: google.maps.TravelMode.DRIVING}`. <b>Эта функция обходит ограничение гугле-карт на максимальные 23+2 точки в маршруте</b>
        *   @param {array=} markers маркеры. Если массив не передан, то будут взяты маркеры с карты (в порядке добавления)
        *   @returns {Promise} обещание. Используйте `.then (response)->` чтоб получить результаты. respone может быть undefined если маркеров меньше 2.
         */
        var _destination, _origin, chunk, defer, destination, i, j, origin, prev_chunk, tasks, waypoints;
        if (!angular.isFunction(done)) {
          done = angular.noop;
        }
        defer = $q.defer();
        markers = markers || this.data.markers || [];
        if (markers.length < 2) {
          defer.resolve();
          done();
          return defer.promise;
        }
        if (!this.directionsService && !this.directionsDisplay) {
          this.directionsService = new google.maps.DirectionsService();
          this.directionsDisplay = new google.maps.DirectionsRenderer();
          this.directionsDisplay.setMap(this.map);
        }
        origin = markers[0];
        destination = markers[markers.length - 1];
        waypoints = markers.slice(1, -1);
        if (waypoints.length > GOOGLEMAP_MAX_ALLOWED_WAYPOINTS) {
          tasks = [];
          i = 0;
          j = 0;
          while (i < waypoints.length) {
            chunk = waypoints.slice(i, i + GOOGLEMAP_MAX_ALLOWED_WAYPOINTS);
            if (!i) {
              _origin = origin;
              _destination = chunk.pop();
            }
            ({
              "else": prev_chunk = waypoints.slice(i - GOOGLEMAP_MAX_ALLOWED_WAYPOINTS, i)
            });
            if (i + GOOGLEMAP_MAX_ALLOWED_WAYPOINTS >= waypoints.length) {
              _origin = prev_chunk[prev_chunk.length - 1];
              _destination = destination;
            } else if (i) {
              _origin = prev_chunk[prev_chunk.length - 1];
              _destination = chunk.pop();
            }
            tasks.push(findRoute(this, _origin, _destination, chunk, j));
            i += GOOGLEMAP_MAX_ALLOWED_WAYPOINTS;
            j += 1;
          }
          $q.all(tasks).then((function(_this) {
            return function(result) {
              var l, lastResponse, len1, r, response;
              response = result.findByProperty('index', 0).response;
              lastResponse = result.findByProperty('index', result.length - 1).response;
              response.request.destination = lastResponse.request.destination;
              response.request.waypoints = response.request.waypoints || [];
              response.routes.forEach(function(r, i) {
                delete response.routes[i].overview_polyline;
                delete response.routes[i].waypoint_order;
                delete response.routes[i].bounds;
                response.routes[i].warnings.push('Merged by UicGoogleMapGeocoderHelper');
              });
              for (i = l = 0, len1 = result.length; l < len1; i = ++l) {
                r = result[i];
                if (!(r.index !== 0)) {
                  continue;
                }
                r = result.findByProperty('index', i).response;
                response.routes[0].legs = response.routes[0].legs.concat(r.routes[0].legs);
                response.request.waypoints = response.request.waypoints.concat(r.waypoints || []);
              }
              response.requestParams = response.request;
              response.requestParams.origin = origin;
              response.requestParams.destination = destination;
              response.requestParams.waypoints = waypoints;
              _this.directionsDisplay.setDirections(response);
              defer.resolve(response);
              return done(response);
            };
          })(this));
          return defer.promise;
        }
        findRoute(this, origin, destination, waypoints).then((function(_this) {
          return function(data) {
            var response;
            response = data.response;
            response.requestParams = data.params;
            response.requestParams.origin = origin;
            response.requestParams.destination = destination;
            response.requestParams.waypoints = waypoints;
            _this.directionsDisplay.setDirections(response);
            defer.resolve(response);
            return done(response);
          };
        })(this), defer.reject);
        return defer.promise;
      };

      UicGoogleMapClass.prototype.generateStaticImgUrl = function(mapSize) {

        /**
        *   @ngdoc property
        *   @name ui.cms.service:UicGoogleMapClass#generateStaticImgUrl
        *   @methodOf ui.cms.service:UicGoogleMapClass
        *   @description генерирует url к static-map картинке на серверах гугла. Отображаются все маркера. Для того, чтоб получить url не обазательно загружать js библиотеки через initMap. <b>Тултипы и кастомные картинки для маркеров не поддерживаются!</b>
        *   @param {object} mapSize размер картинки в пикселях: {height: 100, width: 400}
        *   @returns {string} url к картинке
        *   @example
        *   <pre>
        *       > marker1 = {
        *           coordinate: {lat: 10, lng: 20},
        *           color: 'red',
        *           text: {
        *               ru: 'Заголовок для маркера',
        *               en: 'Marker title'
        *           }
        *        }
        *       > marker2 = {
        *           coordinate: {lat: 15, lng: 23},
        *           color: 'yellow',
        *           text: {
        *               ru: 'Заголовок для маркера',
        *               en: 'Marker title'
        *           }
        *        }
        *       // вы можете генерировать url как с уже загруженой карты с js-библиотеками от гугля, так и не загружая вообще ничего
        *       // для второго случая нужно указывать null как элемент карты
        *       > gm = new UicGoogleMapClass(null, {markers: [marker1, marker2]})
        *       > gm.generateStaticImgUrl({height: 200, width: 100})
        *       FIXME: дописать пример
        *   </pre>
         */
        var i, l, len1, marker, params, ref, url, zoom;
        url = "https://maps.googleapis.com/maps/api/staticmap";
        params = "maptype=roadmap&language=" + $langPicker.currentLang;
        params += "&key=" + CMS_SPECIAL_INTEGRATIONS.mapsApiKey;
        zoom = (this.data.config || {}).zoom || 'default';
        if (zoom !== 'default') {
          params += "&zoom=" + zoom;
        }
        if (mapSize) {
          params += "&size=" + mapSize.width + "x" + mapSize.height;
        }
        ref = this.data.markers || [];
        for (i = l = 0, len1 = ref.length; l < len1; i = ++l) {
          marker = ref[i];
          params = params + '&' + markerToUrlParam(marker, i + 1);
        }
        return url + "?" + params;
      };

      UicGoogleMapClass.prototype.on = function(eventName, func) {
        var id;
        if (!this._callbacks[eventName]) {
          this._callbacks[eventName] = {};
        }
        id = new Date().getTime();
        this._callbacks[eventName][id] = func;
        return (function(_this) {
          return function() {
            delete _this._callbacks[eventName][id];
          };
        })(this);
      };

      UicGoogleMapClass.prototype.emit = function() {
        var args, eventName, func, id, ref;
        eventName = arguments[0];
        args = Array.prototype.slice.call(arguments, 1);
        ref = this._callbacks[eventName] || {};
        for (id in ref) {
          func = ref[id];
          $rootScope.$apply((function(_this) {
            return function() {
              return func.apply(_this, args);
            };
          })(this));
        }
      };

      return UicGoogleMapClass;

    })();
  }]).service('__uicGoogleMap', ["$rootScope", "H", function($rootScope, H) {
    var disableGoogleFontsLoad, gfontsDisabled;
    this.loaded = false;
    this.callbacks = [];
    gfontsDisabled = false;
    disableGoogleFontsLoad = function() {
      var head, insertBefore;
      if (gfontsDisabled) {
        return;
      }
      gfontsDisabled = true;
      head = document.getElementsByTagName('head')[0];
      insertBefore = head.insertBefore;
      head.insertBefore = function(newElement, referenceElement) {
        if (newElement.href && newElement.href.indexOf('https://fonts.googleapis.com') === 0) {
          return;
        }
        return insertBefore.call(head, newElement, referenceElement);
      };
    };
    window.uicGooglemapInitMap = (function(_this) {
      return function() {
        $rootScope.$apply(function() {
          _this.callbacks.forEach(function(cb) {
            return cb();
          });
          _this.callbacks = [];
          _this.loaded = true;
        });
      };
    })(this);
    this.load = function(forceLang, cb) {
      if (this.loaded) {
        if (angular.isFunction(cb)) {
          cb();
        }
        return;
      }
      if (angular.isFunction(cb)) {
        this.callbacks.push(cb);
      }
      disableGoogleFontsLoad();
      if (forceLang) {
        H.loadOnce.js("https://maps.googleapis.com/maps/api/js?key=" + CMS_SPECIAL_INTEGRATIONS.mapsApiKey + "&sensor=false&callback=window.uicGooglemapInitMap&libraries=places&language=" + (forceLang.toLowerCase()));
      } else {
        H.loadOnce.js("https://maps.googleapis.com/maps/api/js?key=" + CMS_SPECIAL_INTEGRATIONS.mapsApiKey + "&sensor=false&callback=window.uicGooglemapInitMap&libraries=places");
      }
    };
    return this;
  }]);

}).call(this);
;
(function() {
  angular.module('ui.cms').directive('autofocus', ["$timeout", function($timeout) {
    var getInputElement;
    getInputElement = function(el) {
      var ref, ref1, ref2;
      if (!el || !el[0] || !el[0].nodeName) {
        return;
      }
      if ((ref = el[0].nodeName) === 'INPUT' || ref === 'SELECT' || ref === 'BUTTON') {
        return el;
      }
      if ((ref1 = el[0].nodeName) === 'UIC-COUNTRY-PICKER') {
        return getInputElement(el.find('button')) || getInputElement(el.find('select'));
      }
      if ((ref2 = el[0].nodeName) === 'UICE-SELECT' || ref2 === 'UIC-SELECT') {
        return getInputElement(el.find('button'));
      }
    };
    return {
      restrict: 'A',
      link: function($scope, $element) {
        $timeout(function() {
          var el;
          el = getInputElement($element) || getInputElement($element.find('input')) || getInputElement($element.find('select')) || getInputElement($element.find('button'));
          if (!el) {
            return;
          }
          el[0].focus();
        }, 150);
      }
    };
  }]);

}).call(this);
;
(function() {
  angular.module('ui.cms').directive('uicAlert', function() {
    var ICONS, getIcon, getType;
    ICONS = {
      'danger': 'fas fa-exclamation-triangle',
      'info': 'fas fa-info-circle',
      'warning': 'fas fa-exclamation-triangle',
      'success': 'fas fa-check-circle'
    };
    getType = function(alertType) {
      if (!['success', 'info', 'warning', 'danger'].includes(alertType)) {
        return 'info';
      }
      return alertType;
    };
    getIcon = function(alertType) {
      return ICONS[getType(alertType)];
    };
    return {

      /**
      *   @ngdoc directive
      *   @name ui.cms.directive:uicAlert
      *   @description плашка-уведомление
      *
      *   @restrict E
      *   @param {string=} [type='info'] (значение) тип плашки-уведомления. В ['danger', 'info', 'warning', 'success']
      *   @param {string=} [icon] (значение) css-классы иконки, которая будет отображена слева. Если не указана, то будет подобрана автоматически на основе верхнего значения type
      *   @example
      *       <pre>
      *           //- pug
      *           uic-alert(type='danger')
      *               b Test alert
      *       </pre>
      *       <example module="DocExampleApp">
      *           <file name="script.js">
      *               mkDocCtrl('uicAlertCtrl', {
      *
      *               });
      *           </file>
      *           <file name="index.html">
      *               <div ng-controller="uicAlertCtrl">
      *                   <uic-alert type='danger'><b>Test alert</b></uic-alert>
      *               </div>
      *           </file>
      *       </example>
       */
      restrict: 'E',
      transclude: true,
      controller: function() {

        /*
        $attrs.$observe 'type', (newValue, oldValue)->
         */
      },
      template: function($element, $attrs) {
        return "<div class=\"alert alert-" + (getType($attrs.type)) + "\" role='alert'>\n  <ul class=\"list-table nomargin\">\n     <li class='w-3em'>\n        <fa-icon class=\"" + ($attrs.icon || getIcon($attrs.type)) + " fa-3x\"></fa-icon>\n     </li>\n     <li ng-transclude=''>\n     </li>\n</div>";
      }
    };
  });

}).call(this);
;
(function() {
  angular.module('ui.cms').directive('uicAutoHeight', function() {
    return {

      /**
      *   @ngdoc directive
      *   @name ui.cms.directive:uicAutoHeight
      *   @description    директива для подстраивания высоты виджета под контент.
      *       В основном имеет смысл для textarea-тега. <b>Обязательно нужно указывать в css минимальную высоту и привязывать ngModel</b>
      *   @restrict A
      *   @example
      *       <pre>
      *           //-pug
      *           textarea.form-control(uic-auto-height='', ng-model="data")
      *       </pre>
      *       <example module="DocExampleApp">
      *           <file name="script.js">
      *               mkDocCtrl('uicAutoHeightCtrl', {
      *                   data: "Директива для подстраивания высоты виджета под контент.\nНажмите несколько раз Enter"
      *               });
      *           </file>
      *           <file name="index.html">
      *               <div ng-controller="uicAutoHeightCtrl">
      *                   <textarea ng-model="data" uic-auto-height='' class='form-control'>
      *                   </textarea>
      *               </div>
      *           </file>
      *       </example>
       */
      restrict: 'A',
      require: 'ngModel',
      controller: ["$element", "$timeout", function($element, $timeout) {
        var error, ngModelCtrl, updateAutoHeight;
        try {
          ngModelCtrl = $element.controller('ngModel');
        } catch (error1) {
          error = error1;
          console.error("uicAutoHeight: Can't find ngModel controller for this tag");
          return;
        }
        updateAutoHeight = function(value) {
          if ($element && $element[0]) {
            $element[0].style.height = "5px";
            $element[0].style.height = ($element[0].scrollHeight || '5') + "px";
          }
          return value;
        };
        ngModelCtrl.$formatters.push(updateAutoHeight);
        ngModelCtrl.$parsers.push(updateAutoHeight);
        $element.css({
          "max-height": 'none'
        });
        $element.css({
          "overflow": 'hidden'
        });
        $timeout(updateAutoHeight, 300);
      }]
    };
  });

}).call(this);
;
(function() {
  angular.module('ui.cms').directive('uicBindHtml', ["$compile", "$timeout", function($compile, $timeout) {
    return {

      /**
      *   @ngdoc directive
      *   @name ui.cms.directive:uicBindHtml
      *   @description директива подобная {@link https://docs.angularjs.org/api/ng/directive/ngBindHtml ng-bind-html},
      *       только лучше.
      *       Не требует установки флага доверия для текстового шаблона полученного не штатным образом.
      *   @restrict A
      *   @param {string} uicBindHtml текстовый шаблон
      *   @param {function} onCompileDone функция, которую можно запустить после того, как шаблон будет привязан
      *   @param {any} bind-* переменная, которую нужно <b>однонаправленно</b> пробросить. ВНИМАНИЕ: это создаст новый $scope! Формат такой: <div></div><i>bind-{alias}="variableNameInScope"</i>
      *   @example
      *       <pre>
      *           $scope.myAngularTemplate = "<h1 ng-hide='someFunc()'>Some text</h1>"
      *           $scope.myAngular2Template = "<h1>User value: {{item.value}}</h1>"
      *           $scope.variable = {value: '123'}
      *       </pre>
      *       <pre>
      *           //- pug
      *           div(uic-bind-html="myAngularTemplate")
      *           div(uic-bind-html="myAngular2Template", bind-item="variable")
      *       </pre>
      *       <example module="DocExampleApp">
      *           <file name="script.js">
      *               mkDocCtrl('uicBindHtmlCtrl', {
      *                   myAngularTemplate: '<h1 ng-hide="someFunc()">Some text</h1>',
      *                   myAngular2Template: '<h1>User value: {{item.value}}</h1>',
      *                   variable: {value: '123'}
      *               });
      *           </file>
      *           <file name="index.html">
      *               <div ng-controller="uicBindHtmlCtrl">
      *                   <div uic-bind-html="myAngularTemplate"></div>
      *                   <input class='form-control' ng-model="variable.value">
      *                   <div uic-bind-html="myAngular2Template" bind-item="variable"></div>
      *               </div>
      *           </file>
      *       </example>
       */
      restrict: 'A',
      link: function($scope, $element, $attrs) {
        var binds, k, onCompileDone, scope;
        binds = {};
        scope = $scope;
        for (k in $attrs) {
          if (k.startsWith('bind')) {
            binds[k[4].toLowerCase() + k.substr(5)] = $attrs[k];
          }
        }
        if ($attrs.onCompileDone) {
          onCompileDone = $scope.$eval($attrs.onCompileDone.split('(')[0]);
        }
        if (Object.keys(binds).length) {
          scope = $scope.$new(true);
          angular.forEach(Object.keys(binds), function(key) {
            $scope.$watch(binds[key], function(value) {
              scope[key] = value;
            }, true);
          });
        }
        $scope.$watch($attrs.uicBindHtml, function(html) {
          $element.html(html || '');
          $compile($element.contents())(scope);
          if (angular.isFunction(onCompileDone)) {
            $timeout(onCompileDone, 0);
          }
        });
      }
    };
  }]).directive('uicBindHtmlWithItem', ["$compile", "$timeout", function($compile, $timeout) {
    return {
      restrict: 'A',
      scope: {
        uicBindHtmlWithItem: '=',
        item: '=',
        onCompileDone: '&?'
      },
      replace: true,
      link: function($scope, $element, attrs) {
        console.warn("uicBindHtmlWithItem is deprecated. Use uicBindHtml with bind-* attributes");
        $scope.$watch('uicBindHtmlWithItem', function(html) {
          $element.html(html);
          $compile($element.contents())($scope);
          if (angular.isFunction($scope.onCompileDone)) {
            $timeout($scope.onCompileDone, 0);
          }
        });
      }
    };
  }]);

}).call(this);
;
(function() {
  angular.module('ui.cms').directive('uicBreadcrumbs', ["$state", function($state) {
    return {
      restrict: 'E',
      controller: ["$scope", function($scope) {
        console.warn('uicBreadcrumbs: deprecated!');
        return $scope.$state = $state;
      }],
      template: "<ol class=\"breadcrumb\">\n    <li ng-repeat=\"crumb in $breadcrumbs\" ng-class=\"{ 'active' : $last }\">\n        <a ng-if=\"!$last\" href=\"{{$state.href(crumb.state, crumb.stateParams)}}\">{{crumb.title}}</a>\n        <span ng-if=\"$last\">{{crumb.title}}</span>\n    </li>\n</ol>"
    };
  }]);

}).call(this);
;
(function() {
  angular.module('ui.cms').directive('uicCaret', function() {
    return {

      /**
      *   @ngdoc directive
      *   @name ui.cms.directive:uicCaret
      *   @description директива-обертка для легкого отображения кареток
      *       <span class="caret" style="margin-top: 9px;"></span>
      *        или
      *       <span class="dropup"><span class="caret" style="margin-top: 9px;"></span></span>
      *       <br/>
      *       В основном используется в заголовках таблиц для отображения направления сортировки.
      *   @restrict E
      *   @param {boolean} direction напраление каретки (true - вверх, false - вниз)
      *   @example
      *       <pre>
      *           $scope.myDirection = true
      *       </pre>
      *       <pre>
      *           //- pug
      *           uic-caret(direction="myDirection")
      *       </pre>
       */
      restrict: 'E',
      scope: {
        direction: '='
      },
      controller: ["$scope", function($scope) {}],
      template: "<span ng-show=\"direction\" class=\"caret\"></span>\n<span ng-show=\"!direction\" class=\"dropup\">\n    <span class=\"caret\"></span>\n</span>"
    };
  });

}).call(this);
;
(function() {
  angular.module('ui.cms').directive('uicCenterBlock', function() {
    return {

      /**
      *   @ngdoc directive
      *   @name ui.cms.directive:uicCenterBlock
      *   @description
      *       директива для центрирования по вертикали и горизонтали контента внутри блока,
      *       которая гарантированно работает во всех браузерах
      *   @restrict EA
      *   @example
      *       <pre>
      *           //- pug
      *           .bg-primary(uic-center-block='', style="height: 10em;")
      *               span.bg-danger(style='padding: 5px;') Контент
      *       </pre>
      *       <example module="DocExampleApp">
      *           <file name="index.html">
      *               <div class='bg-primary' style='height: 10em;' uic-center-block=''>
      *                    <span class='bg-success' style='padding: 5px; color: black;'>Контент</span>
      *               </div>
      *           </file>
      *       </example>
       */
      restrict: 'EA',
      transclude: true,
      controller: function() {},
      template: "<div class='uic-center-block-inner' ng-transclude=''></div>"
    };
  });

}).call(this);
;
(function() {
  angular.module('ui.cms').directive('uicColsGrid', ["$cms", "H", function($cms, H) {
    var SIZE, chunkArray;
    SIZE = {
      1: '100',
      2: '50',
      3: '33-3',
      4: '25',
      5: '20'
    };
    chunkArray = function(arr, len) {
      var chunks, i, j, total;
      chunks = [];
      total = arr.length;
      i = 0;
      j = -1;
      while (i < total) {
        if (i % len === 0) {
          j += 1;
          chunks[j] = [];
        }
        chunks[j].push(i);
        i += 1;
      }
      return chunks;
    };
    return {

      /**
      *   @ngdoc directive
      *   @name ui.cms.directive:uicColsGrid
      *   @description
      *       директива, позволяющая генерировать сетку адаптивной верстки, на основе размеров колонок для разных размеров экранов.
      *       Предпочтительно использовать ее вместо flexbox из css, который не везде и криво поддерживается. В каждой сгенерированной клетке, есть ссылка на текущий элемент $item и $origScope.
      *       <b>Переменная $origScope - ссылка на оригинальный scope - это предположительная ссылка, и она может не указыать на правильный $scope</b>
      *   @restrict E
      *   @param {Array} items (ссылка) массив элементов, для которых строится сетка.
      *   @param {number=} lg (значение) количество колонок для размеров экрана lg (целое число)
      *   @param {number=} md (значение) количество колонок для размеров экрана md (целое число)
      *   @param {number=} sm (значение) количество колонок для размеров экрана sm (целое число)
      *   @param {number=} xs (значение) количество колонок для размеров экрана xs (целое число)
      *   @example
      *       <pre>
      *           $scope.myArray = [
      *               {value: "Hello"}
      *               {value: "World", value2: 33}
      *               {value: "!"}
      *               {value: "Some"}
      *               {value: "other"}
      *               {value: "text"}
      *               {value: "..."}
      *           ]
      *           $scope.sayHi = (val)->
      *               "hi, '#{val}'"
      *       </pre>
      *       <pre>
      *           //- pug
      *           uic-cols-grid.row(items="myArray", lg='4', md='3', sm='2', xs='1')
      *               .bg-primary(style='padding:5px; margin-bottom: 1em;')
      *                   p {{$origScope.sayHi($item.value)}}
      *                   hr
      *                   small Whole item = {{$item | json}}
      *       </pre>
      *       <example module="DocExampleApp">
      *           <file name="script.js">
      *               mkDocCtrl('uicColsGridCtrl', {
      *                   myArray: [
      *                       {value: "Hello"},
      *                       {value: "World", value2: 33},
      *                       {value: "!"},
      *                       {value: "Some"},
      *                       {value: "other"},
      *                       {value: "text"},
      *                       {value: "..."}
      *                   ],
      *                   sayHi: function(value){
      *                       return "hi, '" +value+ "'";
      *                   }
      *               });
      *           </file>
      *           <file name="index.html">
      *               <div ng-controller="uicColsGridCtrl">
      *                   <uic-cols-grid items="myArray" lg='4' md='3' sm='2' xs='1' class='row'>
      *                       <div class='bg-primary' style='padding:5px; margin-bottom: 1em;'>
      *                           <p>{{$origScope.sayHi($item.value)}}</p>
      *                           <hr/>
      *                           <small>Whole item = {{$item | json}}</small>
      *                       </div>
      *                   </uic-cols-grid>
      *               </div>
      *           </file>
      *       </example>
       */
      restrict: 'E',
      transclude: true,
      scope: {
        items: '=',
        lg: '@?',
        md: '@?',
        sm: '@?',
        xs: '@?'
      },
      controller: ["$scope", "$attrs", function($scope, $attrs) {
        var cols, e, k, l, len1, ref, v;
        $scope.$cms = $cms;
        $scope.$origScope = H.findOrigScope($scope, $attrs);
        $scope.colClass = '';
        cols = {};
        ref = ['lg', 'md', 'sm', 'xs'];
        for (l = 0, len1 = ref.length; l < len1; l++) {
          k = ref[l];
          try {
            v = parseInt($scope[k]) || 1;
          } catch (error) {
            e = error;
            v = 1;
          }
          cols[k] = v;
          $scope.colClass += " col-" + k + "-" + SIZE[v];
        }
        return $scope.$watchCollection('items', function(items, oldValue) {
          $scope.cache = {};
          if (angular.isEmpty(items)) {
            return;
          }
          for (k in cols) {
            v = cols[k];
            $scope.cache[k] = chunkArray(items, v);
          }
        });
      }],
      template: ('/client/cms_app/_directives/uicColsGrid/uicColsGrid.html', '<div class="uic-cols-grid-row" ng-repeat="itemsIndexes in cache[screenSize || $cms.screenSize]"><div class="clearfix"><div class="uic-cols-grid-item {{:: colClass}}" ng-repeat="index in itemsIndexes" uic-transclude="" uic-transclude-bind="{$item: items[index], $origScope: $origScope}"></div></div><div class="clearfix"></div></div>' + '')
    };
  }]);

}).call(this);
;
(function() {
  angular.module('ui.cms').directive('uicContact', function() {
    return {

      /**
      *   @ngdoc directive
      *   @name ui.cms.directive:uicContact
      *   @description
      *       директива для отображения L10nContact объектов(рендерит ссылки для почты, возможности позвонить по номеру). Обязательны поля value и type у ngModel
      *   @restrict EA
      *   @param {object} ngModel (ссылка) модель
      *   @param {string} ngModel.value контактные данные
      *   @param {string} ngModel.type тип контакта ['email', 'tel', 'skype', 'fax']
      *   @param {object=} ngModel.description (l10nObject) необязательное текстовое описание на разных языках
      *   @example
      *       <pre>
      *           $scope.contact = {
      *               value: '+3(000) 0000000',
      *               type: 'tel'
      *           }
      *       </pre>
      *       <pre>
      *           //- pug
      *           uic-contact(ng-model="contact")
      *       </pre>
      *       <example module="DocExampleApp">
      *           <file name="script.js">
      *               mkDocCtrl('uicContactCtrl', {
      *                   contact: {
      *                      value: '+3(000) 0000000',
      *                      type: 'tel'
      *                   }
      *               });
      *           </file>
      *           <file name="index.html">
      *               <div ng-controller="uicContactCtrl">
      *                 <uic-contact ng-model="contact"></uic-contact>
      *               </div>
      *           </file>
      *       </example>
       */
      restrict: 'EA',
      replace: true,
      scope: {
        ngModel: '='
      },
      controller: ["$scope", function($scope) {
        var getHref;
        getHref = function() {
          var tel;
          switch ($scope.ngModel.type) {
            case 'tel':
            case 'fax':
            case 'whatsapp-tel':
            case 'whatsapp':
              tel = $scope.ngModel.value;
              tel = tel.replace(/\s+/g, '').replace(/\(+/g, '').replace(/\)+/g, '').replace(/-/, '');
              return "tel:" + tel;
            case 'email':
              return "mailto:" + $scope.ngModel.value;
            case 'skype':
              return "skype:" + $scope.ngModel.value + "?call";
          }
          return $scope.ngModel.value;
        };
        return $scope.$watch('ngModel', function(ngModel) {
          $scope.href = getHref();
        }, true);
      }],
      template: ('/client/cms_app/_directives/uicContact/uicContact.html', '<a class="uic-contact" href="{{href}}" rel="nofollow"><span class="uic-contact-icon" ng-switch="ngModel.type"><span class="far fa-envelope" ng-switch-when="email"></span><span class="fas fa-phone" ng-switch-when="tel"></span><span class="fab fa-whatsapp" ng-switch-when="whatsapp"></span><span ng-switch-when="whatsapp-tel"><span class="fab fa-whatsapp"></span><span class="fas fa-phone"></span></span><span class="fab fa-skype" ng-switch-when="skype"></span><span class="fas fa-fax" ng-switch-when="fax"></span></span><span class="uic-contact-text">{{ngModel.value}}</span></a>' + '')
    };
  });

}).call(this);
;
(function() {
  angular.module('ui.cms').directive('uicDisabledOnCurrentUserPermission', function() {
    return {

      /**
      *   @ngdoc directive
      *   @name ui.cms.directive:uicDisabledOnCurrentUserPermission
      *   @description
      *
      *   @restrict E
      *   @param {string} uicDisabledOnCurrentUserPermission (значение) строка для $currentUser.resolvePermissionsString значение которой потом установит disabled в true/false для тега
      *   @example
      *       <pre>
      *           $scope.
      *       </pre>
      *       <pre>
      *           //-pug
      *       </pre>
      *       <pre>
      *       </pre>
       */
      restrict: 'A',
      controller: ["$element", "$attrs", "$currentUser", function($element, $attrs, $currentUser) {
        if (!$attrs.uicDisabledOnCurrentUserPermission) {
          return;
        }
        if ($attrs.ngDisabled) {
          console.error("uicDisabledOnCurrentUserPermission: You cant use ngDisabled with this directive! Ignoring permissions");
          return;
        }
        $attrs.$observe('uicDisabledOnCurrentUserPermission', function(value) {
          var result;
          result = $currentUser.resolvePermissionsString($attrs.uicDisabledOnCurrentUserPermission);
          if (result) {
            $element.attr('disabled', 'disabled');
          } else {
            $element.removeAttr('disabled');
          }
        });
      }]
    };
  });

}).call(this);
;
(function() {
  angular.module('ui.cms').directive('uicDisqusComments', function() {
    return {

      /**
      *   @ngdoc directive
      *   @name ui.cms.directive:uicDisqusComments
      *   @description
      *       виджет, отображающий кол-во комментариев в disqus
      *   @restrict E
      *   @param {string} identifier (значение) идентификатор треда
      *   @param {string=} url (значение) url страницы треда или текущая страница (без поисковой строки). По умолчанию: $location.absUrl().split('?')[0]
      *   @param {string=} title (значение)
      *   @example
      *       <pre>
      *           $scope.
      *       </pre>
      *       <pre>
      *           //- pug
      *       </pre>
      *       <example module="DocExampleApp">
      *           <file name="script.js">
      *               mkDocCtrl('uicDisqusCommentsCtrl', {
      *
      *               });
      *           </file>
      *           <file name="index.html">
      *               <div ng-controller="uicDisqusCommentsCtrl">
      *
      *               </div>
      *           </file>
      *       </example>
       */
      restrict: 'E',
      transclude: {
        uicDisqusCommentsBefore: '?uicDisqusCommentsBefore'
      },
      scope: {
        identifier: '@',
        url: '@?',
        title: '@?'
      },
      controller: ["$scope", "$element", "$cms", "$langPicker", "$window", "$location", "$rootScope", function($scope, $element, $cms, $langPicker, $window, $location, $rootScope) {
        var rmW, setConfig;
        setConfig = function() {
          if (!$scope.identifier) {
            throw "uicDisqusComments: identifier required!";
          }
          this.page.identifier = $scope.identifier;
          this.page.url = $scope.url || $location.absUrl().split('?')[0];
          this.language = $langPicker.currentLang;
          if ($scope.title) {
            this.page.title = $scope.title;
          }
          $element.addClass('visible');
        };
        if ($window.DISQUS) {
          $window.DISQUS.reset({
            reload: true,
            config: setConfig
          });
          return;
        }
        $scope.$cms = $cms;
        return rmW = $scope.$watch('$cms.seoService.integrations.disqus.active', function(isActive, oldValue) {
          if (!isActive) {
            return;
          }
          $window.disqus_config = setConfig;
          if (angular.isFunction($cms.seoService.integrations.disqus.onReady)) {
            $cms.seoService.integrations.disqus.onReady();
            rmW();
          }
        });
      }],
      template: ('/client/cms_app/_directives/uicDisqusComments/uicDisqusComments.html', '<div uic-transclude="uicDisqusCommentsBefore"></div><div id="disqus_thread"><a class="dsq-brlink" href="http://disqus.com"></a></div>' + '')
    };
  });

}).call(this);
;
(function() {
  angular.module('ui.cms').directive('uicDisqusCommentsCount', function() {
    var getWidgets;
    getWidgets = function() {
      var error;
      try {
        return DISQUSWIDGETS;
      } catch (error1) {
        error = error1;
        return null;
      }
    };
    return {

      /**
      *   @ngdoc directive
      *   @name ui.cms.directive:uicDisqusCommentsCount
      *   @description
      *       виджет, отображающий кол-во комментариев в disqus
      *   @restrict E
      *   @param {string} identifier (значение) идентификатор треда
      *   @param {string=} url (значение) url страницы треда или текущая страница (без поисковой строки). По умолчанию: $location.absUrl().split('?')[0]
      *   @param {string=} placeholder (значение) текст, который отображается по умолчанию (когда нет комментов или какие-то баги в обновлении)
      *   @example
      *       <pre>
      *           $scope.
      *       </pre>
      *       <pre>
      *           //- pug
      *       </pre>
      *       <example module="DocExampleApp">
      *           <file name="script.js">
      *               mkDocCtrl('uicDisqusCommentsCountCtrl', {
      *
      *               });
      *           </file>
      *           <file name="index.html">
      *               <div ng-controller="uicDisqusCommentsCountCtrl">
      *
      *               </div>
      *           </file>
      *       </example>
       */
      restrict: 'E',
      scope: {
        identifier: '@',
        placeholder: '@?'
      },
      controller: ["$scope", "$element", "$cms", "$location", function($scope, $element, $cms, $location) {
        var commentSpan, dw, rmW, updateCount;
        $scope.$cms = $cms;
        commentSpan = $element.find('span')[0];
        dw = getWidgets();
        updateCount = function() {
          if (!$scope.identifier) {
            throw "uicDisqusCommentsCount: identifier required!";
          }
          commentSpan.setAttribute('data-disqus-identifier', $scope.identifier);
          commentSpan.setAttribute('data-disqus-url', $scope.url || $location.absUrl().split('?')[0]);
          dw || (dw = getWidgets());
          if (dw) {
            dw.getCount({
              reset: true
            });
            $element.addClass('visible');
          } else {
            setTimeout(updateCount, 1000);
          }
        };
        if (dw) {
          updateCount();
          return;
        }
        return rmW = $scope.$watch('$cms.seoService.integrations.disqus.active', function(isActive, oldValue) {
          if (!isActive) {
            return;
          }
          if (angular.isFunction($cms.seoService.integrations.disqus.onReady)) {
            $cms.seoService.integrations.disqus.onReady(updateCount);
            rmW();
          }
        });
      }],
      template: ('/client/cms_app/_directives/uicDisqusCommentsCount/uicDisqusCommentsCount.html', '<div class="fas fa-comments"></div><span class="disqus-comment-count">{{placeholder}}</span>' + '')
    };
  });

}).call(this);
;
(function() {
  angular.module('ui.cms').directive('uicEnterPress', function() {
    return {

      /**
      *   @ngdoc directive
      *   @name ui.cms.directive:uicEnterPress
      *   @description директива для обработки события нажатия Enter внутри элемента
      *   @restrict A
      *   @param {function} uicEnterPress функция которая будет вызвана при наступлении события нажатии Enter внутри тега
      *   @example
      *       <pre>
      *           $scope.onEnterFunc = ()->
      *               alert 'Hello Enter!'
      *       </pre>
      *       <pre>
      *           //- pug
      *           textarea.form-control(uic-enter-press="onEnterFunc()")
      *       </pre>
      *       <example module="DocExampleApp">
      *           <file name="script.js">
      *               mkDocCtrl('uicEnterPressCtrl', {
      *                   onEnterFunc: function(){
      *                       alert('Hello Enter!')
      *                   }
      *               });
      *           </file>
      *           <file name="index.html">
      *               <div ng-controller="uicEnterPressCtrl">
      *                   <textarea class='form-control' uic-enter-press="onEnterFunc()" />
      *               </div>
      *           </file>
      *       </example>
       */
      restrict: 'A',
      link: function($scope, $element, attrs) {
        return $element.bind('keydown keypress', function(event) {
          if (event.which === 13) {
            $scope.$apply(function() {
              $scope.$eval(attrs.uicEnterPress);
            });
            return event.preventDefault();
          }
        });
      }
    };
  });

}).call(this);
;
(function() {
  angular.module('ui.cms').directive('uicFeedbackForm', function() {
    return {

      /**
      *   @ngdoc directive
      *   @name ui.cms.directive:uicFeedbackForm
      *   @description
      *       Форма обратной связи. Отправляет письмо админу с полями email, name, text через CmsMailNotification с идентификатором 'form.feedback' или указанным через параметры.
      *       <b>Форма поддерживает переопределение через transclude (см пример), при этом все свои переменные вы должны хранить в $form и поле email должно быть обязательным.</b>
      *       <b class='text-danger'>Не используйте несколько форм на 1 странице</b>
      *   @param {string=} [identifier='form.feedback'] (значение) идентификатор уведомления в бд из CmsMailNotification
      *   @param {object=} [defaultData={}] (ссылка) значения по умолчанию для формы
      *   @param {boolean=} [showPlaceholder=false] (значение) по умолчанию отобразится верстка с label рядом с input, если указать true, то будет отрендерены input-ы с placeholder-ами вместо label (см примеры)
      *   @restrict E
      *   @example
      *       <pre>
      *           //- pug
      *           // Внимание! Форма из документации никуда не отправится, будет только ошибка
      *           h3 Форма с label-ами
      *           uic-feedback-form
      *           h3 Форма с placeholder-ами
      *           uic-feedback-form(show-placeholder='true')
      *           h3 Форма с переопределенной версткой формы
      *           uic-feedback-form
      *               feedback-form
      *                   .form-group
      *                       label(translate) Email for custom form
      *                       input.form-control(ng-model="$formData.email", required, type='email')
      *                   .form-group
      *                       label(translate) My custom int value
      *                       input.form-control(ng-model="$formData.myInt", required, type='number', step='1')
      *                   .form-group
      *                       label(translate) Text for custom form
      *                       input.form-control(ng-model="$formData.text", required, type='text')
      *       </pre>
      *       <example module="DocExampleApp">
      *           <file name="script.js">
      *               mkDocCtrl('uicFeedbackFormCtrl', {
      *
      *               });
      *           </file>
      *           <file name="index.html">
      *               <div ng-controller="uicFeedbackFormCtrl" class='row'>
      *                   <div class='col-md-50'>
      *                       <h3>Форма с label-ами</h3>
      *                       <uic-feedback-form></uic-feedback-form>
      *                   </div>
      *                   <div class='col-md-50'>
      *                       <h3>Форма с placeholder-ами</h3>
      *                       <uic-feedback-form show-placeholder='true'></uic-feedback-form>
      *                   </div>
      *                   <div class='clearfix'></div>
      *                   <hr/>
      *                   <div class='col-md-50'>
      *                       <h3>Форма с переопределенной версткой формы</h3>
      *                       <uic-feedback-form>
      *                           <feedback-form>
      *                              <div class="form-group">
      *                                <label translate="translate">Email for custom form</label>
      *                                <input class="form-control" ng-model="$formData.email" required="required" type="email"/>
      *                              </div>
      *                              <div class="form-group">
      *                                <label translate="translate">My custom int value</label>
      *                                <input class="form-control" ng-model="$formData.myInt" required="required" type="number" step="1"/>
      *                              </div>
      *                              <div class="form-group">
      *                                <label translate="translate">Text for custom form</label>
      *                                <input class="form-control" ng-model="$formData.text" required="required" type="text"/>
      *                              </div>
      *                           </feedback-form>
      *                       </uic-feedback-form>
      *                   </div>
      *               </div>
      *           </file>
      *       </example>
       */
      restrict: 'E',
      transclude: {
        feedbackForm: '?feedbackForm'
      },
      scope: {
        identifier: '@?',
        defaultData: '=?',
        showPlaceholder: '@?',
        labelText: '@?'
      },
      controller: ["$scope", "$langPicker", "$cms", "CmsMailNotification", function($scope, $langPicker, $cms, CmsMailNotification) {
        var resetForm;
        $scope.sended = false;
        $scope.error = false;
        $scope.loading = false;
        $scope.formData = {};
        $scope.$cms = $cms;
        $scope._showPlaceholder = false;
        if ($scope.showPlaceholder === 'true') {
          $scope._showPlaceholder = true;
        }
        resetForm = function() {
          var k, ref, ref1, v;
          ref = $scope.formData;
          for (k in ref) {
            v = ref[k];
            delete $scope.formData[k];
          }
          ref1 = $scope.defaultData || {};
          for (k in ref1) {
            v = ref1[k];
            $scope.formData[k] = angular.copy(v);
          }
          $scope.error = false;
          return $scope.loading = false;
        };
        $scope.resetError = function() {
          return $scope.error = false;
        };
        $scope.resetForm = function() {
          resetForm();
          return $scope.sended = false;
        };
        $scope.sendForm = function() {
          var data, fn, k, params, ref, v;
          $scope.sended = false;
          $scope.error = false;
          $scope.loading = true;
          params = {
            identifier: $scope.identifier || 'form.feedback'
          };
          data = {
            $asFormData: false
          };
          ref = $scope.formData;
          for (k in ref) {
            v = ref[k];
            if ((v instanceof File) || (v instanceof FileList)) {
              data[k] = v;
            } else {
              data[k] = angular.copy(v);
            }
          }
          data.lang = $langPicker.currentLang;
          data.langName = $cms.LANG_MAP[data.lang];
          fn = 'send';
          for (k in data) {
            v = data[k];
            if (!(v && v instanceof File)) {
              continue;
            }
            if (CmsMailNotification.sendMultipart) {
              fn = 'sendMultipart';
              data.$asFormData = true;
            }
            break;
          }
          return CmsMailNotification[fn](params, data, function(data) {
            $scope.sended = true;
            $scope.successText = JSON.parse(JSON.stringify(data));
            return resetForm();
          }, function(data) {
            $scope.error = data;
            return $scope.loading = false;
          });
        };
        return resetForm();
      }],
      template: ('/client/cms_app/_directives/uicFeedbackForm/uicFeedbackForm.html', '<fieldset class="feedback-form" ng-disabled="loading"><form name="feedbackForm" ng-hide="sended || error"><div ng-switch="_showPlaceholder"><div uic-transclude="feedbackForm" uic-transclude-bind="{$formData: $parent.formData, $form: $parent.feedbackForm}" ng-switch-when="true"><div class="row"><div class="col-md-50"><div class="form-group"><input class="form-control" type="email" ng-model="formData.email" name="email" required="required" placeholder="{{\'Your email\' | translate}}"/></div></div><div class="col-md-50"><div class="form-group"><input class="form-control" type="text" ng-model="formData.name" name="name" required="required" placeholder="{{\'Your name\' | translate}}"/></div></div></div><div class="form-group"><textarea class="form-control" ng-model="formData.text" required="required" name="text" placeholder="{{ labelText || (\'Text\' | translate) }}"></textarea></div></div><div uic-transclude="feedbackForm" uic-transclude-bind="{$formData: $parent.formData, $form: $parent.feedbackForm}" ng-switch-when="false"><div class="row"><div class="col-md-50"><div class="form-group"><label><span translate="translate">Your email</span><span class="error" ng-show="feedbackForm.email.$error.required">[required]</span><span class="error" ng-show="feedbackForm.email.$error.email">[invalid email]</span></label><input class="form-control" type="email" ng-model="formData.email" name="email" required="required"/></div></div><div class="col-md-50"><div class="form-group"><label><span translate="translate">Your name</span><span class="error" ng-show="feedbackForm.name.$error.required">[required]</span></label><input class="form-control" type="text" ng-model="formData.name" name="name" required="required"/></div></div></div><div class="form-group"><label><span>{{ labelText || (\'Text\' | translate)}}</span><span class="error" ng-show="feedbackForm.text.$error.required">[required]</span></label><textarea class="form-control" ng-model="formData.text" required="required" name="text"></textarea></div></div></div><div class="feedback-form-submit-btn"><button class="btn btn-primary" type="submit" ng-disabled="feedbackForm.$invalid || loading" ng-click="sendForm()"><span translate="translate" ng-show="loading">Please wait</span><span translate="translate" ng-show="!loading">Submit</span></button></div></form></fieldset><div class="feedback-form-sended text-center" ng-if="sended"><h3>{{(successText | l10n) || (\'Thank you! Email was send\' | translate)}}</h3><button class="btn btn-default" ng-click="resetForm()" style="min-width:4em;"><span translate="">OK</span></button></div><div class="feedback-form-error text-center" ng-if="error"><h3 class="text-danger" translate="translate">Ooops!</h3><p class="text-danger" translate="translate">Cant submit form=(</p><button class="btn btn-default" ng-click="resetError()"><span translate="">Try again</span></button></div>' + '')
    };
  });

}).call(this);
;
(function() {
  angular.module('ui.cms').directive('uicFileThumb', ["H", function(H) {
    return {

      /**
      *   @ngdoc directive
      *   @name ui.cms.directive:uicFileThumb
      *   @description директива для отображения превью мультиязычных объектов(для картинок - превью, для остальных - иконка типа файла).
      *        Для правильного определения url картинки нужны глобальные переменные
      *       CMS_STORAGE.
      *       Подробности см.  {@link ui.cms описание модуля ui.cms}
      *   @restrict EA
      *   @param {object} file (ссылка) L10nAnyFile
      *   @param {string} size (значение) размер превью (напр.: '50','70x70', '120x100').
      *       Для картинок: Если размера нет в file[LANG].thumbs, то директива отобразит файл по defaultUrl.
      *   @param {boolean=} hideName (значение) скрывать ли оригинальное имя файла в превью. Только для не картинок.
      *   @example
      *       <pre>
      *           $scope.myCmsObject ={
      *             mainImg: {
      *               default: "en",
      *               id: null,
      *               ru: null,
      *               en: {
      *                 name: "en-5789de3dbb9a43029fb38021.jpg",
      *                 title: "praha",
      *                 description: "praha",
      *                 meta: {
      *                   size: 495656,
      *                   type: "image/jpeg",
      *                   originalName: "Малостранская_мостовая_башня_Карлова_моста_в_Праге.jpg",
      *                   width: 1200,
      *                   height: 797
      *                 },
      *                 thumbs: [
      *                   "70x70",
      *                   "350x350",
      *                   "440x250"
      *                  ]
      *               },
      *               cz: null,
      *               de: null,
      *               ua: null
      *             }
      *         }
      *       </pre>
      *       <pre>
      *           //- pug
      *           uic-file-thumb(file="myCmsObject.mainImg", size='70', default-url='/client/no-img.jpg')
      *       </pre>
       */
      restrict: 'E',
      scope: {
        file: '=',
        size: '@',
        hideName: '@?',
        forceCmsStorageType: '@?'
      },
      controller: ["$scope", function($scope) {
        var updateScope;
        updateScope = function() {
          var file;
          if (!$scope.file) {
            return;
          }
          file = $scope.file;
          $scope.isImage = H.l10nFile.getFileType(file, $scope.forceLang).includes('image/');
          if (!$scope.isImage) {
            $scope.pb = H.l10nFile.calcPadding($scope.size);
            $scope.iconClass = H.l10nFile.getFileTypeIcon(file, $scope.forceLang);
            $scope.originalName = H.l10nFile.getOriginalName(file, $scope.forceLang);
          }
        };
        return $scope.$watchGroup(['file', 'size'], updateScope);
      }],
      template: ('/client/cms_app/_directives/uicFileThumb/uicFileThumb.html', '<div ng-switch="isImage"><div class="uic-file-thumb-wrapper" ng-switch-when="false" style="padding-bottom:{{pb}}%;"><div class="data"><div class="cell"><span class="fas {{iconClass}}"></span><span class="title" ng-if="!hideName">{{originalName | cut:true:20}}</span></div></div></div><uic-picture ng-switch-when="true" file="file" size="{{size}}" force-cms-storage-type="{{forceCmsStorageType}}"></uic-picture></div>' + '')
    };
  }]);

}).call(this);
;
(function() {
  angular.module('ui.cms').directive('uicHref', ["H", "$http", function(H, $http) {
    var downloadDataAsFile;
    downloadDataAsFile = function(url, fileName) {
      var xhr;
      xhr = new XMLHttpRequest();
      xhr.open('GET', url, true);
      xhr.responseType = 'blob';
      xhr.onload = function() {
        H.downloadDataAsFile(fileName, xhr.response, {
          blob: true,
          mime: xhr.response.type
        });
      };
      xhr.send();
    };
    return {

      /**
      *   @ngdoc directive
      *   @name ui.cms.directive:uicHref
      *   @description
      *       устанавливает атрибут href для элемента согласно переданному l10nObject
      *   @restrict A
      *   @param {object} uicHref (ссылка) модель l10nObject
      *   @param {array<object>} uicHref (ссылка) массив моделей l10nObject
      *   @param {string|number=} fileId (значение) id l10nObject внутри массива (указывать только если uicHref является массивом)
       */
      restrict: 'A',
      link: function($scope, $element, attrs) {
        var id, l10nFile, uicHref;
        if (attrs.hasOwnProperty('fileId')) {
          id = parseInt(attrs.fileId);
          if (isNaN(id)) {
            id = $scope.$eval(attrs.fileId);
          }
          if (isNaN(id)) {
            return;
          }
        }
        uicHref = $scope.$eval(attrs.uicHref);
        if (id && angular.isArray(uicHref)) {
          l10nFile = uicHref.findByProperty('id', id);
        } else {
          l10nFile = uicHref;
        }
        if (!l10nFile || (!id && attrs.fileId)) {
          $element.addClass('ng-hide');
          return;
        }
        attrs.$set('href', H.l10nFile.getFileUrl(l10nFile));
        if (!$element.attr('download')) {
          $element.attr('download', H.l10nFile.getOriginalName(l10nFile));
        }
        if (!$element.attr('target') || $element.attr('uic-rel') === '') {
          $element.attr('target', '_self');
          $element.on('click', function(event) {
            event.preventDefault();
            downloadDataAsFile($element.attr('href'), $element.attr('download'));
          });
        }
      }
    };
  }]);

}).call(this);
;
(function() {
  angular.module('ui.cms').directive('uicIfOnCurrentUserPermission', ["$animate", "$compile", "$currentUser", function($animate, $compile, $currentUser) {
    var getBlockNodes;
    getBlockNodes = function(nodes) {
      var blockNodes, endNode, i, node;
      node = nodes[0];
      endNode = nodes[nodes.length - 1];
      blockNodes = void 0;
      i = 1;
      while (node !== endNode && (node = node.nextSibling)) {
        if (blockNodes || nodes[i] !== node) {
          if (!blockNodes) {
            blockNodes = jqLite(slice.call(nodes, 0, i));
          }
          blockNodes.push(node);
        }
        i++;
      }
      return blockNodes || nodes;
    };
    return {

      /**
      *   @ngdoc directive
      *   @name ui.cms.directive:uicIfOnCurrentUserPermission
      *   @description
      *
      *   @restrict E
      *   @param {string} uicIfOnCurrentUserPermission (значение) строка для $currentUser.resolvePermissionsString значение которой потом либо отрендерит кусок html либо нет
      *   @example
      *       <pre>
      *           $scope.
      *       </pre>
      *       <pre>
      *           //-pug
      *       </pre>
      *       <pre>
      *       </pre>
       */
      multiElement: true,
      transclude: 'element',
      priority: 600,
      terminal: true,
      restrict: 'A',
      $$tlb: true,
      link: function($scope, $element, $attrs, ctrl, $transclude) {
        var block, childScope, previousElements, renderInner, rmW;
        if (!$attrs.uicIfOnCurrentUserPermission) {
          return;
        }
        if ($attrs.ngIf && ($element[0] || {}).nodeType !== Node.COMMENT_NODE) {
          console.error("uicIfOnCurrentUserPermission: You cant use ngIf with this directive! Ignoring permissions");
          return;
        }
        block = null;
        childScope = null;
        previousElements = null;
        renderInner = function() {
          if ($currentUser.resolvePermissionsString($attrs.uicIfOnCurrentUserPermission)) {
            if (!childScope) {
              $transclude(function(clone, newScope) {
                childScope = newScope;
                clone[clone.length++] = $compile.$$createComment('end uicIfOnCurrentUserPermission', $attrs.ngIf);
                block = {
                  clone: clone
                };
                $animate.enter(clone, $element.parent(), $element);
              });
            }
            return;
          }
          if (previousElements) {
            previousElements.remove();
            previousElements = null;
          }
          if (childScope) {
            childScope.$destroy();
            childScope = null;
          }
          if (block) {
            previousElements = getBlockNodes(block.clone);
            $animate.leave(previousElements).done(function(response) {
              if (response !== false) {
                previousElements = null;
              }
            });
            block = null;
          }
        };
        $attrs.$observe('uicIfOnCurrentUserPermission', renderInner);
        rmW = $currentUser.onChange(renderInner);
        $scope.$on('$destroy', function() {
          rmW();
        });
      }
    };
  }]);

}).call(this);
;
(function() {
  angular.module('ui.cms').directive('uicIframe', function() {
    return {

      /**
      *   @ngdoc directive
      *   @name ui.cms.directive:uicIframe
      *   @description
      *
      *   @restrict E
      *   @param {object} ngModel (ссылка) модель
      *   @param {boolean=} ngDisabled (ссылка) флаг доступности тега
      *   @example
      *       <pre>
      *           $scope.
      *       </pre>
      *       <pre>
      *           //-pug
      *       </pre>
      *       <example module="DocExampleApp">
      *           <file name="script.js">
      *               mkDocCtrl('uicIframeCtrl', {
      *
      *               });
      *           </file>
      *           <file name="index.html">
      *               <div ng-controller="uicIframeCtrl">
      *
      *               </div>
      *           </file>
      *       </example>
       */
      restrict: 'E',
      require: 'ngModel',
      scope: {},
      controller: ["$scope", "$element", function($scope, $element) {
        var ngModelCtrl;
        ngModelCtrl = $element.controller('ngModel');
        return ngModelCtrl.$render = function() {
          var iFrame;
          iFrame = $element.find('iframe')[0];
          iFrame.setAttribute('src', URL.createObjectURL(new Blob([ngModelCtrl.$modelValue], {
            type: 'text/html'
          })));
          iFrame.onload = function() {
            var i, len, ref, timeout;
            iFrame.height = iFrame.contentWindow.document.body.scrollHeight + 10;
            if ($element.hasClass('nomargin')) {
              iFrame.contentDocument.body.innerHTML = iFrame.contentDocument.body.innerHTML + "<style>\n    body{\n        margin: 0 !important;\n        padding: 0 !important;\n    }\n</style>";
            }
            ref = [100, 1000, 2000];
            for (i = 0, len = ref.length; i < len; i++) {
              timeout = ref[i];
              setTimeout(function() {
                iFrame.height = iFrame.contentWindow.document.body.scrollHeight + 10;
              }, timeout);
            }
          };
        };
      }],
      template: ('/client/cms_app/_directives/uicIframe/uicIframe.html', '<iframe frameBorder="0"></iframe>' + '')
    };
  });

}).call(this);
;
(function() {
  angular.module('ui.cms').directive('uicNoItems', function() {
    return {

      /**
      *   @ngdoc directive
      *   @name ui.cms.directive:uicNoItems
      *   @description плашка с указанием о том, что объектов нет, или о том, что
      *       поиск по запросу/фильтрации не дал результатов.
      *       Предпологается, что плашка отображается когда действительно объектов нет.
      *       Варианты отображения:
      *       <ul>
      *           <li><b>searchTerm и isFiltered пустые или false</b> - уведомление о том, что объектов нет</li>
      *           <li><b>searchTerm не пустой, а isFiltered == false</b> - уведомление о том, что поиск по словам не дал результатов</li>
      *           <li><b>searchTerm не пустой, а isFiltered == true, или searchTerm пустой, а isFiltered == true</b> - уведомление о том, что поиск не дал результатов</li>
      *       </ul>
      *       Укажите функцию для сброса фильтра, и виджет отрисует кнопку сброса.
      *   @restrict E
      *   @param {string=} searchTerm (ссылка) текст по которому производится поиск
      *   @param {boolean=} isFiltered (ссылка) отфильтрованы ли объекты по каким-то другим критериям
      *   @param {function=} resetFilter (ссылка) функция, по которой можно проводить сброс фильтров
      *   @param {boolean=} ngDisabled (ссылка) флаг доступности тега
      *   @example
      *       <pre>
      *           $scope.mySearchTerm = ""
      *           $scope.mySearchTerm2 = "hello"
      *           $scope.myIsFiltered = false
      *           $scope.reset = ()->
      *               alert('Reset filter!')
      *       </pre>
      *       <pre>
      *           //- pug
      *           uice-no-items(search-term="mySearchTerm", is-filtered="myIsFiltered")
      *           uice-no-items(search-term="mySearchTerm2", is-filtered="myIsFiltered", reset-filter="reset()")
      *       </pre>
      *       <example module="DocExampleApp">
      *           <file name="script.js">
      *               mkDocCtrl('uicNoItemsCtrl', {
      *                   mySearchTerm: '',
      *                   mySearchTerm2: 'hello',
      *                   myIsFiltered: false,
      *                   reset: function(){
      *                       alert('Reset filter!')
      *                   }
      *               });
      *           </file>
      *           <file name="index.html">
      *               <div ng-controller="uicNoItemsCtrl">
      *                   <div class='row'>
      *                       <uice-no-items search-term='mySearchTerm', is-filtered="isFiltered" class='col-md-50'>
      *                       </uice-no-items>
      *                       <uice-no-items search-term='mySearchTerm2', is-filtered="isFiltered" reset-filter="reset()" class='col-md-50'>
      *                       </uice-no-items>
      *                   </div>
      *               </div>
      *           </file>
      *       </example>
       */
      restrict: 'E',
      scope: {
        searchTerm: '=?',
        isFiltered: '=?',
        resetFilter: '&?',
        ngDisabled: '=?'
      },
      controller: ["$scope", function($scope) {}],
      template: ('/client/cms_app/_directives/uicNoItems/uicNoItems.html', '<div ng-switch="!searchTerm &amp;&amp; !isFiltered"><div class="uic-no-items-content" ng-switch-when="true"><div class="fas fa-info-circle"></div><h3 translate="">There\'s nothing in here</h3></div><div class="uic-no-items-content" ng-switch-when="false"><div class="fas fa-frown-o"></div><h3 ng-switch="!!(searchTerm &amp;&amp; !isFiltered)"><span ng-switch-when="true"><span translate="">Your search - <b>"{{searchTerm}}"</b> - did not match any documents</span></span><span ng-switch-when="false" translate="">Your search did not match any documents</span></h3><div class="text-center" ng-show="resetFilter"><button class="btn btn-default" style="pointer-events: all;" ng-click="resetFilter()" ng-disabled="ngDisabled" translate="">Reset filter</button></div></div></div>' + '')
    };
  });

}).call(this);
;
(function() {
  angular.module('ui.cms').directive('uicPagination', function() {
    return {

      /**
      *   @ngdoc directive
      *   @name ui.cms.directive:uicPagination
      *   @description
      *       директива подобная uibPagination, только без кучи обязательных параметров.
      *       на экранах мобилок сворачивается в удобный дропдаун из которого можно
      *       выбрать необходимую страницу
      *   @restrict E
      *   @param {number} ngModel (ссылка) текущая страница (целое, положительное число)
      *   @param {number | string} totalPages (ссылка) общее количество страниц (целое, положительное число)
      *   @param {boolean=} ngDisabled (ссылка) флаг доступности тега
      *   @example
      *       <pre>
      *           $scope.page = 2
      *           $scope.totalPages = 7
      *       </pre>
      *       <pre>
      *           //- pug
      *           p Текущая страница - {{page}}
      *           uic-pagination(ng-model="page", total-pages="totalPages")
      *       </pre>
      *       <example module="DocExampleApp">
      *           <file name="script.js">
      *               mkDocCtrl('uicPaginationCtrl', {
      *                   page: 2,
      *                   totalPages: 7
      *               });
      *           </file>
      *           <file name="index.html">
      *               <div ng-controller="uicPaginationCtrl">
      *                   <p>Уменьшите по размеру окно, чтоб увидеть, как будет выглядеть виджет на мобильных экранах</p>
      *                   <p>Текущая страница - {{page}}</p>
      *                   <uic-pagination ng-model="page" total-pages="totalPages">
      *                   </uic-pagination>
      *               </div>
      *           </file>
      *       </example>
       */
      restrict: 'E',
      scope: {
        ngModel: '=',
        totalPages: '=',
        ngDisabled: '=?'
      },
      controller: ["$scope", "$filter", "$cms", function($scope, $filter, $cms) {
        var translate;
        translate = $filter('translate');
        $scope.getMaxSize = function() {
          if ($cms.screenSize === 'xs') {
            return 3;
          }
          if ($cms.screenSize === 'sm') {
            return 4;
          }
          return 10;
        };
        $scope.prevPage = function() {
          $scope.ngModel -= 1;
        };
        $scope.nextPage = function() {
          $scope.ngModel += 1;
        };
        $scope.getLabel = function(i) {
          return translate("Page") + " " + i;
        };
        return $scope.$watch('totalPages', function(totalPages, oldValue) {
          var i;
          totalPages = totalPages || 1;
          totalPages = Math.abs(parseInt(totalPages));
          if (totalPages < 1) {
            totalPages = 1;
          }
          $scope.$totalPages = totalPages;
          if ($scope.ngModel > totalPages) {
            $scope.ngModel = totalPages;
          }
          $scope.pages = [];
          i = 1;
          while (i <= totalPages) {
            $scope.pages.push(i);
            i += 1;
          }
        });
      }],
      template: ('/client/cms_app/_directives/uicPagination/uicPagination.html', '<div class="text-center" ng-hide="totalPages == 1 || !totalPages"><ul class="hidden-xs pagination-sm" uib-pagination="" total-items="$totalPages" ng-model="ngModel" max-size="getMaxSize()" items-per-page="1" ng-disabled="ngDisabled" boundary-link-numbers="true" rotate="false" next-text="{{\'Next\' | translate}}" previous-text="{{\'Previous\' | translate}}"></ul><div class="visible-xs-block"><div class="btn-group btn-group-uic-pagination"><button class="btn btn-default btn-sm" ng-disabled="ngModel == 1 || ngDisabled" ng-click="prevPage()">&laquo;</button><select class="form-control input-sm btn btn-default" ng-disabled="ngDisabled" ng-model="ngModel" ng-options="p as getLabel(p)   for p in pages"></select><button class="btn btn-default btn-sm" ng-disabled="ngModel == $totalPages || ngDisabled" ng-click="nextPage()">&raquo;</button></div></div></div>' + '')
    };
  });

}).call(this);
;
(function() {
  angular.module('ui.cms').directive('uicParallax', ["H", "$cms", "$rootScope", function(H, $cms, $rootScope) {
    return {

      /**
      *   @ngdoc directive
      *   @name ui.cms.directive:uicParallax
      *   @description
      *       директива, которая добавляет эффект параллакса, при этом если передать ссылку на L10nImage файл, то этот файл будет установлен как background-image у всего тега
      *   @restrict A
      *   @param {L10nFile|string|Array[L10nFile]} uicParallax (ссылка) картинка, которая будет использована в качестве фона. Можно указать как L10nImage, так и любой прямой url. Если в этом параметре указан массив L10nFile, то необходимо указать значение fileId для конкретного файла
      *   @param {number=} fileId (ссылка) id файла из массива файлов выше. <b>Указывать только если параметр uicParallax - массив файлов L10nFile</b>
      *   @param {string=} additionalBackgroundImage (значение) дополнительные css свойства
      *   @param {object=} screenSize (значение) размеры файлов, которые берутся из L10nFile свойства thumb для конкретного размера экрана. Пример: {'xs': '70x70', md: '350x350'}. Если это значение не указано, то берется прямой путь к файлу из L10nFile. <b>Указывать только если параметр uicParallax - файл L10nFile или массив файлов L10nFile</b>
      *   @example
      *       <pre>
      *           $scope.myFile = 'https://www.python.org/static/community_logos/python-logo-master-v3-TM.png'
      *       </pre>
      *       <pre>
      *           //- pug
      *           .bg-primary(uic-parallax="myFile", style="height: 10em;")
      *               br
      *               br
      *               br
      *               br
      *               br
      *               br
      *       </pre>
      *       <example module="DocExampleApp">
      *           <file name="script.js">
      *               mkDocCtrl('uicParralaxCtrl', {
      *                   myFile: 'https://www.python.org/static/community_logos/python-logo-master-v3-TM.png'
      *               });
      *           </file>
      *           <file name="index.html">
      *               <div ng-controller="uicParralaxCtrl">
      *                   <div class='col-md-50'>
      *                   <div class='bg-primary' uic-parallax="myFile">
      *                       <br/>
      *                       <br/>
      *                       <br/>
      *                       <br/>
      *                       <br/>
      *                       <br/>
      *                   </div>
      *                   </div>
      *                   <div class='clearfix'></div>
      *               </div>
      *           </file>
      *       </example>
       */
      restrict: 'A',
      link: function($scope, $element, $attrs) {
        var setBg;
        setBg = function(l10nFile) {
          var imgs, screenSize, url;
          if ($attrs.screenSize) {
            screenSize = $scope.$eval($attrs.screenSize) || {};
          }
          if (angular.isArray(l10nFile)) {
            l10nFile = l10nFile.findByProperty('id', $scope.$eval($attrs.fileId));
          }
          if (angular.isString(l10nFile)) {
            url = l10nFile;
          } else if (angular.isObject(l10nFile)) {
            if (screenSize && screenSize[$cms.screenSize]) {
              url = H.l10nFile.getFileThumbUrl(l10nFile, screenSize[$cms.screenSize]);
            } else {
              url = H.l10nFile.getFileUrl(l10nFile);
            }
          }
          if (url) {
            imgs = [];
            if ($attrs.additionalBackgroundImage) {
              imgs = [$attrs.additionalBackgroundImage];
            }
            imgs.push("url(" + url + ")");
            $element.css('background-image', imgs.join(', '));
          } else {
            $element.css('background-image', $attrs.additionalBackgroundImage || 'none');
          }
        };
        $scope.$watch($attrs.uicParallax, setBg, true);
        if ($attrs.fileId && ($attrs.fileId.indexOf('.') > -1 || isNaN(parseInt($attrs.fileId)))) {
          $scope.$watch($attrs.fileId, function() {
            setBg($scope.$eval($attrs.uicParallax));
          });
        }
        if ($attrs.screenSize) {
          $rootScope.$cms = $rootScope.$cms || $cms;
          $rootScope.$watch('$cms.screenSize', function() {
            setBg($scope.$eval($attrs.uicParallax));
          });
        }
      }
    };
  }]);

}).call(this);
;
(function() {
  var indexOf = [].indexOf || function(item) { for (var i = 0, l = this.length; i < l; i++) { if (i in this && this[i] === item) return i; } return -1; };

  angular.module('ui.cms').directive('uicPdfViewer', function() {
    return {

      /**
      *   @ngdoc directive
      *   @name ui.cms.directive:uicPdfViewer
      *   @description
      *
      *   @restrict E
      *   @param {object} ngModel (ссылка) модель
      *   @param {boolean=} ngDisabled (ссылка) флаг доступности тега
      *   @example
      *       <pre>
      *           $scope.
      *       </pre>
      *       <pre>
      *           //- pug
      *       </pre>
      *       <example module="DocExampleApp">
      *           <file name="script.js">
      *               mkDocCtrl('uicPdfViewerCtrl', {
      *
      *               });
      *           </file>
      *           <file name="index.html">
      *               <div ng-controller="uicPdfViewerCtrl">
      *
      *               </div>
      *           </file>
      *       </example>
       */
      restrict: 'E',
      require: 'ngModel',
      scope: {
        height: '@?'
      },
      controller: ["$scope", "$element", "$sce", "$timeout", "$location", "H", function($scope, $element, $sce, $timeout, $location, H) {
        var ngModelCtrl, renderDirective;
        ngModelCtrl = $element.controller('ngModel');
        $scope.fileUrl = '';
        $scope.googleViewerUrl = '';
        $scope.useGoogle = indexOf.call(window, 'ontouchstart') >= 0 || indexOf.call(window, 'onmsgesturechange') >= 0;
        if ($location.host() !== 'localhost') {
          $scope.useGoogle = true;
        }
        $scope.isReady = false;
        renderDirective = function() {
          var part, parts, url;
          url = ngModelCtrl.$modelValue;
          if (!url) {
            $scope.fileUrl = '';
            $scope.googleViewerUrl = '';
            $scope.isReady = false;
            return;
          }
          if (url.indexOf('?') === -1) {
            url += '?';
          }
          parts = url.split('?');
          part = parts[0].split('.');
          if (part[part.length - 1].toLowerCase() !== 'pdf') {
            parts[parts.length - 1] += '&autoDownload=false';
          }
          $scope.fileUrl = parts.join('?');
          $scope.googleViewerUrl = $sce.trustAsResourceUrl("https://docs.google.com/gview?embedded=true&url=" + (encodeURIComponent($scope.fileUrl)));
          $timeout(function() {
            return $scope.isReady = true;
          }, 300);
        };
        return ngModelCtrl.$render = function() {
          $scope.isReady = false;
          renderDirective();
        };
      }],
      template: ('/client/cms_app/_directives/uicPdfViewer/uicPdfViewer.html', '<div ng-switch="isReady" style="height: {{height || \'520px\'}};line-height:1;" ng-class="{\'with-preloader\': !isReady}"><div class="preloader" ng-switch-when="false"></div><div ng-switch-when="true" ng-switch="useGoogle" style="line-height:1;"><object class="w-100" ng-switch-when="false" data="{{:: fileUrl}}" style="height: {{height || \'520px\'}};line-height:1;" type="application/pdf"></object><iframe class="w-100" ng-switch-when="true" ng-src="{{::googleViewerUrl}}" style="height: {{height || \'520px\'}};line-height:1;" frameborder="0"></iframe></div></div>' + '')
    };
  });

}).call(this);
;

/*
    гадский s3 от aws не сразу, видно, применяет acl к файлу,
    из-за этого только что загруженная картинка по url отвечает 403.
    так что пытаемся ее прогрузить несколько раз пока либо не
    получится либо не истечет запас попыток.
 */

(function() {
  window.onImageLoadError = function(image) {
    var parent;
    image.style.display = 'none';
    image.attempt = image.attempt || 0;
    image.attempt += 1;
    if (image.attempt > 10) {
      return;
    }
    parent = image.parentElement;
    parent.children[0].style.display = 'block';
    setTimeout(function() {
      var url;
      url = image.src.split('?')[0];
      image.src = url + '?t=' + new Date().getTime();
    }, 550 + image.attempt * 10);
  };

  window.onImageLoadDone = function(image) {
    var parent;
    image.style.display = 'block';
    image.attempt = 0;
    parent = image.parentElement;
    parent.children[0].style.display = 'none';
  };

  angular.module('ui.cms').directive('uicPicture', ["H", "$filter", "$cms", "$langPicker", "gettextCatalog", function(H, $filter, $cms, $langPicker, gettextCatalog) {
    var getImageUrlAndPadding, l10nFilter, parseThumbSize;
    l10nFilter = $filter('l10n');
    parseThumbSize = function(size) {
      if (size.indexOf('x') === -1) {
        size = size + "x" + size;
      }
      size = size.split('x');
      return {
        width: parseInt(size[0]),
        height: parseInt(size[1])
      };
    };
    getImageUrlAndPadding = function(l10nFile, forceCmsStorageType, thumbSize) {
      var copyrightHtml, i, knownThumbs, len, padding, tSize, url, useObjectFit;
      useObjectFit = false;
      copyrightHtml = '';
      if (!l10nFile) {
        return {
          url: '',
          padding: 0,
          description: ''
        };
      }
      if (thumbSize) {
        knownThumbs = H.l10nFile.getFileThumbs(l10nFile);
        if (knownThumbs.includes(thumbSize)) {
          url = H.l10nFile.getFileThumbUrl(l10nFile, thumbSize);
        } else {
          thumbSize = parseThumbSize(thumbSize);
          for (i = 0, len = knownThumbs.length; i < len; i++) {
            tSize = knownThumbs[i];
            tSize = parseThumbSize(tSize);
            if (tSize.width >= thumbSize.width - 10 && tSize.height >= thumbSize.height - 10) {
              url = H.l10nFile.getFileThumbUrl(l10nFile, tSize.width + "x" + tSize.height);
              useObjectFit = true;
              break;
            }
          }
          if (!url) {
            url = H.l10nFile.getFileUrl(l10nFile, forceCmsStorageType);
            useObjectFit = true;
          }
        }
        padding = H.l10nFile.calcPadding(thumbSize);
      } else {
        url = H.l10nFile.getFileUrl(l10nFile, forceCmsStorageType);
        padding = H.l10nFile.calcPadding(l10nFile.meta);
        copyrightHtml = H.l10nFile.getCopyrightHtml(l10nFile);
      }
      if (!url) {
        return {
          url: '/client/brand/no-image.png',
          padding: 100,
          description: '',
          copyrightHtml: copyrightHtml
        };
      }
      return {
        useObjectFit: useObjectFit,
        url: url,
        padding: padding,
        title: l10nFile.title,
        description: l10nFile.description || l10nFile.title || l10nFile.meta.originalName,
        copyrightHtml: copyrightHtml
      };
    };
    return {

      /**
      *   @ngdoc directive
      *   @name ui.cms.directive:uicPicture
      *   @description
      *       директива, которая заменяет uicImg*. Должно быть указано ИЛИ <b>file</b> ИЛИ <b>fileList</b> и <b>fileId</b>
      *   @restrict E
      *   @param {L10nFile=} file (ссылка) объект файла
      *   @param {Array<L10nFile>=} fileList (ссылки) массив файлов.
      *   @param {number=} fileId (значение/ссылка) id файла из fileList
      *   @param {number|string=} size (значение) размер картинки (без указания загрузится оригинал). Можно указывать как размер квадрата "250", так и прямоугольником "440x300".
      *       Директива попытается подобрать оптимальную картинку из thumbs или если ее нет, то загрузит оригинал.
      *   @example
      *       <pre>
      *       </pre>
      *       <pre>
      *           //- pug
      *       </pre>
      *       <example module="DocExampleApp">
      *           <file name="script.js">
      *               mkDocCtrl('uicPictureCtrl', {
      *                   searchTerm: 'hello'
      *               });
      *           </file>
      *           <file name="index.html">
      *               <div ng-controller="uicSearchInputCtrl">
      *               </div>
      *           </file>
      *       </example>
       */
      restrict: 'E',
      scope: {
        file: '=?',
        fileList: '=?',
        fileId: '=?',
        forceCmsStorageType: '@?'
      },
      controller: ["$scope", "$attrs", "$element", function($scope, $attrs, $element) {
        var copyrightTag, currentFile, imgTag, pictureTag, updateScope;
        $scope.$cms = $cms;
        imgTag = $element.find('img')[0];
        pictureTag = $element.children()[0];
        copyrightTag = null;
        currentFile = null;
        updateScope = function() {
          var data;
          data = getImageUrlAndPadding(currentFile, $scope.forceCmsStorageType, $attrs.size);
          if (!data.url) {
            data.url = $attrs.defaultUrl || '/client/brand/no-image.png';
            data.padding = H.l10nFile.calcPadding($attrs.size) || 100;
          }
          pictureTag.style['padding-bottom'] = data.padding + "%";
          imgTag.src = l10nFilter(data.url);
          imgTag.setAttribute('title', l10nFilter(data.title));
          imgTag.alt = l10nFilter(data.description);
          if (data.useObjectFit) {
            imgTag.style['object-fit'] = 'cover';
          } else {
            imgTag.style['object-fit'] = '';
          }
          if (data.copyrightHtml) {
            if (!copyrightTag) {
              copyrightTag = document.createElement('picture-copyright');
              copyrightTag.className = 'uic-picture-copyright';
              imgTag.parentNode.appendChild(copyrightTag);
            }
            copyrightTag.innerHTML = data.copyrightHtml;
            pictureTag.classList.add('with-copyright');
          } else {
            pictureTag.classList.remove('with-copyright');
          }
        };
        if ($attrs.hasOwnProperty('file')) {
          $scope.$watchGroup(['file.id', '$cms.currentLang'], function() {
            currentFile = $scope.file;
            updateScope();
          });
        } else if ($attrs.hasOwnProperty('fileList')) {
          $scope.$watchCollection('fileList', function(fileList, oldValue) {
            currentFile = (fileList || []).findByProperty('id', $scope.fileId);
            updateScope();
          });
          $scope.$watch('$cms.currentLang', updateScope);
        }
      }],
      template: ('/client/cms_app/_directives/uicPicture/uicPicture.html', '<picture><span class="sk-cube-container-absolute"><span class="sk-cube-grid"><span class="sk-cube sk-cube1"></span><span class="sk-cube sk-cube2"></span><span class="sk-cube sk-cube3"></span><span class="sk-cube sk-cube4"></span><span class="sk-cube sk-cube5"></span><span class="sk-cube sk-cube6"></span><span class="sk-cube sk-cube7"></span><span class="sk-cube sk-cube8"></span><span class="sk-cube sk-cube9"></span></span></span><img onerror="window.onImageLoadError(this)" onload="window.onImageLoadDone(this)"/></picture>' + '')
    };
  }]);

}).call(this);
;
(function() {
  angular.module('ui.cms').directive('uicImgList', ["H", function(H) {
    var appendCopyrightHtml;
    appendCopyrightHtml = function($element, copyrightHtml) {
      var cssClass, pictureCopyright;
      cssClass = $element[0].className;
      $element[0].className = 'w-100';
      $element.wrap("<picture class='uic-img-list-wrapper with-copyright " + cssClass + "'></picture>");
      pictureCopyright = document.createElement('picture-copyright');
      pictureCopyright.innerHTML = copyrightHtml;
      $element[0].parentNode.appendChild(pictureCopyright);
    };
    return {
      restrict: 'A',
      controller: ["$scope", "$element", function($scope, $element) {
        var copyrightHtml, fileId, fileList, l10nFile;
        fileId = $element[0].getAttribute('file-id');
        if (!fileId) {
          return;
        }
        fileList = $element[0].getAttribute('file-list');
        if (fileList === 'item.files' && ($scope.item || {}).files) {
          fileList = $scope.item.files;
        } else if (fileList) {
          fileList = angular.getValue($scope, fileList);
        } else {
          return;
        }
        l10nFile = fileList.findByProperty('id', parseInt(fileId));
        if (!l10nFile) {
          return;
        }
        copyrightHtml = H.l10nFile.getCopyrightHtml(l10nFile);
        if (copyrightHtml) {
          appendCopyrightHtml($element, copyrightHtml);
        }
      }]
    };
  }]);

}).call(this);
;
(function() {
  angular.module('ui.cms').directive('uicScrollToTopButton', function() {
    return {

      /**
      *   @ngdoc directive
      *   @name ui.cms.directive:uicScrollToTopButton
      *   @description  директива для прокрутки страницы вверх. Укажите дополнительные
      *       классы 'bottom-right' или 'bottom-left' для того, чтоб отобразить кнопку с соответствующих сторон страницы
      *   @restrict E
      *   @param {string=} scrollTo (значение) как должна работать кнопка. По умолчанию - 'top'. Варианат 'top,bottom' чтоб кнопка была отображаема всегда
      *   @param {boolean=} ngDisabled (ссылка) флаг доступности тега
      *   @example
      *       <pre>
      *           //- pug
      *           uic-scroll-to-top-button.bottom-right
      *       </pre>
      *       <example module="DocExampleApp">
      *           <file name="index.html">
      *               <br/>
      *               <br/>
      *               <br/>
      *               <br/>
      *               <br/>
      *               <br/>
      *               <br/>
      *               <br/>
      *               <br/>
      *               <br/>
      *               <br/>
      *               <br/>
      *               <br/>
      *               <br/>
      *               <br/>
      *               <br/>
      *               <br/>
      *               <uic-scroll-to-top-button class='bottom-right'></uic-scroll-to-top-button>
      *           </file>
      *       </example>
       */
      restrict: 'E',
      transclude: true,
      scope: {
        scrollTo: '@?',
        ngDisabled: '=?'
      },
      controller: ["$scope", "$element", "$document", "$window", "$timeout", "$injector", function($scope, $element, $document, $window, $timeout, $injector) {
        var allowScrollToBottom, checkPosition, faIcon, getFaIcon, scrollDown, scrollUp, uiViewContainer;
        allowScrollToBottom = ($scope.scrollTo || '').indexOf('bottom') > -1;
        faIcon = null;
        uiViewContainer = angular.element($document[0].body).find('ui-view-animation-container')[0] || {};
        scrollUp = function() {
          if ($document.scrollTopAnimated) {
            $document.scrollTopAnimated(0);
          } else {
            window.smoothScrollTo(0);
            document.body.scrollTop = document.documentElement.scrollTop = 0;
          }
        };
        scrollDown = function() {
          var documentHeight;
          documentHeight = uiViewContainer.offsetHeight || document.body.offsetHeight;
          if ($document.scrollTopAnimated) {
            $document.scrollTopAnimated(documentHeight);
          } else {
            window.smoothScrollTo(documentHeight);
            document.body.scrollTop = document.documentElement.scrollTop = documentHeight;
          }
        };
        if (!allowScrollToBottom) {
          $scope.showBtn = false;
          checkPosition = function(apply) {
            var currentPosition, documentHeight, error, showBtn, windowHeight;
            documentHeight = uiViewContainer.offsetHeight || document.body.offsetHeight;
            windowHeight = window.innerHeight;
            currentPosition = document.documentElement.scrollTop || document.body.scrollTop;
            showBtn = true;
            if (currentPosition < windowHeight / 2) {
              showBtn = false;
            }
            if (documentHeight < windowHeight + 100) {
              showBtn = false;
            }
            if (currentPosition > documentHeight) {
              showBtn = true;
            }
            if (apply && $scope.showBtn !== showBtn) {
              $scope.$apply(function() {
                $scope.showBtn = showBtn;
              });
            } else {
              $scope.showBtn = showBtn;
            }
            try {
              if (jivo_api) {
                $element.addClass('with-jivosite');
              }
            } catch (error1) {
              error = error1;
              $element.removeClass('with-jivosite');
            }
          };
          $scope.onBtnClick = function() {
            if ($scope.ngDisabled) {
              return;
            }
            scrollUp();
          };
        } else {
          $scope.showBtn = true;
          getFaIcon = function() {
            var i, len, ref, span;
            if (faIcon) {
              return faIcon;
            }
            ref = $element.find('span');
            for (i = 0, len = ref.length; i < len; i++) {
              span = ref[i];
              if (!(span.className.indexOf('fa') > -1)) {
                continue;
              }
              faIcon = angular.element(span);
              break;
            }
            return faIcon;
          };
          checkPosition = function(apply) {
            var currentPosition, documentHeight, error, showBtn, windowHeight;
            documentHeight = uiViewContainer.offsetHeight || document.body.offsetHeight;
            windowHeight = window.innerHeight;
            currentPosition = document.documentElement.scrollTop || document.body.scrollTop;
            faIcon = getFaIcon();
            showBtn = true;
            if (!faIcon) {
              return;
            }
            if (windowHeight + 10 >= documentHeight) {
              showBtn = false;
            }
            if (currentPosition + windowHeight / 2 <= documentHeight / 2) {
              faIcon.addClass('fa-arrow-down');
              faIcon.removeClass('fa-arrow-up');
            } else {
              faIcon.addClass('fa-arrow-up');
              faIcon.removeClass('fa-arrow-down');
            }
            if (currentPosition > documentHeight) {
              showBtn = true;
            }
            if (apply && $scope.showBtn !== showBtn) {
              $scope.$apply(function() {
                $scope.showBtn = showBtn;
              });
            } else {
              $scope.showBtn = showBtn;
            }
            try {
              if (jivo_api) {
                $element.addClass('with-jivosite');
              }
            } catch (error1) {
              error = error1;
              $element.removeClass('with-jivosite');
            }
          };
          $scope.onBtnClick = function() {
            var currentPosition, documentHeight, windowHeight;
            if ($scope.ngDisabled) {
              return;
            }
            documentHeight = uiViewContainer.offsetHeight || document.body.offsetHeight;
            windowHeight = window.innerHeight;
            currentPosition = document.documentElement.scrollTop || document.body.scrollTop;
            if (currentPosition + windowHeight <= documentHeight / 2) {
              scrollDown();
            } else {
              scrollUp();
            }
          };
          if ($injector.has('$bulk')) {
            $injector.get('$bulk').onReady(function() {
              $timeout(function() {
                return checkPosition(true);
              }, 100);
            });
          }
          angular.element($window).bind("resize", function() {
            checkPosition(true);
          });
        }
        checkPosition();
        angular.element($window).bind("scroll", function() {
          checkPosition(true);
        });
      }],
      template: ('/client/cms_app/_directives/uicScrollToTopButton/uicScrollToTopButton.html', '<span ng-transclude="" ng-click="onBtnClick()" ng-show="showBtn"><div class="btn btn-primary animated slideInUp"><span class="fas fa-arrow-up"></span></div></span>' + '')
    };
  });

}).call(this);
;
(function() {
  angular.module('ui.cms').directive('uicSiteBrandA', function() {
    return {

      /**
      *   @ngdoc directive
      *   @name ui.cms.directive:uicSiteBrandA
      *   @description круглая кнопка с логотипом сайта.
      *       Поддерживаются: facebook, youtube, vk, odnoklassniki, twitter,
      *       tripadvisor, booking.com, aribnb.
      *   @restrict E
      *   @param {string} href (значение)
      *   @param {boolean=} [showText=false] (ссылка) отображать ли внутри тега href
      *   @param {string=} [target="_blank"] (значение) атрибут для тега <b>a</b>
      *   @example
      *       <pre>
      *           $scope.mySites = [
      *               "http://facebook.com/some-fake-page", "http://youtube.com/some-fake-page",
      *               "http://vk.com/some-fake-page", "http://odnoklassniki.ru/some-fake-page",
      *               "http://twitter.com/some-fake-page", "http://tripadvisor.com/some-fake-page",
      *               "http://booking.com/some-fake-page", "http://airbnb.com/some-fake-page",
      *               "http://instagram.com/some-fake-page", "http://unknown.com/some-fake-page"
      *           ]
      *       </pre>
      *       <pre>
      *           //- pug
      *           uic-site-brand-a(ng-repeat="site in mySites", href="{{site}}")
      *       </pre>
      *       <example module="DocExampleApp">
      *           <file name="script.js">
      *               mkDocCtrl('uicSiteBrandACtrl', {
      *                   mySites: ["http://facebook.com/some-fake-page", "http://youtube.com/some-fake-page", "http://vk.com/some-fake-page", "http://odnoklassniki.ru/some-fake-page", "http://twitter.com/some-fake-page", "http://tripadvisor.com/some-fake-page", "http://booking.com/some-fake-page", "http://airbnb.com/some-fake-page", "http://instagram.com/some-fake-page", "http://unknown.com/some-fake-page"]
      *               });
      *           </file>
      *           <file name="index.html">
      *               <div ng-controller="uicSiteBrandACtrl">
      *                   <uic-site-brand-a ng-repeat="site in mySites" href="{{site}}" style="margin-right: 10px;"></uic-site-brand-a>
      *               </div>
      *           </file>
      *       </example>
       */
      restrict: 'E',
      replace: true,
      transclude: true,
      scope: {
        href: '@',
        showText: '=?',
        target: '@?'
      },
      controller: ["$scope", function($scope) {
        $scope.getSiteBrandClass = function(url) {
          url = url.toLowerCase();
          if (url.indexOf('facebook.com/') > -1) {
            return 'facebook';
          }
          if (url.indexOf('youtube.com/') > -1) {
            return 'youtube';
          }
          if (url.indexOf('vk.com/') > -1 || url.indexOf('new.vk.com/') > -1 || url.indexOf('vkontakte.com/') > -1) {
            return 'vkontakte';
          }
          if (url.indexOf('odnoklassniki.ru/') > -1 || url.indexOf('ok.ru/') > -1) {
            return 'odnoklassniki';
          }
          if (url.indexOf('twitter.com/') > -1) {
            return 'twitter';
          }
          if (url.indexOf('tripadvisor') > -1) {
            return 'tripadvisor';
          }
          if (url.indexOf('booking.com/') > -1) {
            return 'booking';
          }
          if (url.indexOf('airbnb.com') > -1) {
            return 'airbnb';
          }
          if (url.indexOf('instagram.com/') > -1) {
            return 'instagram';
          }
          return 'missing';
        };
        $scope.getText = function() {
          if (!$scope.showText) {
            return '';
          }
          return $scope.href;
        };
        $scope.getHref = function(href) {
          if (href.indexOf('http://') !== 0 && href.indexOf('https://') !== 0) {
            return "https://" + href;
          }
          return href;
        };
        return $scope.getTarget = function() {
          if ($scope.target) {
            $scope.target;
          }
          return '_blank';
        };
      }],
      template: function($element, $attrs) {
        if ($attrs.showText === 'true') {
          return "<a target=\"{{getTarget()}}\" rel='noreferrer' ng-href='{{getHref(href)}}'>\n    <ul class='list-table vertical-align-middle nomargin nopadding w-auto'>\n        <li class='text-left'>\n            <span class='uic-site-brand-a-default uic-site-brand-a-{{getSiteBrandClass(href)}}'></span>\n        </li>\n        <li ng-transclude='' class='text-left'>\n            {{getText()}}\n        </li>\n    </ul>\n</a>";
        }
        return "<a target=\"{{getTarget()}}\" rel='noreferrer' ng-href='{{getHref(href)}}' class='uic-site-brand-a-default uic-site-brand-a-{{getSiteBrandClass(href)}}'>\n</a>";
      }
    };
  });

}).call(this);
;
(function() {
  angular.module('ui.cms').directive('uicState', function() {
    return {

      /**
      *   @ngdoc directive
      *   @name ui.cms.directive:uicState
      *   @description директива-обертка для легкого отображения состояний
      *       <span class="icon-ban-circle text-error" style="font-size: 1.2em;"></span>
      *        или
      *       <span class="icon-ok-sign text-success"  style="font-size: 1.2em;"></span>
      *       <br/>
      *       В основном используется в таблицах в админке для отображения состояния
      *       объекта (опубликован/неопубликован).
      *   @restrict E
      *   @param {boolean} state тип состояния (true - ok/success, false - fail/danger)
      *   @example
      *       <pre>
      *           $scope.myState = true
      *       </pre>
      *       <pre>
      *           //- pug
      *           uic-state(state="myState")
      *       </pre>
       */
      restrict: 'E',
      scope: {
        state: '=?'
      },
      controller: function() {},
      template: "<span class='far' ng-class=\"{'fa-times-circle text-danger': !state, 'fa-check-circle text-success': state}\">\n</span>"
    };
  });

}).call(this);
;
(function() {
  angular.module('ui.cms').directive('uicStickySidebar', ["H", "$uicIntegrations", function(H, $uicIntegrations) {
    var getStickySidebar;
    getStickySidebar = function(cb) {
      var error;
      try {
        cb(StickySidebar);
      } catch (error1) {
        error = error1;
        $uicIntegrations.getThirdPartyLibrary('sticky-sidebar').load().then(function() {
          cb(StickySidebar);
        });
      }
    };
    return {

      /**
      *   @ngdoc directive
      *   @name ui.cms.directive:uicStickySidebar
      *   @description
      *       обертка над плагином создающим прикрепленный сайдбар. Подробнее на https://github.com/abouolia/sticky-sidebar
      *   @restrict E
      *   @param {number=} [topSpacing=0] (значение) отступ сверху
      *   @param {number=} [bottomSpacing=0] (значение) отступ снизу
      *   @param {string=} containerSelector (значение) селектор контейнера, отностительно которого должен распологаться сайдбар
      *   @example
      *       <pre>
      *           //- pug
      *           #content-container
      *               .row
      *                   .col-md-30
      *                       uic-sticky-sidebar(container-selector='#content-container', top-spacing='60')
      *                           .bg-primary.clearfix(style="padding: 10px;")
      *                               h3 Sticky
      *                               p Hello sticky sidebar!
      *                   .col-md-70
      *                       .well
      *                           h1 Content
      *                           p Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
      *                           p Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
      *       </pre>
      *       <example module="DocExampleApp">
      *           <file name="script.js">
      *               mkDocCtrl('uicStickySidebarCtrl', {
      *
      *               });
      *           </file>
      *           <file name="index.html">
      *               <div ng-controller="uicStickySidebarCtrl">
      *                    <div id="content-container">
      *                      <div class="row">
      *                        <div class="col-md-30">
      *                          <uic-sticky-sidebar container-selector="#content-container" top-spacing='60'>
      *                            <div class="bg-primary clearfix" style="padding: 10px;">
      *                              <h3>Sticky</h3>
      *                              <p>Hello sticky sidebar!</p>
      *                            </div>
      *                          </uic-sticky-sidebar>
      *                        </div>
      *                        <div class="col-md-70">
      *                          <div class="well">
      *                            <h1>Content</h1>
      *                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
      *                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
      *                          </div>
      *                        </div>
      *                      </div>
      *                    </div>
      *               </div>
      *           </file>
      *       </example>
       */
      restrict: 'E',
      transclude: true,
      controller: ["$scope", "$element", "$attrs", "$timeout", "$document", function($scope, $element, $attrs, $timeout, $document) {
        var sticky;
        sticky = null;
        getStickySidebar(function(_StickySidebar) {
          $timeout(function() {
            var options;
            options = {
              topSpacing: H.convert.toInt($attrs.topSpacing, 0),
              bottomSpacing: H.convert.toInt($attrs.bottomSpacing, 0),
              innerWrapperSelector: '.uic-sticky-sidebar-content'
            };
            if ($attrs.containerSelector) {
              options.containerSelector = $attrs.containerSelector;
            }
            sticky = new _StickySidebar($element[0], options);
            $timeout(function() {
              sticky.updateSticky();
            });
          }, 300);
          $scope.$on('$destroy', function() {
            if (sticky) {
              sticky.destroy();
            }
          });
          return $scope.$watch(function() {
            return $document[0].body.offsetHeight;
          }, function(offsetHeight, oldValue) {
            if (sticky && offsetHeight !== oldValue) {
              sticky.updateSticky();
            }
          });
        });
      }],
      template: ('/client/cms_app/_directives/uicStickySidebar/uicStickySidebar.html', '<div class="uic-sticky-sidebar-content clearfix" ng-transclude=""></div>' + '')
    };
  }]);

}).call(this);
;
(function() {
  angular.module('ui.cms').directive('uicTransclude', ["$compile", function($compile) {
    return {
      restrict: 'EAC',
      link: function($scope, $element, $attrs, controller, $transclude) {
        var data, k, slotName, uicTranscludeCloneAttachFn, v;
        uicTranscludeCloneAttachFn = function(clone) {
          if (clone.length) {
            $element.empty();
            $element.append(clone);
          }
        };
        if ($attrs.uicTransclude === $attrs.$attr.uicTransclude) {
          $attrs.uicTransclude = '';
        }
        if (!$transclude) {
          throw 'Illegal use of uicTransclude directive in the template! No parent directive that requires a transclusion found.';
        }
        slotName = $attrs.uicTransclude || $attrs.uicTranscludeSlot;
        if ($attrs.uicTranscludeBind) {
          data = $scope.$eval($attrs.uicTranscludeBind);
          for (k in data) {
            v = data[k];
            if (!$scope.hasOwnProperty(k)) {
              $scope[k] = v;
            }
          }
        }
        $transclude($scope, uicTranscludeCloneAttachFn, null, slotName);
      }
    };
  }]);

}).call(this);
;
(function() {
  angular.module('ui.cms').filter('trustHtml', ["$sce", function($sce) {
    return function(value, type) {
      console.warn("trustHtml: deprecated filter!");
      return $sce.trustAsHtml(value);
    };
  }]).filter('trustSrc', ["$sce", function($sce) {
    return function(value, type) {
      console.warn("trustSrc: deprecated filter!");
      return $sce.trustAsResourceUrl(value);
    };
  }]).filter('toUrlTitle', ["$filter", function($filter) {
    var l10nFilter;
    l10nFilter = $filter('l10n');
    return function(l10nObject, forceLang) {
      var title;
      console.warn("toUrlTitle: deprecated filter! Use toUrlId filter");
      if (angular.isString(l10nObject)) {
        title = l10nObject;
      } else {
        title = l10nFilter(l10nObject, forceLang);
      }
      if (!title) {
        return '-';
      }
      title = title.replaceAll(' ', '-').replaceAll('!', '.').replaceAll('/', '-').replaceAll('?', '-').replaceAll(',', '').replaceAll('&', '-').replaceAll('"', '').replaceAll("'", '').toLowerCase();
      if (!title) {
        return '-';
      }
      return title;
    };
  }]).filter('orderByIds', function() {

    /**
    *   ngdoc filter
    *   name ui.cms.filter:orderByIds
    *   description фильтр упорядочивает массив элементов по списку ids
    *   param {Array<Object>} items  массив элементов
    *   param {Array<string> | Array<number>} ids  массив идентификаторов
    *   returns {Array<Object>} упорядоченнный массив
    *   example
    *       <pre>
    *           $scope.items = [
    *               {id: '1'}
    *               {id: '2'}
    *               {id: '3'}
    *               {id: '4'}
    *               {id: '5'}
    *           ]
    *           $scope.myIds = ['4', '2']
    *       </pre>
    *       <pre>
    *           //- pug
    *           p(ng-repeat="item in items | orderByIds: myIds") {{item.id}}
    *       </pre>
    *       Вывод:
    *       <pre>
    *            <p>4</p>
    *            <p>2</p>
    *            <p>1</p>
    *            <p>3</p>
    *            <p>5</p>
    *       </pre>
    *
     */
    return function(items, ids) {
      var _items, i, item, j, len;
      console.warn("orderByIds: deprecated filter!");
      if (!items || !items.length) {
        return items;
      }
      if (!ids || !ids.length) {
        return items;
      }
      _items = new Array(ids.length);
      for (j = 0, len = items.length; j < len; j++) {
        item = items[j];
        i = ids.indexOf(item.id);
        if (item.id && i > -1 && !_items[i]) {
          _items[i] = item;
        } else {
          _items.push(item);
        }
      }
      i = ids.length - 1;
      while (i !== -1) {
        if (!angular.isDefined(_items[i])) {
          _items.splice(i, 1);
        }
        i -= 1;
      }
      return _items;
    };
  });

}).call(this);
;
(function() {
  angular.module('ui.cms').factory('linker', function() {
    var links;
    links = {};
    return function(arr, key) {
      var err, link;
      if (!key) {
        try {
          key = btoa(String.fromCharCode.apply(null, arr));
        } catch (error) {
          err = error;
          key = arr.toString();
        }
      }
      link = links[key] || [];
      arr.forEach(function(newItem, index) {
        var oldItem;
        oldItem = link[index];
        if (!angular.equals(oldItem, newItem)) {
          link[index] = newItem;
        }
      });
      link.length = arr.length;
      return links[key] = link;
    };
  }).filter('chunk', ["linker", function(linker) {

    /**
    *   @ngdoc filter
    *   @name ui.cms.filter:chunk
    *   @description фильтр разбивает массив объектов на несколько массивов размером size-объектов
    *   @param {Array} array  массив объектов
    *   @param {number} size  кол-во элементов в новых массивах
    *   @param {string=} [mode="horiz"]  тип деления - "horiz" или "vert"
    *   @returns {Array} массив массивов объектов
    *   @example
    *   <pre>
    *        $scope.myArray = [{i:1}, {i:2}, {i:3}, {i:4}, {o:5}];
    *   </pre>
    *   <pre>
    *       //- pug
    *       div(ng-repeat="items in myArray | chunk:3")
    *          p(ng-repeat="item in items") {{item}}
    *   </pre>
    *   Вывод:
    *   <pre>
    *       <div>
    *           <p>{i:1}</p>
    *           <p>{i:2}</p>
    *           <p>{i:3}</p>
    *       </div>
    *       <div>
    *           <p>{i:4}</p>
    *           <p>{o:5}</p>
    *       </div>
    *   </pre>
     */
    return function(array, size, mode, skip_linker) {
      var i, items_per_chunk, newArr;
      if (!array || !array.length) {
        return array;
      }
      if (!size || size < 1) {
        return linker([array]);
      }
      if (mode !== 'horiz' && mode !== 'vert') {
        mode = 'horiz';
      }
      newArr = [];
      if (mode === 'horiz') {
        i = 0;
        while (i < array.length) {
          newArr.push(array.slice(i, i + size));
          i += size;
        }
      } else if (mode === 'vert') {
        items_per_chunk = Math.floor(array.length / size);
        if (items_per_chunk !== array.length / size) {
          items_per_chunk += 1;
        }
        i = 0;
        while (i < size) {
          newArr.push(array.slice(i * items_per_chunk, i * items_per_chunk + items_per_chunk));
          i += 1;
        }
      }
      if (skip_linker) {
        return newArr;
      }
      return linker(newArr);
    };
  }]);

}).call(this);
;
(function() {
  angular.module('ui.cms').filter('constantToText', ["$constants", "$injector", function($constants, $injector) {
    var l10n, translate;
    l10n = null;
    translate = null;

    /**
    *   @ngdoc filter
    *   @name ui.cms.filter:constantToText
    *   @description фильтр переводит константное значение (определенное через $constantsProvider) в текст. Если найденное значение - объект, то он будет пропущен
    *       через l10n фильтр, если значение - строка, то через фильтр translate
    *   @param {any} value  значение
    *   @param {string} constantPath  полный путь для константы в $constants
    *   @returns {string} текст (или любое другое значение)
    *   @example
    *       <pre>
    *           $scope.order = {status: 'n'}
    *       </pre>
    *       <pre>
    *           //- pug
    *           p {{order.status | constantToText: 'CMS_ORDER.status'}}
    *       </pre>
    *       Вывод:
    *       <pre>
    *            <p>Новый</p>
    *       </pre>
    *
     */
    return function(value, constantPath) {
      var $filter, i, item, len, ref;
      if (!constantPath) {
        return '';
      }
      if (!l10n) {
        $filter = $injector.get('$filter');
        l10n = $filter('l10n');
        translate = $filter('translate');
      }
      ref = $constants.resolve(constantPath) || [];
      for (i = 0, len = ref.length; i < len; i++) {
        item = ref[i];
        if (item.value === value) {
          if (angular.isString(item.description)) {
            return translate(item.description);
          }
          return l10n(item.description);
        }
      }
      if (!angular.isDefined(value)) {
        return '';
      }
      return value;
    };
  }]);

}).call(this);
;
(function() {
  angular.module('ui.cms').filter('countryCodeToText', ["$countries", "$injector", function($countries, $injector) {
    $countries = null;

    /**
    *   @ngdoc filter
    *   @name ui.cms.filter:countryCodeToText
    *   @description
    *        возвращает название страны по ее 2-х значному коду
    *   @param {string} countryCode код страны. Например: 'ua', 'de'
    *   @param {string=} forceLang язык, на котором нужно отобразить название страны. Если не указано, то будет взят текущий язык пользователя из $langPicker.currentLang
    *   @returns {string} название страны или "Invalid country code"
    *   @example
    *       <pre>
    *           $scope.country = 'ua'
    *       </pre>
    *       <pre>
    *           //- pug
    *           p {{country | countryCodeToText}}
    *       </pre>
    *       Вывод:
    *       <pre>
    *            <p>Украина</p>
    *       </pre>
    *
     */
    return function(countryCode, forceLang) {
      if (!countryCode) {
        return '';
      }
      $countries = $countries || $injector.get('$countries');
      return $countries.codeToText(countryCode, forceLang);
    };
  }]);

}).call(this);
;
(function() {
  angular.module('ui.cms').filter('cut', function() {

    /**
    *   @ngdoc filter
    *   @name ui.cms.filter:cut
    *   @description фильтр красиво обрезает строку в размер, добавляя в конец троеточие
    *   @param {string} value  текст
    *   @param {boolean} wordwise  если true, то фильтр будет бить строку по словам, а не просто отрывать по символ
    *   @param {number} max  максимальное кол-во символов для форматирования
    *   @param {string=} [tail="..."]  текст который следует добавлять в конец строки после обрезания
    *   @returns {string} обрезанный текст
    *   @example
    *       <pre>
    *           $scope.myText = "Hello my world!";
    *       </pre>
    *       <pre>
    *           //- pug
    *           p {{myText | cut:true:5}}
    *       </pre>
    *       Вывод:
    *       <pre>
    *            <p>Hello...</p>
    *       </pre>
    *
     */
    return function(value, wordwise, max, tail) {
      var lastspace;
      if (!value) {
        return '';
      }
      max = parseInt(max, 10);
      if (!max) {
        return value;
      }
      if (value.length <= max) {
        return value;
      }
      value = value.substr(0, max);
      if (wordwise) {
        lastspace = value.lastIndexOf(' ');
        if (lastspace !== -1) {
          value = value.substr(0, lastspace);
        }
      }
      if (!angular.isDefined(tail)) {
        return value + '…';
      }
      return value + (tail || '');
    };
  });

}).call(this);
;
(function() {
  angular.module('ui.cms').filter('duration', ["gettextCatalog", "H", function(gettextCatalog, H) {
    var TEXT, TEXT_TYPES, duration, translate;
    TEXT = {
      medium: {
        singular: {
          day: 'day',
          hour: 'hour',
          minute: 'minute',
          second: 'second'
        },
        plural: {
          day: 'days',
          hour: 'hours',
          minute: 'minutes',
          second: 'seconds'
        }
      },
      short: {
        singular: {
          day: 'd',
          hour: 'h',
          minute: 'm',
          second: 's'
        }
      }
    };
    TEXT_TYPES = {
      'medium': 'medium duration text',
      'short': 'small duration text'
    };
    translate = function(value, time_type, format) {
      var T, translateContext;
      if (format === 'short-dot') {
        format = 'short';
      }
      T = TEXT[format];
      translateContext = TEXT_TYPES[format];
      return gettextCatalog.getPlural(value || 0, T.singular[time_type], (T.plural || T.singular)[time_type], {}, TEXT_TYPES[format]);
    };
    duration = function(value, format) {

      /**
      *   @ngdoc filter
      *   @name ui.cms.filter:duration
      *   @description
      *   Formats `duration` to a humanized string based on the requested `format`.
      *
      *   `format` string can be one of the following predefined formats:
      *
      *   * `'medium'`
      *   * `'short''
      *   * `'short-dot''
      *
      *   @param {string} duration Duration in format '[DD] [HH:[MM:]]ss[.uuuuuu]'  More info: https://docs.djangoproject.com/en/2.0/_modules/django/db/models/fields/#DurationField
      *   @param {string=} format Formatting rules (see Description). If not specified,
      *    `medium` is used.
      *   @returns {string} Formatted string
      *   @example
      *       <pre>
      *           $scope.duration1 = "09:10:30"
      *       </pre>
      *       <pre>
      *           //- pug
      *           p {{duration1 | duration: 'short'}}
      *       </pre>
      *       Вывод:
      *       <pre>
      *           <p>9 hours, 10 minutes, 30 seconds</p>
      *       </pre>
       */
      var text;
      format = format || 'medium';
      if (!['medium', 'short', 'short-dot'].includes(format)) {
        format = 'medium';
      }
      duration = H.duration.parse(value || '');
      text = [];
      ['day', 'hour', 'minute', 'second'].forEach(function(time_type) {
        if (duration[time_type + "s"]) {
          return text.push(duration[time_type + "s"] + " " + translate(duration[time_type + "s"], time_type, format));
        }
      });
      if (!text.length) {
        text.push("0 " + translate(0, 'second', format));
      }
      if (format === 'short-dot') {
        return text.join('., ');
      }
      return text.join(', ');
    };
    duration.$stateful = true;
    return duration;
  }]);

}).call(this);
;
(function() {
  angular.module('ui.cms').filter('fromLangCodeToLangName', ["$cms", function($cms) {
    return function(value) {
      if (!value) {
        return;
      }
      return $cms.LANG_MAP[value];
    };
  }]);

}).call(this);
;
(function() {
  angular.module('ui.cms').filter('humanBytes', function() {

    /**
    *   @ngdoc filter
    *   @name ui.cms.filter:humanBytes
    *   @description фильтр отображает размер файла в b, kb, mb, tb, pb
    *   @param {number} value  значение размера
    *   @param {number=} [precision=0]  количество знаков после запятой
    *   @returns {string} отформатированный текст
    *   @example
    *   <pre>
    *        $scope.mySize1 = 43128
    *        $scope.mySize2 = 73850656
    *   </pre>
    *   <pre>
    *       //- pug
    *       p {{mySize1 | humanBytes}}
    *       p {{mySize2 | humanBytes}}
    *       p {{mySize2 | humanBytes:2}}
    *   </pre>
    *   Вывод:
    *   <pre>
    *       <p>42 kB</p>
    *       <p>70 MB</p>
    *       <p>70.43 MB</p>
    *   </pre>
     */
    return function(value, precision) {
      var number, units;
      if (!value || value === 0) {
        return '0 b';
      }
      if (isNaN(parseFloat(value)) || !isFinite(value)) {
        return '-';
      }
      if (!precision) {
        precision = 0;
      }
      units = ['bytes', 'kB', 'MB', 'GB', 'TB', 'PB'];
      number = Math.floor(Math.log(value) / Math.log(1024));
      return (value / Math.pow(1024, Math.floor(number))).toFixed(precision) * 1 + ' ' + units[number];
    };
  });

}).call(this);
;
(function() {
  var useAutoConverter;

  useAutoConverter = false;

  angular.module('ui.cms').provider('humanCurrency', function() {
    this.setAutoconvertUsage = function(value) {
      useAutoConverter = !!value;
      return this;
    };
    this.$get = [angular.noop];
    return this;
  }).filter('humanCurrency', ["$filter", "$cms", "H", function($filter, $cms, H) {

    /**
    *   @ngdoc filter
    *   @name ui.cms.filter:humanCurrency
    *   @description фильтр форматирует суммы с знаком валюты
    *   @param {number} value  значение суммы
    *   @param {string} currencySymbol  валюта - 'EUR', 'CZK', 'USD' и т.д.
    *   @param {number=} [fractionSize=0] количество знаков после запятой в сумме, которые следует отображать. Если сумма представляет собой целое число, то этот параметр проигнорируется
    *   @returns {string} Отформатированный текст
    *   @example
    *       <pre>
    *           $scope.myValue = 200.18
    *           $scope.myCurrency = 'USD'
    *       </pre>
    *       <pre>
    *           //- pug
    *           p {{myValue | humanCurrency: 'EUR'}}
    *           p {{myValue | humanCurrency: myCurrency: 2}}
    *       </pre>
    *       Вывод:
    *       <pre>
    *            <p>200 €</p>
    *            <p>200.18 $</p>
    *       </pre>
    *
     */
    var currencyFilter, getCurrencyInfo, humanCurrency;
    currencyFilter = $filter('currency');
    getCurrencyInfo = function(currencySymbol) {
      var _from, _to, converter, k_from, k_to;
      if (!useAutoConverter || !$cms.settings.$preferredCurrency || currencySymbol === $cms.settings.$preferredCurrency) {
        return {
          symbol: currencySymbol,
          k: 1
        };
      }
      converter = $cms.settings.defaultCurrencyConverter || {};
      _from = (currencySymbol || "").toUpperCase();
      _to = $cms.settings.$preferredCurrency.toUpperCase();
      k_from = (converter[$cms.settings.defaultCurrency] || {})[_from];
      k_to = (converter[$cms.settings.defaultCurrency] || {})[_to];
      if (!k_from || !k_to) {
        return {
          symbol: currencySymbol,
          k: 1
        };
      }
      return {
        symbol: $cms.settings.$preferredCurrency,
        k: 1 / k_from * k_to
      };
    };
    humanCurrency = function(value, currencySymbol, fractionSize) {
      var currencyInfo, data;
      if (!angular.isDefined(value) || value === null) {
        return value;
      }
      currencyInfo = getCurrencyInfo(currencySymbol);
      value = value * currencyInfo.k;
      switch (currencyInfo.symbol) {
        case 'CZK':
          currencySymbol = 'Kč';
          break;
        case 'USD':
          currencySymbol = '$';
          break;
        case 'EUR':
          currencySymbol = '€';
          break;
        case 'RUB':
          currencySymbol = '₽';
      }
      if (value % 1 === 0 && !angular.isDefined(fractionSize)) {
        fractionSize = 0;
      }
      data = currencyFilter(value, '', fractionSize);
      if (currencySymbol === '$') {
        return '$' + data;
      } else {
        return data.trim() + ' ' + currencySymbol;
      }
    };
    humanCurrency.$stateful = true;
    return humanCurrency;
  }]);

}).call(this);
;
(function() {
  angular.module('ui.cms').filter('humanDatetime', ["gettextCatalog", "H", function(gettextCatalog, H) {
    var TEXT, TEXT_TYPES, humanDatetime, translate;
    TEXT = {
      medium: {
        singular: {
          year: 'year',
          month: 'month',
          day: 'day',
          hour: 'hour',
          minute: 'minute',
          second: 'second'
        },
        plural: {
          year: 'years',
          month: 'months',
          day: 'days',
          hour: 'hours',
          minute: 'minutes',
          second: 'seconds'
        }
      },
      short: {
        singular: {
          years: 'y',
          month: 'm',
          day: 'd',
          hour: 'h',
          minute: 'm',
          second: 's'
        }
      }
    };
    TEXT_TYPES = {
      'medium': 'medium duration text',
      'short': 'small duration text'
    };
    translate = function(value, time_type, format) {
      var T, translateContext;
      if (format === 'short-dot') {
        format = 'short';
      }
      T = TEXT[format];
      translateContext = TEXT_TYPES[format];
      return Math.floor(value) + ' ' + gettextCatalog.getPlural(parseInt(value || 0), T.singular[time_type], (T.plural || T.singular)[time_type], {}, TEXT_TYPES[format]);
    };
    humanDatetime = function(value, format) {

      /**
      *   @ngdoc filter
      *   @name ui.cms.filter:humanDatetime
      *   @description
      *   Formats `Date` to a humanized string based on the requested `format`.
      *
      *   `format` string can be one of the following predefined formats:
      *
      *   * `'medium'`
      *   * `'short''
      *   * `'short-dot''
      *
      *   @param {string} date Date object or string
      *   @param {string=} format Formatting rules (see Description). If not specified,
      *    `medium` is used.
      *   @returns {string} Formatted string
      *   @example
      *       <pre>
      *           $scope.day = new Date("2019-06-30T15:28:20.759Z")
      *       </pre>
      *       <pre>
      *           //- pug
      *           p {{day | humanDatetime: 'short'}}
      *       </pre>
      *       Вывод:
      *       <pre>
      *           <p>9 hours</p>
      *       </pre>
       */
      var duration, suffix;
      value = new Date(value);
      if (isNaN(value.getTime())) {
        return '';
      }
      format = format || 'medium';
      if (!['medium', 'short', 'short-dot'].includes(format)) {
        format = 'medium';
      }
      duration = (new Date() - value) / 1000;
      if (duration <= 1) {
        return gettextCatalog.getString('now');
      }
      suffix = '';
      if (format === 'short-dot') {
        suffix = '.';
      }
      if (duration < 60) {
        return translate(duration, 'second', format) + suffix;
      }
      if (duration < 60 * 60) {
        return translate(duration / 60, 'minute', format) + suffix;
      }
      if (duration < 60 * 60 * 24) {
        return translate(duration / 60 / 24, 'hour', format) + suffix;
      }
      if (duration < 60 * 60 * 24 * 365) {
        return translate(duration / 60 / 24 / 30, 'month', format) + suffix;
      }
      return translate(duration / 60 / 24 / 365, 'year', format) + suffix;
    };
    humanDatetime.$stateful = true;
    return humanDatetime;
  }]);

}).call(this);
;
(function() {
  angular.module('ui.cms').filter('l10n', ["$langPicker", function($langPicker) {

    /**
    *   @ngdoc filter
    *   @name ui.cms.filter:l10n
    *   @description фильтр для работы с отображением L10n* объектов из cms типа L10nStringXs и т.п.
    *        По умолчанию язык берется из $langPicker.currentLang (текущий язык пользователя).
    *        Если перевод отсутствует, то будет возвращена строка в локали кода языка
    *        CMS_DEFAULT_LANG(глобальная js переменная, подробности см.  {@link ui.cms описание модуля ui.cms})
    *   @param {object} l10nObject мультиязычный объект
    *   @param {string=} forceLang код языка. Принудительное отображение перевода в языке forceLang(напр. "en", "ru", "de") вместо пользовательского.
    *   @returns {string} переведенный текст
    *   @example
    *       <pre>
    *           $scope.myCmsObject = {
    *               title: {
    *                   ru: "Привет",
    *                   en: "Hello",
    *                   ua: "Вiтання"
    *               }
    *           }
    *       </pre>
    *       <pre>
    *           //- pug
    *           p {{myCmsObject.title | l10n}}
    *       </pre>
    *       Вывод (при текущем языке пользователя ua)
    *       <pre>
    *           <p>Вiтання</p>
    *       </pre>
    *       Вывод (при текущем языке cz и CMS_DEFAULT_LANG=='en')
    *       <pre>
    *           <p>Hello</p>
    *       </pre>
     */
    return function(l10nObject, forceLang) {
      var lang, v;
      if (!l10nObject || angular.isString(l10nObject)) {
        return l10nObject || '';
      }
      forceLang = forceLang || $langPicker.currentLang;
      if (l10nObject[forceLang] || l10nObject[CMS_DEFAULT_LANG]) {
        return l10nObject[forceLang] || l10nObject[CMS_DEFAULT_LANG];
      }
      for (lang in l10nObject) {
        v = l10nObject[lang];
        if (v) {
          return v;
        }
      }
      return '';
    };
  }]);

}).call(this);
;
(function() {
  angular.module('ui.cms').filter('time', ["$filter", function($filter) {
    var BANNED_FORMATS, FOMAT_MAP, dateFilter;
    BANNED_FORMATS = ['fullDate', 'longDate', 'mediumDate', 'shortDate'];
    FOMAT_MAP = {
      medium: 'mediumTime',
      short: 'shortTime'
    };
    dateFilter = null;

    /**
    *   @ngdoc filter
    *   @name ui.cms.filter:time
    *   @description
    *   Formats `time` to a string based on the requested `format`.
    *
    *   `format` string can be composed of the following elements:
    *
    *   * `'HH'`: Hour in day, padded (00-23)
    *   * `'H'`: Hour in day (0-23)
    *   * `'hh'`: Hour in AM/PM, padded (01-12)
    *   * `'h'`: Hour in AM/PM, (1-12)
    *   * `'mm'`: Minute in hour, padded (00-59)
    *   * `'m'`: Minute in hour (0-59)
    *   * `'ss'`: Second in minute, padded (00-59)
    *   * `'s'`: Second in minute (0-59)
    *   * `'sss'`: Millisecond in second, padded (000-999)
    *   * `'a'`: AM/PM marker
    *
    *   `format` string can also be one of the following predefined formats:
    *
    *   * `'medium'`: equivalent to `'h:mm:ss a'` for en_US locale (e.g. 12:05:08 PM)
    *   * `'short'`: equivalent to `'h:mm a'` for en_US locale (e.g. 12:05 PM)
    *
    *   `format` string can contain literal values. These need to be escaped by surrounding with single quotes (e.g.
    *   `"h 'in the morning'"`). In order to output a single quote, escape it - i.e., two single quotes in a sequence
    *   (e.g. `"h 'o''clock'"`).
    *
    *   Any other characters in the `format` string will be output as-is.
    *
    *   @param {string} date Time to format, milliseconds (string or
    *    number) or various ISO 8601 datetime string formats (e.g. HH:mm:ss.sss and its
    *    shorter versions like HH:mm or HH:mm:ss).
    *   @param {string=} format Formatting rules (see Description). If not specified,
    *    `medium` is used.
    *   @returns {string} Formatted string or the input if input is not recognized as time.
    *   @example
    *       <pre>
    *           $scope.time1 = "09:10:30"
    *       </pre>
    *       <pre>
    *           //- pug
    *           p {{time1 | time: 'short'}}
    *       </pre>
    *       Вывод:
    *       <pre>
    *           <p>9:10</p>
    *       </pre>
     */
    return function(value, format) {
      var parts;
      format = format || 'medium';
      format = FOMAT_MAP[format] || format;
      if (BANNED_FORMATS.includes(format)) {
        throw "You can't use format '" + format + "' for time filter!";
      }
      dateFilter = dateFilter || $filter('date');
      if (angular.isDate(value)) {
        return dateFilter(value, format);
      }
      value = (value || '').split('.');
      value[0] = value[0] || '';
      value[1] = value[1] || 0;
      parts = value[0].split(':');
      return dateFilter(new Date(0, 0, 0, parts[0] || 0, parts[1] || 0, parts[2] || 0, value[1]), format);
    };
  }]);

}).call(this);
;
(function() {
  angular.module('ui.cms').filter('toUrlId', ["$filter", function($filter) {
    var BANNED_CHARS, cache, l10nFilter;
    BANNED_CHARS = ";/?:@&=+$|,!{}\^[]`<>#%".split('');
    l10nFilter = $filter('l10n');
    cache = {};

    /**
    *   @ngdoc filter
    *   @name ui.cms.filter:toUrlId
    *   @description фильтр для конвертирования объектов из cms типа CmsArticle и т.п. в url-пригодные строки с id объекта в начале
    *   @param {object} l10nObject объект
    *   @param {string=} forceLang код языка. Принудительное отображение перевода в языке forceLang(напр. "en", "ru", "de") вместо пользовательского.
    *   @returns {string} переведенный текст
    *   @example
    *       <pre>
    *           $scope.myCmsObject = {
    *               id: 1
    *               title: {
    *                   ru: "Привет мир!",
    *                   en: "Hello world!",
    *                   ua: "Вiтання миру!"
    *               }
    *           }
    *       </pre>
    *       <pre>
    *           //- pug
    *           p {{myCmsObject | toUrlId}}
    *       </pre>
    *       Вывод (при текущем языке пользователя ua)
    *       <pre>
    *           <p>1-вiтання-миру</p>
    *       </pre>
    *       Вывод (при текущем языке cz и CMS_DEFAULT_LANG=='en')
    *       <pre>
    *           <p>1-hello-world</p>
    *       </pre>
     */
    return function(l10nObject, forceLang) {
      var ch, i, len, title, titleCleaned;
      if (!l10nObject) {
        return;
      }
      if (angular.isObject(l10nObject.urlIdPart)) {
        title = l10nFilter(l10nObject.urlIdPart, forceLang);
        if (title) {
          return l10nObject.id + '-' + title;
        }
      }
      if (!l10nObject.id) {
        console.warn(l10nObject);
        throw "toUrlId filter: id field required!";
      }
      title = l10nFilter(l10nObject.title, forceLang);
      if (!title) {
        return l10nObject.id + '';
      }
      if (cache[title]) {
        return l10nObject.id + '-' + cache[title];
      }
      titleCleaned = title.split(': ').join('-').split(' ').join('-').toLowerCase();

      /*
      titleCleaned = title.replaceAll(' ', '-').replaceAll('!', '.')
                   .replaceAll('/', '-').replaceAll('?', '-')
                   .replaceAll(',', '').replaceAll('&', '-')
                   .replaceAll('"', '').replaceAll("'", '').toLowerCase()
       */
      for (i = 0, len = BANNED_CHARS.length; i < len; i++) {
        ch = BANNED_CHARS[i];
        if (titleCleaned.indexOf(ch) > -1) {
          titleCleaned = titleCleaned.replaceAll(ch, '');
        }
      }
      if (!titleCleaned) {
        return l10nObject.id + '';
      }
      cache[title] = titleCleaned;
      return l10nObject.id + '-' + titleCleaned;
    };
  }]);

}).call(this);
;
(function() {
  angular.module('ui.cms').filter('toYesNo', ["$filter", function($filter) {
    var NO, YES, translateFilter;
    translateFilter = $filter('translate');
    YES = ['true', 'True', 'TRUE', 't', 'T', '1', 1];
    NO = ['false', 'False', 'FALSE', 'f', 'F', '0', 0];
    return function(value, null_is_no) {

      /**
      *   @ngdoc filter
      *   @name ui.cms.filter:toYesNo
      *   @description фильтр для конвертирования переданного булевого значения в текст Yes/No. Результат переводится сразу фильтром translate.
      *   <br/>
      *   <ul>
      *       <li>YES = [true, 'true', 'True', 'TRUE', 't', 'T', '1', 1]</li>
      *       <li>NO = [false, 'false', 'False', 'FALSE', 'f', 'F', '0', 0]</li>
      *   </ul>
      *   @param {boolean|number|string} value значение
      *   @param {boolean=} [null_is_no=true] интерпретировать ли значения <b>null</b> и <b>undefined</b> как false
      *   @returns {string} переведенный текст
      *   @example
      *       <pre>
      *           $scope.myTrue = true
      *       </pre>
      *       <pre>
      *           //- pug
      *           p {{myTrue | toYesNo}}
      *       </pre>
      *       Вывод (при текущем языке пользователя ua)
      *       <pre>
      *           <p>Так</p>
      *       </pre>
      *       Вывод (при текущем языке cz и CMS_DEFAULT_LANG=='en')
      *       <pre>
      *           <p>Yes</p>
      *       </pre>
       */
      if (value === true || YES.indexOf(value) > -1) {
        return translateFilter('Yes');
      }
      if (value === false || NO.indexOf(value) > -1) {
        return translateFilter('No');
      }
      if (!angular.isDefined(null_is_no)) {
        null_is_no = true;
      }
      if (null_is_no && (!angular.isDefined(value) || value === null)) {
        return translateFilter('No');
      }
      return '';
    };
  }]);

}).call(this);
;

/**
*   @ngdoc service
*   @name ui.cms.service:H
*   @description сервис, который содержит вспомогательные функции для работы с примитивами данных;
*       загрузки js, ccs файлов из ангулара;
 */

(function() {
  var HAS_WEBP_SUPPORT, error, resolveExt, webp,
    bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

  HAS_WEBP_SUPPORT = false;

  webp = new Image();

  webp.onload = webp.onerror = function() {
    HAS_WEBP_SUPPORT = webp.height === 2;
    if (HAS_WEBP_SUPPORT && document.body) {
      document.body.className = (document.body.className || "") + " format-support-webp";
    }
  };

  webp.src = 'data:image/webp;base64,UklGRjoAAABXRUJQVlA4IC4AAACyAgCdASoCAAIALmk0mk0iIiIiIgBoSygABc6WWgAA/veff/0PP8bA//LwYAAA';

  resolveExt = function(filePath, ignoreWebp) {
    var ts;
    if (ignoreWebp || !HAS_WEBP_SUPPORT) {
      return filePath;
    }
    if (filePath.endsWith('.jpg') || filePath.endsWith('.jpeg') || filePath.endsWith('.png') || filePath.endsWith('.apng') || filePath.endsWith('.gif')) {
      if (filePath.indexOf('.thumb.') > -1) {
        ts = filePath.split('.thumb.')[1].split('.')[0].split('x');
        ts = {
          width: parseInt(ts[0]),
          height: parseInt(ts[1])
        };
        if (ts.width > 80 || ts.height > 80) {
          filePath = filePath.split('.');
          filePath[filePath.length - 1] = 'webp';
          return filePath.join('.');
        }
      } else {
        filePath = filePath.split('.');
        filePath[filePath.length - 1] = 'webp';
        return filePath.join('.');
      }
    }
    return filePath;
  };

  try {
    if (!CMS_STORAGE.webp) {
      resolveExt = angular.identity;
    }
  } catch (error1) {
    error = error1;
    resolveExt = angular.identity;
  }

  angular.module('ui.cms').service('H', ["$rootScope", "$http", "$window", "$document", "$q", "$timeout", "$langPicker", "$injector", "gettextCatalog", function($rootScope, $http, $window, $document, $q, $timeout, $langPicker, $injector, gettextCatalog) {
    var $cms, HStyleSheet, L10nFileHelper, callbacks, copyToClipboardFallback, getScrollPosition, getViewportSize, isTablsetScope, loadOnceDone, load_started, loaded, now, preLoadOnce, resolveFilePath, resolveFileThumbPath, self, toDatesArray, toFloat, toInt;
    self = this;
    $cms = null;
    toInt = function(value, defaultValue) {
      var e;
      try {
        value = parseInt(value);
        if (!angular.isNumber(value) || isNaN(value)) {
          return defaultValue;
        }
        return value;
      } catch (error1) {
        e = error1;
        return defaultValue;
      }
    };
    toFloat = function(value, defaultValue) {
      var e;
      try {
        value = parseFloat(value);
        if (!angular.isNumber(value) || isNaN(value)) {
          return defaultValue;
        }
        return value;
      } catch (error1) {
        e = error1;
        return defaultValue;
      }
    };

    /**
    *   @ngdoc property
    *   @name ui.cms.service:H#loadOnce
    *   @propertyOf ui.cms.service:H
    *   @description
    *       функции для гарантированной однократной загрузки ресурсов. Пример использования: пусть 1 директива для своего рендеринга требует большую внешнюю js библиотеку.
    *       Тогда не желательно включать эту библиотеку на страницу, а лучше грузить ее внутри директивы
    *       и там уже производить рендеринг. При использовании нескольких таких директив на страницы может получиться ситуация, когда эта библиотека или ресурсы будут загружены несколько раз.
    *       Чтоб этого избежать используйте H.loadOnce.<ресурс>
     */
    load_started = {};
    loaded = {};
    callbacks = {};
    loadOnceDone = function(_type, url, data, use_rootscope) {
      var cb, e, i, len, ref;
      switch (_type) {
        case 'js':
        case 'css':
          loaded[url] = true;
          break;
        case 'data':
          if (self.path.extname(url) === '.json') {
            try {
              data = JSON.parse(data);
            } catch (error1) {
              e = error1;
            }
          }
          loaded[url] = data;
      }
      load_started[url] = false;
      if (use_rootscope) {
        $rootScope.$apply(function() {
          var cb, i, len, ref;
          ref = callbacks[url] || [];
          for (i = 0, len = ref.length; i < len; i++) {
            cb = ref[i];
            cb(data);
          }
          return delete callbacks[url];
        });
      } else {
        ref = callbacks[url] || [];
        for (i = 0, len = ref.length; i < len; i++) {
          cb = ref[i];
          cb(data);
        }
        delete callbacks[url];
      }
    };
    preLoadOnce = function(url, cb, not_loaded_fn) {
      if (!load_started[url] && !loaded[url]) {
        load_started[url] = true;
        callbacks[url] = callbacks[url] || [];
        if (cb) {
          callbacks[url].push(cb);
        }
        return not_loaded_fn();
      }
      if (!cb) {
        return;
      }
      if (loaded[url]) {
        if (angular.isString(loaded[url]) || angular.isObject(loaded[url])) {
          return cb(loaded[url]);
        }
        cb();
      } else {
        callbacks[url] = callbacks[url] || [];
        callbacks[url].push(cb);
      }
    };
    this.loadOnce = {

      /**
      *   @ngdoc property
      *   @name ui.cms.service:H#loadOnce.js
      *   @methodOf ui.cms.service:H
      *   @description
      *        однократная загрузка js-ресурса (не грузите через него json!)
      *   @param {string} url путь к файлу
      *   @param {function=} cb callback-функция, которая будет вызвана после загрузки ресурса
      *   <pre>
      *       > H.path.extname 'htts://some.com/myscript.js', ()->
      *           console.log('All done')
      *   </pre>
       */
      js: function(url, cb) {
        var options;
        options = {};
        if (angular.isObject(cb)) {
          options = cb;
          cb = cb.onReady;
        }
        preLoadOnce(url, cb, function() {
          var script;
          script = document.createElement('script');
          if (options.id) {
            script.setAttribute('id', options.id);
          }
          script.onload = function() {
            return loadOnceDone('js', url, void 0, true);
          };
          script.src = url;
          script.type = "text/javascript";
          document.getElementsByTagName('head')[0].appendChild(script);
        });
      },

      /**
      *   @ngdoc property
      *   @name ui.cms.service:H#loadOnce.css
      *   @methodOf ui.cms.service:H
      *   @description
      *        однократная загрузка css-ресурса
      *   @param {string} url путь к файлу
      *   @param {function=} cb callback-функция, которая будет вызвана после загрузки ресурса.
      *       <b>Не все браузеры могут правильно уведомлять об окончании загрузки</b>
      *   <pre>
      *       > H.path.extname 'htts://some.com/mystyle.css', ()->
      *           console.log('All done')
      *   </pre>
       */
      css: function(url, cb) {
        preLoadOnce(url, cb, function() {
          var link;
          link = document.createElement("link");
          link.rel = "stylesheet";
          link.href = url;
          if (link.addEventListener) {
            link.addEventListener("load", function() {
              return loadOnceDone('css', url, void 0, true);
            });
          } else if (link.attachEvent) {
            link.attachEvent("onload", function() {
              return loadOnceDone('css', url, void 0, true);
            });
          } else {
            loadOnceDone('css', url, void 0);
          }
          document.getElementsByTagName('head')[0].appendChild(link);
        });
      },

      /**
      *   @ngdoc property
      *   @name ui.cms.service:H#loadOnce.data
      *   @methodOf ui.cms.service:H
      *   @description
      *        однократная загрузка ресурса. Если значение загруженного файла можно преобразовать в json, то это будет сделано
      *   @param {string} url путь к файлу
      *   @param {function=} cb callback-функция, которая будет вызвана после загрузки ресурса
      *   <pre>
      *       > H.path.extname 'htts://some.com/myscript.json', (data)->
      *           console.log('All done')
      *           console.log('Loaded data', data)
      *   </pre>
       */
      data: function(url, cb) {
        preLoadOnce(url, cb, function() {
          $http.get(url).then(function(data) {
            return loadOnceDone('data', url, data.data);
          }, function(data) {
            return loadOnceDone('data', url, void 0);
          });
        });
      }
    };

    /*
                path
     */
    resolveFilePath = function(fileName, forceCmsStorageType, ignoreWebp) {
      var ref;
      if (!forceCmsStorageType) {
        forceCmsStorageType = CMS_STORAGE.type;
      }
      if (fileName[0] === '/') {
        fileName = fileName.substr(1);
      }
      if (['local', 'localStorage', ''].includes(forceCmsStorageType)) {
        if (fileName.indexOf("bower_components/") === 0 || fileName.indexOf("client/") === 0) {
          if ((ref = location.hostname) === 'localhost' || ref === '') {
            return "/" + fileName;
          }
          return resolveExt("/client/" + fileName, ignoreWebp);
        }
        return resolveExt("/storage/lbcms-container-" + CMS_DB_BUCKET + "/" + fileName, ignoreWebp);
      }
      if (['amazon', 'google', 'digitaloceanSpaces'].includes(forceCmsStorageType)) {
        return resolveExt(CMS_STORAGE.server + "/lbcms-container-" + CMS_DB_BUCKET + "/" + fileName, ignoreWebp);
      }
      return '';
    };
    resolveFileThumbPath = function(fileName, thumbSize, forceCmsStorageType) {
      var path;
      if (thumbSize.indexOf('x') === -1) {
        thumbSize = thumbSize + "x" + thumbSize;
      }
      path = resolveFilePath(fileName, forceCmsStorageType, true);
      return resolveExt(path + ".thumb." + thumbSize + ".jpg");
    };
    this.path = {

      /**
      *   @ngdoc property
      *   @name ui.cms.service:H#path.extname
      *   @methodOf ui.cms.service:H
      *   @description
      *        возвращает расширение файла
      *   @param {string} filename путь к файлу, или его название
      *   @returns {string} расширение файла (в нижнем регистре) с ведущей точкой
      *   <pre>
      *       > H.path.extname('hello.world')
      *       '.world'
      *
      *       > H.path.extname('/home/kirill/some_file.mp3')
      *       '.mp3'
      *   </pre>
       */
      extname: function(filename) {
        var data, splitPathRe;
        splitPathRe = /^(\/?|)([\s\S]*?)((?:\.{1,2}|[^\/]+?|)(\.[^.\/]*|))(?:[\/]*)$/;
        data = splitPathRe.exec(filename).slice(1);
        return data[3].toLowerCase();
      },
      resolveExt: resolveExt,
      resolveFilePath: resolveFilePath,
      resolveFileThumbPath: resolveFileThumbPath,
      resolveL10nFileThumbPath: function(l10nObject, thumbSize) {

        /**
        *   @ngdoc property
        *   @name ui.cms.service:H#path.resolveL10nFileThumbPath
        *   @methodOf ui.cms.service:H
        *   @description
        *        возвращает путь к превьюшке статичного файла из l10nObject(потомка L10nBaseFile),
        *        который может находиться как в облаке, так и на локальной машине.
        *        Используется в директиве {@link ui.cms.directive:uicImgThumb uicImgThumb} и ей подобных.
        *   @param {object} l10nObject мультиязычный объект файла
        *   @param {string} thumbSize размер превью (напр.: "70", "240x320")
        *   @returns {string} полный путь к файлу или ""
         */
        if (!l10nObject) {
          return '';
        }
        return resolveFileThumbPath(l10nObject.name, thumbSize);
      },
      resolveL10nFilePath: function(l10nObject, forceLang) {

        /**
        *   @ngdoc property
        *   @name ui.cms.service:H#path.resolveL10nFilePath
        *   @methodOf ui.cms.service:H
        *   @description
        *        возвращает путь к статичному файлу из l10nObject(потомка L10nBaseFile),
        *        который может находиться как в облаке, так и на локальной машине.
        *        Используется в директиве {@link ui.cms.directive:uicImg uicImg} и ей подобных.
        *   @param {object} l10nObject мультиязычный объект файла
        *   @returns {string} полный путь к файлу или ""
         */
        if (!l10nObject) {
          return '';
        }
        return resolveFilePath(l10nObject.name);
      }
    };
    L10nFileHelper = (function() {
      function L10nFileHelper() {
        this.isFileHasThumbs = bind(this.isFileHasThumbs, this);
      }


      /**
      *   @ngdoc property
      *   @name ui.cms.service:H#l10nFile.getFileType
      *   @methodOf ui.cms.service:H
      *   @description
      *       возвращает тип файла. Хотя это можно сделать и сразу на месте, следует пользоваться этими врапперами во избежание смены формата описания файлов
      *   @param {L10nFile} l10nFile объект файла
      *   @returns {string} mime-тип файла
      *   <pre>
      *       &gt; myFile ={
      *             mainImg: {
      *                 name: "en-5789de3dbb9a43029fb38021.jpg",
      *                 title: "praha",
      *                 description: "praha",
      *                 meta: {
      *                   size: 495656,
      *                   type: "image/jpeg",
      *                   originalName: "Малостранская_мостовая_башня_Карлова_моста_в_Праге.jpg",
      *                   width: 1200,
      *                   height: 797
      *                 },
      *                 thumbs: [
      *                   "70x70",
      *                   "350x350",
      *                   "440x250"
      *                  ]
      *            }
      *       }
      *       &gt; H.l10nFile.getFileType(myFile)
      *       'image/jpeg'
      *   </pre>
       */

      L10nFileHelper.prototype.getFileType = function(l10nFile) {
        if (!l10nFile) {
          return '';
        }
        return l10nFile.meta.type;
      };


      /**
      *   @ngdoc property
      *   @name ui.cms.service:H#l10nFile.getFileThumbs
      *   @methodOf ui.cms.service:H
      *   @description
      *       возвращает массив размеров превьюшек файла. Хотя это можно сделать и сразу на месте, следует пользоваться этими врапперами во избежание смены формата описания файлов
      *   @param {L10nFile} l10nFile объект файла
      *   @returns {Array<string>} массив размеров
      *   <pre>
      *       &gt; myFile ={
      *             mainImg: {
      *                 name: "en-5789de3dbb9a43029fb38021.jpg",
      *                 title: "praha",
      *                 description: "praha",
      *                 meta: {
      *                   size: 495656,
      *                   type: "image/jpeg",
      *                   originalName: "Малостранская_мостовая_башня_Карлова_моста_в_Праге.jpg",
      *                   width: 1200,
      *                   height: 797
      *                 },
      *                 thumbs: [
      *                   "70x70",
      *                   "350x350",
      *                   "440x250"
      *                  ]
      *             }
      *       }
      *       &gt; H.l10nFile.getFileThumbs(myFile)
      *       ["70x70", "350x350", "440x250"]
      *   </pre>
       */

      L10nFileHelper.prototype.getFileThumbs = function(l10nFile) {
        return (l10nFile || {}).thumbs || [];
      };


      /**
      *   @ngdoc property
      *   @name ui.cms.service:H#l10nFile.isFileHasThumbs
      *   @methodOf ui.cms.service:H
      *   @description
      *       определяет, содержит ли L10nFile превью необходимых размеров. Хотя это можно сделать и сразу на месте, следует пользоваться этими врапперами во избежание смены формата описания файлов
      *   @param {L10nFile} l10nFile объект файла
      *   @param {Array<string>} thumbs массив размеров, которые файл должен содержать
      *   @returns {boolean} результат. Если хоть 1 размер не доступен у файла, то результат - false
      *   <pre>
      *       &gt; myFile ={
      *             mainImg: {
      *                 name: "en-5789de3dbb9a43029fb38021.jpg",
      *                 title: "praha",
      *                 description: "praha",
      *                 meta: {
      *                   size: 495656,
      *                   type: "image/jpeg",
      *                   originalName: "Малостранская_мостовая_башня_Карлова_моста_в_Праге.jpg",
      *                   width: 1200,
      *                   height: 797
      *                 },
      *                 thumbs: [
      *                   "70x70",
      *                   "350x350",
      *                   "440x250"
      *                  ]
      *             }
      *       }
      *       &gt; H.l10nFile.isFileHasThumbs(myFile, ["88x88", "70x70"])
      *       false
      *       &gt; H.l10nFile.isFileHasThumbs(myFile, ["440x250", "70x70"])
      *       true
      *   </pre>
       */

      L10nFileHelper.prototype.isFileHasThumbs = function(l10nFile, thumbs) {
        var fthumbs, has;
        if (angular.isString(thumbs)) {
          thumbs = [thumbs];
        }
        fthumbs = this.getFileThumbs(l10nFile) || [];
        has = thumbs.filter(function(size) {
          return fthumbs.includes(size);
        });
        return has.length === thumbs.length;
      };


      /**
      *   @ngdoc property
      *   @name ui.cms.service:H#l10nFile.isValidFileType
      *   @methodOf ui.cms.service:H
      *   @description
      *       определяет, является ли mime-тип допустимым для перечисленных типов
      *   @param {string} fileType mime-тип файла
      *   @param {string=} [accept='*'] разрешенные mime-типы файлов, перечисленные через запятую. Строка '*' - значит разрешен любой тип
      *   @returns {boolean} результат совпадения
      *   <pre>
      *       &gt; H.l10nFile.isValidFileType('text/html', '*')
      *       true
      *       &gt; H.l10nFile.isValidFileType('text/html', 'video/*,audio/*')
      *       false
      *       &gt; H.l10nFile.isValidFileType('image/png', 'video/*,image/*')
      *       true
      *   </pre>
       */

      L10nFileHelper.prototype.isValidFileType = function(fileType, accept) {
        var a, i, len, r, ref;
        accept = accept || '*';
        if (accept === '*') {
          return true;
        }
        ref = accept.split(',');
        for (i = 0, len = ref.length; i < len; i++) {
          a = ref[i];
          r = new RegExp(a);
          if (fileType.match(r)) {
            return true;
          }
        }
        return false;
      };

      L10nFileHelper.prototype.getProperty = function(l10nFile, propertyName) {
        return (l10nFile || {})[propertyName];
      };


      /**
      *   @ngdoc property
      *   @name ui.cms.service:H#l10nFile.getOriginalName
      *   @methodOf ui.cms.service:H
      *   @description
      *       возвращает оригинальное название файла (которое было у него при загрузке на сервер). Хотя это можно сделать и сразу на месте, следует пользоваться этими врапперами во избежание смены формата описания файлов
      *   @param {L10nFile} l10nFile объект файла
      *   @returns {string} название
      *   <pre>
      *       &gt; myFile ={
      *             mainImg: {
      *                 name: "en-5789de3dbb9a43029fb38021.jpg",
      *                 title: "praha",
      *                 description: "praha",
      *                 meta: {
      *                   size: 495656,
      *                   type: "image/jpeg",
      *                   originalName: "Малостранская_мостовая_башня_Карлова_моста_в_Праге.jpg",
      *                   width: 1200,
      *                   height: 797
      *                 },
      *                 thumbs: [
      *                   "70x70",
      *                   "350x350",
      *                   "440x250"
      *                  ]
      *             }
      *       }
      *       &gt; H.l10nFile.getOriginalName(myFile)
      *       "Малостранская_мостовая_башня_Карлова_моста_в_Праге.jpg"
      *   </pre>
       */

      L10nFileHelper.prototype.getOriginalName = function(l10nFile, forceLang) {
        if (!l10nFile) {
          return '';
        }
        return l10nFile.meta.originalName;
      };


      /**
      *   @ngdoc property
      *   @name ui.cms.service:H#l10nFile.getFileTypeIcon
      *   @methodOf ui.cms.service:H
      *   @description
      *       возвращает название иконки в FontAwesome для l10nFile
      *   @param {L10nFile} l10nFile объект файла
      *   @returns {string} название иконки
      *   <pre>
      *       &gt; myFile ={
      *             mainImg: {
      *                 name: "en-5789de3dbb9a43029fb38021.jpg",
      *                 title: "praha",
      *                 description: "praha",
      *                 meta: {
      *                   size: 495656,
      *                   type: "image/jpeg",
      *                   originalName: "Малостранская_мостовая_башня_Карлова_моста_в_Праге.jpg",
      *                   width: 1200,
      *                   height: 797
      *                 },
      *                 thumbs: [
      *                   "70x70",
      *                   "350x350",
      *                   "440x250"
      *                  ]
      *             }
      *       }
      *       &gt; H.l10nFile.getFileTypeIcon(myFile)
      *       'fa-file-image-o'
      *   </pre>
       */

      L10nFileHelper.prototype.getFileTypeIcon = function(l10nFile, forceLang) {
        var fileType;
        fileType = this.getFileType(l10nFile, forceLang);
        return this.getTypeIconNameByFileType(fileType);
      };


      /**
      *   @ngdoc property
      *   @name ui.cms.service:H#l10nFile.getFileUrl
      *   @methodOf ui.cms.service:H
      *   @description
      *        возвращает путь к статичному файлу из l10nObject,
      *        который может находиться как в облаке, так и на локальной машине.
      *        Используется в директиве {@link ui.cms.directive:uicImg uicImg} и ей подобных.
      *   @param {L10nFile} l10nObject мультиязычный объект файла
      *   @returns {string} полный путь к файлу или ""
       */

      L10nFileHelper.prototype.getFileUrl = function(l10nFile, forceCmsStorageType) {
        if (!l10nFile) {
          return '';
        }
        return resolveFilePath(l10nFile.name, forceCmsStorageType);
      };


      /**
      *   @ngdoc property
      *   @name ui.cms.service:H#l10nFile.getFileThumbUrl
      *   @methodOf ui.cms.service:H
      *   @description
      *        возвращает путь к превьюшке статичного файла из l10nObject,
      *        который может находиться как в облаке, так и на локальной машине.
      *        Используется в директиве {@link ui.cms.directive:uicImgThumb uicImgThumb} и ей подобных.
      *   @param {L10nFile} l10nObject мультиязычный объект файла
      *   @param {string} thumbSize размер превью (напр.: "70", "240x320")
      *   @returns {string} полный путь к файлу или ""
       */

      L10nFileHelper.prototype.getFileThumbUrl = function(l10nFile, thumbSize, forceLang, defaultUrl) {
        var furl;
        if (!l10nFile) {
          if (defaultUrl) {
            return defaultUrl;
          }
          return '';
        }
        furl = resolveFileThumbPath(l10nFile.name, thumbSize);
        return furl || defaultUrl || '/client/brand/no-image.png';
      };

      L10nFileHelper.prototype.getCopyrightHtml = function(l10nFile) {
        var creator, creatorUrl;
        if (!l10nFile || !l10nFile.copyright) {
          return '';
        }
        if (l10nFile.copyright.creatorUrl) {
          creatorUrl = l10nFile.copyright.creatorUrl;
          if (creatorUrl.indexOf('http') !== 0) {
            creatorUrl = "https://" + creatorUrl;
          }
        }
        if (l10nFile.copyright.creator) {
          creator = gettextCatalog.getString('Photo by {{creator}}', {
            creator: l10nFile.copyright.creator
          });
        }
        if (creatorUrl) {
          return "<small class='h-l10nfile-copyright'>\n    <a target='_blank' rel='nofollow' href='" + creatorUrl + "'>" + (creator || creatorUrl) + "</a>\n</small>";
        }
        if (!creator) {
          return '';
        }
        return "<small class='h-l10nfile-copyright'>" + creator + "</small>";
      };


      /**
      *   @ngdoc property
      *   @name ui.cms.service:H#l10nFile.getTypeIconNameByFileType
      *   @methodOf ui.cms.service:H
      *   @description
      *       возвращает название иконки в FontAwesome по mime-тип файла
      *   @param {string=} fileType mime-тип файла
      *   @returns {string} название иконки
      *   <pre>
      *       &gt; H.l10nFile.getTypeIconNameByFileType('text/html')
      *       'fa-file-text-o'
      *       &gt; H.l10nFile.getTypeIconNameByFileType('application/zip')
      *       'fa-file-archive-o'
      *       &gt; H.l10nFile.getTypeIconNameByFileType('application/msword')
      *       'fa-file-word-o'
      *   </pre>
       */

      L10nFileHelper.prototype.getTypeIconNameByFileType = function(fileType) {
        var TYPES, iconName, typesList;
        if (!fileType) {
          return 'fa-file';
        }
        if (fileType === 'application/pdf') {
          return 'fa-file-pdf';
        }
        if (fileType.includes('audio/')) {
          return 'fa-file-audio';
        }
        if (fileType.includes('video/')) {
          return 'fa-file-video';
        }
        if (fileType.includes('text/')) {
          return 'fa-file-alt';
        }
        if (fileType.includes('image/')) {
          return 'fa-file-image';
        }
        TYPES = {
          'fa-file-archive': ['application/zip', 'application/gzip', 'application/x-rar-compressed', 'application/x-tar'],
          'fa-file-word': ['application/vnd.oasis.opendocument.text', 'application/msword', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'],
          'fa-file-excel': ['application/vnd.oasis.opendocument.spreadsheet', 'application/vnd.ms-excel', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet']
        };
        for (iconName in TYPES) {
          typesList = TYPES[iconName];
          if (typesList.includes(fileType)) {
            return iconName;
          }
        }
        return 'fa-file';
      };

      L10nFileHelper.prototype.humanFileSizeToBytes = function(text) {
        var m, num;
        text = text.replace(' ', '');
        num = text.replace(/[^0-9]/g, '');
        m = text.replace(num, '').toLowerCase();
        if (m === 'b' || m === 'bytes') {
          return parseFloat(num);
        }
        if (m === 'kb') {
          return parseFloat(num) * 1024;
        }
        if (m === 'mb') {
          return parseFloat(num) * 1024 * 1024;
        }
        if (m === 'gb') {
          return parseFloat(num) * 1024 * 1024 * 1024;
        }
        throw "Human filesize '" + text + "' is too big";
      };

      L10nFileHelper.prototype.calcPadding = function(size) {
        var s;
        if (!size) {
          return 100;
        }
        if (typeof size === 'string') {
          if (size.indexOf('x') > -1) {
            s = size.split('x');
            return (parseInt(s[1]) / parseInt(s[0])) * 100;
          }
        }
        if (size instanceof Object) {
          return (size.height / size.width) * 100;
        }
        return 100;
      };

      return L10nFileHelper;

    })();
    this.l10nFile = new L10nFileHelper();
    now = new Date();
    getViewportSize = function() {
      return {
        width: Math.max($document[0].documentElement.clientWidth || 0, $window.innerWidth || 0),
        height: Math.max($document[0].documentElement.clientHeight || 0, $window.innerHeight || 0)
      };
    };
    getScrollPosition = function() {
      return {
        top: Math.max($document[0].documentElement.scrollTop || 0, $window.pageYOffset || 0),
        left: Math.max($document[0].documentElement.scrollLeft || 0, $window.pageXOffset || 0)
      };
    };

    /**
    *   @ngdoc property
    *   @name ui.cms.service:H#info
    *   @propertyOf ui.cms.service:H
    *   @description
    *       статическая информация и информация об элементах на странице. См ниже
     */

    /**
    *   @ngdoc property
    *   @name ui.cms.service:H#info.timezoneOffset
    *   @propertyOf ui.cms.service:H
    *   @description целое число
     */
    this.info = {
      timezoneOffset: now.getTimezoneOffset(),
      viewport: {

        /**
        *   @ngdoc property
        *   @name ui.cms.service:H#info.viewport.getSize
        *   @methodOf ui.cms.service:H
        *   @description возвращает размеры текущего viewport в формате {width: 1000, height: 1000} в пикселях
        *   @returns {object} размеры
        *   <pre>
        *       # где-то в контроллере:
        *       size = H.info.viewport.getSize()
        *       console.log(size)
        *   </pre>
        *   Примерный вывод в консоли
        *   <pre>
        *       {width: 1360, height: 700}
        *   </pre>
         */
        getSize: getViewportSize,

        /**
        *   @ngdoc property
        *   @name ui.cms.service:H#info.viewport.getScrollPosition
        *   @methodOf ui.cms.service:H
        *   @description возвращает положение прокрутки на странице в формате {top: 1000, left: 1000} в пикселях
        *   @returns {object} размеры
        *   <pre>
        *       # где-то в контроллере:
        *       pos = H.info.viewport.getScrollPosition()
        *       console.log(pos)
        *   </pre>
        *   Примерный вывод в консоли
        *   <pre>
        *       {top: 120, left: 0}
        *   </pre>
         */
        getScrollPosition: getScrollPosition,

        /**
        *   @ngdoc property
        *   @name ui.cms.service:H#info.viewport.getElementPosition
        *   @methodOf ui.cms.service:H
        *   @description типа "полифила" над getBoundingClientRect у элемента на странице. При отсутствии
        *       функции в браузере, позиции будут высчитаны вручную
        *   @param {angular.element|jQuery|htmlElement} element элемент со страницы. может быть как чистым брауерным
        *       объектом, так и оберткой от angular.element() или $()
        *   @param {object=} scrollPosition это результат вызова функции H.info.viewport.getScrollPosition()
        *   @returns {object} ограничивающий прямоугольник
         */
        getElementPosition: function(element, scrollPosition) {
          var rect, scroll_pos;
          if (element instanceof angular.element) {
            element = element[0];
          }
          if (typeof jQuery === "function" && element instanceof jQuery) {
            element = element[0];
          }
          if (element.getBoundingClientRect) {
            return element.getBoundingClientRect();
          }
          rect = {
            top: element.offsetTop,
            left: element.offsetLeft,
            width: element.offsetWidth,
            height: element.offsetHeight
          };
          scroll_pos = scrollPosition || getScrollPosition();
          rect.top = rect.top - scroll_pos.top;
          rect.left = rect.left - scroll_pos.left;
          rect.bottom = rect.top + rect.height;
          rect.right = rect.left + rect.width;
          return rect;
        }
      },
      formatSupport: {

        /**
        *   @ngdoc property
        *   @name ui.cms.service:H#formatSupport.WEBP
        *   @propertyOf ui.cms.service:H
        *   @description булево значение, которое отображает, поддерживаются ли картинки webp в браузере
         */
        WEBP: HAS_WEBP_SUPPORT,
        hasPDF: (function() {
          var base64pdf, pdf_support;
          pdf_support = null;
          base64pdf = "data:application/pdf;base64,JVBERi0xLgoxIDAgb2JqPDwvUGFnZXMgMiAwIFI+PmVuZG9iagoyIDAgb2JqPDwvS2lkc1szIDAgUl0vQ291bnQgMT4+ZW5kb2JqCjMgMCBvYmo8PC9QYXJlbnQgMiAwIFI+PmVuZG9iagp0cmFpbGVyIDw8L1Jvb3QgMSAwIFI+Pg==";
          return function(cb) {
            var defer, embed, obj;
            if (pdf_support !== null) {
              if (angular.isFunction(cb)) {
                return cb(pdf_support);
              }
              defer = $q.defer();
              defer.resolve(pdf_support);
              return defer.promise;
            }
            obj = document.createElement('object');
            obj.type = "application/pdf";
            obj.style.visibility = 'hidden';
            obj.style.height = '0';
            obj.style.width = '0';
            obj.data = base64pdf;
            embed = document.createElement('embed');
            embed.type = "application/pdf";
            embed.style.visibility = 'hidden';
            embed.style.height = '0';
            embed.style.width = '0';
            embed.src = base64pdf;
            document.body.appendChild(obj);
            document.body.appendChild(embed);
            defer = $q.defer();
            $timeout(function() {
              if (obj.innerHtml || obj.contentWindow) {
                pdf_support = 'object';
              } else if (embed.innerHtml || obj.contentWindow) {
                pdf_support = 'embed';
              }
              document.body.removeChild(obj);
              document.body.removeChild(embed);
              pdf_support || (pdf_support = false);
              if (angular.isFunction(cb)) {
                return cb(pdf_support);
              }
              defer.resolve(pdf_support);
            }, 500);
            return defer.promise;
          };
        })()
      }
    };

    /*
        класс для работы со стилями сгенеренными из js
     */
    HStyleSheet = (function() {
      function HStyleSheet() {
        var styleElement;
        styleElement = document.createElement("style");
        styleElement.appendChild(document.createTextNode(""));
        document.head.appendChild(styleElement);
        this.sheet = styleElement.sheet || styleElement.styleSheet;
        return;
      }

      HStyleSheet.prototype.clean = function() {
        if (this.sheet.cssRules) {
          while (this.sheet.cssRules.length) {
            this.sheet.deleteRule(0);
          }
        } else {
          while (this.sheet.rules.length) {
            this.sheet.removeRule(0);
          }
        }
      };

      HStyleSheet.prototype.addRules = function(rules) {
        if (!angular.isArray(rules)) {
          rules = [rules];
        }
        rules.forEach((function(_this) {
          return function(textRule) {
            _this.sheet.insertRule(textRule || '');
          };
        })(this));
      };

      return HStyleSheet;

    })();
    this.style = {
      StyleSheet: HStyleSheet
    };
    toDatesArray = function(dates) {
      var d, i, items, len, ref;
      items = [];
      ref = dates || [];
      for (i = 0, len = ref.length; i < len; i++) {
        d = ref[i];
        items.push(new Date(d).getTime());
      }
      return items;
    };
    this.date = {

      /**
      *   @ngdoc property
      *   @name ui.cms.service:H#date.isSameDate
      *   @methodOf ui.cms.service:H
      *   @description сравнивает две даты
      *   @param {Date|string|number} date1 первая дата
      *   @param {Date|string|number} date2 вторая дата
      *   @returns {boolean} результат сравнения
      *   <pre>
      *       > H.date.isSameDate(new Date('1990-01-01'), new Date('1990-01-02'))
      *       false
      *
      *       > H.date.isSameDate(new Date('1990-01-01'), new Date('1990-01-01T03:03'))
      *       false
      *
      *       > now = new Date()
      *       H.date.isSameDate(now, new Date(now))
      *       true
      *       # если сделаем сравнение:
      *       > now == new Date(now)
      *       false // хотя ведь фактически оба объекта идентичны
      *   </pre>
       */
      isSameDate: function(date1, date2) {
        if (!date1 || !date2) {
          return false;
        }
        if (date1 === date2) {
          return true;
        }
        date1 = new Date(date1);
        date2 = new Date(date2);
        return date1.getTime() === date2.getTime();
      },

      /**
      *   @ngdoc property
      *   @name ui.cms.service:H#date.isSameDay
      *   @methodOf ui.cms.service:H
      *   @description игнорируя время(часы, минуты, секунды), вернет true, если объеты Date
      *       находятся в одном дне
      *   @param {Date|string|number} date1 первая дата
      *   @param {Date|string|number} date2 вторая дата
      *   @returns {boolean} результат сравнения
      *   <pre>
      *       > H.date.isSameDay(new Date('1990-01-01'), new Date('1990-01-02'))
      *       false
      *
      *       > H.date.isSameDay(new Date('1990-01-01'), new Date('1990-01-01T03:03'))
      *       true
      *
      *       > now = new Date()
      *       H.date.isSameDay(now, new Date(now))
      *       true
      *       # если сделаем сравнение:
      *       > now == new Date(now)
      *       false // хотя ведь фактически оба объекта идентичны
      *   </pre>
       */
      isSameDay: function(date1, date2) {
        if (!date1 || !date2) {
          return false;
        }
        if (date1 === date2) {
          return true;
        }
        date1 = new Date(date1);
        date2 = new Date(date2);
        return date1.getDate() === date2.getDate() && date1.getMonth() === date2.getMonth() && date1.getFullYear() === date2.getFullYear();
      },

      /**
      *   @ngdoc property
      *   @name ui.cms.service:H#date.isSameMonth
      *   @methodOf ui.cms.service:H
      *   @description игнорируя время(часы, минуты, секунды), вернет true, если объеты Date
      *       находятся в одном месяце
      *   @param {Date} date1 первая дата
      *   @param {Date} date2 вторая дата
      *   @returns {boolean} результат сравнения
      *   <pre>
      *       > H.date.isSameMonth(new Date('1990-01-01'), new Date('1990-01-02'))
      *       true
      *
      *       > H.date.isSameMonth(new Date('1990-01-01'), new Date('1990-02-01T03:03'))
      *       false
      *
      *       > now = new Date()
      *       H.date.isSameMonth(now, new Date(now))
      *       true
      *       # если сделаем сравнение:
      *       > now == new Date(now)
      *       false // хотя ведь фактически оба объекта идентичны
      *   </pre>
       */
      isSameMonth: function(date1, date2) {
        if (!date1 || !date2) {
          return false;
        }
        if (date1 === date2) {
          return true;
        }
        date1 = new Date(date1);
        date2 = new Date(date2);
        return date1.getMonth() === date2.getMonth() && date1.getFullYear() === date2.getFullYear();
      },

      /**
      *   @ngdoc property
      *   @name ui.cms.service:H#date.generateRange
      *   @methodOf ui.cms.service:H
      *   @description генерация массива дат с границами от startDate до endDate
      *   @param {Date|string|number} rangeStart начальная дата для массива
      *   @param {Date|string|number} rangeEnd конечная дата для массива (массив может ее не включаь)
      *   @param {string=} [mode='d'] тип генерации. <b>'d'</b> - шаг в 1 день
      *   @returns {Array<Date>} массив дат
      *   <pre>
      *       > start = new Date('2017-03-05T04:21')
      *       > end = new Date('2017-03-08T02:00')
      *       > range = H.date.generateRange(start, end)
      *       > console.log(range)
      *       // тут, конечно, не просто строки в результате, а Date объекты
      *       [
      *           Sun Mar 05 2017 04:21:00 GMT+0200 (EET),
      *           Mon Mar 06 2017 04:21:00 GMT+0200 (EET),
      *           Tue Mar 07 2017 04:21:00 GMT+0200 (EET)
      *       ]
      *   </pre>
       */
      generateRange: function(rangeStart, rangeEnd, mode) {
        var arr, currentDate, endDate, ref, startDate;
        if (!rangeStart || !rangeEnd) {
          return [];
        }
        if (rangeStart === rangeEnd) {
          return [new Date(rangeStart)];
        }
        if (mode !== 'd' && mode !== 'm' && mode !== 'y') {
          mode = 'd';
        }
        startDate = new Date(rangeStart);
        endDate = new Date(rangeEnd);
        if (startDate > endDate) {
          ref = [endDate, startDate], startDate = ref[0], endDate = ref[1];
        }
        arr = [];
        currentDate = startDate;
        while (currentDate <= endDate) {
          arr.push(new Date(currentDate));
          switch (mode) {
            case 'd':
              currentDate.setDate(currentDate.getDate() + 1);
              break;
            case 'm':
            case 'y':
              throw 'Not implemented';
          }
        }
        return arr;
      },
      min: function() {

        /**
        *   @ngdoc property
        *   @name ui.cms.service:H#date.min
        *   @methodOf ui.cms.service:H
        *   @description возвращает минимальную дату из аргументов
        *   @param {Date|string|number} date1 дата 1
        *   @param {Date|string|number} date2 дата 2
        *   @param {Date|string|number} dateN дата N
        *   @returns {Date|string|number} минимальная дата или undefined. Всегда возвращается тот тип, который был в массиве (без приведения к Date)
        *   <pre>
        *       > start = new Date('2017-03-05T04:21')
        *       > end = new Date('2017-03-08T02:00')
        *       > H.date.min(start, end)
        *       Sun Mar 05 2017 04:21:00 GMT+0200 (EET)
        *
        *       // строка с датами:
        *       > H.date.min(start, '2014-01-12' end)
        *       '2014-01-12'
        *   </pre>
         */
        var index, items;
        if (!arguments.length) {
          return void 0;
        }
        items = toDatesArray(arguments);
        index = items.indexOf(Math.min.apply(null, items));
        return arguments[index];
      },
      max: function() {

        /**
        *   @ngdoc property
        *   @name ui.cms.service:H#date.min
        *   @methodOf ui.cms.service:H
        *   @description возвращает максимальную дату из аргументов
        *   @param {Date|string|number} date1 дата 1
        *   @param {Date|string|number} date2 дата 2
        *   @param {Date|string|number} dateN дата N
        *   @returns {Date|string|number} максимальная дата или undefined. Всегда возвращается тот тип, который был в массиве (без приведения к Date)
        *   <pre>
        *       > start = new Date('2017-03-05T04:21')
        *       > end = new Date('2017-03-07T04:00')
        *       > H.date.max(start, end)
        *       Tue Mar 07 2017 04:00:00 GMT+0200 (EET)
        *
        *       // строка, цифра с датами:
        *       > H.date.max(start, 1000, '2014-01-12' end)
        *       Tue Mar 07 2017 04:00:00 GMT+0200 (EET)
        *   </pre>
         */
        var index, items;
        if (!arguments.length) {
          return void 0;
        }
        items = toDatesArray(arguments);
        index = items.indexOf(Math.max.apply(null, items));
        return arguments[index];
      },
      isDateInRange: function(rangeStart, rangeEnd, date) {

        /**
        *   @ngdoc property
        *   @name ui.cms.service:H#date.isDateInRange
        *   @methodOf ui.cms.service:H
        *   @description функция для определения, находится ли date в диапазоне меж rangeStart и rangeEnd
        *   @param {Date|string|number} rangeStart начальная дата
        *   @param {Date|string|number} rangeEnd конечная дата
        *   @param {Date|string|number} date тестируемая дата
        *   @returns {boolean} результат
        *   <pre>
        *       > H.date.isDateInRange('2017-03-05T04:21', '2017-03-08T02:00', '2017-03-06T12:00')
        *       true
        *       > H.date.isDateInRange('2017-03-05T04:21', '2017-03-08T02:00', new Date())
        *       false
        *   </pre>
         */
        var ref;
        if (!date) {
          return false;
        }
        rangeStart = new Date(rangeStart);
        rangeEnd = new Date(rangeEnd);
        date = new Date(date);
        if (rangeStart > rangeEnd) {
          ref = [rangeEnd, rangeStart], rangeStart = ref[0], rangeEnd = ref[1];
        }
        return (rangeStart <= date && date <= rangeEnd);
      },
      isDateRangeInRange: function(rangeStart, rangeEnd, dateStart, dateEnd) {

        /**
        *   @ngdoc property
        *   @name ui.cms.service:H#date.isDateRangeInRange
        *   @methodOf ui.cms.service:H
        *   @description функция для определения, находится ли dateStart...dateEnd в диапазоне меж rangeStart...rangeEnd. Если dateStart или dateEnd находятся за пределом
        *       диапазона rangeStart...rangeEnd, то результат будет false
        *   @param {Date|string|number} rangeStart начальная дата
        *   @param {Date|string|number} rangeEnd конечная дата
        *   @param {Date|string|number} dateStart тестируемое начало диапазона
        *   @param {Date|string|number} dateEnd тестируемый конец диапазона
        *   @returns {boolean} результат
        *   <pre>
        *       > H.date.isDateRangeInRange(
        *           '2017-03-05T04:21', '2017-03-08T02:00',
        *           '2017-03-06T12:00', '2017-03-06T15:30'
        *       )
        *       true
        *   </pre>
         */
        var ref, ref1;
        if (!dateStart || !dateEnd) {
          return false;
        }
        rangeStart = new Date(rangeStart);
        rangeEnd = new Date(rangeEnd);
        dateStart = new Date(dateStart);
        dateEnd = new Date(dateEnd);
        if (rangeStart > rangeEnd) {
          ref = [rangeEnd, rangeStart], rangeStart = ref[0], rangeEnd = ref[1];
        }
        if (dateStart > dateEnd) {
          ref1 = [dateEnd, dateStart], dateStart = ref1[0], dateEnd = ref1[1];
        }
        return (rangeStart <= dateStart && dateStart <= rangeEnd) && (rangeStart <= dateEnd && dateEnd <= rangeEnd);
      },
      isDateRangeIntersectsRange: function(rangeStart, rangeEnd, dateStart, dateEnd) {

        /**
        *   @ngdoc property
        *   @name ui.cms.service:H#date.isDateRangeInRange
        *   @methodOf ui.cms.service:H
        *   @description функция для определения, пересекает ли dateStart...dateEnd диапазон дат rangeStart...rangeEnd.
        *   @param {Date|string|number} rangeStart начальная дата
        *   @param {Date|string|number} rangeEnd конечная дата
        *   @param {Date|string|number} dateStart тестируемое начало диапазона
        *   @param {Date|string|number} dateEnd тестируемый конец диапазона
        *   @returns {boolean} результат
        *   <pre>
        *       > H.date.isDateRangeInRange(
        *           '2017-03-05T04:21', '2017-03-08T02:00',
        *           '2017-03-06T12:00', '2018-03-06T15:30'
        *       )
        *       true
        *       > H.date.isDateRangeInRange(
        *           '2017-03-05T04:21', '2017-03-08T02:00',
        *           '2015-03-06T12:00', '2018-03-06T15:30'
        *       )
        *       true
        *       > H.date.isDateRangeInRange(
        *           '2017-03-05T04:21', '2017-03-08T02:00',
        *           '2018-03-06T12:00', '2018-03-06T15:30'
        *       )
        *       false
        *   </pre>
         */
        var ref, ref1;
        if (!dateStart || !dateEnd) {
          return false;
        }
        rangeStart = new Date(rangeStart);
        rangeEnd = new Date(rangeEnd);
        dateStart = new Date(dateStart);
        dateEnd = new Date(dateEnd);
        if (rangeStart > rangeEnd) {
          ref = [rangeEnd, rangeStart], rangeStart = ref[0], rangeEnd = ref[1];
        }
        if (dateStart > dateEnd) {
          ref1 = [dateEnd, dateStart], dateStart = ref1[0], dateEnd = ref1[1];
        }
        if ((rangeStart <= dateStart && dateStart <= rangeEnd) || (rangeStart <= dateEnd && dateEnd <= rangeEnd)) {
          return true;
        }
        if (dateStart < rangeStart && dateEnd > rangeEnd) {
          return true;
        }
        return false;
      }
    };
    this.duration = {
      parse: function(durationString) {

        /**
        *   @ngdoc property
        *   @name ui.cms.service:H#duration.parse
        *   @methodOf ui.cms.service:H
        *   @description преобразовывает строковое представление DurationField в объект пригодный для js
        *   @param {string|number} durationString целое число в секундах ИЛИ строка вида '[DD] [HH:[MM:]]ss[.uuuuuu]'  Подробнее см: https://docs.djangoproject.com/en/2.0/_modules/django/db/models/fields/#DurationField
        *   @returns {object} oбъект с ключами milliseconds, seconds, minutes, days
        *   <pre>
        *       > H.duration.parse("5 10:30:50")
        *       {
        *           days: 5,
        *           hours: 10,
        *           minutes: 30,
        *           seconds: 50,
        *           milliseconds: 0
        *       }
        *       // преобразовываем одну из кратких записей (минуты + секунды):
        *       > H.date.parse("5:59")
        *       {
        *           days: 0,
        *           hours: 0,
        *           minutes: 5,
        *           seconds: 59,
        *           milliseconds: 0
        *       }
        *   </pre>
         */
        var days, duration, hours, milliseconds, minutes, seconds;
        if (angular.isNumber(durationString)) {
          seconds = durationString;
          days = Math.floor(seconds / 60 / 60 / 24);
          seconds -= days * 60 * 60 * 24;
          hours = Math.floor(seconds / 60 / 60);
          seconds -= hours * 60 * 60;
          minutes = Math.floor(seconds / 60);
          seconds -= minutes * 60;
          return {
            seconds: Math.floor(seconds),
            minutes: minutes,
            hours: hours,
            days: days,
            milliseconds: 0
          };
        }
        duration = durationString || '';
        duration = duration.split('.');
        milliseconds = toInt(duration[1], 0);
        duration = (duration[0] || '').split(' ');
        if (duration.length === 1) {
          days = 0;
          duration = duration[0];
        } else {
          days = toInt(duration[0], 0);
          duration = duration[1];
        }
        duration = (duration || '').split(':');
        seconds = duration[duration.length - 1];
        minutes = duration[duration.length - 2];
        hours = duration[duration.length - 3];
        return {
          days: days,
          hours: toInt(hours, 0),
          minutes: toInt(minutes, 0),
          seconds: toInt(seconds, 0),
          milliseconds: milliseconds
        };
      },
      dump: function(durationObj) {

        /**
        *   @ngdoc property
        *   @name ui.cms.service:H#duration.dump
        *   @methodOf ui.cms.service:H
        *   @description преобразовывает объектное представление DurationField в строку, пригодный для БД и приложений
        *   @param {object} durationObj oбъект с ключами milliseconds, seconds, minutes, days
        *   @returns {string} строка вида '[DD] [HH:[MM:]]ss[.uuuuuu]'  Подробнее см: https://docs.djangoproject.com/en/2.0/_modules/django/db/models/fields/#DurationField
        *   <pre>
        *       > H.duration.dump({
        *           days: 450,
        *           hours: 23,
        *           minutes: 0,
        *           seconds: 10,
        *           milliseconds: 20
        *       })
        *       "450 23:00:10.20"
        *       // преобразовываем одну из кратких записей (минуты + секунды):
        *       > H.date.dump({
        *           days: 0,
        *           hours: 0,
        *           minutes: 50,
        *           seconds: 10,
        *           milliseconds: 0
        *       })
        *       "50:10"
        *   </pre>
         */
        var days, durationString, hours, milliseconds, minutes, padLeft, seconds;
        padLeft = function(value) {
          if (value < 10) {
            return "0" + value;
          }
          return "" + value;
        };
        milliseconds = durationObj.milliseconds || 0;
        seconds = durationObj.seconds || 0;
        minutes = durationObj.minutes || 0;
        hours = durationObj.hours || 0;
        days = durationObj.days || 0;
        durationString = "";
        if (milliseconds) {
          durationString = durationString + "." + milliseconds;
        }
        durationString = "" + (padLeft(seconds)) + durationString;
        if (minutes || hours || days) {
          durationString = (padLeft(minutes)) + ":" + durationString;
        }
        if (hours || days) {
          durationString = (padLeft(hours)) + ":" + durationString;
        }
        if (days) {
          durationString = days + " " + durationString;
        }
        return durationString;
      }
    };
    this.urlParams = {
      parse: function(url) {

        /**
        *   @ngdoc property
        *   @name ui.cms.service:H#urlParams.parse
        *   @methodOf ui.cms.service:H
        *   @description преобразовывает строковое представление query_params от url в массив пар ключ+значение
        *   @param {string} url строка url
        *   @returns {array} массив массивов значений
        *   <pre>
        *       > H.urlParams.parse("localhost:8000/ru?param1=test&param1=world&param2&param3=true")
        *       [
        *           ["param1", "test"],
        *           ["param1", "world"],
        *           ["param2", ""],
        *           ["param3", "true"],
        *       ]
        *   </pre>
         */
        var i, len, p, pairs, ref;
        pairs = [];
        ref = (url.split('?')[1] || '').split('&');
        for (i = 0, len = ref.length; i < len; i++) {
          p = ref[i];
          p = p.split('=');
          if (p[0]) {
            pairs.push([p[0], decodeURIComponent(p[1] || '')]);
          }
        }
        return pairs;
      },
      dump: function(urlParams) {

        /**
        *   @ngdoc property
        *   @name ui.cms.service:H#urlParams.dump
        *   @methodOf ui.cms.service:H
        *   @description преобразовывает массив массивов query_params (или объект) пригодный для подстановки к url объект
        *   @param {Array|Object} urlParams параметры
        *   @returns {string} подготовленная строка
        *   <pre>
        *       // массив
        *       > H.urlParams.dump([
        *           ["param1", "test"],
        *           ["param1", "world"],
        *           ["param2", ""],
        *           ["param3", "true"],
        *       ])
        *       "param1=test&param1=world&param2&param3=true"
        *       // объект
        *       > H.urlParams.dump({
        *           param1: "text",
        *           param2: true,
        *           param3: null,
        *           param4: undefined
        *       })
        *       "param1=text&param2=true&param3"
        *   </pre>
         */
        var dumpValue, i, j, k, keys, len, len1, p, params, ref, v;
        params = [];
        dumpValue = function(value) {
          if (angular.isArray(value)) {
            value = value.map(dumpValue);
            return value.join(',');
          }
          if (angular.isObject(value)) {
            return JSON.stringify(value);
          }
          return value + '';
        };
        if (angular.isArray(urlParams)) {
          urlParams.sort(function(a, b) {
            if (a[0] < b[0]) {
              return -1;
            }
            if (a[0] > b[0]) {
              return 1;
            }
            return 0;
          });
          for (i = 0, len = urlParams.length; i < len; i++) {
            p = urlParams[i];
            if (angular.isDefined(p[1])) {
              if ((ref = p[1]) !== null && ref !== '') {
                params.push(p[0] + "=" + (encodeURIComponent(dumpValue(p[1]))));
              } else {
                params.push(p[0]);
              }
            }
          }
        } else {
          keys = Object.keys(urlParams || {});
          keys.sort();
          for (j = 0, len1 = keys.length; j < len1; j++) {
            k = keys[j];
            v = urlParams[k];
            if (!angular.isDefined(v)) {
              continue;
            }
            if (v !== null && v !== '') {
              params.push(k + "=" + (encodeURIComponent(dumpValue(v))));
            } else {
              params.push(k);
            }
          }
        }
        return params.join('&');
      }
    };

    /**
    *   @ngdoc property
    *   @name ui.cms.service:H#convert
    *   @propertyOf ui.cms.service:H
    *   @description
    *       функции для преобразования. См ниже
     */
    this.convert = {

      /**
      *   @ngdoc property
      *   @name ui.cms.service:H#convert.objToArray
      *   @methodOf ui.cms.service:H
      *   @description преобразовывает объект по ключам в массив {id: КЛЮЧ, data: ЗНАЧЕНИЕ}
      *   @param {object} value объект
      *   @returns {Array} преобразованный массив
      *   <pre>
      *       > H.convert.objToArray({a:1, b:2, c:33})
      *       [
      *           {id: a, data: 1},
      *           {id: b, data: 2},
      *           {id: c, data: 33}
      *       ]
      *   </pre>
       */
      objToArray: function(obj) {
        var items, k, ref, v;
        items = [];
        ref = obj || {};
        for (k in ref) {
          v = ref[k];
          items.push({
            id: k,
            data: v
          });
        }
        return items;
      },

      /**
      *   @ngdoc property
      *   @name ui.cms.service:H#convert.arrayToObj
      *   @methodOf ui.cms.service:H
      *   @description преобразовывает массив в объект, где ключи это id элементов
      *   @param {Array<Object>} items массив объектов
      *   @returns {object} преобразованный объект
      *   <pre>
      *       > H.convert.objToArray([{id: 1, data: "hello"}, {id: 33, data: "world", value: "!"}])
      *       {
      *           1: {
      *               id: 1,
      *               data: "hello"
      *           }
      *           33: {
      *               id: 33,
      *               data: "world",
      *               value: "!"
      *           }
      *       }
      *   </pre>
       */
      arrayToObj: function(items) {
        var i, item, len, obj, ref;
        obj = {};
        ref = items || [];
        for (i = 0, len = ref.length; i < len; i++) {
          item = ref[i];
          obj[item.id] = item;
        }
        return obj;
      },
      objToHtmlProps: function(obj) {
        var k, props, ref, toHtmlPropName, v;
        props = [];
        toHtmlPropName = function(text) {
          return text.replace(/\.?([A-Z])/g, function(x, y) {
            return '-' + y.toLowerCase();
          }).replace(/^_/, '');
        };
        ref = obj || {};
        for (k in ref) {
          v = ref[k];
          if (k[0] !== '$') {
            props.push((toHtmlPropName(k)) + "='" + v + "'");
          }
        }
        return props.join(' ');
      },
      camelCaseToDash: function(text) {
        return text.replace(/([a-zA-Z])(?=[A-Z])/g, '$1-').toLowerCase().trim();
      },

      /**
      *   @ngdoc property
      *   @name ui.cms.service:H#convert.currency
      *   @methodOf ui.cms.service:H
      *   @description конвертирует значение из 1 валюты в другую. Обязательно наличие в CmsSettings объекте ($cms.settings) ключей defaultCurrencyConverter и defaultCurrency
      *   @param {number} value значение
      *   @param {string} fromCurrency валюта из которой переводят
      *   @param {string} [toCurrency=$cms.settings.defaultCurrency] валюта в которую переводят
      *   @returns {number} преобразованое значение
      *   <pre>
      *       > H.convert.currency(10, 'EUR', 'CZK')
      *       224
      *   </pre>
       */
      currency: (function(_this) {
        return function(value, fromCurrency, toCurrency) {
          var converter, k_from, k_to;
          value = value || 0;
          if (!value) {
            return value;
          }
          $cms = $cms || $injector.get('$cms');
          fromCurrency = (fromCurrency + '').toUpperCase();
          toCurrency = ((toCurrency || $cms.settings.defaultCurrency) + '').toUpperCase();
          if (!fromCurrency || !toCurrency) {
            throw "Please provide fromCurrency and toCurrency arguments";
          }
          converter = $cms.settings.defaultCurrencyConverter || {};
          k_from = (converter[$cms.settings.defaultCurrency] || {})[fromCurrency];
          k_to = (converter[$cms.settings.defaultCurrency] || {})[toCurrency];
          if (!k_from || !k_to) {
            throw "Can't convert value";
          }
          return value / k_from * k_to;
        };
      })(this),

      /**
      *   @ngdoc property
      *   @name ui.cms.service:H#convert.toInt
      *   @methodOf ui.cms.service:H
      *   @description преобразовывает значение в целое число. Если преобразование не удалось, то вернется значение по умолчанию
      *   @param {string|number} value значение
      *   @param {number=} [defaultValue=undefined] значение, которое будет возвращено, если value не получится преобразовать
      *   @returns {number} преобразованое значение
      *   <pre>
      *       > H.convert.toInt("10", 20)
      *       10
      *       > H.convert.toInt("10.15", 20)
      *       10
      *       > H.convert.toInt("10.6", 20)
      *       10
      *       > H.convert.toInt("wtf?", 20)
      *       20
      *       > H.convert.toInt(10.20, 20)
      *       10
      *   </pre>
       */
      toInt: toInt,

      /**
      *   @ngdoc property
      *   @name ui.cms.service:H#convert.toFloat
      *   @methodOf ui.cms.service:H
      *   @description преобразовывает значение в дробное число. Если преобразование не удалось, то вернется значение по умолчанию
      *   @param {string|number} value значение
      *   @param {number=} [defaultValue=undefined] значение, которое будет возвращено, если value не получится преобразовать
      *   @returns {number} преобразованое значение
      *   <pre>
      *       > H.convert.toFloat("10", 20)
      *       10.0
      *       > H.convert.toFloat("10.15", 20)
      *       10.15
      *       > H.convert.toFloat("10.6", 20)
      *       10.6
      *       > H.convert.toFloat("wtf?", 20)
      *       20
      *       > H.convert.toFloat(10.20, 20)
      *       10.2
      *   </pre>
       */
      toFloat: toFloat,

      /**
      *   @ngdoc property
      *   @name ui.cms.service:H#convert.toDate
      *   @methodOf ui.cms.service:H
      *   @description преобразовывает значение в Date. Если преобразование не удалось, то вернется значение по умолчанию
      *   @param {string|Date|number} value значение
      *   @param {Date=} [defaultValue=undefined] значение, которое будет возвращено, если value не получится преобразовать
      *   @returns {Date} преобразованое значение
      *   <pre>
      *   </pre>
       */
      toDate: function(value, defaultValue) {
        if (value === null || value === (void 0)) {
          return defaultValue;
        }
        value = new Date(value);
        if (isNaN(value.getTime())) {
          return defaultValue;
        }
        return value;
      }
    };

    /**
    *   @ngdoc property
    *   @name ui.cms.service:H#downloadDataAsFile
    *   @methodOf ui.cms.service:H
    *   @description
    *        скачивает на компьютер пользователя файл с названием filename и содержанием data.
    *        Подробно посмотреть таблицу совместимости можно тут: https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/Data_URIs#Browser_compatibility
    *   @param {string} filename название файла
    *   @param {string|Object} data данные для скачивания
    *   @param {Object=} [options={mime: 'text/plain'}] опции. Можно указать mime-тип файла
    *   <pre>
    *       > H.downloadDataAsFile('mytext.txt', 'hello world!')
    *       // сейчас ваш браузер должен выкачать файл mytext.txt с содержимым 'hello world!'
    *   </pre>
     */
    this.downloadDataAsFile = function(filename, data, options) {
      var event, file, link;
      options = options || {};
      options.mime = options.mime || 'text/plain';
      link = document.createElement('a');
      link.download = filename;
      if (!options.blob) {
        if (angular.isObject(data) || angular.isArray(data)) {
          data = JSON.stringify(data, null, 4);
        } else if (angular.isString(data)) {
          data = data.replaceAll(/\n/g, '\r\n');
        }
        link.href = ("data:" + options.mime + ";base64,") + btoa(data);
      } else {
        file = new Blob([data], {
          type: options.mime
        });
        link.href = URL.createObjectURL(file);
      }
      document.body.appendChild(link);
      try {
        event = new MouseEvent('click', {
          bubbles: true,
          cancelable: true,
          view: window
        });
        event.isTrusted = true;
        link.dispatchEvent(event);
      } catch (error1) {
        error = error1;
        link.click();
      }
      document.body.removeChild(link);
      delete link;
    };

    /**
    *   @ngdoc property
    *   @name ui.cms.service:H#execUrl
    *   @methodOf ui.cms.service:H
    *   @description
    *       выполняет переданный url так, будто пользователь кликнул по ссылке с этим url.
    *   @param {string} url полный или относительный url
    *   @param {string=} target тип открытия. По умолчанию - _blank. В: '_blank', '_self'.
    *   <pre>
    *       > H.execUrl('/myurl.txt')
    *       // сейчас ваш браузер должен открыть эту ссылку
    *   </pre>
     */
    this.execUrl = function(url, target) {
      var event, link;
      if (!url) {
        return;
      }
      if (target !== '_blank' && target !== '_self') {
        target = '_blank';
      }
      url += '';
      if (url[0] === '/') {
        url = "" + location.origin + url;
      }
      if (target === '_self') {
        document.location.href = url;
      }
      link = document.createElement('a');
      link.href = url;
      link.target = target;
      document.body.appendChild(link);
      try {
        link.click();
      } catch (error1) {
        error = error1;
        event = new MouseEvent('click', {
          bubbles: true,
          cancelable: true,
          view: window
        });
        event.isTrusted = true;
        link.dispatchEvent(event);
      }
      document.body.removeChild(link);
      delete link;
    };

    /**
    *   @ngdoc property
    *   @name ui.cms.service:H#copyToClipboard
    *   @methodOf ui.cms.service:H
    *   @description
    *       копирует в clipboard строки/объекты
    *   @param {any} content значение
    *   <pre>
    *       > H.copyToClipboard({text: 'hello'})
    *   </pre>
     */
    copyToClipboardFallback = function(content) {
      var copyText;
      copyText = document.createElement("textarea");
      copyText.value = content + '';
      document.body.appendChild(copyText);
      copyText.select();
      document.execCommand("copy");
      document.body.removeChild(copyText);
    };
    this.copyToClipboard = function(content, indent) {
      var defer, result;
      if (!angular.isNumber(indent) || isNaN(indent) || indent < 0) {
        indent = 4;
      }
      if (angular.isObject(content)) {
        content = JSON.stringify(content, null, indent);
      } else if (angular.isDate(content)) {
        content = content.toISOString();
      }
      if ((navigator.clipboard || {}).writeText) {
        result = navigator.clipboard.writeText(content);
        if (result && result.then) {
          defer = $q.defer();
          result.then(function(data) {
            $timeout(function() {
              return defer.resolve(data);
            });
          }, function(error) {
            copyToClipboardFallback(content);
            $timeout(function() {
              return defer.reject(error);
            });
          });
          return defer.promise;
        }
      } else {
        copyToClipboardFallback(content);
      }
      return $q(function(resolve) {
        resolve();
      });
    };
    this.iframe = {
      isInIframe: function() {
        try {
          return window.self !== window.top;
        } catch (error1) {
          error = error1;
          return true;
        }
      },
      getIframeAllowedFeatures: function() {
        try {
          return document.featurePolicy.allowedFeatures();
        } catch (error1) {
          error = error1;
          return [];
        }
      }
    };
    isTablsetScope = function(keys) {
      if (!(keys.includes('vertical') && keys.includes('tabset') && keys.includes('justified'))) {
        return false;
      }
      if (keys.length === 4 && keys.includes('activeHeadingHtml')) {
        return true;
      }
      return keys.length === 3;
    };
    this.findOrigScope = function($scope, $attrs) {
      var findOrigScope;
      $attrs = $attrs || {};
      findOrigScope = function(scope) {
        var keys;
        if (scope.$origScope) {
          return scope.$origScope;
        }
        if (scope.$id === 1) {
          return scope;
        }
        keys = Object.keys(scope).filter(function(k) {
          return k.indexOf('$') !== 0;
        });
        if (!keys.length || isTablsetScope(keys)) {
          return findOrigScope(scope.$parent);
        }
        return scope;
      };
      if ($attrs.hasOwnProperty('ngIf') || $attrs.hasOwnProperty('ngSwitchWhen')) {
        return findOrigScope($scope.$parent.$parent);
      }
      return findOrigScope($scope.$parent);
    };
    return this;
  }]);

}).call(this);
;
(function() {
  angular.module('ui.cms').factory('ItemsPaginator', ["$filter", "$cms", "$compile", "H", function($filter, $cms, $compile, H) {
    var PAGINATOR_TEMPLATE, calculatePagesForItems, ngFilter, ngLimitTo;
    ngFilter = $filter('filter');
    ngLimitTo = $filter('limitTo');
    PAGINATOR_TEMPLATE = "<div uib-pagination='' class=\"pagination-sm\"\n        ng-if=\"$itemsPaginatorOptions.totalPages > 1\"\n        ng-model=\"$itemsPaginatorOptions.page\"\n        total-items=\"$itemsPaginatorOptions.totalPages\"\n        items-per-page=\"1\"\n        max-size=\"$itemsPaginatorOptions.getMaxSize()\"\n\n        boundary-link-numbers=\"true\"\n        rotate=\"false\"\n        next-text=\"{{'Next' | translate}}\"\n        previous-text=\"{{'Previous' | translate}}\">\n</div>";
    calculatePagesForItems = function(items, itemsPerPage) {
      var l, pages;
      pages = items.length / itemsPerPage;
      l = pages % 1;
      if (l > 0) {
        pages = pages - l + 1;
      }
      return pages;
    };
    return function($scope, options, filterFn) {
      var element, filterItems, getItemsPerPage, getSearchTerm, skip_watch;
      options = options || {};
      options.paths = options.paths || {};
      options.paths.items = options.paths.items || 'items';
      skip_watch = false;
      $scope.$itemsPaginatorOptions = $scope.$itemsPaginatorOptions || {
        page: 1,
        totalPages: 1,
        isFiltered: false
      };
      if (!$scope.$itemsPaginatorOptions.getMaxSize) {
        $scope.$itemsPaginatorOptions.getMaxSize = function() {
          if ($cms.screenSize === 'xs') {
            return 3;
          }
          return 10;
        };
      }
      if (options.element && options.element.replaceWith) {
        element = $compile(PAGINATOR_TEMPLATE)($scope);
        options.element.replaceWith(element);
      } else {
        console.warn('ItemsPaginator: please, provide "options" parameter with property "element" for template generation');
      }
      getSearchTerm = function() {
        if (!options.paths.searchTerm) {
          return;
        }
        return angular.getValue($scope, options.paths.searchTerm);
      };
      getItemsPerPage = function() {
        if (!options.paths.itemsPerPage && !options.itemsPerPage) {
          return 15;
        }
        if (options.paths.itemsPerPage) {
          return angular.getValue($scope, options.paths.itemsPerPage) || 15;
        }
        return options.itemsPerPage || 15;
      };
      filterItems = function(items) {
        var _items, itemsPerPage, search;
        if (!items || !items.length) {
          return items;
        }
        search = getSearchTerm();
        itemsPerPage = getItemsPerPage();
        if (search) {
          _items = ngFilter(items, search);
        } else {
          _items = items;
        }
        if (angular.isFunction(filterFn)) {
          _items = filterFn(_items);
        }
        $scope.filteredItems = _items;
        $scope.$itemsPaginatorOptions.totalPages = calculatePagesForItems(_items, itemsPerPage);
        if ($scope.$itemsPaginatorOptions.page && $scope.$itemsPaginatorOptions.page > 1) {
          if ($scope.$itemsPaginatorOptions.page > $scope.$itemsPaginatorOptions.totalPages) {
            $scope.$itemsPaginatorOptions.page = $scope.$itemsPaginatorOptions.totalPages;
          }
        }
        $scope.$itemsPaginatorOptions.isFiltered = _items.length !== items.length;
        _items = ngLimitTo(_items, itemsPerPage, itemsPerPage * ($scope.$itemsPaginatorOptions.page - 1));
        return _items;
      };
      $scope.$watchCollection(options.paths.items, function(items, oldValue) {
        $scope.itemsOnPage = filterItems(items || []);
        window.smoothScrollTo(0);
      }, true);
      $scope.$watch('$itemsPaginatorOptions.page', function(page, oldValue) {
        if (skip_watch) {
          skip_watch = false;
          return;
        }
        $scope.itemsOnPage = filterItems(angular.getValue($scope, options.paths.items));
        window.smoothScrollTo(0);
      });
      if (options.paths.searchTerm) {
        $scope.$watch(options.paths.searchTerm, function(searchTerm, oldValue) {
          if (searchTerm !== oldValue) {
            $scope.$itemsPaginatorOptions.page = 1;
            $scope.itemsOnPage = filterItems(angular.getValue($scope, options.paths.items));
            window.smoothScrollTo(0);
            skip_watch = true;
          }
        });
      }
      return function() {
        $scope.itemsOnPage = filterItems(angular.getValue($scope, options.paths.items));
        window.smoothScrollTo(0);
      };
    };
  }]);

}).call(this);
;
(function() {
  angular.module('ui.cms').factory('HtmlGenerator', function() {
    var HtmlGenerator;
    return HtmlGenerator = (function() {
      function HtmlGenerator(ngModel, options) {
        this.ngModel = ngModel;

        /*
            this.vars - поскольку ангулар в сгенерированном html не будет знать
            о ссылках на объекты в каждом из пунктов меню,
            создаем хранилище переменных title из которого и будут браться
            строки для перевода.
         */
        this.vars = [];
        this.html = "";
        this.options = options || {};
      }

      HtmlGenerator.prototype.objToAttrsString = function(obj, propName) {
        var k, ref, res, v;
        if (!obj || !obj[propName]) {
          return '';
        }
        res = '';
        ref = obj[propName];
        for (k in ref) {
          v = ref[k];
          res = res + (" " + k + "='" + v + "'");
        }
        return res;
      };

      HtmlGenerator.prototype.normalizeUrl = function(url) {
        if (url.indexOf('http://') !== 0 && url.indexOf('https://') !== 0 && url[0] !== '/' && url[0] !== '#') {
          url = "http://" + url;
        }
        return url;
      };

      HtmlGenerator.prototype.addVar = function(data) {
        this.vars.push(data);
        return this.vars.length - 1;
      };

      HtmlGenerator.prototype.isL10nObjectEmpty = function(l10nObject) {
        var empty, k, v;
        if (!Object.keys(l10nObject || {}).length) {
          return true;
        }
        empty = true;
        for (k in l10nObject) {
          v = l10nObject[k];
          if (v) {
            empty = false;
            break;
          }
        }
        return empty;
      };

      HtmlGenerator.prototype.render = function() {
        return '';
      };

      return HtmlGenerator;

    })();
  });

}).call(this);
;
(function() {
  angular.module('ui.cms').service('$tracking', ["$window", "$timeout", "$location", "H", function($window, $timeout, $location, H) {
    var execFbPixelQueue, execGTagQueue, execYaMetricaQueue, fbPixelQueue, gtagId, gtagQueue, start, yaMetricaId, yaMetricaQueue;
    gtagId = null;
    yaMetricaId = null;
    gtagQueue = [];
    yaMetricaQueue = [];
    fbPixelQueue = [];
    start = (new Date()).getTime();
    execGTagQueue = function() {
      var diff, i, len, now, o, params;
      if (!$window.gtag || !gtagId) {
        now = (new Date()).getTime();
        diff = (now - start) / 1000;
        if (diff > 60 * 5) {
          return;
        }
        if (diff > 15) {
          setTimeout(execGTagQueue, 15000);
        } else {
          setTimeout(execGTagQueue, 2000);
        }
        return;
      }
      for (i = 0, len = gtagQueue.length; i < len; i++) {
        o = gtagQueue[i];
        params = o.map(function(prop) {
          if (angular.isFunction(prop)) {
            return prop();
          }
          return prop;
        });
        $window.gtag.apply($window.gtag, params);
      }
      gtagQueue = [];
    };
    execYaMetricaQueue = function() {
      var diff, i, len, now, o, yam;
      yam = $window["yaCounter" + yaMetricaId];
      if (!yam || !yaMetricaId) {
        now = (new Date()).getTime();
        diff = (now - start) / 1000;
        if (diff > 60 * 5) {
          return;
        }
        if (diff > 15) {
          setTimeout(execYaMetricaQueue, 15000);
        } else {
          setTimeout(execYaMetricaQueue, 2000);
        }
        return;
      }
      for (i = 0, len = yaMetricaQueue.length; i < len; i++) {
        o = yaMetricaQueue[i];
        yam[o.method].apply(yam, o.params);
      }
      yaMetricaQueue = [];
    };
    execFbPixelQueue = function() {
      var diff, i, len, now, o;
      if (!$window.fbq) {
        now = (new Date()).getTime();
        diff = (now - start) / 1000;
        if (diff > 60 * 5) {
          return;
        }
        if (diff > 15) {
          setTimeout(execFbPixelQueue, 15000);
        } else {
          setTimeout(execFbPixelQueue, 2000);
        }
        return;
      }
      for (i = 0, len = fbPixelQueue.length; i < len; i++) {
        o = fbPixelQueue[i];
        $window.fbq.apply($window.fbq, o.params);
      }
      fbPixelQueue = [];
    };
    this.setGTagId = function(id) {
      gtagId = id;
    };
    this.setYaMetricaId = function(id) {
      yaMetricaId = id;
    };
    this.sendPageview = function(options) {
      var gaParams, k, url, v, yaParams;
      options || (options = {});
      options.path = options.path || $location.path();
      options.title = document.title;
      gaParams = {};
      for (k in options) {
        v = options[k];
        if ((k === 'title' || k === 'location' || k === 'path') && v) {
          gaParams["page_" + k] = v;
        }
      }
      gtagQueue.push([
        'config', function() {
          return gtagId;
        }, gaParams
      ]);
      execGTagQueue();
      yaParams = angular.copy(options);
      url = yaParams.path;
      delete yaParams.url;
      delete yaParams.path;
      yaMetricaQueue.push({
        method: 'hit',
        params: [url, yaParams]
      });
      execYaMetricaQueue();
      fbPixelQueue.push({
        params: ['track', 'PageView']
      });
      execFbPixelQueue();
    };
    this.sendEvent = function(action, options) {
      var gaParams, i, k, len, p, ref, v;
      if (!action) {
        return;
      }
      options || (options = {});
      gaParams = {};
      for (k in options) {
        v = options[k];
        if (k === 'category' || k === 'label') {
          gaParams["event_" + k] = v;
        }
      }
      ref = ['value', 'non_interaction'];
      for (i = 0, len = ref.length; i < len; i++) {
        p = ref[i];
        if (options.hasOwnProperty(p)) {
          gaParams[p] = options[p];
        }
      }
      gtagQueue.push(gaParams);
      execGTagQueue();
    };
    return this;
  }]);

}).call(this);
;
(function() {
  var indexOf = [].indexOf || function(item) { for (var i = 0, l = this.length; i < l; i++) { if (i in this && this[i] === item) return i; } return -1; };

  angular.module('ui.cms').provider('$uicIntegrations', function() {
    var $langPicker, $q, $rootScope, $timeout, $tracking, $window, H, SERVICES, chatflowIo, custom, disqusCom, facebookPixel, findDisqusId, findYandexMetricaId, googleAdSense, googleAnalytics, googleSearchConsole, googleTagManager, isAdminApp, jivosite, strToNode, thirdPartyLibs, thirdPartyLibsCbs, uicIntegrations, yandexMetrica, yandexWebmaster;
    $langPicker = null;
    $window = null;
    $timeout = null;
    $rootScope = null;
    $tracking = null;
    $q = null;
    H = null;
    strToNode = function(htmlStr) {
      var el;
      htmlStr = htmlStr.replace(/<!--[\s\S]*?-->/g, '');
      htmlStr = htmlStr.replace(/(\r\n|\n|\r)/gm, '');
      el = document.createElement('div');
      el.innerHTML = htmlStr;
      return el.firstChild;
    };
    isAdminApp = (function() {
      var base, base_href;
      base = document.head.getElementsByTagName('base');
      if (base && base[0]) {
        base_href = base[0].getAttribute('href');
        if (base_href === '/admin/') {
          return true;
        }
      }
      return false;
    })();
    googleSearchConsole = {
      gdprRequired: false,
      isService: function(userData) {
        if (typeof userData !== 'string') {
          return false;
        }
        return userData.indexOf('<meta name="google-site-verification"') > -1;
      },
      run: function(userData) {
        document.getElementsByTagName('head')[0].appendChild(strToNode(userData));
      },
      getInitLink: function() {
        var url;
        url = location.origin;
        return "https://www.google.com/webmasters/verification/verification?hl=" + $langPicker.currentLang + "&siteUrl=" + url + "&priorities=vmeta&tid=alternate";
      },
      getExternalLink: function(userData) {
        var url;
        url = location.origin;
        return "https://www.google.com/webmasters/tools/dashboard?hl=" + $langPicker.currentLang + "&siteUrl=" + url;
      }
    };
    yandexWebmaster = {
      gdprRequired: false,
      isService: function(userData) {
        if (typeof userData !== 'string') {
          return false;
        }
        return userData.indexOf('<meta name="yandex-verification"') > -1;
      },
      run: function(userData) {
        if (isAdminApp) {
          return;
        }
        document.getElementsByTagName('head')[0].appendChild(strToNode(userData));
      },
      getInitLink: function() {
        var url;
        url = location.protocol + location.hostname;
        return "https://webmaster.yandex.ru/site/" + url + ":80/access/";
      },
      getExternalLink: function(userData) {
        var url;
        url = location.protocol + location.hostname;
        return "https://webmaster.yandex.ru/site/" + url + ":80/dashboard/";
      }
    };
    googleAnalytics = {
      gdprRequired: true,
      isService: function(userData) {
        if (typeof userData !== 'string') {
          return false;
        }
        if (userData.indexOf('https://www.google-analytics.com/analytics.js') === -1 || userData.indexOf('<script>') === -1) {
          return false;
        }
        return userData.indexOf('GoogleAnalyticsObject') > -1;
      },
      run: function(userData) {
        var el;
        if (isAdminApp) {
          return;
        }
        el = strToNode(userData);
        eval(el.text);
      },
      getInitLink: function() {},
      getExternalLink: function(userData) {
        return "https://analytics.google.com/analytics/web/";
      }
    };
    googleTagManager = {
      gdprRequired: true,
      isService: function(userData) {
        if (typeof userData !== 'string') {
          return false;
        }
        if (userData.indexOf('https://www.googletagmanager.com/gtag') === -1 || userData.indexOf('<script>') === -1) {
          return false;
        }
        return userData.indexOf('function gtag()') > -1;
      },
      run: function(userData) {
        var el, gtagId, index, j, k, len, len1, line, ref, t, text;
        if (isAdminApp) {
          return;
        }
        gtagId = null;
        ref = angular.element(userData);
        for (j = 0, len = ref.length; j < len; j++) {
          el = ref[j];
          if (el.nodeName.toLowerCase() === 'script') {
            if (el.src) {
              H.loadOnce.js(el.src);
            } else {
              eval(el.innerText);
              text = (el.innerText || '').split('\n');
              for (k = 0, len1 = text.length; k < len1; k++) {
                t = text[k];
                index = t.indexOf('gtag(\'config\',');
                if (index === -1) {
                  index = t.indexOf('gtag("config",');
                }
                if (index === -1) {
                  continue;
                }
                line = t.substr(index + 'gtag("config",'.length);
                line = line.split(')')[0].trim();
                line = line.replaceAll('"', '').replaceAll("'", '');
                if (indexOf.call(line, '-') >= 0) {
                  gtagId = line;
                  break;
                }
              }
            }
          }
        }
        window.gtag = gtag;
        if (!gtagId) {
          return;
        }
        $tracking.setGTagId(gtagId);
        $tracking.sendPageview();
      },
      getInitLink: function() {},
      getExternalLink: function(userData) {
        return "https://www.google.com/analytics/tag-manager/";
      }
    };
    googleAdSense = {
      gdprRequired: true,
      isService: function(userData) {
        if (typeof userData !== 'string') {
          return false;
        }
        return userData.indexOf('data-ad-client') > -1 && userData.indexOf('adsbygoogle') > -1;
      },
      run: function(userData) {
        var el, j, len, ref;
        if (isAdminApp) {
          return;
        }
        uicIntegrations.activeServicesData.googleAdSenseId = null;
        ref = angular.element(userData);
        for (j = 0, len = ref.length; j < len; j++) {
          el = ref[j];
          if (el.nodeName.toLowerCase() === 'script') {
            if (el.src) {
              H.loadOnce.js(el.src);
              uicIntegrations.activeServicesData.googleAdSenseId = el.getAttribute('data-ad-client');
              window.adsbygoogle = window.adsbygoogle || [];
              window.adsbygoogle.push({
                google_ad_client: uicIntegrations.activeServicesData.googleAdSenseId,
                enable_page_level_ads: true
              });
            } else {
              eval(el.innerText);
            }
          }
        }
        $rootScope.$broadcast('$uicIntegrations.ready.googleAdSense', {
          googleAdSenseId: uicIntegrations.activeServicesData.googleAdSenseId
        });
      },
      getInitLink: angular.noop,
      getExternalLink: angular.noop
    };
    jivosite = {
      gdprRequired: true,
      isService: function(userData) {
        if (typeof userData !== 'string') {
          return false;
        }
        if (userData.indexOf('code.jivosite.com') === -1 || userData.indexOf('widget_id') === -1) {
          return false;
        }
        return true;
      },
      run: function(userData) {
        var c, el, j, len, ref, text;
        if (isAdminApp) {
          return;
        }
        userData = userData.replaceAll('\r', ' ');
        userData = userData.replaceAll('\n', ' ');
        el = document.createElement('div');
        el.innerHTML = userData;
        text = "";
        ref = el.children;
        for (j = 0, len = ref.length; j < len; j++) {
          c = ref[j];
          text += c.text;
        }
        eval(text);
        $rootScope.$emit('$uicIntegrations.ready.jivosite', {});
      },
      getInitLink: angular.noop,
      getExternalLink: function(userData) {
        return "https://admin.jivosite.com/";
      }
    };
    findYandexMetricaId = function(text) {
      var index, j, k, len, len1, line, t, yaMetricaId;
      text = (text || '').split('\n');
      yaMetricaId = null;
      for (j = 0, len = text.length; j < len; j++) {
        t = text[j];
        index = t.indexOf('.yaCounter');
        if (index === -1) {
          continue;
        }
        line = t.substr(index + '.yaCounter'.length);
        line = line.split('=')[0].trim();
        line = parseInt(line);
        if (angular.isNumber(line) && !isNaN(line)) {
          yaMetricaId = line;
          break;
        }
      }
      if (!yaMetricaId) {
        for (k = 0, len1 = text.length; k < len1; k++) {
          t = text[k];
          index = t.indexOf('ym(');
          if (index === -1) {
            continue;
          }
          line = t.substr(index + 'ym('.length);
          line = line.split(',')[0].trim();
          line = parseInt(line);
          if (angular.isNumber(line) && !isNaN(line)) {
            yaMetricaId = line;
            break;
          }
        }
      }
      return yaMetricaId;
    };
    yandexMetrica = {
      gdprRequired: true,
      isService: function(userData) {
        if (typeof userData !== 'string') {
          return false;
        }
        if (userData.indexOf('https://mc.yandex.ru/metrika/tag') === -1 || userData.indexOf('</script>') === -1) {
          return false;
        }
        return userData.indexOf('new Ya.Metrika2') > -1 || userData.indexOf('ym(') > -1;
      },
      run: function(userData) {
        var el, j, len, ref, yaMetricaId;
        if (isAdminApp) {
          return;
        }
        yaMetricaId = null;
        ref = angular.element(userData);
        for (j = 0, len = ref.length; j < len; j++) {
          el = ref[j];
          if (el.nodeName.toLowerCase() === 'script') {
            if (el.src) {
              H.loadOnce.js(el.src);
            } else {
              eval(el.innerText);
              if (!yaMetricaId) {
                yaMetricaId = findYandexMetricaId(el.innerText);
              }
            }
          }
        }
        if (!yaMetricaId) {
          return;
        }
        uicIntegrations.activeServicesData.yandexMetricaId = yaMetricaId;
        $rootScope.$emit('$uicIntegrations.ready.yandexMetrica', {
          yandexMetricaId: uicIntegrations.activeServicesData.yandexMetricaId
        });
        $tracking.setYaMetricaId(yaMetricaId);
        $tracking.sendPageview();
      },
      getInitLink: function() {
        return "https://metrika.yandex.ru";
      },
      getExternalLink: function(userData) {
        var yaMetricaId;
        yaMetricaId = findYandexMetricaId(userData);
        return "https://metrika.yandex.ru/dashboard?id=" + yaMetricaId;
      }
    };
    facebookPixel = {
      gdprRequired: true,
      isService: function(userData) {
        if (typeof userData !== 'string') {
          return false;
        }
        if (userData.indexOf('facebook.') === -1 || userData.indexOf('</script>') === -1) {
          return false;
        }
        return userData.indexOf('fbq') > -1;
      },
      run: function(userData) {
        var el, j, len, ref;
        if (isAdminApp) {
          return;
        }
        userData = userData.replaceAll("fbq('track', 'PageView')", "");
        userData = userData.replaceAll("fbq(\"track\", \"PageView\")", "");
        ref = angular.element(userData);
        for (j = 0, len = ref.length; j < len; j++) {
          el = ref[j];
          if (el.nodeName.toLowerCase() === 'script') {
            if (el.src) {
              H.loadOnce.js(el.src);
            } else {
              eval(el.innerText);
            }
          }
        }
      },
      getInitLink: function() {
        return "https://www.facebook.com/events_manager/";
      },
      getExternalLink: function(userData) {
        return "https://www.facebook.com/events_manager/";
      }
    };
    chatflowIo = {
      gdprRequired: true,
      isService: function(userData) {
        if (typeof userData !== 'string') {
          return false;
        }
        if (userData.indexOf('https://chatflow.io/') === -1 || userData.indexOf('</script>') === -1) {
          return false;
        }
        return true;
      },
      run: function(userData) {
        var el, error, i, innerText, j, k, len, len1, line, ref;
        if (isAdminApp) {
          return;
        }
        ref = angular.element(userData);
        for (j = 0, len = ref.length; j < len; j++) {
          el = ref[j];
          if (el.nodeName.toLowerCase() === 'script') {
            if (el.src) {
              H.loadOnce.js(el.src);
            } else {
              innerText = el.innerText;
              if (innerText.indexOf('function r()') > -1) {
                innerText = innerText.split('\n');
                for (i = k = 0, len1 = innerText.length; k < len1; i = ++k) {
                  line = innerText[i];
                  if (!(line.indexOf('if(w.attachEvent)') > -1 || line.indexOf('if(window.attachEvent)') > -1)) {
                    continue;
                  }
                  innerText.insert(i, "try{\n    r();\n} catch (e) {}");
                  break;
                }
                innerText = innerText.join('\n');
              }
              try {
                eval(innerText);
              } catch (error1) {
                error = error1;
                console.error("Failed to run chatflow.io code", error);
                console.log(el.innerText);
              }
            }
          }
        }
      },
      getInitLink: function() {
        return "https://chatflow.io/";
      },
      getExternalLink: function(userData) {
        return "https://chatflow.io/";
      }
    };
    findDisqusId = function(text) {
      var disqusId, j, len, line;
      text = (text || '').split('\n');
      disqusId = null;
      for (j = 0, len = text.length; j < len; j++) {
        line = text[j];
        if (!(line.indexOf('.disqus.com/embed') > -1)) {
          continue;
        }
        line = line.split('.disqus.com/embed')[0];
        line = line.split('://')[1];
        if (line) {
          disqusId = line;
        }
      }
      return disqusId;
    };
    disqusCom = {
      gdprRequired: true,
      isService: function(userData) {
        if (typeof userData !== 'string') {
          return false;
        }
        return !!findDisqusId(userData);
      },
      run: function(userData) {
        var disqusId;
        if (isAdminApp) {
          return;
        }
        disqusId = findDisqusId(userData);
        if (!disqusId) {
          return;
        }
        return function(onReady) {
          H.loadOnce.js("https://" + disqusId + ".disqus.com/count.js", {
            id: "dsq-count-scr"
          });
          H.loadOnce.js("https://" + disqusId + ".disqus.com/embed.js", onReady);
        };
      },
      getInitLink: function() {
        return "https://disqus.com/";
      },
      getExternalLink: function(userData) {
        var disqusId;
        disqusId = findDisqusId(userData);
        return "https://" + disqusId + ".disqus.com/admin/";
      }
    };
    custom = {
      isGdprRequired: false,
      isService: function(userData) {
        return true;
      },
      run: function(userData) {
        if (isAdminApp || !userData || !userData.length) {
          return;
        }
        document.getElementsByTagName('head')[0].appendChild(strToNode(userData));
      },
      getInitLink: angular.noop,
      getExternalLink: angular.noop
    };
    SERVICES = {
      googleSearchConsole: googleSearchConsole,
      googleAnalytics: googleAnalytics,
      googleAdSense: googleAdSense,
      jivosite: jivosite,
      yandexWebmaster: yandexWebmaster,
      googleTagManager: googleTagManager,
      yandexMetrica: yandexMetrica,
      facebookPixel: facebookPixel,
      chatflow: chatflowIo,
      disqus: disqusCom,
      custom: custom
    };
    uicIntegrations = {
      ALLOWED_SERVICES: Object.keys(SERVICES),
      hasService: function(serviceName) {
        if (!SERVICES.hasOwnProperty(serviceName)) {
          console.warn("No such " + serviceName + " in $uicIntegrations. Allowed are " + (Object.keys(SERVICES)));
          return false;
        }
        return true;
      },
      isService: (function(_this) {
        return function(serviceName, userData) {

          /*
              тестирует поддерживается ли те данные которые передает пользователь
              этим сервисом
           */
          if (!uicIntegrations.hasService(serviceName)) {
            return;
          }
          return SERVICES[serviceName].isService(userData);
        };
      })(this),
      isGdprRequired: (function(_this) {
        return function(serviceName) {
          if (!uicIntegrations.hasService(serviceName)) {
            return;
          }
          return !!SERVICES[serviceName].gdprRequired;
        };
      })(this),
      runService: (function(_this) {
        return function(serviceName, userData) {
          if (!uicIntegrations.hasService(serviceName)) {
            return;
          }
          return SERVICES[serviceName].run(userData);
        };
      })(this),
      getExternalLink: (function(_this) {
        return function(serviceName, userData) {
          if (!uicIntegrations.hasService(serviceName)) {
            return;
          }
          return SERVICES[serviceName].getExternalLink(userData);
        };
      })(this),
      getInitLink: function(serviceName) {
        if (!uicIntegrations.hasService(serviceName)) {
          return;
        }
        return SERVICES[serviceName].getInitLink();
      },
      getServiceHelpers: function(serviceName) {
        return uicIntegrations.hasService(serviceName);
      },
      getThirdPartyLibrary: function(name) {
        if (!thirdPartyLibs[name]) {
          return null;
        }
        return {
          config: angular.copy(thirdPartyLibs[name].config) || {},
          load: function() {
            var tasks;
            tasks = (thirdPartyLibs[name].libraries || []).map(function(url) {
              if (url[0] === '/') {
                url = H.path.resolveFilePath(url);
              }
              return $q(function(resolve, reject) {
                if (url.endsWith('.css')) {
                  H.loadOnce.css(url, resolve);
                  return;
                }
                if (url.endsWith('.js')) {
                  H.loadOnce.js(url, resolve);
                  return;
                }
                H.loadOnce.data(url, resolve);
              });
            });
            return $q.all(tasks);
          },
          onReadyCallbacks: (thirdPartyLibsCbs[name] || []).filter(angular.isFunction)
        };
      },
      activeServicesData: {}
    };
    thirdPartyLibs = {};
    this.registerThirdPartyLibrary = function(name, libraries, config) {
      thirdPartyLibs[name] = thirdPartyLibs[name] || {};
      if (angular.isDefined(libraries)) {
        thirdPartyLibs[name].libraries = libraries;
      }
      if (angular.isDefined(config)) {
        thirdPartyLibs[name].config = angular.copy(config);
      }
    };
    thirdPartyLibsCbs = {};
    this.registerThirdPartyLibraryOnReadyCallback = function(name, cb) {
      thirdPartyLibsCbs[name] = thirdPartyLibsCbs[name] || [];
      thirdPartyLibsCbs[name].push(cb);
    };
    this.$get = [
      '$injector', function($injector) {
        $langPicker = $langPicker || $injector.get('$langPicker');
        $window = $window || $injector.get('$window');
        $timeout = $timeout || $injector.get('$timeout');
        $rootScope = $rootScope || $injector.get('$rootScope');
        $tracking = $tracking || $injector.get('$tracking');
        $q = $q || $injector.get('$q');
        H = H || $injector.get('H');
        return uicIntegrations;
      }
    ];
  });

}).call(this);
;
(function() {
  angular.module('ui.cms').controller('UicDatepickerCtrl', ["$scope", "$element", "$attrs", "$filter", "H", function($scope, $element, $attrs, $filter, H) {
    var ALLOW_JSDATE, createRangeValueWatch, dateDisabled, filterDate, fixProxy, ngModelCtrl, normalizeDate, parseDateAttr, parseShortcut;
    filterDate = null;
    ngModelCtrl = $element.controller('ngModel');
    $scope.proxy = null;
    $scope.proxyOptions = {};
    normalizeDate = function(d, allow_jsdate) {
      if (typeof d === 'string') {
        d = new Date(d);
      }
      if (!d) {
        return d;
      }
      if (!allow_jsdate) {
        d.setUTCHours(12);
        d.setUTCMinutes(0);
        d.setUTCSeconds(0);
        d.setUTCMilliseconds(0);
      }
      return d;
    };
    parseShortcut = function(value) {
      var d, o, r;
      if (typeof value !== 'string') {
        console.error("Expect '" + value + "' to be string, got '" + (typeof value) + "'");
      }
      if (!value || value.length < 2) {
        console.error("Got empty date shortcut for UicDatepickerHelper");
      }
      d = new Date();
      if (value === 'today-utc') {
        return normalizeDate(d);
      }
      if (value === 'today') {
        filterDate = filterDate || $filter('date');
        d = filterDate(d, "yyyy-MM-dd");
        return normalizeDate(d);
      }
      r = value[value.length - 1];
      if (r !== 'y' && r !== 'm' && r !== 'd') {
        console.error("Got invalid offset '" + r + "' for '" + value + "' date shortcut");
      }
      o = value.slice(0, value.length - 1);
      o = parseInt(o);
      if (isNaN(o)) {
        console.error("Got invalid date shortcut '" + value + "'. Cant parse '" + (value.slice(0, value.length - 1)) + "' to integer");
      }
      d = new Date();
      d = normalizeDate(d);
      switch (r) {
        case 'y':
          d.setUTCFullYear(d.getUTCFullYear() + o);
          break;
        case 'm':
          d.setUTCMonth(d.getUTCMonth() + o);
          break;
        case 'd':
          d.setUTCDate(d.getUTCDate() + o);
      }
      return normalizeDate(d);
    };
    fixProxy = function() {
      var maxDate, minDate;
      if ($attrs.allowInvalid === 'false') {
        minDate = $scope.datepickerOptions.minDate;
        maxDate = $scope.datepickerOptions.maxDate;
        if (minDate) {
          minDate = normalizeDate(minDate, ALLOW_JSDATE);
          if (!$scope.proxy || $scope.proxy < minDate) {
            $scope.proxy = angular.copy(minDate);
          }
        }
        if (maxDate) {
          maxDate = normalizeDate(maxDate, ALLOW_JSDATE);
          if (!$scope.proxy || $scope.proxy > maxDate) {
            $scope.proxy = angular.copy(maxDate);
          }
        }
      }
      if (angular.isDate($scope.proxy) && isNaN($scope.proxy)) {
        $scope.proxy === null;
      }
    };
    createRangeValueWatch = function(varName, parentVarName) {
      $scope.$watch('$parent.' + parentVarName, function(date, oldValue) {
        if (date !== oldValue) {
          parseDateAttr('minDate');
          parseDateAttr('maxDate');
          fixProxy();
        }
      });
    };
    parseDateAttr = function(varName) {
      var data, is_var, v;
      v = $attrs[varName];
      is_var = false;
      if (v) {
        try {
          data = $scope.$parent.$eval(v);
          if (!data && (v === 'today' || v === 'today-utc')) {
            $scope.datepickerOptions[varName] = parseShortcut(v);
          } else {
            $scope.datepickerOptions[varName] = data;
            if (typeof data === 'string') {
              $scope.datepickerOptions[varName] = normalizeDate(data, ALLOW_JSDATE);
            }
            is_var = true;
          }
        } catch (error) {
          $scope.datepickerOptions[varName] = parseShortcut(v);
        }
      }
      return is_var;
    };
    ALLOW_JSDATE = $attrs.allowJsdate === 'true' || $attrs.ngModelType === 'date';
    if (!ALLOW_JSDATE) {
      $scope.proxyOptions.timezone = '+0000';
    }
    if (typeof $scope.datepickerOptions !== 'object') {
      $scope.datepickerOptions = {
        formatYear: 'yyyy',
        startingDay: 1,
        showWeeks: false
      };
    }
    if ($attrs.dateDisabled) {
      dateDisabled = $scope.$parent.$eval($attrs.dateDisabled.split('(')[0]);
    }
    if (angular.isFunction(dateDisabled) && !$scope.datepickerOptions.dateDisabled) {
      $scope.datepickerOptions.dateDisabled = dateDisabled;
    }
    ['minDate', 'maxDate'].forEach(function(k) {
      if (parseDateAttr(k)) {
        createRangeValueWatch(k, $attrs[k]);
      }
    });
    ngModelCtrl.$render = function() {
      var ngModel;
      ngModel = ngModelCtrl.$modelValue;
      if ((ngModel === null || ngModel === (void 0)) || (angular.isString(ngModel) && !ngModel)) {
        $scope.proxy = null;
        return;
      }
      $scope.proxy = normalizeDate(ngModel, ALLOW_JSDATE);
    };
    return $scope.$watch('proxy', function(proxy, oldValue) {
      var value;
      if (proxy === null || proxy === (void 0)) {
        ngModelCtrl.$setViewValue(null);
        return;
      }
      if (H.date.isSameDate(proxy, oldValue)) {
        return;
      }
      if (isNaN(proxy)) {
        value = null;
      } else if (ALLOW_JSDATE) {
        value = new Date(proxy);
      } else {
        value = proxy.toDateISOString();
      }
      ngModelCtrl.$setViewValue(value);
    });
  }]);

}).call(this);
;
(function() {
  angular.module('ui.cms').directive('uicCheckbox', function() {
    return {

      /**
      *   @ngdoc directive
      *   @name ui.cms.directive:uicCheckbox
      *   @description директива-обертка для легкого отображения input(type="checkbox")
      *       в формах. По умолчанию в bootstrap-e нужно input оборачивать в label, а label
      *       оборачивать в div c классом checkbox. <b> Директива не поддерживает ngChange! </b>
      *   @restrict E
      *   @param {boolean} ngModel (ссылка) модель
      *   @param {string=} name (значение) название для тега input
      *   @param {string=} [type=''] (значение) тип отображения виджета: 'checkbox', 'switch', 'button'
      *   @param {boolean=} ngDisabled (ссылка) флаг доступности тега input
      *   @example
      *       <pre>
      *           $scope.myCheckboxVal = true
      *       </pre>
      *       <pre>
      *           //- pug
      *           uic-checkbox(ng-model="myCheckboxVal") My Super Checkbox
      *       </pre>
      *       Если нужна поддержка перевода, то лучше делать так:
      *       <pre>
      *           uic-checkbox(ng-model="myCheckboxVal")
      *               span(translate) My Super Checkbox
      *       </pre>
       */
      restrict: 'E',
      transclude: true,
      scope: {
        ngModel: '=',
        name: '@?',
        inputClass: '@?',
        ngDisabled: '=?'
      },
      controller: function() {},
      template: function($element, $attrs) {
        if ($attrs.type === 'button') {
          return "<button class=\"btn {{inputClass || 'btn-default'}}\" ng-class=\"{active: ngModel}\" ng-click=\"ngModel = !ngModel\" ng-disabled=\"ngDisabled\">\n    <span class='far' ng-class=\"{'fa-check-square': ngModel, 'fa-square': !ngModel}\"></span>\n    <span ng-transclude=''></span>\n</button>";
        }
        if ($attrs.type === 'switch') {
          return "<div class='checkbox switch {{inputClass}}'>\n    <label>\n        <input type='checkbox' role='switch' ng-model='ngModel' ng-disabled='ngDisabled' name='{{name}}'/>\n        <span ng-transclude=''></span>\n    </label>\n</div>";
        }
        return "<div class='checkbox {{inputClass}}'>\n    <label>\n        <input type='checkbox' ng-model='ngModel' ng-disabled='ngDisabled' name='{{name}}'/>\n        <span ng-transclude=''></span>\n    </label>\n</div>";
      }
    };
  });

}).call(this);
;
(function() {
  angular.module('ui.cms').directive('uicConstant', function() {
    return {

      /**
      *   @ngdoc directive
      *   @name ui.cms.directive:uicConstant
      *   @description директива, которая работает в паре с {@link ui.cms.$constantsProvider $constantsProvider}. Объявите в блоке config с помощью $constantsProvider
      *       переменные и используйте их для сложных вариантов выбора значений.
      *      Для директивы можно указывать <b>только один из параметров - <i>select</i> или <i>radio</i> или <i>checkbox</i></b>
      *
      *   @restrict E
      *   @param {string|number|boolean|object|Array} ngModel (ссылка) модель
      *   @param {string=} select при указании этого параметра с путем к константе в $constants мы получим стандартный select тег (см примеры)
      *   @param {string=} radio при указании этого параметра с путем к константе в $constants мы получим массив из бутстраповских input(type='radio')
      *   @param {string=} checkbox при указании этого параметра с путем к константе в $constants мы получим массив из бутстраповских input(type='checkbox').
      *       <b>При этом значение ngModel будет объектом, ключами в котором будут значения вариантов констант.
      *       Если нужно чтоб ngModel был массивом со значениями, то добавьте атрибут ng-model-type='array' к директиве. </b>
      *   @param {string=} inputClass css-класс для внутренней верстки. Можно, например передать класс 'input-sm' для select
      *       типа виджета, чтоб можно было уменьшить размер тега, или 'radio-inline'('inline') для radio типа, или 'checkbox-inline'('inline') для checkbox типа.
      *   @param {string=} [ngModelType='object'] тип значения ngModel - 'object' или 'array'. Этот атрибут влияет только при указании атрибута <b>checkbox</b>
      *   @param {string=} placeholder текст, который отображается на месте пустого ngModel (только при использовании select)
      *   @param {string=} skipValues (значение) значения, которые можно пропустить (перечисленные через запятую)
      *   @param {boolean=} ngDisabled (ссылка) флаг доступности тега
      *   @example
      *       <pre>
      *           angular.module('myModule')
      *
      *           .config ($constantsProvider)->
      *               $constantsProvider.set('TEST_MODEL', {
      *                   myProperty:[
      *                       {
      *                           value: 'hello',
      *                           description: 'Hello!'
      *                       },{
      *                           value: 11,
      *                           description: 'Integer: 11'
      *                       },{
      *                           value: false,
      *                           description: 'My Boolean: false'
      *                       },{
      *                           value: 'world!!',
      *                           description: 'world'
      *                       }
      *                   ]
      *               })
      *
      *           .controller 'MyCtrl', ($scope)->
      *               $scope.mySimpleValue = 'hello'
      *               $scope.myComplexValue = {11: true}
      *               $scope.myComplexArrayValue = ['11', 'false']
      *       </pre>
      *       <pre>
      *           //- pug
      *           p Select. Ng-Model == {{mySimpleValue}}
      *           uic-constant(ng-model="mySimpleValue", select='TEST_MODEL.myProperty')
      *
      *           p Radio. Ng-Model == {{mySimpleValue}}
      *           uic-constant(ng-model="mySimpleValue", radio='TEST_MODEL.myProperty', input-class='inline')
      *
      *           p Checkbox. Ng-Model == {{myComplexValue}}
      *           uic-constant(ng-model="myComplexValue", checkbox='TEST_MODEL.myProperty')
      *
      *           p Checkbox. Ng-Model (as array) == {{myComplexArrayValue}}
      *           uic-constant(ng-model="myComplexArrayValue", ng-model-type='array', checkbox='TEST_MODEL.myProperty')
      *       </pre>
      *       <example module="DocExampleApp">
      *           <file name="script.js">
      *               mkDocCtrl('uicConstantCtrl', {
      *                   mySimpleValue: 'hello',
      *                   myComplexValue: {11: true},
      *                   myComplexArrayValue: ['11', 'false']
      *               });
      *           </file>
      *           <file name="index.html">
      *               <div ng-controller="uicConstantCtrl">
      *                   <div class='row'>
      *                       <div class='col-xs-100 well well-sm'>
      *                           <p><b>Select. Ng-Model == <span class='text-success'>{{mySimpleValue}}</span></b></p>
      *                           <uic-constant ng-model="mySimpleValue" select='TEST_MODEL.myProperty'></uic-constant>
      *                       </div>
      *                       <div class='col-xs-100 well well-sm'>
      *                           <p><b>Radio. Ng-Model == <span class='text-success'>{{mySimpleValue}}</span></b></p>
      *                           <uic-constant ng-model="mySimpleValue" radio='TEST_MODEL.myProperty' input-class='inline'></uic-constant>
      *                       </div>
      *                       <div class='col-xs-100 well well-sm'>
      *                           <p><b>Checkbox. Ng-Model == <span class='text-success'>{{myComplexValue}}</span></b></p>
      *                           <uic-constant ng-model="myComplexValue" checkbox='TEST_MODEL.myProperty'></uic-constant>
      *                       </div>
      *                       <div class='col-xs-100 well well-sm'>
      *                           <p><b>Checkbox. Ng-Model (as array) == <span class='text-success'>{{myComplexArrayValue}}</span></b></p>
      *                           <uic-constant ng-model="myComplexArrayValue", ng-model-type='array', checkbox='TEST_MODEL.myProperty'></uic-constant>
      *                       </div>
      *                   </div>
      *               </div>
      *           </file>
      *       </example>
       */
      restrict: 'E',
      require: 'ngModel',
      scope: {
        ngDisabled: '=?'
      },
      controller: ["$scope", "$element", "$attrs", "$constants", "$filter", function($scope, $element, $attrs, $constants, $filter) {
        var allowed_values, constantVariantsToValuesList, l10n, ngModelCtrl, setConstantVariants, translate;
        ngModelCtrl = $element.controller('ngModel');
        l10n = $filter('l10n');
        translate = $filter('translate');
        $scope.proxy = null;
        setConstantVariants = function(variants) {
          var skip;
          if (!$attrs.skipValues) {
            $scope.constantVariants = variants;
            return;
          }
          skip = $attrs.skipValues.split(',');
          $scope.constantVariants = variants.filter(function(o) {
            return !skip.includes(o.value + '');
          });
        };
        setConstantVariants($constants.resolve($attrs.select || $attrs.radio || $attrs.checkbox) || $scope.$parent.$eval($attrs.select || $attrs.radio || $attrs.checkbox) || []);
        constantVariantsToValuesList = function(variants) {
          return variants.map(function(o) {
            if (angular.isString(o.value)) {
              return o.value;
            }
            return o.value + '';
          });
        };
        $scope.getText = function(o) {
          if (o.description) {
            if (angular.isString(o.description)) {
              return translate(o.description);
            }
            return l10n(o.description);
          }
          if (angular.isDefined(o.value)) {
            return o.value;
          }
          return '';
        };
        $scope.$on('$constants.update', function(ev, data) {
          var allowed_values, ref;
          if ((ref = data.path) === $attrs.select || ref === $attrs.radio || ref === $attrs.checkbox) {
            setConstantVariants(data.value);
            if ($attrs.checkbox && $attrs.ngModelType === 'array') {
              allowed_values = constantVariantsToValuesList($scope.constantVariants);
            }
          }
        });
        if ($attrs.checkbox && $attrs.ngModelType === 'array') {
          allowed_values = constantVariantsToValuesList($scope.constantVariants);
          ngModelCtrl.$render = function() {
            var i, k, len, ref;
            $scope.proxy = {};
            ref = ngModelCtrl.$modelValue || [];
            for (i = 0, len = ref.length; i < len; i++) {
              k = ref[i];
              if (k !== void 0) {
                $scope.proxy[k] = true;
              }
            }
          };
          return $scope.$watch('proxy', function(proxy, oldValue) {
            var i, k, len, o, ref, ref1, v, value;
            value = [];
            ref = proxy || {};
            for (k in ref) {
              v = ref[k];
              if (v && allowed_values.includes(k)) {
                ref1 = $scope.constantVariants;
                for (i = 0, len = ref1.length; i < len; i++) {
                  o = ref1[i];
                  if (!(o.value + '' === k)) {
                    continue;
                  }
                  value.push(o.value);
                  break;
                }
              }
            }
            value.sort();
            ngModelCtrl.$setViewValue(value);
          }, true);
        } else {
          ngModelCtrl.$render = function() {
            $scope.proxy = ngModelCtrl.$modelValue;
          };
          return $scope.$watch('proxy', function(proxy, oldValue) {
            ngModelCtrl.$setViewValue(proxy);
          });
        }
      }],
      template: function($element, $attrs) {
        var inputClass;
        inputClass = $attrs.inputClass || '';
        if (($attrs.select && $attrs.radio) || ($attrs.radio && $attrs.checkbox) || ($attrs.select && $attrs.checkbox)) {
          throw "You should specify only 1 attribute for uic-constant (checkbox/radio/select)";
        }
        if ($attrs.select) {
          return "<select class='form-control " + inputClass + "'\n    ng-model=\"proxy\"\n    ng-disabled=\"ngDisabled\"\n    ng-options=\"o.value as getText(o)  disable when o.disabled  for o in constantVariants\">\n</select>";
        }
        if ($attrs.radio) {
          if (inputClass === 'radio-inline' || inputClass === 'inline') {
            return "<label class='radio-inline' ng-repeat=\"o in constantVariants\">\n    <input type='radio' ng-model=\"$parent.proxy\"\n           ng-disabled=\"$parent.ngDisabled || o.disabled\"\n           ng-value=\"o.value\"/>\n    {{getText(o)}}\n</label>";
          } else {
            return "<div class='radio' ng-repeat=\"o in constantVariants\">\n    <label class='radio " + inputClass + "'>\n        <input type='radio' ng-model=\"$parent.proxy\"\n               ng-disabled=\"$parent.ngDisabled || o.disabled\"\n               ng-value=\"o.value\"/>\n        {{getText(o)}}\n    </label>\n</div>";
          }
        }
        if ($attrs.checkbox) {
          if (inputClass === 'checkbox-inline' || inputClass === 'inline') {
            return "<label class='checkbox checkbox-inline' ng-repeat=\"o in constantVariants\" ng-class=\"{'nomargin-top': $first}\">\n    <input type='checkbox'\n           ng-model=\"$parent.proxy[o.value]\"\n           ng-disabled='$parent.ngDisabled || o.disabled'/>\n    {{getText(o)}}\n</label>";
          } else {
            return "<div class='checkbox " + inputClass + "' ng-repeat=\"o in constantVariants\" ng-class=\"{'nomargin-top': $first}\">\n    <label>\n        <input type='checkbox'\n               ng-model=\"$parent.proxy[o.value]\"\n               ng-disabled='$parent.ngDisabled || o.disabled'/>\n        {{getText(o)}}\n    </label>\n</div>";
          }
        }
        return "";
      }
    };
  });

}).call(this);
;
(function() {
  angular.module('ui.cms').directive('uicCountryListPicker', function() {
    var parseColumnSize;
    parseColumnSize = function(columns) {
      var c, error, i, k, len, screenSizes, v;
      try {
        columns = JSON.parse(columns);
      } catch (error1) {
        error = error1;
      }
      if (!columns || typeof columns !== 'object') {
        columns = {
          lg: 3,
          md: 3,
          sm: 2,
          xs: 2
        };
      }
      screenSizes = ['xs', 'sm', 'md', 'lg'];
      for (k in columns) {
        v = columns[k];
        screenSizes.splice(screenSizes.indexOf(k), 1);
      }
      for (i = 0, len = screenSizes.length; i < len; i++) {
        c = screenSizes[i];
        columns[c] = 2;
      }
      return columns;
    };
    return {

      /**
      *   @ngdoc directive
      *   @name ui.cms.directive:uicCountryListPicker
      *   @description
      *       пикер массива стран (сохраняются двухзначные iso-коды)
      *
      *   @restrict E
      *   @param {array} ngModel (ссылка) массив кодов стран
      *   @param {string=} [allowed='world'] allowed (значение) страны, которые будут доступны для выбора, перечисленные через запятую.
      *       Варианты: 'europe', 'asia', 'africa', 'northAmerica', 'southAmerica', 'australiaAndOceania', 'world'.
      *   @param {string=} [columns={lg:3, md:3, sm:2, xs:2}] columns (значение) json строка с описанием сколько колонок отображать на экране для разных размеров экранов
      *   @param {boolean=} ngDisabled (ссылка) флаг доступности тега
      *   @example
      *       <pre>
      *           $scope.myCountries = ['ua']
      *       </pre>
      *       <pre>
      *           //- pug
      *           p Выбранные страны - {{myCountries}}
      *           uic-country-list-picker(ng-model="myCountries", allowed='europe,asia', columns='{lg:3, sm:1, xs:1}')
      *       </pre>
      *       <example module="DocExampleApp">
      *           <file name="script.js">
      *               mkDocCtrl('uicCountryListPickerCtrl', {
      *                   myCountries: ['ua']
      *               });
      *           </file>
      *           <file name="index.html">
      *               <div ng-controller="uicCountryListPickerCtrl">
      *                   <p>Выбранные страны - {{myCountries}}</p>
      *                   <uic-country-list-picker ng-model="myCountries" allowed='europe,asia', columns='{lg:3, sm:1, xs:1}'>
      *                   </uic-country-list-picker>
      *               </div>
      *           </file>
      *       </example>
       */
      restrict: 'E',
      require: 'ngModel',
      scope: {
        allowed: '@?',
        ngDisabled: '=?'
      },
      controller: ["$scope", "$attrs", "$countries", "$langPicker", "$cms", function($scope, $attrs, $countries, $langPicker, $cms) {
        var allowedCountries;
        $scope.$cms = $cms;
        $scope.$langPicker = $langPicker;
        $scope.columnsSize = parseColumnSize($attrs.columns);
        $scope.allowedCountries = [];
        allowedCountries = $countries.getList.apply(null, ($scope.allowed || '').split(','));
        $scope.$watch('$langPicker.currentLang', function(currentLang, oldValue) {
          if (currentLang === 'en') {
            $scope.allowedCountries = allowedCountries;
            return;
          }
          $scope.allowedCountries = allowedCountries.map(function(c) {
            return [c[0], $countries.codeToText(c[0], currentLang)];
          });
          $scope.allowedCountries.sort(function(a, b) {
            if (a[1] < b[1]) {
              return -1;
            }
            if (a[1] > b[1]) {
              return 1;
            }
            return 0;
          });
        });
      }],
      link: function($scope, $element, $attrs, ngModelCtrl) {
        $scope.proxy = {};
        ngModelCtrl.$render = function() {
          $scope.proxy = {};
          (ngModelCtrl.$modelValue || []).forEach(function(code) {
            $scope.proxy[code] = true;
          });
        };
        return $scope.$watchCollection('proxy', function(proxy, oldValue) {
          var code, list, v;
          list = (function() {
            var ref, results;
            ref = proxy || {};
            results = [];
            for (code in ref) {
              v = ref[code];
              if (v) {
                results.push(code);
              }
            }
            return results;
          })();
          ngModelCtrl.$setViewValue(list);
        });
      },
      template: function($element, $attrs) {
        var BT_SIZES, columns;
        BT_SIZES = {
          1: '100',
          2: '50',
          3: '33-3',
          4: '25',
          5: '20'
        };
        columns = parseColumnSize($attrs.columns);
        return "<div class=\"row\">\n    <div ng-repeat=\"items in allowedCountries | chunk: columnsSize[$cms.screenSize]: 'vert'\">\n        <div class=\"nopadding-right col-lg-" + BT_SIZES[columns.lg] + " col-md-" + BT_SIZES[columns.md] + " col-sm-" + BT_SIZES[columns.sm] + " col-xs-" + BT_SIZES[columns.xs] + "\">\n            <div class=\"checkbox\" ng-repeat=\"country in items\">\n              <label>\n                <input ng-model=\"$parent.$parent.proxy[country[0]]\" type=\"checkbox\" ng-disabled=\"ngDisabled\"/>\n                <span class='f16'>\n                    <span class=\"flag {{country[0]}}\"></span>\n                </span>\n                <span>{{country[1]}}</span>\n              </label>\n            </div>\n        </div>\n    </div>\n</div>";
      }
    };
  });

}).call(this);
;
(function() {
  angular.module('ui.cms').directive('uicCountryPicker', function() {
    return {

      /**
      *   @ngdoc directive
      *   @name ui.cms.directive:uicCountryPicker
      *   @description  пикер iso-кода страны
      *
      *   @restrict E
      *   @param {string} ngModel (ссылка) iso-код страны
      *   @param {string=} placeholder текст, который отображается на месте пустого ngModel
      *   @param {string=} [allowed='world'] allowed (значение) страны, которые будут доступны для выбора, перечисленные через запятую.
      *       Варианты: 'europe', 'asia', 'africa', 'northAmerica', 'southAmerica', 'australiaAndOceania', 'world'.
      *   @param {string=} inputClass css-класс для внутренней верстки. Можно, например передать класс 'input-sm'.
      *   @param {string=} ngRequired (ссылка/значение) установите этот атрибут в директиве для того, чтоб значение ng-model==null приводило к ошибкам валидации формы
      *   @param {boolean=} ngDisabled (ссылка) флаг доступности тега
      *   @example
      *       <pre>
      *           $scope.myCountry = 'ua'
      *       </pre>
      *       <pre>
      *           //- pug
      *           p Выбранная страна - {{myCountry}}
      *           uic-country-picker(ng-model="myCountry", allowed='europe,asia', input-class='input-sm')
      *       </pre>
      *       <example module="DocExampleApp">
      *           <file name="script.js">
      *               mkDocCtrl('uicCountryPickerCtrl', {
      *                   myCountry: 'ua'
      *               });
      *           </file>
      *           <file name="index.html">
      *               <div ng-controller="uicCountryPickerCtrl">
      *                   <p>Выбранная страна - {{myCountry}}</p>
      *                   <uic-country-picker ng-model="myCountry" allowed='europe,asia' input-class='input-sm'>
      *                   </uic-country-picker>
      *               </div>
      *           </file>
      *       </example>
       */
      restrict: 'E',
      require: 'ngModel',
      scope: {
        allowed: '@?',
        placeholder: '@?',
        inputClass: '@?',
        ngDisabled: '=?',
        ngRequired: '=?'
      },
      controller: ["$scope", "$countries", "$langPicker", function($scope, $countries, $langPicker) {
        var allowedCountries;
        $scope.$langPicker = $langPicker;
        allowedCountries = $countries.getList.apply(null, ($scope.allowed || '').split(','));
        $scope.allowedCountries = allowedCountries.map(function(c) {
          return {
            id: c[0],
            name: c[1]
          };
        });
        $scope.$watch('$langPicker.currentLang', function(currentLang, oldValue) {
          var c, i, j, len, ref;
          if (currentLang === 'en') {
            $scope.allowedCountries = allowedCountries.map(function(c) {
              return {
                id: c[0],
                name: c[1]
              };
            });
            return;
          }
          ref = $scope.allowedCountries;
          for (i = j = 0, len = ref.length; j < len; i = ++j) {
            c = ref[i];
            $scope.allowedCountries[i].name = $countries.codeToText(c.id);
          }
          $scope.allowedCountries.sort(function(a, b) {
            if (a.name < b.name) {
              return -1;
            }
            if (a.name > b.name) {
              return 1;
            }
            return 0;
          });
        });
      }],
      link: function($scope, $element, $attrs, ngModelCtrl) {
        $scope.proxy = null;
        if ($attrs.hasOwnProperty('required')) {
          throw "uicCountryPicker: Dont use 'required' attribute! Use ngRequired instead";
          return;
        }
        ngModelCtrl.$render = function() {
          var modelValue;
          modelValue = ngModelCtrl.$modelValue;
          $scope.proxy = modelValue;
        };
        return $scope.$watchCollection('proxy', function(proxy, oldValue) {
          var viewValue;
          viewValue = proxy;
          ngModelCtrl.$setViewValue(viewValue);
        });
      },
      template: ('/client/cms_app/_inputDirectives/uicCountryPicker/uicCountryPicker.html', '<uic-select ng-model="proxy" items="allowedCountries" placeholder="{{placeholder || (\'Select country\' | translate)}}" input-class="{{:: inputClass}}" ng-required="ngRequired"><item><span class="f16"><span class="flag {{$item.id}}"></span></span> {{$item.name}}</item><selected><span class="f16"><span class="flag {{$selected.id}}"></span></span> {{$selected.name}}</selected></uic-select>' + '')
    };
  });

}).call(this);
;
(function() {
  angular.module('ui.cms').directive('uicDatepicker', function() {
    return {

      /**
      *   @ngdoc directive
      *   @name ui.cms.directive:uicDatepicker
      *   @description
      *       обертка над {@link http://angular-ui.github.io/bootstrap/versioned-docs/1.3.3/#/datepicker}
      *       Предназначен только для работы с датами(без часовых поясов, и часов+минут).
      *       <div class="bg-danger well well-sm">
      *           <p class="text-danger">Почему была создана эта директива:</p>
      *           <p><small>
      *               практически любой datepicker ломал сохранение даты из-за особенностей Date
      *               и невозможности в js иметь стуктуру только для дат.
      *           </small></p>
      *           <p><small>
      *                <b>Пример:</b>
      *                человек в 20 часов вечера на западном побережье сша указывает
      *                дату 17 января. т.к. дата создается через Date, а часовой пояс пользователя
      *                -8h, то эта дата будет в utc date как 18 января 4 часа утра.
      *                когда потребуется эту дату отрендерить к примеру в украинском часовом поясе(+2h),
      *                то эта дата превратится в 18 января 2 часа ночи.
      *           </small></p>
      *           <p><small>
      *                поскольку использование часовых поясов перманентно сломано в ui.bootstrap
      *                и поскольку нет ни одного виджета для ангулара для который выбирал только даты
      *                и не полагался на реализацию стандартного Date так сильно, я сделал
      *                обходное решение.
      *           </small></p>
      *           <p><small>
      *                подробнее как хотели решать эту проблему https://github.com/angular-ui/bootstrap/issues/2072
      *           </small></p>
      *           <p><small>
      *                я взял за основу решение, что в 12 часов дня по UTC во всех часовых
      *                поясах один день(17 января).
      *                также я решил не сохранять время в строке и не держать основную
      *                модель данных в Date. для сравнения дат и для uib-datepicker и
      *                uib-datepicker-popup из состава ui.bootstrap я воспользовался
      *                proxyModel, которая и приводится из строки в Date для этих директив.
      *          </small></p>
      *      </div>
      *   @restrict E
      *   @param {string | Date} ngModel (ссылка) дата. По умолчанию ngModel всегда будет конвертироваться в
      *           string, без времени и часового пояса. Если нужно отключить такое поведение, то используйте параметр ngModelType='date'
      *   @param {object=} datepickerOptions (ссылка) параметры для uib-datepicker.
      *           Подробнее тут {@link http://angular-ui.github.io/bootstrap/versioned-docs/1.3.3/#/datepicker}
      *           <p>По умолчанию datepickerOptions = { formatYear: 'yyyy', startingDay: 1, showWeeks: false }</p>
      *   @param {string | Date =} minDate (ссылка или значение) минимальная дата, можно указать через datepickerOptions.minDate, можно на прямую,
      *           а можно использовать строковые сокращения:
      *           <ul>
      *               <li>
      *                   <b>today-utc</b> - сегодняшняя дата по UTC. Задавая это сокращение для
      *                       minDate:
      *                       житель западного побережья сша увидит, к примеру 17 января, но
      *                       вот житель японии увидит 18 января своим "текущим днем".
      *                       Если нужна начальная дата с оглядкой на часовой пояс, то
      *                       лучше использовать сокращение 'today'.
      *               </li>
      *               <li>
      *                   <b>today</b> - сегодняшняя дата, по часовому поясу текущего пользователя.
      *               </li>
      *               <li>
      *                   <b>+1d</b> или <b>-1d</b> +/- несколько дней(обязательно указывать знаки + или -). Сдвиг идет от "текущего дня" пользователя.
      *               </li>
      *               <li>
      *                   <b>+2m</b> или <b>-2m</b> +/- несколько месяцев(обязательно указывать знаки + или -). Сдвиг идет от "текущего дня" пользователя.
      *               </li>
      *               <li>
      *                   <b>+5y</b> или <b>-5y</b> +/- несколько лет(обязательно указывать знаки + или -). Сдвиг идет от "текущего дня" пользователя.
      *               </li>
      *           </ul>
      *   @param {function=} dateDisabled функция для отключения определнных дат в пикере. как параметры передаются {date: date, mode: mode}
      *   @param {boolean=} [allowInvalid=true] (значение) по умолчанию datepicker не контролирует находится ли
      *           ngModel в диапазоне [minDate...maxDate]. Если передать allow-invalid='false',
      *           то при указании любого из minDate/maxDate(или обоих сразу) директива начнет
      *           контролировать значение ngModel
      *   @param {string=} [ngModelType='string'] (значение) тип значения ngModel. Eсли нужно отключить режим "только дат" и перестать принудительно
      *           конвертировать объект Date в string для ngModel, то передайте ng-model-type='date' <b>Внимание: тогда виджет
      *           не будет стараться исправить время для решения проблем с часовыми поясами!</b>
      *   @param {boolean=} ngDisabled (ссылка) флаг доступности тега
      *   @example
      *       <pre>
      *           $scope.date = new Date()
      *           $scope.date2 = new Date()
      *           $scope.date3 = new Date()
      *           $scope.date4 = new Date()
      *       </pre>
      *       <pre>
      *           //- pug
      *           //- С minDate == 'today'
      *           uic-datepicker(ng-model="date", min-date='today')
      *
      *           //- С minDate от 1 datepicker-a и с offset для maxDate
      *           uic-datepicker(ng-model="date2", min-date="date", max-date='+3m')
      *
      *           //- Тоже что и вариант 2, но еще и с allow-invalid='false'
      *           uic-datepicker(ng-model="date3", min-date="date", max-date='+3m', allow-invalid='false')
      *
      *           //- Вариант без конвертирования Date -> string
      *           uic-datepicker(ng-model="date4", ng-model-type='date')
      *       </pre>
      *       <example module="DocExampleApp">
      *           <file name="script.js">
      *               mkDocCtrl('uicDatepickerCtrl', {
      *                   date: new Date(),
      *                   date2: new Date(),
      *                   date3: new Date(),
      *                   date4: new Date()
      *               });
      *           </file>
      *           <file name="index.html">
      *               <div ng-controller="uicDatepickerCtrl">
      *                   <div class='row'>
      *                       <div class='col-md-33'>
      *                           <p style='height:3em'><b>1. </b>С minDate == 'today'</p>
      *                           <uic-datepicker ng-model="date" min-date='today'></uic-datepicker>
      *                           <hr>
      *                           <p>$scope.date = <b>{{date}}</b></p>
      *                       </div>
      *                       <div class='col-md-33'>
      *                           <p style='height:3em'><b>2. </b>С minDate от 1 datepicker-a и с offset для maxDate</p>
      *                           <uic-datepicker ng-model="date2" min-date="date" max-date='+3m'></uic-datepicker>
      *                           <hr>
      *                           <p>$scope.date2 = <b>{{date2}}</b></p>
      *                       </div>
      *                       <div class='col-md-33'>
      *                           <p style='height:3em'><b>3. </b>Тоже что и вариант 2, но еще и с allow-invalid='false'</p>
      *                           <uic-datepicker ng-model="date3" min-date="date" max-date='+3m' allow-invalid='false'></uic-datepicker>
      *                           <hr>
      *                           <p>$scope.date3 = <b>{{date3}}</b></p>
      *                       </div>
      *                   </div>
      *                   <hr>
      *                   <div class='row'>
      *                       <div class='col-md-33'>
      *                           <p style='height:3em'><b>4. </b>Вариант без конвертирования Date -> string</p>
      *                           <uic-datepicker ng-model="date4" ng-model-type='date'></uic-datepicker>
      *                           <hr>
      *                           <p>$scope.date4 = <b>{{date4}}</b></p>
      *                       </div>
      *                   </div>
      *               </div>
      *           </file>
      *       </example>
       */
      restrict: 'E',
      require: 'ngModel',
      scope: {
        ngRequired: '=?required',
        datepickerOptions: '=?',
        name: '@?',
        ngDisabled: '=?'
      },
      controller: 'UicDatepickerCtrl',
      template: "<div uib-datepicker=''\n    ng-model=\"proxy\"\n    ng-model-options=\"proxyOptions\"\n    datepicker-options=\"datepickerOptions\"\n    ng-disabled=\"ngDisabled\"\n    ng-required=\"ngRequired\"\n    name=\"{{name || 'noname'}}\"\n></div>"
    };
  });

}).call(this);
;
(function() {
  angular.module('ui.cms').directive('uicDatepickerInput', function() {
    return {

      /**
      *   @ngdoc directive
      *   @name ui.cms.directive:uicDatepickerInput
      *   @description
      *       обертка над {@link http://angular-ui.github.io/bootstrap/versioned-docs/1.3.3/#/datepicker}
      *       Предназначен только для работы с датами(без часовых поясов, и часов+минут).
      *       Подробнее можно почитать {@link ui.cms.directive:uicDatepicker тут}
      *
      *   @restrict E
      *   @param {string | Date} ngModel (ссылка) дата. По умолчанию ngModel всегда будет конвертироваться в
      *           string, без времени и часового пояса. Если нужно отключить такое поведение, то используйте параметр ngModelType='date'
      *   @param {object=} datepickerOptions (ссылка) параметры для uib-datepicker.
      *           Подробнее тут {@link http://angular-ui.github.io/bootstrap/versioned-docs/1.3.3/#/datepicker}
      *           <p>По умолчанию datepickerOptions = { formatYear: 'yyyy', startingDay: 1, showWeeks: false }</p>
      *   @param {string | Date =} minDate (ссылка или значение) минимальная дата, можно указать через datepickerOptions.minDate, можно на прямую,
      *           а можно использовать строковые сокращения:
      *           <ul>
      *               <li>
      *                   <b>today-utc</b> - сегодняшняя дата по UTC. Задавая это сокращение для
      *                       minDate:
      *                       житель западного побережья сша увидит, к примеру 17 января, но
      *                       вот житель японии увидит 18 января своим "текущим днем".
      *                       Если нужна начальная дата с оглядкой на часовой пояс, то
      *                       лучше использовать сокращение 'today'.
      *               </li>
      *               <li>
      *                   <b>today</b> - сегодняшняя дата, по часовому поясу текущего пользователя.
      *               </li>
      *               <li>
      *                   <b>+1d</b> или <b>-1d</b> +/- несколько дней(обязательно указывать знаки + или -). Сдвиг идет от "текущего дня" пользователя.
      *               </li>
      *               <li>
      *                   <b>+2m</b> или <b>-2m</b> +/- несколько месяцев(обязательно указывать знаки + или -). Сдвиг идет от "текущего дня" пользователя.
      *               </li>
      *               <li>
      *                   <b>+5y</b> или <b>-5y</b> +/- несколько лет(обязательно указывать знаки + или -). Сдвиг идет от "текущего дня" пользователя.
      *               </li>
      *           </ul>
      *   @param {function=} dateDisabled функция для отключения определнных дат в пикере. как параметры передаются {date: date, mode: mode}
      *   @param {boolean=} [allowInvalid=true] (значение) по умолчанию datepicker не контролирует находится ли
      *           ngModel в диапазоне [minDate...maxDate]. Если передать allow-invalid='false',
      *           то при указании любого из minDate/maxDate(или обоих сразу) директива начнет
      *           контролировать значение ngModel
      *   @param {string=} [ngModelType='string'] (значение) тип значения ngModel. Eсли нужно отключить режим "только дат" и перестать принудительно
      *           конвертировать объект Date в string для ngModel, то передайте ng-model-type='date' <b>Внимание: тогда виджет
      *           не будет стараться исправить время для решения проблем с часовыми поясами!</b>
      *   @param {string=} [iconClass='fa-calendar-alt'] (значение) иконка для input-a
      *   @param {string=} [iconPosition='right'] (значение) положение иконки
      *   @param {string=} [inputClass='form-control'] (значение)
      *   @param {string=} [format='dd MMMM yyyy'] (значение) формат в котором текст отображается внутри input-a
      *   @param {string=} [placeholder='Choose a date'] (значение)
      *   @param {boolean=} ngDisabled (ссылка) флаг доступности тега
      *   @example
      *       <pre>
      *           $scope.date = new Date()
      *           $scope.date2 = new Date()
      *           $scope.date3 = new Date()
      *           $scope.date4 = new Date()
      *       </pre>
      *       <pre>
      *           //- pug
      *           //- С minDate == 'today'
      *           uic-datepicker-input(ng-model="date", min-date='today')
      *
      *           //- С minDate от 1 datepicker-a и с offset для maxDate
      *           uic-datepicker-input(ng-model="date2", min-date="date", max-date='+3m', icon-class='fa-car')
      *
      *           //- Тоже что и вариант 2, но еще и с allow-invalid='false'
      *           uic-datepicker-input(ng-model="date3", min-date="date", max-date='+3m', allow-invalid='false', icon-position='left')
      *
      *           //- Вариант без конвертирования Date -> string
      *           uic-datepicker-input(ng-model="date", ng-model-type='date')
      *       </pre>
      *       <example module="DocExampleApp">
      *           <file name="script.js">
      *               mkDocCtrl('uicDatepickerCtrl', {
      *                   date: new Date(),
      *                   date2: new Date(),
      *                   date3: new Date(),
      *                   date4: new Date()
      *               });
      *           </file>
      *           <file name="index.html">
      *               <div ng-controller="uicDatepickerCtrl">
      *                   <div class='row'>
      *                       <div class='col-md-33'>
      *                           <p style='height:3em'><b>1. </b>С minDate == 'today'</p>
      *                           <uic-datepicker-input ng-model="date" min-date='today'></uic-datepicker-input>
      *                           <hr>
      *                           <p>$scope.date = <b>{{date}}</b></p>
      *                       </div>
      *                       <div class='col-md-33'>
      *                           <p style='height:3em'><b>2. </b>С minDate от 1 datepicker-a и с offset для maxDate</p>
      *                           <uic-datepicker-input ng-model="date2" min-date="date" max-date='+3m' icon-class='fa-car'></uic-datepicker-input>
      *                           <hr>
      *                           <p>$scope.date2 = <b>{{date2}}</b></p>
      *                       </div>
      *                       <div class='col-md-33'>
      *                           <p style='height:3em'><b>3. </b>Тоже что и вариант 2, но еще и с allow-invalid='false'</p>
      *                           <uic-datepicker-input ng-model="date3" min-date="date" max-date='+3m' allow-invalid='false' icon-position='left'></uic-datepicker-input>
      *                           <hr>
      *                           <p>$scope.date3 = <b>{{date3}}</b></p>
      *                       </div>
      *                   </div>
      *                   <hr>
      *                   <div class='row'>
      *                       <div class='col-md-33'>
      *                           <p style='height:3em'><b>4. </b>Вариант без конвертирования Date -> string</p>
      *                           <uic-datepicker-input ng-model="date4" ng-model-type='date'></uic-datepicker-input>
      *                           <hr>
      *                           <p>$scope.date4 = <b>{{date4}}</b></p>
      *                       </div>
      *                   </div>
      *               </div>
      *           </file>
      *       </example>
       */
      restrict: 'E',
      require: 'ngModel',
      scope: {
        datepickerOptions: '=?',
        iconPosition: '@?',
        iconClass: '@?',
        inputClass: '@?',
        allowInvalid: '@?',
        format: '@?',
        name: '@?',
        placeholder: '@?',
        ngDisabled: '=?',
        ngRequired: '=?required'
      },
      controller: ["$scope", "$element", "$attrs", "$controller", function($scope, $element, $attrs, $controller) {
        $controller('UicDatepickerCtrl', {
          $scope: $scope,
          $element: $element,
          $attrs: $attrs
        });
        $scope.isOpened = false;
        $scope.open = function() {
          if ($scope.ngDisabled) {
            return;
          }
          $scope.isOpened = true;
        };
      }],
      template: ('/client/cms_app/_inputDirectives/uicDatepickerInput/uicDatepickerInput.html', '<div class="input-group"><span class="input-group-addon" ng-click="open()" ng-if="iconPosition==\'left\'"><span class="fas {{:: iconClass || \'fa-calendar-alt\'}}"></span></span><input class="form-control {{inputClass}}" type="text" uib-datepicker-popup="{{format || \'dd MMMM yyyy\'}}" ng-model="proxy" ng-model-options="proxyOptions" is-open="isOpened" datepicker-options="datepickerOptions" ng-required="ngRequired" ng-focus="open()" show-button-bar="false" name="{{name || \'noname\'}}" placeholder="{{placeholder || (\'Choose a date\' | translate)}}" ng-disabled="ngDisabled" autocomplete="off" onkeydown="return false" popup-placement="auto"/><span class="input-group-addon" ng-click="open()" ng-if="iconPosition==\'right\' || !iconPosition"><span class="fas {{:: iconClass || \'fa-calendar-alt\'}}"></span></span></div>' + '')
    };
  });

}).call(this);
;
(function() {
  angular.module('ui.cms').directive('uicDbModel', ["$constants", function($constants) {
    return {

      /**
      *   @ngdoc directive
      *   @name ui.cms.directive:uicDbModel
      *   @description
      *       директива для выбора id элементов коллекций из бд (можно выбирать разными стилями, как по 1 id - belongsTo, так и много - hasMany). Пример: в бд есть CmsArticle объекты, с полями id, title и т.п. (см примеры).
      *       <b>Директива полагается, что в $constants было описано свойство $REPRESENTATION.{modelName}. Пример: $REPRESENTATION.CmsArticle, в этом свойстве должно быть описано текстом как нужно отображать django-модель из бд в верстке, какой текст отображать в placeholder.
      *       Это значение должно генерироваться автоматом, с помощью drf_lbcms и drfs модулей для python.</b>
      *       <b class='text-danger'>Если $REPRESENTATION для выбранной модели нет, то будет отображено стандартное 'Select object', а модели будут отображаться как текст из ID</b>
      *       <div></div>
      *       TODO: дописать ссылки на доки для модулей питона
      *   @restrict E
      *   @param {int|string|Array|Object} ngModel (ссылка) модель
      *   @param {Array=} select (ссылка) массив объектов для рендеринга как select (выбирается только 1 id для ngModel)
      *   @param {Array=} radio (ссылка) массив объектов для рендеринга как radio-кнопки (выбирается только 1 id для ngModel)
      *   @param {array=} checkbox (ссылка) массив объектов для рендеринга checkbox-выбора (выбирается несколько id для ngModel. по умолчанию ngModel - объект)
      *       <b>При этом значение ngModel будет объектом, ключи у которого - id, а значения - true/false - указывают на то, используется ли этот id.
      *       Если нужно чтоб ngModel был массивом со значениями, то добавьте атрибут ng-model-type='array' к директиве. </b>
      *   @param {string=} inputClass css-класс для внутренней верстки. Можно, например передать класс 'input-sm' для select
      *       типа виджета, чтоб можно было уменьшить размер тега, или 'radio-inline'('inline') для radio типа, или 'checkbox-inline'('inline') для checkbox типа.
      *   @param {string=} [ngModelType='object'] тип значения ngModel - 'object' или 'array'. Этот атрибут влияет только при указании атрибута <b>checkbox</b>
      *   @param {string=} placeholder текст, который отображается на месте пустого ngModel (только при использовании select)
      *   @param {boolean=} ngDisabled (ссылка) флаг доступности тега
      *   @example
      *       <pre>
      *           # coffee
      *           $scope.myModel = null
      *           $scope.myModel2 = []
      *           CmsArticle.find (items)->
      *               $scope.articles = items
      *       </pre>
      *       <pre>
      *           //- pug
      *           p myModel = {{myModel | json}}; myModel2 = {{myModel2 | json}}
      *           p Выбираем через select тег
      *           uic-db-model(ng-model="myModel", select="articles", model-name='CmsArticle')
      *           p Выбираем через radio-кнопки
      *           uic-db-model(ng-model="myModel", radio="articles", model-name='CmsArticle', input-class='radio-inline')
      *           p Выбираем через chekbox (массив id)
      *           uic-db-model(ng-model="myModel", checkbox="articles", model-name='CmsArticle', ng-model-type='array')
      *       </pre>
      *       <example module="DocExampleApp">
      *           <file name="script.js">
      *               mkDocCtrl('uicDbModelCtrl', {
      *                   myModel: null,
      *                   articles: [
      *                       {id: 1, title: {ru: 'Статья 1'}},
      *                       {id: 5, title: {ru: 'О нас'}},
      *                       {id: 7, title: {ru: 'Статья 3'}},
      *                       {id: 10, title: {ru: 'Еще одна статья'}},
      *                       {id: 12, title: {ru: 'Еще одна статья №2'}},
      *                   ]
      *               });
      *           </file>
      *           <file name="index.html">
      *               <div ng-controller="uicDbModelCtrl">
      *                   <style>uic-db-model input[type="radio"]{margin-left: -20px !important;}</style>
      *                   <p><b>myModel = {{myModel | json}}; myModel2 = {{myModel2 | json}}</b></p>
      *                   <p>Выбираем через select тег</p>
      *                   <uic-db-model ng-model="myModel" select="articles" model-name='CmsArticle'>
      *                   </uic-db-model>
      *                   <p  style="margin-top: 0.7em;">Выбираем через radio-кнопки</p>
      *                   <uic-db-model ng-model="myModel" radio="articles" model-name='CmsArticle' input-class='radio-inline'>
      *                   </uic-db-model>
      *                   <p  style="margin-top: 0.7em;">Выбираем через checkbox (массив id)</p>
      *                   <uic-db-model ng-model="myModel2" checkbox="articles" model-name='CmsArticle' ng-model-type='array'>
      *                   </uic-db-model>
      *               </div>
      *           </file>
      *       </example>
       */
      restrict: 'E',
      require: 'ngModel',
      scope: {
        itemsRadio: '=?radio',
        itemsSelect: '=?select',
        itemsCheckbox: '=?checkbox',
        placeholder: '@?',
        ngDisabled: '=?',
        ngRequired: '=?'
      },
      controller: ["$scope", "$element", "$attrs", function($scope, $element, $attrs) {
        var ngModelCtrl, r, templateForItem;
        ngModelCtrl = $element.controller('ngModel');
        $scope.proxy = null;
        $scope.options = {};
        if ($attrs.select) {
          r = $constants.resolve("$REPRESENTATION." + $attrs.modelName) || {};
          templateForItem = $attrs.templateForItem || r.html || "{{$item.id}}";
          $scope.options.choicesTemplate = templateForItem;
          $scope.options.selectedTemplate = templateForItem.replaceAll('$item', '$selected');
        }
        if ($attrs.checkbox && $attrs.ngModelType === 'array') {
          ngModelCtrl.$render = function() {
            var i, k, len, ref;
            $scope.proxy = {};
            ref = ngModelCtrl.$modelValue || [];
            for (i = 0, len = ref.length; i < len; i++) {
              k = ref[i];
              if (k !== (void 0) && k !== null) {
                $scope.proxy[k] = true;
              }
            }
          };
          return $scope.$watch('proxy', function(proxy, oldValue) {
            var error, ids, k, ref, v, value;
            value = [];
            ids = ($scope.itemsCheckbox || []).map(function(item) {
              return item.id;
            });
            ref = proxy || {};
            for (k in ref) {
              v = ref[k];
              if (!(v)) {
                continue;
              }
              if (ids.includes(k)) {
                value.push(k);
                continue;
              }
              try {
                k = parseInt(k);
              } catch (error1) {
                error = error1;
                continue;
              }
              if (ids.includes(k)) {
                value.push(k);
              } else {
                value.push(k);
              }
            }
            value.sort();
            ngModelCtrl.$setViewValue(value);
          }, true);
        } else {
          ngModelCtrl.$render = function() {
            $scope.proxy = ngModelCtrl.$modelValue;
          };
          return $scope.$watch('proxy', function(proxy, oldValue) {
            ngModelCtrl.$setViewValue(proxy);
          });
        }
      }],
      template: function($element, $attrs) {
        var inputClass, ngRequired, placeholder, r, required, templateForItem;
        if (($attrs.select && $attrs.radio) || ($attrs.radio && $attrs.checkbox) || ($attrs.select && $attrs.checkbox)) {
          throw "You should specify only 1 attribute for uic-db-model (checkbox/radio/select)";
        }
        r = $constants.resolve("$REPRESENTATION." + $attrs.modelName) || {};
        inputClass = $attrs.inputClass || '';
        templateForItem = $attrs.templateForItem || r.html || "{{$item.id}}";
        placeholder = r.placeholder || 'Select object';
        if ($attrs.hasOwnProperty('required') || $attrs.hasOwnProperty('allowNull')) {
          console.warn("uicDbModel: required and allowNull properties are not allowed!");
        }
        if ($attrs.required !== false && $attrs.hasOwnProperty('required')) {
          required = "required=''";
        }
        if ($attrs.hasOwnProperty('allowNull')) {
          if ($attrs.allowNull === 'false') {
            required = "required=''";
          } else {
            required = '';
          }
        }
        if ($attrs.select) {
          ngRequired = 'ngRequired';
          if (required) {
            ngRequired = 'true';
          }
          return "<uic-select input-class='" + inputClass + "'\n    ng-model=\"proxy\"\n    ng-disabled=\"ngDisabled\"\n    items=\"itemsSelect\"\n    placeholder=\"{{placeholder || ('" + placeholder + "' | translate)}}\"\n    ng-required=\"" + ngRequired + "\"\n    selected-template=\"{{options.selectedTemplate}}\"\n    choices-template=\"{{options.choicesTemplate}}\">\n    <item>" + templateForItem + "</item>\n    <selected>" + (templateForItem.replaceAll('$item', '$selected')) + "</selected>\n</uic-select>";
        }
        if ($attrs.radio) {
          if (inputClass === 'radio-inline' || inputClass === 'inline') {
            return "<label class='radio-inline' ng-repeat=\"$item in itemsRadio\">\n    <input type='radio' ng-model=\"$parent.proxy\"\n           ng-disabled=\"$parent.ngDisabled || $item.disabled\"\n           ng-value=\"$item.id\"/>\n    " + templateForItem + "\n</label>";
          } else {
            return "<div class='radio' ng-repeat=\"$item in itemsRadio\">\n    <label class='radio " + inputClass + "'>\n        <input type='radio' ng-model=\"$parent.proxy\"\n               ng-disabled=\"$parent.ngDisabled || $item.disabled\"\n               ng-value=\"$item.id\"/>\n        " + templateForItem + "\n    </label>\n</div>";
          }
        }
        if ($attrs.checkbox) {
          if (inputClass === 'checkbox-inline' || inputClass === 'inline') {
            return "<label class='checkbox checkbox-inline' ng-repeat=\"$item in itemsCheckbox\" ng-class=\"{'nomargin-top': $first}\">\n    <input type='checkbox'\n           ng-model=\"$parent.proxy[$item.id]\"\n           ng-disabled='$parent.ngDisabled || $item.disabled'/>\n    " + templateForItem + "\n</label>";
          } else {
            return "<div class='checkbox " + inputClass + "' ng-repeat=\"$item in itemsCheckbox\" ng-class=\"{'nomargin-top': $first}\">\n    <label>\n        <input type='checkbox'\n               ng-model=\"$parent.proxy[$item.id]\"\n               ng-disabled='$parent.ngDisabled || $item.disabled'/>\n        " + templateForItem + "\n    </label>\n</div>";
          }
        }
        return "";
      }
    };
  }]);

}).call(this);
;
(function() {
  angular.module('ui.cms').config(["$uicIntegrationsProvider", "$directiveOverrideProvider", "$injector", "uicHtmlEditorProvider", "HProvider", function($uicIntegrationsProvider, $directiveOverrideProvider, $injector, uicHtmlEditorProvider, HProvider) {
    var H, conf;
    H = HProvider.$get();
    conf = {
      config: {
        filebrowserUploadUrl: '/none'
      },
      toolbars: {
        basic: {
          toolbarGroups: [
            {
              name: 'clipboard',
              groups: ['clipboard', 'undo']
            }, {
              name: 'links',
              groups: ['links']
            }, {
              name: 'tools',
              groups: ['tools']
            }, {
              name: 'others',
              groups: ['others']
            }, {
              name: 'basicstyles',
              groups: ['basicstyles']
            }, {
              name: 'paragraph',
              groups: ['list', 'indent', 'blocks', 'align', 'bidi', 'paragraph']
            }, {
              name: 'styles',
              groups: ['styles']
            }
          ],
          removeButtons: 'Subscript,Superscript,Cut,Copy,About,Paste,Scayt,Anchor,Image,Table,HorizontalRule,Maximize,Outdent,Indent,Blockquote'
        },
        advanced: {
          toolbarGroups: [
            {
              name: 'clipboard',
              groups: ['clipboard', 'undo']
            }, {
              name: 'editing',
              groups: ['find', 'selection', 'spellchecker', 'editing']
            }, {
              name: 'links',
              groups: ['links']
            }, {
              name: 'insert',
              groups: ['insert', 'fontawesome']
            }, {
              name: 'basicstyles',
              groups: ['basicstyles', 'colors', 'cleanup']
            }, {
              name: 'paragraph',
              groups: ['list', 'indent', 'blocks', 'align', 'bidi', 'paragraph']
            }, {
              name: 'styles',
              groups: ['styles']
            }, {
              name: 'others',
              groups: ['tools', 'mode', 'document', 'doctools', 'others']
            }
          ],
          removeButtons: 'Paste,Subscript,Superscript,Cut,Copy,About'
        }
      }
    };
    $uicIntegrationsProvider.registerThirdPartyLibrary('ckeditor4', ['/bower_components/angular-lbcms-libs/external/ckeditor/4/ckeditor.js'], conf);
    return uicHtmlEditorProvider.setDefaultHtmlEditor('ckeditor4');
  }]).directive('uicSrefForCkeditor', ["$injector", "$state", "$filter", function($injector, $state, $filter) {

    /*
        установка url к статьям для текста, созданного из ckeditor
     */
    var $models, CmsArticle, articlesCache, setArticleHref, toUrlIdFilter;
    $models = null;
    CmsArticle = null;
    toUrlIdFilter = null;
    articlesCache = {};
    setArticleHref = function($element, article) {
      var href;
      toUrlIdFilter = toUrlIdFilter || $filter('toUrlId');
      href = $state.href('app.articles.details', {
        urlId: toUrlIdFilter(article),
        categoryId: toUrlIdFilter(article.$category || {
          id: '-'
        })
      });
      $element.attr('href', href);
    };
    return {
      restrict: 'A',
      link: function($scope, $element, $attrs) {
        var article, filter, id, uiSref;
        uiSref = $attrs.uicSrefForCkeditor || '';
        if (uiSref.indexOf('app.articles.details') !== 0) {
          return;
        }
        id = uiSref.split('(')[1].split(')')[0].split('id:')[1].replaceAll('"', '').replaceAll('}', '').trim();
        id = parseInt(id);
        if (isNaN(id)) {
          return;
        }
        $models = $models || $injector.get('$models');
        article = articlesCache[id] || $models.CmsArticle.findItemByField('id', id);
        if (!article || (article.category && !article.$category)) {
          CmsArticle = CmsArticle || $injector.get('CmsArticle');
          filter = {
            fields: {
              id: true,
              title: true,
              category: true,
              $category: true
            }
          };
          CmsArticle.findById({
            id: id,
            filter: filter,
            expand: {
              category: true
            }
          }, function(article) {
            articlesCache[id] = article;
            return setArticleHref($element, article);
          });
          return;
        }
        setArticleHref($element, article);
      }
    };
  }]).directive('uicHtmlEditorCkeditor4', ["$uicIntegrations", "$injector", "$langPicker", "$q", function($uicIntegrations, $injector, $langPicker, $q) {
    var $timeout, ckeditorIntegration, loadCkeditor;
    $timeout = null;
    ckeditorIntegration = $uicIntegrations.getThirdPartyLibrary('ckeditor4');
    loadCkeditor = function(onReady) {
      var check;
      check = function() {
        if (CKEDITOR.replace) {
          return onReady();
        }
        $timeout = $timeout || $injector.get('$timeout');
        $timeout(check, 300);
      };
      ckeditorIntegration.load().then(check);
    };
    return {
      restrict: 'A',
      require: ['uicHtmlEditorCkeditor4', 'ngModel'],
      scope: {
        cmsModelItem: '=?',
        minHeight: '@?'
      },
      controller: ["$scope", "$element", "$attrs", "$parse", "$q", function($scope, $element, $attrs, $parse, $q) {
        var config, editorElement, heightToPx, instance, readyDeferred, toolbar;
        config = $parse($attrs.options)($scope.$parent) || {};
        config.toolbar = config.toolbar || $attrs.toolbar;
        config = angular.merge(angular.copy(ckeditorIntegration.config.config), config);

        /*
        if !config.contentsCss
            config.contentsCss = []
            for link in document.getElementsByTagName('link')
                if link.href and link.href.indexOf('ckeditor') == -1
                    config.contentsCss.push(link.href)
            config.contentsCss.push(self.cdn.replace('/ckeditor.js', '/contents.css'))
         */
        editorElement = $element[0];
        instance = void 0;
        readyDeferred = $q.defer();
        if (!config.language) {
          config.language = $langPicker.currentLang;
        }
        toolbar = {};
        if (config.toolbar === 'advanced' || config.toolbar === 'undefined' || !config.toolbar) {
          toolbar = ckeditorIntegration.config.toolbars['advanced'];
        }
        if (config.toolbar === 'basic') {
          toolbar = ckeditorIntegration.config.toolbars['basic'];
        }
        config = angular.merge(config, toolbar);

        /**
         * Listen on events of a given type.
         * This make all event asynchronous and wrapped in $scope.$apply.
         *
         * @param {String} event
         * @param {Function} listener
         * @returns {Function} Deregistration function for this listener.
         */
        this.onCKEvent = function(event, listener) {
          var applyListener;
          applyListener = function() {
            var args;
            args = arguments;
            $scope.$apply(function() {
              listener.apply(null, args);
            });
          };
          instance.on(event, applyListener);
          return function() {
            instance.removeListener(event, applyListener);
          };
        };

        /**
         * Check if the editor if ready.
         *
         * @returns {Promise}
         */
        this.ready = function() {
          return readyDeferred.promise;
        };
        $scope.$on('$destroy', function() {
          readyDeferred.promise.then(function() {
            instance.destroy(false);
          });
        });
        heightToPx = function(value) {
          var fsize;
          if (!value) {
            return;
          }
          if (value.indexOf('px') > -1) {
            return parseInt(value.replace('px', ''));
          } else if (value.indexOf('em') > -1) {
            fsize = getComputedStyle(document.body).fontSize;
            fsize = heightToPx(fsize);
            if (!fsize) {
              return 0;
            }
            value = parseInt(value.replace('em', ''));
            return value * fsize;
          }
        };
        $element.parent().addClass('loading-data');
        loadCkeditor((function(_this) {
          return function() {
            CKEDITOR.$injector = $injector;
            if ($injector.has("uicSpecialCkeditorCardEditorDirective")) {
              config.extraPlugins = 'linkcard';
            }
            if (!config.hasOwnProperty('image2_disableResizer')) {
              config.image2_disableResizer = true;
            }
            config.allowedContent = {
              $1: {
                elements: CKEDITOR.dtd,
                attributes: true,
                styles: true,
                classes: true
              }
            };
            config.disallowedContent = 'script; *[on*]';
            if (editorElement.hasAttribute('contenteditable') && editorElement.getAttribute('contenteditable').toLowerCase() === 'true') {
              instance = _this.instance = CKEDITOR.inline(editorElement, config);
            } else {
              instance = _this.instance = CKEDITOR.replace(editorElement, config);
            }
            _this.onCKEvent('instanceReady', function() {
              var cb, i, len, minHeight, ref;
              $element.parent().addClass('ready');
              instance.$scope = $scope;
              minHeight = heightToPx($scope.minHeight || '40em');
              if (minHeight) {
                instance.resize('100%', minHeight, true, true);
              }
              ref = ckeditorIntegration.onReadyCallbacks;
              for (i = 0, len = ref.length; i < len; i++) {
                cb = ref[i];
                cb(instance);
              }
              readyDeferred.resolve(true);
            });
          };
        })(this));
      }],
      link: function($scope, $element, $attrs, ctrls) {
        var controller, ngModelController;
        controller = ctrls[0];
        ngModelController = ctrls[1];
        controller.ready().then(function() {
          var event, i, len, ref;
          ref = ['dataReady', 'change', 'blur', 'saveSnapshot'];
          for (i = 0, len = ref.length; i < len; i++) {
            event = ref[i];
            controller.onCKEvent(event, function() {
              ngModelController.$setViewValue(controller.instance.getData() || '');
            });
          }
          controller.instance.setReadOnly(!!$attrs.readonly);
          $attrs.$observe('readonly', function(readonly) {
            controller.instance.setReadOnly(!!readonly);
          });
        });
        ngModelController.$render = function() {
          controller.ready().then(function() {
            controller.instance.setData(ngModelController.$viewValue || '', {
              noSnapshot: true,
              callback: function() {
                controller.instance.fire('updateSnapshot');
              }
            });
          });
        };
      }
    };
  }]);

}).call(this);
;
(function() {
  var defaultHtmlEditor;

  defaultHtmlEditor = null;

  angular.module('ui.cms').provider('uicHtmlEditor', function() {
    this.setDefaultHtmlEditor = function(name) {
      defaultHtmlEditor = name;
    };
    this.$get = angular.noop;
    return this;
  }).directive('uicHtmlEditor', ["H", function(H) {
    return {
      restrict: 'E',
      scope: {
        ngModel: '=',
        cmsModelItem: '=?',
        ngDisabled: '=?',
        maxlength: '@?',
        minlength: '@?',
        name: '@?',
        minHeight: '@?'
      },
      controller: ["$scope", "$attrs", "$parse", function($scope, $attrs, $parse) {
        $scope.options = $parse($attrs.options)($scope.$parent) || {};
      }],
      template: function($element, $attrs) {
        var attrs;
        attrs = {
          name: '{{::name}}',
          'ng-disabled': 'ngDisabled',
          'ng-model': 'ngModel',
          'cms-model-item': 'cmsModelItem',
          'min-height': $attrs.minHeight,
          style: $attrs.style || '',
          options: 'options',
          toolbar: $attrs.toolbar
        };
        if ($attrs.hasOwnProperty('placeholder')) {
          attrs.placeholder = '{{placeholder | translate}}';
        }
        if ($attrs.hasOwnProperty('required')) {
          attrs.required = '';
        }
        attrs.maxlength = '{{maxlength}}';
        attrs.minlength = '{{minlength}}';
        return "<div uic-html-editor-" + defaultHtmlEditor + "='' " + (H.convert.objToHtmlProps(attrs)) + ">\n</div>";
      }
    };
  }]);

}).call(this);
;
(function() {
  angular.module('ui.cms').directive('uicInputFile', function() {
    return {

      /**
      *   @ngdoc directive
      *   @name ui.cms.directive:uicInputFile
      *   @description директива-замена стандартного тега input(type='file'),
      *       поддерживает выбор только одного файла. Добавлена, поскольку ангулар
      *       не может нормально ссылаться на ngModel. Эта директива является надстройкой
      *       над ngFile, подробнее cms_app/_directives/ngFile.coffee
      *       <br/>
      *   @restrict E
      *   @param {object} ngModel (ссылка)
      *   @param {string=} [accept='*'] (значение) маска MIME типов файлов для выбора(через запятую)
      *   @param {string=} maxSize (значение) максимальный размер файла в ['b', 'kb', 'mb'].
      *       Если размер файла больше указанного, то для формы будет установлена ошибка валидации maxsize.
      *       Прим.: <b>my_form.my_file.$error.maxsize</b> == true
      *   @param {boolean=} ngDisabled (значение) неактивен ли тег для взаимодействия
      *   @example
      *       <pre>
      *           //- pug
      *           uic-input-file.btn.btn-success(
      *               ng-model="myFile",
      *               accept='video/*,audio/*',
      *               max-size='15 MB'
      *           )
      *       </pre>
       */
      restrict: 'E',
      transclude: true,
      require: 'ngModel',
      scope: {
        accept: '@?',
        maxSize: '@?',
        multiple: '@?',
        ngDisabled: '=?'
      },
      controller: ["$scope", "$element", "$cms", "gettextCatalog", "H", function($scope, $element, $cms, gettextCatalog, H) {
        var allowMultiple, fileInput, ngModelCtrl, ref, validateFile;
        fileInput = $element.find('input');
        allowMultiple = (ref = $scope.multiple) === 'true' || ref === true;
        ngModelCtrl = $element.controller('ngModel');
        if (allowMultiple) {
          fileInput[0].multiple = true;
        }
        validateFile = function(file) {
          var accepted, f, fileListCleaned, i, len, maxsize;
          if (file instanceof FileList) {
            if (!file.length) {
              ngModelCtrl.$setValidity('acceptType', true);
              ngModelCtrl.$setValidity('maxsize', true);
              return;
            }
            fileListCleaned = [];
            if ($scope.maxSize) {
              maxsize = H.l10nFile.humanFileSizeToBytes($scope.maxSize);
            }
            for (i = 0, len = file.length; i < len; i++) {
              f = file[i];
              accepted = H.l10nFile.isValidFileType(f.type, $scope.accept || '*');
              if (!accepted) {
                $cms.showNotification(gettextCatalog.getString("Invalid file type. Allowed are: <b>{{options.accept}}</b>", {
                  options: {
                    accept: $scope.accept
                  }
                }), null, 'error');
                continue;
              }
              if (maxsize && f.size) {
                if (f.size > maxsize) {
                  $cms.showNotification(gettextCatalog.getString("File {{name}} is too big!", {
                    name: f.name
                  }), null, 'error');
                  continue;
                }
              }
              fileListCleaned.push(f);
            }
            return fileListCleaned;
          }
          if (!file) {
            ngModelCtrl.$setValidity('acceptType', true);
            ngModelCtrl.$setValidity('maxsize', true);
            return;
          }
          accepted = H.l10nFile.isValidFileType(file.type, $scope.accept || '*');
          ngModelCtrl.$setValidity('acceptType', accepted);
          if (!accepted) {
            return;
          }
          if (!$scope.maxSize || !file.size) {
            ngModelCtrl.$setValidity('maxsize', true);
            return file;
          }
          maxsize = H.l10nFile.humanFileSizeToBytes($scope.maxSize);
          if (file.size > maxsize) {
            ngModelCtrl.$setValidity('maxsize', false);
            return;
          }
          ngModelCtrl.$setValidity('maxsize', true);
          return file;
        };
        return fileInput.bind('change', function(changeEvent) {
          $scope.$apply(function() {
            var file;
            if (allowMultiple) {
              file = validateFile(changeEvent.target.files);
            } else {
              file = validateFile(changeEvent.target.files[0]);
            }
            $scope.proxy = file;
            return ngModelCtrl.$setViewValue(file);
          });
        });
      }],
      template: ('/client/cms_app/_inputDirectives/uicInputFile/uicInputFile.html', '<div ng-switch="!proxy" ng-if="!multiple"><div class="text" ng-transclude="" ng-switch-when="true"><span translate="">Select file</span></div><div class="text" translate="" ng-switch-when="false">Change file</div></div><div ng-switch="!proxy.length" ng-if="multiple"><div class="text" ng-transclude="" ng-switch-when="true"><span translate="">Select files</span></div><div class="text" translate="" ng-switch-when="false">Change files</div></div><input type="file" accept="{{ accept || \'*\'}}" ng-hide="ngDisabled"/>' + '')
    };
  });

}).call(this);
;
(function() {
  angular.module('ui.cms').directive('uicLangPicker', ["$cms", "$compile", function($cms, $compile) {
    return {

      /**
      *   @ngdoc directive
      *   @name ui.cms.directive:uicLangPicker
      *   @description
      *       Пикер языка. Языки - не все перечисленные в CMS_ALLOWED_LANGS, а только те, которые
      *       перечислены в настройках сайта - CmsSettings - в поле cmsLangCodes. Если доступен только 1 язык, то дропдаун рендерится с disabled
      *
      *   @restrict E
      *   @param {string} ngModel (ссылка) iso-код языка
      *   @param {string=} [display='select'] (значение) как рендерить элемент выбора языка (как select или как бутстраповский dropdown)
      *   @param {string=} [size='md'] (значение) размер пикера - 'lg', 'md', 'sm', 'xs'
      *   @param {boolean=} ngDisabled (ссылка) флаг доступности тега
      *   @example
      *       <pre>
      *           $scope.lang = "ru"
      *       </pre>
      *       <pre>
      *           //- pug
      *           uic-lang-picker(ng-model="lang")
      *           uic-lang-picker(ng-model="lang", size='sm')
      *           uic-lang-picker(ng-model="lang", size='sm', display='dropdown')
      *           uic-lang-picker(ng-model="lang", size='xs', display='dropdown')
      *           //- как часть input-a
      *           .input-group
      *               input.form-control(placeholder='Search for...')
      *               .input-group-btn
      *                   uic-lang-picker(ng-model="lang", display='dropdown')
      *
      *       </pre>
      *       <example module="DocExampleApp">
      *           <file name="script.js">
      *               mkDocCtrl('uicLangPickerCtrl', {
      *                   lang: "ru"
      *               });
      *           </file>
      *           <file name="index.html">
      *               <div ng-controller="uicLangPickerCtrl">
      *                 <uic-lang-picker ng-model="lang"></uic-lang-picker>
      *                 <div  style='margin-bottom:1em;'></div>
      *                 <uic-lang-picker ng-model="lang" size='sm'></uic-lang-picker>
      *                 <div  style='margin-bottom:1em;'></div>
      *                 <uic-lang-picker ng-model="lang" size='sm' display='dropdown'></uic-lang-picker>
      *                 <div  style='margin-bottom:1em;'></div>
      *                 <uic-lang-picker ng-model="lang" size='xs' display='dropdown'></uic-lang-picker>
      *                 <div  style='margin-bottom:1em;'></div>
      *                 <div class="input-group">
      *                      <input type="text" class="form-control" placeholder="Search for...">
      *                      <span class="input-group-btn">
      *                         <uic-lang-picker ng-model="lang" display='dropdown'></uic-lang-picker>
      *                      </span>
      *                 </div>
      *               </div>
      *           </file>
      *       </example>
       */
      restrict: 'E',
      scope: {
        ngModel: '=',
        size: '@?',
        display: '@?',
        ngDisabled: '=?'
      },
      controller: ["$scope", "$element", function($scope, $element) {
        var _class, template;
        $scope.singleLang = CMS_ALLOWED_LANGS.length === 1 && CMS_ALLOWED_LANGS[0] === CMS_DEFAULT_LANG;
        $scope.$cms = $cms;
        _class = '';
        template = '';
        if ($scope.display === 'dropdown') {
          if ($scope.size) {
            _class = 'btn-' + $scope.size;
          }
          template = "<div uib-dropdown=''>\n    <button type=\"button\" class=\"btn btn-default dropdown-toggle " + _class + "\" uib-dropdown-toggle ng-disabled=\"ngDisabled || singleLang\">\n        {{$cms.LANG_MAP[ngModel]}}\n        <span class=\"caret\"></span>\n    </button>\n    <ul class=\"dropdown-menu dropdown-menu-right\"  uib-dropdown-menu>\n        <li ng-repeat=\"(key, value) in $cms.settings.cmsLangCodesJson\"\n            ng-class=\"{'active': value==ngModel}\">\n            <a ng-click=\"setLang(value)\" style=\"cursor:pointer;\">\n                {{key}}\n            </a>\n        </li>\n    </ul>\n  </div>";
          $scope.setLang = function(l) {
            return $scope.ngModel = l;
          };
        } else {
          if ($scope.size === 'sm' || $scope.size === 'xs') {
            _class = 'input-sm';
          }
          template = "<select class=\"form-control " + _class + "\"\n            ng-options=\"key for (key,value) in $cms.settings.cmsLangCodesJson\"\n            ng-model=\"ngModel\"\n            ng-disabled=\"ngDisabled || singleLang\">\n</select>";
        }
        $element.html(template);
        $compile($element.contents())($scope);
      }]
    };
  }]);

}).call(this);
;
(function() {
  angular.module('ui.cms').directive('uicLocalFilePicker', function() {
    return {

      /**
      *   @ngdoc directive
      *   @name ui.cms.directive:uicLocalFilePicker
      *   @description надстройка над uicInputFile отображает превью для картинки, выбранной на компьютере пользователя, или если это файл другого типа - иконку типа файла.
      *   @restrict E
      *   @param {object} ngModel (ссылка) File
      *   @param {object=} oldL10nFile (ссылка) Старый L10nFile
      *   @param {string=} [accept='*'] (значение) маска MIME типов файлов для выбора(через запятую)
      *   @param {string=} maxSize (значение) максимальный размер файла в ['b', 'kb', 'mb'].
      *       Если размер файла больше указанного, то для формы будет установлена ошибка валидации maxsize.
      *       Прим.: <b>my_form.my_file.$error.maxsize</b> == true
      *   @param {boolean=} ngDisabled (значение) неактивен ли тег для взаимодействия
      *   @param {string=} forceCmsStorageType (значение)
      *   @example
      *       <pre>
      *           //- pug
      *           p Виджет выбора файла с превью (выберите картинку или другой файл)
      *           uic-local-file-picker.btn.btn-success(
      *               ng-model="myFile",
      *               max-size='15 MB'
      *           )
      *       </pre>
      *       <example module="DocExampleApp">
      *           <file name="script.js">
      *               mkDocCtrl('uicInputFilePreviewCtrl', {
      *                   myFile: null
      *               });
      *           </file>
      *           <file name="index.html">
      *               <div ng-controller="uicInputFilePreviewCtrl" class='col-md-30'>
      *                   <p>Виджет выбора файла (выберите картинку или другой файл)</p>
      *                   <uic-local-file-picker ng-model="myFile" max-size='15 mb'></uic-local-file-picker>
      *               </div>
      *               <div class='clearfix'></div>
      *           </file>
      *       </example>
       */
      restrict: 'E',
      transclude: {
        noFile: "?uicLocalFilePickerNoFile"
      },
      scope: {
        ngModel: '=',
        oldL10nFile: '=?',
        name: '@?',
        accept: '@?',
        maxSize: '@?',
        forceCmsStorageType: '@?',
        ngDisabled: '=?'
      },
      controller: ["$scope", "H", function($scope, H) {
        var imgFilter;
        imgFilter = /^(?:image\/bmp|image\/cis\-cod|image\/gif|image\/ief|image\/jpeg|image\/jpeg|image\/jpeg|image\/pipeg|image\/png|image\/svg\+xml|image\/tiff|image\/x\-cmu\-raster|image\/x\-cmx|image\/x\-icon|image\/x\-portable\-anymap|image\/x\-portable\-bitmap|image\/x\-portable\-graymap|image\/x\-portable\-pixmap|image\/x\-rgb|image\/x\-xbitmap|image\/x\-xpixmap|image\/x\-xwindowdump)$/i;
        $scope.isImage = false;
        $scope.data = '';
        $scope.error = '';
        return $scope.$watch('ngModel', function(file) {
          var reader;
          $scope.error = '';
          $scope.data = '';
          $scope.isImage = false;
          if (!file) {
            return;
          }
          if (!imgFilter.test(file.type) || angular.isUndefined(FileReader)) {
            $scope.data = H.l10nFile.getTypeIconNameByFileType(file.type);
            return;
          }
          $scope.isImage = true;
          reader = new FileReader();
          reader.onloadend = function() {
            $scope.$apply(function() {
              return $scope.data = reader.result;
            });
          };
          reader.readAsDataURL(file);
        });
      }],
      template: ('/client/cms_app/_inputDirectives/uicLocalFilePicker/uicLocalFilePicker.html', '<div class="thumbnail" ng-switch="!!ngModel"><div ng-switch-when="true" ng-switch="!!$parent.isImage"><div ng-switch-when="true"><img class="w-100" ng-src="{{data}}"/></div><div class="noimage-placeholder" ng-switch-when="false"><span ng-show="!data" uic-transclude="noFile"><span translate="">No file</span></span><div ng-if="data"><span class="fas {{data}}"></span><span class="title">{{ngModel.name | cut:true:20}}</span></div></div></div><div ng-switch-when="false" ng-switch="!!oldL10nFile.id"><div ng-switch-when="true"><uic-file-thumb file="oldL10nFile" force-cms-storage-type="{{$parent.forceCmsStorageType}}" size="440x250"></uic-file-thumb></div><div class="noimage-placeholder" ng-switch-when="false" uic-transclude="noFile"><span translate="">No file</span></div></div></div><div class="text-center"><uic-input-file class="btn btn-sm btn-success" ng-model="ngModel" accept="{{accept || \'*\'}}" name="{{name}}" max-size="{{maxSize}}"></uic-input-file></div>' + '')
    };
  });

}).call(this);
;
(function() {
  angular.module('ui.cms').directive('uicMapCoordinatePicker', function() {
    var getDefaultProxy, toInt;
    getDefaultProxy = function() {
      return {
        lat: null,
        lng: null,
        exists: false,
        text: ''
      };
    };
    toInt = function(value, defaultValue) {
      var e;
      try {
        value = parseInt(value);
      } catch (error) {
        e = error;
        value = defaultValue;
      }
      if (angular.isNumber(value)) {
        return value;
      }
      return defaultValue;
    };
    return {

      /**
      *   @ngdoc directive
      *   @name ui.cms.directive:uicMapCoordinatePicker
      *   @description
      *       Пикер координаты на основе Google Maps. Его стоит использовать, т.к. библиотеки от гуглекарт подгружаются только тогда, когда виджет видим, а не всегда по умолчанию(что только увеличивает размер загружаемой пользователем старницы)
      *   @restrict E
      *   @param {object} ngModel (ссылка) модель GeoPoint
      *   @param {number=} [zoom=15] (значение) величина масштаба карты по умолчанию
      *   @param {string=} [mode='edit'] (значение) тип отображения карты - 'edit', 'view'. При значении 'view' параметры allowSearchbox, hideSetLocationBtn, placeholder игнорируются
      *   @param {string=} [markerColor='green'] (значение) цвет маркера
      *   @param {string=} markerIcon (значение) путь к svg-картинке маркера. Можно указать либо markerIcon либо markerColor
      *   @param {boolean} [allowSearchbox=false] (значение) отображать ли гуглевский searchbox в инпуте
      *   @param {string=} autocompleteTypes (значение) типы адресов в поисковом инпуте. Все возможные значения перечислены в {@link https://developers.google.com/maps/documentation/javascript/places-autocomplete#add_autocomplete options.types} переменной. Указывать значения через запятую
      *   @param {boolean=} [hideSetLocationBtn=false] (значение) скрывать ли кнопку для выбора маркером на карте, когда allowSearchbox == true
      *   @param {string=} placeholder (значение) для карты с searchbox-ом текст в инпуте - Type to search location. Без searchbox-а - Select location
      *   @param {string=} required установите этот атрибут в директиве для того, чтоб значение ng-model==null приводило к ошибкам валидации формы
      *   @param {boolean=} ngDisabled (ссылка) флаг доступности тега
      *   @example
      *       <pre>
      *           $scope.myLocation = null
      *       </pre>
      *       <pre>
      *           //- pug
      *           p Значение myLocation = {{myLocation | json}}
      *           p В виджете резрешен searchbox и сбросить значение нельзя
      *           uic-map-coordinate-picker(ng-model="myLocation", allow-searchbox='true', required)
      *           p В виджете резрешен searchbox и сбросить значение можно
      *           uic-map-coordinate-picker(ng-model="myLocation", allow-searchbox='true')
      *           p В виджете НЕ резрешен searchbox и сбросить значение можно
      *           uic-map-coordinate-picker(ng-model="myLocation")
      *       </pre>
      *       <example module="DocExampleApp">
      *           <file name="script.js">
      *               mkDocCtrl('uicMapCoordinatePickerCtrl', {
      *                   myLocation: null
      *               });
      *           </file>
      *           <file name="index.html">
      *               <div ng-controller="uicMapCoordinatePickerCtrl">
      *                   <p><b>Значение myLocation = {{myLocation | json}}</b></p>
      *                   <p>В виджете резрешен searchbox и сбросить значение нельзя</p>
      *                   <uic-map-coordinate-picker ng-model="myLocation" allow-searchbox='true' required>
      *                   </uic-map-coordinate-picker>
      *                   <hr/>
      *                   <p>В виджете резрешен searchbox и сбросить значение можно</p>
      *                   <uic-map-coordinate-picker ng-model="myLocation" allow-searchbox='true'>
      *                   </uic-map-coordinate-picker>
      *                   <hr/>
      *                   <p>В виджете НЕ резрешен searchbox и сбросить значение можно</p>
      *                   <uic-map-coordinate-picker ng-model="myLocation">
      *                   </uic-map-coordinate-picker>
      *               </div>
      *           </file>
      *       </example>
       */
      restrict: 'E',
      require: 'ngModel',
      scope: {
        markerColor: '@?',
        markerIcon: '@?',
        zoom: '@?',
        placeholder: '@?',
        ngDisabled: '=?'
      },
      controller: ["$scope", "$element", "$attrs", "$timeout", "$location", "$worldMaps", "gettextCatalog", function($scope, $element, $attrs, $timeout, $location, $worldMaps, gettextCatalog) {
        var WorldMapClass, autocompleteBox, forceRedrawGMap, getLocationForMapCenter, gm, initMap, mapMarker, ngModelCtrl, ref, ref1, ref2, searchBox, updateProxyFromEvent, zoom;
        WorldMapClass = $worldMaps.getFactory();
        if (!WorldMapClass) {
          $element.html("<div class='alert alert-danger nomargin'>\n    <b>" + (gettextCatalog.getString("World maps are disabled by admin")) + "</b>\n</div>");
          return;
        }
        ngModelCtrl = $element.controller('ngModel');
        $scope.proxy = getDefaultProxy();
        $scope.options = {
          isMapVisible: false,
          allowSearchbox: false,
          showSetLocationBtn: true,
          autocompleteTypes: ($attrs.autocompleteTypes || '').split(',').filter(function(t) {
            return !!t;
          }),
          required: false
        };
        if ($attrs.hasOwnProperty('allowSearchbox') && ((ref = $attrs.allowSearchbox) !== false && ref !== 'false')) {
          $scope.options.allowSearchbox = true;
        }
        if ($attrs.hasOwnProperty('hideSetLocationBtn') && ((ref1 = $attrs.hideSetLocationBtn) !== false && ref1 !== 'false')) {
          $scope.options.showSetLocationBtn = false;
        }
        if ($attrs.hasOwnProperty('required') && ((ref2 = $attrs.required) !== false && ref2 !== 'false')) {
          $scope.options.required = true;
        }
        mapMarker = null;
        searchBox = null;
        autocompleteBox = null;
        zoom = toInt($scope.zoom, 15);
        gm = new WorldMapClass($element[0].children[1], {
          config: {
            zoom: zoom
          }
        });
        ngModelCtrl.$render = function() {
          var modelValue;
          modelValue = ngModelCtrl.$modelValue || {};
          if (!angular.isNumber(modelValue.lng) || !angular.isNumber(modelValue.lat)) {
            $scope.proxy = getDefaultProxy();
          } else {
            $scope.proxy = {
              lat: modelValue.lat,
              lng: modelValue.lng,
              text: modelValue.text || '',
              exists: true
            };
          }
        };
        $scope.$watchCollection('proxy', function(proxy, oldValue) {
          if ($attrs.mode !== 'view') {
            if (proxy.exists) {
              ngModelCtrl.$setViewValue({
                lat: proxy.lat,
                lng: proxy.lng,
                text: proxy.text || ''
              });
            } else {
              ngModelCtrl.$setViewValue(null);
            }
          }
          if (mapMarker) {
            mapMarker.setPosition(gm.getLatLng(getLocationForMapCenter()));
            gm.fitBounds(true);
          }
        });

        /*
                работа с гуглекартами
         */
        updateProxyFromEvent = function(event) {
          if ($attrs.mode === 'view') {
            return;
          }
          $scope.$apply(function() {
            $scope.proxy.lat = event.latLng.lat();
            $scope.proxy.lng = event.latLng.lng();
            $scope.proxy.exists = true;
            return gm.geocoder.geocode({
              'location': $scope.proxy
            }, function(results, status) {
              if (status !== google.maps.GeocoderStatus.OK || (!results || !results[0])) {
                return;
              }
              $scope.$apply(function() {
                $scope.proxy = {
                  lat: event.latLng.lat(),
                  lng: event.latLng.lng(),
                  text: results[0].formatted_address,
                  exists: true
                };
              });
            });
          });
        };
        getLocationForMapCenter = function() {
          if ($scope.proxy.exists) {
            return {
              lat: $scope.proxy.lat,
              lng: $scope.proxy.lng
            };
          }
          return {
            lat: 50.075538,
            lng: 14.437800
          };
        };
        forceRedrawGMap = function() {
          return setTimeout(function() {
            google.maps.event.trigger(gm.map, "resize");
            return gm.map.setCenter(getLocationForMapCenter());
          }, 100);
        };
        initMap = function() {
          return gm.initMap(function() {
            var markerOptions;
            $element.find('input').on('keypress', function(event) {
              if (event.which === 13) {
                event.preventDefault();
              }
            });
            if (!$scope.options.allowSearchbox) {
              $scope.options.isMapVisible = true;
            }
            markerOptions = {
              coordinate: getLocationForMapCenter()
            };
            if ($scope.markerColor) {
              markerOptions.color = $scope.markerColor;
            }
            if ($scope.markerIcon) {
              markerOptions.icon = $scope.markerIcon;
            }
            mapMarker = gm.setMarker(markerOptions, {
              draggable: true
            });
            gm.map.set('center', mapMarker.position);
            google.maps.event.addListener(mapMarker, 'dragend', updateProxyFromEvent);
            google.maps.event.addListener(gm.map, 'click', updateProxyFromEvent);
            if ($scope.options.allowSearchbox && !$scope.options.autocompleteTypes.length) {
              searchBox = new google.maps.places.SearchBox($element[0].getElementsByTagName('input')[0]);
              gm.map.addListener('bounds_changed', function() {
                return searchBox.setBounds(gm.map.getBounds());
              });
              searchBox.addListener('places_changed', function() {
                var bounds, places;
                places = searchBox.getPlaces();
                if (!places.length) {
                  return;
                }
                $scope.$apply(function() {
                  $scope.proxy = {
                    lat: places[0].geometry.location.lat(),
                    lng: places[0].geometry.location.lng(),
                    text: places[0].formatted_address,
                    exists: true
                  };
                });
                bounds = new google.maps.LatLngBounds();
                if (places[0].geometry.viewport) {
                  bounds.union(places[0].geometry.viewport);
                } else {
                  bounds.extend(places[0].geometry.location);
                }
                gm.map.fitBounds(bounds);
                if (gm.map.getZoom() > 16) {
                  gm.map.setZoom(16);
                }
                if (gm.map.getZoom() < zoom) {
                  gm.map.setZoom(zoom);
                }
              });
            } else if ($scope.options.allowSearchbox && $scope.options.autocompleteTypes.length) {
              autocompleteBox = new google.maps.places.Autocomplete($element[0].getElementsByTagName('input')[0], {
                types: $scope.options.autocompleteTypes
              });
              autocompleteBox.addListener('place_changed', function() {
                var place;
                place = autocompleteBox.getPlace();
                if (!place) {
                  return;
                }
                $scope.$apply(function() {
                  if (!place) {
                    return;
                  }
                  $scope.proxy = {
                    lat: place.geometry.location.lat(),
                    lng: place.geometry.location.lng(),
                    text: place.formatted_address,
                    exists: true
                  };
                });
              });
            }
            return forceRedrawGMap();
          });
        };
        $scope.toggleShow = function() {
          if (!$scope.options.showSetLocationBtn) {
            return;
          }
          $scope.options.isMapVisible = !$scope.options.isMapVisible;
          if ($scope.options.isMapVisible) {
            forceRedrawGMap();
          }
        };
        $scope.reset = function() {
          $scope.proxy = getDefaultProxy();
        };
        return $timeout(initMap, 0);
      }],
      template: ('/client/cms_app/_inputDirectives/uicMapCoordinatePicker/uicMapCoordinatePicker.html', '<div class="gmap-toolbar" ng-switch="options.allowSearchbox"><div ng-switch-when="true" ng-class="{\'input-group\': (proxy.exists &amp;&amp; options.showSetLocationBtn) || !options.required}"><input class="form-control" type="text" placeholder="{{placeholder || (\'Type to search location\' | translate)}}" ng-model="proxy.text" ng-disabled="ngDisabled"/><div class="input-group-btn"><button class="btn btn-info" ng-click="toggleShow()" ng-disabled="ngDisabled" ng-class="{\'active\': options.isMapVisible}" ng-show="proxy.exists &amp;&amp; options.showSetLocationBtn"><div class="fas fa-map-marker-alt"></div><span class="btn-set-location-text" translate="">Set location</span></button><button class="btn btn-danger" ng-if="!options.required" ng-click="reset()" ng-disabled="ngDisabled || !proxy.exists" title="{{\'Reset\' | translate}}"><span class="fas fa-times nopadding"></span></button></div></div><div ng-switch-when="false"><label><span>{{placeholder || (\'Select location\' | translate)}}</span><span> </span><a class="text-danger" ng-click="reset()" ng-if="proxy.exists &amp;&amp; !proxy.required &amp;&amp; !ngDisabled"><span class="fas fa-times"></span><span translate="">reset</span></a></label></div></div><div class="gmap" ng-class="{visible: options.isMapVisible}"></div>' + '')
    };
  });

}).call(this);
;
(function() {
  angular.module('ui.cms').directive('uicSearchInput', function() {
    return {

      /**
      *   @ngdoc directive
      *   @name ui.cms.directive:uicSearchInput
      *   @description
      *       виджет для ввода строки под поиск
      *   @restrict E
      *   @param {string} ngModel (ссылка) текст строки поиска
      *   @param {string=} [placeholder='Type to search'] текст, который отображается на месте пустого ngModel
      *   @param {string=} inputClass (значение) класс для внутреннего виджета (напр.: 'input-sm')
      *   @param {boolean=} ngDisabled (ссылка) флаг доступности тега
      *   @example
      *       <pre>
      *           $scope.searchTerm = 'hello'
      *       </pre>
      *       <pre>
      *           //- pug
      *           uic-search-input(ng-model="searchTerm")
      *       </pre>
      *       <example module="DocExampleApp">
      *           <file name="script.js">
      *               mkDocCtrl('uicSearchInputCtrl', {
      *                   searchTerm: 'hello'
      *               });
      *           </file>
      *           <file name="index.html">
      *               <div ng-controller="uicSearchInputCtrl">
      *                   <div><b>Строка поиска: </b>{{searchTerm}}</div>
      *                   <uic-search-input ng-model="searchTerm"></uic-search-input>
      *               </div>
      *           </file>
      *       </example>
       */
      restrict: 'E',
      scope: {
        ngModel: '=',
        placeholder: '@?',
        inputClass: '@?',
        ngDisabled: '=?'
      },
      controller: ["$scope", function($scope) {
        return $scope.resetSearch = function() {
          return $scope.ngModel = '';
        };
      }],
      template: ('/client/cms_app/_inputDirectives/uicSearchInput/uicSearchInput.html', '<div class="input-group"><input class="form-control {{::inputClass}}" placeholder="{{ placeholder || (\'Type to search\' | translate) }}" ng-model="ngModel" ng-model-options="{ debounce: 350 }" type="search" ng-type="search" ng-disabled="ngDisabled"/><div class="input-group-btn"><button class="btn btn-default" ng-click="resetSearch()" ng-class="{\'btn-danger\': ngModel, \'btn-sm\': inputClass == \'input-sm\'}" title="{{\'Reset search\' | translate}}" ng-disabled="!ngModel"><span class="fas nopadding" ng-class="{\'fa-search\': !ngModel, \'fa-times\': ngModel}"></span></button></div></div>' + '')
    };
  });

}).call(this);
;
(function() {
  var scrollToElement;

  scrollToElement = function(scrollable_area, element, safe_height) {
    var top;
    if (!element) {
      return;
    }
    top = element.offsetTop - element.offsetHeight * 2;
    if (top > safe_height - element.offsetHeight * 2) {
      scrollable_area.scrollTop = top;
    }
  };

  angular.module('ui.cms').controller('UicSelectItemsAsyncLoadCtrl', ["$scope", "$element", "ngModelCtrl", function($scope, $element, ngModelCtrl) {

    /*
        асинхронная загрузка элементов
     */
    var currentLoadItemsFootprint, currentPage, fixProxy, init, items, loadItems, scrollableContainer, totalPages, watchScroll;
    currentPage = 1;
    totalPages = 1;
    items = [];
    scrollableContainer = null;
    $scope.options.searchInputOptions = {
      debounce: 400
    };
    $scope.getItemById = function(id) {
      return items.findByProperty('id', id);
    };
    $scope.forceRefresh = function() {
      loadItems(1, $scope.options.searchTerm);
    };
    watchScroll = function() {
      var checkAndLoad;
      checkAndLoad = function() {
        var isDown;
        if (!scrollableContainer.clientHeight) {
          return;
        }
        isDown = scrollableContainer.scrollHeight - scrollableContainer.scrollTop - 60 <= scrollableContainer.clientHeight;
        if (isDown && currentPage + 1 <= totalPages) {
          currentPage += 1;
          loadItems(currentPage, $scope.options.searchTerm);
        }
      };
      checkAndLoad();
      scrollableContainer.addEventListener('scroll', checkAndLoad);
    };
    currentLoadItemsFootprint = null;
    loadItems = function(page, searchTerm, isFirstCall) {
      if (currentLoadItemsFootprint === (page + "-" + searchTerm)) {
        return;
      }
      currentLoadItemsFootprint = page + "-" + searchTerm;
      $scope.options.loading = true;
      $scope.itemsLoadCallback({
        page: page,
        searchTerm: searchTerm,
        ngModel: $scope.options.proxy
      }).then(function(data) {
        var i, item, len, ref;
        if (searchTerm !== $scope.options.searchTerm) {
          return;
        }
        console.log(data);
        $scope.options.loading = false;
        currentLoadItemsFootprint = null;
        currentPage = data.page;
        totalPages = data.totalPages;
        ref = data.items;
        for (i = 0, len = ref.length; i < len; i++) {
          item = ref[i];
          if (!items.findByProperty('id', item.id)) {
            items.push(item);
          }
        }
        if (isFirstCall && fixProxy) {
          $scope.options.proxy = fixProxy($scope.options.proxy);
        }
        $scope.filteredItems = items;
        if ($scope.options.proxy && !$scope.$selected) {
          $scope.$selected = items.findByProperty('id', $scope.options.proxy);
        }
        if (!scrollableContainer && $element) {
          scrollableContainer = $element[0].getElementsByClassName('scrollable')[0];
          watchScroll();
        }
      }, function(data) {
        $scope.options.loading = false;
        currentLoadItemsFootprint = null;
        $scope.filteredItems = [];
      });
    };
    $scope.$watch('options.searchTerm', function(searchTerm, oldValue) {
      if (searchTerm === oldValue) {
        return;
      }
      currentPage = 1;
      totalPages = 1;
      items = [];
      $scope.filteredItems = [];
      loadItems(1, searchTerm);
    });
    if (ngModelCtrl) {
      fixProxy = function(proxy) {
        if (items.length && !$scope.options.searchTerm && !items.findByProperty('id', proxy)) {
          ngModelCtrl.$setViewValue(null);
          $scope.$selected = null;
          return null;
        }
        return proxy;
      };
      init = false;
      ngModelCtrl.$render = function() {
        $scope.options.proxy = fixProxy(ngModelCtrl.$modelValue);
        if (items.length) {
          $scope.$selected = items.findByProperty('id', $scope.options.proxy);
        }
        if (!init) {
          loadItems(1, '', true);
          init = true;
        }
      };
      $scope.$watch('options.proxy', function(proxy, oldValue) {
        ngModelCtrl.$setViewValue(fixProxy(proxy));
      });
    } else {
      loadItems(1, '', true);
    }
  }]).controller('UicSelectItemsSyncCtrl', ["$scope", "$element", "$filter", "ngModelCtrl", function($scope, $element, $filter, ngModelCtrl) {
    var fixProxy, ngFilter;
    ngFilter = $filter('filter');
    $scope.options.searchInputOptions = {
      debounce: 80
    };
    $scope.getItemById = function(id) {
      return $scope.items.findByProperty('id', id);
    };
    $scope.forceRefresh = angular.noop;
    $scope.$watch('options.searchTerm', function(searchTerm, oldValue) {
      if (!searchTerm) {
        $scope.filteredItems = $scope.items;
      } else {
        $scope.filteredItems = ngFilter($scope.items, searchTerm);
      }
    });
    if (ngModelCtrl) {
      fixProxy = function(proxy) {
        if (($scope.items || []).length && !$scope.items.findByProperty('id', proxy)) {
          ngModelCtrl.$setViewValue(null);
          return null;
        }
        return proxy;
      };
      $scope.$watchCollection('items', function(items) {
        $scope.$selected = (items || []).findByProperty('id', $scope.options.proxy) || null;
        $scope.filteredItems = ngFilter(items, $scope.searchTerm);
        $scope.options.proxy = fixProxy($scope.options.proxy);
      });
      ngModelCtrl.$render = function() {
        $scope.options.proxy = fixProxy(ngModelCtrl.$modelValue);
        $scope.$selected = ($scope.items || []).findByProperty('id', $scope.options.proxy);
      };
      $scope.$watch('options.proxy', function(proxy, oldValue) {
        ngModelCtrl.$setViewValue(proxy);
      });
    }
  }]).controller('UicSelectModalCtrl', ["$scope", "$timeout", "$document", "$controller", "$uibModalInstance", "H", "ngModel", "items", "options", function($scope, $timeout, $document, $controller, $uibModalInstance, H, ngModel, items, options) {
    var $element;
    $scope.options = options || {};
    $scope.options.searchTerm = '';
    $scope.options.proxy = ngModel;
    $scope.timestamp = new Date().getTime();
    if (options.placeholder) {
      $scope.options.maxContentHeight = H.info.viewport.getSize()['height'] - 220;
    } else {
      $scope.options.maxContentHeight = H.info.viewport.getSize()['height'] - 170;
    }
    $scope.options.maxContentHeight -= 30;
    $element = $document[0].getElementsByClassName('uic-select-modal');
    if ($uibModalInstance && $element) {
      $uibModalInstance.rendered.then(function() {
        var footerHeight, headingHeight, modalWindow;
        modalWindow = $element[0].getElementsByClassName('modal-content')[0];
        headingHeight = (modalWindow.getElementsByClassName('modal-header')[0] || {}).offsetHeight || 0;
        footerHeight = (modalWindow.getElementsByClassName('modal-footer')[0] || {}).offsetHeight || 0;
        $scope.options.maxContentHeight = H.info.viewport.getSize()['height'] - headingHeight - footerHeight - 145;
      });
    }
    if (angular.isFunction(items)) {
      $scope.itemsLoadCallback = items;
      $controller('UicSelectItemsAsyncLoadCtrl', {
        $scope: $scope,
        $element: $element,
        ngModelCtrl: null
      });
    } else {
      $scope.items = items || [];
      $scope.filteredItems = $scope.items;
      $controller('UicSelectItemsSyncCtrl', {
        $scope: $scope,
        $element: $element,
        ngModelCtrl: null
      });
    }
    $scope.cancel = function() {
      $uibModalInstance.dismiss('cancel');
    };
    $scope.reset = function() {
      $uibModalInstance.close(null);
    };
    $scope.ok = function() {
      $uibModalInstance.close($scope.getItemById($scope.options.proxy));
    };
    $scope.setSelected = function(item) {
      $scope.options.proxy = item.id;
    };
    $scope.resetSearchTerm = function() {
      $scope.options.searchTerm = '';
    };
    return $timeout(function() {
      return scrollToElement($document[0].getElementById("uic-select-scrollable-" + $scope.timestamp), $document[0].getElementById("uic-select-option-" + $scope.timestamp + "-" + $scope.options.proxy), $scope.options.maxContentHeight / 1.5);
    }, 0);
  }]).service('uicSelectDefaultModal', ["$uibModal", function($uibModal) {
    this.open = function(ngModel, items, options) {
      var modal;
      return modal = $uibModal.open({
        animation: true,
        template: ('/client/cms_app/_inputDirectives/uicSelect/modal.html', '<div class="modal-header" ng-if="options.placeholder"><button class="close" ng-click="cancel()"><span>&times;</span></button><h4 class="modal-title">{{:: options.placeholder}}</h4></div><div class="modal-body uic-select-modal-body"><div class="form-group nomargin" ng-show="items.length &gt; 5 || itemsLoadCallback"><div class="input-group"><input class="form-control input-sm" ng-model="options.searchTerm" placeholder="{{\'Search by word\' | translate}}" ng-model-options="options.searchInputOptions"/><div class="input-group-addon" ng-click="resetSearchTerm()" title="{{!!options.searchTerm ?  (\'Reset filter\' | translate) : \'\' }}"><span class="fas nopadding" ng-class="{\'fa-search\': !options.searchTerm, \'fa-times text-danger\': options.searchTerm}"></span></div></div><hr/></div><div class="uic-select-modal-body-items scrollable" ng-hide="!filteredItems.length &amp;&amp; !options.loading" style="max-height: {{::options.maxContentHeight}}px;" id="uic-select-scrollable-{{timestamp}}"><table class="w-100"><tbody><tr ng-repeat="$item in filteredItems" ng-click="setSelected($item)" ng-class="{\'active bg-primary\': options.proxy==$item.id}" id="uic-select-option-{{timestamp}}-{{$item.id}}"><td class="text-left" uic-bind-html="options.choicesTemplate"></td><td class="w-2em text-center"><div class="fas fa-check"></div></td></tr><tr ng-if="itemsLoadCallback &amp;&amp; options.loading"><td class="text-center" colspan="2"><b class="uic-select-loading-dots" translate="">Loading data</b></td></tr></tbody></table></div><div ng-show="!filteredItems.length &amp;&amp; !options.loading"><uic-no-items search-term="options.searchTerm" reset-filter="resetSearchTerm()"></uic-no-items></div></div><div class="modal-footer"><button class="btn btn-success" ng-click="ok()" translate="" ng-disabled="!filteredItems.length || (!options.proxy &amp;&amp; !options.allowNull)">Select</button><button class="btn btn-danger" ng-click="reset()" translate="" ng-disabled="!filteredItems.length" ng-show="options.allowNull">Clean</button><button class="btn btn-default" ng-click="cancel()" translate="">Cancel</button></div>' + ''),
        controller: 'UicSelectModalCtrl',
        size: 'md',
        windowClass: 'uic-select-modal',
        resolve: {
          ngModel: function() {
            return ngModel;
          },
          items: function() {
            return items;
          },
          options: function() {
            return options || {};
          }
        }
      }).result;
    };
  }]).directive('uicSelect', ["$timeout", "$document", "$injector", "$controller", "$window", "$cms", "H", function($timeout, $document, $injector, $controller, $window, $cms, H) {
    var uicSelectModal;
    if ($injector.has('uicSelectModal')) {
      uicSelectModal = $injector.get('uicSelectModal');
    } else {
      uicSelectModal = $injector.get('uicSelectDefaultModal');
    }
    return {

      /**
      *   @ngdoc directive
      *   @name ui.cms.directive:uicSelect
      *   @description
      *       директива-аналог ui-select от ангулара. но она избавлена от необходимости тащить с собой левые css
      *       и также не тащит с собой реализацию дропдауна(которая уже есть у бутстрапа). Особенности: варианты могут быть заданы
      *       только через items атрибут.  При этом у каждого элемента в массиве
      *       должен быть уникальный идентификатор id.
      *       <br/>
      *       <b>Громадный плюс этой директивы: на мобилках она превращается в кнопку,
      *       по нажатию на которую открывается модальное окно с выбором вариантов.</b>
      *        Не отображает инпут с поиском по словам, если элементов меньше 6. См примеры ниже
      *       <br/>
      *       Внутри директивы можно указывать верстку для обычных элементов из списка, и для
      *       елеменnа выбранного (подробнее см. примеры):
      *       <br/>
      *       <pre>
      *           //- верстка элементов в дропдауне ($item - текущий элемент внутри списка)
      *           item {{$item.id}}
      *       </pre>
      *       <pre>
      *           //- верстка выбранного элемента ($selected - выбранный элемент)
      *           selected {{$selected.id}}
      *       </pre>
      *   @restrict E
      *   @param {string|number} ngModel (ссылка) модель (id, который берется у выбранного элемента)
      *   @param {Array} items (ссылка) варианты выбора
      *   @param {string=} placeholder текст, который отображается на месте пустого ngModel.
      *       также для мобилок этот текст становится заголовком окна.
      *   @param {string=} inputClass css-класс для внутренней верстки. Можно, например передать класс 'input-sm'.
      *   @param {string=} [iconClass] (значение) иконка для input-a  (всегда отображается слева)
      *   @param {string=} ngRequired (ссылка/значение) установите этот атрибут в директиве для того, чтоб значение ng-model==null приводило к ошибкам валидации формы
      *   @param {boolean=} ngDisabled (ссылка) флаг доступности тега
      *   @example
      *       <pre>
      *           $scope.myItems = [
      *               {id: 'hello', text: 'Text for hello'},
      *               {id: 'world', text: 'Another text for word'},
      *               {id: '!', text: 'text !!!'},
      *               {id: 'coffee', text: 'coffeescript'},
      *               {id: 'js', text: 'js'},
      *               {id: 'css', text: 'CSS'},
      *               {id: 'pug', text: 'Pug'}
      *           ]
      *           $scope.myModel = 'hello'
      *       </pre>
      *       <pre>
      *           //- pug
      *           // поресайзите окно до размера дисплея мобилок. нажмите на дропдаун,
      *           // при этом отрисуется модальное окно.
      *           uic-select(ng-model="myModel", items="myItems", ng-required='true')
      *           // отображаем в дропдауне значения свойств 'text'
      *           uic-select(ng-model="myModel", items="myItems", ng-required='true')
      *               item {{$item.text}}
      *               selected {{$selected.text}}
      *           // разрешаем сброс в null нашу ngModel, опуская атрибут ng-required
      *           uic-select(ng-model="myModel", items="myItems")
      *               item {{$item.text}}
      *               selected {{$selected.text}}
      *           // делаем кастомную верстку для элементов в дропдауне
      *           uic-select(ng-model="myModel", items="myItems", ng-required='true')
      *               item
      *                   b {{$item.id}}
      *                   | : {{$item.text}}
      *               selected
      *                   span.fas.fa-check
      *                   |  {{$selected.text}}
      *       </pre>
      *       <example module="DocExampleApp">
      *           <file name="script.js">
      *               mkDocCtrl('uicSelectCtrl', {
      *                   myItems: [
      *                       {id: "hello", text: "Text for hello"},
      *                       {id: "world", text: "Another text for word"},
      *                       {id: "!", text: "text !!!"},
      *                       {id: "coffee", text: "coffeescript"},
      *                       {id: "js", text: "js"},
      *                       {id: "css", text: "CSS"},
      *                       {id: "pug", text: "Pug"}
      *                   ],
      *                   myModel: 'hello',
      *                   mySelectedTemplate: "<span class='fas fa-check'></span> {{$selected.text}}",
      *                   myChoicesTemplate: "<b>{{$item.id}}: {{$item.text}}</b>"
      *               });
      *           </file>
      *           <file name="index.html">
      *               <div ng-controller="uicSelectCtrl">
      *                   <div class='row'>
      *                       <div class='col-xs-100'>
      *                           <p>без указания верстки элементов (по умолчанию будет выводиться содержание свойства 'id')</p>
      *                           <uic-select ng-model="myModel" items="myItems" ng-required='true'></uic-select>
      *                       </div>
      *                       <div class='col-xs-100'>
      *                           <p>ображаем в дропдауне значения свойств 'text'</p>
      *                           <uic-select ng-model="myModel" items="myItems" ng-required='true'>
      *                               <item>{{$item.text}}</item>
      *                               <selected>{{$selected.text}}</selected>
      *                           </uic-select>
      *                       </div>
      *                   </div>
      *                   <div class='row'>
      *                       <div class='col-xs-100'>
      *                           <p>разрешаем сброс в null нашу ngModel, опуская атрибут ng-required</p>
      *                           <uic-select ng-model="myModel" items="myItems">
      *                               <item>{{$item.text}}</item>
      *                               <selected>{{$selected.text}}</selected>
      *                           </uic-select>
      *                       </div>
      *                       <div class='col-xs-100'>
      *                           <p>делаем кастомную верстку для элементов в дропдауне</p>
      *                           <uic-select ng-model="myModel" items="myItems" ng-required='true'>
      *                               <item><b>{{$item.id}}</b>: {{$item.text}}</item>
      *                               <selected><span class='fas fa-check'></span> {{$selected.text}}</selected>
      *                           </uic-select>
      *                       </div>
      *                   </div>
      *               </div>
      *           </file>
      *       </example>
       */
      restrict: 'E',
      require: 'ngModel',
      scope: {
        items: '=?',
        placeholder: '@?',
        iconClass: '@?',
        inputClass: '@?',
        ngDisabled: '=?',
        ngRequired: '=?',
        allowNull: '@?',
        selectedTemplate: '@?',
        choicesTemplate: '@?'
      },
      transclude: true,
      compile: function($element, $attrs, transclude) {
        return {
          pre: function($scope) {
            return transclude($scope, function(clone) {
              var el, i, len, nodeName, ref;
              $scope.$transcluded = {};
              ref = clone || [];
              for (i = 0, len = ref.length; i < len; i++) {
                el = ref[i];
                if (!(el)) {
                  continue;
                }
                nodeName = el.nodeName.toLowerCase();
                if (nodeName !== 'item' && nodeName !== 'selected') {
                  continue;
                }
                $scope.$transcluded[nodeName] = angular.element(el).html();
              }
            });
          }
        };
      },
      controller: ["$scope", "$element", "$attrs", "$state", function($scope, $element, $attrs, $state) {
        var $$window, isModalActive, ngModelCtrl, onResizeWindow, onScroll, scrollPos, updateMenuPositionFlag, viewportSize;
        ngModelCtrl = $element.controller('ngModel');
        viewportSize = {
          width: 0,
          height: 0
        };
        scrollPos = {
          top: 0,
          left: 0
        };
        isModalActive = false;
        $$window = angular.element($window);
        $scope.$cms = $cms;
        $scope.timestamp = new Date().getTime();
        $scope.options = {
          dropdownOpen: false,
          menuPositionTop: false,
          dropdownMaxHeight: 0,
          allowNull: true,
          searchTerm: ''
        };
        if ($attrs.hasOwnProperty('itemsLoadCallback')) {
          $scope.itemsLoadCallback = $scope.$parent.$eval($attrs.itemsLoadCallback.split('(')[0]) || $scope.$parent.$parent.$eval($attrs.itemsLoadCallback.split('(')[0]);
          $scope.filteredItems = null;
          if (!angular.isFunction($scope.itemsLoadCallback)) {
            $scope.itemsLoadCallback = null;
          }
        }
        if ($attrs.hasOwnProperty('items')) {
          $scope.filteredItems = $scope.items;
        }
        if ($scope.itemsLoadCallback && $attrs.hasOwnProperty('items')) {
          throw "uicSelect: You can't set 'items' and 'items-load-callback' at the same time!";
        }
        if ($attrs.hasOwnProperty('ngRequired')) {
          $scope.$watch('ngRequired', function(ngRequired, oldValue) {
            $scope.options.allowNull = !(ngRequired === true || ngRequired === 'true');
          });
        }
        if ($attrs.hasOwnProperty('required')) {
          console.warn("uicSelect: Dont use 'required' attribute! Use ngRequired instead");
          $scope.options.allowNull = false;
        }
        if ($attrs.hasOwnProperty('allowNull')) {
          console.warn("uicSelect: Dont use 'allowNull' attribute! Use ngRequired instead");
          $scope.$watch('allowNull', function(allowNull, oldValue) {
            if (allowNull === 'true') {
              $scope.options.allowNull = true;
            }
            if (allowNull === 'false') {
              $scope.options.allowNull = false;
            }
          });
        }
        $scope.resetSearchTerm = function() {
          $scope.options.searchTerm = '';
        };
        $scope.setSelected = function(item) {
          ngModelCtrl.$setViewValue(item.id);
          $scope.options.proxy = item.id;
          $scope.$selected = item;
          $scope.options.dropdownOpen = false;
        };
        $scope.resetSelected = function() {
          ngModelCtrl.$setViewValue(null);
          $scope.options.proxy = null;
          $scope.$selected = null;
          $scope.options.dropdownOpen = false;
        };
        $scope.onDropdownToggle = function() {
          var ref, search_input;
          $scope.options.showModalOnMobile = !$state.current.$ignoreUicSelectModal;
          if (((ref = $cms.screenSize) !== 'xs' && ref !== 'sm') || !$scope.options.showModalOnMobile) {
            scrollToElement($document[0].getElementById("uic-select-scrollable-" + $scope.timestamp), $document[0].getElementById("uic-select-option-" + $scope.timestamp + "-" + $scope.options.proxy), $scope.options.dropdownMaxHeight);
            search_input = $element.find('input');
            if (search_input[0]) {
              search_input[0].focus();
            }
            return;
          }
          $scope.options.dropdownOpen = false;
          if (isModalActive) {
            return;
          }
          isModalActive = true;
          return uicSelectModal.open(angular.copy($scope.options.proxy), $scope.itemsLoadCallback || $scope.items, {
            placeholder: $scope.placeholder,
            selectedTemplate: $scope.selectedTemplate,
            choicesTemplate: $scope.choicesTemplate,
            allowNull: $scope.options.allowNull
          }).then(function(selected) {
            $scope.$selected = selected;
            if (selected) {
              ngModelCtrl.$setViewValue(selected.id);
              $scope.options.proxy = selected.id;
              if (!$scope.getItemById('id', selected.id)) {
                $scope.forceRefresh();
              }
            } else {
              ngModelCtrl.$setViewValue(null);
              $scope.options.proxy = null;
            }
            return isModalActive = false;
          }, function() {
            return isModalActive = false;
          });
        };

        /*
            слежение за расположением виджета, для того чтоб можно было правильно
            определять положение открываемого меню
         */
        updateMenuPositionFlag = function() {
          var element_pos, top;
          element_pos = H.info.viewport.getElementPosition($element, scrollPos);
          top = (element_pos.top + element_pos.bottom) / 2;
          $scope.options.menuPositionTop = (viewportSize.height / 2) <= top;
          $scope.options.dropdownMaxHeight = viewportSize.height / 3;
        };
        onResizeWindow = function() {
          viewportSize = H.info.viewport.getSize();
          return $scope.$apply(function() {
            return updateMenuPositionFlag();
          });
        };
        onScroll = function() {
          scrollPos = H.info.viewport.getScrollPosition();
          return $scope.$apply(function() {
            return updateMenuPositionFlag();
          });
        };
        $timeout(function() {
          $scope.choicesTemplate = $scope.choicesTemplate || $scope.$transcluded.item || "{{ $item.id }}";
          $scope.selectedTemplate = $scope.selectedTemplate || $scope.$transcluded.selected || "{{ $selected.id }}";
          viewportSize = H.info.viewport.getSize();
          scrollPos = H.info.viewport.getScrollPosition();
          updateMenuPositionFlag();
          $$window.on('resize', onResizeWindow);
          $$window.on('scroll', onScroll);
          return $scope.$on('$destroy', function() {
            $$window.off('resize', onResizeWindow);
            return $$window.off('scroll', onScroll);
          });
        }, 1);
        if ($scope.itemsLoadCallback) {
          return $controller('UicSelectItemsAsyncLoadCtrl', {
            $scope: $scope,
            $element: $element,
            ngModelCtrl: ngModelCtrl
          });
        } else {
          return $controller('UicSelectItemsSyncCtrl', {
            $scope: $scope,
            $element: $element,
            ngModelCtrl: ngModelCtrl
          });
        }
      }],
      template: ('/client/cms_app/_inputDirectives/uicSelect/uicSelect.html', '<div uib-dropdown="" is-open="options.dropdownOpen" auto-close="outsideClick" ng-class="{dropup: options.menuPositionTop, \'allow-null\': options.allowNull}" on-toggle="onDropdownToggle(open)"><button class="form-control {{::inputClass}}" uib-dropdown-toggle="" ng-disabled="ngDisabled"><table class="table-transparent"><tbody><tr><td class="text-left text-icon" ng-if="iconClass"><div class="fas {{iconClass}}"></div></td><td class="text-left text-selected" ng-class="{\'text-placeholder\': options.proxy == null || options.proxy == undefined || options.proxy == \'\'}"><span>{{placeholder || \'\'}}</span><span class="uic-select-selected-template" uic-bind-html="selectedTemplate"></span></td><td class="text-right" ng-switch="options.allowNull"><span ng-switch-when="false"> </span><button class="btn btn-xs btn-danger" ng-switch-when="true" ng-click="resetSelected(); $event.stopPropagation()" title="{{\'Reset value\' | translate}}" ng-disabled="ngDisabled"><div class="fas fa-times"></div></button></td><td class="text-right" style="width:5px;"><span class="caret"></span></td></tr></tbody></table></button><div class="dropdown-menu" uib-dropdown-menu="" ng-class="{\'dropdown-menu-sm\': inputClass==\'input-sm\', \'hidden-xs hidden-sm\': options.showModalOnMobile}"><div class="dropdown-menu-search-input" ng-show="items.length &gt; 6 || itemsLoadCallback"><div class="input-group"><input class="form-control input-sm" ng-model="options.searchTerm" placeholder="{{\'Search by word\' | translate}}" ng-model-options="options.searchInputOptions"/><div class="input-group-addon" ng-click="resetSearchTerm()" title="{{!!options.searchTerm ?  (\'Reset filter\' | translate) : \'\' }}"><span class="fas nopadding" ng-class="{\'fa-search\': !options.searchTerm, \'fa-times text-danger\': options.searchTerm}"></span></div></div></div><div class="scrollable" ng-if="(options.dropdownOpen &amp;&amp; filteredItems.length) || itemsLoadCallback" style="max-height: {{options.dropdownMaxHeight}}px;" id="uic-select-scrollable-{{timestamp}}"><li ng-repeat="$item in filteredItems" ng-class="{active: options.proxy == $item.id}"><a ng-click="setSelected($item)" uic-bind-html="choicesTemplate" id="uic-select-option-{{timestamp}}-{{$item.id}}"></a></li><li class="text-center" ng-if="itemsLoadCallback &amp;&amp; options.loading"><b class="uic-select-loading-dots" translate="">Loading data</b></li></div><div ng-show="options.dropdownOpen &amp;&amp; !filteredItems.length &amp;&amp; !options.loading"><uic-no-items class="sm" search-term="options.searchTerm" reset-filter="resetSearchTerm()"></uic-no-items></div></div></div>' + '')
    };
  }]);

}).call(this);
;
(function() {
  angular.module('ui.cms').directive('uicSiteReviewForm', function() {
    return {

      /**
      *   @ngdoc directive
      *   @name ui.cms.directive:uicSiteReviewForm
      *   @description
      *       Форма отзыва о сайте/работе. Создает CmsSiteReview с полями title, body, description.
      *       <b>Форма поддерживает переопределение через transclude (см пример), при этом все свои переменные вы должны хранить в $form и поле title должно быть обязательным</b>
      *   @param {boolean=} [showPlaceholder=false] (значение) по умолчанию отобразится верстка с label рядом с input, если указать true, то будет отрендерены input-ы с placeholder-ами вместо label (см примеры)
      *   @restrict E
      *   @example
      *       <pre>
      *           //- pug
      *           // Внимание! Форма из документации никуда не отправится, будет только ошибка
      *           h3 Форма с label-ами
      *           uic-site-review-form
      *           h3 Форма с placeholder-ами
      *           uic-site-review-form(show-placeholder='true')
      *           h3 Форма с переопределенной версткой формы
      *           uic-site-review-form
      *               site-review-form
      *                   .form-group
      *                       label(translate) Email for custom form
      *                       input.form-control(ng-model="$formData.email", required, type='email')
      *                   .form-group
      *                       label(translate) My custom int value
      *                       input.form-control(ng-model="$formData.myInt", required, type='number', step='1')
      *       </pre>
      *       <example module="DocExampleApp">
      *           <file name="script.js">
      *               mkDocCtrl('uicSiteReviewFormCtrl', {
      *
      *               });
      *           </file>
      *           <file name="index.html">
      *               <div ng-controller="uicSiteReviewFormCtrl" class='row'>
      *                   <div class='col-md-50'>
      *                       <h3>Форма с label-ами</h3>
      *                       <uic-site-review-form></uic-site-review-form>
      *                   </div>
      *                   <div class='col-md-50'>
      *                       <h3>Форма с placeholder-ами</h3>
      *                       <uic-site-review-form show-placeholder='true'></uic-site-review-form>
      *                   </div>
      *                   <div class='clearfix'></div>
      *                   <hr/>
      *                   <div class='col-md-50'>
      *                       <h3>Форма с переопределенной версткой формы</h3>
      *                       <uic-site-review-form>
      *                           <site-review-form>
      *                              <div class="form-group">
      *                                <label translate="translate">Email for custom form</label>
      *                                <input class="form-control" ng-model="$formData.email" required="required" type="email"/>
      *                              </div>
      *                              <div class="form-group">
      *                                <label translate="translate">My custom int value</label>
      *                                <input class="form-control" ng-model="$formData.myInt" required="required" type="number" step="1"/>
      *                              </div>
      *                           </site-review-form>
      *                       </uic-site-review-form>
      *                   </div>
      *               </div>
      *           </file>
      *       </example>
       */
      restrict: 'E',
      transclude: {
        siteReviewForm: '?siteReviewForm'
      },
      scope: {
        showPlaceholder: '@?'
      },
      controller: ["$scope", "$langPicker", "$cms", "$injector", function($scope, $langPicker, $cms, $injector) {
        var resetForm;
        $scope.sended = false;
        $scope.error = false;
        $scope.loading = false;
        $scope.formData = {};
        $scope._showPlaceholder = false;
        if ($scope.showPlaceholder === 'true') {
          $scope._showPlaceholder = true;
        }
        resetForm = function() {
          var k, ref, v;
          ref = $scope.formData;
          for (k in ref) {
            v = ref[k];
            delete $scope.formData[k];
          }
          $scope.error = false;
          return $scope.loading = false;
        };
        $scope.resetError = function() {
          return $scope.error = false;
        };
        $scope.resetForm = function() {
          resetForm();
          return $scope.sended = false;
        };
        $scope.sendForm = function() {
          var CmsSiteReview, data, i, k, lang, langs, len, ref, v;
          CmsSiteReview = $injector.get('CmsSiteReview');
          $scope.sended = false;
          $scope.error = false;
          $scope.loading = true;
          langs = CMS_ALLOWED_LANGS;
          langs.push(CMS_DEFAULT_LANG);
          langs = langs.removeDuplicates();
          data = {};
          ref = $scope.formData;
          for (k in ref) {
            v = ref[k];
            if (k === 'description' || k === 'title' || k === 'body') {
              data[k] = {};
              for (i = 0, len = langs.length; i < len; i++) {
                lang = langs[i];
                data[k][lang] = v;
              }
            } else {
              data[k] = v;
            }
          }
          return CmsSiteReview.create(data, function(data) {
            $scope.sended = true;
            return resetForm();
          }, function(data) {
            $scope.error = data;
            return $scope.loading = false;
          });
        };
        return resetForm();
      }],
      template: ('/client/cms_app/_inputDirectives/uicSiteReviewForm/uicSiteReviewForm.html', '<fieldset class="feedback-form" ng-disabled="loading"><form name="siteReviewForm" ng-hide="sended || error"><div ng-switch="_showPlaceholder"><div uic-transclude="siteReviewForm" uic-transclude-bind="{$formData: $parent.formData, $form: $parent.siteReviewForm}" ng-switch-when="true"><div class="row"><div class="col-md-50"><div class="form-group"><input class="form-control" type="text" ng-model="formData.title" name="name" required="required" placeholder="{{\'Your name\' | translate}}"/></div></div><div class="col-md-50"><div class="form-group"><input class="form-control" type="email" ng-model="formData.description" name="email" required="required" placeholder="{{\'Your email\' | translate}}"/></div></div></div><div class="form-group"><textarea class="form-control" ng-model="formData.body" required="required" name="text" placeholder="{{ labelText || (\'Leave your review here\' | translate) }}"></textarea></div></div><div uic-transclude="siteReviewForm" uic-transclude-bind="{$formData: $parent.formData, $form: $parent.siteReviewForm}" ng-switch-when="false"><div class="row"><div class="col-md-50"><div class="form-group"><label><span translate="translate">Your name</span><span class="error" ng-show="siteReviewForm.name.$error.required">[required]</span></label><input class="form-control" type="text" ng-model="formData.title" name="name" required="required"/></div></div><div class="col-md-50"><div class="form-group"><label><span translate="translate">Your email</span><span class="error" ng-show="siteReviewForm.email.$error.required">[required]</span><span class="error" ng-show="siteReviewForm.email.$error.email">[invalid email]</span></label><input class="form-control" type="email" ng-model="formData.description" name="email" required="required"/></div></div></div><div class="form-group"><label><span>{{ labelText || (\'Leave your review here\' | translate)}}</span><span class="error" ng-show="siteReviewForm.text.$error.required">[required]</span></label><textarea class="form-control" ng-model="formData.body" required="required" name="text"></textarea></div></div></div><button class="btn btn-primary" type="submit" ng-disabled="siteReviewForm.$invalid || loading" ng-click="sendForm()"><span translate="translate" ng-show="loading">Please wait</span><span translate="translate" ng-show="!loading">Submit</span></button></form></fieldset><div class="feedback-form-sended text-center" ng-if="sended"><h3>{{(successText | l10n) || (\'Thank you! Review was send.\' | translate)}}</h3><button class="btn btn-default" ng-click="resetForm()" style="min-width:4em;">OK</button></div><div class="feedback-form-error text-center" ng-if="error"><h3 class="text-danger" translate="translate">Ooops!</h3><p class="text-danger" translate="translate">Cant submit form=(</p><button class="btn btn-default" translate="translate" ng-click="resetError()">Try again</button></div>' + '')
    };
  });

}).call(this);
;
(function() {
  var indexOf = [].indexOf || function(item) { for (var i = 0, l = this.length; i < l; i++) { if (i in this && this[i] === item) return i; } return -1; };

  angular.module('ui.cms').directive('uicTelephoneInput', function() {
    var countryCodeToLang, isValidProxy;
    countryCodeToLang = function(lang) {
      return {
        cs: 'cz',
        gb: 'en'
      }[lang] || lang;
    };
    isValidProxy = function(proxy) {
      var ch, i, len;
      if (!proxy.length) {
        return true;
      }
      proxy = proxy.replaceAll('-', '').replaceAll('+', '').replaceAll('(', '').replaceAll(')', '').replaceAll('*', '').replaceAll('#', '').replaceAll(' ', '');
      for (i = 0, len = proxy.length; i < len; i++) {
        ch = proxy[i];
        ch = parseInt(ch);
        if (!angular.isNumber(ch) || isNaN(ch)) {
          return false;
        }
      }
      return true;
    };
    return {

      /**
      *   @ngdoc directive
      *   @name ui.cms.directive:uicTelephoneInput
      *   @description
      *       инпут для телефонных номеров. Загрузится вот этот виджет https://github.com/jackocnr/intl-tel-input
      *   @restrict E
      *   @param {object} ngModel (ссылка) модель
      *   @param {string=} [placeholder='Telephone'] текст, который отображается на месте пустого ngModel
      *   @param {string=} initialCountry (значение) страна по умолчанию для пустого виджета
      *   @param {string=} preferredCountries (значение) список стран, которые отображаются вверху списка (по умолчанию берутся страны для языков из CMS_ALLOWED_LANGS). Например: "ru,uk"
      *   @param {string=} allowedCountries (значение) список стран, которые доступны для выбора. Можно указать через запятую также и значение "континентов" из {@link ui.cms.service:$countries}
      *   @param {string=} inputClass css-класс для внутренней верстки. Можно, например передать класс 'input-sm'.
      *   @param {string=} ngRequired (ссылка/значение) установите этот атрибут в директиве для того, чтоб значение ng-model==null приводило к ошибкам валидации формы
      *   @param {boolean=} ngDisabled (ссылка) флаг доступности тега
      *   @example
      *       <pre>
      *           $scope.phoneNumber = ""
      *       </pre>
      *       <pre>
      *           //-pug
      *           uic-telephone-input(ng-model="phoneNumber", preferred-countries='ua,ru', allowed-countries='europe')
      *       </pre>
      *       <example module="DocExampleApp">
      *           <file name="script.js">
      *               mkDocCtrl('uicTelephoneInputCtrl', {
      *                   phoneNumber: '+380123412345'
      *               });
      *           </file>
      *           <file name="index.html">
      *               <div ng-controller="uicTelephoneInputCtrl">
      *                   <form name='form'><fieldset style='background: white; padding: 15px;'>
      *                   <div class='well well-sm'>
      *                       <b>Значение:</b> {{phoneNumber | json}}
      *                       <div class='text-danger' ng-show="form.phone.$error.telephone">Неправильный телефон</div>
      *                   </div>
      *                   <uic-telephone-input ng-model="phoneNumber" preferred-countries='ua,ru' allowed-countries='europe' name='phone'></uic-telephone-input>
      *                   </fieldset></form>
      *               </div>
      *           </file>
      *       </example>
       */
      restrict: 'E',
      require: 'ngModel',
      scope: {
        initialCountry: '@?',
        preferredCountries: '@?',
        allowedCountries: '@?',
        placeholder: '@?',
        inputClass: '@?',
        ngDisabled: '=?',
        ngRequired: '=?'
      },
      controller: ["$scope", "$element", "$attrs", "$uicIntegrations", "$timeout", "$countries", function($scope, $element, $attrs, $uicIntegrations, $timeout, $countries) {
        var i, inputElement, intlTelInput, intlTelInputLib, len, ngModelCtrl, onCountryChange, prop, ref, updateNgModel;
        ngModelCtrl = $element.controller('ngModel');
        intlTelInputLib = $uicIntegrations.getThirdPartyLibrary('intl-tel-input');
        $element.addClass('loading-data');
        $scope.proxy = null;
        inputElement = $element.find('input')[0];
        intlTelInput = null;
        ref = ['maxlength', 'minlength'];
        for (i = 0, len = ref.length; i < len; i++) {
          prop = ref[i];
          if ($attrs[prop]) {
            $element.attr(prop, $attrs[prop]);
          }
        }
        updateNgModel = function() {
          var proxy, viewValue;
          proxy = ($scope.proxy || '').trim();
          if (!isValidProxy(proxy)) {
            ngModelCtrl.$setViewValue(void 0);
            ngModelCtrl.$setValidity('telephone', false);
            return;
          }
          if (intlTelInput.getValidationError()) {
            ngModelCtrl.$setViewValue(void 0);
            ngModelCtrl.$setValidity('telephone', false);
            return;
          }
          viewValue = intlTelInput.getNumber(intlTelInputUtils.numberFormat.E164);
          ngModelCtrl.$setValidity('telephone', true);
          ngModelCtrl.$setViewValue(viewValue);
        };
        onCountryChange = function() {
          $timeout(updateNgModel);
        };
        return intlTelInputLib.load().then(function() {
          var allowedCountries, c, code, j, k, l, len1, len2, len3, options, ref1, ref2, ref3, ref4, setPreferredCountires;
          options = {
            separateDialCode: true,
            formatOnDisplay: true,
            localizedCountries: {}
          };
          if ($scope.initialCountry) {
            options.initialCountry = $scope.initialCountry;
          }
          if ($scope.preferredCountries) {
            options.preferredCountries = $scope.preferredCountries.split(',');
          }
          if ($scope.allowedCountries) {
            allowedCountries = [];
            ref1 = $scope.allowedCountries.split(',');
            for (j = 0, len1 = ref1.length; j < len1; j++) {
              code = ref1[j];
              if ($countries[code]) {
                ref2 = $countries[code];
                for (k = 0, len2 = ref2.length; k < len2; k++) {
                  c = ref2[k];
                  allowedCountries.push(c[0]);
                }
              } else {
                allowedCountries.push(code);
              }
            }
            if (allowedCountries.length) {
              options.onlyCountries = allowedCountries;
            }
          }
          setPreferredCountires = !(options.preferredCountries || []).length;
          ref3 = $countries.world;
          for (l = 0, len3 = ref3.length; l < len3; l++) {
            c = ref3[l];
            code = c[0];
            options.localizedCountries[code] = $countries.codeToText(code);
            if (setPreferredCountires && (ref4 = countryCodeToLang(code), indexOf.call(CMS_ALLOWED_LANGS, ref4) >= 0)) {
              options.preferredCountries || (options.preferredCountries = []);
              options.preferredCountries.push(code);
            }
          }
          intlTelInput = window.intlTelInput(inputElement, options);
          $element.addClass('ready');
          if (ngModelCtrl.$modelValue) {
            intlTelInput.setNumber(ngModelCtrl.$modelValue);
            $scope.proxy = intlTelInput.getNumber(intlTelInputUtils.numberFormat.NATIONAL);
          }
          ngModelCtrl.$render = function() {
            intlTelInput.setNumber(ngModelCtrl.$modelValue);
            $scope.proxy = intlTelInput.getNumber(intlTelInputUtils.numberFormat.NATIONAL);
          };
          $scope.$watch('proxy', function(proxy, oldValue) {
            if (proxy === oldValue) {
              return;
            }
            updateNgModel();
          });
          inputElement.addEventListener('countrychange', onCountryChange);
          $scope.$on('$destroy', function() {
            intlTelInput.destroy();
            inputElement.removeEventListener('countrychange', onCountryChange);
          });
        });
      }],
      template: ('/client/cms_app/_inputDirectives/uicTelephoneInput/uicTelephoneInput.html', '<input class="form-control {{inputClass}}" ng-model="proxy" type="tel" placeholder="{{placeholder || (\'Telephone\' | translate)}}" ng-disabled="ngDisabled" ng-required="ngRequired"/>' + '')
    };
  });

}).call(this);
;
(function() {
  var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  angular.module('ui.cms').directive('uicTimepicker', ["H", "$q", function(H, $q) {
    var BaseTimepickerController, JsDateUicTimepickerController, StringUicTimepickerController, TimeHelper, timeStringToJsDate;
    timeStringToJsDate = function(timestr) {
      var d, t;
      if (!timestr) {
        return null;
      }
      d = new Date();
      t = timestr.split(':');
      if (t.length > 1) {
        d.setHours(parseInt(t[0]));
        d.setMinutes(parseInt(t[1]));
      } else {
        d.setHours(parseInt(t[0]));
      }
      return d;
    };
    TimeHelper = (function() {
      function TimeHelper(steps, timeDisabledFn) {
        var hourStep, minuteStep;
        this.min = {
          h: 0,
          m: 0
        };
        this.max = {
          h: 23,
          m: 59
        };
        this.timeDisabledFn = timeDisabledFn || angular.identity;
        steps = steps || {};
        hourStep = this.parseStep(steps.hour, 1);
        if (hourStep > 12) {
          hourStep = 1;
        }
        minuteStep = this.parseStep(steps.minute, 5);
        if (minuteStep > 30) {
          minuteStep = 5;
        }
        this.steps = {
          hour: hourStep,
          minute: minuteStep
        };
        this.validDatetimes = [];
      }

      TimeHelper.prototype.parseStep = function(value, _default) {
        var error, step;
        try {
          step = parseInt(value);
        } catch (error1) {
          error = error1;
          step = _default;
        }
        if (!step || isNaN(step)) {
          step = _default;
        }
        return Math.abs(step) || _default;
      };

      TimeHelper.prototype.padLeft = function(value) {
        if (value < 10) {
          return "0" + value;
        }
        return "" + value;
      };

      TimeHelper.prototype.setMinTime = function(hours, minutes) {
        this.min = {
          h: parseInt(hours) || 0,
          m: parseInt(minutes) || 0
        };
      };

      TimeHelper.prototype.setMaxTime = function(hours, minutes) {
        this.max = {
          h: parseInt(hours) || 0,
          m: parseInt(minutes) || 0
        };
      };

      TimeHelper.prototype.normalizeDate = function(value) {
        if (!value) {
          return null;
        }
        value = new Date(value);
        if (isNaN(value.getTime())) {
          return null;
        }
        return value;
      };

      TimeHelper.prototype.isHourDisabled = function(minutes) {
        var disabled;
        disabled = minutes.map(function(m) {
          return m.disabled;
        });
        return !disabled.includes(false);
      };

      TimeHelper.prototype.setMinMaxTime = function(ngModel, minDate, maxDate) {
        var err;
        ngModel = this.normalizeDate(ngModel);
        try {
          if (H.date.isSameDay(ngModel, minDate)) {
            this.setMinTime(minDate.getHours(), minDate.getMinutes());
          } else {
            this.setMinTime(0, 0);
          }
        } catch (error1) {
          err = error1;
        }
        try {
          if (H.date.isSameDay(ngModel, maxDate)) {
            this.setMaxTime(maxDate.getHours(), maxDate.getMinutes());
          } else {
            this.setMaxTime(23, 59);
          }
        } catch (error1) {
          err = error1;
        }
      };

      TimeHelper.prototype.fixProxy = function(proxy) {
        var h, j, l, len, len1, len2, m, n, ref, ref1, ref2;
        if (proxy.m !== null) {
          return proxy;
        }
        if (proxy.h === null) {
          ref = this.validDatetimes;
          for (j = 0, len = ref.length; j < len; j++) {
            h = ref[j];
            if (!(!h.disabled)) {
              continue;
            }
            proxy.h = h.value;
            break;
          }
        }
        ref1 = this.validDatetimes;
        for (l = 0, len1 = ref1.length; l < len1; l++) {
          h = ref1[l];
          if (!(h.value === proxy.h)) {
            continue;
          }
          ref2 = h.minutes;
          for (n = 0, len2 = ref2.length; n < len2; n++) {
            m = ref2[n];
            if (!(!m.disabled)) {
              continue;
            }
            proxy.m = m.value;
            break;
          }
          break;
        }
        if (proxy.m === null) {
          proxy.h -= 1;
          if (proxy.h === -1) {
            proxy.h = this.min.h;
            proxy.m = this.min.m;
            return proxy;
          }
          return this.fixProxy(proxy);
        }
        return proxy;
      };

      TimeHelper.prototype.fixNgModel = function(ngModel, minDate, maxDate) {
        var _ngModel, diff, maxTime, minTime;
        minTime = this.normalizeDate(minDate);
        maxTime = this.normalizeDate(maxDate);
        _ngModel = this.normalizeDate(ngModel);
        if (minTime && _ngModel < minTime) {
          _ngModel = minTime;
        }
        if (maxTime && _ngModel > maxTime) {
          _ngModel = maxTime;
        }
        diff = _ngModel.getMinutes() % this.steps.minute;
        if (diff !== 0) {
          if (_ngModel === minTime) {
            _ngModel.setMinutes(_ngModel.getMinutes() + diff);
          } else {
            _ngModel.setMinutes(_ngModel.getMinutes() + (this.steps.minute - diff));
          }
        }
        return _ngModel;
      };

      TimeHelper.prototype.generateHoursAndMinutesList = function(ngModel) {
        var cleanHoursAndMinutes, d, defer, generateMinutesList, h, hours, j, len, m, ref, result, timelist;
        if (angular.isString(ngModel) && !ngModel.includes('T')) {
          ngModel = timeStringToJsDate(ngModel);
        }
        if (!ngModel) {
          ngModel = new Date();
        }
        ngModel = new Date(ngModel);
        ngModel.setSeconds(0);
        ngModel.setMilliseconds(0);
        generateMinutesList = (function(_this) {
          return function(forHour) {
            var diff, mMax, mMin, minutes;
            minutes = [];
            mMin = 0;
            mMax = 59;
            if (forHour === _this.min.h) {
              mMin = _this.min.m;
            } else if (forHour === _this.max.h) {
              mMax = _this.max.m;
            }
            diff = mMin % _this.steps.minute;
            if (diff !== 0) {
              mMin = mMin + (_this.steps.minute - diff);
            }
            while (mMin <= mMax) {
              minutes.push(mMin);
              mMin += _this.steps.minute;
            }
            return minutes;
          };
        })(this);
        timelist = [];
        hours = [];
        h = this.min.h;
        while (h <= this.max.h) {
          hours.push(h);
          ref = generateMinutesList(h);
          for (j = 0, len = ref.length; j < len; j++) {
            m = ref[j];
            d = new Date(ngModel);
            d.setHours(h);
            d.setMinutes(m);
            timelist.push({
              datetime: d,
              h: h,
              m: m
            });
          }
          h += this.steps.hour;
        }
        defer = $q.defer();
        cleanHoursAndMinutes = (function(_this) {
          return function(cleaned_timelist) {
            var cleaned, ct, dt, i, l, len1;
            cleaned = {};
            for (i = l = 0, len1 = cleaned_timelist.length; l < len1; i = ++l) {
              ct = cleaned_timelist[i];
              dt = timelist[i];
              cleaned[dt.h] = cleaned[dt.h] || [];
              cleaned[dt.h].push({
                value: dt.m,
                text: _this.padLeft(dt.m),
                disabled: !ct
              });
            }
            hours = cleaned.arrayMap(function(k) {
              return parseInt(k);
            });
            hours.sort(function(a, b) {
              return a - b;
            });
            _this.validDatetimes = hours.map(function(h) {
              return {
                value: h,
                text: _this.padLeft(h),
                disabled: _this.isHourDisabled(cleaned[h]),
                minutes: cleaned[h] || []
              };
            });
            return defer.resolve(_this.validDatetimes);
          };
        })(this);
        result = this.timeDisabledFn(angular.copy(timelist));
        if (angular.isFunction(result.then)) {
          result.then(cleanHoursAndMinutes);
        } else if (angular.isArray(result)) {
          cleanHoursAndMinutes(result);
        } else {
          console.error("uicTimepicker: timeDisabled function return invalid value. Expected array or promise. Got - " + (typeof result));
          cleanHoursAndMinutes(timelist);
        }
        return defer.promise;
      };

      return TimeHelper;

    })();
    BaseTimepickerController = (function() {
      function BaseTimepickerController($scope1, $attrs, ngModelCtrl1, timeDisabledFn) {
        this.$scope = $scope1;
        this.ngModelCtrl = ngModelCtrl1;
        this.allowInvalid = ['true', true].includes($attrs.allowInvalid);
        this.helper = new TimeHelper({
          hour: $attrs.hourStep,
          minute: $attrs.step || $attrs.minuteStep
        }, timeDisabledFn || angular.identity);
      }

      BaseTimepickerController.prototype.normalizeNgModel = function(ngModel) {
        return ngModel;
      };

      BaseTimepickerController.prototype.normalizeProxy = function(proxy) {
        return this.helper.fixProxy(proxy);
      };

      BaseTimepickerController.prototype.setNgModelFromProxy = function(proxy) {};

      BaseTimepickerController.prototype.getMinutesListForHour = function(hourList, forHour) {
        var h, j, len, ref;
        ref = hourList || [];
        for (j = 0, len = ref.length; j < len; j++) {
          h = ref[j];
          if (h.value === forHour) {
            return h.minutes;
          }
        }
        return [];
      };

      BaseTimepickerController.prototype.generateHoursMinutesList = function(ngModel) {
        this.helper.generateHoursAndMinutesList(ngModel).then((function(_this) {
          return function(hm) {
            _this.$scope.hours = hm;
            _this.$scope.minutes = _this.getMinutesListForHour(hm, _this.$scope.proxy.h);
          };
        })(this));
      };

      BaseTimepickerController.prototype.runRenderProxyFromNgModel = function() {};

      BaseTimepickerController.prototype.runProxyWatch = function() {
        return this.$scope.$watch('proxy', (function(_this) {
          return function(proxy, oldValue) {
            if (proxy.m === null || proxy.h === null) {
              _this.$scope.proxy = _this.normalizeProxy(proxy);
              _this.setNgModelFromProxy(_this.$scope.proxy);
              return;
            }
            if (proxy.h === _this.helper.max.h && proxy.m > _this.helper.max.m) {
              _this.$scope.proxy.m = _this.helper.max.m;
              if (proxy.h !== oldValue.h) {
                _this.$scope.minutes = _this.getMinutesListForHour(_this.$scope.hours, proxy.h);
              }
              return;
            } else if (proxy.h === _this.helper.min.h && proxy.m < _this.helper.min.m) {
              _this.$scope.proxy.m = _this.helper.min.m;
              if (proxy.h !== oldValue.h) {
                _this.$scope.minutes = _this.getMinutesListForHour(_this.$scope.hours, proxy.h);
              }
              return;
            }
            if (proxy.h < _this.helper.min.h) {
              proxy.h = _this.helper.min.h;
              _this.$scope.proxy = _this.normalizeProxy(proxy);
              return;
            }
            if (proxy.h > _this.helper.max.h) {
              proxy.h = _this.helper.max.h;
              _this.$scope.proxy = _this.normalizeProxy(proxy);
              return;
            }
            if (proxy.h !== oldValue.h) {
              _this.$scope.minutes = _this.getMinutesListForHour(_this.$scope.hours, proxy.h);
            }
            _this.setNgModelFromProxy(proxy);
          };
        })(this), true);
      };

      BaseTimepickerController.prototype.run = function() {
        this.$scope.proxy = {
          h: null,
          m: null
        };
        this.runProxyWatch();
        this.runRenderProxyFromNgModel();
      };

      return BaseTimepickerController;

    })();
    JsDateUicTimepickerController = (function(superClass) {
      extend(JsDateUicTimepickerController, superClass);

      function JsDateUicTimepickerController() {
        return JsDateUicTimepickerController.__super__.constructor.apply(this, arguments);
      }

      JsDateUicTimepickerController.prototype.normalizeNgModel = function(ngModel) {
        return this.helper.fixNgModel(ngModel, this.$scope.minDate, this.$scope.maxDate);
      };

      JsDateUicTimepickerController.prototype.runRenderProxyFromNgModel = function() {
        var lastNgModelValue;
        lastNgModelValue = null;
        this.ngModelCtrl.$render = (function(_this) {
          return function() {
            var error, modelValue;
            modelValue = _this.ngModelCtrl.$modelValue;
            if (!modelValue) {
              _this.$scope.proxy = {
                h: null,
                m: null
              };
              return;
            }
            modelValue = _this.normalizeNgModel(modelValue);
            try {
              _this.$scope.proxy = _this.normalizeProxy({
                h: modelValue.getHours(),
                m: modelValue.getMinutes()
              });
            } catch (error1) {
              error = error1;
              _this.$scope.proxy = {
                h: null,
                m: null
              };
              return;
            }
            _this.helper.setMinMaxTime(modelValue, _this.$scope.minDate, _this.$scope.maxDate);
            if (!H.date.isSameDay(lastNgModelValue, modelValue)) {
              lastNgModelValue = new Date(modelValue);
              _this.generateHoursMinutesList(lastNgModelValue);
            }
          };
        })(this);
      };

      JsDateUicTimepickerController.prototype.setNgModelFromProxy = function(proxy) {
        var modelValue;
        if (proxy.m === null && proxy.h === null) {
          this.ngModelCtrl.$setViewValue(null);
          return;
        }
        modelValue = this.helper.normalizeDate(this.ngModelCtrl.$modelValue);
        if (!modelValue) {
          modelValue = new Date();
        }
        modelValue.setHours(proxy.h);
        modelValue.setMinutes(proxy.m);
        modelValue.setMilliseconds(0);
        this.ngModelCtrl.$setViewValue(this.normalizeNgModel(modelValue));
      };

      JsDateUicTimepickerController.prototype.run = function() {
        JsDateUicTimepickerController.__super__.run.call(this);
        this.$scope.$watchGroup(['minDate', 'maxDate', 'ngDisabled'], (function(_this) {
          return function(newValue, oldValue) {
            _this.helper.setMinMaxTime(_this.ngModelCtrl.$modelValue, _this.$scope.minDate, _this.$scope.maxDate);
            _this.$scope.proxy = _this.normalizeProxy(_this.$scope.proxy);
            _this.$scope.proxy._forceUpdate = new Date().getTime();
            _this.generateHoursMinutesList(_this.ngModelCtrl.$modelValue);
          };
        })(this));
      };

      return JsDateUicTimepickerController;

    })(BaseTimepickerController);
    StringUicTimepickerController = (function(superClass) {
      extend(StringUicTimepickerController, superClass);

      function StringUicTimepickerController() {
        return StringUicTimepickerController.__super__.constructor.apply(this, arguments);
      }

      StringUicTimepickerController.prototype.normalizeNgModel = function(ngModel) {
        if (angular.isString(ngModel) && !ngModel.includes('T')) {
          ngModel = timeStringToJsDate(ngModel);
        } else {
          ngModel = new Date(ngModel);
        }
        return (this.helper.padLeft(ngModel.getHours())) + ":" + (this.helper.padLeft(ngModel.getMinutes()));
      };

      StringUicTimepickerController.prototype.runRenderProxyFromNgModel = function() {
        var lastNgModelValue;
        lastNgModelValue = null;
        this.ngModelCtrl.$render = (function(_this) {
          return function() {
            var error, modelValue, t;
            try {
              t = _this.ngModelCtrl.$modelValue.split(':');
              _this.$scope.proxy = _this.helper.fixProxy({
                h: parseInt(t[0]),
                m: parseInt(t[1])
              });
            } catch (error1) {
              error = error1;
              _this.$scope.proxy = {
                h: null,
                m: null
              };
              return;
            }
            modelValue = timeStringToJsDate(_this.ngModelCtrl.$modelValue);
            _this.helper.setMinMaxTime(modelValue, timeStringToJsDate(_this.$scope.minTime), timeStringToJsDate(_this.$scope.maxTime));
            if (!H.date.isSameDay(lastNgModelValue, modelValue)) {
              lastNgModelValue = new Date(modelValue);
              _this.generateHoursMinutesList(lastNgModelValue);
            }
          };
        })(this);
      };

      StringUicTimepickerController.prototype.setNgModelFromProxy = function(proxy) {
        if (proxy.h === null || proxy.m === null) {
          this.ngModelCtrl.$setViewValue(null);
          return;
        }
        this.ngModelCtrl.$setViewValue((this.helper.padLeft(proxy.h)) + ":" + (this.helper.padLeft(proxy.m)));
      };

      StringUicTimepickerController.prototype.run = function() {
        StringUicTimepickerController.__super__.run.call(this);
        this.$scope.$watchGroup(['minTime', 'maxTime', 'ngDisabled'], (function(_this) {
          return function(newValue, oldValue) {
            _this.helper.setMinMaxTime(timeStringToJsDate(_this.ngModelCtrl.$modelValue), timeStringToJsDate(_this.$scope.minTime), timeStringToJsDate(_this.$scope.maxTime));
            _this.$scope.proxy = _this.normalizeProxy(_this.$scope.proxy);
            _this.generateHoursMinutesList(timeStringToJsDate(_this.ngModelCtrl.$modelValue));
          };
        })(this));
      };

      return StringUicTimepickerController;

    })(BaseTimepickerController);
    return {

      /**
      *   @ngdoc directive
      *   @name ui.cms.directive:uicTimepicker
      *   @description
      *       пикер времени. Может работать как с текстовыми представлениями в формате "XX:YY", так и с js-объектом Date
      *   @restrict E
      *   @param {string | Date} ngModel (ссылка) время. По умолчанию ngModel всегда будет конвертироваться в
      *           string, без даты и часового пояса (т.е. в формат: "XX:YY"). Если нужно отключить такое поведение, то используйте параметр ngModelType='date'
      *   @param {string=} minTime (значение) минимальное время в формате "XX:YY" (атрибут доступен если <b>НЕ</b> указан ngModelType='date')
      *   @param {string=} maxTime (значение) максимальное время в формате "XX:YY" (атрибут доступен если <b>НЕ</b> указан ngModelType='date')
      *   @param {string | Date =} minDate (ссылка) минимальная дата+время (атрибут доступен только при указании ngModelType='date')
      *   @param {string | Date =} maxDate (ссылка) максимальное дата+время (атрибут доступен только при указании ngModelType='date')
      *   @param {number=} [minuteStep=5] (значение) шаг минут (1...30)
      *   @param {number=} [hourStep=1] (значение) шаг часов (1...12)
      *   @param {function=} timeDisabled функция для отключения определённых времен в пикере. как параметр передается громадный массив объектов в формате {datetime: new Date(), h: 10, m: 30}.
      *       При этом функция должна либо вернуть promise, в котором в один из моментов разрешит значение, либо вернуть массив.
      *       Массив, который подается на вход функции timeDisabled в отключенных временах должен вместо значений содержать false
      *   @param {string=} [ngModelType='string'] (значение) тип переменной ngModel. Если нужно отключить режим "только времени" и перестать принудительно
      *           конвертировать объект Date в string для ngModel, то передайте ng-model-type='date'.
      *   @param {string=} inputClass (значение) дополнительный класс для двух select-ов (часы + минуты) в верстке
      *   @param {boolean=} ngDisabled (ссылка) флаг доступности тега
      *   @example
      *       <pre>
      *           $scope.myStringDate = '11:20'
      *           $scope.myDate = new Date()
      *           # функция, которая сделает недоступным выбор первых 10 вхождений времени
      *           # и вхождений, индекс которых более 140
      *           $scope.myTimeDisabled = (datetimes)->
      *               for dt,i in datetimes when (i < 10 or i>140)
      *                   # dt = {datetime: <Date>, h: <Number>, m: <Number> }
      *                   datetimes[i] = false
      *               datetimes
      *       </pre>
      *       <pre>
      *           //- pug
      *           // обычная текстовая дата, с ограничением максимального времени и шагом в 10 минут
      *           uic-timepicker(ng-model="myStringDate", max-time='22:30', step='10')
      *           // js Date, без ограничений по времени
      *           uic-timepicker(ng-model="myDate", ng-model-type='date')
      *           // js Date, с отключенными временами
      *           uic-timepicker(ng-model="myDate", time-disabled="myTimeDisabled()", ng-model-type='date')
      *       </pre>
      *       <example module="DocExampleApp">
      *           <file name="script.js">
      *               mkDocCtrl('uicTimepickerCtrl', {
      *                   myDate: new Date(),
      *                   myTimeDisabled: function(datetimes){
      *                       for (var i = 0; i < datetimes.length; i++) {
      *                           if (i<10 || i>40){
      *                               datetimes[i] = false;
      *                           }
      *                       }
      *                       return datetimes
      *                   }
      *               });
      *           </file>
      *           <file name="index.html">
      *               <div ng-controller="uicTimepickerCtrl">
      *                   <div class='row'>
      *                       <div class='col-sm-25'>
      *                           <p>обычная текстовая дата, с ограничением максимального времени и шагом в 10 минут</p>
      *                           <p><b>$scope.myDate =</b> {{myStringDate}}</p>
      *                           <uic-timepicker ng-model="myStringDate" max-time='22:30', step='10'></uic-timepicker>
      *                       </div>
      *                   </div>
      *                   <br/>
      *                   <div class='row'>
      *                       <div class='col-sm-25'>
      *                           <p>js Date, Без ограничений по времени</p>
      *                           <p><b>$scope.myDate =</b> {{myDate}}</p>
      *                           <uic-timepicker ng-model="myDate" ng-model-type='date'></uic-timepicker>
      *                       </div>
      *                       <div class='col-sm-25'>
      *                           <p>js Date, С отключенными временами</p>
      *                           <p><b>$scope.myDate =</b> {{myDate}}</p>
      *                           <uic-timepicker ng-model="myDate" time-disabled="myTimeDisabled()" ng-model-type='date'></uic-timepicker>
      *                       </div>
      *                   </div>
      *               </div>
      *           </file>
      *       </example>
       */
      restrict: 'E',
      require: 'ngModel',
      scope: {
        inputClass: '@?',
        minDate: '=?',
        maxDate: '=?',
        minTime: '@?',
        maxTime: '@?',
        timeDisabled: '&?',
        ngDisabled: '=?'
      },
      link: function($scope, $element, $attrs, ngModelCtrl) {
        var ctrl, timeDisabled;
        if (($attrs.minDate && $attrs.minTime) || ($attrs.minDate && $attrs.maxTime) || ($attrs.maxDate && $attrs.minTime) || ($attrs.maxDate && $attrs.maxTime)) {
          throw "uicTimepicker: you can't set both *-time and *-date attributes on this directive";
        }
        if ($attrs.timeDisabled) {
          timeDisabled = $scope.$parent.$eval($attrs.timeDisabled.split('(')[0]);
        }
        if (!angular.isFunction(timeDisabled)) {
          timeDisabled = null;
        }
        if ($attrs.allowJsdate === 'true' || $attrs.ngModelType === 'date') {
          ctrl = new JsDateUicTimepickerController($scope, $attrs, ngModelCtrl, timeDisabled);
        } else {
          ctrl = new StringUicTimepickerController($scope, $attrs, ngModelCtrl, timeDisabled);
        }
        return ctrl.run();
      },
      template: ('/client/cms_app/_inputDirectives/uicTimepicker/uicTimepicker.html', '<table class="w-100"><tbody><tr><td class="uic-timepicker-hour"><select class="form-control {{:: inputClass}}" ng-model="proxy.h" ng-options="h.value as h.text  disable when h.disabled  for h in hours" ng-disabled="ngDisabled"></select></td><td class="uic-timepicker-separator">:</td><td class="uic-timepicker-minute"><select class="form-control {{:: inputClass}}" ng-model="proxy.m" ng-options="m.value as m.text  disable when m.disabled  for m in minutes" ng-disabled="ngDisabled"></select></td></tr></tbody></table>' + '')
    };
  }]);

}).call(this);
;
(function() {
  angular.module('ui.cms').directive('uicTimezonePicker', function() {
    return {

      /**
      *   @ngdoc directive
      *   @name ui.cms.directive:uicTimezonePicker
      *   @description  пикер названия таймзоны
      *
      *   @restrict E
      *   @param {string} ngModel (ссылка) название таймзоны
      *   @param {string=} placeholder текст, который отображается на месте пустого ngModel.
      *   @param {string=} [allowed='world'] allowed (значение) часовые пояса, которые будут доступны для выбора, перечисленные через запятую.
      *       Варианты: 'europe', 'asia', 'africa', 'america', 'australia', 'world'.
      *   @param {string=} inputClass css-класс для внутренней верстки. Можно, например передать класс 'input-sm'.
      *   @param {string=} ngRequired (ссылка/значение) установите этот атрибут в директиве для того, чтоб значение ng-model==null приводило к ошибкам валидации формы
      *   @param {boolean=} ngDisabled (ссылка) флаг доступности тега
      *   @example
      *       <pre>
      *           $scope.myTimezone = 'Europe/Amsterdam'
      *       </pre>
      *       <pre>
      *           //- pug
      *           p Выбранный часовой пояс - {{myTimezone}}
      *           uic-timezone-picker(ng-model="myTimezone", allowed='europe,asia', input-class='input-sm')
      *       </pre>
      *       <example module="DocExampleApp">
      *           <file name="script.js">
      *               mkDocCtrl('uicTimezonePickerCtrl', {
      *                   myTimezone: 'Europe/Amsterdam'
      *               });
      *           </file>
      *           <file name="index.html">
      *               <div ng-controller="uicTimezonePickerCtrl">
      *                   <p>Выбранный часовой пояс - {{myTimezone}}</p>
      *                   <uic-timezone-picker ng-model="myTimezone" allowed='europe,asia' input-class='input-sm'>
      *                   </uic-timezone-picker>
      *               </div>
      *           </file>
      *       </example>
       */
      restrict: 'E',
      require: 'ngModel',
      scope: {
        allowed: '@?',
        placeholder: '@?',
        inputClass: '@?',
        ngDisabled: '=?',
        ngRequired: '=?'
      },
      controller: ["$scope", "$element", "$attrs", "$timezones", "$langPicker", function($scope, $element, $attrs, $timezones, $langPicker) {
        var allowedTimezones, ngModelCtrl;
        ngModelCtrl = $element.controller('ngModel');
        $scope.proxy = null;
        $scope.$langPicker = $langPicker;
        allowedTimezones = $timezones.getList.apply(null, ($scope.allowed || '').split(','));
        $scope.allowedTimezones = allowedTimezones.map(function(c) {
          return {
            id: c[0],
            name: c[1]
          };
        });
        if ($attrs.hasOwnProperty('required')) {
          console.warn("uicTimezonePicker: Dont use 'required' attribute! Use ngRequired instead");
          $scope.ngRequired = true;
        }
        $scope.$watch('$langPicker.currentLang', function(currentLang, oldValue) {
          var c, i, j, k, len, len1, ref, ref1;
          if (currentLang === 'en') {
            ref = $scope.allowedTimezones;
            for (i = j = 0, len = ref.length; j < len; i = ++j) {
              c = ref[i];
              $scope.allowedTimezones[i].name = allowedTimezones[i][1];
            }
            return;
          }
          ref1 = $scope.allowedTimezones;
          for (i = k = 0, len1 = ref1.length; k < len1; i = ++k) {
            c = ref1[i];
            $scope.allowedTimezones[i].name = $timezones.codeToText(c.id);
          }
          $scope.allowedTimezones.sort(function(a, b) {
            if (a.name < b.name) {
              return -1;
            }
            if (a.name > b.name) {
              return 1;
            }
            return 0;
          });
        });
        ngModelCtrl.$render = function() {
          $scope.proxy = ngModelCtrl.$modelValue;
        };
        $scope.$watch('proxy', function(proxy, oldValue) {
          ngModelCtrl.$setViewValue(proxy || null);
        });
      }],
      template: ('/client/cms_app/_inputDirectives/uicTimezonePicker/uicTimezonePicker.html', '<uic-select ng-model="proxy" items="allowedTimezones" placeholder="{{placeholder || (\'Select timezone\' | translate)}}" input-class="{{:: inputClass}}" ng-required="ngRequired"><item>{{$item.name}}</item><selected>{{$selected.name}}</selected></uic-select>' + '')
    };
  });

}).call(this);
;
(function() {
  angular.module('ui.cms').directive('uicCurrencyPickerForNavbar', function() {
    return {

      /**
      *   ngdoc directive
      *   name ui.cms.directive:uicCurrencyPickerForNavbar
      *   description
      *       <b>DEPRECATED</b>: переключатель валюты для навбара. Перерубает валюту по умолчанию для фильтра {@link ui.cms.filter:humanCurrency}
      *       Используется в редакторе меню
      *   restrict A
       */
      restrict: 'A',
      replace: true,
      scope: {},
      controller: ["$scope", "$cms", "$bulk", function($scope, $cms, $bulk) {
        $scope.$cms = $cms;
        if ($cms.settings.defaultCurrency) {
          $cms.settings.$preferredCurrency = $cms.settings.defaultCurrency;
        } else {
          $bulk.onReady(function() {
            return $cms.settings.$preferredCurrency = $cms.settings.defaultCurrency;
          });
        }
        return $scope.setCurrency = function(c) {
          return $cms.settings.$preferredCurrency = c;
        };
      }],
      template: ('/client/cms_app/_navbarDirectives/uicCurrencyPickerForNavbar/uicCurrencyPickerForNavbar.html', '<li uib-dropdown=""><a style="cursor:pointer;" ng-disabled="attrs.disabled" uib-dropdown-toggle=""><span translate="">Currency</span>: {{$cms.settings.$preferredCurrency}}<span class="caret"></span></a><ul class="dropdown-menu"><li ng-repeat="c in $cms.settings.allowedCurrency" ng-class="{active: c == $cms.settings.$preferredCurrency}"><a style="cursor:pointer;" ng-click="setCurrency(c)">{{c}}</a></li></ul></li>' + '')
    };
  });

}).call(this);
;
(function() {
  var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  angular.module('ui.cms').factory('FooterNavGenerator', ["NavbarNavGenerator", function(NavbarNavGenerator) {
    var FooterNavGenerator;
    return FooterNavGenerator = (function(superClass) {
      extend(FooterNavGenerator, superClass);

      function FooterNavGenerator() {
        return FooterNavGenerator.__super__.constructor.apply(this, arguments);
      }

      FooterNavGenerator.prototype.itemToLi = function(item) {
        switch (item.type) {
          case 'href':
            return this.hrefToLi(item);
          case 'ui-sref':
            return this.uisrefToLi(item);
          case 'ui-sref:app.articles.details':
            return this.uisrefToLi(item, 'app.articles.details');
          case 'ui-sref:app.galleries.details':
            return this.uisrefToLi(item, 'app.galleries.details');
          case 'ui-sref:anchor':
            return this.uisrefAnchorToLi(item);
          case 'file':
            return this.fileToLi(item);
        }
        return '';
      };

      return FooterNavGenerator;

    })(NavbarNavGenerator);
  }]);

}).call(this);
;
(function() {
  angular.module('ui.cms').directive('uicFooterNav', function() {
    return {

      /**
      *   @ngdoc directive
      *   @name ui.cms.directive:uicFooterNav
      *   @description директива, которая генерирует элементы для navbar-nav, который можно размещать в footer-е.
      *       Директива аналогична {@link ui.cms.directive:uicNavbarNav uicNavbarNav}, только она НЕ РЕНДЕРИТ dropdown-ы и кастомные директивы.
      *       Все параметры аналогичны {@link ui.cms.directive:uicNavbarNav uicNavbarNav}. Подробнее см. пример.
      *   @restrict E
      *   @param {object} ngModel (ссылка) модель
      *   @example
      *       <pre>
      *           $scope.nav = {
      *                right: [
      *                    {
      *                        type: 'href',
      *                        data: 'www.google.com',
      *                        title: {ru:"Ссылка 1", en:"Link 1"},
      *                        attrs: {target: '_blank'}
      *                    },
      *                    {type: 'ui-sref', data: 'app.index', title:{en: "Index"}},
      *                    {type: 'action', data:'someActionService'},
      *                    {type: 'directive', data: 'ui-lang-picker-for-navbar'},
      *                    {
      *                        title: {en:'Menu', ru:'Меню'},
      *                        items:[
      *                            {
      *                               type: 'href',
      *                               data: 'www.yandex.ru',
      *                               title: {ru:"Ссылка 2", en:"Link 2"}
      *                            },
      *                            {
      *                               type: 'ui-sref',
      *                               data: 'app.index',
      *                               title: {ru:"Основная страница", en:"Index page"}
      *                           }
      *                        ]
      *                    }
      *                ]
      *            }
      *       </pre>
      *       <pre>
      *           //- pug
      *           uic-footer-nav(ng-model="nav")
      *       </pre>
      *       <example module="DocExampleApp">
      *           <file name="script.js">
      *               mkDocCtrl('uicFooterNavCtrl', {
      *                 nav: {
      *                   right: [
      *                        {
      *                            type: 'href',
      *                            data: 'www.google.com',
      *                            title: {ru:"Ссылка 1", en:"Link 1"},
      *                            attrs: {target: '_blank'}
      *                       },
      *                        {type: 'ui-sref', data: 'app.index', title:{en: "Index"}},
      *                        {type: 'action', data:'someActionService'},
      *                        {type: 'directive', data: 'ui-lang-picker-for-navbar'},
      *                        {
      *                            title: {en:'Menu', ru:'Меню'},
      *                            items:[
      *                                {
      *                                   type: 'href',
      *                                  data: 'www.yandex.ru',
      *                                   title: {ru:"Ссылка 2", en:"Link 2"}
      *                                },
      *                                {
      *                                   type: 'ui-sref',
      *                                   data: 'app.index',
      *                                   title: {ru:"Основная страница", en:"Index page"}
      *                               }
      *                           ]
      *                        }
      *                    ]
      *                   }
      *               });
      *           </file>
      *           <file name="index.html">
      *               <div ng-controller="uicFooterNavCtrl">
      *                   <uic-footer-nav ng-model="nav"></uic-footer-nav>
      *               </div>
      *           </file>
      *       </example>
       */
      restrict: 'E',
      scope: {
        ngModel: '='
      },
      controller: ["$scope", "$cms", "FooterNavGenerator", "$langPicker", "$timeout", function($scope, $cms, FooterNavGenerator, $langPicker, $timeout) {
        var generator;
        $scope.$cms = $cms;
        $scope.$langPicker = $langPicker;
        generator = new FooterNavGenerator($scope.ngModel, {});
        $scope.updateGeneratedData = function() {
          generator.ngModel = $scope.ngModel;
          generator.render();
          $scope.generatedNav = generator.html;
          $scope.vars = generator.vars;
        };
        $scope.$watch('ngModel', $scope.updateGeneratedData, true);
        return $scope.$watch('$langPicker.currentLang', function(currentLang, oldValue) {
          if (currentLang !== oldValue) {
            $scope.generatedNav = {};
            $timeout($scope.updateGeneratedData, 100);
          }
        });
      }],
      template: ('/client/cms_app/_navbarDirectives/uicFooterNav/uicFooterNav.html', '<ul class="footer-nav-left" uic-bind-html="generatedNav.left" ng-if="generatedNav.left"></ul><ul class="footer-nav-center" uic-bind-html="generatedNav.center" ng-if="generatedNav.center"></ul><ul class="footer-nav-right" uic-bind-html="generatedNav.right" ng-if="generatedNav.right"></ul>' + '')
    };
  });

}).call(this);
;
(function() {
  var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  angular.module('ui.cms').provider('NavbarNavGenerator', function() {
    var getTemplate, templates;
    templates = {
      href: "<li ui-sref-active=\"active\" {{liAttrs}}>\n    <a href='{{href}}' {{attrs}} ng-click=\"$cms.collapseNavbar()\">\n        {{title}}\n    </a>\n</li>",
      uiSref: "<li ui-sref-active=\"active\" {{liAttrs}}>\n    <a ui-sref='{{uiSref}}' {{attrs}} ng-click=\"$cms.collapseNavbar()\">{{title}}</a>\n</li>",
      uisrefAnchor: "<li du-scrollspy='' offset='{{offset}}' {{liAttrs}}>\n    <a du-smooth-scroll='{{anchor}}' offset='{{offset}}' {{attrs}} ng-click=\"toView('{{uiSref}}', '{{anchor}}'); $cms.collapseNavbar()\">\n        {{title}}\n    </a>\n</li>",
      dropdown: "<li class='dropdown' uib-dropdown {{liAttrs}}>\n    <a class='dropdown-toggle' uib-dropdown-toggle style='cursor:pointer;'>\n        <span>{{title}}</span>\n        <span class='caret'></span>\n    </a>\n    <ul class='dropdown-menu'>\n        {{itemsHtml}}\n    </ul>\n</li>",
      action: "<li ui-sref-active=\"active\" {{liAttrs}}>\n    <a ng-click='$cms.collapseNavbar();$cms.runSiteAction(\"{{actionName}}\")' style='cursor:pointer;'>\n        {{title}}\n    </a>\n</li>",
      file: "<li ng-if=\"cmsModelItem.files.length\" {{liAttrs}}>\n    <a ng-click='$cms.collapseNavbar()' style='cursor:pointer;'\n        download='' target='_blank'\n        uic-href=\"cmsModelItem.files\" file-id='{{fileId}}'\n        >\n        {{title}}\n    </a>\n</li>",
      directive: "<li {{directiveName}} {{liAttrs}}></li>",
      contact: "<li {{liAttrs}} class='li-uic-contact'>\n    <uic-contact ng-model='$cms.settings.publicContacts[{{contactIndex}}]'>\n</li>"
    };
    getTemplate = function(name, context) {
      var html, k, v;
      context || (context = {});
      html = templates[name] + '';
      for (k in context) {
        v = context[k];
        html = html.replaceAll("{{" + k + "}}", v).replaceAll("{{ " + k + " }}", v);
      }
      return html;
    };
    this.setNavbarTemplates = function(data) {
      var k, ref, v;
      ref = data || {};
      for (k in ref) {
        v = ref[k];
        templates[k] = v || templates[k];
      }
    };
    this.$get = [
      "$injector", "H", "HtmlGenerator", function($injector, H, HtmlGenerator) {
        var $cms, $models, NavbarNavGenerator;
        if ($injector.has('$models')) {
          $models = $injector.get('$models');
        } else {
          $models = {};
        }
        if ($injector.has('$cms')) {
          $cms = $injector.get('$cms');
        } else {
          $cms = {
            settings: {}
          };
        }
        return NavbarNavGenerator = (function(superClass) {
          extend(NavbarNavGenerator, superClass);

          function NavbarNavGenerator() {
            return NavbarNavGenerator.__super__.constructor.apply(this, arguments);
          }

          NavbarNavGenerator.prototype.getTitle = function(item) {
            var i, template, title;
            template = this.ngModel.itemInnerTemplate || '{{title}}';
            if (item.html) {
              if (item.html.pre) {
                template = item.html.pre + template;
              }
              if (item.html.post) {
                template = template + item.html.post;
              }
            }
            title = '';
            if (item.title) {
              i = this.addVar(item.title);
              return template.replace(/{{title}}/g, "{{vars[" + i + "] | l10n}}");
            }
            if (!title) {
              return template.replace(/{{title}}/g, "" + item.data);
            }
            return title;
          };

          NavbarNavGenerator.prototype.getHideItemAttr = function(item) {
            var hide, lang_code, ref, ref1, screenSize, v;
            if (!item) {
              return '';
            }
            hide = [];
            ref = item.hideForLangs || {};
            for (lang_code in ref) {
              v = ref[lang_code];
              if (v) {
                hide.push("$langPicker.currentLang=='" + lang_code + "'");
              }
            }
            ref1 = item.hideForScreenSize || {};
            for (screenSize in ref1) {
              v = ref1[screenSize];
              if (v) {
                hide.push("$cms.screenSize=='" + screenSize + "'");
              }
            }
            if (!hide.length) {
              return '';
            }
            return "ng-hide=\"" + (hide.join(' || ')) + "\" ";
          };

          NavbarNavGenerator.prototype.hrefToLi = function(item) {
            var attrs, href, ref, title;
            href = item.data;
            if (!href) {
              return '';
            }
            title = this.getTitle(item);
            attrs = this.objToAttrsString(item, 'attrs');
            href = this.normalizeUrl(href);
            if ((ref = href[0]) !== '#' && ref !== '/') {
              attrs = attrs + ' uic-rel';
            }
            return getTemplate('href', {
              attrs: attrs,
              href: href,
              title: title,
              liAttrs: this.getHideItemAttr(item)
            });
          };

          NavbarNavGenerator.prototype.uisrefToLi = function(item, view) {
            var attrs, categoryIndex, data, index, obj, title;
            title = this.getTitle(item);
            attrs = this.objToAttrsString(item, 'attrs');
            if (!view) {
              view = item.data;
            } else {
              if (view === 'app.articles.details') {
                obj = $models.CmsArticle.findItemByField('id', item.data);
              } else if (view === 'app.galleries.details') {
                obj = $models.CmsPictureAlbum.findItemByField('id', item.data);
              }
              if (!obj) {
                return '';
              }
              if (this.isL10nObjectEmpty(item.title)) {
                data = angular.copy(item);
                data.title = obj.title;
                title = this.getTitle(data);
              }
              index = this.addVar(obj);
              if (obj.$category) {
                categoryIndex = this.addVar(obj.$category);
              } else if (obj.category) {
                categoryIndex = this.addVar({
                  id: obj.category
                });
              } else {
                categoryIndex = this.addVar({
                  id: '-'
                });
              }
              view = view = view + "({urlId: (vars[" + index + "] | toUrlId), categoryId: (vars[" + categoryIndex + "] | toUrlId)})";
            }
            return getTemplate('uiSref', {
              attrs: attrs,
              uiSref: view,
              title: title,
              liAttrs: this.getHideItemAttr(item)
            });
          };

          NavbarNavGenerator.prototype.uisrefAnchorToLi = function(item) {
            var anchor, attrs, data, offset, title, view;
            title = this.getTitle(item);
            attrs = this.objToAttrsString(item, 'attrs');
            data = item.data.split('#');
            view = data[0];
            anchor = data[1];
            offset = this.options.navbarHeight || 0;
            if (!this.options.navbarFixedTop) {
              offset = 0;
            }
            return getTemplate('uisrefAnchor', {
              attrs: attrs,
              uiSref: view,
              anchor: anchor,
              offset: offset,
              title: title,
              liAttrs: this.getHideItemAttr(item)
            });
          };

          NavbarNavGenerator.prototype.uisrefGeneratorToLi = function(item, _type) {
            var attrs, category, categoryIndex, data, index, itemsLi, j, len, obj, ref, title;
            if (this.isL10nObjectEmpty(item.title)) {
              category = $models.CmsCategoryForArticle.getItemById(item.data);
              data = {
                title: category.title,
                html: item.html
              };
              title = this.getTitle(data);
            } else {
              title = this.getTitle(item);
            }
            attrs = this.objToAttrsString(item, 'attrs');
            itemsLi = "";
            if (_type === 'article-category') {
              ref = $models.CmsArticle.items || [];
              for (j = 0, len = ref.length; j < len; j++) {
                obj = ref[j];
                if (!(obj.category === item.data)) {
                  continue;
                }
                index = this.addVar(obj);
                if (obj.$category) {
                  categoryIndex = this.addVar(obj.$category);
                } else if (obj.category) {
                  categoryIndex = this.addVar({
                    id: obj.category
                  });
                } else {
                  categoryIndex = this.addVar({
                    id: '-'
                  });
                }
                data = {
                  title: obj.title,
                  data: "app.articles.details({urlId:(vars[" + index + "] | toUrlId), categoryId: (vars[" + categoryIndex + "] | toUrlId)})"
                };
                itemsLi += this.uisrefToLi(data);
              }
            }
            if (!itemsLi.length) {
              return '';
            }
            return getTemplate('dropdown', {
              itemsHtml: itemsLi,
              title: title,
              liAttrs: this.getHideItemAttr(item)
            });
          };

          NavbarNavGenerator.prototype.actionToLi = function(item) {
            var title;
            title = this.getTitle(item);
            return getTemplate('action', {
              actionName: item.data,
              title: title,
              liAttrs: this.getHideItemAttr(item)
            });
          };

          NavbarNavGenerator.prototype.fileToLi = function(item) {
            var title;
            if (!item.data) {
              return '';
            }
            title = this.getTitle(item);
            return getTemplate('file', {
              fileId: item.data,
              title: title,
              liAttrs: this.getHideItemAttr(item)
            });
          };

          NavbarNavGenerator.prototype.directiveToLi = function(item) {
            return getTemplate('directive', {
              directiveName: item.data,
              liAttrs: this.getHideItemAttr(item)
            });
          };

          NavbarNavGenerator.prototype.contactsToLi = function(item) {
            var contact, html, i, j, len, ref, ref1;
            if (!$cms.settings) {
              return '';
            }
            html = "";
            ref = $cms.settings.publicContacts || [];
            for (i = j = 0, len = ref.length; j < len; i = ++j) {
              contact = ref[i];
              if (item.data === 'tel' && contact.type !== 'tel') {
                continue;
              }
              if (item.data === 'email' && contact.type !== 'email') {
                continue;
              }
              if (item.data === 'tel-skype-whatsapp' && ((ref1 = contact.type) !== 'tel' && ref1 !== 'skype' && ref1 !== 'whatsapp-tel' && ref1 !== 'whatsapp')) {
                continue;
              }
              html += getTemplate('contact', {
                contactIndex: i,
                liAttrs: this.getHideItemAttr(item)
              });
            }
            return html;
          };

          NavbarNavGenerator.prototype.itemToLi = function(item) {
            switch (item.type) {
              case 'href':
                return this.hrefToLi(item);
              case 'ui-sref':
                return this.uisrefToLi(item);
              case 'ui-sref:generator:article-category':
                return this.uisrefGeneratorToLi(item, 'article-category');
              case 'ui-sref:app.articles.details':
                return this.uisrefToLi(item, 'app.articles.details');
              case 'ui-sref:app.galleries.details':
                return this.uisrefToLi(item, 'app.galleries.details');
              case 'ui-sref:anchor':
                return this.uisrefAnchorToLi(item);
              case 'file':
                return this.fileToLi(item);
              case 'action':
                return this.actionToLi(item);
              case 'directive':
                return this.directiveToLi(item);
              case 'contacts':
                return this.contactsToLi(item);
            }
            if (item.items && item.items.length) {
              return this.itemsToLi(item);
            }
            return '';
          };

          NavbarNavGenerator.prototype.itemsToLi = function(item) {
            var _item, itemsLi, j, len, ref, title;
            title = this.getTitle(item);
            itemsLi = "";
            ref = item.items;
            for (j = 0, len = ref.length; j < len; j++) {
              _item = ref[j];
              itemsLi += this.itemToLi(_item);
            }
            if (!itemsLi.length) {
              return '';
            }
            return getTemplate('dropdown', {
              itemsHtml: itemsLi,
              title: title,
              liAttrs: this.getHideItemAttr(item)
            });
          };

          NavbarNavGenerator.prototype.render = function() {
            var _item, items, itemsLi, j, len, position, ref;
            this.vars = [];
            this.html = {};
            ref = this.ngModel;
            for (position in ref) {
              items = ref[position];
              if (!items) {
                this.html[position] = null;
                continue;
              }
              itemsLi = "";
              for (j = 0, len = items.length; j < len; j++) {
                _item = items[j];
                itemsLi += this.itemToLi(_item);
              }
              this.html[position] = itemsLi;
            }
          };

          return NavbarNavGenerator;

        })(HtmlGenerator);
      }
    ];
    return this;
  });

}).call(this);
;
(function() {
  angular.module('ui.cms').directive('uicNavbarNav', ["$cms", "NavbarNavGenerator", function($cms, NavbarNavGenerator) {
    return {

      /**
      *   @ngdoc directive
      *   @name ui.cms.directive:uicNavbarNav
      *   @description директива, которая генерирует элементы для navbar-nav. Подробнее см. пример.
      *           Для редактирования использовать {@link ui.cms.editable.directive:uiceNavbarNavEditor uiceNavbarNavEditor}
      *
      *   @restrict E
      *   @param {object} ngModel (ссылка) модель
      *   @param {string=} ngModel.itemInnerTemplate текстовый шаблон элемента внутри пункта меню
      *   @param {array<Object>} ngModel.left элементы выравненные влево
      *   @param {array<Object>} ngModel.center элементы выравненные по-центру
      *   @param {array<Object>} ngModel.right элементы выравненные вправо
      *   @param {Object} ngModel.align[index] один элемент в align == ["left", "center", "right"],
      *           а index - номер объекта
      *   @param {string=} ngModel.align[index].type тип элемента в навбаре:
      *       <b>href</b> - обычная ссылка на внешний источник (напр. google.com);
      *       <b>ui-sref</b> - ссылка ui.router;
      *       <b>ui-sref:generator:article-category</b> - категория статей. Поле data - это id категории;
      *       <b>ui-sref:anchor</b> - ссылка на якорь во вьюхе
      *       <b>action</b> - специальное действие;
      *       <b>directive</b> - директива.
      *       пустая строка или отсутствующее свойство - тогда пункт меню содержит подпункты
      *   @param {object=} ngModel.align[index].attrs атрибуты html-тега в формате ключ-значение.
      *       <b>Не стоит добавлять атрибуты ng-*</b>
      *   @param {l10nObject=} ngModel.align[index].title заголовок пункта меню
      *   @param {string=} ngModel.align[index].data значение пункта меню
      *   @param {array<Object>=} ngModel.align[index].items подпункты меню
      *   @example
      *       <pre>
      *           $scope.nav = {
      *                right: [
      *                    {
      *                        type: 'href',
      *                        data: 'www.google.com',
      *                        title: {ru:"Ссылка 1", en:"Link 1"},
      *                        attrs: {target: '_blank'}
      *                    },
      *                    {type: 'ui-sref', data: 'app.index', title:{en: "Index"}},
      *                    {type: 'action', data:'someActionService'},
      *                    {type: 'directive', data: 'ui-lang-picker-for-navbar'},
      *                    {
      *                        title: {en:'Menu', ru:'Меню'},
      *                        items:[
      *                            {
      *                               type: 'href',
      *                               data: 'www.yandex.ru',
      *                               title: {ru:"Ссылка 2", en:"Link 2"}
      *                            },
      *                            {
      *                               type: 'ui-sref',
      *                               data: 'app.index',
      *                               title: {ru:"Основная страница", en:"Index page"}
      *                           }
      *                        ]
      *                    }
      *                ]
      *            }
      *       </pre>
      *       <pre>
      *           //- pug
      *           nav.navbar.navbar-default
      *               .container-fluid
      *                   .navbar-header
      *                        button.navbar-toggle.collapsed(ng-click="$cms.toggleNavbar()")
      *                            span.icon-bar
      *                            span.icon-bar
      *                            span.icon-bar
      *                        a.navbar-brand(href="/") Site
      *
      *                    .collapse.navbar-collapse(uib-collapse="$cms.isNavbarCollapsed")
      *                        uic-navbar-nav(ng-model="nav")
      *       </pre>
      *       <example module="DocExampleApp">
      *           <file name="script.js">
      *               mkDocCtrl('uicNavbarNavCtrl', {
      *                 nav: {
      *                   right: [
      *                        {
      *                            type: 'href',
      *                            data: 'www.google.com',
      *                            title: {ru:"Ссылка 1", en:"Link 1"},
      *                            attrs: {target: '_blank'}
      *                       },
      *                        {type: 'ui-sref', data: 'app.index', title:{en: "Index"}},
      *                        {type: 'action', data:'someActionService'},
      *                        {type: 'directive', data: 'ui-lang-picker-for-navbar'},
      *                        {
      *                            title: {en:'Menu', ru:'Меню'},
      *                            items:[
      *                                {
      *                                   type: 'href',
      *                                  data: 'www.yandex.ru',
      *                                   title: {ru:"Ссылка 2", en:"Link 2"}
      *                                },
      *                                {
      *                                   type: 'ui-sref',
      *                                   data: 'app.index',
      *                                   title: {ru:"Основная страница", en:"Index page"}
      *                               }
      *                           ]
      *                        }
      *                    ]
      *                   }
      *               });
      *           </file>
      *           <file name="index.html">
      *               <div ng-controller="uicNavbarNavCtrl">
      *                    <nav class="navbar navbar-default">
      *                      <div class="container-fluid">
      *                        <div class="navbar-header">
      *                          <button class="navbar-toggle collapsed" ng-click="$cms.toggleNavbar()"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button><a class="navbar-brand" href="/">Site</a>
      *                        </div>
      *                        <div class="collapse navbar-collapse" uib-collapse="$cms.isNavbarCollapsed">
      *                          <uic-navbar-nav ng-model="nav"></uic-navbar-nav>
      *                        </div>
      *                      </div>
      *                    </nav>
      *               </div>
      *           </file>
      *       </example>
       */
      restrict: 'E',
      scope: {
        ngModel: '=',
        cmsModelItem: '=?'
      },
      controller: ["$scope", "$element", "$langPicker", "$currentUser", "$state", "$document", "$timeout", function($scope, $element, $langPicker, $currentUser, $state, $document, $timeout) {

        /*
            вместо стандартного метода работы с шаблонами здесь сделана генерация
            html прямо в js коде. почему?
                1. ангулар не дает нормальной установки атрибутов у елементов,
                которые должны превращаться в директивы (типа lang-picker-for-navbar).
                2. этот метод не вызывает постоянной перерисовки шаблона,
                меню генерируется 1 раз, а потом каждый раз при изменении ngModel.
         */
        var generator, getNavbarHeight, isNavbarFixedTop, options;
        $scope.$cms = $cms;
        $scope.$langPicker = $langPicker;
        $scope.$currentUser = $currentUser;
        isNavbarFixedTop = function(el) {
          if (!el) {
            return false;
          }
          if (el.className.indexOf('navbar ') > -1) {
            return el.className.indexOf('navbar-fixed-top') > -1;
          }
          return isNavbarFixedTop(el.parentElement);
        };
        getNavbarHeight = function(el) {
          if (!el) {
            return 0;
          }
          if (el.className.indexOf('navbar ') > -1) {
            return el.offsetHeight;
          }
          return getNavbarHeight(el.parentElement);
        };
        options = {
          navbarFixedTop: isNavbarFixedTop($element[0]),
          navbarHeight: getNavbarHeight($element[0])
        };
        generator = new NavbarNavGenerator($scope.ngModel, options);
        $scope.toView = function(name, anchor) {
          var toAnchor;
          toAnchor = function(anchor) {
            var el, offset;
            el = document.getElementById(anchor);
            if (el) {
              offset = $element[0].offsetHeight || 0;
              if ($document.scrollToElementAnimated) {
                $document.scrollToElementAnimated(angular.element(el), offset);
              } else {
                $document.scrollToElement(angular.element(el), offset);
              }
            }
          };
          if ($state.current.name !== name) {
            $state.go(name).then(function() {
              toAnchor(anchor);
            });
          } else {
            toAnchor(anchor);
          }
        };
        $scope.updateGeneratedData = function() {
          generator.ngModel = $scope.ngModel;
          generator.render();
          $scope.generatedNav = generator.html;
          $scope.vars = generator.vars;
        };
        $scope.$watch('ngModel', $scope.updateGeneratedData, true);
        return $scope.$watch('$langPicker.currentLang', function(currentLang, oldValue) {
          if (currentLang !== oldValue) {
            $scope.generatedNav = {};
            $timeout($scope.updateGeneratedData, 100);
          }
        });
      }],
      template: ('/client/cms_app/_navbarDirectives/uicNavbarNav/uicNavbarNav.html', '<ul class="nav navbar-nav navbar-left" uic-bind-html="generatedNav.left" ng-if="generatedNav.left"></ul><ul class="nav navbar-nav navbar-center" uic-bind-html="generatedNav.center" ng-if="generatedNav.center"></ul><ul class="nav navbar-nav navbar-right" uic-bind-html="generatedNav.right" ng-if="generatedNav.right"></ul>' + '')
    };
  }]);

}).call(this);
;

/**
*   @ngdoc object
*   @name ui.cms.$constantsProvider
*   @header ui.cms.$constantsProvider
*   @description провайдер для определения констант. В основном используется django-командами для генерации разных схем и экспорта choices из полей моделей в бд.
 */

(function() {
  angular.module('ui.cms').provider('$constants', function() {
    var constants;
    constants = {};

    /**
    * @ngdoc method
    * @name ui.cms.$constantsProvider#set
    * @methodOf ui.cms.$constantsProvider
    * @param {string} name название константы. Желательно записывать ее в upper-case и с нижними подчеркиваниями. Напр: MY_VALUE
    * @param {object} value значение переменной. Может быть любым значением
    * @description Объявление константы в приложении
    * @example
    *   <pre>
    *       angular.module('myApp')
    *
    *       .config ($constantsProvider)->
    *           $constantsProvider.set 'CMS_USER', {
    *                userRole: [
    *                    {
    *                        value: 'a'
    *                        description: {
    *                            en: 'Admin'
    *                            ru: 'Администратор'
    *                        }
    *                    },{
    *                        value: 'u'
    *                        description: 'Default user'
    *                    }
    *                ]
    *            }
    *   </pre>
    *
     */
    this.set = function(name, value) {
      constants[name] = value;
    };

    /**
    *   @ngdoc service
    *   @name ui.cms.service:$constants
    *   @description сервис, который дает возможность работать с константами, которые были определены с помощью {@link ui.cms.$constantsProvider $constantsProvider}
     */
    this.$get = [
      'H', '$rootScope', '$filter', function(H, $rootScope, $filter) {
        var l10n, translate;
        l10n = $filter('l10n');
        translate = $filter('translate');
        return {

          /**
          *   @ngdoc property
          *   @name ui.cms.service:$constants#resolve
          *   @methodOf ui.cms.service:$constants
          *   @description
          *        возвращает значение по полному пути к переменой. Можно получать как всю переменную, так и свойства в ней
          *   @param {string} constantPath путь к переменной, определенной через {@link ui.cms.$constantsProvider $constantsProvider}
          *   @returns {any} значение
           */
          resolve: function(constantPath) {
            return angular.getValue(constants, constantPath);
          },
          update: function(constantPath, value) {
            angular.setValue(constants, constantPath, value);
            $rootScope.$emit('$constants.update', {
              path: constantPath,
              value: value
            });
          },

          /**
          *   @ngdoc property
          *   @name ui.cms.service:$constants#toText
          *   @methodOf ui.cms.service:$constants
          *   @description
          *        возвращает текстовую репрезентацию по полному пути к переменой. Если значение, которое хранится в полном пути - объект, то будет к этому объекту применен l10n фильтр, если строка - то translate фильтр
          *   @param {string} constantPath путь к переменной, определенной через {@link ui.cms.$constantsProvider $constantsProvider}
          *   @returns {string} текст
           */
          toText: function(constantPath) {
            var value;
            value = angular.getValue(constants, constantPath);
            if (!value) {
              return value;
            }
            if (typeof value === 'object') {
              return l10n(value);
            }
            if (typeof value === 'string') {
              return translate(value);
            }
            return value + '';
          }
        };
      }
    ];
    return this;
  });

}).call(this);
;

/**
*   @ngdoc object
*   @name ui.cms.$directiveOverrideProvider
*   @header ui.cms.$directiveOverrideProvider
*   @description провайдер для переопределения объявления директив.
*       Доступен только в блоках config.
*       <b>Приложение, которое "переопределяет" родительское приложение
*       должно быть подключено в html ПОСЛЕ родительского, и подключено к тегу html.body ПОСЛЕ родительского.</b>
*       Переопределять можно части директивы, а не ее всю.
*       Помните: стиль(если нужно) необходимо переопределять отдельно.
 */

(function() {
  angular.module('ui.cms').provider('$directiveOverride', ["$injector", function($injector) {
    var $compileProvider, $provide, fixDirectiveTemplateProp;
    $provide = $injector.get('$provide');
    $compileProvider = $injector.get('$compileProvider');
    fixDirectiveTemplateProp = function(oldDirective, newDirectiveObj) {
      if (oldDirective.hasOwnProperty('template') && oldDirective.hasOwnProperty('templateUrl')) {
        if (newDirectiveObj.hasOwnProperty('template')) {
          delete oldDirective.templateUrl;
        }
        if (newDirectiveObj.hasOwnProperty('templateUrl')) {
          delete oldDirective.template;
        }
      }
    };
    this.override = (function(_this) {
      return function(directiveName, directiveObject) {

        /**
        * @ngdoc method
        * @name ui.cms.$directiveOverrideProvider#override
        * @methodOf ui.cms.$directiveOverrideProvider
        * @param {string} directiveName название директивы,
        *       напр.: "mySpecialSmth". Директива предварительно должна быть объявлена!
        * @param {object} directiveObject описание директивы. Подробнее можно прочитать в
        *       {@link https://docs.angularjs.org/guide/directive }
        *       Необязательно переопределять все параметры.
        * @returns {self} ссылка на самого себя
        * @description Переопределяет <b>ранее зарегистрированную директиву</b> приложения.
        *       Не создает новых директив!
        * @example
        *   <pre>
        *       angular.module('OverrideAfterMyApp')
        *
        *       .config ($directiveOverrideProvider)->
        *           $directiveOverrideProvider
        *               .override('uiceLoginForm', {
        *                   templateUrl: 'MyNew.html'
        *               })
        *   </pre>
        *
         */
        $provide.decorator(directiveName + "Directive", ["$delegate", function($delegate) {
          var directive;
          directive = $delegate[0];
          if (angular.isFunction(directiveObject)) {
            directiveObject = directiveObject($delegate[0]);
          }
          directive = angular.extend(directive, directiveObject);
          fixDirectiveTemplateProp(directive, directiveObject);
          return $delegate;
        }]);
        return _this;
      };
    })(this);
    this.duplicate = function(directiveOriginalName, directiveNewName, directiveObject) {

      /**
      * @ngdoc method
      * @name ui.cms.$directiveOverrideProvider#duplicate
      * @methodOf ui.cms.$directiveOverrideProvider
      * @param {string} directiveOriginalName название оригинальной директивы,
      *       напр.: "mySpecialSmth".
      * @param {string} directiveNewName название директивы копия которой будет сделана
      *       с  directiveOriginalName,
      *       напр.: "mySpecialSmth2".
      * @param {object} directiveObject описание директивы. Подробнее можно прочитать в
      *       {@link https://docs.angularjs.org/guide/directive }
      *       Необязательно переопределять все параметры.
      * @returns {self} ссылка на самого себя
      * @description Создает копию <b>ранее зарегистрированной директивы</b> приложения
      *       с именем directiveNewName и переопределенными свойствами directiveObject
      *       <p><b>Особенности:</b></p>
      *       <ul>
      *           <li>Все что было объявлено за пределами controller и link оригинальной директивы
      *               будет расшарено между этими 2 директивами</li>
      *           <li>Стили от оригинальной директивы наследоваться не будут(нужно либо
      *               использовать общий класс, либо задавать новые стили)</li>
      *           <li>grunt и ngAnnotate не будут аннотировать controller и link(поэтому нельзя просто объявить
      *               <pre>
      *               controller: ($scope)->
      *                  console.log('hello')
      *               </pre>
      *               Нужно объявлять так:
      *               <pre>
      *               controller: ['$scope', ($scope)->
      *                   console.log('hello')
      *               ]
      *               </pre>
      *               )</li>
      *           <li>Tакже grunt и angular_inline_template не будут заменять ссылки templateUrl на текст шаблона.
      *               Поэтому нужно указывать весь шаблон в coffee файлах</li>
      *       </ul>
      * @example
      *   <pre>
      *       angular.module('SomeApp')
      *
      *       .config ($directiveOverrideProvider)->
      *           $directiveOverrideProvider
      *               .duplicate('uiceLoginForm', 'uiceLoginFormCustom' {
      *                   template: '<h1>Hello</h1>'
      *               })
      *   </pre>
      *
       */
      var directive, factory;
      factory = $injector.get(directiveOriginalName + "DirectiveProvider");
      directive = angular.copy(factory.$get()[0]);
      delete directive.name;
      if (angular.isFunction(directiveObject)) {
        directiveObject = directiveObject(factory.$get()[0]);
      }
      directive = angular.extend(directive, directiveObject);
      fixDirectiveTemplateProp(directive, directiveObject);
      $compileProvider.directive(directiveNewName, function() {
        return directive;
      });
      return this;
    };
    this.$get = [
      function() {
        return null;
      }
    ];
    return this;
  }]);

}).call(this);
;

/**
*   @ngdoc service
*   @name ui.cms.service:$cms
*   @description сервис, который содержит вспомогательные функции для работы с сайтом,
*       такие как: работа с настройками, разрешение полных путей к статическим файлам,
*       которые могут находиться в google cloud, amazon s3 или на локальном сервере,
*       установка заголовка для страницы, получение название текущего состояния из $state,
*       переменная в которой отражен bootstrap-размер дисплея ['xs', 'sm', 'md', 'lg'] и т.п.
 */

(function() {
  var cmsCustomSiteActions, cmsCustomSiteAnchors, cmsNavbarCustomDirectives, cmsService, getCmsService;

  cmsService = null;

  cmsNavbarCustomDirectives = {};

  cmsCustomSiteActions = {};

  cmsCustomSiteAnchors = {};

  getCmsService = function($q, $timeout, $injector) {
    var $currentUser, $document, $filter, $interpolate, $langPicker, $rootScope, $state, $uicIntegrations, $window, CmsService, CmsSettings, LoopBackAuth, SeoService, _Notification, brandFaviconUrl, brandLogoAltUrl, brandLogoUrl, gettextCatalog, j, l10nFilter, len, link, pageTitle, ref, ref1, translateFilter;
    $filter = $injector.get('$filter');
    $rootScope = $injector.get('$rootScope');
    $window = $injector.get('$window');
    $document = $injector.get('$document');
    $uicIntegrations = $injector.get('$uicIntegrations');
    $langPicker = $injector.get('$langPicker');
    $interpolate = $injector.get('$interpolate');
    LoopBackAuth = $injector.get('LoopBackAuth');
    CmsSettings = $injector.get('CmsSettings');
    gettextCatalog = $injector.get('gettextCatalog');
    l10nFilter = $filter('l10n');
    translateFilter = $filter('translate');
    pageTitle = null;
    $currentUser = null;
    $state = null;
    brandLogoUrl = null;
    brandLogoAltUrl = null;
    brandFaviconUrl = null;
    _Notification = null;
    if ($injector.has('Notification')) {
      _Notification = $injector.get('Notification');
    }
    ref = $document[0].head.getElementsByTagName('link');
    for (j = 0, len = ref.length; j < len; j++) {
      link = ref[j];
      if (!link.href) {
        continue;
      }
      if (link.rel === 'logo') {
        brandLogoUrl = link.href;
      }
      if (link.rel === 'logo-alt') {
        brandLogoAltUrl = link.href;
      }
      if ((ref1 = link.rel) === 'favicon' || ref1 === 'icon') {
        brandFaviconUrl = link.href;
      }
    }
    SeoService = (function() {
      function SeoService() {
        this.title = '';
        this.pageTitle = '';
        this.description = '';
        this.breadcrumbs = [];
        this.__forcedValues = {};
        this.__loadedIntegrations = {};
      }

      SeoService.prototype.getHtmlElements = (function() {
        var descriptionTag, titleTag;
        titleTag = document.head.getElementsByTagName('title')[0];
        descriptionTag = [].findByProperty.call(document.head.getElementsByTagName('meta'), 'name', 'description');
        if (!titleTag) {
          titleTag = document.createElement('title');
          document.head.appendChild(titleTag);
        }
        if (!descriptionTag) {
          descriptionTag = document.createElement('meta');
          descriptionTag.setAttribute('name', 'description');
          document.head.appendChild(descriptionTag);
        }
        if (titleTag.getAttribute('ng-bind') || titleTag.getAttribute('ngBind')) {
          console.warn("ui.cms: Please remove ng-bind in 'title' tag from head, so app could update properly this tags");
        }
        return function() {
          return {
            titleTag: titleTag,
            descriptionTag: descriptionTag,
            htmlTag: document.body.parentElement
          };
        };
      })();

      SeoService.prototype.setBreadcrumbs = function(breadcrumbs) {
        if (!angular.isArray(breadcrumbs)) {
          breadcrumbs = [];
        }
        this.breadcrumbs = breadcrumbs;
      };

      SeoService.prototype.forceSetValue = function(propertyType, value, stateName) {
        var base;
        if (propertyType !== 'title' && propertyType !== 'description' && propertyType !== 'pageTitle') {
          throw "$cms: seoService.forceSetValue - unknown propertyType '" + propertyType + "'";
        }
        if (!stateName) {
          $state = $state || $injector.get('$state');
          stateName = $state.current.name;
        }
        if (!stateName) {
          throw "$cms: seoService.forceSetValue - please provide state name!";
        }
        (base = this.__forcedValues)[stateName] || (base[stateName] = {});
        if (value === void 0) {
          delete this.__forcedValues[stateName][propertyType];
        } else {
          this.__forcedValues[stateName][propertyType] = value;
        }
      };

      SeoService.prototype.resolveAll = function() {
        var allDescriptions, allPageTitles, allTitles, b, description, getTranslatedValue, htmlElements, i, len1, m, mainSiteDescription, mainSiteTitle, ref2, title;
        htmlElements = this.getHtmlElements();
        allTitles = [];
        allDescriptions = [];
        allPageTitles = [];
        getTranslatedValue = function(value, stateName) {
          if (!value) {
            return null;
          } else if (angular.isString(value)) {
            value = translateFilter(value);
          } else if (angular.isObject(value)) {
            value = l10nFilter(value);
          } else if (angular.isFunction(value)) {
            value = value();
          } else if (value) {
            value += '';
          }
          return value;
        };
        ref2 = this.breadcrumbs || [];
        for (i = m = 0, len1 = ref2.length; m < len1; i = ++m) {
          b = ref2[i];
          if ((this.__forcedValues[b.stateName] || {}).hasOwnProperty('title')) {
            title = getTranslatedValue(this.__forcedValues[b.stateName].title);
          } else {
            title = getTranslatedValue(b.title);
          }
          if ((this.__forcedValues[b.stateName] || {}).hasOwnProperty('description')) {
            description = getTranslatedValue(this.__forcedValues[b.stateName].description);
          } else {
            description = getTranslatedValue(b.description);
          }
          if ((this.__forcedValues[b.stateName] || {}).hasOwnProperty('pageTitle')) {
            pageTitle = getTranslatedValue(this.__forcedValues[b.stateName].pageTitle);
          } else {
            pageTitle = getTranslatedValue(b.pageTitle);
          }
          if (title) {
            allTitles.push(title);
          }
          if (description) {
            allDescriptions.push(description);
          }
          if (pageTitle) {
            allPageTitles.push(pageTitle);
          }
        }
        this.title = '';
        this.pageTitle = '';
        this.description = '';
        mainSiteTitle = l10nFilter(cmsService.settings.siteTitle);
        mainSiteDescription = l10nFilter(cmsService.settings.siteDescription);
        if (allTitles.length) {
          this.title = allTitles[allTitles.length - 1];
        } else if (mainSiteTitle && allPageTitles.indexOf(mainSiteTitle) === -1) {
          this.title = allPageTitles.join(' — ');
          if (this.title) {
            this.title = this.title + ' | ' + mainSiteTitle;
          } else {
            this.title = mainSiteTitle;
          }
        } else {
          this.title = allPageTitles.join(' — ');
        }
        if (!this.title) {
          this.title = mainSiteTitle;
        }
        this.pageTitle = allPageTitles[allPageTitles.length - 1] || mainSiteTitle;
        this.description = allDescriptions[allDescriptions.length - 1] || mainSiteDescription;
        htmlElements.titleTag.textContent = this.title || '';
        htmlElements.descriptionTag.setAttribute('content', this.description || '');
        htmlElements.htmlTag.setAttribute('lang', $langPicker.currentLang);
      };

      SeoService.prototype.loadIntergations = function(integrations) {
        var $gdprStorage, gdprPermission, k, storageSettings, v;
        if (!integrations || angular.isEmpty(integrations)) {
          return;
        }
        $gdprStorage = $injector.get('$gdprStorage');
        storageSettings = $injector.get('storageSettings');
        gdprPermission = $gdprStorage.gdprPermission || {};
        storageSettings.thirdPartyServices = [];
        this.integrations || (this.integrations = {});
        for (k in integrations) {
          v = integrations[k];
          if ($uicIntegrations.isGdprRequired(k)) {
            storageSettings.thirdPartyServices.push(k);
          }
          if (this.__loadedIntegrations[k]) {
            continue;
          }
          if ($uicIntegrations.isGdprRequired(k) && !gdprPermission[k] && !$gdprStorage['*']) {
            continue;
          }
          this.integrations[k] = {
            active: true,
            onReady: $uicIntegrations.runService(k, v)
          };
          this.__loadedIntegrations[k] = true;
        }
      };

      return SeoService;

    })();
    CmsService = (function() {
      function CmsService() {
        this.seoService = new SeoService();
        this.LANG_MAP = {
          en: "English",
          ru: "Русский",
          cz: "Čeština",
          ua: "Українська",
          de: "Deutsch",
          es: "Español",
          da: "Dansk"
        };

        /**
        *   @ngdoc property
        *   @name ui.cms.service:$cms#isNavbarCollapsed
        *   @propertyOf ui.cms.service:$cms
        *   @description
        *       <label class="label type-hint type-hint-boolean">boolean</label>
        *       свернут ли навбар в кнопку для xs/sm дисплеев
         */
        this.isNavbarCollapsed = true;

        /**
        *   @ngdoc property
        *   @name ui.cms.service:$cms#screenSize
        *   @propertyOf ui.cms.service:$cms
        *   @description
        *       <label class="label type-hint type-hint-string">string</label>
        *       переменная в которой отражен bootstrap-размер дисплея ['xs', 'sm', 'md', 'lg']
         */
        this.screenSize = null;

        /**
        *   @ngdoc property
        *   @name ui.cms.service:$cms#settings
        *   @propertyOf ui.cms.service:$cms
        *   @description
        *       <label class="label type-hint type-hint-object">object</label>
        *       объект CmsSettings для конкретного bucket-a. Загружается либо
        *       через $bulk, либо в ручном режиме через $cms.loadSettings(). Сохранить
        *       можно с помощью $cms.saveSettings()
         */
        this.settings = {};

        /**
        *   @ngdoc property
        *   @name ui.cms.service:$cms#brand
        *   @propertyOf ui.cms.service:$cms
        *   @description
        *       <label class="label type-hint type-hint-object">object</label>
        *       объект-описание путей к файлам бренда: logoUrl, logoAltUrl (опционален), faviconUrl. Объект не предназначен для изменений!
         */
        this.brand = {
          logoUrl: brandLogoUrl,
          logoAltUrl: brandLogoAltUrl,
          faviconUrl: brandFaviconUrl
        };
      }

      CmsService.prototype.toggleNavbar = function() {

        /**
        *   @ngdoc property
        *   @name ui.cms.service:$cms#toggleNavbar
        *   @methodOf ui.cms.service:$cms
        *   @description
        *        переключает состояние навбара для xs/sm дисплеев
         */
        this.isNavbarCollapsed = !this.isNavbarCollapsed;
      };

      CmsService.prototype.collapseNavbar = function() {

        /**
        *   @ngdoc property
        *   @name ui.cms.service:$cms#collapseNavbar
        *   @methodOf ui.cms.service:$cms
        *   @description
        *        скрывает навбар для xs/sm дисплеев
         */
        this.isNavbarCollapsed = true;
      };

      CmsService.prototype.showNotification = function(template, context, notifyType) {

        /**
        *   @ngdoc property
        *   @name ui.cms.service:$cms#showNotification
        *   @methodOf ui.cms.service:$cms
        *   @description
        *        отрисовывает плашку с уведомлениями. Если доступен
        *       ui-notifications, то через Notification. если нет, то через
        *       стандартный js Notification.
        *   @param {string} template текстовая строка уведомления(может содержать ангуларовские выражения). Пропускается через gettextCatalog, т.е. она переводима
        *   @param {object} context контекст с которым будет скомпилирован шаблон template
        *   @param {string='success'} notifyType тип уведомления в ['success', 'primary', 'error', 'warning']
        *   @example
        *       <pre>
        *           $cms.showNotification("Hello, {name}!", {name: 'Kirill'}, 'primary')
        *       </pre>
         */
        var compiled, html, sendNotification;
        if (notifyType !== 'success' && notifyType !== 'primary' && notifyType !== 'error' && notifyType !== 'warning') {
          notifyType = 'success';
        }
        compiled = $interpolate(gettextCatalog.getString(template));
        html = compiled(context || {});
        if (!html || !html.length) {
          return;
        }
        if (_Notification) {
          _Notification[notifyType]({
            message: html
          });
        } else {
          sendNotification = function() {
            switch (notifyType) {
              case 'success':
                html = gettextCatalog.getString('Success') + ': ' + html;
                break;
              case 'error':
                html = gettextCatalog.getString('Error') + ': ' + html;
                break;
              case 'warning':
                html = gettextCatalog.getString('Warning') + ': ' + html;
            }
            new Notification(html);
          };
          if (Notification && Notification.permission === "granted") {
            sendNotification();
          } else {
            Notification.requestPermission(function(permission) {
              if (permission === 'granted') {
                sendNotification();
              }
            });
          }
        }
      };

      CmsService.prototype.detectIDLE = function(delay, options, cb) {

        /**
        *   @ngdoc property
        *   @name ui.cms.service:$cms#detectIDLE
        *   @methodOf ui.cms.service:$cms
        *   @description
        *       при бездействии пользователя delay миллисекунд вызывает <b>один раз</b> функцию cb
        *   @param {number} delay через сколько миллисекунд бездействия пользователя следует запускать функцию cb
        *   @param {object=} options  дополнительные опции. Укажите в options.eventTypes желаемые события которые будут считаться как проявление активности пользователя (По умолчанию: ['click', 'mousedown', 'touchend', 'keypress']).
        *           Укажите options.stopOnStateNameChange = false чтоб отключить завершение слежения при переходе пользователя на другую вьюху (по умолчанию - true - слежение отключится и функция cb никогда не будет вызвана если пользователь перешел в другую вьюху).
        *           <b>Если вы решили указать options.stopOnStateNameChange = false то вы обязаны сами завершить отслеживание активности пользователя с помощью фукнции rmDetectIDLE</b>
        *           <p><em>(default: {"eventTypes": ["click", "mousedown", "touchend", "keypress"], "stopOnStateNameChange": true})</em></p>
        *   @param {function=} cb функция, которая будет запущена
        *   @returns {function} rmDetectIDLE функция отключения слежения за пользователем. Используйте ее если хотите досрочно прекратить слежение или если указали options.stopOnStateNameChange = false
        *   @example
        *       <pre>
        *           $cms.detectIDLE 3000, ()->
        *               alert 'Hello IDLE!'
        *       </pre>
         */
        var resetTimeout, rmDetectIDLE, t;
        t = null;
        if (angular.isFunction(options)) {
          cb = options;
        }
        if (!angular.isFunction(cb)) {
          return angular.noop;
        }
        if (!angular.isObject(options)) {
          options = {};
        }
        if (!angular.isArray(options.eventTypes) || !options.eventTypes.length) {
          options.eventTypes = ['click', 'mousedown', 'touchend', 'keypress'];
        }
        if (!options.hasOwnProperty('stopOnStateNameChange')) {
          options.stopOnStateNameChange = true;
        }
        rmDetectIDLE = function() {
          if (t) {
            $timeout.cancel(t);
          }
          options.eventTypes.forEach(function(ev) {
            $document.off(ev, resetTimeout);
          });
        };
        resetTimeout = function() {
          if (t) {
            $timeout.cancel(t);
          }
          t = $timeout(function() {
            rmDetectIDLE();
            return cb();
          }, delay);
        };
        options.eventTypes.forEach(function(ev) {
          $document.on(ev, resetTimeout);
        });
        resetTimeout();
        return rmDetectIDLE;
      };

      CmsService.prototype.loadSettings = function(data) {
        var deferred, parseSettings;
        parseSettings = (function(_this) {
          return function(data) {
            var c, k, l, len1, m, ref2, ref3, v;
            _this.settings = data;
            _this.settings.cmsLangCodes = _this.settings.cmsLangCodes || [];
            _this.settings.cmsLangCodesJson = {};
            ref2 = _this.settings.cmsLangCodes;
            for (m = 0, len1 = ref2.length; m < len1; m++) {
              c = ref2[m];
              l = _this.LANG_MAP[c];
              _this.settings.cmsLangCodesJson[l] = c;
            }
            _this.settings.seo = _this.settings.seo || {};
            _this.settings.seo.social = _this.settings.seo.social || {};
            _this.settings.data = _this.settings.data || {};
            _this.settings.data.navbarConf = _this.settings.data.navbarConf || {};
            ref3 = $langPicker.languageList || {};
            for (k in ref3) {
              v = ref3[k];
              if (_this.settings.cmsLangCodes.indexOf(k) === -1) {
                delete $langPicker.languageList[k];
              }
            }
            if (data.currentUser) {
              $currentUser = $currentUser || $injector.get('$currentUser');
              $currentUser.setData(data.currentUser);
            }
            _this.seoService.resolveAll();
            _this.seoService.loadIntergations(_this.settings.integrations || {});
          };
        })(this);
        if (data) {
          return parseSettings(data);
        }
        deferred = $q.defer();
        CmsSettings.findOne({}, function(data) {
          parseSettings(data);
          deferred.resolve(data);
        }, function(data) {
          LoopBackAuth.clearUser();
          LoopBackAuth.clearStorage();
          CmsSettings.findOne({}, function(data) {
            parseSettings(data);
            return deferred.resolve(data);
          }, function(data) {
            return deferred.reject(data);
          });
        });
        return deferred.promise;
      };

      CmsService.prototype.getBootstrapScreenSize = function() {
        var body, display, el_a, el_j, len1, m, ref2, screen, size, style;
        screen = null;
        el_j = document.getElementById('getBootstrapScreenSize');
        if (!el_j) {
          el_a = angular.element('<div id="getBootstrapScreenSize"></div>');
          body = $document[0].body;
          angular.element(body).append(el_a);
          el_j = document.getElementById('getBootstrapScreenSize');
        }
        ref2 = ['xs', 'sm', 'md', 'lg'];
        for (m = 0, len1 = ref2.length; m < len1; m++) {
          size = ref2[m];
          el_j.className = el_j.className + (" hidden-" + size);
          style = window.getComputedStyle(el_j);
          display = style.getPropertyValue('display');
          if (display === 'none') {
            screen = size;
            el_j.className = '';
            break;
          }
        }
        return screen;
      };

      CmsService.prototype.resolveValueForScreenSize = function(obj) {

        /**
        *   @ngdoc property
        *   @name ui.cms.service:$cms#resolveValueForScreenSize
        *   @methodOf ui.cms.service:$cms
        *   @description
        *        возвращает значение из переданного словаря obj в
        *       зависимости от текущего bootstrap-размера экрана.
        *       Можно использовать для фильтров типа {@link ui.cms.filter:chunk chunk}
        *   @param {object} obj объект ключами которого есть текстовые
        *        названия размеров экрана
        *   @returns {any} значение которое соответствует ключу размера экрана
        *        или null.
        *   @example
        *       <pre>
        *           $cms.resolveValueForScreenSize({
        *              xs:1,
        *              sm:2,
        *           })
        *       </pre>
         */
        if (!this.screenSize) {
          this.screenSize = this.getBootstrapScreenSize();
        }
        if (obj.hasOwnProperty(this.screenSize)) {
          return obj[this.screenSize];
        }
        return null;
      };

      CmsService.prototype.runSiteAction = function(name) {

        /**
        *   @ngdoc property
        *   @name ui.cms.service:$cms#runSiteAction
        *   @methodOf ui.cms.service:$cms
        *   @description
        *       запускает действие зарегистрированное через $cmsProvider.registerCustomSiteAction
        *   @param {string} name название(id) действия
        *   @example
        *       <pre>
        *           //- pug
        *           button.btn.btn-default(ng-click="$cms.runSiteAction('myAction')")
        *       </pre>
         */
        var s;
        if (!cmsCustomSiteActions[name]) {
          throw "$cms: no such '" + cmsCustomSiteActions[name] + "' action in site actions";
        }
        if (!$injector.has(cmsCustomSiteActions[name].service) && !$injector.has(name)) {
          throw "$cms: no service for action '" + name + "'";
        }
        if ($injector.has(cmsCustomSiteActions[name].service)) {
          s = $injector.get(cmsCustomSiteActions[name].service);
        } else {
          s = $injector.get(name);
        }
        return s.run();
      };

      return CmsService;

    })();

    /*
        создаем синглтон $cms-сервиса
     */
    cmsService = new CmsService();
    cmsService.screenSize = cmsService.getBootstrapScreenSize();
    cmsService.navbarCustomDirectives = cmsNavbarCustomDirectives;
    cmsService.customSiteActions = cmsCustomSiteActions;
    cmsService.customSiteAnchors = cmsCustomSiteAnchors;
    angular.element($window).bind('resize', function() {
      $rootScope.$apply(function() {
        cmsService.screenSize = cmsService.getBootstrapScreenSize();
      });
    });
    $rootScope.$on('storage.gdpr.permissionUpdate', function() {
      $timeout(function() {
        cmsService.seoService.loadIntergations(cmsService.settings.integrations || {});
      }, 0);
    });
    return cmsService;
  };


  /**
  *   @ngdoc object
  *   @name ui.cms.$cmsProvider
  *   @header ui.cms.$cmsProvider
  *   @description провайдер для настроек $cms
   */

  angular.module('ui.cms').provider('$cms', function() {
    this.registerNavbarCustomDirective = function(name, config) {

      /**
      *   @ngdoc method
      *   @name ui.cms.$cmsProvider#registerNavbarCustomDirective
      *   @methodOf ui.cms.$cmsProvider
      *   @description
      *       регистрирует директиву, которую можно затолкать в навбар. Лучше всего его регистировать в
      *       override.cms, так описание этого действия будет доступно и в
      *       админке и на сайте. Но сама директива может находится в
      *       в IndexApp или в helpers. Директива обязательно должна указывать
      *       для себя replace:true и restrict:'A'
      *   @param {string} name название атрибута директивы
      *   @param {object} config конфигурация директивы
      *   @param {string=} config.description  текстовое описание(для админки)
      *   @example
      *       <pre>
      *           $cmsProvider.registerNavbarCustomDirective('my-directive', {
      *               description: 'My Fancy directive for navbar',
      *           })
      *       </pre>
      *       Где-то в IndexApp:
      *       <pre>
      *           .directive 'myDirective', ()->
      *               replace: true
      *               restrict: 'AE'
      *               controller: ($scope)->
      *                   $scope.v = 'world'
      *               template: '<hello>{{v}}</hello>'
      *       </pre>
       */
      cmsNavbarCustomDirectives[name] = config;
      return this;
    };
    this.registerCustomSiteAction = function(name, config) {

      /**
      *   @ngdoc method
      *   @name ui.cms.$cmsProvider#registerCustomSiteAction
      *   @methodOf ui.cms.$cmsProvider
      *   @description
      *       регистрирует действие для сайта. Лучше всего его регистировать в
      *       override.cms, так описание этого действия будет доступно и в
      *       админке и на сайте. Но сам сервис config.service должен находиться
      *       в IndexApp(не стоит засорять override.cms чем-то кроме переопределений)
      *   @param {string} name название(id) действия
      *   @param {object} config конфигурация действия
      *   @param {string=} config.description  текстовое описание(для админки)
      *   @param {string=} config.service текстовое название сервиса/фабрики в котором есть метод run (если не указано, то будет взято из name)
      *   @example
      *       Где-то в override.cms:
      *       <pre>
      *           $cmsProvider.registerCustomSiteAction('myAction',{
      *               description: 'My Fancy action',
      *               service: 'myService'
      *           })
      *       </pre>
      *       Где-то в IndexApp:
      *       <pre>
      *           .service 'myService', ()->
      *               &#64;run = ()->
      *                   console.log 'Hello!'
      *
      *               &#64;
      *       </pre>
      *       Теперь это действие можно использовать в кнопках, меню и т.п.:
      *       <pre>
      *           //- pug
      *           button.btn.btn-default(ng-click="$cms.runSiteAction('myAction')")
      *       </pre>
       */
      cmsCustomSiteActions[name] = config;
      return this;
    };
    this.registerCustomSiteAnchor = function(uiSref, config) {

      /**
      *   @ngdoc property
      *   @name ui.cms.$cmsProvider#registerCustomSiteAnchor
      *   @methodOf ui.cms.$cmsProvider
      *   @description
      *       регистрирует специальный anchor(якорь) на лендинг-странице для того, чтоб можно было указывать в navbar-е. Лучше всего его регистировать в
      *       override.cms, так описание будет доступно и в
      *       админке и на сайте. Но сам view и anchor должен находиться
      *       в IndexApp(не стоит засорять override.cms чем-то кроме переопределений)
      *   @param {string} uiSref название state-a (например: 'app.index'). Через '#' можно указывать anchor (или указать в config.anchor)
      *   @param {object} config конфигурация действия
      *   @param {string=} config.description  текстовое описание(для админки)
      *   @param {string=} config.anchor id якоря
      *   @example
      *       Где-то в override.cms:
      *       <pre>
      *           $cmsProvider.registerCustomSiteAnchor('app.index',{
      *               description: 'My Fancy anchor',
      *               anchor: 'test'
      *           })
      *           # равноценно:
      *           $cmsProvider.registerCustomSiteAnchor('app.index#test',{
      *               description: 'My Fancy anchor'
      *           })
      *       </pre>
       */
      config = config || {};
      if (config.anchor) {
        uiSref = uiSref + '#' + config.anchor;
      }
      cmsCustomSiteAnchors[uiSref] = config;
      return this;
    };
    this.$get = [
      '$q', '$timeout', '$injector', function($q, $timeout, $injector) {
        if (cmsService) {
          return cmsService;
        }
        return getCmsService($q, $timeout, $injector);
      }
    ];
    return this;
  });

}).call(this);
;

/**
*   @ngdoc service
*   @name ui.cms.service:$countries
*   @description сервис, который содержит вспомогательные функции для работы с
*       названиями и кодами стран
 */

(function() {
  angular.module('ui.cms').service('$countries', ["$injector", function($injector) {
    var $langPicker, cache, k, self, translations, v;
    self = this;
    $langPicker = null;
    translations = {
      ru: {
        gw: "Гвинея-Бисау",
        gt: "Гватемала",
        gr: "Греция",
        gq: "Экваториальная Гвинея",
        gy: "Гайана",
        ge: "Грузия",
        gd: "Гренада",
        gb: "Соединённое Королевство",
        ga: "Габон",
        gn: "Гвинея",
        gm: "Гамбия",
        gh: "Гана",
        tz: "Танзания",
        lc: "Сент-Люсия",
        la: "Лаосская Народно-Демократическая Республика",
        tv: "Тувалу",
        tw: "Тайвань",
        tt: "Тринидад и Тобаго",
        tr: "Турция",
        lk: "Шри-Ланка",
        li: "Лихтенштейн",
        lv: "Латвия",
        to: "Тонга",
        lt: "Литва",
        tm: "Туркменистан",
        lr: "Либерия",
        ls: "Лесото",
        th: "Таиланд",
        tg: "Того",
        td: "Чад",
        ly: "Ливия",
        "do": "Доминиканская республика",
        dm: "Доминика",
        dj: "Джибути",
        dk: "Дания",
        de: "Германия",
        ye: "Йемен",
        dz: "Алжир",
        uy: "Уругвай",
        qa: "Катар",
        zm: "Замбия",
        ee: "Эстония",
        eg: "Египет",
        za: "Южная Африка",
        ec: "Эквадор",
        sg: "Сингапур",
        mk: "Республика Македония",
        et: "Эфиопия",
        zw: "Зимбабве",
        es: "Испания",
        tj: "Таджикистан",
        ru: "Российская Федерация",
        rw: "Руанда",
        rs: "Сербия",
        it: "Италия",
        ro: "Румыния",
        bd: "Бангладеш",
        be: "Бельгия",
        bf: "Буркина-Фасо",
        bg: "Болгария",
        ba: "Босния и Герцеговина",
        bb: "Барбадос",
        jp: "Япония",
        bn: "Бруней",
        bo: "Боливия",
        bh: "Бахрейн",
        bi: "Бурунди",
        bj: "Бенин",
        bt: "Бутан",
        jm: "Ямайка",
        jo: "Иордания",
        ws: "Самоа",
        br: "Бразилия",
        bs: "Багамы",
        lb: "Ливан",
        by: "Беларусь",
        bz: "Белиз",
        tn: "Тунис",
        om: "Оман",
        my: "Малайзия",
        bw: "Ботсвана",
        xk: "Косово",
        ci: "Кот-д’Ивуар",
        ch: "Швейцария",
        co: "Колумбия",
        cm: "Камерун",
        cl: "Чили",
        ca: "Канада",
        cg: "Конго",
        cf: "Центрально-африканская республика",
        cd: "Демократическая Республика Конго",
        cz: "Чехия",
        cy: "Кипр",
        cr: "Коста-Рика",
        cv: "Кабо-Верде",
        cu: "Куба",
        ve: "Боливарианская Республика Венесуэла",
        ps: "Палестина",
        pw: "Палау",
        pt: "Португалия",
        py: "Парагвай",
        tl: "Восточный Тимор",
        pa: "Панама",
        pg: "Папуа — Новая Гвинея",
        pe: "Перу",
        pk: "Пакистан",
        ph: "Филиппины",
        pl: "Польша",
        hr: "Хорватия",
        ht: "Гаити",
        hu: "Венгрия",
        lu: "Люксембург",
        hn: "Гондурас",
        ao: "Ангола",
        me: "Черногория",
        md: "Республика Молдова",
        mg: "Мадагаскар",
        ma: "Марокко",
        mc: "Монако",
        uz: "Узбекистан",
        mm: "Мьянма",
        ml: "Мали",
        mn: "Монголия",
        mh: "Маршалловы острова",
        us: "Соединённые штаты",
        mu: "Маврикий",
        mt: "Мальта",
        mw: "Малави",
        mv: "Мальдивы",
        mr: "Мавритания",
        ug: "Уганда",
        ua: "Украина",
        mx: "Мексика",
        mz: "Мозамбик",
        va: "Государство-город Ватикан",
        vc: "Сент-Винсент и Гренадины",
        ae: "Объединённые Арабские Эмираты",
        ad: "Андорра",
        ag: "Антигуа и Барбуда",
        af: "Афганистан",
        iq: "Ирак",
        is: "Исландия",
        ir: "Иран",
        am: "Армения",
        al: "Албания",
        vn: "Вьетнам",
        sv: "Сальвадор",
        ar: "Аргентина",
        au: "Австралия",
        vu: "Вануату",
        "in": "Индия",
        az: "Азербайджан",
        ie: "Ирландия",
        id: "Индонезия",
        ni: "Никарагуа",
        nl: "Нидерланды",
        no: "Норвегия",
        il: "Израиль",
        na: "Намибия",
        ne: "Нигер",
        ng: "Нигерия",
        nz: "Новая Зеландия",
        np: "Непал",
        kw: "Кувейт",
        nr: "Науру",
        er: "Эритрея",
        fr: "Франция",
        kz: "Казахстан",
        fi: "Финляндия",
        fj: "Фиджи",
        fm: "Федеративные Штаты Микронезии",
        sz: "Свазиленд",
        sy: "Сирийская Арабская Республика",
        kg: "Кыргызстан",
        ke: "Кения",
        ss: "Южный Судан",
        sr: "Суринам",
        ki: "Кирибати",
        kh: "Камбоджа",
        kn: "Сент-Китс и Невис",
        km: "Коморские острова",
        st: "Сан-Томе и Принсипи",
        sk: "Словакия",
        kr: "Республика Корея",
        si: "Словения",
        kp: "Корейская Народно-Демократическая Республика",
        so: "Сомали",
        sn: "Сенегал",
        sm: "Сан-Марино",
        sl: "Сьерра-Леоне",
        sc: "Сейшельские Острова",
        sb: "Соломоновы Острова",
        sa: "Саудовская Аравия",
        at: "Австрия",
        se: "Швеция",
        sd: "Судан"
      },
      cz: {
        gw: "Guinea-Bissau",
        gt: "Guatemala",
        gr: "Řecko",
        gq: "Rovníková Guinea",
        gy: "Guyana",
        ge: "Gruzie",
        gd: "Grenada",
        gb: "Spojené království",
        ga: "Gabon",
        gn: "Guinea",
        gm: "Gambie",
        gh: "Ghana",
        tz: "Tanzanie, sjednocená republika",
        lc: "Svatá Lucie",
        la: "Laoská lidově demokratická republika",
        tv: "Tuvalu",
        tw: "Čínská republika",
        tt: "Trinidad a Tobago",
        tr: "Turecko",
        lk: "Šrí Lanka",
        li: "Lichtenštejnsko",
        lv: "Lotyšsko",
        to: "Tonga",
        lt: "Litva",
        tm: "Turkmenistán",
        lr: "Libérie",
        ls: "Lesotho",
        th: "Thajsko",
        tg: "Togo",
        td: "Čad",
        ly: "Libye",
        "do": "Dominikánská republika",
        dm: "Dominika",
        dj: "Džibutsko",
        dk: "Dánsko",
        de: "Německo",
        ye: "Jemen",
        dz: "Alžírsko",
        uy: "Uruguay",
        qa: "Katar",
        zm: "Zambie",
        ee: "Estonsko",
        eg: "Egypt",
        za: "Jihoafrická republika",
        ec: "Ekvádor",
        sg: "Singapur",
        mk: "Makedonie, republika",
        et: "Etiopie",
        zw: "Zimbabwe",
        es: "Španělsko",
        tj: "Tádžikistán",
        ru: "Ruská federace",
        rw: "Rwanda",
        rs: "Srbsko",
        it: "Itálie",
        ro: "Rumunsko",
        bd: "Bangladéš",
        be: "Belgie",
        bf: "Burkina Faso",
        bg: "Bulharsko",
        ba: "Bosna a Hercegovina",
        bb: "Barbados",
        jp: "Japonsko",
        bn: "Brunej",
        bo: "Mnohonárodní stát Bolívie",
        bh: "Bahrajn",
        bi: "Burundi",
        bj: "Benin",
        bt: "Bhútán",
        jm: "Jamajka",
        jo: "Jordánsko",
        ws: "Samoa",
        br: "Brazílie",
        bs: "Bahamy",
        lb: "Libanon",
        by: "Bělorusko",
        bz: "Belize",
        tn: "Tunisko",
        om: "Omán",
        my: "Malajsie",
        bw: "Botswana",
        xk: "Kosovská republika",
        ci: "Pobřeží slonoviny",
        ch: "Švýcarsko",
        co: "Kolumbie",
        cm: "Kamerun",
        cl: "Chile",
        ca: "Kanada",
        cg: "Kongo",
        cf: "Středoafrická republika",
        cd: "Konžská demokratická republika",
        cz: "Česká republika",
        cy: "Kypr",
        cr: "Kostarika",
        cv: "Kapverdy",
        cu: "Kuba",
        ve: "Bolívarovská republika Venezuela",
        ps: "Palestina",
        pw: "Palau",
        pt: "Portugalsko",
        py: "Paraguay",
        tl: "Východní Timor",
        pa: "Panama",
        pg: "Papua Nová Guinea",
        pe: "Peru",
        pk: "Pákistán",
        ph: "Filipíny",
        pl: "Polsko",
        hr: "Chorvatsko",
        ht: "Haiti",
        hu: "Maďarsko",
        lu: "Lucembursko",
        hn: "Honduras",
        ao: "Angola",
        me: "Černá Hora",
        md: "Moldavská republika",
        mg: "Madagaskar",
        ma: "Maroko",
        mc: "Monako",
        uz: "Uzbekistán",
        mm: "Myanmar",
        ml: "Mali",
        mn: "Mongolsko",
        mh: "Marshallovy ostrovy",
        us: "Spojené státy",
        mu: "Mauricius",
        mt: "Malta",
        mw: "Malawi",
        mv: "Maledivy",
        mr: "Mauritánie",
        ug: "Uganda",
        ua: "Ukrajina",
        mx: "Mexiko",
        mz: "Mosambik",
        va: "Svatý stolec (Vatikánský městský stát)",
        vc: "Svatý Vincenc a Grenadiny",
        ae: "Spojené arabské emiráty",
        ad: "Andorra",
        ag: "Antigua a Barbuda",
        af: "Afghánistán",
        iq: "Irák",
        is: "Island",
        ir: "Írán, islámská republika",
        am: "Arménie",
        al: "Albánie",
        vn: "Vietnam",
        sv: "Salvador",
        ar: "Argentina",
        au: "Austrálie",
        vu: "Vanuatu",
        "in": "Indie",
        az: "Ázerbájdžán",
        ie: "Irsko",
        id: "Indonésie",
        ni: "Nikaragua",
        nl: "Nizozemsko",
        no: "Norsko",
        il: "Izrael",
        na: "Namibie",
        ne: "Niger",
        ng: "Nigérie",
        nz: "Nový Zéland",
        np: "Nepál",
        kw: "Kuvajt",
        nr: "Nauru",
        er: "Eritrea",
        fr: "Francie",
        kz: "Kazachstán",
        fi: "Finsko",
        fj: "Fidži",
        fm: "Mikronésie, federativní státy",
        sz: "Svazijsko",
        sy: "Syrská arabská republika",
        kg: "Kyrgyzstán",
        ke: "Keňa",
        ss: "Jižní Súdán",
        sr: "Surinam",
        ki: "Kiribati",
        kh: "Kambodža",
        kn: "Svatý Kryštof a Nevis",
        km: "Komory",
        st: "Svatý Tomáš a Princův ostrov",
        sk: "Slovensko",
        kr: "Korea, republika",
        si: "Slovinsko",
        kp: "Korea, lidově demokratická republika",
        so: "Somálsko",
        sn: "Senegal",
        sm: "San Marino",
        sl: "Sierra Leone",
        sc: "Seychely",
        sb: "Šalamounovy ostrovy",
        sa: "Saúdská Arábie",
        at: "Rakousko",
        se: "Švédsko",
        sd: "Súdán"
      }
    };

    /**
    *   @ngdoc property
    *   @name ui.cms.service:$countries#europe
    *   @propertyOf ui.cms.service:$countries
    *   @description
    *       <label class="label type-hint type-hint-array">array</label>
    *       список стран в европе в виде массива (только английские названия стран):
    *       <pre>
    *           $countries.europe = [
    *               ["al","Albania"]
    *               ["ad","Andorra"]
    *               ["am","Armenia"]
    *               ...
    *           ]
    *       </pre>
     */
    this.europe = [["al", "Albania"], ["ad", "Andorra"], ["am", "Armenia"], ["at", "Austria"], ["az", "Azerbaijan"], ["by", "Belarus"], ["be", "Belgium"], ["ba", "Bosnia and Herzegovina"], ["bg", "Bulgaria"], ["hr", "Croatia"], ["cy", "Cyprus"], ["cz", "Czech Republic"], ["dk", "Denmark"], ["ee", "Estonia"], ["fi", "Finland"], ["fr", "France"], ["ge", "Georgia"], ["de", "Germany"], ["gr", "Greece"], ["hu", "Hungary"], ["is", "Iceland"], ["ie", "Ireland"], ["it", "Italy"], ["kz", "Kazakhstan"], ["xk", "Kosovo"], ["lv", "Latvia"], ["li", "Liechtenstein"], ["lt", "Lithuania"], ["lu", "Luxembourg"], ["mk", "Macedonia"], ["mt", "Malta"], ["md", "Moldova"], ["mc", "Monaco"], ["me", "Montenegro"], ["nl", "Netherlands"], ["no", "Norway"], ["pl", "Poland"], ["pt", "Portugal"], ["ro", "Romania"], ["ru", "Russia"], ["sm", "San Marino"], ["rs", "Serbia"], ["sk", "Slovakia"], ["si", "Slovenia"], ["es", "Spain"], ["se", "Sweden"], ["ch", "Switzerland"], ["tr", "Turkey"], ["ua", "Ukraine"], ["gb", "United Kingdom"], ["va", "Vatican City"]];

    /**
    *   @ngdoc property
    *   @name ui.cms.service:$countries#asia
    *   @propertyOf ui.cms.service:$countries
    *   @description
    *       <label class="label type-hint type-hint-array">array</label>
    *       список стран в азии в виде массива (только английские названия стран):
    *       <pre>
    *           $countries.asia = [
    *               ["af", "Afghanistan"]
    *               ["am", "Armenia"]
    *               ["az", "Azerbaijan"]
    *               ...
    *           ]
    *       </pre>
     */
    this.asia = [["af", "Afghanistan"], ["am", "Armenia"], ["az", "Azerbaijan"], ["bh", "Bahrain"], ["bd", "Bangladesh"], ["bt", "Bhutan"], ["bn", "Brunei"], ["kh", "Cambodia"], ["tw", "China"], ["cy", "Cyprus"], ["ge", "Georgia"], ["in", "India"], ["id", "Indonesia"], ["ir", "Iran"], ["iq", "Iraq"], ["il", "Israel"], ["jp", "Japan"], ["jo", "Jordan"], ["kz", "Kazakhstan"], ["kw", "Kuwait"], ["kg", "Kyrgyzstan"], ["la", "Laos"], ["lb", "Lebanon"], ["my", "Malaysia"], ["mv", "Maldives"], ["mn", "Mongolia"], ["mm", "Myanmar (Burma)"], ["np", "Nepal"], ["kp", "North Korea"], ["om", "Oman"], ["pk", "Pakistan"], ["ps", "Palestine"], ["ph", "Philippines"], ["qa", "Qatar"], ["ru", "Russia"], ["sa", "Saudi Arabia"], ["sg", "Singapore"], ["kr", "South Korea"], ["lk", "Sri Lanka"], ["sy", "Syria"], ["tw", "Taiwan"], ["tj", "Tajikistan"], ["th", "Thailand"], ["tl", "Timor-Leste"], ["tr", "Turkey"], ["tm", "Turkmenistan"], ["ae", "United Arab Emirates (UAE)"], ["uz", "Uzbekistan"], ["vn", "Vietnam"], ["ye", "Yemen"]];

    /**
    *   @ngdoc property
    *   @name ui.cms.service:$countries#africa
    *   @propertyOf ui.cms.service:$countries
    *   @description
    *       <label class="label type-hint type-hint-array">array</label>
    *       список стран африки в виде массива (только английские названия стран):
    *       <pre>
    *           $countries.africa = [
    *               ["dz", "Algeria"]
    *               ["ao", "Angola"]
    *               ["bj", "Benin"]
    *               ...
    *           ]
    *       </pre>
     */
    this.africa = [["dz", "Algeria"], ["ao", "Angola"], ["bj", "Benin"], ["bw", "Botswana"], ["bf", "Burkina Faso"], ["bi", "Burundi"], ["cv", "Cabo Verde"], ["cm", "Cameroon"], ["cf", "Central African Republic (CAR)"], ["td", "Chad"], ["km", "Comoros"], ["ci", "Cote d'Ivoire"], ["cd", "Democratic Republic of the Congo"], ["dj", "Djibouti"], ["eg", "Egypt"], ["gq", "Equatorial Guinea"], ["er", "Eritrea"], ["et", "Ethiopia"], ["ga", "Gabon"], ["gm", "Gambia"], ["gh", "Ghana"], ["gn", "Guinea"], ["gw", "Guinea-Bissau"], ["ke", "Kenya"], ["ls", "Lesotho"], ["lr", "Liberia"], ["ly", "Libya"], ["mg", "Madagascar"], ["mw", "Malawi"], ["ml", "Mali"], ["mr", "Mauritania"], ["mu", "Mauritius"], ["ma", "Morocco"], ["mz", "Mozambique"], ["na", "Namibia"], ["ne", "Niger"], ["ng", "Nigeria"], ["cg", "Republic of the Congo"], ["rw", "Rwanda"], ["st", "Sao Tome and Principe"], ["sn", "Senegal"], ["sc", "Seychelles"], ["sl", "Sierra Leone"], ["so", "Somalia"], ["za", "South Africa"], ["ss", "South Sudan"], ["sd", "Sudan"], ["sz", "Swaziland"], ["tz", "Tanzania"], ["tg", "Togo"], ["tn", "Tunisia"], ["ug", "Uganda"], ["zm", "Zambia"], ["zw", "Zimbabwe"]];

    /**
    *   @ngdoc property
    *   @name ui.cms.service:$countries#northAmerica
    *   @propertyOf ui.cms.service:$countries
    *   @description
    *       <label class="label type-hint type-hint-array">array</label>
    *       список стран северной америки в виде массива (только английские названия стран):
    *       <pre>
    *           $countries.northAmerica = [
    *               ["ag", "Antigua and Barbuda"]
    *               ["bs", "Bahamas"]
    *               ["bb", "Barbados"]
    *               ...
    *           ]
    *       </pre>
     */
    this.northAmerica = [["ag", "Antigua and Barbuda"], ["bs", "Bahamas"], ["bb", "Barbados"], ["bz", "Belize"], ["ca", "Canada"], ["cr", "Costa Rica"], ["cu", "Cuba"], ["dm", "Dominica"], ["do", "Dominican Republic"], ["sv", "El Salvador"], ["gd", "Grenada"], ["gt", "Guatemala"], ["ht", "Haiti"], ["hn", "Honduras"], ["jm", "Jamaica"], ["mx", "Mexico"], ["ni", "Nicaragua"], ["pa", "Panama"], ["kn", "Saint Kitts and Nevis"], ["lc", "Saint Lucia"], ["vc", "Saint Vincent and the Grenadines"], ["tt", "Trinidad and Tobago"], ["us", "United States of America (USA)"]];

    /**
    *   @ngdoc property
    *   @name ui.cms.service:$countries#southAmerica
    *   @propertyOf ui.cms.service:$countries
    *   @description
    *       <label class="label type-hint type-hint-array">array</label>
    *       список стран южной америки в виде массива (только английские названия стран):
    *       <pre>
    *           $countries.southAmerica = [
    *               ["ag", "Antigua and Barbuda"]
    *               ["bs", "Bahamas"]
    *               ["bb", "Barbados"]
    *               ...
    *           ]
    *       </pre>
     */
    this.southAmerica = [["ar", "Argentina"], ["bo", "Bolivia"], ["br", "Brazil"], ["cl", "Chile"], ["co", "Colombia"], ["ec", "Ecuador"], ["gy", "Guyana"], ["py", "Paraguay"], ["pe", "Peru"], ["sr", "Suriname"], ["uy", "Uruguay"], ["ve", "Venezuela"]];

    /**
    *   @ngdoc property
    *   @name ui.cms.service:$countries#southAmerica
    *   @propertyOf ui.cms.service:$countries
    *   @description
    *       <label class="label type-hint type-hint-array">array</label>
    *       список стран автсралии и океании в виде массива (только английские названия стран):
    *       <pre>
    *           $countries.australiaAndOceania = [
    *               ["au", "Australia"]
    *               ["fj", "Fiji"]
    *               ["ki", "Kiribati"]
    *               ...
    *           ]
    *       </pre>
     */
    this.australiaAndOceania = [["au", "Australia"], ["fj", "Fiji"], ["ki", "Kiribati"], ["mh", "Marshall Islands"], ["fm", "Micronesia"], ["nr", "Nauru"], ["nz", "New Zealand"], ["pw", "Palau"], ["pg", "Papua New Guinea"], ["ws", "Samoa"], ["sb", "Solomon Islands"], ["to", "Tonga"], ["tv", "Tuvalu"], ["vu", "Vanuatu"]];
    cache = {};
    ['europe', 'asia', 'africa', 'northAmerica', 'southAmerica', 'australiaAndOceania'].forEach(function(s) {
      self[s].forEach(function(c) {
        cache[c[0]] = c[1];
      });
    });

    /**
    *   @ngdoc property
    *   @name ui.cms.service:$countries#world
    *   @propertyOf ui.cms.service:$countries
    *   @description
    *       <label class="label type-hint type-hint-array">array</label>
    *       список стран всего мира.
    *       <b> На данный момент тут НЕ включены зависимые территории!</b>
    *       <pre>
    *           $countries.world = [
    *               ["al","Albania"]
    *               ["ad","Andorra"]
    *               ["at","Austria"]
    *               ...
    *           ]
    *       </pre>
     */
    this.world = (function() {
      var results;
      results = [];
      for (k in cache) {
        v = cache[k];
        results.push([k, v]);
      }
      return results;
    })();
    this.world.sort(function(a, b) {
      if (a[1] < b[1]) {
        return -1;
      }
      if (a[1] > b[1]) {
        return 1;
      }
      return 0;
    });

    /**
    *   @ngdoc property
    *   @name ui.cms.service:$countries#codeToText
    *   @methodOf ui.cms.service:$countries
    *   @description
    *        возвращает название страны по ее 2-х значному коду, или строку
    *       "Invalid country code"
    *   @param {string} code код страны. Например: 'ua', 'de'
    *   @param {string=} forceLang язык, на котором нужно отобразить название страны. Если не указано, то будет взят текущий язык пользователя.
    *   @returns {string} название страны
     */
    this.codeToText = function(code, forceLang) {
      if (!$langPicker) {
        $langPicker = $injector.get('$langPicker');
      }
      forceLang = forceLang || $langPicker.currentLang;
      code = ((code || '') + '').toLowerCase();
      if (forceLang === 'en') {
        return cache[code] || 'Invalid country code';
      }
      if (translations[forceLang] && translations[forceLang][code]) {
        return translations[forceLang][code];
      }
      forceLang = $langPicker.currentLang;
      if (translations[forceLang] && translations[forceLang][code]) {
        return translations[forceLang][code];
      }
      return cache[code] || 'Invalid country code';
    };

    /**
    *   @ngdoc property
    *   @name ui.cms.service:$countries#getList
    *   @methodOf ui.cms.service:$countries
    *   @description
    *        возвращает отсортированные списки стран, согласно названию континетов. По умолчанию вернет весь мир.
    *   @param {string=} p1 название. Напр: world. Варианты: 'europe', 'asia', 'africa', 'northAmerica', 'southAmerica', 'australiaAndOceania', 'world'
    *   @param {string=} p2 название. Напр: world. Варианты: 'europe', 'asia', 'africa', 'northAmerica', 'southAmerica', 'australiaAndOceania', 'world'
    *   @param {string=} pN название. Напр: world. Варианты: 'europe', 'asia', 'africa', 'northAmerica', 'southAmerica', 'australiaAndOceania', 'world'
    *   @returns {array} список стран
    *   @example
    *       <pre>
    *           > $countries.getList('australiaAndOceania', 'asia')
    *           [
    *               ["au", "Australia"]
    *               ["fj", "Fiji"]
    *               ["ki", "Kiribati"]
    *               ...
    *           ]
    *       </pre>
     */
    this.getList = function() {
      var a, allowed, i, in_list, len, list;
      allowed = [];
      for (i = 0, len = arguments.length; i < len; i++) {
        a = arguments[i];
        if (!allowed.includes(a)) {
          if (['europe', 'asia', 'africa', 'northAmerica', 'southAmerica', 'australiaAndOceania', 'world'].includes(a)) {
            allowed.push(a);
          }
        }
      }
      if (!allowed.length) {
        allowed = ['world'];
      }
      if (allowed.includes('world')) {
        allowed = ['world'];
      }
      list = [];
      in_list = [];
      allowed.forEach(function(a) {
        (self[a] || []).forEach(function(c) {
          if (in_list.includes(c[0])) {
            return;
          }
          in_list.push(c[0]);
          list.push(c);
        });
      });
      list.sort(function(a, b) {
        if (a[1] < b[1]) {
          return -1;
        }
        if (a[1] > b[1]) {
          return 1;
        }
        return 0;
      });
      return list;
    };
    return this;
  }]);

}).call(this);
;

/**
*   @ngdoc service
*   @name ui.cms.service:$currentUser
*   @description сервис, который содержит вспомогательные функции для работы с текущим залогиненным пользователем
 */

(function() {
  var onChangeCallbacks,
    indexOf = [].indexOf || function(item) { for (var i = 0, l = this.length; i < l; i++) { if (i in this && this[i] === item) return i; } return -1; };

  onChangeCallbacks = {};

  angular.module('ui.cms').config(["$provide", function($provide) {
    return $provide.decorator('LoopBackAuth', ["$delegate", "$injector", function($delegate, $injector) {
      var $currentUser, LoopBackAuth;
      $currentUser = $injector.get('$currentUser');
      LoopBackAuth = $delegate;
      LoopBackAuth.__proto__.origSetUser = LoopBackAuth.__proto__.setUser;
      LoopBackAuth.__proto__.setUser = function(accessTokenId, userId, userData) {
        this.origSetUser(accessTokenId, userId, userData);
        $currentUser.setData({
          id: userId
        });
      };
      LoopBackAuth.__proto__.origClearUser = LoopBackAuth.__proto__.clearUser;
      LoopBackAuth.__proto__.clearUser = function() {
        var $bulk;
        this.origClearUser();
        $currentUser.clearData();
        if ($injector.has('$bulk')) {
          $bulk = $injector.get('$bulk');
          $bulk.reset();
        }
      };
      return LoopBackAuth;
    }]);
  }]).service('$currentUser', ["$injector", function($injector) {
    var $state, H, currentUserPermissions, self, setData;
    $state = null;

    /**
    *   @ngdoc property
    *   @name ui.cms.service:$currentUser#id
    *   @propertyOf ui.cms.service:$currentUser
    *   @description
    *       <label class="label type-hint type-hint-number">integer</label> или <label class="label type-hint type-hint-string">string</label>
    *        id текущего залогиненного пользователя. 0 - значит пользователя нет
     */
    this.id = 0;

    /**
    *   @ngdoc property
    *   @name ui.cms.service:$currentUser#username
    *   @propertyOf ui.cms.service:$currentUser
    *   @description
    *       <label class="label type-hint type-hint-string">string</label>
    *       username текущего залогиненного пользователя. '' - значит пользователя нет
     */
    this.username = '';

    /**
    *   @ngdoc property
    *   @name ui.cms.service:$currentUser#userRole
    *   @propertyOf ui.cms.service:$currentUser
    *   @description
    *       <label class="label type-hint type-hint-string">string</label>
    *       роль пользователя текущего залогиненного пользователя из бд
     */
    this.userRole = '';

    /**
    *   @ngdoc property
    *   @name ui.cms.service:$currentUser#avatar
    *   @propertyOf ui.cms.service:$currentUser
    *   @description
    *       <label class="label type-hint type-hint-object">object</label>
    *       полные пути к аватаркам пользователя. Прекрасно работает с директивой
    *       {@link ui.cms.editable.directive:uiceAvatarManager uice-avatar-manager}
    *       при загрузке нового изображения url изменится и в avatar, а также
    *       в url добавляется параметр t чтоб браузер принудительно перезагрузил фото,
    *       если url совпадают.
    *       Содержит аватарки размером 70x70 и 350х350
    *       <pre>
    *           > console.log($currentUser.avatar)
    *           {
    *               xs: '/full/path/to/img.70x70.jpg?t=1500626146585'
    *               sm: '/full/path/to/img.350x350.jpg?t=1500626146585'
    *           }
    *       </pre>
     */
    this.avatar = {};

    /**
    *   @ngdoc property
    *   @name ui.cms.service:$currentUser#data
    *   @propertyOf ui.cms.service:$currentUser
    *   @description
    *       <label class="label type-hint type-hint-object">object</label>
    *       Объект модели CmsUser
     */
    this.data = {};
    currentUserPermissions = {};
    self = this;
    H = null;
    setData = function() {
      var cb, i, k, len, p, ref, t;
      self.username = self.data.username || '';
      self.userRole = self.data.userRole || '';
      self.id = self.data.id || 0;
      if (!self.data.mainImg) {
        self.avatar = {};
      } else {
        t = new Date().getTime();
        H = H || $injector.get('H');
        self.avatar = {
          xs: H.path.resolveL10nFileThumbPath(self.data.mainImg, '70') + '?t=' + t,
          sm: H.path.resolveL10nFileThumbPath(self.data.mainImg, '350') + '?t=' + t
        };
      }
      if (self.userRole) {
        document.body.setAttribute('user-role', self.userRole.replaceAll('*', 'any'));
      } else {
        document.body.removeAttribute('user-role');
      }
      currentUserPermissions = {};
      ref = self.data.allPermissions || [];
      for (i = 0, len = ref.length; i < len; i++) {
        p = ref[i];
        currentUserPermissions[p.toLowerCase()] = true;
      }
      for (k in onChangeCallbacks) {
        cb = onChangeCallbacks[k];
        if (angular.isFunction(cb)) {
          cb();
        }
      }
    };

    /**
    *   @ngdoc property
    *   @name ui.cms.service:$currentUser#isAdmin
    *   @methodOf ui.cms.service:$currentUser
    *   @description
    *        является ли текущий пользователь админом
    *   @returns {boolean} true/false
     */
    this.isAdmin = function() {
      return this.userRole === 'a';
    };

    /**
    *   @ngdoc property
    *   @name ui.cms.service:$currentUser#isUserRole
    *   @methodOf ui.cms.service:$currentUser
    *   @description
    *        находится ли пользователь с текущей ролью в списке переданных параметров
    *   @returns {boolean} true/false
    *   @example
    *       <pre>
    *           > $currentUser.setData({id: 1, userRole: 'editor'})
    *           > isInRole = $currentUser.isUserRole('admin', 'editor')
    *           > console.log(isInRole)
    *           true
    *       </pre>
     */
    this.isUserRole = function() {
      var ref;
      return ref = this.userRole, indexOf.call(arguments, ref) >= 0;
    };

    /**
    *   @ngdoc property
    *   @name ui.cms.service:$currentUser#hasPermission
    *   @methodOf ui.cms.service:$currentUser
    *   @description
    *        имеет ли пользователь доступ согласно переданному разрешению.
    *        Внимание: проверяются права, которые можно получить через CmsUser.get_all_permissions() в django-коде
    *       <br/>
    *       Функция сделана для того, чтоб можно было гибко назначать и управлять разрешениями пользователя из питоновского кода джанги
    *       и определять на клиенте.
    *   @returns {boolean} true/false
    *   @example
    *       <pre>
    *
    *           .controller 'MyCtrl', ($currentUser)->
    *               $currentUser.setData({id: 1, userRole: 'editor', permissions: ['my_app.edit_post']})
    *               permission = $currentUser.hasPermission('my_app.edit_post')
    *               console.log(permission)
    *               permission = $currentUser.hasPermission('my_app.create_post')
    *               console.log(permission)
    *               $currentUser.setData({id: 1, userRole: 'a', permissions: ['my_app.edit_post']})
    *               permission = $currentUser.hasPermission('my_app.create_post')
    *               console.log(permission)
    *       </pre>
    *       Вывод:
    *       <pre>
    *           true
    *           false
    *           true // админы всегда true
    *       </pre>
     */
    this.hasPermission = function(permission) {
      var k;
      if (permission.indexOf('*') === 0) {
        permission = (permission.split('.')[1] || '').toLowerCase();
        for (k in currentUserPermissions) {
          if (k.indexOf('.' + permission) > -1) {
            return true;
          }
        }
      }
      if (currentUserPermissions[permission.toLowerCase()]) {
        return true;
      }
      return this.isAdmin();
    };

    /**
    *   @ngdoc property
    *   @name ui.cms.service:$currentUser#resolvePermissionsString
    *   @methodOf ui.cms.service:$currentUser
    *   @description
    *        имеет ли пользователь доступ согласно переданным условиям.
    *        На вход в функцию можно подавать строку в формате: "<условие1> || <условие2> || <условие3>". Как условия можно подставить следующие значения:
    *       <ul>
    *           <li>isAdmin()</li>
    *           <li>isUserRole('your-user-role')</li>
    *           <li>django_app.permission_codename</li>
    *           <li>false</li>
    *           <li>true</li>
    *       </ul>
    *        Внимание: проверяются права, которые можно получить через CmsUser.get_all_permissions() в django-коде
    *       <br/>
    *       Функция сделана для того, чтоб можно было гибко назначать и управлять разрешениями пользователя из питоновского кода джанги
    *       и определять на клиенте.
    *   @returns {boolean} true/false
    *   @example
    *       <pre>
    *
    *           .controller 'MyCtrl', ($currentUser)->
    *               $currentUser.setData({id: 1, userRole: 'editor', permissions: ['my_app.edit_post']})
    *               permission = $currentUser.resolvePermissionsString('my_app.edit_post || isAdmin()')
    *               console.log(permission)
    *               permission = $currentUser.resolvePermissionsString('my_app.create_post || isAdmin()')
    *               console.log(permission)
    *               permission = $currentUser.resolvePermissionsString('my_app.create_post || isUserRole("editor")')
    *               console.log(permission)
    *               $currentUser.setData({id: 1, userRole: 'a', permissions: ['my_app.edit_post']})
    *               permission = $currentUser.resolvePermissionsString('my_app.create_post || isAdmin()')
    *               console.log(permission)
    *               permission = $currentUser.resolvePermissionsString('!isAdmin()')
    *               console.log(permission)
    *       </pre>
    *       Вывод:
    *       <pre>
    *           true
    *           false
    *           true
    *           true // админы всегда true
    *           false
    *       </pre>
     */
    this.resolvePermissionsString = function(permissionsString) {
      var i, len, notPart, permission, permissions, resolveWithNotPart, role;
      permissions = (permissionsString + '').split('||').map(function(value) {
        return value.trim();
      }).filter(function(value) {
        return !!value;
      });
      if (!permissions.length) {
        return true;
      }
      if (permissions.length === 1) {
        if (permissions[0] === 'false') {
          return false;
        }
        if (permissions[0] === 'true') {
          return true;
        }
      }
      notPart = false;
      resolveWithNotPart = function(value) {
        if (notPart) {
          return !value;
        }
        return value;
      };
      for (i = 0, len = permissions.length; i < len; i++) {
        permission = permissions[i];
        notPart = false;
        if (permission[0] === '!') {
          permission = permission.substr(1).trim();
          notPart = true;
        }
        if (permission === 'isAdmin()' && resolveWithNotPart(self.isAdmin())) {
          return true;
        }
        if (permission.indexOf('isUserRole(') === 0) {
          role = permission.split('(')[0].split(')')[0].replaceAll('"', '').replaceAll("'", '');
          if (resolveWithNotPart(this.isUserRole(role))) {
            return true;
          }
        }
        if (resolveWithNotPart(this.hasPermission(permission.toLowerCase()))) {
          return true;
        }
      }
      return false;
    };

    /**
    *   @ngdoc property
    *   @name ui.cms.service:$currentUser#setData
    *   @methodOf ui.cms.service:$currentUser
    *   @param {object} data данные о пользователе. Объект модели CmsUser
    *   @description
    *        принудительное обновление информации о пользовате. Будут обновлены
    *       поля у $currentUser: id, username, avatar, userRole, data.
    *       $currentUser будет полностью обновлен только тогда, когда
    *       $currentUser.id != data.id. Иначе будет просто "добавлены" поля в
    *       $currentUser
    *   @example
    *       <pre>
    *           CmsUser.me (user)->
    *               $currentUser.setData(user)
    *       </pre>
     */
    this.setData = function(data) {
      if (this.data.id !== data.id) {
        this.data = angular.copy(data);
        setData();
        return;
      }
      this.data = angular.extend(this.data, data);
      setData();
    };

    /**
    *   @ngdoc property
    *   @name ui.cms.service:$currentUser#clearData
    *   @methodOf ui.cms.service:$currentUser
    *   @description
    *        обнуление текущего пользователя
     */
    this.clearData = function() {
      this.setData({
        id: 0
      });
    };

    /**
    *   @ngdoc property
    *   @name ui.cms.service:$currentUser#canViewPage
    *   @methodOf ui.cms.service:$currentUser
    *   @param {object} pageObject объект описания условий
    *   @returns {boolean} true/false
    *   @description
    *       функция для определения возможен ли доступ пользователю к текущей странице
    *   @example
    *       <pre>
    *       </pre>
     */
    this.canViewPage = function(pageObject) {
      var shouldTest;
      console.error('$currentUser.canViewPage: DEPRECATED function!');
      shouldTest = false;
      if (pageObject.url) {
        if (pageObject.url instanceof RegExp) {
          shouldTest = !!pageObject.url.test(location.pathname);
        } else if (angular.isString(pageObject.url)) {
          shouldTest = location.pathname === pageObject.url;
        }
      }
      if (pageObject.stateName) {
        $state = $state || $injector.get('state');
        shouldTest = $state.current.name === pageObject.stateName;
      }
      if (!shouldTest) {
        return true;
      }
      if (pageObject.allowedUserRoles) {
        return pageObject.allowedUserRoles.indexOf(this.userRole) !== -1;
      }
      return false;
    };
    this.onChange = function(callback) {
      var id;
      id = new Date().getTime() + '-' + Math.random().toString(36).substr(2, 9);
      onChangeCallbacks[id] = callback;
      return function() {
        delete onChangeCallbacks[id];
      };
    };
    this.hasSpecialPermission = function() {
      throw "$currentUser: hasSpecialPermission service is not allowed anymore. Use django permissions instead ($currentUser.hasPermission) and remove CurrentUserSpecialPermissionResolver factory from your code";
    };
    return this;
  }]);

}).call(this);
;

/**
*   @ngdoc service
*   @name ui.cms.service:$timezones
*   @description сервис, который содержит вспомогательные функции для работы с
*       таймзонами и их названиями. Взяты из пакета для питона pytz
 */

(function() {
  angular.module('ui.cms').service('$timezones', ["$injector", function($injector) {
    var $langPicker, africa, america, asia, australia, cache, europe, self, translations;
    self = this;
    $langPicker = null;
    translations = {};
    europe = [["Europe/Amsterdam", "Europe/Amsterdam"], ["Europe/Andorra", "Europe/Andorra"], ["Europe/Astrakhan", "Europe/Astrakhan"], ["Europe/Athens", "Europe/Athens"], ["Europe/Belgrade", "Europe/Belgrade"], ["Europe/Berlin", "Europe/Berlin"], ["Europe/Bratislava", "Europe/Bratislava"], ["Europe/Brussels", "Europe/Brussels"], ["Europe/Bucharest", "Europe/Bucharest"], ["Europe/Budapest", "Europe/Budapest"], ["Europe/Busingen", "Europe/Busingen"], ["Europe/Chisinau", "Europe/Chisinau"], ["Europe/Copenhagen", "Europe/Copenhagen"], ["Europe/Dublin", "Europe/Dublin"], ["Europe/Gibraltar", "Europe/Gibraltar"], ["Europe/Guernsey", "Europe/Guernsey"], ["Europe/Helsinki", "Europe/Helsinki"], ["Europe/Isle_of_Man", "Europe/Isle of Man"], ["Europe/Istanbul", "Europe/Istanbul"], ["Europe/Jersey", "Europe/Jersey"], ["Europe/Kaliningrad", "Europe/Kaliningrad"], ["Europe/Kiev", "Europe/Kiev"], ["Europe/Kirov", "Europe/Kirov"], ["Europe/Lisbon", "Europe/Lisbon"], ["Europe/Ljubljana", "Europe/Ljubljana"], ["Europe/London", "Europe/London"], ["Europe/Luxembourg", "Europe/Luxembourg"], ["Europe/Madrid", "Europe/Madrid"], ["Europe/Malta", "Europe/Malta"], ["Europe/Mariehamn", "Europe/Mariehamn"], ["Europe/Minsk", "Europe/Minsk"], ["Europe/Monaco", "Europe/Monaco"], ["Europe/Moscow", "Europe/Moscow"], ["Europe/Oslo", "Europe/Oslo"], ["Europe/Paris", "Europe/Paris"], ["Europe/Podgorica", "Europe/Podgorica"], ["Europe/Prague", "Europe/Prague"], ["Europe/Riga", "Europe/Riga"], ["Europe/Rome", "Europe/Rome"], ["Europe/Samara", "Europe/Samara"], ["Europe/San_Marino", "Europe/San Marino"], ["Europe/Sarajevo", "Europe/Sarajevo"], ["Europe/Saratov", "Europe/Saratov"], ["Europe/Simferopol", "Europe/Simferopol"], ["Europe/Skopje", "Europe/Skopje"], ["Europe/Sofia", "Europe/Sofia"], ["Europe/Stockholm", "Europe/Stockholm"], ["Europe/Tallinn", "Europe/Tallinn"], ["Europe/Tirane", "Europe/Tirane"], ["Europe/Ulyanovsk", "Europe/Ulyanovsk"], ["Europe/Uzhgorod", "Europe/Uzhgorod"], ["Europe/Vaduz", "Europe/Vaduz"], ["Europe/Vatican", "Europe/Vatican"], ["Europe/Vienna", "Europe/Vienna"], ["Europe/Vilnius", "Europe/Vilnius"], ["Europe/Volgograd", "Europe/Volgograd"], ["Europe/Warsaw", "Europe/Warsaw"], ["Europe/Zagreb", "Europe/Zagreb"], ["Europe/Zaporozhye", "Europe/Zaporozhye"], ["Europe/Zurich", "Europe/Zurich"]];
    asia = [["Asia/Aden", "Asia/Aden"], ["Asia/Almaty", "Asia/Almaty"], ["Asia/Amman", "Asia/Amman"], ["Asia/Anadyr", "Asia/Anadyr"], ["Asia/Aqtau", "Asia/Aqtau"], ["Asia/Aqtobe", "Asia/Aqtobe"], ["Asia/Ashgabat", "Asia/Ashgabat"], ["Asia/Atyrau", "Asia/Atyrau"], ["Asia/Baghdad", "Asia/Baghdad"], ["Asia/Bahrain", "Asia/Bahrain"], ["Asia/Baku", "Asia/Baku"], ["Asia/Bangkok", "Asia/Bangkok"], ["Asia/Barnaul", "Asia/Barnaul"], ["Asia/Beirut", "Asia/Beirut"], ["Asia/Bishkek", "Asia/Bishkek"], ["Asia/Brunei", "Asia/Brunei"], ["Asia/Chita", "Asia/Chita"], ["Asia/Choibalsan", "Asia/Choibalsan"], ["Asia/Colombo", "Asia/Colombo"], ["Asia/Damascus", "Asia/Damascus"], ["Asia/Dhaka", "Asia/Dhaka"], ["Asia/Dili", "Asia/Dili"], ["Asia/Dubai", "Asia/Dubai"], ["Asia/Dushanbe", "Asia/Dushanbe"], ["Asia/Famagusta", "Asia/Famagusta"], ["Asia/Gaza", "Asia/Gaza"], ["Asia/Hebron", "Asia/Hebron"], ["Asia/Ho_Chi_Minh", "Asia/Ho Chi Minh"], ["Asia/Hong_Kong", "Asia/Hong Kong"], ["Asia/Hovd", "Asia/Hovd"], ["Asia/Irkutsk", "Asia/Irkutsk"], ["Asia/Jakarta", "Asia/Jakarta"], ["Asia/Jayapura", "Asia/Jayapura"], ["Asia/Jerusalem", "Asia/Jerusalem"], ["Asia/Kabul", "Asia/Kabul"], ["Asia/Kamchatka", "Asia/Kamchatka"], ["Asia/Karachi", "Asia/Karachi"], ["Asia/Kathmandu", "Asia/Kathmandu"], ["Asia/Khandyga", "Asia/Khandyga"], ["Asia/Kolkata", "Asia/Kolkata"], ["Asia/Krasnoyarsk", "Asia/Krasnoyarsk"], ["Asia/Kuala_Lumpur", "Asia/Kuala Lumpur"], ["Asia/Kuching", "Asia/Kuching"], ["Asia/Kuwait", "Asia/Kuwait"], ["Asia/Macau", "Asia/Macau"], ["Asia/Magadan", "Asia/Magadan"], ["Asia/Makassar", "Asia/Makassar"], ["Asia/Manila", "Asia/Manila"], ["Asia/Muscat", "Asia/Muscat"], ["Asia/Nicosia", "Asia/Nicosia"], ["Asia/Novokuznetsk", "Asia/Novokuznetsk"], ["Asia/Novosibirsk", "Asia/Novosibirsk"], ["Asia/Omsk", "Asia/Omsk"], ["Asia/Oral", "Asia/Oral"], ["Asia/Phnom_Penh", "Asia/Phnom Penh"], ["Asia/Pontianak", "Asia/Pontianak"], ["Asia/Pyongyang", "Asia/Pyongyang"], ["Asia/Qatar", "Asia/Qatar"], ["Asia/Qyzylorda", "Asia/Qyzylorda"], ["Asia/Riyadh", "Asia/Riyadh"], ["Asia/Sakhalin", "Asia/Sakhalin"], ["Asia/Samarkand", "Asia/Samarkand"], ["Asia/Seoul", "Asia/Seoul"], ["Asia/Shanghai", "Asia/Shanghai"], ["Asia/Singapore", "Asia/Singapore"], ["Asia/Srednekolymsk", "Asia/Srednekolymsk"], ["Asia/Taipei", "Asia/Taipei"], ["Asia/Tashkent", "Asia/Tashkent"], ["Asia/Tbilisi", "Asia/Tbilisi"], ["Asia/Tehran", "Asia/Tehran"], ["Asia/Thimphu", "Asia/Thimphu"], ["Asia/Tokyo", "Asia/Tokyo"], ["Asia/Tomsk", "Asia/Tomsk"], ["Asia/Ulaanbaatar", "Asia/Ulaanbaatar"], ["Asia/Urumqi", "Asia/Urumqi"], ["Asia/Ust-Nera", "Asia/Ust-Nera"], ["Asia/Vientiane", "Asia/Vientiane"], ["Asia/Vladivostok", "Asia/Vladivostok"], ["Asia/Yakutsk", "Asia/Yakutsk"], ["Asia/Yangon", "Asia/Yangon"], ["Asia/Yekaterinburg", "Asia/Yekaterinburg"], ["Asia/Yerevan", "Asia/Yerevan"]];
    africa = [["Africa/Abidjan", "Africa/Abidjan"], ["Africa/Accra", "Africa/Accra"], ["Africa/Addis_Ababa", "Africa/Addis Ababa"], ["Africa/Algiers", "Africa/Algiers"], ["Africa/Asmara", "Africa/Asmara"], ["Africa/Bamako", "Africa/Bamako"], ["Africa/Bangui", "Africa/Bangui"], ["Africa/Banjul", "Africa/Banjul"], ["Africa/Bissau", "Africa/Bissau"], ["Africa/Blantyre", "Africa/Blantyre"], ["Africa/Brazzaville", "Africa/Brazzaville"], ["Africa/Bujumbura", "Africa/Bujumbura"], ["Africa/Cairo", "Africa/Cairo"], ["Africa/Casablanca", "Africa/Casablanca"], ["Africa/Ceuta", "Africa/Ceuta"], ["Africa/Conakry", "Africa/Conakry"], ["Africa/Dakar", "Africa/Dakar"], ["Africa/Dar_es_Salaam", "Africa/Dar es Salaam"], ["Africa/Djibouti", "Africa/Djibouti"], ["Africa/Douala", "Africa/Douala"], ["Africa/El_Aaiun", "Africa/El Aaiun"], ["Africa/Freetown", "Africa/Freetown"], ["Africa/Gaborone", "Africa/Gaborone"], ["Africa/Harare", "Africa/Harare"], ["Africa/Johannesburg", "Africa/Johannesburg"], ["Africa/Juba", "Africa/Juba"], ["Africa/Kampala", "Africa/Kampala"], ["Africa/Khartoum", "Africa/Khartoum"], ["Africa/Kigali", "Africa/Kigali"], ["Africa/Kinshasa", "Africa/Kinshasa"], ["Africa/Lagos", "Africa/Lagos"], ["Africa/Libreville", "Africa/Libreville"], ["Africa/Lome", "Africa/Lome"], ["Africa/Luanda", "Africa/Luanda"], ["Africa/Lubumbashi", "Africa/Lubumbashi"], ["Africa/Lusaka", "Africa/Lusaka"], ["Africa/Malabo", "Africa/Malabo"], ["Africa/Maputo", "Africa/Maputo"], ["Africa/Maseru", "Africa/Maseru"], ["Africa/Mbabane", "Africa/Mbabane"], ["Africa/Mogadishu", "Africa/Mogadishu"], ["Africa/Monrovia", "Africa/Monrovia"], ["Africa/Nairobi", "Africa/Nairobi"], ["Africa/Ndjamena", "Africa/Ndjamena"], ["Africa/Niamey", "Africa/Niamey"], ["Africa/Nouakchott", "Africa/Nouakchott"], ["Africa/Ouagadougou", "Africa/Ouagadougou"], ["Africa/Porto-Novo", "Africa/Porto-Novo"], ["Africa/Sao_Tome", "Africa/Sao Tome"], ["Africa/Tripoli", "Africa/Tripoli"], ["Africa/Tunis", "Africa/Tunis"], ["Africa/Windhoek", "Africa/Windhoek"]];
    america = [["America/Adak", "America/Adak"], ["America/Anchorage", "America/Anchorage"], ["America/Anguilla", "America/Anguilla"], ["America/Antigua", "America/Antigua"], ["America/Araguaina", "America/Araguaina"], ["America/Argentina/Buenos_Aires", "America/Argentina/Buenos Aires"], ["America/Argentina/Catamarca", "America/Argentina/Catamarca"], ["America/Argentina/Cordoba", "America/Argentina/Cordoba"], ["America/Argentina/Jujuy", "America/Argentina/Jujuy"], ["America/Argentina/La_Rioja", "America/Argentina/La Rioja"], ["America/Argentina/Mendoza", "America/Argentina/Mendoza"], ["America/Argentina/Rio_Gallegos", "America/Argentina/Rio Gallegos"], ["America/Argentina/Salta", "America/Argentina/Salta"], ["America/Argentina/San_Juan", "America/Argentina/San Juan"], ["America/Argentina/San_Luis", "America/Argentina/San Luis"], ["America/Argentina/Tucuman", "America/Argentina/Tucuman"], ["America/Argentina/Ushuaia", "America/Argentina/Ushuaia"], ["America/Aruba", "America/Aruba"], ["America/Asuncion", "America/Asuncion"], ["America/Atikokan", "America/Atikokan"], ["America/Bahia", "America/Bahia"], ["America/Bahia_Banderas", "America/Bahia Banderas"], ["America/Barbados", "America/Barbados"], ["America/Belem", "America/Belem"], ["America/Belize", "America/Belize"], ["America/Blanc-Sablon", "America/Blanc-Sablon"], ["America/Boa_Vista", "America/Boa Vista"], ["America/Bogota", "America/Bogota"], ["America/Boise", "America/Boise"], ["America/Cambridge_Bay", "America/Cambridge Bay"], ["America/Campo_Grande", "America/Campo Grande"], ["America/Cancun", "America/Cancun"], ["America/Caracas", "America/Caracas"], ["America/Cayenne", "America/Cayenne"], ["America/Cayman", "America/Cayman"], ["America/Chicago", "America/Chicago"], ["America/Chihuahua", "America/Chihuahua"], ["America/Costa_Rica", "America/Costa Rica"], ["America/Creston", "America/Creston"], ["America/Cuiaba", "America/Cuiaba"], ["America/Curacao", "America/Curacao"], ["America/Danmarkshavn", "America/Danmarkshavn"], ["America/Dawson", "America/Dawson"], ["America/Dawson_Creek", "America/Dawson Creek"], ["America/Denver", "America/Denver"], ["America/Detroit", "America/Detroit"], ["America/Dominica", "America/Dominica"], ["America/Edmonton", "America/Edmonton"], ["America/Eirunepe", "America/Eirunepe"], ["America/El_Salvador", "America/El Salvador"], ["America/Fort_Nelson", "America/Fort Nelson"], ["America/Fortaleza", "America/Fortaleza"], ["America/Glace_Bay", "America/Glace Bay"], ["America/Godthab", "America/Godthab"], ["America/Goose_Bay", "America/Goose Bay"], ["America/Grand_Turk", "America/Grand Turk"], ["America/Grenada", "America/Grenada"], ["America/Guadeloupe", "America/Guadeloupe"], ["America/Guatemala", "America/Guatemala"], ["America/Guayaquil", "America/Guayaquil"], ["America/Guyana", "America/Guyana"], ["America/Halifax", "America/Halifax"], ["America/Havana", "America/Havana"], ["America/Hermosillo", "America/Hermosillo"], ["America/Indiana/Indianapolis", "America/Indiana/Indianapolis"], ["America/Indiana/Knox", "America/Indiana/Knox"], ["America/Indiana/Marengo", "America/Indiana/Marengo"], ["America/Indiana/Petersburg", "America/Indiana/Petersburg"], ["America/Indiana/Tell_City", "America/Indiana/Tell City"], ["America/Indiana/Vevay", "America/Indiana/Vevay"], ["America/Indiana/Vincennes", "America/Indiana/Vincennes"], ["America/Indiana/Winamac", "America/Indiana/Winamac"], ["America/Inuvik", "America/Inuvik"], ["America/Iqaluit", "America/Iqaluit"], ["America/Jamaica", "America/Jamaica"], ["America/Juneau", "America/Juneau"], ["America/Kentucky/Louisville", "America/Kentucky/Louisville"], ["America/Kentucky/Monticello", "America/Kentucky/Monticello"], ["America/Kralendijk", "America/Kralendijk"], ["America/La_Paz", "America/La Paz"], ["America/Lima", "America/Lima"], ["America/Los_Angeles", "America/Los Angeles"], ["America/Lower_Princes", "America/Lower Princes"], ["America/Maceio", "America/Maceio"], ["America/Managua", "America/Managua"], ["America/Manaus", "America/Manaus"], ["America/Marigot", "America/Marigot"], ["America/Martinique", "America/Martinique"], ["America/Matamoros", "America/Matamoros"], ["America/Mazatlan", "America/Mazatlan"], ["America/Menominee", "America/Menominee"], ["America/Merida", "America/Merida"], ["America/Metlakatla", "America/Metlakatla"], ["America/Mexico_City", "America/Mexico City"], ["America/Miquelon", "America/Miquelon"], ["America/Moncton", "America/Moncton"], ["America/Monterrey", "America/Monterrey"], ["America/Montevideo", "America/Montevideo"], ["America/Montserrat", "America/Montserrat"], ["America/Nassau", "America/Nassau"], ["America/New_York", "America/New York"], ["America/Nipigon", "America/Nipigon"], ["America/Nome", "America/Nome"], ["America/Noronha", "America/Noronha"], ["America/North_Dakota/Beulah", "America/North Dakota/Beulah"], ["America/North_Dakota/Center", "America/North Dakota/Center"], ["America/North_Dakota/New_Salem", "America/North Dakota/New Salem"], ["America/Ojinaga", "America/Ojinaga"], ["America/Panama", "America/Panama"], ["America/Pangnirtung", "America/Pangnirtung"], ["America/Paramaribo", "America/Paramaribo"], ["America/Phoenix", "America/Phoenix"], ["America/Port-au-Prince", "America/Port-au-Prince"], ["America/Port_of_Spain", "America/Port of Spain"], ["America/Porto_Velho", "America/Porto Velho"], ["America/Puerto_Rico", "America/Puerto Rico"], ["America/Punta_Arenas", "America/Punta Arenas"], ["America/Rainy_River", "America/Rainy River"], ["America/Rankin_Inlet", "America/Rankin Inlet"], ["America/Recife", "America/Recife"], ["America/Regina", "America/Regina"], ["America/Resolute", "America/Resolute"], ["America/Rio_Branco", "America/Rio Branco"], ["America/Santarem", "America/Santarem"], ["America/Santiago", "America/Santiago"], ["America/Santo_Domingo", "America/Santo Domingo"], ["America/Sao_Paulo", "America/Sao Paulo"], ["America/Scoresbysund", "America/Scoresbysund"], ["America/Sitka", "America/Sitka"], ["America/St_Barthelemy", "America/St Barthelemy"], ["America/St_Johns", "America/St Johns"], ["America/St_Kitts", "America/St Kitts"], ["America/St_Lucia", "America/St Lucia"], ["America/St_Thomas", "America/St Thomas"], ["America/St_Vincent", "America/St Vincent"], ["America/Swift_Current", "America/Swift Current"], ["America/Tegucigalpa", "America/Tegucigalpa"], ["America/Thule", "America/Thule"], ["America/Thunder_Bay", "America/Thunder Bay"], ["America/Tijuana", "America/Tijuana"], ["America/Toronto", "America/Toronto"], ["America/Tortola", "America/Tortola"], ["America/Vancouver", "America/Vancouver"], ["America/Whitehorse", "America/Whitehorse"], ["America/Winnipeg", "America/Winnipeg"], ["America/Yakutat", "America/Yakutat"], ["America/Yellowknife", "America/Yellowknife"]];
    australia = [["Australia/Adelaide", "Australia/Adelaide"], ["Australia/Brisbane", "Australia/Brisbane"], ["Australia/Broken_Hill", "Australia/Broken Hill"], ["Australia/Currie", "Australia/Currie"], ["Australia/Darwin", "Australia/Darwin"], ["Australia/Eucla", "Australia/Eucla"], ["Australia/Hobart", "Australia/Hobart"], ["Australia/Lindeman", "Australia/Lindeman"], ["Australia/Lord_Howe", "Australia/Lord Howe"], ["Australia/Melbourne", "Australia/Melbourne"], ["Australia/Perth", "Australia/Perth"], ["Australia/Sydney", "Australia/Sydney"]];

    /**
    *   @ngdoc property
    *   @name ui.cms.service:$timezones#europe
    *   @propertyOf ui.cms.service:$timezones
    *   @description
    *       <label class="label type-hint type-hint-array">array</label>
    *       список таймзон в европе в виде массива (только английские названия часовых поясов):
    *       <pre>
    *           $timezones.europe = [
    *               ["Europe/Amsterdam", "Europe/Amsterdam"]
    *               ["Europe/Andorra", "Europe/Andorra"]
    *               ...
    *           ]
    *       </pre>
     */
    this.europe = europe.concat([['UTC', 'UTC']]);

    /**
    *   @ngdoc property
    *   @name ui.cms.service:$timezones#asia
    *   @propertyOf ui.cms.service:$timezones
    *   @description
    *       <label class="label type-hint type-hint-array">array</label>
    *       список таймзон в азии в виде массива (только английские названия часовых поясов):
    *       <pre>
    *           $timezones.asia = [
    *               ["Asia/Aden", "Asia/Aden"]
    *               ["Asia/Almaty", "Asia/Almaty"]
    *               ...
    *           ]
    *       </pre>
     */
    this.asia = asia.concat([['UTC', 'UTC']]);

    /**
    *   @ngdoc property
    *   @name ui.cms.service:$timezones#africa
    *   @propertyOf ui.cms.service:$timezones
    *   @description
    *       <label class="label type-hint type-hint-array">array</label>
    *       список таймзон в африке в виде массива (только английские названия часовых поясов):
    *       <pre>
    *           $timezones.africa = [
    *               ["Africa/Abidjan", "Africa/Abidjan"]
    *               ["Africa/Accra", "Africa/Accra"]
    *               ...
    *           ]
    *       </pre>
     */
    this.africa = africa.concat([['UTC', 'UTC']]);

    /**
    *   @ngdoc property
    *   @name ui.cms.service:$timezones#america
    *   @propertyOf ui.cms.service:$timezones
    *   @description
    *       <label class="label type-hint type-hint-array">array</label>
    *       список таймзон в америке в виде массива (только английские названия часовых поясов):
    *       <pre>
    *           $timezones.america = [
    *               ["America/Adak", "America/Adak"]
    *               ["America/Anchorage", "America/Anchorage"]
    *               ...
    *           ]
    *       </pre>
     */
    this.america = america.concat([['UTC', 'UTC']]);

    /**
    *   @ngdoc property
    *   @name ui.cms.service:$timezones#australia
    *   @propertyOf ui.cms.service:$timezones
    *   @description
    *       <label class="label type-hint type-hint-array">array</label>
    *       список таймзон в австралии в виде массива (только английские названия часовых поясов):
    *       <pre>
    *           $timezones.australia = [
    *               ["Australia/Adelaide", "Australia/Adelaide"]
    *               ["Australia/Brisbane", "Australia/Brisbane"]
    *               ...
    *           ]
    *       </pre>
     */
    this.australia = australia.concat([['UTC', 'UTC']]);
    cache = {};
    ['europe', 'asia', 'africa', 'america', 'australia'].forEach(function(s) {
      self[s].forEach(function(c) {
        cache[c[0]] = c[1];
      });
    });

    /**
    *   @ngdoc property
    *   @name ui.cms.service:$timezones#world
    *   @propertyOf ui.cms.service:$timezones
    *   @description
    *       <label class="label type-hint type-hint-array">array</label>
    *       список таймзон всего мира.
    *       <pre>
    *           $timezones.world = [
    *               ["Europe/Amsterdam", "Europe/Amsterdam"]
    *               ["Europe/Andorra", "Europe/Andorra"]
    *               ...
    *           ]
    *       </pre>
     */
    this.world = europe.concat(asia).concat(america).concat(africa).concat(australia).concat([['UTC', 'UTC']]);

    /**
    *   @ngdoc property
    *   @name ui.cms.service:$timezones#codeToText
    *   @methodOf ui.cms.service:$timezones
    *   @description
    *        возвращает название таймзоны по ее 2-х значному коду, или строку
    *       "Invalid timezone code"
    *   @param {string} code название таймзоны. Например: 'Europe/Amsterdam', 'Asia/Ho_Chi_Minh'
    *   @param {string=} forceLang язык, на котором нужно отобразить название таймзоны. Если не указано, то будет взят текущий язык пользователя.
    *   @returns {string} название таймзоны
     */
    this.codeToText = function(code, forceLang) {
      if (!$langPicker) {
        $langPicker = $injector.get('$langPicker');
      }
      forceLang = forceLang || $langPicker.currentLang;
      if (forceLang === 'en') {
        return cache[code] || 'Invalid timezone code';
      }
      if (translations[forceLang] && translations[forceLang][code]) {
        return translations[forceLang][code];
      }
      forceLang = $langPicker.currentLang;
      if (translations[forceLang] && translations[forceLang][code]) {
        return translations[forceLang][code];
      }
      return cache[code] || 'Invalid timezone code';
    };

    /**
    *   @ngdoc property
    *   @name ui.cms.service:$timezones#getList
    *   @methodOf ui.cms.service:$timezones
    *   @description
    *        возвращает отсортированные списки таймзон, согласно названию континетов. По умолчанию вернет весь мир.
    *   @param {string=} p1 название. Напр: world. Варианты: 'europe', 'asia', 'africa', 'america', 'australia', 'world'
    *   @param {string=} p2 название. Напр: world. Варианты: 'europe', 'asia', 'africa', 'america', 'australia', 'world'
    *   @param {string=} pN название. Напр: world. Варианты: 'europe', 'asia', 'africa', 'america', 'australia', 'world'
    *   @returns {array} список таймзон
    *   @example
    *       <pre>
    *           > $timezones.getList('australia', 'asia')
    *           [
    *               ["Asia/Aden", "Asia/Aden"]
    *               ["Asia/Almaty", "Asia/Almaty"]
    *               ...
    *           ]
    *       </pre>
     */
    this.getList = function() {
      var a, allowed, i, in_list, len, list;
      allowed = [];
      for (i = 0, len = arguments.length; i < len; i++) {
        a = arguments[i];
        if (!allowed.includes(a)) {
          if (['europe', 'asia', 'africa', 'america', 'australia', 'world'].includes(a)) {
            allowed.push(a);
          }
        }
      }
      if (!allowed.length) {
        allowed = ['world'];
      }
      if (allowed.includes('world')) {
        allowed = ['world'];
      }
      list = [];
      in_list = [];
      allowed.forEach(function(a) {
        (self[a] || []).forEach(function(c) {
          if (in_list.includes(c[0])) {
            return;
          }
          in_list.push(c[0]);
          list.push(c);
        });
      });
      list.sort(function(a, b) {
        if (a[1] < b[1]) {
          return -1;
        }
        if (a[1] > b[1]) {
          return 1;
        }
        return 0;
      });
      return list;
    };
    return this;
  }]);

}).call(this);
;

/**
*   @ngdoc service
*   @name ui.cms.service:$worldMaps
*   @description сервис, который содержит вспомогательные функции для работы с картами
 */

(function() {
  angular.module('ui.cms').service('$worldMaps', ["$injector", function($injector) {

    /**
    *   @ngdoc property
    *   @name ui.cms.service:$worldMaps#provider
    *   @propertyOf ui.cms.service:$worldMaps
    *   @description
    *       <label class="label type-hint type-hint-string">string</label>
    *       Название доступного провайдера. null - карты запрещены, 'google' - Google Maps
     */
    var error, ref;
    this.provider = null;
    try {
      this.provider = CMS_SPECIAL_INTEGRATIONS.maps || null;
      if ((ref = this.provider) !== null && ref !== 'google') {
        throw "$worldMaps: unknown provider '" + this.provider + "' in CMS_SPECIAL_INTEGRATIONS.maps!";
      }
    } catch (error1) {
      error = error1;
    }

    /**
    *   @ngdoc property
    *   @name ui.cms.service:$worldMaps#getFactory
    *   @methodOf ui.cms.service:$worldMaps
    *   @description
    *        возвращает фабрику-обертку для карт. На данный момент может вернуть ТОЛЬКО либо null, либо UicGoogleMapClass
     */
    this.getFactory = function() {
      if (!this.provider) {
        return null;
      }
      return $injector.get('UicGoogleMapClass');
    };
    return this;
  }]);

}).call(this);
;
(function() {
  angular.module('ui.cms').controller('UicBaseView', ["$scope", "$document", "$location", "$anchorScroll", function($scope, $document, $location, $anchorScroll) {
    $scope.scrollToElement = function(element, options) {
      options || (options = {});
      if ($document.scrollToElementAnimated) {
        if (angular.isString(element)) {
          element = document.getElementById(element.replace('#', ''));
        }
        if (!element) {
          return;
        }
        $document.scrollToElementAnimated(element);
        return;
      }
      if (angular.isString(element)) {
        $location.hash(element);
        $anchorScroll();
        $location.hash(element);
      }
    };
  }]);

}).call(this);
;
(function() {
  angular.module('ng').config(["$provide", function($provide) {
    var parentQ;
    parentQ = null;
    $provide.decorator('$q', ["$delegate", function($delegate) {
      var $q, hasError;
      $q = $delegate;
      parentQ = $delegate;
      hasError = function(arr) {
        var a, i, len;
        for (i = 0, len = arr.length; i < len; i++) {
          a = arr[i];
          if (a) {
            return true;
          }
        }
        return false;
      };
      $q.waterfall = function(promises, skipErrors) {
        var deferred, errors, results, run;
        deferred = $q.defer();
        results = [];
        errors = [];
        run = function(_promises) {
          var fn;
          if (!_promises[0]) {
            if (hasError(errors)) {
              results.$errors = errors;
            }
            return deferred.resolve(results);
          }
          fn = _promises[0].$promise || _promises[0];
          if (angular.isFunction(fn) && !fn.then) {
            fn = fn();
          }
          fn = fn.$promise || fn;
          fn.then(function(data) {
            results.push(data);
            errors.push(null);
            return run(_promises.slice(1));
          }, function(data) {
            if (skipErrors) {
              errors.push(data);
              results.push(null);
              return run(_promises.slice(1));
            }
            return deferred.reject(data);
          });
        };
        run(promises);
        return deferred.promise;
      };
      $q.allSettled = function(promises) {
        var fulfilled, rejected;
        fulfilled = function(value) {
          return {
            status: 'fulfilled',
            value: value
          };
        };
        rejected = function(reason) {
          return {
            status: 'rejected',
            reason: reason
          };
        };
        return $q.all(promises.map(function(promise) {
          return promise.then(fulfilled)["catch"](rejected);
        }));
      };
      return $q;
    }]);
    $provide.decorator('$timeout', ["$delegate", function($delegate) {
      $delegate.debounce = function(func, wait, immediate) {
        var defer, timeout;
        timeout = null;
        defer = parentQ.defer();
        return function() {
          var args, callNow, context, later;
          context = this;
          args = arguments;
          later = function() {
            timeout = null;
            if (!immediate) {
              defer.resolve(func.apply(context, args));
              defer = parentQ.defer();
            }
          };
          callNow = immediate && !timeout;
          if (timeout) {
            $delegate.cancel(timeout);
          }
          timeout = $delegate(later, wait);
          if (callNow) {
            defer.resolve(func.apply(context, args));
            defer = parentQ.defer();
          }
          return defer.promise;
        };
      };
      return $delegate;
    }]);
  }]);

  angular.module('gettext').config(["$provide", function($provide) {
    $provide.decorator('gettextCatalog', ["$delegate", "$http", "$q", function($delegate, $http, $q) {
      $delegate.loadRemote = function(url) {
        var defer;
        if ((url.split('?')[0] || '').length === 0) {
          defer = $q.defer();
          defer.resolve({
            data: {}
          });
          return defer.promise;
        }
        return $http({
          method: 'GET',
          url: url,
          cache: $delegate.cache
        }).then(function(response) {
          var data, lang;
          data = response.data;
          if (!angular.isObject(data)) {
            return {
              data: {}
            };
          }
          for (lang in data) {
            $delegate.setStrings(lang, data[lang]);
          }
          return response;
        });
      };
      return $delegate;
    }]);
    return $provide.decorator('gettextCatalog', ["$delegate", function($delegate) {

      /*
          поскольку я ленив, то очень не хочется писать в jade конструкции типа:
          span
              span(translate='') hello world
              | :
          когда требуется чтоб строка которая будет переводиться была без спецсимволов в конце или начале.
      
          декоратор gettextCatalog сначала ищет перевод по старинке, но если у него не получается, то
          он отрывает с конца и начала строки символы: []-*"!.',:_= и пробел,
          после чего снова пытается найти перевод
       */
      var cleanString, gettextCatalog, origGetPlural, origGetString;
      gettextCatalog = $delegate;
      cleanString = function(string) {
        return string.replace(/^[\/\[\]\-\*\"\'\!\.\,\ :_=]+/g, '').replace(/[\/\[\]\-\*\"\'\!\.\,\ :_=]+$/, '');
      };
      origGetString = gettextCatalog.getString;
      origGetPlural = gettextCatalog.getPlural;
      gettextCatalog.getString = function(string, scope, context) {
        var cleaned, tr;
        if (!string) {
          return '';
        }
        tr = origGetString.call(this, string, scope, context);
        if (tr !== string) {
          return tr;
        }
        cleaned = cleanString(string);
        tr = origGetString.call(this, cleaned, scope, context);
        return string.replace(cleaned, tr);
      };
      gettextCatalog.getPlural = function(n, string, stringPlural, scope, context) {
        var cleaned, cleanedPlural, tr;
        tr = origGetPlural.call(this, n, string, stringPlural, scope, context);
        if (tr !== string && tr !== stringPlural) {
          return tr;
        }
        cleaned = cleanString(string || '');
        cleanedPlural = cleanString(stringPlural || '');
        tr = origGetPlural.call(this, n, cleaned, cleanedPlural, scope, context);
        return string.replace(cleanedPlural, tr).replace(cleaned, tr);
      };
      return gettextCatalog;
    }]);
  }]);

  angular.module('storage.gdpr').config(["storageSettingsProvider", "$gdprStorageProvider", function(storageSettingsProvider, $gdprStorageProvider) {
    storageSettingsProvider.setPrefix('$lb$');
    $gdprStorageProvider.registerKey('accessTokenId', {
      purpose: angular.tr('User authentication token')
    });
    $gdprStorageProvider.registerKey('currentUserId', {
      purpose: angular.tr('User id')
    });
    $gdprStorageProvider.registerKey('rememberMe', {
      purpose: angular.tr('Flag that shows the authentication credentials should be remembered in localStorage across app/browser restarts')
    });
    return $gdprStorageProvider.registerKey('googleMapsCache', {
      purpose: angular.tr('Cache for google maps')
    });
  }]);

  angular.module('ui.cms').config(["$httpProvider", "$gdprStorageProvider", "storageSettingsProvider", "$provide", "$locationProvider", "$injector", "$uicIntegrationsProvider", function($httpProvider, $gdprStorageProvider, storageSettingsProvider, $provide, $locationProvider, $injector, $uicIntegrationsProvider) {
    var encodeUriQuery, encodeUriSegment;
    $locationProvider.html5Mode(true);
    $uicIntegrationsProvider.registerThirdPartyLibrary('googlemaps-infobox', ["/bower_components/angular-lbcms-libs/external/infobox-lib.js"]);
    $uicIntegrationsProvider.registerThirdPartyLibrary('aggrid', ["/bower_components/angular-lbcms-libs/external/aggrid/15/ag-grid.min.js"]);
    $uicIntegrationsProvider.registerThirdPartyLibrary('fontawesome4-json', ["/bower_components/angular-lbcms-libs/external/FontAwesomeIcons4.json"]);
    $uicIntegrationsProvider.registerThirdPartyLibrary('fontawesome5-json', ["/bower_components/angular-lbcms-libs/external/FontAwesomeIcons5.json"]);
    $uicIntegrationsProvider.registerThirdPartyLibrary('intl-tel-input', ["https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.8/js/intlTelInput.min.js", "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.8/js/utils.min.js", "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.8/css/intlTelInput.min.css"]);
    $uicIntegrationsProvider.registerThirdPartyLibrary('sticky-sidebar', ["https://cdn.jsdelivr.net/npm/sticky-sidebar@3.3.1/dist/sticky-sidebar.min.js"]);

    /*
        мод окна от бустрапа бросают исключения если они закрыты по escape, cancel
        https://github.com/angular-ui/bootstrap/issues/6501
     */
    $provide.decorator("$exceptionHandler", ["$delegate", "$injector", function($delegate, $injector) {
      var exceptionsToIgnore;
      exceptionsToIgnore = ['Possibly unhandled rejection: backdrop click', 'Possibly unhandled rejection: cancel', 'Possibly unhandled rejection: escape key press'];
      return function(exception, cause) {
        if (exceptionsToIgnore.includes(exception)) {
          return;
        }
        return $delegate(exception, cause);
      };
    }]);
    delete $httpProvider.defaults.headers.common['X-Requested-With'];
    $httpProvider.interceptors.push(function() {
      return {
        request: function(config) {
          config.params = config.params || {};
          if (!config.cache) {
            config.params['x_default_lang'] = CMS_DEFAULT_LANG;
          }
          return config;
        }
      };
    });
    $provide.decorator('dateFilter', ["$delegate", function($delegate) {
      $delegate.$stateful = true;
      return $delegate;
    }]);

    /*
        декорируем $state.go для того, чтоб можно было спокойно оставлять json в параметрах
     */
    $provide.decorator('$state', ["$delegate", function($delegate) {
      var $state, baseGo;
      $state = $delegate;
      baseGo = $state.go;
      $state.go = function(to, toParams, options) {
        var error, k, v;
        if (angular.isString(toParams)) {
          try {
            toParams = JSON.parse(toParams);
          } catch (error1) {
            error = error1;
          }
        }
        toParams = toParams || {};
        for (k in toParams) {
          v = toParams[k];
          if (typeof v === 'object') {
            toParams[k] = JSON.stringify(v);
          }
        }
        return baseGo(to, toParams, options);
      };
      return $state;
    }]);
    encodeUriQuery = function(val, pctEncodeSpaces) {
      return encodeURIComponent(val).replace(/%40/gi, '@').replace(/%3A/gi, ':').replace(/%24/g, '$').replace(/%2C/gi, ',').replace(/%20/g, pctEncodeSpaces ? '%20' : '+');
    };
    encodeUriSegment = function(val) {
      return encodeUriQuery(val, true).replace(/%26/gi, '&').replace(/%3D/gi, '=').replace(/%2B/gi, '+');
    };
    $provide.decorator('$resource', [
      '$delegate', '$langPicker', 'LoopBackAuth', 'H', function($delegate, $langPicker, LoopBackAuth, H) {
        return function(resourceUrl, resourceParams, actions) {
          var getUrlParams, resource;
          resource = $delegate(resourceUrl, resourceParams, actions);
          resource.$$actions = actions;
          resource.$url = resourceUrl;
          getUrlParams = function(url) {
            var i, len, param, ref, urlParams;
            urlParams = {};
            ref = url.split(/\W/);
            for (i = 0, len = ref.length; i < len; i++) {
              param = ref[i];
              if (param === 'hasOwnProperty') {
                throw 'hasOwnProperty is not a valid parameter name';
              }
              if (!new RegExp('^\\d+$').test(param) && param && new RegExp('(^|[^\\\\]):' + param + '(\\W|$)').test(url)) {
                urlParams[param] = {
                  isQueryParamValue: new RegExp('\\?.*=:' + param + '(?:\\W|$)').test(url)
                };
              }
            }
            return urlParams;
          };
          resource.$actionToUrl = function(actionName, params) {
            var encodedVal, i, k, key, len, paramInfo, queryParams, ref, url, urlParam, urlParams, val, value;
            if (!actions[actionName]) {
              throw "No such action '" + actionName + "'";
            }
            url = actions[actionName].url;
            urlParams = getUrlParams(url);
            params = angular.copy(params || {});
            params.x_db_bucket = params.x_db_bucket || CMS_DB_BUCKET;
            params.access_token = params.access_token || LoopBackAuth.accessTokenId;
            params.preferredLang = params.preferredLang || $langPicker.currentLang;
            params.timezoneOffset = params.timezoneOffset || H.info.timezoneOffset;
            ref = ['x_db_bucket', 'access_token', 'preferredLang'];
            for (i = 0, len = ref.length; i < len; i++) {
              k = ref[i];
              if (params[k] === null || params[k] === '') {
                delete params[k];
              }
            }
            for (urlParam in urlParams) {
              paramInfo = urlParams[urlParam];
              if (params.hasOwnProperty(urlParam)) {
                val = params[urlParam];
              } else {
                val = resourceParams[urlParam];
              }
              if (angular.isDefined(val) && val !== null) {
                if (paramInfo.isQueryParamValue) {
                  encodedVal = encodeUriQuery(val, true);
                } else {
                  encodedVal = encodeUriSegment(val);
                }
                url = url.replace(new RegExp(':' + urlParam + '(\\W|$)', 'g'), function(match, p1) {
                  return encodedVal + p1;
                });
              } else {
                url = url.replace(new RegExp('(/?):' + urlParam + '(\\W|$)', 'g'), function(match, leadingSlashes, tail) {
                  if (tail.charAt(0) === '/') {
                    return tail;
                  } else {
                    return leadingSlashes + tail;
                  }
                });
              }
            }
            queryParams = [];
            for (key in params) {
              value = params[key];
              if (!urlParams[key]) {
                if (angular.isObject(value) || angular.isArray(value)) {
                  queryParams.push(key + "=" + encodeUriQuery(JSON.stringify(value)));
                } else {
                  queryParams.push(key + "=" + value);
                }
              }
            }
            if (queryParams.length) {
              url = url + "?" + (queryParams.join('&'));
            }
            if (url[0] === '/') {
              url = "" + location.origin + url;
            }
            return url;
          };
          return resource;
        };
      }
    ]);
  }]);

}).call(this);
;
(function() {
  angular.module('ui.cms').run(["gettextCatalog", "$langPicker", "$http", "$rootScope", "$window", "$injector", "$location", "H", function(gettextCatalog, $langPicker, $http, $rootScope, $window, $injector, $location, H) {
    var $localeDynamic, CmsTranslations, host, iOS, iOSSafari, macOSSafari, rmOn, url, userAgent, webkit;
    if ($injector.has('indexConf')) {
      console.error("DEPRECATED: remove indexConf service!");
    }
    if ($injector.has('cmsFiltersConfig') || $injector.has('cmsFiltersConfigProvider')) {
      console.error("DEPRECATED: remove cmsFiltersConfig provider/factory!");
    }
    CmsTranslations = $injector.get('CmsTranslations');
    url = CmsTranslations.$actionToUrl('getByLangCodeForGettext').split('?')[0];
    $langPicker.setLanguageRemoteUrl(url + "?code=");
    if ($langPicker.setIgnoreLoadRemoteLanguages) {
      $langPicker.setIgnoreLoadRemoteLanguages([CMS_DEFAULT_LANG]);
    }
    if ($injector.has('$localeDynamic')) {
      $localeDynamic = $injector.get('$localeDynamic');
      $localeDynamic.setLocale(CMS_DEFAULT_LANG);
      rmOn = $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams) {
        if (toParams.lang) {
          $localeDynamic.setLocale(toParams.lang);
          rmOn();
        }
      });
      $rootScope.$on('$langPickerLangChanged', function(event, lang) {
        $localeDynamic.setLocale(lang);
      });
    }
    $http.defaults.transformRequest = function(data) {
      var fd;
      if (data === void 0) {
        return data;
      }
      if (data.$asFormData) {
        delete data.$asFormData;
        fd = new FormData;
        angular.forEach(data, function(value, key) {
          if (value instanceof FileList) {
            if (value.length === 1) {
              fd.append(key, value[0]);
            } else {
              angular.forEach(value, function(file, index) {
                fd.append(key + '_' + index, file);
              });
            }
          } else if (value instanceof File) {
            fd.append(key, value);
          } else {
            fd.append(key, angular.toJson(value));
          }
        });
        return fd;
      } else {
        return angular.toJson(data);
      }
    };

    /*
        добавляем к body классы указывающие на:
            * название домена
            * название ui-state из $state
            * запущен ли сайт в сафари
     */
    $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
      var c, classes, i, len, new_classes, ref;
      document.body.scrollTop = document.documentElement.scrollTop = 0;
      classes = document.body.className || '';
      new_classes = ['body-' + toState.name.split('.').join('-')];
      ref = classes.split(' ');
      for (i = 0, len = ref.length; i < len; i++) {
        c = ref[i];
        if (!c.includes('body-app-')) {
          new_classes.push(c);
        }
      }
      document.body.className = new_classes.join(' ');
    });
    userAgent = window.navigator.userAgent;
    iOS = !!userAgent.match(/iPad/i) || !!userAgent.match(/iPhone/i);
    webkit = !!userAgent.match(/WebKit/i);
    macOSSafari = !iOS && webkit && userAgent.match(/Safari/i) && !userAgent.match(/Chrome/i);
    iOSSafari = iOS && webkit && !userAgent.match(/CriOS/i);
    host = $location.host().split('.').join('-');
    document.body.className = (document.body.className || '') + (" domain-" + host);
    if (macOSSafari && !iOSSafari) {
      document.body.className += ' browser-safari-desktop';
    } else if (iOSSafari) {
      document.body.className += ' browser-safari-mobile';
    }
  }]);

}).call(this);
;
(function() {
  window.smoothScrollTo = function(element, duration, offset) {
    var _element, _to, getScrollTop, setScrollTop, smoothScrollTo;
    if (!offset) {
      offset = 0;
    }
    if (!duration) {
      duration = 100;
    }
    getScrollTop = function(htmlElement) {
      if (htmlElement.tagName.toLowerCase() === 'body') {
        return htmlElement.scrollTop || document.documentElement.scrollTop;
      }
      return htmlElement.scrollTop || 0;
    };
    setScrollTop = function(htmlElement, value) {
      htmlElement.scrollTop = value;
      if (htmlElement.tagName.toLowerCase() === 'body') {
        document.documentElement.scrollTop = value;
      }
    };
    smoothScrollTo = function(element, to, duration) {
      var difference, perTick;
      if (duration <= 0) {
        return;
      }
      difference = to - getScrollTop(element);
      perTick = difference / duration * 20;
      return setTimeout(function() {
        setScrollTop(element, element.scrollTop + perTick);
        if (getScrollTop(element) === to && element.tagName.toLowerCase() !== 'body') {
          if (element.scrollIntoView) {
            element.scrollIntoView({
              behavior: 'smooth'
            });
          }
          return;
        }
        return smoothScrollTo(element, to, duration - 20);
      }, 7);
    };
    _element = document.body;
    if (typeof element !== 'number') {
      _to = element.offsetTop;
    } else {
      _to = element;
    }
    return smoothScrollTo(_element, _to, duration);
  };

}).call(this);
;
angular.module('ui.cms').run(['gettextCatalog', function (gettextCatalog) {
/* jshint -W100 */
    gettextCatalog.setStrings('cz', {"Cache for google maps":"Cache pro google mapy","Cancel":"Zrušit","Cant submit form=(":"Nepodařilo se odeslat formu=(","Change file":"Změnit soubor","Change files":"Změnit soubory","Choose a date":"Vyberte datum","Clean":"Vyčistit","Contact us":"Kontaktujte nás","Currency":"Měna","Details":"Detaily","Error":"Chyba","File {{name}} is too big":"Soubor {{name}} je příliš velký","Flag that shows the authentication credentials should be remembered in localStorage across app/browser restarts":"Kontrola, která ukazuje, že authentication credentials měly by být zapamatovány v localStorage během restartování aplikace nebo prohlížeče","Gallery":"Galerie","Invalid file type. Allowed are: <b>{{options.accept}}</b>":"Neplatný typ souboru. Povolené jsou: <b>{{options.accept}}</b>","Loading data":"Načítání dat","Next":"Následující","No":"Ne","No file":"Žádný soubor","OK":"OK","Ooops":"Ups","Picture":"Obrázek","Please wait":"Vyčkejte prosím","Previous":"Předchozí","Required fields":"Požadovaná pole","Reset":"Resetovat","Reset filter":"Obnovit filtr","Reset search":"Obnovit vyhledávání","Reset value":"Resetovat hodnotu","Search by word":"Vyhledávat podle slova","Select":"Vybrat","Select country":"Vybrat zemi","Select file":"Vyberte soubor","Select files":"Vyberte sobory","Select location":"Vybrat umístění","Select timezone":"Vyberte časové pásmo","Set location":"Nastavit umístění","Submit":"Odeslat","Success":"Úspěšně","Text":"Text","Thank you! Email was send":"Děkuji! E-mail byl odeslán","There's nothing in here":"Zde nic není","Try again":"Zkusit znovu","Type to search location":"Začněte psát pro vyhledávání podle umístění","User authentication token":"Token ověřování uživatele","User id":"Uživatelské ID","Warning":"Upozornění","World maps are disabled by admin":"Mapy světa jsou zakázány administrátorem","Yes":"Ano","Your email":"Váš email","Your name":"Vaše jméno","Your search - <b>\"{{searchTerm}}\"</b> - did not match any documents":"Vašemu vyhledávání - <b>\"{{searchTerm}}\"</b> - neodpovídají žádné dokumenty","Your search did not match any documents":"Na vyžádání nebylo nalezeno nic","d":{"small duration text":["d","d","d"]},"day":{"medium duration text":["den","dny","dní"]},"h":{"small duration text":["h","h","h"]},"hour":{"medium duration text":["hodina","hodiny","hodin"]},"m":{"small duration text":["m","m","m"]},"minute":{"medium duration text":["minuta","minuty","minut"]},"month":{"medium duration text":["měsíc","měsíce","měsíce"]},"now":"nyní","reset":"resetovat","s":{"small duration text":["s","s","s"]},"second":{"medium duration text":["sekunda","sekundy","sekund"]},"y":{"small duration text":["r","r","r"]},"year":{"medium duration text":["rok","lety","lety"]}});
    gettextCatalog.setStrings('de', {"Cant submit form=(":"Das Formular kann nicht eingereicht werden=(","Change file":"Datei ändern","Change files":"Dateien ändern","Contact us":"Kontaktiere uns","Currency":"Währung","Details":"Weiterlesen","Gallery":"Galerie","No file":"Keine Datei","Ooops":"Ooops","Please wait":"Bitte warten","Submit":"Senden","Text":"Text","Too old browser(or IE < 10)":"Zu alter Browser (oder IE <10)","Try again":"Bitte versuche es nochmal","Your email":"Ihren E-Mail","Your name":"Ihre Name"});
    gettextCatalog.setStrings('ru', {"Cache for google maps":"Кеш для google maps","Cancel":"Отменить","Cant submit form=(":"Отправить форму не получилось=(","Change file":"Изменить файл","Change files":"Изменить файлы","Choose a date":"Выберите дату","Clean":"Очистить","Contact us":"Свяжитесь с нами","Currency":"Валюта","Details":"Подробнее","Error":"Ошибка","File {{name}} is too big":"Файл {{name}} слишком большой","Flag that shows the authentication credentials should be remembered in localStorage across app/browser restarts":"Флаг отображающий где сохраняются пользовательские данные о логине (localStorage/sessionStorage)","Gallery":"Галерея","Invalid file type. Allowed are: <b>{{options.accept}}</b>":"Неверный тип файла. Разрешены только: <b>{{options.accept}}</b>","Loading data":"Загрузка данных","Next":"Следующая","No":"Нет","No file":"Нет файла","Ooops":"Ой","Photo by {{creator}}":"Фотограф {{creator}}","Picture":"Картинка","Please wait":"Пожалуйста, подождите","Previous":"Предыдущая","Required fields":"Обязательные к заполнению поля","Reset":"Сбросить","Reset filter":"Сбросить фильтр","Reset search":"Сбросить поиск","Reset value":"Сбросить значение","Search by word":"Поиск по слову","Select":"Выбрать","Select country":"Выберите страну","Select file":"Выберите файл","Select files":"Выберите файлы","Select location":"Выберите местоположение","Select timezone":"Выберите часовой пояс","Set location":"Указать местоположение","Submit":"Отправить","Success":"Успешно","Text":"Текст","Thank you! Email was send":"Спасибо! Email отправлен","There's nothing in here":"Объектов нет","Try again":"Попытаться снова","Type to search location":"Наберите текст чтоб начать поиск по местоположениям","User authentication token":"Auth-token пользователя","User id":"id пользователя","Warning":"Предупреждение","World maps are disabled by admin":"Карты отключены админом","Yes":"Да","Your email":"Ваш почтовый ящик","Your name":"Ваше имя","Your search - <b>\"{{searchTerm}}\"</b> - did not match any documents":"По запросу <b>\"{{searchTerm}}\"</b> ничего не найдено","Your search did not match any documents":"По запросу ничего не найдено","d":{"small duration text":["д","д","д"]},"day":{"medium duration text":["день","дня","дней"]},"h":{"small duration text":["ч","ч","ч"]},"hour":{"medium duration text":["час","часа","часов"]},"m":{"small duration text":["мин","мин","мин"]},"minute":{"medium duration text":["минута","минуты","минут"]},"month":{"medium duration text":["месяц","месяца","месяцев"]},"now":"сейчас","reset":"сбросить","s":{"small duration text":["сек","сек","сек"]},"second":{"medium duration text":["секунда","секунды","секунд"]},"y":{"small duration text":["г","г","л"]},"year":{"medium duration text":["год","года","лет"]}});
/* jshint +W100 */
}]);;
(function() {
  angular.module('ui.cms').directive('uicvArticle', function() {
    return {

      /**
      *   @ngdoc directive
      *   @name ui.cms.directive:uicvArticle
      *   @description отображает один объект CmsArticle. Для
      *       переопределения шаблона см {@link ui.cms.$directiveOverrideProvider}
      *
      *   @restrict E
      *   @param {object} item (ссылка) объект CmsArticle
      *   @param {string=} forceLang (ссылка) код языка. Принудительное отображение объекта в языке forceLang(напр. "en", "ru", "de") вместо пользовательского.
       */
      restrict: 'E',
      scope: {
        item: '=',
        forceLang: '=?'
      },
      controller: ["$scope", "$cms", function($scope, $cms) {
        return $scope.$cms = $cms;
      }],
      template: ('/client/cms_app/viewDirectives/uicvArticle/uicvArticle.html', '<div ng-if="forceLang"><div class="title-block lg"><h1 class="text-center">{{item.title[forceLang]}}</h1><p>{{item.created | date}}</p></div><uic-picture class="pull-right col-lg-50 col-sm-100" file="item.mainImg" ng-if="item.mainImg"></uic-picture><div class="body-block" uic-bind-html="item.body[forceLang]"></div></div><div ng-if="!forceLang"><div class="title-block lg"><h1 class="text-center">{{item.title | l10n}}</h1><p>{{item.created | date}}</p></div><uic-picture class="pull-right col-lg-50 col-sm-100" file="item.mainImg" ng-if="item.mainImg"></uic-picture><div class="body-block" uic-bind-html="item.body | l10n"></div></div>' + '')
    };
  });

}).call(this);
;
(function() {
  angular.module('ui.cms').directive('uicvSiteReview', function() {
    return {

      /**
      *   @ngdoc directive
      *   @name ui.cms.directive:uicvSiteReview
      *   @description отображает один объект CmsSiteReview. Для
      *       переопределения шаблона см {@link ui.cms.$directiveOverrideProvider}
      *
      *   @restrict E
      *   @param {object} item (ссылка) объект CmsSiteReview
      *   @param {string=} forceLang (ссылка) код языка. Принудительное отображение объекта в языке forceLang(напр. "en", "ru", "de") вместо пользовательского.
      *   @param {string=} itemClass (значение) css класс для lw-reviews-object
       */
      restrict: 'E',
      scope: {
        item: '=',
        forceLang: '=?',
        itemClass: '@?'
      },
      controller: ["$scope", function($scope) {}],
      template: ('/client/cms_app/viewDirectives/uicvSiteReview/uicvSiteReview.html', '<lw-quote-bubble class="{{itemClass || \'\'}}" ng-if="forceLang" img-url="{{item.mainImgUrl.urlL10n | l10n:forceLang}}" img-l10n-file="item.mainImg" title="{{item.title[forceLang]}}" sub-title="{{item.description[forceLang] || item.importedFrom}}" ng-href="{{item.importedUrl}}"><blockquote>{{item.body[forceLang]}}</blockquote></lw-quote-bubble><lw-quote-bubble class="{{itemClass || \'\'}}" ng-if="!forceLang" img-url="{{item.mainImgUrl.urlL10n | l10n}}" img-l10n-file="item.mainImg" title="{{item.title | l10n}}" sub-title="{{(item.description | l10n) || item.importedFrom}}" ng-href="{{item.importedUrl}}"><blockquote>{{item.body | l10n}}</blockquote></lw-quote-bubble>' + '')
    };
  });

}).call(this);
;

/*
        Модуль дает обертку над window.BaseModelClass для ngResource моделей
        от cms (которые объявлены в lb.services.js).
        Практически, BaseModelClass предоставляет временное "хранилище" для данных запро
        шенных от сервера в озу.
 */

(function() {
  angular.module('cms.models', ['lbServices', 'ui.router', 'ui.cms']);

}).call(this);
;

/*
    как использовать:
        1. файл должен быть подключен до того, как будет загружен модуль
        который его использует в ангуларе.
        2. обязательно указывать $inject если используется как сервис



    **** Использование как сервиса:

    .service 'MyModel',
        class MyModel extends window.BaseModelClass
            @$inject: ['$injector', 'CmsMyModel']
            constructor: (@$injector, CmsMyModel) ->
                super(CmsMyModel, @$injector)

    теперь можно везде импортировать сервис MyModel



    **** Использование как экземпляра класса:

    MyModel = new window.BaseModelClass(CmsMyModel, $injector)

    теперь везде можно использовать потомка MyModel
 */

(function() {
  var BaseModelClass,
    bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
    extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  BaseModelClass = (function(superClass) {
    extend(BaseModelClass, superClass);

    function BaseModelClass(ngResourceModel, $injector, defaults) {
      this.safeRemoveObj = bind(this.safeRemoveObj, this);
      this.safeMergeObjWithArray = bind(this.safeMergeObjWithArray, this);
      this.safeMergeArrays = bind(this.safeMergeArrays, this);
      this.orderItems = bind(this.orderItems, this);
      this.deleteItemById = bind(this.deleteItemById, this);
      this.createItem = bind(this.createItem, this);
      this.updateItem = bind(this.updateItem, this);
      this.getItemById = bind(this.getItemById, this);
      this.findItemByField = bind(this.findItemByField, this);
      this.loadItemById = bind(this.loadItemById, this);
      this.loadItemsByOwner = bind(this.loadItemsByOwner, this);
      this.loadItems = bind(this.loadItems, this);
      this.r = ngResourceModel;
      this.$q = $injector.get('$q');
      this.$filter = $injector.get('$filter');
      this.defaults = defaults || {
        loadItems: {
          filter: {}
        }
      };
      this.items = [];
      this.item = {};
    }

    BaseModelClass.prototype.loadItems = function(params, func_name) {
      var deferred;
      deferred = this.$q.defer();
      this.loading = true;
      this.error = '';
      this.items = this.items || [];
      if (!params) {
        params = {};
        if (this.defaults.loadItems) {
          params = this.defaults.loadItems;
        }
      }
      if (!func_name) {
        func_name = 'query';
      }
      this.r[func_name](params, (function(_this) {
        return function(data) {
          _this.items = angular.copy(data);
          _this.loading = false;
          return deferred.resolve(data);
        };
      })(this), (function(_this) {
        return function(data) {
          _this.items.length = 0;
          _this.error = "Can't retrieve objects";
          _this.loading = false;
          return deferred.reject(data);
        };
      })(this));
      return deferred.promise;
    };

    BaseModelClass.prototype.loadItemsByOwner = function(ownerId, params) {
      if (params == null) {
        params = {};
      }
      params.filter = params.filter || {};
      params.filter.where = params.filter.where || {};
      params.filter.where.ownerId = ownerId;
      return this.loadItems(params);
    };

    BaseModelClass.prototype.loadItemById = function(id, params) {
      var deferred;
      if (params == null) {
        params = {};
      }
      deferred = this.$q.defer();
      this.loading_item = true;
      this.error = '';
      this.item = {};
      params.id = id;
      this.r.findById(params, (function(_this) {
        return function(data) {
          _this.item = angular.copy(data);
          _this.loading_item = false;
          return deferred.resolve(data);
        };
      })(this), (function(_this) {
        return function(data) {
          _this.item = angular.copy({});
          _this.error = "Can't retrieve object";
          _this.loading_item = false;
          return deferred.reject(data);
        };
      })(this));
      return deferred.promise;
    };

    BaseModelClass.prototype.filterItemsByField = function(field, value) {
      var j, len, obj, ref, results;
      if (!this.items) {
        return null;
      }
      ref = this.items;
      results = [];
      for (j = 0, len = ref.length; j < len; j++) {
        obj = ref[j];
        if (obj[field] === value) {
          results.push(angular.copy(obj));
        }
      }
      return results;
    };

    BaseModelClass.prototype.findItemByField = function(field, value) {
      var j, len, obj, ref, res;
      if (!this.items) {
        return null;
      }
      res = null;
      ref = this.items;
      for (j = 0, len = ref.length; j < len; j++) {
        obj = ref[j];
        if (obj[field] === value) {
          res = obj;
          break;
        }
      }
      return res;
    };

    BaseModelClass.prototype.getItemById = function(id) {
      var item;
      if (this.item && this.item.id === id) {
        return this.item;
      }
      item = this.findItemByField('id', id);
      if (!item) {
        return this.loadItemById(id);
      }
      return item;
    };

    BaseModelClass.prototype.updateItem = function(item) {
      var deferred, updateAttributes;
      deferred = this.$q.defer();
      updateAttributes = this.r.updateAttributes || this.r.update;
      updateAttributes({
        id: item.id
      }, item, (function(_this) {
        return function(data) {
          var index, j, len, o, ref;
          ref = _this.items;
          for (index = j = 0, len = ref.length; j < len; index = ++j) {
            o = ref[index];
            if (o.pk === data.pk) {
              _this.items[index] = angular.copy(item);
              break;
            }
          }
          return deferred.resolve(data);
        };
      })(this), (function(_this) {
        return function(data) {
          _this.error = "Can't update object with id " + item.id;
          return deferred.reject(data);
        };
      })(this));
      return deferred.promise;
    };

    BaseModelClass.prototype.createItem = function(item) {
      var deferred;
      deferred = this.$q.defer();
      this.loading_create = true;
      this.error = '';
      this.r.create({}, item, (function(_this) {
        return function(data) {
          if (!_this.items) {
            _this.items = [];
          }
          _this.items.push(data);
          _this.loading_create = false;
          return deferred.resolve(data);
        };
      })(this), (function(_this) {
        return function(data) {
          _this.error = "Can't create object";
          _this.loading_create = false;
          return deferred.reject(data);
        };
      })(this));
      return deferred.promise;
    };

    BaseModelClass.prototype.deleteItemById = function(id) {
      var deferred;
      deferred = this.$q.defer();
      this.r.deleteById({
        id: id
      }, (function(_this) {
        return function(data) {
          var index, j, len, obj, ref;
          ref = _this.items;
          for (index = j = 0, len = ref.length; j < len; index = ++j) {
            obj = ref[index];
            if (obj.id === id) {
              _this.items.splice(index, 1);
              break;
            }
          }
          return deferred.resolve();
        };
      })(this), (function(_this) {
        return function(data) {
          _this.error = "Can't delete object with id " + id;
          return deferred.reject();
        };
      })(this));
      return deferred.promise;
    };

    BaseModelClass.prototype.orderItems = function(field, reverse) {
      var loadItems, orderBy, ref;
      if (!field) {
        loadItems = this.defaults.loadItems;
        if (!loadItems) {
          return;
        }
        if (loadItems.hasOwnProperty('filter')) {
          if (loadItems.filter.order) {
            ref = loadItems.filter.order.split(' '), field = ref[0], reverse = ref[1];
            if (reverse === 'DESC') {
              reverse = false;
            } else {
              reverse = true;
            }
          } else {
            return;
          }
        }
      }
      orderBy = this.$filter('orderBy');
      return this.items = orderBy(this.items, field, reverse);
    };

    BaseModelClass.prototype.safeMergeObjs = function(propertyName, item) {
      if (!angular.isObject(this[propertyName])) {
        return console.error("Can't call safeMergeObjWithArray method for " + propertyName + " property. Not an object.");
      }
      if (this[propertyName].id === item.id && item.id) {
        this[propertyName] = angular.merge(this[propertyName], item);
      }
      return this[propertyName];
    };

    BaseModelClass.prototype.safeMergeArrays = function(propertyName, items, addToEndIfNotExists) {
      var i, item, j, k, len, len1, m, ref;
      if (!(this[propertyName] instanceof Array)) {
        return console.error("Can't call safeMergeArrays method for " + propertyName + " property. Not an array.");
      }
      if (addToEndIfNotExists === null || addToEndIfNotExists === (void 0)) {
        addToEndIfNotExists = true;
      }
      m = {};
      ref = this[propertyName];
      for (i = j = 0, len = ref.length; j < len; i = ++j) {
        item = ref[i];
        m[item.id] = i;
      }
      for (k = 0, len1 = items.length; k < len1; k++) {
        item = items[k];
        if (m.hasOwnProperty(item.id)) {
          this[propertyName][m[item.id]] = angular.merge(this[propertyName][m[item.id]], item);
        } else if (addToEndIfNotExists) {
          this[propertyName].push(item);
        }
      }
      if (propertyName === 'items') {
        this.orderItems();
      }
      return this[propertyName];
    };

    BaseModelClass.prototype.safeMergeObjWithArray = function(propertyName, obj, addToEndIfNotExists) {
      var i, item, j, len, merged, ref;
      if (!(this[propertyName] instanceof Array)) {
        return console.error("Can't call safeMergeObjWithArray method for " + propertyName + " property. Not an array.");
      }
      if (addToEndIfNotExists === null || addToEndIfNotExists === (void 0)) {
        addToEndIfNotExists = true;
      }
      merged = false;
      ref = this[propertyName];
      for (i = j = 0, len = ref.length; j < len; i = ++j) {
        item = ref[i];
        if (item.id === obj.id) {
          this[propertyName][i] = angular.merge(item, obj);
          merged = true;
          break;
        }
      }
      if (!merged && addToEndIfNotExists) {
        this[propertyName].push(obj);
      }
      if (propertyName === 'items') {
        this.orderItems();
      }
      return this[propertyName];
    };

    BaseModelClass.prototype.safeRemoveObj = function(propertyName, obj) {
      var i, item, j, len, ref;
      if (!this[propertyName]) {
        return;
      }
      if (angular.isObject(this[propertyName])) {
        this[propertyName] = {};
      } else if (angular.isArray(this[propertyName])) {
        ref = this[propertyName];
        for (i = j = 0, len = ref.length; j < len; i = ++j) {
          item = ref[i];
          if (!(obj.id === item.id && obj.id)) {
            continue;
          }
          this[propertyName].remove(i);
          break;
        }
      }
    };

    BaseModelClass.prototype.getStorageProperties = function() {
      var fn;
      fn = Object.getOwnPropertyNames || Object.keys;
      return fn(this).filter((function(_this) {
        return function(propertyName) {
          return !angular.isFunction(_this[propertyName]) && !['defaults', 'r'].includes(propertyName) && !angular.isString(_this[propertyName]);
        };
      })(this));
    };

    return BaseModelClass;

  })(Object);

  window.BaseModelClass = BaseModelClass;

}).call(this);
;
(function() {
  angular.module('cms.models').filter('bulkPagination', ["$filter", function($filter) {
    return function(baseModelClass, page, itemsPerPage) {

      /*
          всего лишь возвращает "страницу" объектов загруженную $bulk.
          фильтр ничего не вернет, если страница предварительно не была загружена!
          фильтр всего лишь сахар для конструкций вида:
      
              $models.CmsNews["itemsForPage_#{currentPage}_by#{itemsPerPage}"]
      
          которые можно записать в html так:
      
              {{ $models.CmsNews | bulkPagination:2:30 }}
       */
      var limitTo, prop;
      if (!itemsPerPage) {
        itemsPerPage = 10;
      }
      if (!page) {
        page = 0;
      }
      prop = "itemsForPage_" + page + "_by" + itemsPerPage;
      if (baseModelClass.hasOwnProperty(prop)) {
        return baseModelClass[prop];
      }
      if (baseModelClass.count && baseModelClass.items) {
        if (baseModelClass.count === baseModelClass.items.length) {
          limitTo = $filter('limitTo');
          return limitTo(baseModelClass.items, itemsPerPage, page * itemsPerPage);
        }
      }
      console.warn("No such page #" + page + " with " + itemsPerPage + " items per page.");
      return [];
    };
  }]);

}).call(this);
;
(function() {
  angular.module('cms.models').filter('findItemByIdInModels', ["$models", function($models) {
    return function(id, modelPath) {
      var i, item, items, len;
      if (!id || !modelPath) {
        return;
      }
      items = angular.getValue($models, modelPath) || [];
      if (!items || !items.length) {
        return;
      }
      for (i = 0, len = items.length; i < len; i++) {
        item = items[i];
        if (item.id === id) {
          return item;
        }
      }
    };
  }]);

}).call(this);
;
(function() {
  angular.module('cms.models').provider('$bulk', function() {

    /*
        $bulk - сервис для загрузки c сервера разных моделей через
            одну точку входа - /api/v1/Helpers/bulk.
            Не стоит использовать его для POST, PUT, PATCH, DELETE запросов,
            его основное предназначение - минимализация количества GET
            запросов к cms серверу от сайтов, а также "декларативное" описание в json-структуре
            "что нужно загружать с сервера cms" вместо "что и КАК загружать с сервера"
            описанного в js коде котроллеров.
    
    
        Переменная $bulk.ready будет равна true после первого удачного GET запроса (ее
        имеет смысл использовать для отображения крутилки загрузки сайта).
        Для наблюдения за сервисом лучше использовать $bulk.loading и $bulk.error.
    
    
        пример в state:
    
        controller: 'BlaBlaView',
        $bulk: (defaults)->
          {
              "CmsNews": {
                    "find": {
                       "filter": {"where": {"isPublished": true}},
                       "$mergeWithArray": "myItems"   # $bulk смешает данные с сервера с $models.CmsNews.myItems
                    }
                },
              "CmsReviews": {"find": true}  # так мы показываем, что метод будет запущен без параметров
              "CmsArticle": {"find": defaults.CmsArticle.loadItems }  # параметры по умолчанию которые были зарегестрированны в $models
          }
     */
    var bulk, fixModelNameForData, initBulkService, maxAttempt;
    this.defaultOperations = {};
    maxAttempt = 2;
    bulk = null;
    fixModelNameForData = function(data, modelName) {
      var i, item, j, len;
      if (data instanceof Array) {
        for (i = j = 0, len = data.length; j < len; i = ++j) {
          item = data[i];
          if (item) {
            data[i].__proto__.constructor.modelName = modelName;
          }
        }
      } else if (typeof data === 'object') {
        data.__proto__.constructor.modelName = modelName;
      }
      return data;
    };
    initBulkService = (function(_this) {
      return function($q, $injector) {
        var $cms, $filter, $models, $rootScope, $state, $timeout, H, Helpers, LoopBackAuth, firstRun, l10nFilter, translateFilter;
        bulk = {
          ready: false,
          loading: false,
          error: false
        };
        H = $injector.get('H');
        Helpers = $injector.get('Helpers');
        $filter = $injector.get('$filter');
        $cms = $injector.get('$cms');
        $models = $injector.get('$models');
        $state = $injector.get('$state');
        $timeout = $injector.get('$timeout');
        $rootScope = $injector.get('$rootScope');
        LoopBackAuth = $injector.get('LoopBackAuth');
        l10nFilter = $filter('l10n');
        translateFilter = $filter('translate');
        bulk.execute = function(operations, special) {
          var attempt, deferred, fetchOperationsData, getOperationParams, initPropForModel, key, params, prop, remoteMethod, val;
          bulk.loading = true;
          bulk.error = false;
          attempt = 1;
          deferred = $q.defer();
          getOperationParams = function(modelName, remoteMethod) {
            var params;
            params = operations[modelName][remoteMethod];
            if (typeof params !== 'object') {
              return {};
            }
            return params;
          };
          initPropForModel = function(modelName, propertyName, defaultValue) {
            if (!$models[modelName].hasOwnProperty(propertyName)) {
              $models[modelName][propertyName] = angular.copy(defaultValue);
            }
          };
          fetchOperationsData = function() {
            return Helpers.bulk({
              data: operations
            }, function(data) {
              var i, j, len, modelName, modelNames, params, remoteMethod, res, value;
              modelNames = Object.keys(data.data);
              i = modelNames.indexOf('CmsSettings');
              if (i > -1) {
                modelNames.remove(i);
                modelNames.unshift('CmsSettings');
              }
              for (j = 0, len = modelNames.length; j < len; j++) {
                modelName = modelNames[j];
                value = data.data[modelName];
                for (remoteMethod in value) {
                  res = value[remoteMethod];
                  params = getOperationParams(modelName, remoteMethod);
                  res = fixModelNameForData(res, modelName);
                  if (modelName === 'CmsSettings' && (remoteMethod === 'findOne' || remoteMethod === 'findById')) {
                    $cms = $injector.get('$cms');
                    $cms.loadSettings(res);
                  }
                  if (params.$mergeWithArray) {
                    initPropForModel(modelName, params.$mergeWithArray, []);
                    if (res instanceof Array) {
                      $models[modelName].safeMergeArrays(params.$mergeWithArray, res);
                    } else {
                      $models[modelName].safeMergeObjWithArray(params.$mergeWithArray, res);
                    }
                  }
                  if (params.$mergeWithObj) {
                    initPropForModel(modelName, params.$mergeWithObj, {});
                    $models[modelName][params.$mergeWithObj] = angular.merge($models[modelName][params.$mergeWithObj], res);
                  }
                  if (params.$replaceObj) {
                    initPropForModel(modelName, params.$replaceObj, {});
                    $models[modelName][params.$replaceObj] = angular.merge($models[modelName][params.$replaceObj], res);
                  }
                  if (params.$replaceArray) {
                    initPropForModel(modelName, params.$replaceArray, []);
                    $models[modelName][params.$replaceArray] = angular.merge($models[modelName][params.$replaceArray], res);
                  }
                  if (!params.$mergeWithObj && !params.$mergeWithArray) {
                    $models[modelName][remoteMethod] = res;
                  }
                }
              }
              if (Object.keys(data.error).length) {
                console.error('$bulk: error on operations.');
                console.error(data.error);
              }
              bulk.ready = true;
              bulk.loading = false;
              $rootScope.$emit('$bulkSuccess', {
                operations: operations,
                data: data,
                special: special || {}
              });
              return deferred.resolve(data);
            }, function(data) {
              if (attempt < maxAttempt) {
                LoopBackAuth.clearUser();
                LoopBackAuth.clearStorage();
                attempt += 1;
                return fetchOperationsData();
              } else {
                console.error('$bulk: error');
                console.error(data.data.error);
                bulk.loading = false;
                bulk.error = data.data.error;
                $rootScope.$emit('$bulkFail', {
                  operations: operations,
                  data: data,
                  special: special || {}
                });
                return deferred.reject(data);
              }
            });
          };
          for (key in operations) {
            val = operations[key];
            if (!$models.hasOwnProperty(key)) {
              $models.registerModel(key, val);
            }
            for (remoteMethod in val) {
              params = val[remoteMethod];
              if (typeof params !== 'object') {
                continue;
              }
              prop = $models[key][params.$mergeWithArray];
              if (angular.isFunction(prop)) {
                throw "$bulk: You can't merge to $model." + key + "." + params.$mergeWithArray + "! It's a function.";
              }
              prop = $models[key][params.$mergeWithObj];
              if (angular.isFunction(prop)) {
                throw "$bulk: You can't merge to $model." + key + "." + params.$mergeWithObj + "! It's a function.";
              }
              if (params.$mergeWithArray && !$models[key].hasOwnProperty(params.$mergeWithArray)) {
                $models[key][params.$mergeWithArray] = angular.copy([]);
              } else if (params.$mergeWithObj && !$models[key].hasOwnProperty(params.$mergeWithObj)) {
                $models[key][params.$mergeWithObj] = angular.copy({});
              } else if (params.$replaceObj) {
                $models[key][params.$replaceObj] = angular.copy({});
              } else if (params.$replaceArray) {
                $models[key][params.$replaceArray] = angular.copy([]);
              }
            }
          }
          fetchOperationsData();
          return deferred.promise;
        };
        bulk.onReady = function(func, onErr) {
          var rmOn, rmOnErr, runFn;
          func = func || angular.noop;
          onErr = onErr || angular.noop;
          if (!bulk.loading && bulk.ready) {
            func();
            return;
          }
          runFn = function(special, fn) {
            if (special.fromStateChangeStart) {
              if (special.toStateName === $state.current.name) {
                fn();
                rmOn();
                rmOnErr();
                return;
              }
              $timeout(function() {
                if (special.toStateName === $state.current.name) {
                  fn();
                }
                rmOn();
                rmOnErr();
              }, 500);
            } else {
              fn();
              rmOn();
              rmOnErr();
            }
          };
          rmOn = $rootScope.$on('$bulkSuccess', function(ev, response) {
            runFn(response.special, func);
          });
          rmOnErr = $rootScope.$on('$bulkFail', function(ev, response) {
            runFn(response.special, onErr);
          });
        };

        /*
            для того, чтоб при логине/разлогине, в одной вкладке, у разных пользователей
            не получалось так, что новые данные смешиваются со старым кешем:
                * сбрасываем состояние (кеш) в $models
                * сбрасываем состояние $bulk в начальное положение (будто вкладку только открыли)
            при этом ОБЩИЙ ВНУТРЕННИЙ КЕШ НЕ СБРАСЫВАЕТСЯ.
         */
        firstRun = true;
        bulk.reset = function() {
          $models.reset();
          firstRun = true;
        };
        $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams, options) {
          var defaults, operation_func, operations;
          operation_func = toState.$bulk || function() {
            return {};
          };
          defaults = $models.getDefaults();
          operations = operation_func(defaults, toParams, $models, bulk, H);
          if (firstRun || (!fromState.name && fromState.abstract)) {
            operations = angular.merge(_this.defaultOperations, operations);
            firstRun = false;
          }
          if (operations && Object.keys(operations).length) {
            bulk.execute(operations, {
              fromStateChangeStart: true,
              toStateName: toState.name
            });
          }
        });
        return bulk;
      };
    })(this);
    this.$get = [
      '$q', '$injector', function($q, $injector) {
        if (!bulk) {
          return initBulkService($q, $injector);
        }
        return bulk;
      }
    ];
    return this;
  });

}).call(this);
;
(function() {
  var Cache, MODEL_CACHE, MODEL_NAMES, RESOURCE_PIPE_TRANSFORM, RESOURCE_RELATIONS, RelationsResolver, toSafeObject,
    hasProp = {}.hasOwnProperty,
    extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    indexOf = [].indexOf || function(item) { for (var i = 0, l = this.length; i < l; i++) { if (i in this && this[i] === item) return i; } return -1; };

  toSafeObject = function(obj) {
    var k, safe_obj, v;
    if (obj && obj.__proto__ && obj.__proto__.constructor) {
      if (obj.__proto__.constructor.name === 'Object') {
        return obj;
      }
      safe_obj = {};
      for (k in obj) {
        if (!hasProp.call(obj, k)) continue;
        v = obj[k];
        if (!['$promise', '$resolved'].includes(k)) {
          safe_obj[k] = v;
        }
      }
      return safe_obj;
    }
    return obj;
  };

  Cache = (function() {
    function Cache(resourceNames) {
      var l, len, n, ref;
      ref = resourceNames || [];
      for (l = 0, len = ref.length; l < len; l++) {
        n = ref[l];
        this.initResource(n);
      }
      return;
    }

    Cache.prototype.initResource = function(resourceName) {
      this[resourceName] = {};
    };

    Cache.prototype.addItem = function(resourceName, item) {
      if (!item || !item.id) {
        return;
      }
      if (!this[resourceName][item.id]) {
        this[resourceName][item.id] = toSafeObject(item);
      } else {
        this[resourceName][item.id] = angular.merge(this[resourceName][item.id], toSafeObject(item));
      }
    };

    Cache.prototype.getItem = function(resourceName, id) {
      if (this[resourceName]) {
        return this[resourceName][id];
      }
      return void 0;
    };

    Cache.prototype.hasResource = function(resourceName) {
      return !!this[resourceName];
    };

    Cache.prototype.clean = function() {
      var k, ref, v;
      ref = this;
      for (k in ref) {
        v = ref[k];
        if (!['add', 'clean'].includes(k)) {
          this[k] = {};
        }
      }
    };

    return Cache;

  })();

  RelationsResolver = (function() {
    function RelationsResolver(relations) {
      this.ONE_TO_ONE = ['hasOne', 'belongsTo'];
      this.WEAK_ONE_TO_ONE = ['weakHasOne', 'weakBelongsTo'];
      this.MANY_TO_ONE = ['hasMany'];
      this.WEAK_MANY_TO_ONE = ['weakHasMany'];
      this.setRelations(relations || {});
    }

    RelationsResolver.prototype.setRelations = function(relations) {
      var f, fields, findPriority, i, l, len, resourceName;
      this.relations = {};
      this.priorities = [];
      findPriority = function(name, minPriority) {
        var field, l, len, priority, ref;
        if (minPriority === 100) {
          return 100;
        }
        priority = [minPriority];
        ref = relations[name] || [];
        for (l = 0, len = ref.length; l < len; l++) {
          field = ref[l];
          if (relations[field.model]) {
            priority.push(findPriority(field.model, minPriority + 1));
          }
        }
        return Math.max.apply(null, priority);
      };
      for (resourceName in relations) {
        fields = relations[resourceName];
        for (i = l = 0, len = fields.length; l < len; i = ++l) {
          f = fields[i];
          fields[i].type = f.type || 'hasOne';
          if (this.WEAK_ONE_TO_ONE.includes(fields[i].type) || this.WEAK_MANY_TO_ONE.includes(fields[i].type)) {
            fields[i].alias = f.field.split('.')[0] + '.' + f.weakAlias;
          }
        }
        this.relations[resourceName] = {
          priority: findPriority(resourceName, 0),
          fields: fields
        };
        if (!this.priorities.includes(this.relations[resourceName].priority)) {
          this.priorities.push(this.relations[resourceName].priority);
        }
      }
      this.priorities.sort(function(a, b) {
        return a - b;
      });
    };

    RelationsResolver.prototype.hasRelations = function(resourceName) {
      if (resourceName) {
        return !!this.relations[resourceName];
      }
      return !angular.isEmpty(this.relations);
    };

    RelationsResolver.prototype._resolveComplexWeakRelation = function(item, relation, ignoreAddToCache) {
      var i, l, len, level1, level2, obj, ref, resolveValue, value;
      if (!MODEL_CACHE.hasResource(relation.model)) {
        return;
      }
      if (this.WEAK_ONE_TO_ONE.includes(relation.type)) {
        resolveValue = (function(_this) {
          return function(fieldValue) {
            var resolved;
            if (!fieldValue) {
              return;
            }
            resolved = MODEL_CACHE.getItem(relation.model, fieldValue);
            if (resolved) {
              return _this.resolve(relation.model, resolved, ignoreAddToCache);
            }
          };
        })(this);
      } else if (this.WEAK_MANY_TO_ONE.includes(relation.type)) {
        resolveValue = (function(_this) {
          return function(fieldValue) {
            var aliasValue, id, l, len, ref, resolved;
            if (!fieldValue) {
              return;
            }
            aliasValue = [];
            ref = fieldValue || [];
            for (l = 0, len = ref.length; l < len; l++) {
              id = ref[l];
              resolved = MODEL_CACHE.getItem(relation.model, id);
              if (resolved) {
                resolved = _this.resolve(relation.model, resolved, ignoreAddToCache);
                aliasValue.push(resolved);
              }
            }
            return aliasValue;
          };
        })(this);
      }
      level1 = relation.field.split('.')[0].replace('[X]', '').replace('[x]', '');
      level2 = relation.field.split('.')[1];
      ref = angular.getValue(item, level1) || [];
      for (i = l = 0, len = ref.length; l < len; i = ++l) {
        obj = ref[i];
        if (!(obj)) {
          continue;
        }
        value = resolveValue(obj[level2]);
        if (value) {
          item[level1][i][relation.weakAlias] = value;
        } else {
          delete item[level1][i][relation.weakAlias];
        }
      }
    };

    RelationsResolver.prototype.resolve = function(resourceName, item, ignoreAddToCache) {
      var aliasValue, fieldValue, id, l, len, len1, o, ref, ref1, relation, resolved;
      if (!this.relations[resourceName] || !item) {
        return item;
      }
      ref = this.relations[resourceName].fields;
      for (l = 0, len = ref.length; l < len; l++) {
        relation = ref[l];
        if (item.hasOwnProperty(relation.alias)) {
          continue;
        }
        if (this.WEAK_ONE_TO_ONE.includes(relation.type) || this.WEAK_MANY_TO_ONE.includes(relation.type)) {
          if (relation.field.indexOf('[X]') > -1 || relation.field.indexOf('[x]') > -1) {
            this._resolveComplexWeakRelation(item, relation, ignoreAddToCache);
            continue;
          }
        }
        fieldValue = angular.getValue(item, relation.field, void 0);
        if (!MODEL_CACHE.hasResource(relation.model) || fieldValue === void 0) {
          continue;
        }
        if (this.ONE_TO_ONE.includes(relation.type) || this.WEAK_ONE_TO_ONE.includes(relation.type)) {
          resolved = MODEL_CACHE.getItem(relation.model, fieldValue);
          if (resolved) {
            resolved = this.resolve(relation.model, resolved, ignoreAddToCache);
            angular.setValue(item, relation.alias, resolved);
          } else {
            if (relation.alias.indexOf('.') === -1 && relation.alias.indexOf('[') === -1) {
              delete item[relation.alias];
            } else {
              angular.setValue(item, relation.alias, void 0);
            }
          }
        } else if (this.MANY_TO_ONE.includes(relation.type) || this.WEAK_MANY_TO_ONE.includes(relation.type)) {
          aliasValue = [];
          ref1 = fieldValue || [];
          for (o = 0, len1 = ref1.length; o < len1; o++) {
            id = ref1[o];
            resolved = MODEL_CACHE.getItem(relation.model, id);
            if (resolved) {
              resolved = this.resolve(relation.model, resolved, ignoreAddToCache);
              aliasValue.push(resolved);
            }
          }
          angular.setValue(item, relation.alias, aliasValue);
        }
      }
      if (!ignoreAddToCache) {
        MODEL_CACHE.addItem(resourceName, item);
      }
      return item;
    };

    RelationsResolver.prototype.bulkResolve = function(bulkData) {
      var action, data, i, item, l, len, len1, o, priority, ref, ref1, ref2, relationsInfo, resourceName;
      ref = this.priorities;
      for (l = 0, len = ref.length; l < len; l++) {
        priority = ref[l];
        ref1 = this.relations;
        for (resourceName in ref1) {
          relationsInfo = ref1[resourceName];
          if ((relationsInfo.priority || 0) !== priority || !bulkData[resourceName]) {
            continue;
          }
          ref2 = bulkData[resourceName];
          for (action in ref2) {
            data = ref2[action];
            if (angular.isArray(data)) {
              for (i = o = 0, len1 = data.length; o < len1; i = ++o) {
                item = data[i];
                bulkData[resourceName][action][i] = this.resolve(resourceName, item, true);
              }
            } else if (angular.isObject(data)) {
              bulkData[resourceName][action] = this.resolve(resourceName, data, true);
            }
          }
        }
      }
      return bulkData;
    };

    return RelationsResolver;

  })();

  MODEL_NAMES = [];

  MODEL_CACHE = new Cache();

  RESOURCE_RELATIONS = new RelationsResolver();

  RESOURCE_PIPE_TRANSFORM = null;

  angular.module('lbServices').config(["$httpProvider", "$injector", function($httpProvider, $injector) {
    var CmsModelsInterceptor, LoopBackResourceProvider, baseApiUrl, getResourceName, resourceCache;
    LoopBackResourceProvider = $injector.get('CustomResourceProvider');
    baseApiUrl = LoopBackResourceProvider.getUrlBase();
    resourceCache = {};
    getResourceName = function($injector, name) {
      if (name === 'Helpers' || resourceCache[name] === false) {
        return false;
      }
      if (resourceCache[name]) {
        return resourceCache[name];
      }
      if ($injector.has(name)) {
        resourceCache[name] = name;
      } else if ($injector.has(name.substr(0, name.length - 1))) {
        resourceCache[name] = name.substr(0, name.length - 1);
      } else {
        resourceCache[name] = false;
      }
      return resourceCache[name];
    };
    CmsModelsInterceptor = function($injector) {
      var pipes;
      pipes = null;
      return {
        response: function(response) {
          var action, actionsData, data, i, item, j, k, l, len, len1, len2, o, p, processObj, queryParams, ref, ref1, ref2, resourceName;
          if (['DELETE', 'HEAD', 'CONNECT', 'OPTIONS'].includes(response.config.method) || response.config.headers.Accept === 'text/html' || response.config.url.indexOf('uib/') === 0 || (!RESOURCE_PIPE_TRANSFORM && !RESOURCE_RELATIONS.hasRelations())) {
            return response;
          }
          response.config.params = response.config.params || {};
          queryParams = {
            ignoreAddToCache: [true, 'true'].includes(response.config.params.$ignoreAddToCache),
            ignoreResourceRelations: [true, 'true'].includes(response.config.params.$ignoreResourceRelations)
          };
          if (queryParams.ignoreAddToCache && queryParams.ignoreResourceRelations) {
            return response;
          }
          if (!pipes && angular.isFunction(RESOURCE_PIPE_TRANSFORM)) {
            pipes = RESOURCE_PIPE_TRANSFORM(MODEL_CACHE);
          }
          pipes = pipes || {};
          resourceName = response.config.url.replace(baseApiUrl + "/", "").split('/')[0];
          if (resourceName === 'Helpers' && response.config.url.indexOf(baseApiUrl + "/Helpers/bulk") === 0) {
            if (!queryParams.ignoreAddToCache) {
              ref = response.data.data;
              for (k in ref) {
                actionsData = ref[k];
                for (action in actionsData) {
                  data = actionsData[action];
                  if (angular.isArray(data)) {
                    for (l = 0, len = data.length; l < len; l++) {
                      item = data[l];
                      if (angular.isObject(item)) {
                        MODEL_CACHE.addItem(k, item);
                      }
                    }
                  } else if (angular.isObject(data)) {
                    MODEL_CACHE.addItem(k, data);
                  }
                }
              }
            }
            if (!queryParams.ignoreResourceRelations) {
              response.data.data = RESOURCE_RELATIONS.bulkResolve(response.data.data);
            }
            ref1 = response.data.data;
            for (k in ref1) {
              actionsData = ref1[k];
              if (pipes[k]) {
                for (action in actionsData) {
                  data = actionsData[action];
                  if (angular.isArray(data)) {
                    for (j = o = 0, len1 = data.length; o < len1; j = ++o) {
                      item = data[j];
                      if (angular.isObject(item)) {
                        response.data.data[k][action][j] = pipes[k](item);
                      }
                    }
                  } else if (angular.isObject(data)) {
                    response.data.data[k][action] = pipes[k](data);
                  }
                }
              }
            }
            return response;
          }
          resourceName = getResourceName($injector, resourceName);
          if (!resourceName || (!pipes[resourceName] && !RESOURCE_RELATIONS.hasRelations(resourceName) && queryParams.ignoreAddToCache)) {
            return response;
          }
          processObj = function(obj) {
            if (!queryParams.ignoreResourceRelations) {
              obj = RESOURCE_RELATIONS.resolve(resourceName, obj, true);
            }
            obj = (pipes[resourceName] || angular.identity)(obj);
            if (!queryParams.ignoreAddToCache) {
              MODEL_CACHE.addItem(resourceName, obj);
            }
            return obj;
          };
          if (!MODEL_CACHE.hasResource(resourceName)) {
            MODEL_CACHE.initResource(resourceName);
          }
          if (angular.isArray(response.data)) {
            ref2 = response.data;
            for (i = p = 0, len2 = ref2.length; p < len2; i = ++p) {
              item = ref2[i];
              response.data[i] = processObj(item);
            }
          } else if (angular.isObject(response.data)) {
            response.data = processObj(response.data);
          }
          return response;
        }
      };
    };
    $httpProvider.interceptors.push(['$injector', CmsModelsInterceptor]);
  }]);

  angular.module('cms.models').provider('$models', function() {
    var CmsModelClass, iQ, initModelsService, l, len, models, modelsServiceWrapper, ref, userDefaults;
    this.defaultModels = {};
    userDefaults = {};
    models = null;
    ref = angular.module('lbServices')._invokeQueue;
    for (l = 0, len = ref.length; l < len; l++) {
      iQ = ref[l];
      if (iQ[1] !== 'factory') {
        continue;
      }
      if (iQ[2][0].indexOf('Cms') !== 0) {
        continue;
      }
      MODEL_NAMES.push(iQ[2][0]);
      MODEL_CACHE.initResource(iQ[2][0]);
    }
    this.setResourcePipeTransform = function(fn) {
      if (!angular.isFunction(fn)) {
        return;
      }
      RESOURCE_PIPE_TRANSFORM = fn;
    };
    this.setResourceRelationsMapping = function(mapping) {
      if (!angular.isObject(mapping)) {
        return;
      }
      RESOURCE_RELATIONS.setRelations(mapping);
      return RESOURCE_RELATIONS.relations;
    };
    this.setDefaults = function(modelName, config) {
      userDefaults[modelName] = config || {};
    };
    CmsModelClass = (function(superClass) {
      extend(CmsModelClass, superClass);

      function CmsModelClass(modelName, resource, $injector, defaults) {
        CmsModelClass.__super__.constructor.call(this, resource, $injector, defaults);
        this.modelName = modelName;
        return;
      }

      CmsModelClass.prototype.resolveRelations = function(data) {
        var i, item, len1, o;
        if (!RESOURCE_RELATIONS.hasRelations(this.modelName)) {
          return item;
        }
        if (angular.isArray(data)) {
          for (i = o = 0, len1 = data.length; o < len1; i = ++o) {
            item = data[i];
            data[i] = RESOURCE_RELATIONS.resolve(this.modelName, item);
          }
          return data;
        }
        return RESOURCE_RELATIONS.resolve(this.modelName, data);
      };

      return CmsModelClass;

    })(window.BaseModelClass);
    initModelsService = (function(_this) {
      return function($q, $injector) {
        var len1, methods, name, o;
        models = {};
        methods = ['registerModel', 'registerModels', 'getDefaults', 'getModels', 'getAllModelNames', 'reset'];
        models.registerModel = function(modelName, defaults) {
          var e, resource;
          try {
            resource = $injector.get(modelName);
          } catch (error) {
            e = error;
            return console.error("Can't get " + modelName + " with $injector. Is it exist?");
          }
          if (!typeof defaults === 'object') {
            defaults = userDefaults[modelName] || {};
          }
          return models[modelName] = new CmsModelClass(modelName, resource, $injector, defaults);
        };
        models.registerModels = function(modelList) {
          var d, m;
          for (m in modelList) {
            d = modelList[m];
            models.registerModel(m, d);
          }
        };
        models.getDefaults = function() {
          var defaults, name, obj;
          defaults = {};
          for (name in models) {
            obj = models[name];
            if (obj && (indexOf.call(methods, name) < 0)) {
              defaults[name] = obj.defaults;
            }
          }
          return defaults;
        };
        models.getModels = function() {
          var _models, name, obj;
          _models = [];
          for (name in models) {
            obj = models[name];
            if (indexOf.call(methods, name) < 0) {
              _models[name] = obj.defaults;
            }
          }
          return _models;
        };
        models.reset = function() {
          var len1, name, o, prop;
          for (o = 0, len1 = MODEL_NAMES.length; o < len1; o++) {
            name = MODEL_NAMES[o];
            if (models[name] && name !== 'CmsSettings') {
              for (prop in models[name]) {
                if (!(!angular.isFunction(models[name][prop]))) {
                  continue;
                }
                if (prop === 'modelName' || prop === 'defaults') {
                  continue;
                }
                delete models[name][prop];
              }
            }
          }
        };
        models.getAllModelNames = function() {
          return angular.copy(MODEL_NAMES);
        };
        models.registerModels(_this.defaultModels);
        for (o = 0, len1 = MODEL_NAMES.length; o < len1; o++) {
          name = MODEL_NAMES[o];
          if (_this.defaultModels && !_this.defaultModels[name]) {
            models.registerModel(name);
          }
        }
        return models;
      };
    })(this);
    modelsServiceWrapper = function($q, $injector) {
      if (!models) {
        initModelsService($q, $injector);
      }
      return models;
    };
    this.$get = ['$q', '$injector', modelsServiceWrapper];
    return this;
  });

}).call(this);
;
(function() {
  angular.module('cms.models').run(["$injector", "$timeout", "$rootScope", "$bulk", "$models", function($injector, $timeout, $rootScope, $bulk, $models) {
    var $cms, $langPicker, $state, $tracking, cache, resolveStateDefValue;
    $state = null;
    $cms = null;
    $langPicker = null;
    cache = {};
    if ($injector.has('$tracking')) {
      $tracking = $injector.get('$tracking');
    }
    if ($injector.has('$langPicker')) {
      $langPicker = $injector.get('$langPicker');
    }
    resolveStateDefValue = function(definition, stateParams) {
      var parent, path, resolveValue, value;
      value = '';
      resolveValue = function(t) {
        var k, v;
        if (angular.isFunction(t)) {
          return t(stateParams, $models);
        }
        if (angular.isString(t)) {
          for (k in stateParams) {
            v = stateParams[k];
            t = t.replace("{{" + k + "}}", v);
          }
          return t;
        }
        if (typeof t === 'object') {
          return t;
        }
        return '';
      };
      if (angular.isArray(definition)) {
        if (definition.length >= 1) {
          path = resolveValue(definition[0]);
          parent = path.split('.')[0];
          if (!cache[parent] && $injector.has(parent)) {
            cache[parent] = $injector.get(parent);
          }
          value = angular.getValue(cache, path);
          if (!angular.isString(value) && typeof value !== 'object') {
            if (definition.length === 3) {
              value = resolveStateDefValue([definition[1], definition[2]], stateParams);
            } else if (definition.length === 2) {
              value = resolveValue(definition[1]);
            } else {
              value = resolveValue(definition[0]);
            }
          }
        } else {
          value = '';
        }
      } else {
        value = resolveValue(definition);
      }
      return value;
    };
    $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams, options) {
      var resolveSeoTags;
      resolveSeoTags = function() {
        var breadcrumb, breadcrumbs, i, len, prop, ref, state;
        breadcrumbs = [];
        $state = $state || $injector.get('$state');
        $cms = $cms || $injector.get('$cms');
        state = $state.$current;
        while (state) {
          breadcrumb = {};
          ref = ['title', 'description', 'pageTitle'];
          for (i = 0, len = ref.length; i < len; i++) {
            prop = ref[i];
            if (!state.self['$' + prop]) {
              continue;
            }
            breadcrumb[prop] = resolveStateDefValue(state.self['$' + prop], state.locals.globals.$stateParams);
            if (!breadcrumb[prop]) {
              delete breadcrumb[prop];
            }
          }
          if (Object.keys(breadcrumb).length) {
            breadcrumb.stateName = state.self.name;
            breadcrumb.stateParams = state.locals.globals.$stateParams;
            breadcrumbs.unshift(breadcrumb);
          }
          if (state.self.$seoObject) {
            console.error("$bulk: do not use $seoObject in route definition!", state.self);
          }
          state = state.parent;
        }
        $cms.seoService.setBreadcrumbs(breadcrumbs);
        $cms.seoService.resolveAll();
        if ($tracking) {
          $timeout(function() {
            return $tracking.sendPageview();
          });
        }
      };
      $bulk.onReady(resolveSeoTags, resolveSeoTags);
    });

    /*
        также при смене языка запускаем пересчет всех значений для seo
     */
    if ($langPicker) {
      $rootScope.$langPicker = $langPicker;
      return $rootScope.$watch('$langPicker.currentLang', function(currentLang, oldValue) {
        if (currentLang !== oldValue) {
          $cms = $cms || $injector.get('$cms');
          $cms.seoService.resolveAll();
        }
      });
    }
  }]);

}).call(this);
