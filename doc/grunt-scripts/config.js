var CMS_BASE_URL = '';
var CMS_DEFAULT_LANG = 'en';
var CMS_ALLOWED_LANGS = ['en'];
var CMS_DB_BUCKET = 'demo';
var CMS_STORAGE = {
    type: 'local'
};


/**
*   @ngdoc overview
*   @name LESS
*   @description
*       Классы css и хаки, которые идут в поставке с цмс-ными библиотеками. Пакет CmsLibs.
*       <div></div>
*       <b>Дополнительно подключать css к проекту не нужно. Этот мини стилевик находится внутри js файла от пакета CmsLibs</b>
*       <div></div>
*       Подробнее о less: <a href="http://lesscss.org/" target='blank'>http://lesscss.org/</a>
*   <br/>
*   <br/>
 */

/**
*   @ngdoc service
*   @name LESS.Hacks
*   @description хаки используемые библиотекой.
*   <div></div>
*   <hr/>
*   <b style="font-size:1.3em;" class="text-danger">Хаки для firefox</b>
*   <div></div>
*   убогий фф рендерит select.form-control с серым квадратом и треугольником справа, вместо просто треугольника.
*   К сожалению, цвет треугольника с этим хаком в фф будет всегда rgb(51,51,51).
*   <div></div>
*   см: https://jsfiddle.net/77ht2pt7/
*   <div></div>
*   Пример как выглядят selet-ы в firefox-е с хаком и без:
*   <div></div>
*   <img src="./firefox-hacks.jpg" />
*
*
*   <hr/>
*   <b style="font-size:1.3em;" class="text-danger">Хаки для библиотеки ui.bootstrap для ангулара</b>
*   <div></div>
*   для datepicker-a который полагается на стандартные иконки от бутстрапа,
*   заменяем шрифт на FontAwesome. Т.е. стили классов иконок .glyphicon-chevron-right, .glyphicon-menu-right, .glyphicon-chevron-left, .glyphicon-menu-left,
*   .glyphicon-menu-up заменяются на похожие глифы от FontAwesome
 */
