(function() {
  angular.module('AdminApp', ['gettext', 'ui.bootstrap', 'ui.gettext.langPicker', 'ui.router', 'ngAnimate', 'lbServices', 'ngResource', 'angular-loading-bar', 'duScroll', 'ui.cms', 'ui.cms.editable', 'cms.models', 'ui.landingWidgets', 'ui-notification', 'pageslide-directive']);

}).call(this);
;
(function() {
  var getCardGenerator;

  getCardGenerator = function($injector) {
    var $constants, getCssClass, getGlyph, getName;
    $constants = $injector.get('$constants');
    getCssClass = function(modelName) {
      if (modelName.indexOf('Keyword') > -1 || modelName.indexOf('Category') > -1) {
        return 'success';
      }
      if (modelName.indexOf('Article') > -1 || modelName.indexOf('Page') > -1) {
        return 'primary';
      }
      if (modelName.indexOf('User') > -1) {
        return 'danger';
      }
      if (modelName.indexOf('Product') > -1 || modelName.indexOf('Album') > -1 || modelName.indexOf('News') > -1) {
        return 'info';
      }
      if (modelName.indexOf('Order') > -1) {
        return 'default';
      }
      return 'default';
    };
    getName = function(modelName) {
      if (modelName.indexOf('Album') > -1) {
        return 'Gallery';
      }
      return modelName.replace('Cms', '').replace('Simple', '');
    };
    getGlyph = function(modelName) {
      if (modelName.indexOf('News') > -1) {
        return 'fas fa-newspaper';
      }
      if (modelName.indexOf('Category') > -1) {
        return 'fas fa-list-ul';
      }
      if (modelName.indexOf('Keyword') > -1) {
        return 'fas fa-tags';
      }
      if (modelName.indexOf('Page') > -1) {
        return 'fas fa-file-text';
      }
      if (modelName.indexOf('Article') > -1) {
        return 'fas fa-font';
      }
      if (modelName.indexOf('User') > -1) {
        return 'fas fa-user';
      }
      if (modelName.indexOf('SimpleProduct') > -1) {
        return 'fas fa-shopping-cart';
      }
      if (modelName.indexOf('PictureAlbum') > -1) {
        return 'fas fa-images';
      }
      if (modelName.indexOf('Order') > -1) {
        return 'fas fa-cart-arrow-down';
      }
      return 'fas fa-asterisk';
    };
    return function(card) {
      var r;
      if (angular.isString(card)) {
        card = {
          modelName: card,
          cssClass: getCssClass(card),
          iconClass: getGlyph(card),
          name: getName(card),
          addHref: "app.modelEditors." + card + ".add",
          listHref: "app.modelEditors." + card + ".list"
        };
      }
      r = $constants.resolve("$REPRESENTATION." + card.modelName) || {};
      if (!card.uicIfOnCurrentUserPermission) {
        card.uicIfOnCurrentUserPermission || (card.uicIfOnCurrentUserPermission = (r.app || 'main_app') + ".view_" + card.modelName);
      }
      if (!card.cssClass) {
        card.cssClass = r.cssClass || getCssClass(card.modelName);
      }
      if (!card.iconClass) {
        card.iconClass = r.iconClass || getGlyph(card.modelName);
      }
      if (card.iconClass && card.iconClass.startsWith('fa-')) {
        card.iconClass += ' fas';
      }
      if (!card.name) {
        card.name = r.name || getName(card.modelName);
      }
      if (angular.isUndefined(card.addHref)) {
        card.addHref = "app.modelEditors." + card.modelName + ".add";
        card.addHrefCurrentUserPermission || (card.addHrefCurrentUserPermission = (r.app || 'main_app') + ".add_" + card.modelName);
      }
      if (angular.isUndefined(card.detailsHref)) {
        card.listHref = "app.modelEditors." + card.modelName + ".list";
        card.listHrefCurrentUserPermission || (card.listHrefCurrentUserPermission = (r.app || 'main_app') + ".view_" + card.modelName + " || " + (r.app || 'main_app') + ".change_" + card.modelName);
      }
      return card;
    };
  };


  /**
  *   @ngdoc object
  *   @name AdminApp.AdminAppConfigProvider
  *   @header AdminApp.AdminAppConfigProvider
  *   @description провайдер для настройки приложения admin-app (админка цмс-ки)
   */

  angular.module('AdminApp').provider('AdminAppConfig', function() {
    var AdminAppConfig, DEFAULT_MODEL_NAMES_FOR_DASHBOARD, allowedLanguages, copyrightMetadataEditorForModels, copyrightMetadataFields, dashboardCards, glossaryCards, loadTemplates, loadTemplatesWithDestinations, loadedSettingsTabs, loadedSidebarTemplates, loginPageTitle, navbarConfig, settingsTabs, settingsTabsConfig, showResetPasswordOnLoginPage, sidebarTemplates, sidebarTitle, visibleModels, visiblePages;
    allowedLanguages = {
      ru: 'Русский',
      en: 'English'
    };
    dashboardCards = [];
    visibleModels = ['CmsArticle', 'CmsKeyword', 'CmsPictureAlbum', 'CmsSiteReview', 'CmsUser', 'CmsMailNotification', 'CmsSmsNotification', 'CmsPdfData', 'CmsTranslations', 'CmsCategoryForArticle', 'CmsCategoryForPictureAlbum', 'CmsGdprRequest', 'GroupPermissions'];
    copyrightMetadataEditorForModels = [];
    copyrightMetadataFields = ['creator', 'creatorUrl'];
    visiblePages = ['siteMenu', 'mainPage'];
    settingsTabsConfig = {
      emailNotifyForUsers: false,
      smsNotifyForUsers: false,
      currency: false,
      redirections: true,
      dumpAndRestore: true,
      maps: true
    };
    settingsTabs = [];
    sidebarTemplates = [];
    glossaryCards = [];
    loginPageTitle = 'CMS Admin';
    showResetPasswordOnLoginPage = false;
    sidebarTitle = 'Menu';
    navbarConfig = {
      right: [
        {
          type: 'directive',
          data: 'a-prev-object-for-navbar'
        }, {
          type: 'directive',
          data: 'a-next-object-for-navbar'
        }, {
          type: 'ui-sref',
          data: 'app.settings',
          attrs: {
            'uic-if-on-current-user-permission': 'isAdmin()'
          },
          title: {
            en: 'Settings',
            ru: 'Настройки'
          },
          html: {
            pre: '<span class="fas fa-cog"></span>'
          }
        }, {
          type: 'href',
          data: "/",
          title: {
            en: 'Site',
            ru: 'Сайт'
          },
          attrs: {
            target: '_blank'
          },
          html: {
            pre: '<span class="fas fa-external-link-alt"></span>'
          }
        }, {
          type: 'directive',
          data: 'ui-lang-picker-for-navbar'
        }, {
          type: 'directive',
          data: 'uice-logout-for-navbar'
        }
      ]
    };

    /**
    * @ngdoc method
    * @name AdminApp.AdminAppConfigProvider#setLoginPageTitle
    * @methodOf AdminApp.AdminAppConfigProvider
    * @param {string|L10nString=} title заголовок для панельки с формой логина и название логин-страницы <i>(default: 'CMS Admin')</i>
    * @returns {self} ссылка на самого себя
    * @description Переопределяет заголовок для панельки с формой логина и название логин-страницы
    * @example
    *   <pre>
    *       angular.module('OverrideMyApp')
    *       .config (AdminAppConfigProvider)->
    *            AdminAppConfigProvider
    *                .setLoginPageTitle("Hello world!")
    *                .setLoginPageTitle("Hello world #2!")
    *            # в данном случае заголовком станет именно последняя строка
    *   </pre>
     */
    this.setLoginPageTitle = function(title) {
      loginPageTitle = title || loginPageTitle;
      return this;
    };
    this.setShowResetPasswordOnLoginPage = function(value) {
      showResetPasswordOnLoginPage = !!value;
      return this;
    };

    /**
    * @ngdoc method
    * @name AdminApp.AdminAppConfigProvider#setSidebarTitle
    * @methodOf AdminApp.AdminAppConfigProvider
    * @param {string|L10nString=} title заголовок сайдбара администратора <i>(default: 'Menu')</i>
    * @returns {self} ссылка на самого себя
    * @description Переопределяет заголовок сайдбара администратора
    * @example
    *   <pre>
    *       angular.module('OverrideMyApp')
    *       .config (AdminAppConfigProvider)->
    *            AdminAppConfigProvider
    *                .setSidebarTitle("Some site")
    *   </pre>
     */
    this.setSidebarTitle = function(title) {
      sidebarTitle = title || sidebarTitle;
      return this;
    };

    /**
    * @ngdoc method
    * @name AdminApp.AdminAppConfigProvider#setAllowedLanguages
    * @methodOf AdminApp.AdminAppConfigProvider
    * @param {object=} langs разрешенные языки, на которых может отображаться админка (ключи - iso-код языка, значения - текст, который будет отображаться в дропдауне переключалки) <i>(default: {ru: 'Русский', en: 'English'})</i>
    * @returns {self} ссылка на самого себя
    * @description Переопределяет разрешенные языки, на которых может отображаться админка
    * @example
    *   <pre>
    *       angular.module('OverrideMyApp')
    *       .config (AdminAppConfigProvider)->
    *            AdminAppConfigProvider
    *                .setLoginPageTitle({en: "English"})
    *   </pre>
     */
    this.setAllowedLanguages = function(langs) {
      allowedLanguages = langs || allowedLanguages;
      return this;
    };

    /**
    * @ngdoc method
    * @name AdminApp.AdminAppConfigProvider#setVisibleModels
    * @methodOf AdminApp.AdminAppConfigProvider
    * @param {Array<string>=} models название моделей, которые можно отображать в админке <i>(default: ['CmsArticle', 'CmsKeyword', 'CmsPictureAlbum', 'CmsSiteReview', 'CmsUser', 'CmsMailNotification', 'CmsSmsNotification', 'CmsPdfData', 'CmsTranslations', 'CmsCategoryForArticle', 'CmsCategoryForPictureAlbum', 'CmsGdprRequest'])</i>
    * @returns {self} ссылка на самого себя
    * @description Переопределяет название моделей, которые можно отображать в админке
    * @example
    *   <pre>
    *       angular.module('OverrideMyApp')
    *       .config (AdminAppConfigProvider)->
    *            AdminAppConfigProvider
    *                .setVisibleModels(['CmsArticle', 'CmsMyModel'])
    *   </pre>
     */
    this.setVisibleModels = function(models) {
      visibleModels = models || visibleModels;
      return this;
    };

    /**
    * @ngdoc method
    * @name AdminApp.AdminAppConfigProvider#setCopyrightMetadataEditorForModels
    * @methodOf AdminApp.AdminAppConfigProvider
    * @param {Array<string>=} models название моделей, для которых при загрузке картинок можно будет указывать copyright <i>(default: [])</i>
    * @returns {self} ссылка на самого себя
    * @description Переопределяет название моделей, для которых при загрузке картинок можно будет указывать copyright
    * @example
    *   <pre>
    *       angular.module('OverrideMyApp')
    *       .config (AdminAppConfigProvider)->
    *            AdminAppConfigProvider
    *                .setCopyrightMetadataEditorForModels(['CmsArticle', 'CmsMyModel'])
    *   </pre>
     */
    this.setCopyrightMetadataEditorForModels = function(models) {
      copyrightMetadataEditorForModels = models || [];
      return this;
    };

    /**
    * @ngdoc method
    * @name AdminApp.AdminAppConfigProvider#setCopyrightMetadataFields
    * @methodOf AdminApp.AdminAppConfigProvider
    * @param {Array<string>=} fields название полей внутри L10nFile, для которых при загрузке картинок можно будет указывать copyright <i>(default: ['creator', 'creatorUrl'])</i>
    * @returns {self} ссылка на самого себя
    * @description Переопределяет название полей внутри L10nFile, для которых при загрузке картинок можно будет указывать copyright
    * @example
    *   <pre>
    *       angular.module('OverrideMyApp')
    *       .config (AdminAppConfigProvider)->
    *            AdminAppConfigProvider
    *                .setCopyrightMetadataFields(['creator', 'creatorUrl', 'creditLine', 'copyrightNotice'])
    *   </pre>
     */
    this.setCopyrightMetadataFields = function(fields) {
      copyrightMetadataFields = fields || [];
      return this;
    };

    /**
    * @ngdoc method
    * @name AdminApp.AdminAppConfigProvider#setVisiblePages
    * @methodOf AdminApp.AdminAppConfigProvider
    * @param {Array<string>=} pages список дополнительных страниц, встроенных в админку, которые можно отображать <i>(default: ['siteMenu', 'mainPage'])</i>
    * @returns {self} ссылка на самого себя
    * @description Переопределяет список дополнительных страниц, встроенных в админку, которые можно отображать
    * @example
    *   <pre>
    *       angular.module('OverrideMyApp')
    *       .config (AdminAppConfigProvider)->
    *            AdminAppConfigProvider
    *                .setVisiblePages(['siteMenu'])
    *   </pre>
     */
    this.setVisiblePages = function(pages) {
      visiblePages = pages || visiblePages;
      return this;
    };

    /**
    * @ngdoc method
    * @name AdminApp.AdminAppConfigProvider#setNavbarConfig
    * @methodOf AdminApp.AdminAppConfigProvider
    * @param {object=} conf конфиг навбара <i>(default: см исходный код)</i>
    * @returns {self} ссылка на самого себя
    * @description Переопределяет конфиг навбара
    * @example
    *   <pre>
    *       angular.module('OverrideMyApp')
    *       .config (AdminAppConfigProvider)->
    *            AdminAppConfigProvider
    *                .setVisiblePages({right: [{type: 'directive', data: 'test-directive'}]})
    *   </pre>
     */
    this.setNavbarConfig = function(conf) {
      navbarConfig = conf || navbarConfig;
      return this;
    };

    /**
    * @ngdoc method
    * @name AdminApp.AdminAppConfigProvider#setSettingsTabsConfig
    * @methodOf AdminApp.AdminAppConfigProvider
    * @param {object=} conf конфиг вкладки
    * @returns {self} ссылка на самого себя
    * @description Переопределяет конфиг базовых вкладок страницы настройки CMS (страницу может просматривать ТОЛЬКО тот, у кого есть разрешение site_settings.change_CmsSettings)
    * @example
    *   <pre>
    *       angular.module('OverrideMyApp')
    *       .config (AdminAppConfigProvider)->
    *            AdminAppConfigProvider
    *                .setSettingsTabsConfig({currency: true, redirections: true})
    *   </pre>
     */
    this.setSettingsTabsConfig = function(conf) {
      settingsTabsConfig = conf || settingsTabsConfig;
      return this;
    };

    /**
    * @ngdoc method
    * @name AdminApp.AdminAppConfigProvider#registerCustomSettingsTab
    * @methodOf AdminApp.AdminAppConfigProvider
    * @param {object=} conf конфиг вкладки. Обязательно нужно указывать ИЛИ template ИЛИ templateUrl
    *     - `heading` – <i>(required)</i> <span class='label type-hint type-hint-string' style='display: inline-block;'>string</span> – название вкладки (можно использовать html-верстку)
    *     - `template` – <i>(optional)</i> <span class='label type-hint type-hint-string' style='display: inline-block;'>string</span> – контент вкладки (html-шаблон)
    *     - `templateUrl` – <i>(optional)</i> <span class='label type-hint type-hint-string' style='display: inline-block;'>string</span> – url шаблона с версткой контента вкладки
    * @returns {self} ссылка на самого себя
    * @description Добавляет новую вкладку на страницу 'app.settings' (эту страницу может просматривать ТОЛЬКО тот, у кого есть разрешение site_settings.change_CmsSettings)
    * @example
    *   <pre>
    *       angular.module('OverrideMyApp')
    *       .config (AdminAppConfigProvider)->
    *            AdminAppConfigProvider
    *                .registerCustomSettingsTab({heading: "Hello!", template: "<b>Tab content</b>"})
    *   </pre>
     */
    this.registerCustomSettingsTab = function(obj) {
      var i, k, len, s;
      if (!obj) {
        return this;
      }
      if (obj.name) {
        for (i = k = 0, len = settingsTabs.length; k < len; i = ++k) {
          s = settingsTabs[i];
          if (!(obj.name === s.name)) {
            continue;
          }
          settingsTabs[i] = obj;
          return;
        }
      }
      settingsTabs.push(obj);
      return this;
    };

    /**
    * @ngdoc method
    * @name AdminApp.AdminAppConfigProvider#registerCustomSidebarTemplate
    * @methodOf AdminApp.AdminAppConfigProvider
    * @param {object=} conf конфиг шаблона. Обязательно нужно указывать ИЛИ template ИЛИ templateUrl
    *     - `destination` – <i>(optional)</i> <span class='label type-hint type-hint-string' style='display: inline-block;'>string</span> – где именно располагать верстку (top, middle, bottom) <i>(default: 'middle')</i>
    *     - `template` – <i>(optional)</i> <span class='label type-hint type-hint-string' style='display: inline-block;'>string</span> – html-шаблон
    *     - `templateUrl` – <i>(optional)</i> <span class='label type-hint type-hint-string' style='display: inline-block;'>string</span> – url шаблона с версткой
    * @returns {self} ссылка на самого себя
    * @description Добавляет новый кусок верстки внутри директивы с сайдбаром a-sidebar-admin
    * @example
    *   <pre>
    *       angular.module('OverrideMyApp')
    *       .config (AdminAppConfigProvider)->
    *            AdminAppConfigProvider
    *                .registerCustomSidebarTemplate({destination: "top", template: "<b>Hello sidebar!</b>"})
    *   </pre>
     */
    this.registerCustomSidebarTemplate = function(obj) {
      var i, k, len, s;
      if (!obj) {
        return this;
      }
      if (obj.destination) {
        for (i = k = 0, len = sidebarTemplates.length; k < len; i = ++k) {
          s = sidebarTemplates[i];
          if (!(obj.destination === s.destination)) {
            continue;
          }
          sidebarTemplates[i] = obj;
          return;
        }
      }
      sidebarTemplates.push(obj);
      return this;
    };

    /**
    * @ngdoc method
    * @name AdminApp.AdminAppConfigProvider#registerDashboardCard
    * @methodOf AdminApp.AdminAppConfigProvider
    * @param {object} conf конфиг карточки
    *     - `modelName` – <i>(required)</i> <span class='label type-hint type-hint-string' style='display: inline-block;'>string</span> – название REST-модели. Например: 'CmsArticle'
    *     - `name` – <i>(optional)</i> <span class='label type-hint type-hint-string' style='display: inline-block;'>string</span> – название. Если не указано, то возьмется из $REPRESENTATION[conf.modelName].name или будет сгенерированно из conf.modelName значения
    *     - `cssClass` – <i>(optional)</i> <span class='label type-hint type-hint-string' style='display: inline-block;'>string</span> – css-класс для верстки. Например: 'primary', 'danger'.
    *     - `iconClass` – <i>(optional)</i> <span class='label type-hint type-hint-string' style='display: inline-block;'>string</span> – иконка font-awesome. Наример: 'fab fa-github'
    *     - `addHref` – <i>(optional)</i> <span class='label type-hint type-hint-string' style='display: inline-block;'>string</span> – полный url к странице, на которой будет происходить редактирование объекта или название из $state. По умолчанию: "app.modelEditors.#{conf.modelName}.add"
    *     - `listHref` – <i>(optional)</i> <span class='label type-hint type-hint-string' style='display: inline-block;'>string</span> – полный url к странице, на которой можно увидеть список всех объектов или название из $state. По умолчанию: "app.modelEditors.#{conf.modelName}.list"
    * @returns {self} ссылка на самого себя
    * @description Регистрирует новую карточку в dashboard для a-dashboard-admin директивы
    * @example
    *   <pre>
    *       angular.module('OverrideMyApp')
    *       .config (AdminAppConfigProvider)->
    *            AdminAppConfigProvider
    *                .registerCustomSidebarTemplate({modelName: 'CmsMyModel', iconClass: 'fab fa-youtube'})
    *   </pre>
     */
    this.registerDashboardCard = function(obj) {
      if (!obj.modelName) {
        throw "AdminAppConfigProvider.registerDashboardCard: modelName is required param!";
      }
      dashboardCards.push(obj);
      return this;
    };

    /**
    * @ngdoc method
    * @name AdminApp.AdminAppConfigProvider#registerGlossaryCard
    * @methodOf AdminApp.AdminAppConfigProvider
    * @param {object|string} conf конфиг карточки (или объект или просто название REST-модели)
    *     - `modelName` – <i>(required)</i> <span class='label type-hint type-hint-string' style='display: inline-block;'>string</span> – название REST-модели. Например: 'CmsArticle'
    *     - `name` – <i>(optional)</i> <span class='label type-hint type-hint-string' style='display: inline-block;'>string</span> – название. Если не указано, то возьмется из $REPRESENTATION[conf.modelName].name или будет сгенерированно из conf.modelName значения
    *     - `cssClass` – <i>(optional)</i> <span class='label type-hint type-hint-string' style='display: inline-block;'>string</span> – css-класс для верстки. Например: 'primary', 'danger'.
    *     - `iconClass` – <i>(optional)</i> <span class='label type-hint type-hint-string' style='display: inline-block;'>string</span> – иконка font-awesome. Наример: 'fab fa-github'
    *     - `addHref` – <i>(optional)</i> <span class='label type-hint type-hint-string' style='display: inline-block;'>string</span> – полный url к странице, на которой будет происходить редактирование объекта или название из $state. По умолчанию: "app.modelEditors.#{conf.modelName}.add"
    *     - `listHref` – <i>(optional)</i> <span class='label type-hint type-hint-string' style='display: inline-block;'>string</span> – полный url к странице, на которой можно увидеть список всех объектов или название из $state. По умолчанию: "app.modelEditors.#{conf.modelName}.list"
    * @returns {self} ссылка на самого себя
    * @description Регистрирует новую карточку в справочнике. Внимание: справочник могут просматривать только те пользователи, у которых есть разрешение 'site_settings.view_glossary'
    * @example
    *   <pre>
    *       angular.module('OverrideMyApp')
    *       .config (AdminAppConfigProvider)->
    *            AdminAppConfigProvider
    *                .registerGlossaryCard({modelName: 'CmsMyModel', iconClass: 'fab fa-youtube'})
    *   </pre>
     */
    this.registerGlossaryCard = function(obj) {
      if (angular.isString(obj)) {
        obj = {
          modelName: obj,
          listHref: "app.modelEditors." + obj + ".list",
          addHref: "app.modelEditors." + obj + ".add"
        };
      }
      if (!obj.modelName) {
        throw "AdminAppConfigProvider.registerGlossaryCard: modelName is required param!";
      }
      glossaryCards.push(obj);
      return this;
    };
    AdminAppConfig = null;
    loadedSidebarTemplates = null;
    loadedSettingsTabs = null;
    DEFAULT_MODEL_NAMES_FOR_DASHBOARD = ['CmsArticle', 'CmsPictureAlbum', 'CmsKeyword', 'CmsUser'];
    loadTemplates = function($sce, $templateRequest, templates) {
      var fn, i, j, k, len, result, template;
      result = [];
      fn = function(template, j) {
        var url;
        url = $sce.getTrustedResourceUrl(template.templateUrl);
        $templateRequest(url).then(function(data) {
          template.template = data;
          result[j] = template;
        });
      };
      for (i = k = 0, len = templates.length; k < len; i = ++k) {
        template = templates[i];
        if (template.template) {
          result.push(template);
          continue;
        }
        if (!template.templateUrl) {
          continue;
        }
        j = result.length;
        result.push(null);
        fn(template, j);
      }
      return result;
    };
    loadTemplatesWithDestinations = function($sce, $templateRequest, templates) {
      var fn, i, j, k, len, result, template;
      result = {
        top: [],
        middle: [],
        bottom: []
      };
      fn = function(template, j) {
        var url;
        url = $sce.getTrustedResourceUrl(template.templateUrl);
        $templateRequest(url).then(function(data) {
          template.template = data;
          result[template.destination][j] = template;
        });
      };
      for (i = k = 0, len = templates.length; k < len; i = ++k) {
        template = templates[i];
        if (!result[template.destination]) {
          template.destination = 'middle';
        }
        if (template.template) {
          result[template.destination].push(template);
          continue;
        }
        if (!template.templateUrl) {
          continue;
        }
        j = result[template.destination].length;
        result[template.destination].push(null);
        fn(template, j);
      }
      return result;
    };
    this.$get = [
      '$sce', '$templateRequest', '$injector', function($sce, $templateRequest, $injector) {
        var card, cardGenerator, cm, k, l, len, len1, len2, len3, len4, len5, m, n, name, o, p;
        if (!AdminAppConfig) {
          cardGenerator = getCardGenerator($injector);
          AdminAppConfig = {
            allowedLanguages: allowedLanguages,
            dashboardCards: [],
            glossaryCards: [],
            navbarConfig: navbarConfig,
            loginPageTitle: loginPageTitle,
            showResetPasswordOnLoginPage: showResetPasswordOnLoginPage,
            sidebarTitle: sidebarTitle,
            visibleModels: {},
            visiblePages: {},
            copyrightMetadataEditorForModels: {},
            copyrightMetadataFields: copyrightMetadataFields,
            sidebarTemplates: {
              templates: loadTemplatesWithDestinations($sce, $templateRequest, sidebarTemplates)
            },
            settingsTabs: {
              config: settingsTabsConfig,
              customTabs: loadTemplates($sce, $templateRequest, settingsTabs)
            }
          };
          for (k = 0, len = visibleModels.length; k < len; k++) {
            name = visibleModels[k];
            if ($injector.has(name)) {
              AdminAppConfig.visibleModels[name] = true;
            }
          }
          for (l = 0, len1 = dashboardCards.length; l < len1; l++) {
            card = dashboardCards[l];
            if ($injector.has(card.modelName)) {
              AdminAppConfig.dashboardCards.push(cardGenerator(card));
            }
          }
          for (m = 0, len2 = DEFAULT_MODEL_NAMES_FOR_DASHBOARD.length; m < len2; m++) {
            name = DEFAULT_MODEL_NAMES_FOR_DASHBOARD[m];
            if (AdminAppConfig.visibleModels[name]) {
              AdminAppConfig.dashboardCards.push(cardGenerator(name));
            }
          }
          for (n = 0, len3 = visiblePages.length; n < len3; n++) {
            name = visiblePages[n];
            AdminAppConfig.visiblePages[name] = true;
          }
          for (o = 0, len4 = glossaryCards.length; o < len4; o++) {
            card = glossaryCards[o];
            if (!($injector.has(card.modelName))) {
              continue;
            }
            card = cardGenerator(card, true);
            AdminAppConfig.glossaryCards.push(card);
          }
          for (p = 0, len5 = copyrightMetadataEditorForModels.length; p < len5; p++) {
            cm = copyrightMetadataEditorForModels[p];
            if (angular.isString(cm)) {
              AdminAppConfig.copyrightMetadataEditorForModels[cm] = '*';
            } else {
              AdminAppConfig.copyrightMetadataEditorForModels[cm.model] = cm.fields || [];
            }
          }
          if (!AdminAppConfig.visibleModels.CmsMailNotification) {
            AdminAppConfig.settingsTabs.config.emailNotify = false;
            AdminAppConfig.settingsTabs.config.emailNotifyForUsers = false;
          } else {
            AdminAppConfig.settingsTabs.config.emailNotify = true;
          }
          if (!AdminAppConfig.visibleModels.CmsSmsNotification) {
            AdminAppConfig.settingsTabs.config.smsNotify = false;
            AdminAppConfig.settingsTabs.config.smsNotifyForUsers = false;
          } else {
            AdminAppConfig.settingsTabs.config.smsNotify = true;
          }
          AdminAppConfig.settingsTabs.config.languages = CMS_ALLOWED_LANGS.length > 1;
        }
        return AdminAppConfig;
      }
    ];
    return this;
  }).run(["$injector", function($injector) {
    if ($injector.has('adminConf')) {
      console.error("DEPRECATED: replace adminConf service with AdminAppConfig.set* functions!");
    }
    if ($injector.has('adminSettingsTabs')) {
      console.error("DEPRECATED: replace adminSettingsTabs service with AdminAppConfigProvider.registerCustomSettingsTab and AdminAppConfigProvider.setSettingsTabsConfig functions!");
    }
  }]);

}).call(this);
;
(function() {
  var indexOf = [].indexOf || function(item) { for (var i = 0, l = this.length; i < l; i++) { if (i in this && this[i] === item) return i; } return -1; };

  angular.module('AdminApp').controller('AdBaseCardsEditor', ["$scope", "$element", "$attrs", "$cms", "cardsPropertyName", "allowedCardFields", "generateNewCard", "onProxyChange", function($scope, $element, $attrs, $cms, cardsPropertyName, allowedCardFields, generateNewCard, onProxyChange) {
    var ngModelCtrl;
    ngModelCtrl = $element.controller('ngModel');
    $scope.$cms = $cms;
    $scope.options = $scope.options || {};
    $scope.options.currentCard = 1;
    $scope.options.editorToolbar = 'basic';
    $scope.options.maxPaginationSize = 3;
    $scope.proxy = $scope.proxy || {};
    $scope.proxy[cardsPropertyName] = $scope.proxy[cardsPropertyName] || [];
    if ($attrs.hasOwnProperty('cmsModelItem')) {
      $scope.options.editorToolbar = 'advanced';
    }
    $scope.addCard = function(insertType) {
      var card, index;
      if ($scope.ngDisabled) {
        return;
      }
      card = generateNewCard(insertType);
      if (!card) {
        return;
      }
      if (insertType === 'before') {
        index = $scope.options.currentCard - 1;
      } else if (insertType === 'after') {
        index = $scope.options.currentCard;
      } else {
        return;
      }
      if (index < 0) {
        index = 0;
      }
      $scope.proxy[cardsPropertyName].insert(index, card);
      $scope.options.currentCard = index + 1;
    };
    $scope.moveCard = function(i) {
      if ($scope.ngDisabled) {
        return;
      }
      $scope.proxy[cardsPropertyName].move($scope.options.currentCard - 1, $scope.options.currentCard - 1 + i);
      $scope.options.currentCard += i;
    };
    $scope.removeCard = function() {
      if ($scope.ngDisabled) {
        return;
      }
      $scope.proxy[cardsPropertyName].remove($scope.options.currentCard - 1);
      if ($scope.options.currentCard > $scope.proxy[cardsPropertyName].length) {
        $scope.options.currentCard = $scope.proxy[cardsPropertyName].length;
      }
    };
    ngModelCtrl.$render = function() {
      $scope.proxy = ngModelCtrl.$modelValue || {};
      $scope.proxy[cardsPropertyName] = $scope.proxy[cardsPropertyName] || [];
      $scope.proxy.options = $scope.proxy.options || {};
      if (!$scope.proxy[cardsPropertyName][$scope.options.currentCard]) {
        $scope.options.currentCard = 1;
      }
    };
    $scope.$watch('proxy', function(proxy, oldValue) {
      var viewValue;
      if (angular.isFunction(onProxyChange)) {
        proxy = onProxyChange(proxy);
        $scope.proxy = proxy;
      }
      viewValue = angular.copy(proxy);
      viewValue[cardsPropertyName] = viewValue[cardsPropertyName].map(function(card) {
        var k;
        for (k in card) {
          if (indexOf.call(allowedCardFields, k) < 0) {
            delete card[k];
          }
        }
        return card;
      });
      ngModelCtrl.$setViewValue(viewValue);
    }, true);
    $scope.$watch('$cms.screenSize', function() {
      $scope.options.maxPaginationSize = $cms.resolveValueForScreenSize({
        xs: 3,
        sm: 4,
        md: 5,
        lg: 8
      });
    });
  }]);

}).call(this);
;
(function() {
  angular.module('AdminApp').controller('AdBaseEditView', ["$scope", "$state", "$stateParams", "$injector", "$q", "$cms", "$filter", "$bulk", "$models", "$interpolate", "$currentUser", "AdminAppConfig", "uiceItemEditor", function($scope, $state, $stateParams, $injector, $q, $cms, $filter, $bulk, $models, $interpolate, $currentUser, AdminAppConfig, uiceItemEditor) {

    /*
        контроллер для наследования.
        ====================================================================
        $scope.Model - ngResource от серверной модели
            (
            CmsArticle и его потомки,
            CmsKeyword и его потомки
            ).
        $scope.MODEL_NAME - ее название.
        $scope.MODEL_DEFAULT_ITEM - данные по умолчанию
        ==========================
        если в url есть ключ modelName, то переменные $scope.Model, $scope.MODEL_NAME
        берутся из него
     */
    var representation;
    $scope.error = 0;
    $scope.activeTab = 0;
    $scope.item = {};
    $scope.queryFilter = $scope.queryFilter || {};
    $scope.options = $scope.options || {};
    $scope.loading = false;
    $scope.forceLang = CMS_DEFAULT_LANG;
    $scope.$cms = $cms;
    $scope.$bulk = $bulk;
    $scope.$models = $models;
    $scope.$currentUser = $currentUser;
    $scope.NOTIFY_MSGS = angular.merge({
      created: 'Saved!',
      saved: 'Saved!',
      deleted: '',
      cant_save: "Error. The object cannot be saved"
    }, $scope.NOTIFY_MSGS || {});
    if ($scope.NOTIFY_MSGS.delete_object) {
      console.warn("AdBaseEditView: NOTIFY_MSGS.delete_object is deprecated!");
    }
    if ($scope.ERROR_MSGS) {
      if ($scope.ERROR_MSGS.cant_delete) {
        console.warn("AdBaseEditView: ERROR_MSGS.cant_delete is deprecated!");
      }
      if ($scope.ERROR_MSGS.cant_save) {
        console.warn("AdBaseEditView: ERROR_MSGS.cant_save is deprecated! Use NOTIFY_MSGS.cant_save");
      }
    }
    if ($stateParams.modelName && !$scope.MODEL_NAME) {
      $scope.MODEL_NAME = $stateParams.modelName;
    }
    if ($scope.MODEL_NAME) {
      $scope.Model = $injector.get($scope.MODEL_NAME);
    }
    if (!$scope.DJANGO_APP_NAME) {
      representation = $injector.get("$constants").resolve("$REPRESENTATION." + $scope.MODEL_NAME) || {};
      $scope.DJANGO_APP_NAME = representation.app || 'main_app';
    }
    if (!$scope.MODEL_DEFAULT_ITEM && $scope.MODEL_NAME) {
      $scope.MODEL_DEFAULT_ITEM = uiceItemEditor.generateNewItem($scope.MODEL_NAME);
    }
    $scope.showNotification = function(text, context, notifyType) {
      $cms.showNotification(text, context, notifyType);
    };
    $scope.gotoParentState = function() {
      var name, ref, returnTo, returnToParams;
      if ($stateParams.returnTo) {
        returnTo = $stateParams.returnTo;
      } else {
        name = $state.current.name.split('.');
        if ((ref = name[name.length - 1]) === 'edit' || ref === 'add' || ref === 'details') {
          name[name.length - 1] = 'list';
        }
        returnTo = name.join('.');
      }
      returnToParams = angular.copy($stateParams.returnToParams) || {};
      if ($stateParams.modelName && !returnToParams.modelName) {
        returnToParams.modelName = $stateParams.modelName;
      }
      $state.go(returnTo, returnToParams);
    };
    $scope.loadData = $scope.loadData || function(queryFilter) {
      var defer, filter, onFail, onOk;
      if (!$stateParams.id) {
        $scope.item = angular.copy($scope.MODEL_DEFAULT_ITEM || {});
        return;
      }
      $scope.loading = true;
      $scope.error = 0;
      $scope.item = {};
      filter = queryFilter || $scope.queryFilter || {};
      defer = $q.defer();
      if (angular.isFunction($scope.beforeLoadData)) {
        filter = $scope.beforeLoadData() || filter;
      }
      onOk = function(data) {
        $scope.item = data;
        $scope.loading = false;
        defer.resolve(data);
      };
      onFail = function(data) {
        $scope.error = data;
        $scope.loading = false;
        defer.reject(data);
      };
      $scope.Model.findById({
        id: $stateParams.id,
        filter: filter
      }, function(data) {
        if (angular.isFunction($scope.afterLoadData)) {
          data = $scope.afterLoadData(data);
          if (data && angular.isFunction(data.then)) {
            data.then(onOk, onFail);
            return;
          }
        }
        onOk(data);
      }, function(data) {
        if (angular.isFunction($scope.afterFailLoadData)) {
          data = $scope.afterFailLoadData(data);
          if (data && angular.isFunction(data.then)) {
            data.then(onFail, onFail);
            return;
          }
        }
        onFail(data);
      });
      return defer.promise;
    };
    $scope.save = function(item, skip_redirect) {
      var action, defer, k, onFail, onOk, original, params, v;
      $scope.loading = true;
      params = {};
      defer = $q.defer();
      original = item || $scope.item;
      item = {};
      for (k in original) {
        v = original[k];
        if (v instanceof File || v instanceof FileList) {
          item[k] = v;
        } else {
          item[k] = angular.copy(v);
        }
      }
      if (angular.isFunction($scope.beforeSave)) {
        item = $scope.beforeSave(item);
      }
      if (angular.isFunction($scope.getCreateUpdateRestParams)) {
        params = $scope.getCreateUpdateRestParams(item) || {};
      }
      if (!item.id) {
        action = 'create';
        if (item.isPublished) {
          item.publicationDate = new Date();
        } else if (!item.isPublished) {
          item.publicationDate = null;
        }
      } else {
        action = 'updateAttributes';
        if (item.isPublished && !item.publicationDate) {
          item.publicationDate = new Date();
        }
        if (!item.isPublished) {
          item.publicationDate = null;
        }
      }
      onOk = function(data) {
        $scope.item = data;
        $scope.loading = false;
        if (action === 'create') {
          $scope.showNotification($scope.NOTIFY_MSGS.created, {
            item: data
          });
        } else {
          $scope.showNotification($scope.NOTIFY_MSGS.saved, {
            item: data
          });
        }
        defer.resolve(data);
      };
      onFail = function(data) {
        $scope.error = data;
        $scope.loading = false;
        $scope.showNotification($scope.NOTIFY_MSGS.cant_save, {
          item: item,
          error: data
        }, 'error');
        defer.reject(data);
      };
      $scope.Model[action](params, item, function(data) {
        if (angular.isFunction($scope.afterSave)) {
          data = $scope.afterSave(data, action === 'create');
          if (data && angular.isFunction(data.then)) {
            data.then(onOk, onFail);
          } else {
            onOk(data);
          }
          return;
        }
        onOk(data);
        if (!skip_redirect) {
          $scope.gotoParentState();
        }
      }, onFail);
      return defer.promise;
    };
    $scope.saveAndPublish = function() {
      var item;
      item = angular.copy($scope.item);
      item.isPublished = true;
      $scope.save(item);
    };
    $scope.unpublish = function() {
      var item;
      item = angular.copy($scope.item);
      item.isPublished = false;
      $scope.save(item);
    };
    $scope["delete"] = $scope["delete"] || function(item) {
      item = angular.copy(item || $scope.item);
      uiceItemEditor["delete"](item, {
        modelName: $scope.MODEL_NAME
      }).then(function() {
        $scope.showNotification($scope.NOTIFY_MSGS.deleted, {
          item: item
        }, 'primary');
        if (angular.isFunction($scope.afterDelete)) {
          $scope.afterDelete(item);
          return;
        }
        return $scope.gotoParentState();
      });
    };
    $scope.isVisibleModel = function(modelName) {
      return !!AdminAppConfig.visibleModels[modelName];
    };
    if ($scope.Model) {
      $scope.loadData();
    }
  }]);

}).call(this);
;
(function() {
  angular.module('AdminApp').controller('AdBaseListView', ["$scope", "$state", "$q", "$stateParams", "$injector", "$models", "$bulk", "$cms", "$interpolate", "$currentUser", "AdminAppConfig", "uiceFileViewModal", "uiceItemEditor", function($scope, $state, $q, $stateParams, $injector, $models, $bulk, $cms, $interpolate, $currentUser, AdminAppConfig, uiceFileViewModal, uiceItemEditor) {

    /*
        контроллер для наследования.
        ==========================
        $scope.Model = (
                        CmsArticle и его потомки,
                        CmsKeyword и его потомки
                       ).
        $scope.MODEL_NAME - название модели.
        $scope.queryFilter.fields = {title:true, ...} - список полей, которые будут вытягиваться
        с сервера
        ==========================
        если в url есть ключ modelName, то переменные $scope.Model, $scope.MODEL_NAME
        берутся из него
     */
    var getSublingState, representation;
    $scope.error = 0;
    $scope.loading = false;
    $scope.itemFilter = $scope.itemFilter || {
      searchTerm: ''
    };
    $scope.queryFilter = $scope.queryFilter || {};
    $scope.options = $scope.options || {};
    $scope.forceLang = CMS_DEFAULT_LANG;
    $scope.$cms = $cms;
    $scope.$models = $models;
    $scope.$bulk = $bulk;
    $scope.$currentUser = $currentUser;
    $scope.NOTIFY_MSGS = angular.merge({}, $scope.NOTIFY_MSGS || {});
    if ($scope.NOTIFY_MSGS.delete_object) {
      console.warn("AdBaseListView: NOTIFY_MSGS.delete_object is deprecated!");
    }
    if ($scope.NOTIFY_MSGS.delete_objects) {
      console.warn("AdBaseListView: NOTIFY_MSGS.delete_objects is deprecated!");
    }
    if ($scope.ERROR_MSGS && $scope.ERROR_MSGS.cant_delete) {
      console.warn("AdBaseListView: ERROR_MSGS.cant_delete is deprecated!");
    }
    if ($stateParams.modelName && !$scope.MODEL_NAME) {
      $scope.MODEL_NAME = $stateParams.modelName;
    }
    if ($scope.MODEL_NAME) {
      $scope.Model = $injector.get($scope.MODEL_NAME);
      if (!$scope.DJANGO_APP_NAME) {
        representation = $injector.get("$constants").resolve("$REPRESENTATION." + $scope.MODEL_NAME) || {};
        $scope.DJANGO_APP_NAME = representation.app || 'main_app';
      }
    }
    if (!$scope.MODEL_QUERY_NAME) {
      $scope.MODEL_QUERY_NAME = 'query';
    }
    getSublingState = function(subling) {
      var name;
      name = $state.current.name.split('.');
      name[name.length - 1] = subling;
      return name.join('.');
    };
    $scope.showNotification = function(text, context, notifyType) {
      $cms.showNotification(text, context, notifyType);
    };
    $scope.onItemEdit = $scope.onItemEdit || function(item, ids) {
      var currentParams, editStateName;
      if (!ids) {
        ids = ($scope.items || $models[$scope.modelName].items || []).map(function(o) {
          return o.id;
        });
      }
      ids = ids.join(',') || null;
      editStateName = getSublingState('edit');
      currentParams = angular.copy($stateParams);
      if (ids.length > 1200) {
        ids = "";
      }
      $state.go(editStateName, {
        id: item.id,
        ids: ids,
        returnTo: $state.current.name,
        returnToParams: currentParams,
        modelName: $stateParams.modelName
      });
    };
    $scope.onItemAdd = $scope.onItemAdd || function() {
      var addStateName;
      addStateName = getSublingState('add');
      $state.go(addStateName, {
        returnTo: $state.current.name,
        returnToParams: $state.params,
        modelName: $stateParams.modelName
      });
    };
    $scope.onItemDelete = $scope.onItemDelete || function(item) {
      if (!item || !item.id) {
        return;
      }
      item = angular.copy(item);
      uiceItemEditor["delete"](item, {
        modelName: $scope.MODEL_NAME
      }).then(function() {
        $scope.showNotification($scope.NOTIFY_MSGS.deleted, {
          item: item
        }, 'primary');
        if (angular.isFunction($scope.afterDelete)) {
          $scope.afterDelete(item);
          return;
        }
        return $scope.forceReloadData();
      });
    };
    $scope.onItemsDelete = $scope.onItemsDelete || function(items) {
      if (!items || !items.length) {
        return;
      }
      return uiceItemEditor.deleteMultiple(items, {
        modelName: $scope.MODEL_NAME
      }).then(function() {
        $scope.showNotification($scope.NOTIFY_MSGS.deleted, {
          items: items
        }, 'primary');
        if (angular.isFunction($scope.afterDelete)) {
          $scope.afterDelete(item);
          return;
        }
        $scope.forceReloadData();
      });
    };
    $scope.onItemRenderAsFile = function(item, options) {
      var queryParams;
      options = options || {};
      if (angular.isObject(item)) {
        if (options.fileType === 'pdf') {
          queryParams = options.queryParams || {};
          queryParams.id = item.id;
          item = $scope.Model.$actionToUrl('renderPdf', queryParams);
        } else if (options.fileType === 'html') {
          queryParams = options.queryParams || {};
          queryParams.id = item.id;
          item = $scope.Model.$actionToUrl('renderPrintableHtml', queryParams);
        }
      }
      return uiceFileViewModal.open(item, options);
    };
    $scope.loadData = $scope.loadData || function(queryFilter, quickFilter) {
      var defer, filter, onFail, onOk;
      $scope.loading = true;
      $scope.error = 0;
      $scope.items = [];
      filter = queryFilter || $scope.queryFilter || {};
      defer = $q.defer();
      if (angular.isFunction($scope.beforeLoadData)) {
        filter = $scope.beforeLoadData() || filter;
      }
      if (filter.hasOwnProperty('quickFilter')) {
        quickFilter = quickFilter || filter.quickFilter;
        delete filter.quickFilter;
      }
      onOk = function(data) {
        $scope.items = data;
        $scope.loading = false;
        defer.resolve(data);
      };
      onFail = function(data) {
        $scope.error = data;
        $scope.loading = false;
        defer.reject(data);
      };
      $scope.Model[$scope.MODEL_QUERY_NAME]({
        filter: filter,
        quickFilter: quickFilter || ''
      }, function(data) {
        if (angular.isFunction($scope.afterLoadData)) {
          data = $scope.afterLoadData(data);
          if (data && angular.isFunction(data.then)) {
            data.then(onOk, onFail);
            return;
          }
        }
        onOk(data);
      }, function(data) {
        if (angular.isFunction($scope.afterFailLoadData)) {
          data = $scope.afterFailLoadData(data);
          if (data && angular.isFunction(data.then)) {
            data.then(onFail, onFail);
            return;
          }
        }
        onFail(data);
      });
      return defer.promise;
    };
    $scope.forceReloadData = function() {
      if (!$scope.USE_SERVER_PAGINATION) {
        $scope.loadData();
        return;
      }
      $scope.queryFilter._forceUpdate = new Date();
    };
    $scope.countData = function(whereFilter, quickFilter) {
      return $scope.Model.count({
        where: whereFilter || {},
        quickFilter: quickFilter || ''
      }).$promise;
    };
    $scope.isVisibleModel = function(modelName) {
      return !!AdminAppConfig.visibleModels[modelName];
    };
    if ($scope.Model && !$scope.USE_SERVER_PAGINATION) {
      $scope.loadData();
    }
    $bulk.onReady(function() {
      return $scope.ready = true;
    });
  }]);

}).call(this);
;
(function() {
  angular.module('AdminApp').controller('AdBaseModelEditor', ["$scope", "$cms", "$currentUser", "uiceItemEditor", function($scope, $cms, $currentUser, uiceItemEditor) {

    /*
        контроллер для наследования.
        ==========================
        предназначен для директив-редакторов моделей
     */
    $scope.$cms = $cms;
    $scope.$currentUser = $currentUser;
    $scope.activeTab = 0;
    $scope.options = {};
    $scope.generateNewModelItem = function(modelName, override) {
      var item;
      item = uiceItemEditor.generateNewItem(modelName);
      if (override) {
        return angular.merge(item, override);
      }
      return item;
    };
  }]);

}).call(this);
;
(function() {
  angular.module('AdminApp').directive('admKeywordsPicker', function() {
    return {
      restrict: 'E',
      scope: {},
      controller: ["$scope", function($scope) {
        throw "admKeywordsPicker: deprecated! Use aKeywordsIdListEditor instead";
      }]
    };
  }).directive('aUserPicker', function() {
    return {

      /**
      *   ngdoc directive
      *   name AdminApp.directive:aUserPicker
      *   description
      *
      *   restrict E
      *   param {object} ngModel (ссылка) модель
      *   param {string=} required установите этот атрибут в директиве для того, чтоб значение ng-model==null приводило к ошибкам валидации формы
      *   param {boolean=} ngDisabled (ссылка) флаг доступности тега
      *   example
      *       <pre>
      *           $scope.
      *       </pre>
      *       <pre>
      *           //- pug
      *           a-user-picker(ng-model="user")
      *           // обязательное поле
      *           a-user-picker(ng-model="user", required='')
      *       </pre>
      *       <example module="DocExampleApp">
      *           <file name="script.js">
      *               mkDocCtrl('aUserPickerCtrl', {
      *
      *               });
      *           </file>
      *           <file name="index.html">
      *               <div ng-controller="aUserPickerCtrl">
      *
      *               </div>
      *           </file>
      *       </example>
       */
      restrict: 'E',
      require: 'ngModel',
      scope: {
        placeholder: '@?',
        userRole: '@?',
        filterWhere: '=?',
        users: '=?',
        inputClass: '@?',
        ngDisabled: '=?',
        ngRequired: '=?'
      },
      link: function($scope, $element, $attrs, ngModelCtrl) {
        throw "aUserPicker: deprecated! Use aDbRelationEditor instead";

        /*
        $scope.loading = false
        $scope.items = []
        $scope.ngModelProxy = null
        if $attrs.hasOwnProperty('required')
            console.warn("aUserPicker: required attribute not allowed anymore. Use ngRequired")
        
        
        loadData = ()->
            $scope.loading = true
            filter = {
                fields:{
                    id:true, username:true, firstName: true, lastName: true,
                    middleName:true, name: true
                }
                where: angular.copy($scope.filterWhere or {})
            }
            if $scope.userRole
                userRole = $scope.userRole+''
                userRole = userRole.replaceAll('[', '').replaceAll(']', '')
                filter.where.userRole = {
                    inq: userRole.split(',')
                }
        
            CmsUser.find filter,
                (data)->
                    $scope.loading = false
                    $scope.items = []
                    for item in data or []
                        _item = {
                            id:item.id
                            username: item.username
                            name: composeUserName(item)
                        }
                        if _item.name
                            _item.username += ':'
                        $scope.items.push(_item)
                ,
                ()->
                    $scope.items = []
                    $scope.loading = false
        
        
        
         * обновляем модель в обе стороны
        ngModelCtrl.$render = ()->
            $scope.ngModelProxy = ngModelCtrl.$viewValue
            return
        
        $scope.$watch 'ngModelProxy', (ngModelProxy, oldValue)->
            ngModelCtrl.$setViewValue(ngModelProxy)
            return
        
        if !$attrs.users
            $scope.$watch 'userRole', (userRole, oldValue)->
                if userRole!=oldValue
                    loadData()
        
            loadData()
        else
            $scope.$watchCollection 'users', (users, oldValue)->
                if $scope.userRole
                    userRole = $scope.userRole+''
                    userRole = userRole.replaceAll('[', '').replaceAll(']', '')
                else
                    userRole = false
                $scope.items = []
                for item in users or []
                    if userRole and item.userRole and !userRole.includes(item.userRole)
                        continue
                    _item = {
                        id: item.id
                        username: item.username
                        name: composeUserName(item)
                    }
                    if _item.name
                        _item.username += ':'
                    $scope.items.push(_item)
                return
         */
      }
    };
  });

}).call(this);
;
(function() {
  angular.module('AdminApp').directive('aBtmToolbar', function() {
    return {
      restrict: 'E',
      scope: {},
      controller: ["$scope", "$attrs", "$currentUser", function($scope, $attrs, $currentUser) {
        console.error("aBtmToolbar: deprecated directive. Replace it with aFooterToolbar");
      }],
      template: ''
    };
  });

}).call(this);
;
(function() {
  angular.module('AdminApp').directive('aCardsEditor', ["$uibModal", function($uibModal) {

    /*
    modalController = ($scope, $uibModalInstance, ngModel, options)->
        $scope.ngModel = angular.copy(ngModel)
        $scope.options = options
    
        $scope.cancel = () ->
            $uibModalInstance.dismiss('cancel')
    
        $scope.ok = () ->
            $uibModalInstance.close()
    
    
    
    openModal = (ngModel, options)->
        result = $uibModal.open {
            animation: true
            windowClass: 'a-cards-editor-modal'
            template: ('/client/admin_app/_directives/aCardsEditor/modal.html', '<div class="modal-header"><button class="close" ng-click="cancel()"><span>&times;</span></button><h4 class="modal-title" translate=""> </h4></div><form class="modal-body" name="form"></form><div class="modal-footer"><button class="btn btn-success" ng-click="ok()" translate="" ng-disabled="form.$invalid">Ok</button><button class="btn btn-default" ng-click="cancel()" translate="">Cancel</button></div>' + '')
            controller: [
                '$scope'
                '$uibModalInstance'
                'ngModel'
                'options'
                modalController
            ]
            size: 'md'
            resolve:{
                ngModel: -> ngModel
                options: -> options or {}
            }
        }
        result.result
     * конец модального окна
     */
    var getCardsType;
    getCardsType = function($attrs) {
      var cardsType;
      cardsType = ($attrs.type || '').split('+')[0];
      if (!['icon', 'image'].includes(cardsType)) {
        cardsType = 'image';
      }
      if (!$attrs.cmsModelItem) {
        cardsType = 'icon';
      }
      return cardsType;
    };
    return {

      /**
      *   @ngdoc directive
      *   @name AdminApp.directive:aCardsEditor
      *   @description редактор карточек, которые можно отображать через {@link ui.landingWidgets.directive:lwCards}
      *
      *   @restrict E
      *   @param {object} ngModel (ссылка) модель-описание блока карточек
      *   @param {string} lang (ссылка) язык, на котором происходит редактирование значений текста
      *   @param {string=} [type='image+title'] (значение) тип карточки в ['icon+*', 'image+*', 'none+*']. Если указан тип 'image+*', но НЕ указан cmsModelItem, то тип будет принудительно понижен до 'icon-*'.
      *        <b>ВНИМАНИЕ:</b> после знака "+" указываются необходимые поля через запятую:
      *           <ul>
      *               <li> title - всегда активно (указывать не надо)</li>
      *               <li> subTitle - подзаголовок</li>
      *               <li> price - текстовая строка цены</li>
      *               <li> text - html-текст (если передан cmsModelItem в директиву, то будет использован редактор с полным тулбаром. иначе - усеченный)</li>
      *               <li> link - ссылка</li>
      *               <li> html - все поля выше будут проигнорированы и редактировать дадут только одно поле с шаблоном</li>
      *           </ul>
      *   @param {Object=} cmsModelItem (ссылка) объект в бд, в котором есть свойство 'files' (картинки ассоциированные с каруселью, которые должны отображаться).
      *   @param {string=} [imgSize=''] (значение) дополнительные размеры картинок, которые должны загружаться на сервер для карточки (в общем случае указывать вообще не обязательно). Размеры в px, перечисляются через запятую.
      *   @param {boolean=} ngDisabled (ссылка) флаг доступности тега
      *   @example
      *       <pre>
      *           $scope.cardsBlock = {cards: [{title: {en: "Test!"}}]}
      *           $scope.lang = 'en'
      *           CmsArticle.findOne (data)->
      *               $scope.article = data
      *       </pre>
      *       <pre>
      *           //-pug
      *           .well {{cardsBlock | json}}
      *           h3 Редактор
      *           a-cards-editor.well.bg-warning(ng-model="cardsBlock", type='image+subTitle,price,link', lang="lang", cms-model-item="article")
      *           h3 Превью
      *           lw-cards(ng-model="cardsBlock", cms-model-item="article")
      *               .bg-primary
      *                   b {{$item.title | l10n}}
      *                   p {{$item.subTitle | l10n}}
      *                   small {{$item.price | l10n}}
      *       </pre>
      *       <example module="DocExampleApp">
      *           <file name="script.js">
      *               mkDocCtrl('aCardsEditorCtrl', {
      *                   cardsBlock: {cards: [{title: {en: 'Test!'}, link: {type: ''}}]},
      *                   lang: 'en'
      *               });
      *           </file>
      *           <file name="index.html">
      *               <div ng-controller="aCardsEditorCtrl">
      *                   <div class='well'>{{cardsBlock|json}}</div>
      *                   <h2><b>Редактор (внимание: загрузка и отображение файлов в документации не работает!)</b></h2><hr>
      *                   <a-cards-editor ng-model="cardsBlock" type='image+subTitle,price,link' lang="lang" cms-model-item="article" class='bg-warning'></a-cards-editor>
      *                   <h2><b>Превью</b></h2><hr>
      *                   <lw-cards ng-model="cardsBlock" cms-model-item="article">
      *                       <div class='bg-primary text-center' style='padding: 10px'>
      *                           <b>{{$item.title | l10n}}</b>
      *                           <p>{{$item.subTitle | l10n}}</p>
      *                           <small>{{$item.price | l10n}}</small>
      *                       </div>
      *                   </lw-cards>
      *               </div>
      *           </file>
      *       </example>
       */
      restrict: 'E',
      require: 'ngModel',
      transclude: {
        cardAdditional: '?aCardsEditorCardAdditional'
      },
      scope: {
        lang: '=',
        cmsModelItem: '=?',
        ngDisabled: '=?'
      },
      controller: ["$scope", "$element", "$attrs", "$controller", function($scope, $element, $attrs, $controller) {
        var allowedFields, field, generateNewCard, j, len, onProxyChange, ref;
        $scope.options = {
          type: getCardsType($attrs),
          imgSize: $attrs.imgSize || '',
          allowedFields: {
            title: true,
            subTitle: false,
            text: false,
            price: false,
            link: false,
            template: false
          },
          LINK_TYPE: [
            {
              value: '',
              description: angular.tr("Do not use link")
            }, {
              value: 'ui-sref:app.articles.details',
              description: angular.tr("Link to article")
            }, {
              value: 'href',
              description: angular.tr("External link")
            }
          ]
        };
        if ($attrs.options) {
          throw "aCardsEditor: 'options' attribute is not allowed any more!";
        }
        if ($attrs.heading) {
          throw "aCardsEditor: 'heading' attribute is not allowed any more!";
        }
        allowedFields = ($attrs.type || '').split('+')[1] || '';
        ref = allowedFields.split(',');
        for (j = 0, len = ref.length; j < len; j++) {
          field = ref[j];
          if (!($scope.options.allowedFields.hasOwnProperty(field))) {
            continue;
          }
          $scope.options.allowedFields[field] = true;
          if (field === 'html') {
            $scope.options.allowedFields = {
              template: true
            };
            if ($attrs.type === 'html') {
              $scope.options.type = 'none';
            }
            break;
          }
        }

        /*
        $scope.openModal = () ->
            options = {}
            openModal($scope.proxy, options).then (data)->
                 * ответ от модального окна
         */
        onProxyChange = function(proxy) {
          var card, i, k, len1, ref1, ref2;
          ref1 = proxy.cards;
          for (i = k = 0, len1 = ref1.length; k < len1; i = ++k) {
            card = ref1[i];
            if ((ref2 = (card.link || {}).type) !== '' && ref2 !== (void 0)) {
              if (card.link.type === 'href' && !angular.isObject(card.link.data)) {
                proxy.cards[i].link.data = {};
              } else if (card.link.type === 'ui-sref:app.articles.details' && !angular.isNumber(card.link.data) && card.link.data) {
                proxy.cards[i].link.data = null;
              }
            }
          }
          return proxy;
        };
        generateNewCard = function() {
          var card;
          card = {
            title: {}
          };
          if ($scope.options.allowedFields.template) {
            card = {
              template: {}
            };
            card.template[$scope.lang || CMS_DEFAULT_LANG] = "<h3>" + (gettextCatalog.getString('Title')) + "</h3>\n<p>" + (gettextCatalog.getString('Text')) + "</p>";
          }
          if ($scope.options.allowedFields.link) {
            card.link = {
              data: '',
              type: ''
            };
          }
          return card;
        };
        return $controller('AdBaseCardsEditor', {
          $scope: $scope,
          $attrs: $attrs,
          $element: $element,
          cardsPropertyName: 'cards',
          allowedCardFields: ['imgId', 'faIcon', 'title', 'subTitle', 'price', 'link', 'template', 'templateUrl', 'other'],
          onProxyChange: onProxyChange,
          generateNewCard: generateNewCard
        });
      }],
      template: ('/client/admin_app/_directives/aCardsEditor/aCardsEditor.html', '<div ng-switch="proxy.cards.length == 0"><div class="text-center cards-empty" ng-switch-when="true" style="margin: 2em 0;"><div style="margin-bottom: 1em;"><i translate="">No cards</i></div><button class="btn btn-success btn-sm" ng-click="addCard(\'before\')"><div class="fas fa-plus"></div><span translate="">Add card</span></button></div><div ng-switch-when="false"><div class="text-center"><ul class="list-table nomargin"><li class="text-left" title="{{\'Cards\' | translate}}"><ul class="nomargin pagination-sm" uib-pagination="" boundary-links="false" total-items="proxy.cards.length" ng-model="options.currentCard" previous-text="&lt;" next-text="&gt;" force-ellipses="true" items-per-page="1" max-size="options.maxPaginationSize" ng-disabled="ngDisabled"></ul></li><li class="text-right"><div class="btn-group"><button class="btn btn-default btn-sm" ng-click="moveCard(-1)" ng-if="$cms.screenSize != \'xs\'" ng-disabled="options.currentCard == 1" title="{{\'Move card left\' | translate}}"><div class="fas fa-arrow-left nopadding"></div></button><button class="btn btn-default btn-sm" ng-click="moveCard(1)" ng-if="$cms.screenSize != \'xs\'" ng-disabled="options.currentCard == proxy.cards.length" title="{{\'Move card right\' | translate}}"><div class="fas fa-arrow-right nopadding"></div></button><div class="btn-group" uib-dropdown=""><button class="btn btn-success btn-sm" uib-dropdown-toggle=""><span translate=""> Card</span><span> </span><span class="caret"></span></button><ul class="dropdown-menu pull-right" uib-dropdown-menu=""><li class="hidden-sm hidden-md hidden-lg" ng-if="options.currentCard != 1"><a ng-click="moveCard(-1)" role="button"><i class="fas fa-arrow-left"></i><span translate="">Move card left</span></a></li><li class="hidden-sm hidden-md hidden-lg" ng-if="options.currentCard != proxy.cards.length"><a ng-click="moveCard(0)" role="button"><i class="fas fa-arrow-right"></i><span translate="">Move card right</span></a></li><li> <a ng-click="addCard(\'after\')" role="button"><i class="fas fa-plus"></i><span translate="">Add card after current</span></a></li><li> <a ng-click="addCard(\'before\')" role="button"><i class="fas fa-plus"></i><span translate="">Add card before current</span></a></li><li><a ng-click="removeCard()" role="button"><i class="fas fa-trash-alt text-danger"></i><span class="text-danger" translate="">Remove current card</span></a></li></ul></div></div></li></ul></div><div class="well well-sm"><div class="row"><div ng-switch="options.type" ng-class="{\'col-md-35\': options.type != \'none\' || options.allowedFields.link }"><div class="form-group" ng-switch-when="icon"><label translate="translate">Icon</label><div class="thumbnail thumbnail-icon text-center hidden-xs hidden-sm" ng-show="proxy.cards[options.currentCard-1].faIcon &amp;&amp; options.allowedFields.text || options.allowedFields.html"><div class="fa-8x {{proxy.cards[options.currentCard-1].faIcon}}"></div></div><uice-fa-icon-picker ng-model="proxy.cards[options.currentCard-1].faIcon" type="class"></uice-fa-icon-picker></div><div class="form-group" ng-switch-when="image"><label ng-switch="!!proxy.cards[options.currentCard-1].imgId"><span translate="" ng-switch-when="false">Choose picture for card</span><span translate="" ng-switch-when="true">Picture</span></label><uice-file-id-picker ng-model="proxy.cards[options.currentCard-1].imgId" cms-model-item="$parent.$parent.cmsModelItem" accept="image/*" accept-thumb-sizes="{{:: options.imgSize}}" allow-preview="true"></uice-file-id-picker></div><div class="form-group" ng-if="options.allowedFields.link"><label translate="">Link</label><uic-constant ng-model="proxy.cards[options.currentCard-1].link.type" radio="options.LINK_TYPE"></uic-constant></div></div><div ng-class="{\'col-md-65\': options.type != \'none\' || options.allowedFields.link, \'col-md-100\': options.type == \'none\' &amp;&amp; !options.allowedFields.link}"><div ng-switch="proxy.cards[options.currentCard-1].link.type" ng-if="options.allowedFields.link"><div class="form-group" ng-switch-when="ui-sref:app.articles.details"><label translate="">Article</label><a-db-relation-editor ng-model="proxy.cards[options.currentCard-1].link.data" model-name="CmsArticle" filter-where="{isPublished: true}" current-ser-permission-create="*.add_CmsArticle" current-ser-permission-edit="*.change_CmsArticle"></a-db-relation-editor></div><div ng-switch-when="href"><uice-l10n-input class="form-group" ng-model="proxy.cards[options.currentCard-1].link.data" lang="$parent.$parent.$parent.lang" placeholder="{{\'Link to site. Ex.: google.com\' | translate}}" label="Link"></uice-l10n-input></div></div><uice-l10n-input class="form-group" ng-if="options.allowedFields.title" ng-model="proxy.cards[options.currentCard-1].title" lang="$parent.$parent.lang" label="Title"></uice-l10n-input><uice-l10n-input class="form-group" ng-if="options.allowedFields.subTitle" ng-model="proxy.cards[options.currentCard-1].subTitle" lang="$parent.$parent.lang" label="Subtitle"></uice-l10n-input><uice-l10n-input class="form-group" ng-if="options.allowedFields.price" ng-model="proxy.cards[options.currentCard-1].price" lang="$parent.$parent.lang" label="Price text"></uice-l10n-input><uice-l10n-input class="form-group" ng-if="options.allowedFields.text" ng-model="proxy.cards[options.currentCard-1].text" lang="$parent.$parent.lang" type="html" toolbar="basic" label="Text"></uice-l10n-input><uice-l10n-input class="form-group" ng-if="options.allowedFields.template" ng-model="proxy.cards[options.currentCard-1].template" lang="$parent.$parent.lang" type="html" toolbar="{{:: options.editorToolbar}}" label="Card content"></uice-l10n-input><div uic-transclude="cardAdditional" uic-transclude-bind="{$item: proxy.cards[options.currentCard-1], $lang: $parent.lang}"></div></div></div></div></div></div>' + '')
    };
  }]);

}).call(this);
;
(function() {
  angular.module('AdminApp').directive('aCarouselEditor', ["$uibModal", function($uibModal) {
    var getSliderType, modalController, openModal;
    getSliderType = function($attrs) {
      var sliderType;
      sliderType = $attrs.type;
      if (!['html', 'image+html'].includes(sliderType)) {
        sliderType = 'image+html';
      }
      if (!$attrs.cmsModelItem) {
        sliderType = 'html';
      }
      return sliderType;
    };
    modalController = function($scope, $uibModalInstance, ngModel, options) {
      $scope.ngModel = angular.copy(ngModel);
      $scope.options = options;
      $scope.cancel = function() {
        return $uibModalInstance.dismiss('cancel');
      };
      return $scope.ok = function() {
        return $uibModalInstance.close($scope.ngModel);
      };
    };
    openModal = function(ngModel, options) {
      var result;
      result = $uibModal.open({
        animation: true,
        windowClass: 'a-carousel-editor-modal',
        template: ('/client/admin_app/_directives/aCarouselEditor/modal.html', '<div class="modal-header"><button class="close" ng-click="cancel()"><span>&times;</span></button><h4 class="modal-title" translate="">Slider settings</h4></div><form class="modal-body" name="form"><uic-checkbox ng-model="ngModel.options.noPause"><span translate="">Don\'t pause slider on mouseover</span></uic-checkbox><uic-checkbox ng-model="ngModel.options.noTransition"><span translate="">Disable the transition animation between slides</span></uic-checkbox><uic-checkbox ng-model="ngModel.options.noWrap"><span translate="">Disable the looping of slides</span></uic-checkbox><div class="form-group"><label translate="">An interval to cycle through the slides(msec)</label><input class="form-control input-sm" ng-model="ngModel.options.interval" min="1" type="number" step="1"/></div></form><div class="modal-footer"><button class="btn btn-primary" ng-click="ok()" translate="" ng-disabled="form.$invalid">Ok</button><button class="btn btn-default" ng-click="cancel()" translate="">Cancel</button></div>' + ''),
        controller: ['$scope', '$uibModalInstance', 'ngModel', 'options', modalController],
        size: 'md',
        resolve: {
          ngModel: function() {
            return ngModel;
          },
          options: function() {
            return options || {};
          }
        }
      });
      return result.result;
    };
    return {

      /**
      *   @ngdoc directive
      *   @name AdminApp.directive:aCarouselEditor
      *   @description редактор слайдера (отображать можно с помощью {@link ui.landingWidgets.directive:lwCarousel})
      *
      *   @restrict E
      *   @param {object} ngModel (ссылка) модель-описание слайдера
      *   @param {string} lang (ссылка) язык, на котором происходит редактирование значений текста
      *   @param {string=} [type='image+html'] (значение) тип карусели в ['html', 'image+html']. Если указан тип 'image+html', но НЕ указан cmsModelItem, то тип будет принудительно понижен до 'html'
      *   @param {Object=} cmsModelItem (ссылка) объект в бд, в котором есть свойство 'files' (картинки ассоциированные с каруселью, которые должны отображаться).
      *   @param {string=} [imgSize='1600x700,800x600'] (значение) размеры картинок, которые должны загружаться на сервер для карусели. Размеры в px, перечисляются через запятую.
      *       Первый размер - для десктопов (lg, md), второй - для мобилок (sm, xs). Если указан только первый размер, то он будет использован для всех экранов. Подробно см модуль AdminApp и директиву в нем aCorouselEditor
      *   @param {boolean=} ngDisabled (ссылка) флаг доступности тега
      *   @example
      *       <pre>
      *           $scope.slider = {slides: [{template: {en: "Test!"}}], options: {}}
      *           $scope.lang = 'en'
      *           CmsArticle.findOne (data)->
      *               $scope.article = data
      *       </pre>
      *       <pre>
      *           //-pug
      *           .well {{slider | json}}
      *           h3 Редактор
      *           a-carousel-editor.well.bg-warning(ng-model="slider", type='image+html', lang="lang", cms-model-item="article")
      *           h3 Превью
      *           lw-carousel.bg-primary(ng-model="slider", type='image+html', cms-model-item="article")
      *       </pre>
      *       <example module="DocExampleApp">
      *           <file name="script.js">
      *               mkDocCtrl('aCarouselEditorCtrl', {
      *                   slider: {slides: [{template: {en: '<'+'h3'+'>Test!</'+'h3'+'>'}}], options: {}},
      *                   lang: 'en'
      *               });
      *           </file>
      *           <file name="index.html">
      *               <div ng-controller="aCarouselEditorCtrl">
      *                   <div class='well'>{{slider|json}}</div>
      *                   <h2><b>Редактор (внимание: загрузка и отображение файлов в документации не работает!)</b></h2><hr>
      *                   <a-carousel-editor ng-model="slider" type='image+html' lang="lang" cms-model-item="article" class='bg-warning'></a-carousel-editor>
      *                   <h2><b>Превью</b></h2><hr>
      *                   <lw-carousel ng-model="slider" type='image+html' cms-model-item="article" class='bg-primary'></lw-carousel>
      *               </div>
      *           </file>
      *       </example>
       */
      restrict: 'E',
      require: 'ngModel',
      scope: {
        lang: '=',
        cmsModelItem: '=?',
        ngDisabled: '=?'
      },
      controller: ["$scope", "$element", "$attrs", "$cms", "$controller", "gettextCatalog", function($scope, $element, $attrs, $cms, $controller, gettextCatalog) {
        $scope.options = {
          type: getSliderType($attrs),
          imgSize: $attrs.imgSize || '1600x700,800x600'
        };
        $scope.openSettingsModal = function() {
          openModal($scope.proxy).then(function(data) {
            $scope.proxy.options = data.options;
          });
        };
        $controller('AdBaseCardsEditor', {
          $scope: $scope,
          $attrs: $attrs,
          $element: $element,
          cardsPropertyName: 'slides',
          allowedCardFields: ['imgId', 'template', 'templateUrl'],
          onProxyChange: angular.identity,
          generateNewCard: function() {
            var slide;
            slide = {
              template: {}
            };
            slide.template[$scope.lang || CMS_DEFAULT_LANG] = "<h3>" + (gettextCatalog.getString('Title')) + "</h3>\n<p>" + (gettextCatalog.getString('Text')) + "</p>";
            return slide;
          }
        });
      }],
      template: ('/client/admin_app/_directives/aCarouselEditor/aCarouselEditor.html', '<div ng-switch="proxy.slides.length == 0"><div class="text-center cards-empty" ng-switch-when="true" style="margin: 2em 0;"><div style="margin-bottom: 1em;"><i translate="">Slider is empty</i></div><button class="btn btn-success btn-sm" ng-click="addCard(\'before\')"><div class="fas fa-plus"></div><span translate="">Add slide</span></button></div><div ng-switch-when="false"><div class="text-center"><ul class="list-table nomargin"><li class="text-left" title="{{\'Slides\' | translate}}"><ul class="nomargin pagination-sm" uib-pagination="" boundary-links="false" total-items="proxy.slides.length" ng-model="options.currentCard" previous-text="&lt;" next-text="&gt;" force-ellipses="true" items-per-page="1" max-size="options.maxPaginationSize" ng-disabled="ngDisabled"></ul></li><li class="text-right"><div class="btn-group"><button class="btn btn-default btn-sm" ng-click="moveCard(-1)" ng-if="$cms.screenSize != \'xs\'" ng-disabled="options.currentCard == 1" title="{{\'Move slide left\' | translate}}"><div class="fas fa-arrow-left nopadding"></div></button><button class="btn btn-default btn-sm" ng-click="moveCard(1)" ng-if="$cms.screenSize != \'xs\'" ng-disabled="options.currentCard == proxy.slides.length" title="{{\'Move slide right\' | translate}}"><div class="fas fa-arrow-right nopadding"></div></button><div class="btn-group" uib-dropdown=""><button class="btn btn-success btn-sm" uib-dropdown-toggle=""><span translate=""> Slide</span><span> </span><span class="caret"></span></button><ul class="dropdown-menu pull-right" uib-dropdown-menu=""><li class="hidden-sm hidden-md hidden-lg" ng-if="options.currentCard != 1"><a ng-click="moveCard(-1)" role="button"><i class="fas fa-arrow-left"></i><span translate="">Move slide left</span></a></li><li class="hidden-sm hidden-md hidden-lg" ng-if="options.currentCard != proxy.slides.length"><a ng-click="moveCard(0)" role="button"><i class="fas fa-arrow-right"></i><span translate="">Move slide right</span></a></li><li> <a ng-click="addCard(\'after\')" role="button"><i class="fas fa-plus"></i><span translate="">Add slide after current</span></a></li><li> <a ng-click="addCard(\'before\')" role="button"><i class="fas fa-plus"></i><span translate="">Add slide before current</span></a></li><li><a ng-click="removeCard()" role="button"><i class="fas fa-trash-alt text-danger"></i><span class="text-danger" translate="">Remove current slide</span></a></li></ul></div><button class="btn btn-default btn-sm" ng-click="openSettingsModal()"><div class="fas fa-cog" ng-class="{nopadding: $cms.screenSize == \'xs\'}"></div><span class="hidden-xs" translate="">Settings</span></button></div></li></ul></div><div class="well well-sm" ng-switch="options.type"><div ng-switch-when="html"><uice-l10n-input class="form-group nomargin" ng-model="proxy.slides[options.currentCard-1].template" lang="$parent.$parent.lang" type="html" toolbar="{{:: options.editorToolbar}}" label="Slide content"></uice-l10n-input></div><div class="row" ng-switch-when="image+html"><div class="col-md-35"><div class="form-group"><label ng-switch="!!proxy.slides[options.currentCard-1].imgId"><span translate="" ng-switch-when="false">Choose picture for slide</span><span translate="" ng-switch-when="true">Picture</span></label><uice-file-id-picker ng-model="proxy.slides[options.currentCard-1].imgId" cms-model-item="$parent.$parent.cmsModelItem" accept="image/*" accept-thumb-sizes="{{:: options.imgSize}}" allow-preview="true"></uice-file-id-picker></div></div><div class="col-md-65"><uice-l10n-input class="form-group nomargin" ng-model="proxy.slides[options.currentCard-1].template" label="Content" lang="$parent.$parent.lang" type="html" cms-model-item="$parent.$parent.cmsModelItem"></uice-l10n-input></div></div></div></div></div>' + '')
    };
  }]);

}).call(this);
;
(function() {
  angular.module('AdminApp').directive('aDashboardBaseObjectsCard', function() {
    return {
      restrict: 'E',
      transclude: true,
      scope: {
        cssClass: '@?',
        iconClass: '@?',
        addHref: '@?',
        listHref: '@?',
        name: '@?'
      },
      controller: function() {},
      template: ('/client/admin_app/_directives/aDashboardAdmin/aDashboardBaseObjectsCard.html', '<div class="btn-group"><div class="btn darken col-xs-40 text-center material-shadow btn-{{cssClass}}"><span ng-switch="!!addHref"><a class="fas {{iconClass}}" ng-href="{{addHref}}" ng-switch-when="true"></a><div class="fas {{iconClass}}" ng-switch-when="false"></div></span><div class="overlay overlay-right" ng-if="addHref"><div><a class="fas fa-plus" title="{{\'Add new\' | translate}}" ng-href="{{addHref}}"></a></div></div></div><a class="btn col-xs-60 material-shadow btn-{{cssClass}}" ng-href="{{listHref}}"><div class="title" ng-transclude=""></div><div class="title text-uppercase name">{{ name | translate}}</div><div class="overlay overlay-left" ng-if="listHref"><div><span class="subtitle" translate="">Details</span></div></div></a></div>' + '')
    };
  }).directive('aDashboardAdmin', ["AdminAppConfig", "$state", function(AdminAppConfig, $state) {
    var getHref;
    getHref = function(href) {
      if (!href) {
        return;
      }
      if (href.indexOf('/') !== 0) {
        return $state.href(href);
      }
      return href;
    };
    return {

      /**
      *   @ngdoc directive
      *   @name AdminApp.directive:aDashboardAdmin
      *   @description
      *
      *   @restrict E
      *   @param {object} ngModel (ссылка) модель
      *   @param {boolean=} ngDisabled (ссылка) флаг доступности тега
      *   @example
      *       <pre>
      *           $scope.
      *       </pre>
      *       <pre>
      *           //- pug
      *       </pre>
      *       <example module="DocExampleApp">
      *           <file name="script.js">
      *               mkDocCtrl('aDashboardAdminCtrl', {
      *
      *               });
      *           </file>
      *           <file name="index.html">
      *               <div ng-controller="aDashboardAdminCtrl">
      *
      *               </div>
      *           </file>
      *       </example>
       */
      restrict: 'E',
      scope: {},
      controller: ["$scope", "$injector", "$cms", "$models", "$state", "$bulk", "$currentUser", "H", "CmsSettings", "aIntegrationsModal", function($scope, $injector, $cms, $models, $state, $bulk, $currentUser, H, CmsSettings, aIntegrationsModal) {
        var additionalBulkOperations, bulkStateOperations, card, error, i, len, ref, saveSettings;
        $scope.$currentUser = $currentUser;
        $scope.$cms = $cms;
        $scope.lang = CMS_DEFAULT_LANG;
        $scope.$models = $models;
        bulkStateOperations = {};
        additionalBulkOperations = {};
        try {
          bulkStateOperations = $state.current.$bulk();
        } catch (error1) {
          error = error1;
        }
        $scope.dashboardCardsHtml = "";
        ref = AdminAppConfig.dashboardCards;
        for (i = 0, len = ref.length; i < len; i++) {
          card = ref[i];
          card = angular.copy(card);
          if ($currentUser.hasPermission('*.add_' + card.modelName)) {
            card.addHref = getHref(card.addHref);
          } else {
            delete card.addHref;
          }
          if ($currentUser.hasPermission('*.view_' + card.modelName)) {
            card.listHref = getHref(card.listHref);
          } else {
            delete card.listHref;
          }
          if (!(bulkStateOperations[card.modelName] || {}).count) {
            additionalBulkOperations[card.modelName] = {
              count: {
                $replaceObj: 'dashboardCount'
              }
            };
            card.inner = card.inner || ("{{$models." + card.modelName + ".dashboardCount.count || 0}}");
          } else {
            card.inner = card.inner || ("{{$models." + card.modelName + ".count.count || 0}}");
          }
          $scope.dashboardCardsHtml += "<a-dashboard-base-objects-card class=\"col-lg-25 col-md-33 col-sm-50 col-xs-100\" " + (H.convert.objToHtmlProps(card)) + ">\n    " + card.inner + "\n</a-dashboard-base-objects-card>";
        }
        if (!angular.isEmpty(additionalBulkOperations)) {
          $bulk.execute(additionalBulkOperations);
        }
        saveSettings = function(data, fields, onDone) {
          var _data, field, j, len1;
          _data = {
            id: data.id
          };
          for (j = 0, len1 = fields.length; j < len1; j++) {
            field = fields[j];
            _data[field] = data[field];
          }
          CmsSettings.updateAttributes({
            id: data.id
          }, _data, function(data) {
            $cms.loadSettings(data);
            if (angular.isFunction(onDone)) {
              onDone();
            }
          });
        };
        $scope.getCountryCode = function(lang) {
          if (lang === 'en') {
            return 'gb';
          }
          return lang;
        };
        $scope.toggleShowTitleContactEdit = function() {
          if (!$scope.showTitleContactEdit) {
            CmsSettings.findOne(function(data) {
              $scope.showTitleContactEdit = true;
              $scope.settings = data;
            });
            return;
          }
          saveSettings($scope.settings, ['siteTitle', 'seo', 'publicContacts'], function() {
            $scope.showTitleContactEdit = false;
          });
        };
        $scope.hideShowTitleContactEdit = function() {
          return $scope.showTitleContactEdit = false;
        };
        $scope.addIntegration = function() {
          aIntegrationsModal.add($cms.settings.integrations).then(function(data) {
            var base;
            (base = $cms.settings).integrations || (base.integrations = {});
            $cms.settings.integrations[data.type] = data.data;
          });
        };
        return $scope.$watch('$cms.settings.integrations', function(integrations, oldValue) {
          $scope.hasIntegrations = !angular.isEmpty(integrations);
          if (!angular.equals(integrations, oldValue)) {
            saveSettings($cms.settings, ['integrations']);
          }
        }, true);
      }],
      template: ('/client/admin_app/_directives/aDashboardAdmin/aDashboardAdmin.html', '<div class="clearfix row" uic-bind-html="dashboardCardsHtml"></div><div class="clearfix row" uic-if-on-current-user-permission="isAdmin()"><div class="col-md-50"><div class="panel panel-primary material-shadow"><div class="panel-heading"><h4 class="panel-title" translate="">Main</h4><button class="btn btn-xs btn-primary" ng-click="toggleShowTitleContactEdit()" ng-show="!showTitleContactEdit"><span class="fas fa-pencil-alt"></span><span translate="">Edit</span></button><div class="btn-group" ng-show="showTitleContactEdit"><button class="btn btn-xs btn-success" ng-click="toggleShowTitleContactEdit()"><span class="fas fa-save"></span><span translate="">Save</span></button><button class="btn btn-xs btn-default" translate="" ng-click="hideShowTitleContactEdit()">Close</button></div></div><div class="panel-body" ng-if="!showTitleContactEdit"><p><b translate="">Site title</b><b>:</b><span ng-repeat="(lang, title) in $cms.settings.siteTitle" ng-if="title"><flag country="{{::getCountryCode(lang)}}"></flag>{{::title}}</span></p><p><b translate="">Public contacts</b><b>:</b><uic-contact ng-repeat="item in $cms.settings.publicContacts track by $index" ng-model="item" style="margin-right:3px;margin-left:3px;"></uic-contact><span class="text-danger" ng-show="!$cms.settings.publicContacts.length" style="padding-left:3px;"><span translate="">No contacts</span><small> (<span translate="">click \'Edit\' button to add contacts</span>)</small></span></p><p><b translate="">Social links</b><b>:</b><span class="text-danger" ng-show="!$cms.settings.seo.social.length" style="padding-left:3px;"><span translate="">No links</span><small> (<span translate="">click \'Edit\' button to add social links</span>)</small></span></p><div class="social-links"><uic-site-brand-a ng-repeat="item in $cms.settings.seo.social track by $index" href="{{::item}}" target="_blank" style="margin-right: 10px;"></uic-site-brand-a></div></div><div class="panel-body" ng-if="showTitleContactEdit"><div class="form-group"><label translate="">Site title</label><div class="input-group"><input class="form-control" ng-model="settings.siteTitle[lang]"/><div class="input-group-btn"><uic-lang-picker ng-model="lang" display="dropdown"></uic-lang-picker></div></div></div><div class="form-group"><label translate="">Public contacts</label><uice-contacts-editor ng-model="settings.publicContacts"></uice-contacts-editor></div><div class="form-group"><label translate="">Links to facebook, twitter, etc. social accounts</label><uice-social-links-editor ng-model="settings.seo.social"></uice-social-links-editor></div></div></div></div><div class="col-md-50"><div class="panel panel-primary material-shadow"><div class="panel-heading"><h4 class="panel-title" translate="">Integrations</h4><button class="btn btn-xs btn-primary" ng-click="addIntegration()" ng-if="hasIntegrations"><span class="fas fa-plus"></span><span translate="">Add Service</span></button></div><a-integrations-editor class="panel-body nopadding" ng-model="$cms.settings.integrations"></a-integrations-editor></div></div></div>' + '')
    };
  }]);

}).call(this);
;
(function() {
  var getRelation;

  getRelation = function($attrs) {
    var ref;
    if (((ref = $attrs.type) !== 'belongsTo' && ref !== 'hasOne' && ref !== 'hasMany') || $attrs.type === 'hasOne') {
      return 'belongsTo';
    }
    return $attrs.type;
  };

  angular.module('AdminApp').controller('ADbRelationEditorCtrl', ["$scope", "$element", "$attrs", "$constants", "$injector", "uiceItemEditor", function($scope, $element, $attrs, $constants, $injector, uiceItemEditor) {
    var $currentUser, fields, getDefaultItem, html, j, len, ngModelCtrl, prop, ref, relationType, representation, resource, where;
    ngModelCtrl = $element.controller('ngModel');
    $scope.proxy = null;
    $scope.permissions = {
      create: true,
      edit: true
    };
    $currentUser = null;
    relationType = getRelation($attrs);
    representation = $constants.resolve("$REPRESENTATION." + $attrs.modelName) || {};
    representation.html || (representation.html = '');
    getDefaultItem = function() {
      var item;
      if (angular.isFunction($scope.getNewItem)) {
        item = $scope.getNewItem();
      }
      return item || {};
    };
    $scope.createItem = function() {
      var options;
      if ($scope.ngDisabled || !$scope.permissions.create) {
        return;
      }
      options = {
        modelName: $attrs.modelName,
        title: representation.name
      };
      uiceItemEditor.edit(getDefaultItem(), options).then(function(data) {
        $scope.items.push(data);
        if (relationType === 'belongsTo') {
          $scope.proxy = data.id;
        } else {
          $scope.proxy = $scope.proxy || [];
          $scope.proxy.push(data.id);
        }
      });
    };
    $scope.editItem = function(item) {
      var options;
      if ($scope.ngDisabled || !$scope.permissions.edit) {
        return;
      }
      item = item || $scope.items.findByProperty('id', $scope.proxy);
      if (!item) {
        return;
      }
      options = {
        modelName: $attrs.modelName,
        title: representation.name
      };
      uiceItemEditor.edit(item, options).then(function(data) {
        var _item, i, j, len, ref;
        ref = $scope.items;
        for (i = j = 0, len = ref.length; j < len; i = ++j) {
          _item = ref[i];
          if (!(_item.id === item.id)) {
            continue;
          }
          $scope.items[i] = data;
          break;
        }
      });
    };
    ngModelCtrl.$render = function() {
      var modelValue;
      modelValue = ngModelCtrl.$modelValue;
      $scope.proxy = modelValue;
    };
    $scope.$watchCollection('proxy', function(proxy, oldValue) {
      var viewValue;
      viewValue = proxy;
      ngModelCtrl.$setViewValue(viewValue);
    });
    if ($attrs.currentUserPermissionCreate) {
      $currentUser = $currentUser || $injector.get('$currentUser');
      $scope.permissions.create = $currentUser.resolvePermissionsString($attrs.currentUserPermissionCreate);
    }
    if ($attrs.currentUserPermissionEdit) {
      $currentUser = $currentUser || $injector.get('$currentUser');
      $scope.permissions.edit = $currentUser.resolvePermissionsString($attrs.currentUserPermissionEdit);
    }
    if ($attrs.hasOwnProperty('items')) {
      return;
    }
    $element.addClass('loading-data');
    fields = {};
    where = {};
    if ($attrs.filterWhere) {
      where = $scope.$parent.$eval($attrs.filterWhere);
      if (!angular.isObject(where)) {
        where = {};
      }
    }
    if ($attrs.filterFields) {
      fields = $scope.$parent.$eval($attrs.filterFields);
      if (!angular.isObject(fields)) {
        fields = {};
      }
    }
    if (angular.isEmpty(fields)) {
      html = representation.html.trim();
      fields = {
        id: true
      };
      ref = ['title', 'name', 'username', 'fullName', 'identifier'];
      for (j = 0, len = ref.length; j < len; j++) {
        prop = ref[j];
        if (html.includes("$item." + prop)) {
          fields[prop] = true;
        }
      }
    }
    resource = $injector.get($attrs.modelName);
    resource.find({
      filter: {
        where: where,
        fields: fields
      }
    }, function(data) {
      $scope.items = data;
      $element.addClass('ready');
    });
  }]).directive('aDbRelationEditor', function() {
    return {

      /**
      *   @ngdoc directive
      *   @name AdminApp.directive:aDbRelationEditor
      *   @description
      *       директива для редактирования ссылок на другие объекты в бд. Поддерживаются типы отношений hasOne, hasMany, belongsTo. Подробнее см примеры использования.
      *   @restrict E
      *   @param {number|Array<number>} ngModel (ссылка) id объекта или массив id объектов
      *   @param {string=} [type='belongsTo'] (значение) тип взаимоотношений. В ['hasOne', 'hasMany', 'belongsTo']. Для hasMany в ngModel будет записан массив, иначе - id объекта (число)
      *   @param {boolean=} [sorted=false] (значение) <b>только для типа hasMany</b>. Дает возможность менять порядок выбранных элементов.
      *   @param {string} modelName (значение) название модели, на которую ссылаются отношением. Например: CmsArticle.
      *   @param {object=} filterWhere (значение) фильтр для загрузки modelName (если не указано свойство items). Например: {isPublished: true, id__gte: 10}
      *   @param {object=} filterFields (значение) загружаемые свойства для modelName (если не указано свойство items). Например: {id: true, title: true}.
      *        Если не указано, то поля будут подцеплены из константы $REPRESENTATION.&lt;modelName&gt;.html.
      *   @param {Array<Object>=} items (ссылка) объекты, среди которых возможен выбор (если свойство не указано, то будут загружены объекты по modelName)/
      *   @param {Function=} getNewItem (ссылка) функция для генерации новых объектов по умолчанию (когда пользователь нажимает в директиве кнопку "Создать").
      *           Если функция не передана, то будет сгенерен пустой объект. Затем новый объет будет передан в uiceItemEditor.edit (где этот сервис добавит недостающие поля)
      *   @param {string=} [currentUserPermissionCreate='true'] (значение) текстовая строка разрешения, которая проверяется на {@link ui.cms.service:$currentUser#hasPermission} для определения может ли пользователь создавать объекты modelName
      *   @param {string=} [currentUserPermissionEdit='true'] (значение) текстовая строка разрешения, которая проверяется на {@link ui.cms.service:$currentUser#hasPermission} для определения может ли пользователь создавать объекты modelName
      *   @param {boolean=} ngRequired (ссылка) флаг обязательности заполнения
      *   @param {boolean=} ngDisabled (ссылка) флаг доступности тега
      *   @example
      *       <pre>
      *           $scope.articleId = 1
      *           $scope.articleIds = [2,3]
      *           $scope.fakeArticleIds = []
      *           $scope.myFakeArticles = [
      *               {id: 99, title: 'fake 99'}
      *               {id: 100, title: 'fake 100'}
      *           ]
      *       </pre>
      *       <pre>
      *           //-pug
      *           // belongsTo-отношение
      *           a-db-relation-editor(
      *               ng-model="articleId",
      *               model-name='CmsArticle',
      *               current-user-permission-create='main_app.add_CmsArticle',
      *               current-user-permission-edit='main_app.change_CmsArticle',
      *           )
      *           // belongsTo-отношение, с запретом любому пользователю создавать статьи из директивы
      *           a-db-relation-editor(
      *               ng-model="articleId",
      *               model-name='CmsArticle',
      *               current-user-permission-create='false',
      *               current-user-permission-edit='main_app.change_CmsArticle',
      *           )
      *           // hasMany-отношение
      *           a-db-relation-editor(
      *               ng-model="articleIds",
      *               type='hasMany',
      *               model-name='CmsArticle',
      *               current-user-permission-create='main_app.add_CmsArticle',
      *               current-user-permission-edit='main_app.change_CmsArticle',
      *           )
      *           // hasMany-отношение, с предзагруженными объектами. разрешено перемещать объекты
      *           a-db-relation-editor(
      *               ng-model="myFakeArticles",
      *               type='hasMany', sorted='true',
      *               items="myFakeArticles",
      *               model-name='CmsArticle',
      *               current-user-permission-create='main_app.add_CmsArticle',
      *               current-user-permission-edit='main_app.change_CmsArticle',
      *           )
      *       </pre>
      *       <example module="DocExampleApp">
      *           <file name="script.js">
      *               mkDocCtrl('aDbRelationEditorCtrl', {
      *                   articleId: 1,
      *                   articleIds: [2,3],
      *                   fakeArticleIds: [],
      *                   myFakeArticles: [{id: 99, title: 'fake 99'}, {id: 100, title: 'fake 100'}],
      *                   articles: [{id: 1, title: 'Article 1'}, {id: 2, title: 'Article #2'}, {id: 3, title: 'Article three'}],
      *               });
      *           </file>
      *           <file name="index.html">
      *               <div ng-controller="aDbRelationEditorCtrl">
      *                   <b class='text-danger'>Создание и редактирование объектов в документации невозможно. Директива откроет модальное окно, но оно будет практически бесполезным</b><hr>
      *                   <div class='form-group'>
      *                       <label>belongsTo-отношение. <b>articleId = {{articleId | json}}</b></label>
      *                       <a-db-relation-editor
      *                           ng-model="articleId" type='belongsTo' model-name='CmsArticle'
      *                           items="articles"
      *                           >
      *                       </a-db-relation-editor>
      *                   </div>
      *                   <div class='form-group'>
      *                       <label>belongsTo-отношение, с запретом любому пользователю создавать статьи из директивы. <b>articleId = {{articleId | json}}</b></label>
      *                       <a-db-relation-editor
      *                           ng-model="articleId" type='belongsTo' model-name='CmsArticle'
      *                           items="articles" current-user-permission-create='false'
      *                           >
      *                       </a-db-relation-editor>
      *                   </div>
      *                   <div class='form-group'>
      *                       <label>hasMany-отношение. <b>articleIds = {{articleIds | json}}</b></label>
      *                       <a-db-relation-editor
      *                           ng-model="articleIds" type='hasMany' model-name='CmsArticle'
      *                           items="articles"
      *                           >
      *                       </a-db-relation-editor>
      *                   </div>
      *                   <div class='form-group'>
      *                       <label>hasMany-отношение, с предзагруженными объектами. разрешено перемещать объекты <b>fakeArticleIds = {{fakeArticleIds | json}}</b></label>
      *                       <a-db-relation-editor
      *                           ng-model="fakeArticleIds" type='hasMany' model-name='CmsArticle'
      *                           items="myFakeArticles" sorted='true'
      *                           >
      *                       </a-db-relation-editor>
      *                   </div>
      *               </div>
      *           </file>
      *       </example>
       */
      restrict: 'E',
      require: 'ngModel',
      scope: {
        items: '=?',
        ngDisabled: '=?',
        ngRequired: '=?',
        getNewItem: '&?'
      },
      controller: 'ADbRelationEditorCtrl',
      template: function($element, $attrs) {
        var allowDuplicates, disableMoveActions, ref, relationType;
        relationType = getRelation($attrs);
        if (relationType === 'belongsTo') {
          return "<div ng-class=\"{'input-group-hack': (permissions.edit && proxy) || permissions.create}\">\n  <uic-db-model\n        ng-model=\"proxy\"\n        select=\"items\"\n        ng-disabled=\"ngDisabled\"\n        model-name=\"" + $attrs.modelName + "\"\n        ng-required=\"ngRequired\"\n    >\n  </uic-db-model>\n  <div class='input-group-btn' ng-if=\"(permissions.edit && proxy) || permissions.create\">\n      <button class=\"btn btn-success\" ng-click=\"editItem()\" ng-disabled=\"ngDisabled\" title=\"{{'Edit' | translate}}\" ng-if=\"permissions.edit && proxy\">\n        <span class=\"fas fa-pencil-alt nopadding\"></span>\n      </button>\n      <button class=\"btn btn-success\" ng-click=\"createItem()\" ng-disabled=\"ngDisabled\" title=\"{{'Create new' | translate}}\" ng-if=\"permissions.create\">\n        <span class=\"fas fa-plus\"></span><span translate=\"\">Create</span>\n      </button>\n  </div>\n</div>";
        }
        disableMoveActions = (ref = $attrs.sorted) !== 'true' && ref !== 'allow-duplicates';
        allowDuplicates = $attrs.sorted === 'allow-duplicates';
        return "<uice-db-model-list\n    ng-model=\"proxy\"\n    items=\"items\"\n    model-name=\"" + $attrs.modelName + "\"\n    on-create-click=\"createItem()\"\n    on-edit-click=\"editItem(item)\"\n    disable-move-actions=\"" + disableMoveActions + "\"\n    allow-duplicates=\"" + allowDuplicates + "\"\n    ng-disabled=\"ngDisabled\"\n    ng-disabled-create=\"!permissions.create\"\n    ng-disabled-edit=\"!permissions.edit\"\n>\n</uice-db-model-list>";
      }
    };
  });

}).call(this);
;
(function() {
  angular.module('AdminApp').controller('aFooterToolbarCtrl', ["$scope", "$element", "$attrs", "$currentUser", function($scope, $element, $attrs, $currentUser) {
    var PROPS, generatePermissions, i, len, prop, results;
    PROPS = ['Create', 'Update', 'Delete'];
    generatePermissions = function() {
      var i, len, prop;
      $scope.permissions = {
        create: true,
        update: true,
        "delete": true,
        save: true
      };
      if (!angular.isFunction($scope.onSave)) {
        $scope.permissions.create = false;
        $scope.permissions.update = false;
      }
      if (!angular.isFunction($scope.onDelete)) {
        $scope.permissions["delete"] = false;
      }
      for (i = 0, len = PROPS.length; i < len; i++) {
        prop = PROPS[i];
        if ($attrs["currentUserPermission" + prop] && $scope.permissions[prop.toLowerCase()]) {
          $scope.permissions[prop.toLowerCase()] = $currentUser.resolvePermissionsString($attrs["currentUserPermission" + prop]);
        }
      }
      $scope.permissions.save = $scope.permissions.update;
      if ($attrs.item && !($scope.item || {}).id) {
        $scope.permissions.save = $scope.permissions.create;
        $scope.permissions["delete"] = false;
      }
      if (!$scope.permissions["delete"] && !$scope.permissions.save) {
        $element.addClass('ng-hide');
      } else {
        $element.removeClass('ng-hide');
      }
    };
    if ($attrs.item) {
      $scope.$watch('item.id', generatePermissions);
      return;
    }
    results = [];
    for (i = 0, len = PROPS.length; i < len; i++) {
      prop = PROPS[i];
      if ($attrs["currentUserPermission" + prop]) {
        results.push($attrs.$observe("currentUserPermission" + prop, generatePermissions));
      }
    }
    return results;
  }]).directive('aFooterToolbar', function() {
    return {

      /**
      *   @ngdoc directive
      *   @name AdminApp.directive:aFooterToolbar
      *   @description
      *
      *   @restrict E
      *   @param {object} ngModel (ссылка) модель
      *   @param {boolean=} ngDisabled (ссылка) флаг доступности тега
      *   @example
      *       <pre>
      *           $scope.
      *       </pre>
      *       <pre>
      *           //-pug
      *       </pre>
      *       <example module="DocExampleApp">
      *           <file name="script.js">
      *               mkDocCtrl('aFooterToolbarCtrl', {
      *
      *               });
      *           </file>
      *           <file name="index.html">
      *               <div ng-controller="aFooterToolbarCtrl">
      *
      *               </div>
      *           </file>
      *       </example>
       */
      restrict: 'E',
      scope: {
        item: '=?',
        onSave: '&?',
        onDelete: '&?',
        saveDisabled: '=?',
        deleteDisabled: '=?'
      },
      controller: 'aFooterToolbarCtrl',
      template: ('/client/admin_app/_directives/aFooterToolbar/aFooterToolbar.html', '<div class="toolbar lg md"><div class="material-shadow" ng-class="{\'btn-group\': permissions.save &amp;&amp; permissions.delete}"><button class="btn btn-primary" ng-click="onSave()" ng-if="permissions.save" ng-disabled="saveDisabled"><span class="fas fa-save"></span><span translate="">Save</span></button><button class="btn btn-danger" ng-click="onDelete()" ng-if="permissions.delete" ng-disabled="deleteDisabled"><span class="fas fa-trash-alt"></span><span translate="">Delete</span></button></div></div><div class="toolbar sm xs" ng-switch="!permissions.delete &amp;&amp; permissions.save"><div class="dropup" uib-dropdown="" ng-switch-when="false"><button class="btn btn-warning btn-material-fab" uib-dropdown-toggle=""><span class="fas fa-list"></span></button><ul class="dropdown-menu dropdown-menu-right warning"><li ng-if="permissions.save"><a ng-click="onSave()" ng-disabled="saveDisabled" translate="">Save</a></li><li ng-if="permissions.delete"><a ng-click="onDelete()" ng-disabled="deleteDisabled" translate="">Delete</a></li></ul></div><button class="btn btn-warning btn-material-fab" ng-switch-when="true" ng-if="permissions.save" ng-click="onSave()" ng-disabled="saveDisabled" title="{{\'Save\' | translate}}"><span class="fas fa-save"></span></button></div>' + '')
    };
  });

}).call(this);
;
(function() {
  angular.module('AdminApp').directive('aIntegrationsEditor', ["$uicIntegrations", function($uicIntegrations) {
    return {
      restrict: 'E',
      scope: {
        ngModel: '=',
        ngDisabled: '=?'
      },
      controller: ["$scope", "aIntegrationsModal", function($scope, aIntegrationsModal) {
        var SERVICES;
        $scope.addIntegration = function() {
          aIntegrationsModal.add($scope.ngModel).then(function(data) {
            $scope.ngModel || ($scope.ngModel = {});
            $scope.ngModel[data.type] = data.data;
          });
        };
        $scope.editIntegration = function(serviceName) {
          return aIntegrationsModal.edit($scope.ngModel, serviceName).then(function(data) {
            return $scope.ngModel[data.type] = data.data;
          });
        };
        $scope.removeIntegration = function(serviceName) {
          delete $scope.ngModel[serviceName];
        };
        SERVICES = {
          googleSearchConsole: 'Google Search Console',
          googleAdSense: 'Google AdSense',
          googleAnalytics: 'Google Analytics',
          googleTagManager: 'Google Tag Manager',
          yandexWebmaster: 'Yandex Webmaster',
          yandexMetrica: 'Yandex Metrica',
          facebookPixel: 'Facebook Pixel',
          chatflow: 'Chatflow',
          jivosite: 'Jivosite',
          disqus: 'Disqus comments',
          custom: 'Custom integrations'
        };
        $scope.getServiceTitle = function(serviceName) {
          return SERVICES[serviceName];
        };
        $scope.getExternalLink = $uicIntegrations.getExternalLink;
        return $scope.$watch('ngModel', function(ngModel) {
          if (typeof ngModel !== 'object') {
            $scope.hasIntegrations = false;
            return;
          }
          return $scope.hasIntegrations = Object.keys(ngModel).length;
        }, true);
      }],
      template: ('/client/admin_app/_directives/aIntegrationsEditor/aIntegrationsEditor.html', '<div class="text-center" ng-if="!hasIntegrations" style="margin:1em auto;"><h5><span translate="">Here you can add support for</span><b style="padding-left:3px;">Google Search Console</b>,<b style="padding-left:3px;">Google Analytics</b>,<b style="padding-left:3px;">Google Tag Manager</b>,<b style="padding-left:3px;">Yandex Webmaster</b>,<b style="padding-left:3px;">Yandex Metrica</b>,<b style="padding-left:3px;">Facebook Pixel</b>,<b style="padding-left:3px;">Jivosite</b>.</h5><button class="btn btn-sm btn-success" ng-click="addIntegration()"><span translate="">Add third-party service</span></button></div><ul class="list-group" ng-if="hasIntegrations"><li class="list-group-item" ng-repeat="(serviceName, userData) in ngModel"><span ng-switch="!!(href = getExternalLink(serviceName, userData))"><a href="{{href}}" target="_blank" ng-switch-when="true"><b>{{getServiceTitle(serviceName)}}</b></a><b ng-switch-when="false">{{getServiceTitle(serviceName)}}</b></span><div class="btn-group pull-right"><button class="btn btn-xs btn-default" ng-click="editIntegration(serviceName)"><span class="fas fa-pencil-alt"></span><span translate="">Edit</span></button><button class="btn btn-xs btn-danger" ng-click="removeIntegration(serviceName)"><span class="fas fa-trash-alt nopadding"></span></button></div></li></ul>' + '')
    };
  }]);

}).call(this);
;
(function() {
  angular.module('AdminApp').directive('aKeywordsIdListEditor', function() {
    return {

      /**
      *   @ngdoc directive
      *   @name AdminApp.directive:aKeywordsIdListEditor
      *   @description
      *
      *   @restrict E
      *   @param {object} ngModel (ссылка) модель
      *   @param {boolean=} ngDisabled (ссылка) флаг доступности тега
      *   @example
      *       <pre>
      *           $scope.
      *       </pre>
      *       <pre>
      *           //-pug
      *       </pre>
      *       <example module="DocExampleApp">
      *           <file name="script.js">
      *               mkDocCtrl('aKeywordsIdListEditorCtrl', {
      *
      *               });
      *           </file>
      *           <file name="index.html">
      *               <div ng-controller="aKeywordsIdListEditorCtrl">
      *
      *               </div>
      *           </file>
      *       </example>
       */
      restrict: 'E',
      scope: {
        ngModel: '=',
        forceLang: '=?',
        labelPicked: '@?',
        labelAvailable: '@?',
        ngDisabled: '=?'
      },
      controller: ["$scope", "$currentUser", "$attrs", "CmsKeyword", "uiceItemEditor", function($scope, $currentUser, $attrs, CmsKeyword, uiceItemEditor) {
        var loadData;
        $scope.permissions = {
          create: true,
          edit: true
        };
        loadData = function() {
          return CmsKeyword.find({
            filter: {
              where: {
                isPublished: true
              }
            }
          }, function(data) {
            var allowed, id, item, j, k, len, len1, ngModel, ref;
            $scope.items = [];
            allowed = [];
            for (j = 0, len = data.length; j < len; j++) {
              item = data[j];
              if (item.isPublished) {
                $scope.items.push(item);
                allowed.push(item.id);
              }
            }
            if ($scope.ngModel) {
              ngModel = [];
              ref = $scope.ngModel || [];
              for (k = 0, len1 = ref.length; k < len1; k++) {
                id = ref[k];
                if (allowed.indexOf(id) > -1) {
                  ngModel.push(id);
                }
              }
              $scope.ngModel = ngModel;
            }
          });
        };
        $scope.toggleKeyword = function(id) {
          var i;
          $scope.ngModel = $scope.ngModel || [];
          i = $scope.ngModel.indexOf(id);
          if (i > -1) {
            $scope.ngModel.remove(i);
          } else {
            $scope.ngModel.push(id);
          }
        };
        $scope.editKeyword = function(item) {
          if ($scope.ngDisabled || !$scope.permissions.edit) {
            return;
          }
          uiceItemEditor.edit(item, {
            modelName: 'CmsKeyword'
          }).then(function(data) {
            var _item, i, j, len, ref;
            ref = $scope.items;
            for (i = j = 0, len = ref.length; j < len; i = ++j) {
              _item = ref[i];
              if (!(_item.id === data.id)) {
                continue;
              }
              $scope.items[i] = data;
              break;
            }
          });
        };
        $scope.createKeyword = function() {
          if ($scope.ngDisabled || !$scope.permissions.create) {
            return;
          }
          uiceItemEditor.edit({
            isPublished: true
          }, {
            modelName: 'CmsKeyword'
          }).then(function(data) {
            $scope.items.push(data);
            $scope.ngModel = $scope.ngModel || [];
            $scope.ngModel.push(data.id);
          });
        };
        $scope.isKeywordPicked = function(id) {
          $scope.ngModel = $scope.ngModel || [];
          return $scope.ngModel.includes(id);
        };
        loadData();
        if ($attrs.currentUserPermissionCreate) {
          $scope.permissions.create = $currentUser.resolvePermissionsString($attrs.currentUserPermissionCreate);
        }
        if ($attrs.currentUserPermissionEdit) {
          $scope.permissions.edit = $currentUser.resolvePermissionsString($attrs.currentUserPermissionEdit);
        }
      }],
      template: ('/client/admin_app/_directives/aKeywordsIdListEditor/aKeywordsIdListEditor.html', '<div ng-hide="!items || !ngModel.length"><label ng-switch="!!labelPicked"><span translate="" ng-switch-when="false">Picked keywords:</span><span ng-switch-when="true">{{labelPicked}}</span></label><div class="btn-group" ng-repeat="item in items" ng-if="isKeywordPicked(item.id)"><button class="btn btn-default active btn-xs" ng-click="toggleKeyword(item.id)" title="{{\'Remove\' | translate}}"><div class="fas fa-check"></div>{{item.title[forceLang] || (item.title | l10n)}}</button><button class="btn btn-info btn-xs" ng-click="editKeyword(item)"><span class="fas fa-pencil-alt nopadding"></span></button></div></div><div><label ng-switch="!!labelAvailable"><span translate="" ng-switch-when="false">Available keywords:</span><span ng-switch-when="true">{{labelAvailable}}</span></label><div class="btn-group" ng-repeat="item in items | filter: searchTerm" ng-if="!isKeywordPicked(item.id)"><button class="btn btn-default btn-xs" ng-click="toggleKeyword(item.id)">{{item.title[forceLang] || (item.title | l10n)}}</button><button class="btn btn-info btn-xs" ng-click="editKeyword(item)" title="{{\'Edit\' | translate}}"><span class="fas fa-pencil-alt nopadding"></span></button></div><div class="btn-group">  <button class="btn btn-success btn-xs" ng-click="createKeyword()"><span class="fa fa-plus"></span><span translate="">Create</span></button></div></div>' + '')
    };
  });

}).call(this);
;
(function() {
  angular.module('AdminApp').directive('aNavbar', function() {
    return {
      restrict: 'E',
      controller: ["$scope", "$cms", "$window", "$rootScope", "AdminAppConfig", "aSidebar", function($scope, $cms, $window, $rootScope, AdminAppConfig, aSidebar) {
        $scope.$cms = $cms;
        $scope.$rootScope = $rootScope;
        $scope.AdminAppConfig = AdminAppConfig;
        $scope.toggleSidebar = function() {
          if (aSidebar.visible) {
            aSidebar.hide();
          } else {
            aSidebar.show();
          }
        };
        return $scope.back = function() {
          return $window.history.back();
        };
      }],
      template: ('/client/admin_app/_directives/aNavbar/aNavbar.html', '<nav class="navbar navbar-inverse"><div class="container-fluid"><div class="navbar-header"><div class="navbar-brand btn-special visible-xxs-table" ng-click="back()" role="button"><span class="fas fa-arrow-left"></span></div><div class="navbar-brand visible-xs-inline-block" ng-click="toggleSidebar()" role="button"><span class="fas fa-bars"></span><span class="text" translate="">Menu</span></div><div class="navbar-brand visible-xs-inline-block pull-right" ng-click="$cms.toggleNavbar()" role="button"><span class="fas fa-ellipsis-v"></span></div><span class="navbar-brand hidden-xxs">{{$cms.seoService.pageTitle}}</span></div><div class="collapse navbar-collapse" uib-collapse="$cms.isNavbarCollapsed"><uic-navbar-nav ng-model="AdminAppConfig.navbarConfig"></uic-navbar-nav></div></div></nav>' + '')
    };
  });

}).call(this);
;
(function() {
  angular.module('AdminApp').directive('aObjectEditSidebar', function() {
    return {

      /**
      *   @ngdoc directive
      *   @name AdminApp.directive:aObjectEditSidebar
      *   @description
      *           директива с правым сайдбаром редактора свойств объекта типа главной картинки, идентификатора,
      *   @restrict E
      *   @param {object} ngModel (ссылка) модель
      *   @param {boolean=} ngDisabled (ссылка) флаг доступности тега
      *   @example
      *       <pre>
      *           $scope.
      *       </pre>
      *       <pre>
      *           //- pug
      *       </pre>
      *       <example module="DocExampleApp">
      *           <file name="script.js">
      *               mkDocCtrl('aObjectEditSidebarCtrl', {
      *
      *               });
      *           </file>
      *           <file name="index.html">
      *               <div ng-controller="aObjectEditSidebarCtrl">
      *
      *               </div>
      *           </file>
      *       </example>
       */
      restrict: 'E',
      transclude: {
        mainPanel: '?mainPanel'
      },
      scope: {
        ngModel: '=',
        ngDisabled: '=?'
      },
      controller: ["$scope", "$attrs", function($scope, $attrs) {
        $scope._hide = {};
        if ($attrs.hideIsPublished === 'true') {
          $scope._hide.isPublished = true;
        }
        if ($attrs.hideMainImg === 'true') {
          $scope._hide.mainImg = true;
        }
        if ($attrs.hideFiles === 'true') {
          $scope._hide.files = true;
        }
        if ($scope.$parent.save) {
          return $scope.save = $scope.$parent.save;
        }
      }],
      template: ('/client/admin_app/_directives/aObjectEditSidebar/aObjectEditSidebar.html', '<div class="panel panel-primary panel-sm"><div class="panel-heading"><div class="panel-title" translate="">Main</div></div><div class="panel-body"><div ng-if="!_hide.isPublished"><b translate="">Status</b><span>: </span><span class="checkbox nomargin-tb" style="display:inline-block;margin-left:5px;"><label><input type="checkbox" ng-model="ngModel.isPublished" ng-disabled="ngDisabled"/><span translate="">Published</span></label></span></div><div ng-if="!_hide.mainImg"><uice-main-img-manager ng-model="ngModel" v2="true" ng-disabled="ngDisabled"></uice-main-img-manager></div><div ng-transclude="mainPanel"></div></div></div><uice-file-manager ng-model="ngModel" field="files" type="panel" ng-disabled="ngDisabled" ng-if="!_hide.files"></uice-file-manager>' + '')
    };
  });

}).call(this);
;
(function() {
  angular.module('AdminApp').directive('aParentRelationEditor', function() {
    return {
      restrict: 'E',
      scope: {
        ngModel: '=',
        items: '=?',
        ngDisabled: '=?'
      },
      controller: ["$scope", function($scope) {
        var setChildren, setParents;
        $scope.children = {};
        $scope.parents = [];
        setChildren = function(items, id) {
          var item, j, len, ref;
          $scope.children = {};
          ref = items || [];
          for (j = 0, len = ref.length; j < len; j++) {
            item = ref[j];
            $scope.children[item.id] = item.parentId === id;
          }
        };
        setParents = function(items) {
          var item, j, len, ref;
          $scope.parents = [
            {
              id: null,
              title: {
                en: 'Without parent',
                ru: 'Без родительской категории'
              }
            }
          ];
          ref = items || [];
          for (j = 0, len = ref.length; j < len; j++) {
            item = ref[j];
            if (!$scope.children[item.id]) {
              $scope.parents.push({
                id: item.id,
                title: item.title
              });
            }
          }
        };
        $scope.$watch('items', function(items, oldValue) {
          if (!$scope.ngModel) {
            return;
          }
          return setChildren(items, $scope.ngModel.id);
        });
        $scope.$watch('ngModel.id', function(id, oldValue) {
          if (id) {
            return setChildren($scope.items, id);
          }
        });
        $scope.$watch('ngModel.parentId', function(parentId, oldValue) {
          if (parentId) {
            $scope.children[parentId] = false;
          }
        });
        return $scope.$watch('children', function(children, oldValue) {
          var i, item, j, len, ref;
          ref = $scope.items || [];
          for (i = j = 0, len = ref.length; j < len; i = ++j) {
            item = ref[i];
            if (children[item.id]) {
              if (item.id === $scope.ngModel.parentId) {
                $scope.children[item.id] = false;
                return;
              }
              $scope.items[i].parentId = $scope.ngModel.id;
            } else if (!children[item.id] && item.parentId === $scope.ngModel.id) {
              $scope.items[i].parentId = null;
            }
          }
          return setParents($scope.items);
        }, true);
      }],
      template: ('/client/admin_app/_directives/aParentRelationEditor/aParentRelationEditor.html', '<div class="form-group"><label><span translate="">Parent category</span><span class="text-danger" translate="">**</span></label><select class="form-control" ng-model="ngModel.parentId" ng-options="p.id as (p.title | l10n)  for p in parents"></select></div><div class="form-group" ng-show="ngModel.id"><label translate="">Child categories:</label><div class="checkbox nomargin-tb" ng-repeat="item in items"><label><input type="checkbox" ng-model="children[item.id]" ng-disabled="item.id==ngModel.parentId"/>{{item.title | l10n}}</label></div></div><hr style="margin:10px 0;"/><p class="nomargin"><small style="padding-right:3px;">** -</small><small translate="">Categories can have a hierarchy. You might have a "Music" category, and under that have children categories for "Pop" and "Rock". It\'s optional.</small></p>' + '')
    };
  });

}).call(this);
;
(function() {
  angular.module('AdminApp').directive('aSidebarAdmin', ["$state", "$cms", "$models", "AdminAppConfig", "aSidebar", function($state, $cms, $models, AdminAppConfig, aSidebar) {
    return {
      restrict: 'E',
      scope: {
        ngModel: '=',
        uiViewId: '@'
      },
      controller: ["$scope", "$currentUser", "$injector", function($scope, $currentUser, $injector) {
        $scope.$cms = $cms;
        $scope.$models = $models;
        $scope.$currentUser = $currentUser;
        $scope.AdminAppConfig = AdminAppConfig;
        $scope.aSidebar = aSidebar;
        $scope.isCurrentState = function(stateName) {
          return $state.current.name === stateName;
        };
        aSidebar.setUiViewElement(angular.element(document.getElementById($scope.uiViewId)));
      }],
      template: ('/client/admin_app/_directives/aSidebarAdmin/aSidebarAdmin.html', '<div class="{{aSidebar.backdropClass}}"></div><pageslide ps-open="aSidebar.visible" ps-click-outside="aSidebar.allowClickOutsideToClose" onclose="aSidebar.onClose" ps-side="left" ps-class="{{aSidebar.sidebarClass}}"><nav class="navbar navbar-inverse"><div class="container-fluid"><div class="navbar-header"><div class="navbar-brand btn-special visible-xxs-table" ng-click="aSidebar.hide()"><div class="fas fa-times"></div></div><div class="navbar-brand visible-xs-inline-block" ng-click="aSidebar.hide()"><div class="fas fa-bars"></div><span class="text">{{AdminAppConfig.sidebarTitle | l10n | translate}}</span></div><div class="navbar-brand hidden-xs"><ul class="list-table nomargin nopadding vertical-align-middle"><li><span class="fas fa-bars" ng-click="aSidebar.togglePin()"></span></li><li><span class="text">{{AdminAppConfig.sidebarTitle | l10n | translate}}</span></li><li><div class="btn-pin"><button class="btn btn-default" ng-click="aSidebar.togglePin()" ng-class="{\'active\':aSidebar.isPinned}"><div class="fas fa-thumbtack"></div></button></div></li></ul></div></div></div></nav><div class="list-group"><div ng-repeat="tab in AdminAppConfig.sidebarTemplates.templates.top"><h4 ng-if="tab.heading">{{tab.heading | l10n | translate}}</h4><div uic-bind-html="tab.template"></div></div><a class="list-group-item" ui-sref-active="active" ui-sref="app.dashboard" ng-click="aSidebar.hide(\'xs\')"><div class="fa-container"><div class="fas fa-tachometer-alt"></div></div><span class="text" translate="">Dashboard</span></a><div ng-if="AdminAppConfig.visiblePages.mainPage"><a class="list-group-item" ui-sref-active="active" ui-sref="app.mainPage" uic-if-on-current-user-permission="site_settings.change_CmsSettings" ng-click="aSidebar.hide(\'xs\')"><div class="fa-container"><div class="far fa-heart"></div></div><span class="text" translate="">Main Page</span></a></div><div ng-if="AdminAppConfig.visiblePages.siteMenu"><a class="list-group-item" ui-sref-active="active" ui-sref="app.siteMenu" uic-if-on-current-user-permission="site_settings.change_CmsSettings" ng-click="aSidebar.hide(\'xs\')"><div class="fa-container"><div class="fas fa-bars"></div></div><span class="text" translate="">Site Menu</span></a></div><div class="divider"></div><div ng-repeat="tab in AdminAppConfig.sidebarTemplates.templates.middle"><h4 ng-if="tab.heading">{{tab.heading | l10n | translate}}</h4><div uic-bind-html="tab.template"></div></div><div ng-if="AdminAppConfig.visibleModels.CmsArticle"><div class="subitems" uic-if-on-current-user-permission="lbcms.view_CmsArticle || *.view_CmsArticle"><a class="list-group-item" ui-sref-active="active" ui-sref="app.modelEditors.CmsArticle.list({category:null})"><div class="fa-container"><div class="fas fa-font"></div></div><span class="text" translate="">Articles</span></a><div class="list-group" uib-collapse="!(showArticleSubitems = ($models.CmsCategoryForArticle.items.length &amp;&amp; AdminAppConfig.visibleModels.CmsCategoryForArticle &amp;&amp; isCurrentState(\'app.modelEditors.CmsArticle.list\')))"><a class="list-group-item" ui-sref-active="active" ng-repeat="item in $models.CmsCategoryForArticle.items" ui-sref="app.modelEditors.CmsArticle.list({category: item.id})" ng-click="aSidebar.hide(\'xs\')"><span class="text">{{item.title | l10n | cut:false:28}}</span></a></div></div></div><div ng-if="AdminAppConfig.visibleModels.CmsPictureAlbum"><a class="list-group-item" ui-sref-active="active" ui-sref="app.modelEditors.CmsPictureAlbum.list" uic-if-on-current-user-permission="lbcms.view_CmsPictureAlbum || *.view_CmsPictureAlbum" ng-click="aSidebar.hide(\'xs\')"><div class="fa-container"><div class="fas fa-images"></div></div><span class="text" translate="">Picture Gallery</span></a></div><div ng-if="AdminAppConfig.visibleModels.CmsSiteReview"><a class="list-group-item" ui-sref-active="active" ui-sref="app.modelEditors.CmsSiteReview.list" uic-if-on-current-user-permission="lbcms.view_CmsSiteReview || *.view_CmsSiteReview" ng-click="aSidebar.hide(\'xs\')"><div class="fa-container"><div class="fas fa-comments"></div></div><span class="text" translate="">Site Reviews</span></a></div><div ng-if="AdminAppConfig.visibleModels.CmsUser"><a class="list-group-item" ui-sref-active="active" ui-sref="app.modelEditors.CmsUser.list" uic-if-on-current-user-permission="*.view_CmsUser || *.view_CmsUser" ng-click="aSidebar.hide(\'xs\')"><div class="fa-container"><div class="fas fa-users"></div></div><span class="text" translate="">Users</span></a></div><div ng-if="AdminAppConfig.glossaryCards.length"><a class="list-group-item" ui-sref-active="active" ui-sref="app.glossary" uic-if-on-current-user-permission="site_settings.view_glossary" ng-click="aSidebar.hide(\'xs\')"><div class="fa-container"><div class="fab fa-leanpub"></div></div><span class="text" translate="">Glossary</span></a></div><div class="divider" ng-show="AdminAppConfig.visibleModels.CmsMailNotification || AdminAppConfig.visibleModels.CmsSmsNotification || AdminAppConfig.visibleModels.CmsPdfData"></div><div ng-if="AdminAppConfig.visibleModels.CmsMailNotification"><a class="list-group-item" ui-sref-active="active" ui-sref="app.modelEditors.CmsMailNotification.list" uic-if-on-current-user-permission="drf_lbcms.view_CmsMailNotification" ng-click="aSidebar.hide(\'xs\')"><div class="fa-container"><span class="fas fa-bell"></span></div><span class="text" translate="translate">Mail Notifications</span></a></div><div ng-if="AdminAppConfig.visibleModels.CmsSmsNotification"><a class="list-group-item" ui-sref-active="active" ui-sref="app.modelEditors.CmsSmsNotification.list" uic-if-on-current-user-permission="drf_lbcms.view_CmsSmsNotification" ng-click="aSidebar.hide(\'xs\')"><div class="fa-container"><span class="fas fa-mobile-alt"></span></div><span class="text" translate="translate">Sms Notifications</span></a></div><div ng-if="AdminAppConfig.visibleModels.CmsPdfData"><a class="list-group-item" ui-sref-active="active" ui-sref="app.modelEditors.CmsPdfData.list" uic-if-on-current-user-permission="drf_lbcms.view_CmsPdfData" ng-click="aSidebar.hide(\'xs\')"><div class="fa-container"><div class="fas fa-file-pdf"></div></div><span class="text" translate="">Templates for Pdf generator</span></a></div><div ng-if="AdminAppConfig.visibleModels.CmsTranslations"><a class="list-group-item" ui-sref-active="active" ui-sref="app.modelEditors.CmsTranslations.list" uic-if-on-current-user-permission="drf_lbcms.view_CmsTranslations" ng-click="aSidebar.hide(\'xs\')"><div class="fa-container"><div class="fas fa-language"></div></div><span class="text" translate="">Static content translations</span></a></div><div ng-if="AdminAppConfig.visibleModels.CmsGdprRequest"><a class="list-group-item" ui-sref-active="active" ui-sref="app.modelEditors.CmsGdprRequest.list" uic-if-on-current-user-permission="drf_lbcms.view_CmsGdprRequest" ng-click="aSidebar.hide(\'xs\')"><div class="fa-container"><span class="fas fa-unlock-alt"></span></div><span class="text" translate="translate">GDPR Requests</span></a></div><div ng-repeat="tab in AdminAppConfig.sidebarTemplates.templates.bottom"><h4 ng-if="tab.heading">{{tab.heading | l10n | translate}}</h4><div uic-bind-html="tab.template"></div></div></div></pageslide>' + '')
    };
  }]);

}).call(this);
;
(function() {
  var indexOf = [].indexOf || function(item) { for (var i = 0, l = this.length; i < l; i++) { if (i in this && this[i] === item) return i; } return -1; };

  angular.module('AdminApp').service('aSidebar', ["$rootScope", "$cms", function($rootScope, $cms) {
    var _body, bodyOverflowYInitial, setOverflowYForHtmlBody;
    bodyOverflowYInitial = document.getElementsByTagName('body')[0].style.overflowY;
    _body = null;
    this.visible = true;
    this.isPinned = true;
    this.allowClickOutsideToClose = false;
    this.backdropClass = '';
    setOverflowYForHtmlBody = function(val) {
      document.getElementsByTagName('body')[0].style.overflowY = val;
    };
    this.setUiViewElement = function(element) {
      _body = element;
      _body.addClass('open-sidebar');
    };
    this.hide = (function(_this) {
      return function() {
        var ref;
        if (arguments.length) {
          if (ref = $cms.screenSize, indexOf.call(arguments, ref) < 0) {
            return;
          }
        }
        _this.visible = false;
        _this.backdropClass = '';
        setOverflowYForHtmlBody(bodyOverflowYInitial);
        if (_body) {
          _body.removeClass('open-sidebar');
        }
      };
    })(this);
    this.show = (function(_this) {
      return function() {
        _this.visible = true;
        if ($cms.screenSize === "xs") {
          _this.backdropClass = "a-sidebar-backdrop";
          setOverflowYForHtmlBody('hidden');
        } else {
          _this.backdropClass = '';
          setOverflowYForHtmlBody(bodyOverflowYInitial);
        }
      };
    })(this);
    this.togglePin = (function(_this) {
      return function() {
        _this.isPinned = !_this.isPinned;
        if (_this.isPinned) {
          _this.sidebarClass = 'a-sidebar-admin open';
          _body.addClass('open-sidebar');
        } else {
          _this.sidebarClass = 'a-sidebar-admin';
          _body.removeClass('open-sidebar');
        }
      };
    })(this);
    this.onClose = (function(_this) {
      return function() {
        if (!_this.visible && !_this.backdropClass) {
          return;
        }
        $rootScope.$apply(function() {
          _this.hide();
        });
      };
    })(this);
    if (!$rootScope.hasOwnProperty('$cms')) {
      $rootScope.$cms = $cms;
    }
    $rootScope.$watch('$cms.screenSize', (function(_this) {
      return function(screenSize) {
        setOverflowYForHtmlBody(bodyOverflowYInitial);
        if (screenSize === 'xs') {
          _this.isPinned = false;
          _this.allowClickOutsideToClose = true;
          _this.sidebarClass = 'a-sidebar-admin open';
          _this.visible = false;
          return;
        }
        _this.allowClickOutsideToClose = false;
        _this.backdropClass = '';
        _this.sidebarClass = 'a-sidebar-admin open';
        _this.isPinned = true;
        _this.visible = true;
        if (_body) {
          _body.addClass('open-sidebar');
        }
      };
    })(this));
    return this;
  }]);

}).call(this);
;
(function() {
  angular.module('AdminApp').controller('ATableToolbarCtrl', ["$scope", "$attrs", "$state", "$cms", "$currentUser", "AdminAppConfig", function($scope, $attrs, $state, $cms, $currentUser, AdminAppConfig) {
    $scope.$cms = $cms;
    $scope.$currentUser = $currentUser;
    $scope.AdminAppConfig = AdminAppConfig;
    $scope.options = $scope.options || {};
    $scope.permissions = {
      create: true
    };
    if (CMS_ALLOWED_LANGS === CMS_DEFAULT_LANG || (CMS_ALLOWED_LANGS.length === 1 && CMS_ALLOWED_LANGS[0] === CMS_DEFAULT_LANG) || !CMS_ALLOWED_LANGS.length) {
      $scope.options.hideLangPicker = true;
    }
    if ($attrs.currentUserPermissionCreate) {
      $attrs.$observe('currentUserPermissionCreate', function() {
        $scope.permissions.create = $currentUser.resolvePermissionsString($attrs.currentUserPermissionCreate);
      });
    }
    $scope.resetSearchTerm = function() {
      $scope.searchTerm = '';
      $scope.proxySearchTerm = '';
    };
    $scope.onAddNewClick = function() {
      if ($scope.options.addStateName) {
        return $state.go($scope.options.addStateName);
      }
      if ($state.get("app.modelEditors." + $scope.modelName + ".add")) {
        return $state.go("app.modelEditors." + $scope.modelName + ".add");
      }
    };
    return this;
  }]).directive('aTableToolbar', function() {
    return {
      restrict: 'E',
      transclude: true,
      scope: {
        forceLang: '=?',
        searchTerm: '=',
        modelName: '@?',
        options: '=?',
        ngDisabled: '=?'
      },
      controller: ["$scope", "$controller", "$attrs", function($scope, $controller, $attrs) {
        if (($scope.options || {}).submitSearchBtn) {
          console.error("aTableToolbar: options.submitSearchBtn is not allowed anymore!");
        }
        return $controller('ATableToolbarCtrl', {
          $scope: $scope,
          $attrs: $attrs
        });
      }],
      template: ('/client/admin_app/_directives/aTableToolbar/aTableToolbar.html', '<div class="clearfix"><ul class="list-table w-auto vertical-align-middle xxs-block"><li class="form-group nomargin" ng-hide="options.hideSearch" ng-class="{\'form-inline\': $cms.screenSize != \'xs\'}"><uic-search-input ng-model="searchTerm"></uic-search-input></li><li class="hidden-xs a-table-toolbar-btn-lg-create" ng-if="modelName &amp;&amp; permissions.create"><button class="btn btn-primary" ng-click="onAddNewClick()"><span class="fas fa-plus"></span><span translate="translate">Add new</span></button></li><li class="hidden-xs" ng-hide="options.hideLangPicker || $cms.settings.cmsLangCodes.length &lt; 2" translate="">View as</li><li class="hidden-xs" ng-hide="options.hideLangPicker || $cms.settings.cmsLangCodes.length &lt; 2"><div class="has-feedback left"><uic-lang-picker ng-model="forceLang"></uic-lang-picker><div class="form-control-feedback fas fa-language"></div></div></li><li><ng-transclude></ng-transclude></li></ul></div><div class="a-footer-toolbar" ng-if="modelName &amp;&amp; permissions.create"><div class="toolbar xs sm"><button class="btn btn-warning btn-material-fab" ng-click="onAddNewClick()"><div class="fas fa-plus"></div></button></div></div>' + '')
    };
  });

}).call(this);
;
(function() {
  angular.module('AdminApp').directive('aUserNotifySettings', function() {
    return {
      restrict: 'E',
      scope: {
        ngModel: '='
      },
      controller: ["$scope", "$attrs", "$constants", "CmsMailNotification", "CmsSmsNotification", function($scope, $attrs, $constants, CmsMailNotification, CmsSmsNotification) {
        var initSettings;
        $scope.USER_ROLES = $constants.resolve('CMS_USER.userRole');
        initSettings = function(userRole, identifier) {
          $scope.ngModel = $scope.ngModel || {};
          $scope.ngModel[userRole] = $scope.ngModel[userRole] || {};
          $scope.ngModel[userRole].enabled = $scope.ngModel[userRole].enabled || [];
          $scope.ngModel[userRole].canDisable = $scope.ngModel[userRole].canDisable || {};
        };
        $scope.removeNotify = function(userRole, identifier) {
          var i;
          initSettings(userRole, identifier);
          i = $scope.ngModel[userRole].enabled.indexOf(identifier);
          if (i > -1) {
            $scope.ngModel[userRole].enabled.splice(i, 1);
          }
        };
        $scope.addNotify = function(userRole, identifier) {
          var i;
          initSettings(userRole, identifier);
          i = $scope.ngModel[userRole].enabled.indexOf(identifier);
          if (i === -1) {
            $scope.ngModel[userRole].enabled.push(identifier);
          }
        };
        if ($attrs.type === 'sms') {
          CmsSmsNotification.find({
            filter: {
              fields: {
                body: false
              }
            }
          }, function(items) {
            return $scope.notifications = items;
          });
        } else {
          CmsMailNotification.find({
            filter: {
              fields: {
                body: false
              }
            }
          }, function(items) {
            return $scope.notifications = items;
          });
        }
      }],
      template: ('/client/admin_app/_directives/aUserNotifySettings/aUserNotifySettings.html', '<uib-tabset active="activeTab"><uib-tab ng-repeat="userRole in USER_ROLES" index="$index" heading="{{userRole.description | translate}}"><h6 class="text-primary nomargin-top" translate="">Enabled notifications</h6><div class="form-group"><div class="input-group"><select class="form-control" ng-model="newIdentifier" ng-options="o.identifier as (o.identifier + \' \' + (o.description|l10n|cut:true:50) ) for o in notifications"></select><div class="input-group-btn"><button class="btn btn-success" ng-disabled="!newIdentifier" ng-click="addNotify(userRole.value, newIdentifier); newIdentifier=\'\'" translate="" translate-context="append">Add</button></div></div></div><div ng-show="ngModel[userRole.value].enabled.length"><hr/><table class="table"><thead><tr><th translate="">Notification name</th><th class="td-can-disable text-center" translate="">Can user disable it?</th><th class="td-actions" translate="">Actions</th></tr></thead><thead><tr ng-repeat="notify in notifications | orderBy: \'identifier\'" ng-if="ngModel[userRole.value].enabled.indexOf(notify.identifier)&gt;-1"><td><b>{{notify.identifier}}</b> {{notify.description | l10n}}</td><td class="td-can-disable text-center"><uic-checkbox ng-model="ngModel[userRole.value].canDisable[notify.identifier]"></uic-checkbox></td><td class="td-actions"><button class="btn btn-danger btn-sm" ng-click="removeNotify(userRole.value, notify.identifier)" translate="">Remove</button></td></tr></thead></table></div></uib-tab></uib-tabset>' + '')
    };
  });

}).call(this);
;
(function() {
  angular.module('AdminApp').directive('aValidateNotificationTemplate', ["$uibModal", function($uibModal) {
    return {

      /**
      *   @ngdoc directive
      *   @name AdminApp.directive:aValidateNotificationTemplate
      *   @description валидация текстовых шаблонов экземпляров CmsSmsNotification, CmsMailNotification
      *
      *   @restrict E
      *   @param {object} ngModel (ссылка) потомок CmsSmsNotification или CmsMailNotification
      *   @param {string} fieldName (значение) название поля, для которого проводится валидация (напр 'body', 'echoSubject')
      *   @param {string} cmsModelName (значение) название модели. 'CmsSmsNotification' или 'CmsMailNotification'
      *   @example
      *       <pre>
      *           $scope.
      *       </pre>
      *       <pre>
      *           //- pug
      *       </pre>
      *       <example module="DocExampleApp">
      *           <file name="script.js">
      *               mkDocCtrl('aValidateNotificationTemplateCtrl', {
      *
      *               });
      *           </file>
      *           <file name="index.html">
      *               <div ng-controller="aValidateNotificationTemplateCtrl">
      *
      *               </div>
      *           </file>
      *       </example>
       */
      restrict: 'E',
      scope: {
        ngModel: '=',
        fieldName: '@',
        cmsModelName: '@'
      },
      controller: ["$scope", "$timeout", "$injector", "$cms", function($scope, $timeout, $injector, $cms) {
        var R, canRestore, defaultData, dpromise, generateCanRestore, initialData, isEmpty, validate;
        $scope.$cms = $cms;
        $scope.options = {};
        initialData = null;
        defaultData = null;
        R = $injector.get($scope.cmsModelName);
        if (!R.validate) {
          $scope.valid = true;
          return;
        }
        isEmpty = function(obj) {
          var k;
          for (k in obj || {}) {
            if (obj[k]) {
              return false;
            }
          }
          return true;
        };
        canRestore = function(data, langCode) {
          data = data || {};
          data = data[$scope.fieldName] || {};
          return !!data[langCode];
        };
        generateCanRestore = function() {
          var k, ref, v;
          $scope.options.canRestoreDefault = {};
          $scope.options.canRestoreInitial = {};
          ref = $scope.ngModel[$scope.fieldName] || {};
          for (k in ref) {
            v = ref[k];
            $scope.options.canRestoreDefault[k] = canRestore(defaultData, k);
            $scope.options.canRestoreInitial[k] = canRestore(initialData, k);
          }
        };
        validate = function() {
          var ngModel, vData;
          ngModel = $scope.ngModel || {};
          vData = {};
          vData[$scope.fieldName] = ngModel[$scope.fieldName] || {};
          R.validate(vData, angular.noop, function(data) {
            var k, ref, v;
            $scope.valid = false;
            $scope.errors = data.data[$scope.fieldName];
            ref = $scope.errors;
            for (k in ref) {
              v = ref[k];
              $scope.errors[k] = v.map(function(e) {
                var i, j, l, len, len1, line, p, ref1, source_lines;
                if (e.type !== 'parsing') {
                  return e;
                }
                ref1 = ['total', 'line', 'end', 'bottom', 'top', 'start'];
                for (j = 0, len = ref1.length; j < len; j++) {
                  p = ref1[j];
                  e.description[p] = parseInt(e.description[p]);
                }
                source_lines = e.description.source_lines;
                e.description.source_lines = {};
                for (i = l = 0, len1 = source_lines.length; l < len1; i = ++l) {
                  line = source_lines[i];
                  e.description.source_lines.length = parseInt(line[0]);
                  e.description.source_lines[line[0]] = "<span ng-non-bindable>\n    " + line[1] + "\n</span";
                }
                return e;
              });
              $scope.errors[k] = v[0];
            }
            if (!initialData && $scope.ngModel.id) {
              R.getInitial({
                id: $scope.ngModel.id
              }, function(data) {
                initialData = data;
                return generateCanRestore();
              });
            } else {
              generateCanRestore();
            }
          });
        };
        dpromise = null;
        $scope.$watch("ngModel." + $scope.fieldName, function() {
          var ngModel;
          ngModel = $scope.ngModel || {};
          if (!defaultData) {
            defaultData = angular.copy(ngModel);
          }
          $scope.valid = true;
          $scope.errors = [];
          if (isEmpty(ngModel[$scope.fieldName])) {
            return;
          }
          if (dpromise) {
            $timeout.cancel(dpromise);
          }
          dpromise = $timeout(validate, 1000);
        }, true);
        return $scope.restore = function(langCode, type) {
          var data;
          if (type === 'initial') {
            data = initialData || {};
          } else {
            data = defaultData || {};
          }
          $scope.ngModel[$scope.fieldName][langCode] = (data[$scope.fieldName] || {})[langCode] || '';
        };
      }],
      template: ('/client/admin_app/_directives/aValidateNotificationTemplate/aValidateNotificationTemplate.html', '<div class="alert alert-danger" ng-class="{visible: !valid}"><b translate="">Errors in template:</b><ul><li ng-repeat="(langCode, langErrors) in errors"><div><span>{{$cms.LANG_MAP[langCode]}}</span><span> </span><div ng-class="{\'btn-group\': options.canRestoreDefault[langCode] &amp;&amp; options.canRestoreInitial[langCode]}"><button class="btn btn-warning btn-xs" ng-click="restore(langCode)" ng-show="options.canRestoreDefault[langCode]"><span class="fas fa-redo"></span><span translate="">restore</span></button><button class="btn btn-default btn-xs" ng-click="restore(langCode, \'initial\')" ng-show="options.canRestoreInitial[langCode]"><span class="fas fa-redo"></span><span translate="">restore initial data</span></button></div></div><div class="well" ng-switch="errors[langCode].type"><b class="text-danger" translate="">Template engine output:</b><span> {{errors[langCode].description.message}}</span><div ng-switch-when="general"></div><div ng-switch-when="parsing"><table class="w-100"><tbody><tr ng-if="errors[langCode].description.line != 1"><td class="line-number">{{errors[langCode].description.line-1}}</td><td class="line-content" uic-bind-html="errors[langCode].description.source_lines[ errors[langCode].description.line-1 ]"></td></tr><tr><td class="line-number">{{errors[langCode].description.line}}</td><td class="line-content current" uic-bind-html="errors[langCode].description.source_lines[ errors[langCode].description.line ]"></td></tr><tr ng-if="errors[langCode].description.line + 1 &lt;=  errors[langCode].description.source_lines.length"><td class="line-number">{{errors[langCode].description.line+1}}</td><td class="line-content" uic-bind-html="errors[langCode].description.source_lines[ errors[langCode].description.line+1 ]"></td></tr></tbody></table></div></div></li></ul></div>' + '')
    };
  }]);

}).call(this);
;
(function() {
  angular.module('AdminApp').directive('aVariablesDescriptionEditor', function() {
    return {

      /**
      *   @ngdoc directive
      *   @name AdminApp.directive:aVariablesDescriptionEditor
      *   @description  директива, которая предназначена для редактирования описаний переменных на основе VariableDescription модели
      *
      *   @restrict E
      *   @param {Array} ngModel (ссылка) массив VariableDescription объектов
      *   @param {boolean=} ngDisabled (ссылка) флаг доступности тега
      *   @example
      *       <pre>
      *           $scope.
      *       </pre>
      *       <pre>
      *           //- pug
      *       </pre>
      *       <example module="DocExampleApp">
      *           <file name="script.js">
      *               mkDocCtrl('aVariablesDescriptionEditorCtrl', {
      *
      *               });
      *           </file>
      *           <file name="index.html">
      *               <div ng-controller="aVariablesDescriptionEditorCtrl">
      *
      *               </div>
      *           </file>
      *       </example>
       */
      restrict: 'E',
      scope: {
        ngModel: '=',
        lang: '=?',
        viewOnly: '@?',
        ngDisabled: '=?'
      },
      controller: ["$scope", "$element", function($scope, $element) {
        var ref;
        $scope._viewOnly = (ref = $scope.viewOnly) === 'true' || ref === true;
        $scope.add = function() {
          $scope.ngModel || ($scope.ngModel = []);
          $scope.ngModel.push({
            name: 'New variable',
            type: 'string',
            description: {}
          });
        };
        return $scope.remove = function(index) {
          $scope.ngModel.remove(index);
        };
      }],
      template: ('/client/admin_app/_directives/aVariablesDescriptionEditor/aVariablesDescriptionEditor.html', '<div ng-switch="_viewOnly"><div ng-switch-when="false"><table class="table table-striped hidden-xs"><thead><tr><th translate="">Variable name</th><th translate="">Variable type</th><th title="{{\'This field is multilingual\' | translate}}"><span translate="">Description</span><span> </span><span class="fas fa-language"></span></th></tr></thead><tbody><tr ng-repeat="v in ngModel"><td class="form-group"><div class="input-group"><input class="form-control input-sm" ng-model="ngModel[$index].name" required="required"/><div class="input-group-btn"><button class="btn btn-sm btn-danger" ng-click="remove($index)" title="{{\'Delete\' | translate}}"><div class="fas fa-trash-alt"></div></button></div></div></td><td class="form-group"><uic-constant ng-model="ngModel[$index].type" select="VARIABLE_DESCRIPTION.type" input-class="input-sm"></uic-constant></td><td class="form-group"><uice-l10n-input ng-model="ngModel[$index].description" lang="lang" input-class="input-sm form-control"></uice-l10n-input></td></tr><tr><td></td><td></td><td><button class="btn btn-sm btn-block btn-primary" ng-click="add()"><span class="fas fa-plus"></span><span translate="">Add variable description</span></button></td></tr></tbody></table><div class="visible-xs-block"><table class="table table-striped"><thead><tr><th translate="">Variable</th></tr></thead><tbody><tr ng-repeat="v in ngModel"><td><div class="form-group"><label translate="">Variable name</label><div class="input-group"><input class="form-control input-sm" ng-model="ngModel[$index].name" required="required"/><div class="input-group-btn"><button class="btn btn-sm btn-danger" ng-click="remove($index)" title="{{\'Delete\' | translate}}"><div class="fas fa-trash"></div></button></div></div></div><div class="form-group"><label translate="">Variable type</label><uic-constant ng-model="ngModel[$index].type" select="VARIABLE_DESCRIPTION.type" input-class="input-sm"></uic-constant></div><div class="form-group"><uice-l10n-input ng-model="ngModel[$index].description" label="Description" lang="lang" input-class="input-sm form-control"></uice-l10n-input></div></td></tr><tr><td><button class="btn btn-sm btn-block btn-primary" ng-click="add()"><span class="fas fa-plus"></span><span translate="">Add variable description</span></button></td></tr></tbody></table></div></div><div ng-switch-when="true"><table class="table table-striped"><thead><tr><th translate="">Variable name</th><th class="hidden-xs" translate="">Variable type</th><th class="hidden-xs" translate="">Variable description</th></tr></thead><tbody><tr ng-repeat="v in ngModel"><td> <b class="text-success">{{v.name}}</b><div class="visible-xs-block"><span>{{v.type | constantToText: \'VARIABLE_DESCRIPTION.type\'}}</span><span ng-if="v.description | l10n">: {{v.description | l10n}}</span></div></td><td class="hidden-xs">{{v.type | constantToText: \'VARIABLE_DESCRIPTION.type\'}}</td><td class="hidden-xs">{{v.description | l10n}}</td></tr></tbody></table></div></div>' + '')
    };
  });

}).call(this);
;
(function() {
  angular.module('AdminApp').directive('aViewTitle', ["$cms", function($cms) {
    return {
      restrict: 'E',
      controller: ["$scope", function($scope) {
        $scope.$cms = $cms;
      }],
      template: "<div class='visible-xxs-block text-center'>\n    <h4>{{$cms.seoService.pageTitle}}</h4>\n</div>"
    };
  }]);

}).call(this);
;
(function() {
  angular.module('AdminApp').directive('adminApp', ["$cms", "$state", "$models", "$bulk", "$timeout", "AdminAppConfig", function($cms, $state, $models, $bulk, $timeout, AdminAppConfig) {
    return {

      /**
      *   @ngdoc directive
      *   @name AdminApp.directive:adminApp
      *   @description
      *
      *   @restrict E
      *   @param {object} ngModel (ссылка) модель
      *   @param {boolean=} ngDisabled (ссылка) флаг доступности тега
      *   @example
      *       <pre>
      *           $scope.
      *       </pre>
      *       <pre>
      *           //- pug
      *       </pre>
      *       <pre>
      *       </pre>
       */
      restrict: 'E',
      controller: ["$scope", "$element", "$currentUser", "$state", "$rootScope", "aSidebar", "LoopBackAuth", function($scope, $element, $currentUser, $state, $rootScope, aSidebar, LoopBackAuth) {
        $scope.$bulk = $bulk;
        $scope.$state = $state;
        $scope.$models = $models;
        $scope.$cms = $cms;
        $scope.$currentUser = $currentUser;
        $scope.aSidebar = aSidebar;
        $scope.onLogout = function() {
          $state.go('app.login', {
            returnTo: $state.current.name,
            returnToParams: $state.params
          }, {
            reload: true,
            notify: true
          });
        };
        return $bulk.onReady(function() {
          if (!$currentUser.resolvePermissionsString('site_settings.view_adminpage')) {
            LoopBackAuth.clearUser();
            $state.go('app.login', {
              reload: true,
              notify: true
            });
          }
          $timeout(function() {
            return $rootScope.ready = true;
          }, 10);
          $rootScope.$on('$stateChangeStart', function(event, toState, toStateParams) {
            if ($currentUser.id) {
              return;
            }
            if (!$currentUser.id && toState.name !== 'app.login') {
              event.preventDefault();

              /*
              $state.go('app.login', {
                  returnTo: toState.name
                  returnToParams: toStateParams
              })
               */
            }
          });
        });
      }],
      template: ('/client/admin_app/_directives/adminApp/adminApp.html', '<div id="AdminUiView" ng-class="{\'a-sidebar-admin-body\': $state.current.name != \'app.login\'}" ng-if="ready &amp;&amp; $bulk.ready"><a-navbar ng-if="$state.current.name != \'app.login\'"></a-navbar><div class="content-block"><a-sidebar-admin ng-if="$state.current.name != \'app.login\'" ui-view-id="AdminUiView"></a-sidebar-admin><ui-view-animation-container class="ui-view-animation-container" ui-view=""></ui-view-animation-container><gdpr-request-permission-banner class="bg-warning"></gdpr-request-permission-banner></div></div>' + '')
    };
  }]);

}).call(this);
;
(function() {
  var indexOf = [].indexOf || function(item) { for (var i = 0, l = this.length; i < l; i++) { if (i in this && this[i] === item) return i; } return -1; };

  angular.module('AdminApp').config(["$provide", function($provide) {
    $provide.decorator('maxlengthDirective', ["$delegate", function($delegate) {
      $delegate.shift();
      return $delegate;
    }]);
  }]).directive('maxlength', function() {
    var BANNED_NODES;
    BANNED_NODES = ['uice-l10n-input', 'uice-string-list-editor'];
    return {
      restrict: 'A',
      require: '?ngModel',
      link: function(scope, elm, attr, ctrl) {
        var maxlength, ref;
        if (!ctrl) {
          return;
        }
        if (elm[0] && (ref = elm[0].nodeName.toLowerCase(), indexOf.call(BANNED_NODES, ref) >= 0)) {
          return;
        }
        maxlength = -1;
        attr.$observe('maxlength', function(value) {
          var intVal;
          intVal = parseInt(value, 10);
          if (isNaN(intVal)) {
            maxlength = -1;
          } else {
            maxlength = intVal;
          }
          return ctrl.$validate();
        });
        return ctrl.$validators.maxlength = function(modelValue, viewValue) {
          return (maxlength < 0) || ctrl.$isEmpty(viewValue) || (viewValue.length <= maxlength);
        };
      }
    };
  });

}).call(this);
;
(function() {
  angular.module('AdminApp').service('aIntegrationsModal', ["$uibModal", "$uicIntegrations", function($uibModal, $uicIntegrations) {
    var modalController, open;
    modalController = function($scope, $uibModalInstance, ngModel, options) {
      var i, k, len, ref;
      $scope.ngModel = ngModel;
      $scope.options = options;
      $scope.origin = location.origin;
      if (options.integration) {
        $scope.integration = options.integration;
      } else {
        $scope.integration = {
          type: '',
          data: ''
        };
        ref = $uicIntegrations.ALLOWED_SERVICES;
        for (i = 0, len = ref.length; i < len; i++) {
          k = ref[i];
          if (!ngModel.hasOwnProperty(k)) {
            $scope.integration.type = k;
            break;
          }
        }
      }
      $scope.integrationLink = function(t) {
        return $uicIntegrations.getInitLink(t);
      };
      $scope.cancel = function() {
        return $uibModalInstance.dismiss('cancel');
      };
      return $scope.ok = function() {
        return $uibModalInstance.close($scope.integration);
      };
    };
    open = function(ngModel, options) {
      var result;
      result = $uibModal.open({
        animation: true,
        template: ('/client/admin_app/_modals/aIntegrations/modal.html', '<div class="modal-body" ng-show="integration.type"><div class="form-group" ng-hide="options.edit"><label translate="translate">Third-party service</label><div style="margin-left:1em;"><div class="radio"><label><input type="radio" ng-model="integration.type" value="googleSearchConsole" ng-disabled="ngModel.googleSearchConsole"/><span translate="">Google Search Console</span></label></div><div class="radio"><label><input type="radio" ng-model="integration.type" value="googleAnalytics" ng-disabled="ngModel.googleAnalytics"/><span translate="">Google Analytics</span></label></div><div class="radio"><label><input type="radio" ng-model="integration.type" value="googleAdSense" ng-disabled="ngModel.googleAdSense"/><span translate="">Google AdSense</span></label></div><div class="radio"><label><input type="radio" ng-model="integration.type" value="googleTagManager" ng-disabled="ngModel.googleTagManager"/><span translate="">Google Tag Manager</span></label></div><div class="radio"><label><input type="radio" ng-model="integration.type" value="yandexWebmaster" ng-disabled="ngModel.yandexWebmaster"/><span translate="">Yandex Webmaster</span></label></div><div class="radio"><label><input type="radio" ng-model="integration.type" value="yandexMetrica" ng-disabled="ngModel.yandexMetrica"/><span translate="">Yandex Metrica</span></label></div><div class="radio"><label><input type="radio" ng-model="integration.type" value="facebookPixel" ng-disabled="ngModel.facebookPixel"/><span translate="">Facebook Pixel</span></label></div><div class="radio"><label><input type="radio" ng-model="integration.type" value="jivosite" ng-disabled="ngModel.jivosite"/><span translate="">Chat</span> jivosite</label></div><div class="radio"><label><input type="radio" ng-model="integration.type" value="chatflow" ng-disabled="ngModel.chatflow"/><span translate="">Chat</span> chatflow</label></div><div class="radio"><label><input type="radio" ng-model="integration.type" value="disqus" ng-disabled="ngModel.disqus"/><span translate="">Disqus comments</span></label></div><div class="radio"><label><input type="radio" ng-model="integration.type" value="custom" ng-disabled="ngModel.custom"/><span translate="">Custom integrations</span></label></div></div><hr/></div><form ng-show="integration.type==\'googleSearchConsole\'" name="GSCform"><label><span translate="">Integration with</span><span style="padding-left:5px;">Google Search Console</span></label><ul style="list-style:none;padding-left:1em;"><li><b>1.</b><span translate="">Copy the meta tag from <a href="{{integrationLink(integration.type)}}" target=\'_blank\'>this page</a></span></li><li><b>2.</b><span translate="">Paste here meta tag from Alternate methods in Google Search Console for site</span><div class="form-group"><textarea class="form-control" ng-model="integration.data" name="data" a-integration="googleSearchConsole" placeholder="&lt;meta name=&quot;google-site-verification&quot; content=&quot;some code&quot; /&gt;"></textarea></div><span class="text-danger" ng-if="GSCform.$invalid &amp;&amp; GSCform.$dirty">[invalid meta html-tag]</span></li><li><b>3.</b><span translate="">Click \'Add\' button in this window</span></li><li><b>4.</b><span translate="">Return to Google Search Console and click "VERIFY" button</span></li></ul></form><form ng-show="integration.type==\'googleAdSense\'" name="GASform"><label><span translate="">Integration with</span><span style="padding-left:5px;">Google AdSense</span></label><ul style="list-style:none;padding-left:1em;"><li><b>1.</b><span translate="">Paste here script tag from Google AdSense</span><div class="form-group"><textarea class="form-control" ng-model="integration.data" name="data" a-integration="googleAdSense" placeholder="&lt;script data-ad-client=&quot;ca-pub-YOUR-NUMBER&quot; async src=&quot;https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js&quot;&gt;&lt;/script&gt;"></textarea></div><span class="text-danger" ng-if="GASform.$invalid &amp;&amp; GASform.$dirty">[invalid script html-tag]</span></li><li><b>2.</b><span translate="">Click \'Add\' button in this window</span></li></ul></form><form ng-show="integration.type==\'googleAnalytics\'" name="GAform"><label><span translate="">Integration with</span><span style="padding-left:5px;">Google Analytics</span></label><ul style="list-style:none;padding-left:1em;"><li><b>1.</b><span translate="">Copy here script tag from Tracking Info page in Google Analytics for site</span><div class="form-group"><textarea class="form-control" ng-model="integration.data" placeholder="{{\'Universal Analytics tracking code\' | translate}} {{origin}}" a-integration="googleAnalytics" name="data" style="min-height:10em;"></textarea></div><span class="text-danger" ng-if="GAform.$invalid &amp;&amp; GAform.$dirty">[invalid script html-tag]</span></li><li><b>2.</b><span translate="">Click \'Add\' button in this window</span></li></ul></form><form ng-show="integration.type==\'googleTagManager\'" name="GTform"><label><span translate="">Integration with</span><span style="padding-left:5px;">Google Tag Manager</span></label><ul style="list-style:none;padding-left:1em;"><li><b>1.</b><span translate="">Copy here script tag from Tracking Info page in Google Tag Manager for site</span><div class="form-group"><textarea class="form-control" ng-model="integration.data" a-integration="googleTagManager" name="data" style="min-height:10em;"></textarea></div><span class="text-danger" ng-if="GTform.$invalid &amp;&amp; GTform.$dirty">[invalid script html-tag]</span></li><li><b>2.</b><span translate="">Click \'Add\' button in this window</span></li></ul></form><form ng-show="integration.type==\'yandexMetrica\'" name="YMetricaform"><label><span translate="">Integration with</span><span style="padding-left:5px;">Yandex Metrica</span></label><ul style="list-style:none;padding-left:1em;"><li><b>1.</b><span translate="">Copy here script tag from Settings-Counter page in Yandex Metrica for site</span><div class="form-group"><textarea class="form-control" ng-model="integration.data" a-integration="yandexMetrica" name="data" style="min-height:10em;"></textarea></div><span class="text-danger" ng-if="YMetricaform.$invalid &amp;&amp; YMetricaform.$dirty">[invalid script html-tag]</span></li><li><b>2.</b><span translate="">Click \'Add\' button in this window</span></li></ul></form><form ng-show="integration.type==\'yandexWebmaster\'" name="YWform"><label><span translate="">Integration with</span><span style="padding-left:5px;">Yandex Webmaster</span></label><ul style="list-style:none;padding-left:1em;"><li><b>1.</b><span translate="">Copy the meta tag from <a href="{{integrationLink(integration.type)}}" target=\'_blank\'>this page</a></span></li><li><b>2.</b><span translate="">Paste here meta tag</span><div class="form-group"><textarea class="form-control" ng-model="integration.data" name="data" a-integration="yandexWebmaster" placeholder="&lt;meta name=&quot;yandex-verification&quot; content=&quot;some code&quot; /&gt;"></textarea></div><span class="text-danger" ng-if="YWform.$invalid &amp;&amp; YWform.$dirty">[invalid meta html-tag]</span></li><li><b>3.</b><span translate="">Click \'Add\' button in this window</span></li><li><b>4.</b><span translate="">Return to Yandex Webmaster and click "VERIFY" button</span></li></ul></form><form ng-show="integration.type==\'facebookPixel\'" name="fbPixelform"><label><span translate="">Integration with</span><span style="padding-left:5px;">Facebook Pixel</span></label><ul style="list-style:none;padding-left:1em;"><li><b>1.</b><span translate="">Copy here facebook pixel code from Event Manager page for this site</span><div class="form-group"><textarea class="form-control" ng-model="integration.data" a-integration="facebookPixel" name="data" style="min-height:10em;"></textarea></div><span class="text-danger" ng-if="fbPixelform.$invalid &amp;&amp; fbPixelform.$dirty">[invalid script html-tag]</span></li><li><b>2.</b><span translate="">Click \'Add\' button in this window</span></li></ul></form><form ng-show="integration.type==\'jivosite\'" name="Jform"><label><span translate="">Integration with</span><span style="padding-left:5px;">Jivosite</span></label><ul style="list-style:none;padding-left:1em;"><li><b>1.</b><span translate="">Copy code from https://admin.jivosite.com</span><div class="form-group"><textarea class="form-control" ng-model="integration.data" placeholder="{{\'Paste here html code for site\' | translate}} {{origin}}" a-integration="jivosite" name="data" style="min-height:7em;"></textarea></div><span class="text-danger" ng-if="Jform.$invalid &amp;&amp; Jform.$dirty">[invalid html code]</span></li><li><b>2.</b><span translate="">Click \'Add\' button in this window</span></li></ul></form><form ng-show="integration.type==\'chatflow\'" name="chatflowForm"><label><span translate="">Integration with</span><span style="padding-left:5px;">Chatflow</span></label><ul style="list-style:none;padding-left:1em;"><li><b>1.</b><span translate="">Copy here chatflow code from https://chatflow.io/</span><div class="form-group"><textarea class="form-control" ng-model="integration.data" a-integration="chatflow" name="data" style="min-height:10em;"></textarea></div><span class="text-danger" ng-if="chatflowForm.$invalid &amp;&amp; chatflowForm.$dirty">[invalid script html-tag]</span></li><li><b>2.</b><span translate="">Click \'Add\' button in this window</span></li></ul></form><form ng-show="integration.type==\'disqus\'" name="disqusForm"><label><span translate="">Integration with</span><span style="padding-left:5px;">Disqus</span></label><ul style="list-style:none;padding-left:1em;"><li><b>1.</b><span translate="">Copy here disqus universal code from https://disqus.com/</span><div class="form-group"><textarea class="form-control" ng-model="integration.data" a-integration="disqus" name="data" style="min-height:10em;"></textarea></div><span class="text-danger" ng-if="disqusForm.$invalid &amp;&amp; disqusForm.$dirty">[invalid script html-tag]</span></li><li><b>2.</b><span translate="">Click \'Add\' button in this window</span></li></ul></form><form ng-show="integration.type==\'custom\'" name="CustomForm"><label><span translate="">Custom integrations</span></label><ul style="list-style:none;padding-left:1em;"><li><b>1.</b><span translate="">Paste here any html tags that you want to add to document.head</span><div class="form-group"><textarea class="form-control" ng-model="integration.data" name="data" a-integration="custom" placeholder=""></textarea></div><span class="text-danger" ng-if="CustomForm.$invalid &amp;&amp; CustomForm.$dirty">[invalid html-tags]</span></li><li><b>2.</b><span translate="">Click \'Add\' button in this window</span></li></ul></form></div><div class="modal-body" ng-show="!integration.type"><h4 class="text-center" translate="" style="margin-top:1em;">You\'ve added all available integrations</h4><div class="text-center" style="margin:2em;"><button class="btn btn-success" ng-click="cancel()">OK</button></div></div><div class="modal-footer" ng-show="integration.type"><button class="btn btn-primary" ng-click="ok()" translate="" ng-disabled="GSCform.$invalid" ng-show="integration.type==\'googleSearchConsole\'" translate-context="append">Add</button><button class="btn btn-primary" ng-click="ok()" translate="" ng-disabled="GASform.$invalid" ng-show="integration.type==\'googleAdSense\'" translate-context="append">Add</button><button class="btn btn-primary" ng-click="ok()" translate="" ng-disabled="GAform.$invalid" ng-show="integration.type==\'googleAnalytics\'" translate-context="append">Add</button><button class="btn btn-primary" ng-click="ok()" translate="" ng-disabled="GTform.$invalid" ng-show="integration.type==\'googleTagManager\'" translate-context="append">Add</button><button class="btn btn-primary" ng-click="ok()" translate="" ng-disabled="YWform.$invalid" ng-show="integration.type==\'yandexWebmaster\'" translate-context="append">Add</button><button class="btn btn-primary" ng-click="ok()" translate="" ng-disabled="YMetricaform.$invalid" ng-show="integration.type==\'yandexMetrica\'" translate-context="append">Add</button><button class="btn btn-primary" ng-click="ok()" translate="" ng-disabled="fbPixelform.$invalid" ng-show="integration.type==\'facebookPixel\'" translate-context="append">Add</button><button class="btn btn-primary" ng-click="ok()" translate="" ng-disabled="Jform.$invalid" ng-show="integration.type==\'jivosite\'" translate-context="append">Add</button><button class="btn btn-primary" ng-click="ok()" translate="" ng-disabled="chatflowForm.$invalid" ng-show="integration.type==\'chatflow\'" translate-context="append">Add</button><button class="btn btn-primary" ng-click="ok()" translate="" ng-disabled="disqusForm.$invalid" ng-show="integration.type==\'disqus\'" translate-context="append">Add</button><button class="btn btn-primary" ng-click="ok()" translate="" ng-disabled="CustomForm.$invalid" ng-show="integration.type==\'custom\'" translate-context="append">Add</button><button class="btn btn-default" ng-click="cancel()" translate="">Cancel</button></div>' + ''),
        windowClass: 'a-integrations-modal',
        controller: ['$scope', '$uibModalInstance', 'ngModel', 'options', modalController],
        size: 'md',
        resolve: {
          ngModel: function() {
            return ngModel || {};
          },
          options: function() {
            return options || {};
          }
        }
      });
      return result.result;
    };
    this.add = open;
    this.edit = (function(_this) {
      return function(ngModel, serviceName, options) {
        options = options || {};
        options.edit = true;
        options.integration = {
          type: serviceName,
          data: ngModel[serviceName]
        };
        return open(ngModel, options);
      };
    })(this);
    return this;
  }]);

}).call(this);
;
(function() {
  angular.module('AdminApp').config(["uiceItemEditorProvider", function(uiceItemEditorProvider) {
    return uiceItemEditorProvider.setDefaultConfigForModel('CmsArticle', {
      editModalSize: 'lg',
      getNewItem: function() {
        return {};
      }
    });
  }]).controller('ACmsArticleEditorCtrl', ["$scope", "$cms", "$injector", "AdminAppConfig", function($scope, $cms, $injector, AdminAppConfig) {
    var CmsCategoryForArticle;
    $scope.AdminAppConfig = AdminAppConfig;
    $scope.$cms = $cms;
    $scope.activeTab = 0;
    if (AdminAppConfig.visibleModels.CmsCategoryForArticle) {
      CmsCategoryForArticle = $injector.get('CmsCategoryForArticle');
      $scope.options = $scope.options || {};
      $scope.options.gdprWidgetOptions = [
        {
          value: null,
          description: angular.tr('Without widget')
        }, {
          value: 'info',
          description: angular.tr('Information request')
        }, {
          value: 'anonimization',
          description: angular.tr('Anonimization request')
        }, {
          value: 'deletion',
          description: angular.tr('Deletion request')
        }
      ];
      return CmsCategoryForArticle.find({
        filter: {
          where: {
            identifier: 'legal-information'
          }
        }
      }, function(items) {
        $scope.options.legalInformationCategory = items[0];
        $scope.ngModel.data = $scope.ngModel.data || {};
        if (!$scope.ngModel.data.gdprWidget) {
          $scope.ngModel.data.gdprWidget = null;
        }
      });
    }
  }]).directive('aCmsArticleEditor', function() {
    return {
      restrict: 'E',
      scope: {
        ngModel: '=',
        form: '=?',
        forceLang: '=?',
        modal: '@?'
      },
      controller: 'ACmsArticleEditorCtrl',
      template: ('/client/admin_app/_modelEditorDirectives/aCmsArticleEditor/aCmsArticleEditor.html', '<div ng-class="{\'content-with-sidebar\': modal != \'true\'}"><uib-tabset active="activeTab" ng-class="{content: modal != \'true\'}"><uib-tab index="0"><uib-tab-heading><span class="visible-xxs-inline" translate="">Editor</span><span ng-switch="!!$cms.settings.cmsLangCodes.length"><ul class="list-inline hidden-xxs" ng-switch-when="true"><li><span translate="">Editor</span><span>. </span><span translate="">Language</span></li><li class="nopadding-lr"><uic-lang-picker class="xs" ng-model="$parent.forceLang" size="sm"></uic-lang-picker></li></ul><span ng-switch-when="false" translate="">Editor</span></span></uib-tab-heading><div class="uic-lang-picker-xs-block"><ul class="list-inline"><li><span translate="">Content language</span></li><li><uic-lang-picker ng-model="forceLang" size="sm"></uic-lang-picker></li></ul></div><uice-l10n-input class="form-group" ng-model="ngModel.title" lang="forceLang" maxlength="75" label="Title" required="true"></uice-l10n-input><div class="form-group" uic-if-on-current-user-permission="*.view_CmsCategoryForArticle"><label translate="">Category for article</label><a-db-relation-editor ng-model="ngModel.category" type="belongsTo" model-name="CmsCategoryForArticle" current-user-permission-create="*.add_CmsCategoryForArticle" current-user-permission-edit="*.change_CmsCategoryForArticle"></a-db-relation-editor></div><uice-l10n-input class="form-group" ng-model="ngModel.description" lang="forceLang" type="textarea" maxlength="450" label="Description"></uice-l10n-input><uice-l10n-input class="form-group" ng-model="ngModel.body" lang="forceLang" type="html" cms-model-item="ngModel" maxlength="57000" min-height="40em" label="Body"></uice-l10n-input><div ng-if="AdminAppConfig.visibleModels.CmsKeyword"><hr/><a-keywords-id-list-editor ng-model="ngModel.keywords" force-lang="forceLang" current-user-permission-create="*.add_CmsKeyword" current-user-permission-edit="*.change_CmsKeyword"></a-keywords-id-list-editor></div><div><hr/><small class="help-block"><span class="text-danger">*</span><span> - </span><span translate="">Required fields</span></small></div></uib-tab><uib-tab class="preview-tab" heading="{{\'Preview\' | translate }}" index="2" ng-if="ngModel.id"><div class="content-container"><br/><uicv-article item="ngModel" force-lang="forceLang"></uicv-article></div></uib-tab></uib-tabset><div class="sidebar" ng-if="modal != \'true\'"><a-object-edit-sidebar ng-model="ngModel"><main-panel><div class="form-group nomargin" ng-show="$parent.$parent.options.legalInformationCategory.id == $parent.$parent.ngModel.category &amp;&amp; $parent.$parent.ngModel.category"><label translate="">GDPR widget:</label><uic-constant ng-model="$parent.$parent.ngModel.data.gdprWidget" select="$parent.$parent.options.gdprWidgetOptions" input-class="input-sm"></uic-constant></div></main-panel></a-object-edit-sidebar></div></div>' + '')
    };
  });

}).call(this);
;
(function() {
  angular.module('AdminApp').config(["uiceItemEditorProvider", function(uiceItemEditorProvider) {
    return uiceItemEditorProvider.setDefaultConfigForModel('CmsCategoryForArticle', {
      editModalSize: 'md',
      getNewItem: function() {
        return {
          "isPublished": false,
          "data": {},
          "identifier": "",
          "keywords": []
        };
      }
    });
  }]).directive('aCmsCategoryForArticleEditor', function() {
    return {
      restrict: 'E',
      scope: {
        ngModel: '=',
        form: '=?',
        forceLang: '=?',
        modal: '@?'
      },
      controller: ["$scope", "$cms", "AdminAppConfig", function($scope, $cms, AdminAppConfig) {
        $scope.$cms = $cms;
        $scope.activeTab = 0;
        return $scope.AdminAppConfig = AdminAppConfig;
      }],
      template: ('/client/admin_app/_modelEditorDirectives/aCmsCategoryForArticleEditor/aCmsCategoryForArticleEditor.html', '<div ng-class="{\'content-with-sidebar\': modal != \'true\'}"><uib-tabset active="activeTab" ng-class="{content: modal != \'true\'}"><uib-tab index="0"><uib-tab-heading><span class="visible-xxs-inline" translate="">Editor</span><span ng-switch="!!$cms.settings.cmsLangCodes.length"><ul class="list-inline hidden-xxs" ng-switch-when="true"><li><span translate="">Editor</span><span>. </span><span translate="">Language</span></li><li class="nopadding-lr"><uic-lang-picker class="xs" ng-model="$parent.forceLang" size="sm"></uic-lang-picker></li></ul><span ng-switch-when="false" translate="">Editor</span></span></uib-tab-heading><div class="uic-lang-picker-xs-block"><ul class="list-inline"><li><span translate="">Content language</span></li><li><uic-lang-picker ng-model="forceLang" size="sm"></uic-lang-picker></li></ul></div><div class="form-group"><label><span translate="">Unique identifier</span><span class="text-danger"> *</span></label><input class="form-control" ng-model="ngModel.identifier" maxlength="75" minlength="1" ng-disabled="ngModel.id" required=""/></div><uice-l10n-input class="form-group" ng-model="ngModel.title" type="text" lang="forceLang" required="" label="Title"></uice-l10n-input><uice-l10n-input class="form-group" ng-model="ngModel.description" type="textarea" lang="forceLang" label="Description"></uice-l10n-input><div class="form-group" ng-if="modal == \'true\'"><uic-checkbox ng-model="ngModel.isPublished"><span translate="">Is published</span></uic-checkbox></div><div ng-if="AdminAppConfig.visibleModels.CmsKeyword &amp;&amp; modal != \'true\'"><hr/><a-keywords-id-list-editor ng-model="ngModel.keywords" force-lang="forceLang" current-user-permission-create="*.add_CmsKeyword" current-user-permission-edit="*.change_CmsKeyword"></a-keywords-id-list-editor></div><div><hr/><small class="help-block"><span class="text-danger">*</span><span> - </span><span translate="">Required fields</span></small></div></uib-tab></uib-tabset><div class="sidebar" ng-if="modal != \'true\'"><a-object-edit-sidebar ng-model="ngModel" hide-files="true"></a-object-edit-sidebar></div></div>' + '')
    };
  });

}).call(this);
;
(function() {
  angular.module('AdminApp').config(["uiceItemEditorProvider", function(uiceItemEditorProvider) {
    return uiceItemEditorProvider.setDefaultConfigForModel('CmsCategoryForPictureAlbum', {
      editModalSize: 'md',
      getNewItem: function() {
        return {
          "isPublished": false,
          "data": {},
          "identifier": "",
          "keywords": []
        };
      }
    });
  }]).directive('aCmsCategoryForPictureAlbumEditor', function() {
    return {
      restrict: 'E',
      scope: {
        ngModel: '=',
        form: '=?',
        forceLang: '=?',
        modal: '@?'
      },
      controller: ["$scope", "$cms", "AdminAppConfig", function($scope, $cms, AdminAppConfig) {
        $scope.$cms = $cms;
        $scope.activeTab = 0;
        return $scope.AdminAppConfig = AdminAppConfig;
      }],
      template: ('/client/admin_app/_modelEditorDirectives/aCmsCategoryForPictureAlbumEditor/aCmsCategoryForPictureAlbumEditor.html', '<div ng-class="{\'content-with-sidebar\': modal != \'true\'}"><uib-tabset active="activeTab" ng-class="{content: modal != \'true\'}"><uib-tab index="0"><uib-tab-heading><span class="visible-xxs-inline" translate="">Editor</span><span ng-switch="!!$cms.settings.cmsLangCodes.length"><ul class="list-inline hidden-xxs" ng-switch-when="true"><li><span translate="">Editor</span><span>. </span><span translate="">Language</span></li><li class="nopadding-lr"><uic-lang-picker class="xs" ng-model="$parent.forceLang" size="sm"></uic-lang-picker></li></ul><span ng-switch-when="false" translate="">Editor</span></span></uib-tab-heading><div class="uic-lang-picker-xs-block"><ul class="list-inline"><li><span translate="">Content language</span></li><li><uic-lang-picker ng-model="forceLang" size="sm"></uic-lang-picker></li></ul></div><div class="form-group"><label><span translate="">Unique identifier</span><span class="text-danger"> *</span></label><input class="form-control" ng-model="ngModel.identifier" maxlength="75" minlength="1" ng-disabled="ngModel.id" required=""/></div><uice-l10n-input class="form-group" ng-model="ngModel.title" type="text" lang="forceLang" required="" label="Title"></uice-l10n-input><uice-l10n-input class="form-group" ng-model="ngModel.description" type="textarea" lang="forceLang" label="Description"></uice-l10n-input><div class="form-group" ng-if="modal == \'true\'"><uic-checkbox ng-model="ngModel.isPublished"><span translate="">Is published</span></uic-checkbox></div><div ng-if="AdminAppConfig.visibleModels.CmsKeyword &amp;&amp; modal != \'true\'"><hr/><a-keywords-id-list-editor ng-model="ngModel.keywords" force-lang="forceLang" current-user-permission-create="*.add_CmsKeyword" current-user-permission-edit="*.change_CmsKeyword"></a-keywords-id-list-editor></div><div><hr/><small class="help-block"><span class="text-danger">*</span><span> - </span><span translate="">Required fields</span></small></div></uib-tab></uib-tabset><div class="sidebar" ng-if="modal != \'true\'"><a-object-edit-sidebar ng-model="ngModel" hide-url-id="true" hide-files="true"></a-object-edit-sidebar></div></div>' + '')
    };
  });

}).call(this);
;
(function() {
  angular.module('AdminApp').directive('aCmsKeywordEditor', function() {
    return {
      restrict: 'E',
      scope: {
        ngModel: '=',
        form: '=?',
        forceLang: '=?',
        modal: '@?'
      },
      controller: ["$scope", "$cms", function($scope, $cms) {
        $scope.$cms = $cms;
        return $scope.activeTab = 0;
      }],
      template: ('/client/admin_app/_modelEditorDirectives/aCmsKeywordEditor/aCmsKeywordEditor.html', '<div ng-class="{\'content-with-sidebar\': modal != \'true\'}"><uib-tabset active="activeTab" ng-class="{content: modal != \'true\'}"><uib-tab><uib-tab-heading><span class="visible-xxs-inline" translate="">Editor</span><span ng-switch="!!$cms.settings.cmsLangCodes.length"><ul class="list-inline hidden-xxs" ng-switch-when="true"><li><span translate="">Editor</span><span>. </span><span translate="">Language</span></li><li class="nopadding-lr"><uic-lang-picker class="xs" ng-model="$parent.forceLang" size="sm"></uic-lang-picker></li></ul><span ng-switch-when="false" translate="">Editor</span></span></uib-tab-heading><div class="uic-lang-picker-xs-block"><ul class="list-inline"><li><span translate="">Content language</span></li><li><uic-lang-picker ng-model="forceLang" size="sm"></uic-lang-picker></li></ul></div><uice-l10n-input class="form-group" ng-model="ngModel.title" lang="forceLang" maxlength="35" label="Title"></uice-l10n-input><uice-l10n-input class="form-group" ng-model="ngModel.description" lang="forceLang" type="textarea" maxlength="450" label="Description"></uice-l10n-input><div class="form-group"><uic-checkbox ng-model="ngModel.isPublished"><span translate="">Is published</span></uic-checkbox></div><div><hr/><small class="help-block"><span class="text-danger">*</span><span> - </span><span translate="">Required fields</span></small></div></uib-tab></uib-tabset><div class="sidebar" ng-if="modal != \'true\'"><a-object-edit-sidebar ng-model="ngModel" hide-url-id="true" hide-files="true"></a-object-edit-sidebar></div></div>' + '')
    };
  });

}).call(this);
;
(function() {
  angular.module('ui.cms.editable').directive('aCmsTranslationsEditor', ["$filter", "$cms", "$timeout", function($filter, $cms, $timeout) {
    var GETTEXT_FORMS;
    GETTEXT_FORMS = {
      ru: [1, 2, 5],
      uk: [1, 2, 5],
      cz: [1, 2, 5],
      es: [1, 2],
      de: [1, 2],
      fr: [1, 2]
    };
    return {

      /**
      *   @ngdoc directive
      *   @name AdminApp.directive:aCmsTranslationsEditor
      *   @description
      *       редактор переводов для CmsTranslations
      *   @restrict E
      *   @param {object} ngModel (ссылка) модель CmsTranslations
      *   @param {boolean=} [disableAutoScroll=false] (значение) отключить автоскролл вниз страницы(
      *       при выборе пункта из таблицы строк, директива сама проматывает стринцу вниз чтоб можно
      *       было оказаться у textarea с текстом)
      *   @example
      *       <pre>
      *           $scope.translations ={
      *                lang: "ru",
      *                msgs:[
      *                    {
      *                        msgId: "Hello first user!",
      *                        msgCtxt: "Some translation context",
      *                        msgIdPlural: "Hello user #{{count}}",
      *                        msgStr: []
      *                    },
      *                    {
      *                        msgId: "Hello",
      *                        msgComm: "Greeting for user",
      *                        msgStr:["Привет"]
      *                    },
      *                    {
      *                        msgId: "World",
      *                        msgStr:[]
      *                    }
      *                ]
      *            };
      *       </pre>
      *       <pre>
      *           //- pug
      *           a-cms-translations-editor(ng-model="translations", disable-auto-scroll='true')
      *       </pre>
      *       <example module="DocExampleApp">
      *           <file name="script.js">
      *               mkDocCtrl('aCmsTranslationsEditorCtrl', {
      *                   translations: {
      *                       lang: "ru",
      *                        msgs:[
      *                            {
      *                                msgId: "Hello first user!",
      *                                msgCtxt: "Some translation context",
      *                                msgIdPlural: "Hello user #{{count}}",
      *                                msgStr: []
      *                           },
      *                            {
      *                                msgId: "Hello",
      *                                msgComm: "Greeting for user",
      *                               msgStr:["Привет"]
      *                            },
      *                            {
      *                                msgId: "World",
      *                               msgStr:[]
      *                            }
      *                        ]
      *                   }
      *               });
      *           </file>
      *           <file name="index.html">
      *               <div ng-controller="aCmsTranslationsEditorCtrl">
      *                   <a-cms-translations-editor ng-model="translations" disable-auto-scroll='true'></a-cms-translations-editor>
      *               </div>
      *           </file>
      *       </example>
       */
      restrict: 'E',
      scope: {
        ngModel: '=',
        disableAutoScroll: '@?'
      },
      controller: ["$scope", "$filter", "$translationService", function($scope, $filter, $translationService) {
        var filterMsgs, googleTranslations, ngFilter, processMsgs;
        googleTranslations = {};
        $scope.activeTabs = {
          translation: 0
        };
        $scope.qfilter = {
          module: null,
          searchTerm: ''
        };
        $scope.$cms = $cms;
        $scope.currentMsgId = -1;
        $scope.CMS_DEFAULT_LANG = $filter('fromLangCodeToLangName')(CMS_DEFAULT_LANG);
        ngFilter = $filter('filter');
        if (!$scope.ngModel) {
          return console.error("Provide ngModel for aCmsTranslationsEditor directive! Got null or undefined");
        }
        if (GETTEXT_FORMS[$scope.ngModel.lang]) {
          $scope.GFORM = GETTEXT_FORMS[$scope.ngModel.lang];
        } else {
          $scope.GFORM = [1, 2];
        }
        processMsgs = function(msgs) {
          var f, google_tr, i, item, j, k, l, len, len1, not_translated, ref, translated;
          google_tr = [];
          translated = [];
          not_translated = [];
          $scope.modules = [];
          for (i = k = 0, len = msgs.length; k < len; i = ++k) {
            item = msgs[i];
            google_tr.push(item.msgId);
            item._source = item.msgId;
            if (item.msgCtxt) {
              item._source = item._source + " [" + item.msgCtxt + "]";
            }
            if (item.msgIdPlural) {
              google_tr.push(item.msgIdPlural);
              if (item.msgStr.length < $scope.GFORM.length) {
                ref = $scope.GFORM;
                for (j = l = 0, len1 = ref.length; l < len1; j = ++l) {
                  f = ref[j];
                  if (!item.msgStr[j]) {
                    item.msgStr[j] = '';
                  }
                }
              }
            } else if (!item.msgStr[0]) {
              item.msgStr[0] = '';
            }
            item.$$modules = [];
            (item.references || []).forEach(function(r) {
              r = r.split('/');
              if (r.length >= 2 && r[0] === 'client') {
                r = "client/" + r[1];
              } else if (r[0] !== 'client') {
                r = r[0];
              }
              if (!item.$$modules.includes(r)) {
                item.$$modules.push(r);
              }
              if (!$scope.modules.includes(r)) {
                $scope.modules.push(r);
              }
            });
            if (item.msgStr[0].length) {
              translated.push(item);
            } else {
              not_translated.push(item);
            }
          }
          $translationService.translate(CMS_DEFAULT_LANG, $scope.ngModel.lang, google_tr).then(function(data) {
            googleTranslations = data;
          });
          return not_translated.concat(translated).map(function(item, i) {
            item.id = i;
            return item;
          });
        };
        $scope.edit = function(id) {
          $scope.currentMsgId = id;
          $scope.translationActiveTab = 0;
          $scope.googleTranslations = {
            msgId: googleTranslations[$scope.ngModel.msgs[id].msgId],
            msgIdPlural: googleTranslations[$scope.ngModel.msgs[id].msgIdPlural],
            url: $translationService.getTranslateUrl(CMS_DEFAULT_LANG, $scope.ngModel.lang, $scope.ngModel.msgs[id].msgId)
          };
          if ($scope.disableAutoScroll === 'true') {
            return;
          }
          return $timeout(function() {
            return window.smoothScrollTo(document.getElementById('editor-gettext-pot'));
          }, 0);
        };
        filterMsgs = function(msgs) {
          var filtered;
          if ($scope.qfilter.module) {
            filtered = msgs.filter(function(item) {
              var k, len, m, ref;
              ref = item.$$modules;
              for (k = 0, len = ref.length; k < len; k++) {
                m = ref[k];
                if ($scope.qfilter.module === m) {
                  return true;
                }
              }
              return false;
            });
          } else {
            filtered = msgs;
          }
          filtered = filtered.filter(function(item) {
            return !item.hidden;
          });
          return ngFilter(filtered, $scope.qfilter.searchTerm);
        };
        $scope.setFromTranslations = function(t) {
          var i, k, len, ref, v;
          if (t === 'singular' && $scope.googleTranslations.msgId) {
            $scope.ngModel.msgs[$scope.currentMsgId].msgStr[0] = $scope.googleTranslations.msgId;
            $scope.activeTabs.translation = 0;
          } else if (t === 'plural' && $scope.googleTranslations.msgIdPlural) {
            $scope.activeTabs.translation = 1;
            ref = $scope.ngModel.msgs[$scope.currentMsgId].msgStr;
            for (i = k = 0, len = ref.length; k < len; i = ++k) {
              v = ref[i];
              if (i > 0) {
                $scope.ngModel.msgs[$scope.currentMsgId].msgStr[i] = $scope.googleTranslations.msgIdPlural;
              }
            }
          }
        };
        $scope.resetFilter = function() {
          return $scope.qfilter = {
            module: null,
            searchTerm: ''
          };
        };
        $scope.ngModel.msgs = processMsgs($scope.ngModel.msgs || []);
        return $scope.$watch('qfilter', function() {
          return $scope.filteredMsgs = filterMsgs($scope.ngModel.msgs || []);
        }, true);
      }],
      template: ('/client/admin_app/_modelEditorDirectives/aCmsTranslationsEditor/aCmsTranslationsEditor.html', '<a-table-toolbar search-term="qfilter.searchTerm" options="{hideLangPicker: true}"><div class="form-group nomargin hidden-xs" ng-show="modules.length &gt; 1"><ul class="list-table w-auto vertical-align-middle"><li translate="">Modules:</li><li><select class="form-control" ng-model="qfilter.module" ng-options="m as m  for m in modules"><option value="" translate="">All modules</option></select></li></ul></div></a-table-toolbar><a-view-title></a-view-title><div ng-switch="!!filteredMsgs.length"><uice-no-items ng-switch-when="false" search-term="qfilter.searchTerm" reset-filter="resetFilter()"></uice-no-items><div class="table-container" ng-switch-when="true"><table class="table table-condensed table-bordered" ng-switch="$cms.screenSize!=\'xs\'"><thead ng-switch-when="true"><tr><th><span translate="">Source text</span> ({{:: CMS_DEFAULT_LANG}})</th><th><span translate="">Translation</span> ({{:: ngModel.lang | fromLangCodeToLangName}})</th></tr></thead><thead ng-switch-when="false"><tr><th><span translate="">Translation</span> {{:: CMS_DEFAULT_LANG}} -&#62; {{:: ngModel.lang | fromLangCodeToLangName}}</th></tr></thead></table><div class="wrapper-table-msgs"><table class="table table-striped table-hover table-condensed table-bordered uic-items-table"><tbody ng-if="$cms.screenSize!=\'xs\'"><tr ng-repeat="item in filteredMsgs  track by $index" ng-click="edit(item.id)" ng-class="{\'active\': currentMsgId==item.id}"><td>{{item._source}}</td><td><span ng-show="item.msgStr[0]">{{item.msgStr[0]}}</span><b class="text-danger" ng-show="!item.msgStr[0]" translate="">Without translation</b></td></tr></tbody><tbody ng-if="$cms.screenSize==\'xs\'"><tr ng-repeat="item in filteredMsgs  track by $index" ng-click="edit(item.id)" ng-class="{\'active\': currentMsgId==item.id}"><td><uic-state state="item.msgStr[0].length"></uic-state>{{item._source}}</td></tr></tbody></table></div></div></div><div id="editor-gettext-pot" ng-show="currentMsgId&gt;-1"><hr/><div class="tab-content"><div class="tab-pane active tab-form nomargin-bottom"><div class="row"><div class="col-md-50"><div class="translation-block"><h5><span class="text-success" translate="">Source text</span><small ng-show="ngModel.msgs[currentMsgId].msgCtxt" style="padding-left:5px;">[ {{ngModel.msgs[currentMsgId].msgCtxt}} ]</small></h5><div ng-show="!ngModel.msgs[currentMsgId].msgIdPlural"><p><b><span translate="">Text</span>:</b> {{ngModel.msgs[currentMsgId].msgId}}</p></div><div ng-show="ngModel.msgs[currentMsgId].msgIdPlural"><p><b><span translate="">Singular</span>:</b> {{ngModel.msgs[currentMsgId].msgId}}</p><p><b><span translate="">Plural</span>:</b> {{ngModel.msgs[currentMsgId].msgIdPlural}}</p></div><p ng-show="ngModel.msgs[currentMsgId].msgComm"><small><b><span translate="">Comment(notes for translators)</span>:</b> {{ngModel.msgs[currentMsgId].msgComm}}</small></p></div><div class="gtranslate-block" ng-show="googleTranslations.msgId || googleTranslations.msgIdPlural"><h5><div class="gtranslate-block-logo"></div><a class="text-success" translate="" target="_blank" ng-href="{{googleTranslations.url}}" translate-context="Варианты перевода от...">Google Translate</a></h5><p><b ng-show="googleTranslations.msgIdPlural"><span translate="">Singular</span>:</b><b ng-show="!googleTranslations.msgIdPlural"><span translate="">Text</span>:</b> {{googleTranslations.msgId}}<span class="p5"></span><button class="btn btn-primary btn-xs" ng-click="setFromTranslations(\'singular\')" title="{{\'Apply translation\' | translate}}"><span class="far fa-copy"></span><span translate="">Apply</span></button></p><p ng-show="googleTranslations.msgIdPlural"><b><span translate="">Plural</span></b>: {{googleTranslations.msgIdPlural}}<span class="p5"></span><button class="btn btn-primary btn-xs" ng-click="setFromTranslations(\'plural\')" title="{{\'Apply translation\' | translate}}"><div class="far fa-copy"></div><span translate="">Apply</span></button></p></div></div><div class="col-md-50"><div class="translation-block"><h5 class="text-success" translate="">Translation</h5><div ng-if="!ngModel.msgs[currentMsgId].msgIdPlural"><textarea class="form-control w-margin" ng-model="ngModel.msgs[currentMsgId].msgStr[0]" placeholder="{{\'Type translation for text\' | translate}} \'{{ngModel.msgs[currentMsgId].msgId}}\'"></textarea></div><uib-tabset ng-if="ngModel.msgs[currentMsgId].msgIdPlural" active="activeTabs.translation" index="{{$index}}"><uib-tab ng-repeat="gettext_form in GFORM"><uib-tab-heading><span translate="">Form {{$index+1}}(e.g. "{{gettext_form}}")</span></uib-tab-heading><textarea class="form-control" ng-model="ngModel.msgs[currentMsgId].msgStr[$index]" placeholder="{{\'Type translation for text\' | translate}} \'{{ngModel.msgs[currentMsgId].msgIdPlural}}\'"></textarea></uib-tab></uib-tabset></div></div></div></div></div></div>' + '')
    };
  }]);

}).call(this);
;
(function() {
  angular.module('AdminApp').config(["uiceItemEditorProvider", function(uiceItemEditorProvider) {
    return uiceItemEditorProvider.setDefaultConfigForModel('CmsUser', {
      editModalSize: 'lg',
      getNewItem: function() {
        return {
          userRole: 'u',
          timezone: 'Europe/Prague',
          accountData: {}
        };
      }
    });
  }]).controller('ACmsUserEditorCtrl', ["$scope", "$cms", "$uiceUserHelpers", "$currentUser", "$timeout", "GroupPermissions", "$constants", "AdminAppConfig", function($scope, $cms, $uiceUserHelpers, $currentUser, $timeout, GroupPermissions, $constants, AdminAppConfig) {
    var USER_ROLES, rmW;
    USER_ROLES = $constants.resolve('CMS_USER.userRole');
    $scope.AdminAppConfig = AdminAppConfig;
    $scope.$cms = $cms;
    $scope.$currentUser = $currentUser;
    $scope.activeTab = 0;
    $scope.options = $scope.options || {};
    $scope.options.additionalPermissionsVisible = false;
    $scope.options.passwordType = 'password';
    $scope.open_forceSetPasswordModal = function() {
      $uiceUserHelpers.forceSetPassword($scope.ngModel);
    };
    $scope.toggleShowPassword = function() {
      if ($scope.options.passwordType === 'password') {
        $scope.options.passwordType = 'text';
      } else {
        $scope.options.passwordType = 'password';
      }
    };
    $scope.generatePassword = function() {
      $scope.ngModel.password = $uiceUserHelpers.generateNewPassword();
      $scope.ngModel.password2 = $scope.ngModel.password + '';
    };
    rmW = $scope.$watch('ngModel.id', function(id, oldValue) {
      if (id) {
        $scope.options.additionalPermissionsVisible = !!($scope.ngModel.user_permissions || []).length;
        rmW();
      }
    });
  }]).directive('aCmsUserEditor', function() {
    return {
      restrict: 'E',
      scope: {
        ngModel: '=',
        form: '=?',
        forceLang: '=?',
        modal: '@?'
      },
      controller: 'ACmsUserEditorCtrl',
      template: ('/client/admin_app/_modelEditorDirectives/aCmsUserEditor/aCmsUserEditor.html', '<div ng-class="{\'content-with-sidebar\': modal != \'true\'}"><uib-tabset active="activeTab" ng-class="{content: modal != \'true\'}"><uib-tab index="0" heading="{{\'Main\' | translate}}"><div class="row"><div class="col-md-50" ng-switch="!!ngModel.id"><div class="form-group" ng-switch-when="false"><label><span translate="">Username</span><span class="text-danger"> *</span><small class="error" ng-show="form.username.$error.usernameAvailable" translate="">Username alredy taken</small><small class="text-success" ng-show="form.username.$pending" translate="">Please wait...</small><small class="error" ng-show="form.username.$error.usernameChars" translate="">Invalid chars in username</small></label><input class="form-control" type="text" name="username" ng-disabled="ngDisabled" required="required" username-available="" username-chars="" ng-model="ngModel.username" placeholder="{{\'Only a-z, 0-9, and -_. allowed\' | translate}}" maxlength="150"/></div><div class="form-group" ng-switch-when="true"><label translate="">Username</label><input class="form-control" ng-model="ngModel.username" disabled="true"/></div></div><div class="col-md-50" ng-switch="!!ngModel.id"><div class="form-group" ng-switch-when="false"><label><span translate="">Email</span><span class="text-danger"> *</span><small class="error" ng-show="form.email.$error.emailAvailable" translate="">Email alredy registered</small><small class="text-success" ng-show="form.email.$pending" translate="">Please wait...</small></label><input class="form-control" type="email" name="email" ng-disabled="ngDisabled" required="" email-available="" ng-model="ngModel.email"/></div><div class="form-group" ng-switch-when="true"><label><span translate="">Email</span><span class="text-danger">*</span><small class="error" ng-show="form.email.$error.emailForUsernameAvailable" translate="">Email alredy registered</small></label><input class="form-control" type="email" required="" name="email" email-for-username-available="ngModel.username" ng-model="ngModel.email"/></div></div></div><div class="row" ng-if="!ngModel.id"><div class="form-group col-md-50"><label><span translate="translate">Password</span><span class="text-danger"> *</span></label><div class="input-group"><input class="form-control" type="{{options.passwordType}}" name="password" required="" ng-model="ngModel.password" autocomplete="new-password"/><div class="input-group-btn"><button class="btn btn-default" ng-click="toggleShowPassword()" title="{{\'Toggle password visibility\' | translate}}" ng-class="{active: options.passwordType == \'password\'}"><span class="fas nopadding" ng-class="{\'fa-eye\': options.passwordType == \'text\', \'fa-eye-slash\': options.passwordType == \'password\'}"></span></button><button class="btn btn-warning" ng-click="generatePassword()" title="{{\'Generate password\' | translate}}"><span class="fas fa-unlock-alt nopadding"></span></button></div></div></div><div class="form-group col-md-50"><label><span translate="translate">Repeat password</span><span class="text-danger"> *</span><small class="error" ng-show="form.password2.$error.compareEqual" translate="">Password doesn\'t match</small></label><input class="form-control" type="{{options.passwordType}}" name="password2" ng-disabled="ngDisabled" required="" ng-model="ngModel.password2" compare-equal="ngModel.password"/></div></div><div class="row"><div class="form-group col-md-50"><label><span translate="">User role</span><span class="text-danger"> *</span></label><uic-constant ng-model="ngModel.userRole" select="CMS_USER.userRole" ng-disabled="!$currentUser.isAdmin()"></uic-constant></div><div class="form-group col-md-50"><label><span translate="">Timezone</span><span class="text-danger"> *</span></label><uic-timezone-picker ng-model="ngModel.timezone" ng-required="true"></uic-timezone-picker></div></div><div class="row"><div class="col-md-50"><div class="form-group"><label translate="">First Name</label><input class="form-control" ng-model="ngModel.firstName"/></div></div><div class="col-md-50"><div class="form-group"><label translate="">Last Name</label><input class="form-control" ng-model="ngModel.lastName"/></div></div></div><div class="form-group" ng-show="ngModel.id" uic-if-on-current-user-permission="*.change_CmsUser"><a class="text-danger" translate="" ng-click="open_forceSetPasswordModal()">Set user password</a></div><div><hr/><small class="help-block"><span class="text-danger">*</span><span> - </span><span translate="">Required fields</span></small></div></uib-tab><uib-tab index="1" heading="{{\'Permissions\' | translate}}" uic-if-on-current-user-permission="*.change_CmsUser"><div ng-switch="!!AdminAppConfig.visibleModels.GroupPermissions"><div ng-switch-when="true"><div ng-hide="ngModel.userRole == \'a\'"><div class="form-group"><label translate="">Groups</label><a-group-permissions-editor-for-user ng-model="ngModel.groups" user-role="ngModel.userRole"></a-group-permissions-editor-for-user></div><hr/><div class="form-group"><label class="fake-link" ng-click="options.additionalPermissionsVisible = !options.additionalPermissionsVisible"><span translate="">Additional permissions</span><span> </span><span class="fas fa-chevron-down nopadding" ng-class="{\'fa-rotate-90\': !options.additionalPermissionsVisible}"></span></label><div uib-collapse="!options.additionalPermissionsVisible"><a-permissions-editor-for-user ng-model="ngModel.user_permissions"></a-permissions-editor-for-user></div></div></div><div ng-show="ngModel.userRole == \'a\'"><uic-alert class="nomargin" type="success"><b translate="">Admin has all permissions</b></uic-alert></div></div><div ng-switch-when="false"><uic-alert class="nomargin" type="info"><b translate="">Permissions disabled for this site</b></uic-alert></div></div></uib-tab></uib-tabset><div class="sidebar" ng-if="modal != \'true\'"><a-object-edit-sidebar ng-model="ngModel" hide-is-published="true" hide-files="true"></a-object-edit-sidebar></div></div>' + '')
    };
  });

}).call(this);
;

/*
    джанга не дает возможность держать переводы разрешений в бд.
    делаем это на стороне клиента
 */

(function() {
  angular.module('AdminApp').directive('aGroupPermissionsEditorForUser', ["gettextCatalog", "$constants", "AdminAppConfig", function(gettextCatalog, $constants, AdminAppConfig) {
    var GROUP_NAMES_BY_USER_ROLES, USER_ROLES, c, generateTranslationsForGroup, j, len;
    USER_ROLES = $constants.resolve('CMS_USER.userRole');
    GROUP_NAMES_BY_USER_ROLES = {};
    for (j = 0, len = USER_ROLES.length; j < len; j++) {
      c = USER_ROLES[j];
      GROUP_NAMES_BY_USER_ROLES[c.value] = "Permissions for '" + c.description + "'";
    }
    generateTranslationsForGroup = function(group) {
      var k, lang, userRole, v;
      group.nameL10n = {
        en: group.name
      };
      for (lang in AdminAppConfig.allowedLanguages) {
        if (!(lang !== 'en')) {
          continue;
        }
        for (k in GROUP_NAMES_BY_USER_ROLES) {
          v = GROUP_NAMES_BY_USER_ROLES[k];
          if (!(v === group.name)) {
            continue;
          }
          userRole = gettextCatalog.getString(USER_ROLES.findByProperty('value', k).description);
          group.nameL10n[lang] = gettextCatalog.getString("Permissions for '{{userRole}}", {
            userRole: userRole
          }, "user permission group") + "'";
          break;
        }
        group.nameL10n[lang] = group.nameL10n[lang] || gettextCatalog.getString(group.name, null, "user permission group");
      }
      return group;
    };
    return {
      restrict: 'E',
      scope: {
        ngModel: '=',
        userRole: '='
      },
      controller: ["$scope", "$element", "$constants", "GroupPermissions", "uiceItemEditor", function($scope, $element, $constants, GroupPermissions, uiceItemEditor) {
        var ngModelCtrl;
        $element.addClass('loading-data');
        ngModelCtrl = $element.controller('ngModel');
        GroupPermissions.find(function(groups) {
          $scope.groups = groups.map(generateTranslationsForGroup);
          $element.addClass('ready');
          $scope.$watch('userRole', function(userRole, oldValue) {
            var newGroup, oldGroup;
            oldGroup = ($scope.groups || []).findByProperty('name', GROUP_NAMES_BY_USER_ROLES[oldValue]);
            newGroup = ($scope.groups || []).findByProperty('name', GROUP_NAMES_BY_USER_ROLES[userRole]);
            if (oldGroup) {
              $scope.proxy[oldGroup.id] = false;
            }
            if (newGroup) {
              $scope.proxy[newGroup.id] = true;
            }
          });
        }, function(data) {
          $element.addClass('ready');
        });
        $scope.isDisabledName = function(name) {
          return GROUP_NAMES_BY_USER_ROLES[$scope.userRole] === name;
        };
        $scope.editGroup = function(group) {
          uiceItemEditor.edit(group, {
            modelName: 'GroupPermissions',
            title: angular.tr("Group permissions")
          }).then(function(data) {
            var i, item, l, len1, ref;
            ref = $scope.groups;
            for (i = l = 0, len1 = ref.length; l < len1; i = ++l) {
              item = ref[i];
              if (!(item.id === data.id)) {
                continue;
              }
              $scope.groups[i] = data;
              break;
            }
          });
        };
        ngModelCtrl.$render = function() {
          var id, l, len1, ref;
          $scope.proxy = {};
          ref = ngModelCtrl.$modelValue || [];
          for (l = 0, len1 = ref.length; l < len1; l++) {
            id = ref[l];
            $scope.proxy[id] = true;
          }
        };
        return $scope.$watch('proxy', function(proxy, oldValue) {
          var id, viewValue;
          viewValue = [];
          for (id in proxy || {}) {
            if (proxy[id]) {
              viewValue.push(parseInt(id));
            }
          }
          ngModelCtrl.$setViewValue(viewValue);
        }, true);
      }],
      template: ('/client/admin_app/_modelEditorDirectives/aGroupPermissionsEditor/aGroupPermissionsEditorForUser.html', '<div class="checkbox" ng-repeat="group in groups"><label><input ng-model="proxy[group.id]" type="checkbox" ng-disabled="isDisabledName(group.name)"/><span>{{group.nameL10n | l10n}}</span><span> </span><a ng-click="editGroup(group); $event.stopPropagation()"><span class="fas fa-pencil-alt"></span><span translate="">Edit</span></a></label></div>' + '')
    };
  }]).directive('aPermissionsEditorForUser', ["gettextCatalog", "AdminAppConfig", function(gettextCatalog, AdminAppConfig) {
    var KNOWN_PERMISSIONS, generateTranslationsForPermission, textToModelName;
    KNOWN_PERMISSIONS = [
      {
        prefix: 'view_cms',
        name: 'Can view cms',
        translate: "Can view {{modelName}}"
      }, {
        prefix: 'add_cms',
        name: 'Can add cms',
        translate: "Can add {{modelName}}"
      }, {
        prefix: 'change_cms',
        name: 'Can change cms',
        translate: "Can change {{modelName}}"
      }, {
        prefix: 'delete_cms',
        name: 'Can delete cms',
        translate: "Can delete {{modelName}}"
      }
    ];
    textToModelName = function(text) {
      text = text.split(' cms ')[1].trim().split(' ').map(function(str) {
        return str[0].toUpperCase() + str.substr(1);
      });
      return text.join('');
    };
    generateTranslationsForPermission = function(permission) {
      var j, knownPerm, lang, len, modelName;
      permission.nameL10n = {
        en: permission.name
      };
      for (j = 0, len = KNOWN_PERMISSIONS.length; j < len; j++) {
        knownPerm = KNOWN_PERMISSIONS[j];
        if (!(permission.name.startsWith(knownPerm.name) && permission.codename.startsWith(knownPerm.prefix))) {
          continue;
        }
        modelName = textToModelName(permission.name);
        if (permission.codename === ("" + knownPerm.prefix + (modelName.toLowerCase()))) {
          permission.nameL10n.en = knownPerm.translate.replace("{{modelName}}", "Cms" + modelName);
          for (lang in AdminAppConfig.allowedLanguages) {
            if (lang !== 'en') {
              permission.nameL10n[lang] = gettextCatalog.getString(knownPerm.translate, {
                modelName: "Cms" + modelName
              }, 'user permission');
            }
          }
          break;
        }
      }
      for (lang in AdminAppConfig.allowedLanguages) {
        if (lang !== 'en' && !permission.nameL10n[lang]) {
          permission.nameL10n[lang] = gettextCatalog.getString(permission.name, null, 'user permission');
        }
      }
      return permission;
    };
    return {
      restrict: 'E',
      require: 'ngModel',
      scope: {
        ngDisabled: '=?'
      },
      controller: ["$scope", "$attrs", "$element", "GroupPermissions", function($scope, $attrs, $element, GroupPermissions) {
        var isPermInSkipValues, ngModelCtrl, skipValues;
        ngModelCtrl = $element.controller('ngModel');
        $scope.proxy = null;
        $element.addClass('loading-data');
        skipValues = [];
        isPermInSkipValues = function(permission) {
          return skipValues.includes(permission.content_type) || skipValues.includes(permission.content_type + "." + permission.codename);
        };
        GroupPermissions.listAllPermissions(function(data) {
          var allPermissions, appName, err, item, j, len, name1, permissions;
          allPermissions = {};
          try {
            skipValues = $scope.$parent.$eval($attrs.ignorePermissions) || $attrs.ignorePermissions;
          } catch (error) {
            err = error;
            skipValues = $attrs.ignorePermissions;
          }
          skipValues = skipValues || [];
          if (angular.isString(skipValues)) {
            skipValues = skipValues.trim().split(',');
          }
          for (j = 0, len = data.length; j < len; j++) {
            item = data[j];
            if (!(!isPermInSkipValues(item))) {
              continue;
            }
            allPermissions[name1 = item.content_type] || (allPermissions[name1] = []);
            allPermissions[item.content_type].push(generateTranslationsForPermission(item));
          }
          $scope.allPermissions = [];
          for (appName in allPermissions) {
            permissions = allPermissions[appName];
            if (appName !== 'site_settings') {
              if (appName === 'drf_lbcms' || appName === 'lbcms') {
                $scope.allPermissions.push({
                  appName: appName,
                  permissions: permissions
                });
              } else {
                $scope.allPermissions.insert(0, {
                  appName: appName,
                  permissions: permissions
                });
              }
            }
          }
          $scope.allPermissions.insert(0, {
            appName: 'site_settings',
            permissions: allPermissions.site_settings
          });
          $element.addClass('ready');
        });
        ngModelCtrl.$render = function() {
          var id, j, len, ref;
          $scope.proxy = {};
          ref = ngModelCtrl.$modelValue || [];
          for (j = 0, len = ref.length; j < len; j++) {
            id = ref[j];
            $scope.proxy[id] = true;
          }
        };
        return $scope.$watch('proxy', function(proxy, oldValue) {
          var id, viewValue;
          viewValue = [];
          for (id in proxy || {}) {
            if (proxy[id]) {
              viewValue.push(parseInt(id));
            }
          }
          ngModelCtrl.$setViewValue(viewValue);
        }, true);
      }],
      template: ('/client/admin_app/_modelEditorDirectives/aGroupPermissionsEditor/aPermissionsEditorForUser.html', '<ul><li ng-repeat="conf in allPermissions"><b class="text-success">{{conf.appName}}:</b><div class="checkbox" ng-repeat="p in conf.permissions"><label><input type="checkbox" ng-model="$parent.proxy[p.id]"/><span>{{p.nameL10n | l10n}}</span><small> <b><i>({{p.codename}})</i></b></small></label></div></li></ul>' + '')
    };
  }]).directive('aGroupPermissionsEditor', function() {
    return {
      restrict: 'E',
      scope: {
        ngModel: '=',
        form: '=?',
        modal: '@?'
      },
      controller: ["$scope", "$constants", function($scope, $constants) {
        var GROUP_NAMES_BY_USER_ROLES, c, j, len, ref;
        GROUP_NAMES_BY_USER_ROLES = {};
        ref = $constants.resolve('CMS_USER.userRole');
        for (j = 0, len = ref.length; j < len; j++) {
          c = ref[j];
          GROUP_NAMES_BY_USER_ROLES["Permissions for '" + c.description + "'"] = c.value;
        }
        return $scope.isDisabledName = function() {
          if (!$scope.ngModel.id) {
            return false;
          }
          return !!GROUP_NAMES_BY_USER_ROLES[$scope.ngModel.name];
        };
      }],
      template: ('/client/admin_app/_modelEditorDirectives/aGroupPermissionsEditor/aGroupPermissionsEditor.html', '<div class="form-group"><label><span translate="">Name for group</span><span class="text-danger"> *</span></label><input class="form-control" ng-model="ngModel.name" required="" maxlength="150" ng-disabled="isDisabledName()"/></div><div class="form-group"><label translate="">Permissions</label><a-permissions-editor-for-user ng-model="ngModel.permissions"></a-permissions-editor-for-user></div><div><hr/><small class="help-block"><span class="text-danger">*</span><span> - </span><span translate="">Required fields</span></small></div>' + '')
    };
  });

}).call(this);
;
(function() {
  angular.module('AdminApp').service('aPrevNextObjectForNavbar', ["$rootScope", "$state", function($rootScope, $state) {
    var currentIndex, currentParams, currentState, ids, onStateChange, self;
    this.nextAllowed = false;
    this.prevAllowed = false;
    ids = [];
    currentIndex = -1;
    currentParams = {};
    currentState = '';
    self = this;
    onStateChange = function(event, toState, toParams, fromState, fromParams, options) {
      var id;
      currentParams = toParams;
      currentState = toState.name;
      ids = toParams.ids || "";
      ids = ids.split(',');
      id = toParams.id || "";
      currentIndex = ids.indexOf(id);
      self.nextAllowed = false;
      self.prevAllowed = false;
      self.isVisible = true;
      if (!id || !ids.length || currentIndex === -1) {
        self.isVisible = false;
        return;
      }
      if (currentIndex !== 0) {
        self.prevAllowed = true;
      }
      if (currentIndex !== ids.length - 1) {
        self.nextAllowed = true;
      }
    };
    this.next = function() {
      if (!self.nextAllowed) {
        return;
      }
      currentParams.id = ids[currentIndex + 1];
      return $state.go(currentState, currentParams, {
        reload: true
      });
    };
    this.prev = function() {
      if (!self.prevAllowed) {
        return;
      }
      currentParams.id = ids[currentIndex - 1];
      return $state.go(currentState, currentParams, {
        reload: true
      });
    };
    $rootScope.$on('$stateChangeStart', onStateChange);
    onStateChange(null, $state.current, $state.params);
    return this;
  }]).directive('aPrevObjectForNavbar', ["aPrevNextObjectForNavbar", function(aPrevNextObjectForNavbar) {
    return {
      restrict: 'A',
      replace: true,
      scope: {},
      link: function($scope, $element, $attrs) {
        $scope.attrs = $attrs;
        return $scope.aPrevNextObjectForNavbar = aPrevNextObjectForNavbar;
      },
      template: ('/client/admin_app/_navbarDirectives/aPrevNextObjectForNavbar/aPrevObjectForNavbar.html', '<li ng-class="{disabled: !aPrevNextObjectForNavbar.prevAllowed}" ng-show="aPrevNextObjectForNavbar.isVisible"><a style="cursor:pointer;" ng-disabled="attrs.disabled" ng-click="aPrevNextObjectForNavbar.prev()" title="{{\'Previous object\' | translate}}"><span class="fas fa-chevron-circle-left"></span><span class="visible-xs-inline" translate="">Previous object</span></a></li>' + '')
    };
  }]).directive('aNextObjectForNavbar', ["aPrevNextObjectForNavbar", function(aPrevNextObjectForNavbar) {
    return {
      restrict: 'A',
      replace: true,
      scope: {},
      link: function($scope, $element, $attrs) {
        $scope.attrs = $attrs;
        return $scope.aPrevNextObjectForNavbar = aPrevNextObjectForNavbar;
      },
      template: ('/client/admin_app/_navbarDirectives/aPrevNextObjectForNavbar/aNextObjectForNavbar.html', '<li ng-class="{disabled: !aPrevNextObjectForNavbar.nextAllowed}" ng-show="aPrevNextObjectForNavbar.isVisible"><a style="cursor:pointer;" ng-disabled="attrs.disabled" ng-click="aPrevNextObjectForNavbar.next()" title="{{\'Next object\' | translate}}"><span class="fas fa-chevron-circle-right"></span><span class="visible-xs-inline" translate="">Next object</span></a></li>' + '')
    };
  }]);

}).call(this);
;
(function() {
  angular.module('AdminApp').directive('aIntegration', ["$uicIntegrations", function($uicIntegrations) {
    return {
      require: 'ngModel',
      link: function(scope, elem, attr, ngModel) {
        var serviceName;
        serviceName = attr.aIntegration;
        ngModel.$parsers.unshift(function(value) {
          var valid;
          valid = $uicIntegrations.isService(serviceName, value);
          ngModel.$setValidity('aIntegration', valid);
          if (valid) {
            return value;
          } else {
            return void 0;
          }
        });
        ngModel.$formatters.unshift(function(value) {
          ngModel.$setValidity('aIntegration', $uicIntegrations.isService(serviceName, value));
          return value;
        });
      }
    };
  }]);

}).call(this);
;
(function() {
  angular.module('AdminApp').directive('aIsUrl', ["$uicIntegrations", function($uicIntegrations) {
    return {
      require: 'ngModel',
      link: function(scope, elem, attr, ngModel) {
        var R, R_FULL, isValid;
        R = /^(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/;
        R_FULL = /^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/;
        isValid = function(value) {
          if (R.test(value) || R_FULL.test(value)) {
            return true;
          }
          if (value && value[0] === '/') {
            return true;
          }
          return false;
        };
        ngModel.$parsers.unshift(function(value) {
          var valid;
          valid = isValid(value);
          ngModel.$setValidity('aIsUrl', valid);
          if (valid) {
            return value;
          } else {
            return void 0;
          }
        });
        ngModel.$formatters.unshift(function(value) {
          ngModel.$setValidity('aIsUrl', isValid(value));
          return value;
        });
      }
    };
  }]);

}).call(this);
;
(function() {
  angular.module('AdminApp').run(["$langPicker", "AdminAppConfig", function($langPicker, AdminAppConfig) {
    $langPicker.setLanguageList(AdminAppConfig.allowedLanguages);
    $langPicker.detectLanguage();
  }]).config(["$locationProvider", "$stateProvider", "$urlRouterProvider", "cfpLoadingBarProvider", "$modelsProvider", "$httpProvider", "$bulkProvider", "$injector", function($locationProvider, $stateProvider, $urlRouterProvider, cfpLoadingBarProvider, $modelsProvider, $httpProvider, $bulkProvider, $injector) {
    cfpLoadingBarProvider.includeSpinner = false;
    $modelsProvider.defaultModels = {
      CmsCategoryForArticle: {},
      CmsCategoryForNews: {},
      CmsCategoryForPage: {},
      CmsArticle: {},
      CmsUser: {},
      CmsKeyword: {},
      CmsCategoryForProduct: {},
      CmsMailNotification: {},
      CmsSmsNotification: {}
    };
    if (location.protocol === 'file://') {
      $locationProvider.hashPrefix('!');
    } else {
      $locationProvider.html5Mode(true);
    }
    $urlRouterProvider.otherwise(function($injector) {
      var $state;
      $state = $injector.get('$state');
      $state.go('app.dashboard');
    });
    $stateProvider.state('app', {
      abstract: true,
      url: '/{lang:(?:ru|en|de|cz|ua)}',
      template: '<ui-view/>'
    });
    return $bulkProvider.defaultOperations = {
      CmsCategoryForArticle: {
        find: {
          $mergeWithArray: 'items'
        }
      },
      CmsSettings: {
        findOne: true
      }
    };
  }]);

  angular.module('ui.cms').factory('redirectToLogin', ["$q", "$injector", function($q, $injector) {
    var $state, LoopBackAuth;
    LoopBackAuth = null;
    $state = null;
    return {
      responseError: function(rejection) {
        if (rejection.status === 401) {
          LoopBackAuth = LoopBackAuth || $injector.get('LoopBackAuth');
          $state = $state || $injector.get('$state');
          LoopBackAuth.clearUser();
          LoopBackAuth.clearStorage();
          if ($state.current.name === 'app.login') {
            return $q.reject(rejection);
          }
          $state.go('app.login', {
            returnTo: $state.current.name,
            returnToParams: $state.params
          });
        }
        return $q.reject(rejection);
      }
    };
  }]).config(["$httpProvider", function($httpProvider) {
    return $httpProvider.interceptors.push('redirectToLogin');
  }]);

}).call(this);
;
(function() {
  angular.module('AdminApp').controller('DashboardView', ["$scope", function($scope) {}]).config(["$stateProvider", "$bulkProvider", "$injector", function($stateProvider, $bulkProvider, $injector) {
    var bulk_operations, k, v;
    bulk_operations = {
      CmsArticle: {
        count: true
      },
      CmsUser: {
        count: true
      },
      CmsCategoryForArticle: {
        count: true
      },
      CmsKeyword: {
        count: true
      },
      CmsSimpleProduct: {
        count: true
      },
      CmsPictureAlbum: {
        count: true
      },
      CmsOrder: {
        count: {
          where: {
            status: 'new'
          }
        }
      }
    };
    for (k in bulk_operations) {
      v = bulk_operations[k];
      if (!$injector.has(k + 'Provider')) {
        delete bulk_operations[k];
      }
    }
    return $stateProvider.state('app.dashboard', {
      url: '/dashboard?filter',
      controller: 'DashboardView',
      template: ('/client/admin_app/dashboard/dashboardView.html', '<div id="DashboardView"><a-dashboard-admin></a-dashboard-admin></div>' + ''),
      $pageTitle: 'Dashboard',
      $bulk: function(defaults) {
        return bulk_operations;
      }
    });
  }]);

}).call(this);
;
(function() {
  angular.module('AdminApp').controller('GlossaryView', ["$scope", "$state", "$stateParams", "$timeout", "$cms", "$currentUser", "AdminAppConfig", function($scope, $state, $stateParams, $timeout, $cms, $currentUser, AdminAppConfig) {
    var card, cardFooter, checkPermission, getHref, href, i, keys, len, ref;
    $scope.stateParams = angular.copy($stateParams);
    $scope.stateParams.returnTo = $state.current.name;
    keys = Object.keys($stateParams);
    if (keys.length > 0 && !(keys.length === 1 && keys[0] === 'lang')) {
      $scope.stateParams.returnToParams = JSON.stringify($stateParams);
    }
    checkPermission = function(permissionString) {
      if (angular.isUndefined(permissionString)) {
        return true;
      }
      return $currentUser.resolvePermissionsString(permissionString);
    };
    getHref = function(href) {
      if (!href) {
        return;
      }
      if (href.indexOf('/') !== 0) {
        return "ui-sref='" + href + "(stateParams)'";
      }
      if (href.indexOf('?') !== 0) {
        href += '?';
      } else {
        href += '&';
      }
      href = href + ("returnTo=" + $state.current.name);
      if (!angular.isEmpty($stateParams)) {
        href += "&returnToParams=" + (encodeURIComponent(JSON.stringify($stateParams)));
      }
      return "href='" + href + "'";
    };
    $scope.glossaryHtml = "";
    ref = AdminAppConfig.glossaryCards;
    for (i = 0, len = ref.length; i < len; i++) {
      card = ref[i];
      if (!(checkPermission(card.uicIfOnCurrentUserPermission))) {
        continue;
      }
      cardFooter = "";
      if (checkPermission(card.addHrefCurrentUserPermission)) {
        href = getHref(card.addHref);
        if (href) {
          cardFooter += "<li class='text-left'>\n    <a class=\"btn btn-" + card.cssClass + " btn-sm\" " + href + ">\n        <span class='fas fa-plus'></span>\n        <span translate=''>Add</span>\n    </a>\n</li>";
        }
      }
      if (checkPermission(card.listHrefCurrentUserPermission)) {
        href = getHref(card.listHref);
        if (href) {
          cardFooter += "<li class='text-right'>\n    <a class=\"btn btn-" + card.cssClass + " btn-sm\" " + href + ">\n        <span class='fas fa-pencil-alt'></span>\n        <span translate='' translate-context='View items'>View</span>\n    </a>\n</li>";
        }
      }
      if (!cardFooter) {
        continue;
      }
      cardFooter = "<ul class=\"list-table nomargin nopadding\">" + cardFooter + "</ul>";
      $scope.glossaryHtml += "<div class=\"col-md-50 col-lg-25\">\n  <div class=\"panel panel-" + card.cssClass + "\">\n    <div class=\"panel-heading\">\n      <div class=\"panel-title\" translate=\"\">" + card.name + "</div>\n    </div>\n    <div class=\"panel-body text-center\">\n      <div class=\"" + card.iconClass + " fa-5x\"></div>\n    </div>\n    <div class=\"panel-footer\">\n        " + cardFooter + "\n    </div>\n  </div>\n</div>";
    }
    if (!$scope.glossaryHtml) {
      $cms.showNotification("There is nothing in here", null, 'error');
      $state.go('app.dashboard');
    }
  }]).config(["$stateProvider", function($stateProvider) {
    return $stateProvider.state('app.glossary', {
      url: '/glossary',
      controller: 'GlossaryView',
      template: ('/client/admin_app/glossary/View.html', '<div id="GlossaryView"><div class="row" uic-bind-html="glossaryHtml"></div></div>' + ''),
      $pageTitle: 'Glossary',
      $bulk: function(defaults, toParams, $models, bulk) {}
    });
  }]);

}).call(this);
;
(function() {
  angular.module('AdminApp').controller('LoginView', ["$scope", "$cms", "$state", "$stateParams", "AdminAppConfig", "LoopBackAuth", "$currentUser", function($scope, $cms, $state, $stateParams, AdminAppConfig, LoopBackAuth, $currentUser) {
    $scope.AdminAppConfig = AdminAppConfig;
    LoopBackAuth.clearUser();
    LoopBackAuth.clearStorage();
    return $scope.onLogin = function() {
      if (!$currentUser.resolvePermissionsString('site_settings.view_adminpage')) {
        $cms.showNotification("<b>{{username}}</b>, уou are not allowed to access this page", {
          username: $currentUser.username
        }, 'error');
        LoopBackAuth.clearUser();
        return;
      }
      $cms.loadSettings();
      if ($stateParams.returnTo && $stateParams.returnTo !== 'app.login') {
        return $state.go($stateParams.returnTo, $stateParams.returnToParams || {});
      }
      return $state.go('app.dashboard');
    };

    /*
    $scope.$on '$locationChangeSuccess', ()->
        return
     */

    /*
    .run ($rootScope, $state, $currentUser)->
        $rootScope.$on '$stateChangeStart', (event, toState, toStateParams)->
            if $currentUser.id then return
            console.log event, toState, toStateParams
            if !$currentUser.id and toState.name != 'app.login'
                 * отключаем пользователю возможность свалить назад по истории
                debugger
                $state.go('app.login', {
                    returnTo: toState.name
                    returnToParams: toStateParams
                })
            return
     */
  }]).config(["$stateProvider", function($stateProvider) {
    return $stateProvider.state('app.login', {
      url: '/login?returnTo&returnToParams',
      controller: 'LoginView',
      template: ('/client/admin_app/login/loginView.html', '<div id="LoginView"><div class="clearfix"><div class="col-lg-offset-35 col-lg-30 panel login-form"><div class="h2-container"><h2 ng-switch="!!AdminAppConfig.loginPageTitle"><span translate="" ng-switch-when="false">CMS Admin</span><span ng-switch-when="true">{{AdminAppConfig.loginPageTitle | l10n | translate}}</span></h2></div><uice-login-form on-login="onLogin()" show-remember-me="true" show-reset-password="AdminAppConfig.showResetPasswordOnLoginPage"></uice-login-form></div></div></div>' + ''),
      $pageTitle: 'Login'
    });
  }]);

}).call(this);
;
(function() {
  angular.module('AdminApp').controller('MainPageView', ["$scope", "$stateParams", "$cms", "$models", "CmsSettings", function($scope, $stateParams, $cms, $models, CmsSettings) {
    $scope.$models = $models;
    $scope.item = {};
    $scope.options = {
      lang: CMS_DEFAULT_LANG
    };
    CmsSettings.findOne(function(data) {
      $scope.item = data;
    });
    return $scope.save = function() {
      CmsSettings.findOne(function(data) {
        data.publicContacts = $scope.item.publicContacts;
        data.viewsData = $scope.item.viewsData;
        return CmsSettings.updateAttributes({
          id: data.id
        }, data, function(data) {
          $cms.loadSettings(data);
          $cms.showNotification("Main page updated");
        }, function(data) {
          $cms.showNotification("Error. The main page cannot be saved. Try again later", null, 'error');
        });
      });
    };
  }]).config(["$stateProvider", function($stateProvider) {
    return $stateProvider.state('app.mainPage', {
      url: '/mainPage',
      controller: 'MainPageView',
      template: ('/client/admin_app/mainPage/View.html', '<fieldset id="MainPageView" uic-disabled-on-current-user-permission="!site_settings.change_CmsSettings"><uice-tabset class="sm" dropdown-btn-class="info"><uice-tabset-tab heading="{{\'Slider\' | translate}}"><uic-lang-picker class="pull-right" ng-model="options.lang"></uic-lang-picker><!--a-carousel-editor(ng-model="item.viewsData.index.carousel",\ncms-model-item="item", lang="options.lang")--><h3>Carousel</h3><lw-carousel ng-model="item.viewsData.index.carousel" cms-model-item="item" type="image+html"></lw-carousel><div style="Height:1em"></div><a-carousel-editor ng-model="item.viewsData.index.carousel" cms-model-item="item" lang="options.lang" type="image+html"></a-carousel-editor></uice-tabset-tab><uice-tabset-tab heading="{{\'Popular items\' | translate}}"><uic-lang-picker class="pull-right" ng-model="options.lang"></uic-lang-picker><h4 class="text-primary" translate="">Popular items</h4><lw-cards ng-model="item.viewsData.index.popularItems"><div class="card">Card {{$item}}</div></lw-cards><hr/><a-cards-editor ng-model="item.viewsData.index.popularItems" cms-model-item="item" lang="options.lang" type="icon+subTitle,link,price,text"></a-cards-editor><!--)--></uice-tabset-tab><uice-tabset-tab heading="{{\'Featured items\' | translate}}"><uic-lang-picker class="pull-right" ng-model="options.lang"></uic-lang-picker><h4 class="text-primary" translate="">Featured items</h4><a-cards-editor ng-model="item.viewsData.index.featuredItems" cms-model-item="item" lang="options.lang" options="featuredEditorOptions"></a-cards-editor></uice-tabset-tab><uice-tabset-tab heading="{{\'Special offer\' | translate}}"><uic-lang-picker class="pull-right" ng-model="options.lang"></uic-lang-picker><h4 class="text-primary" translate="">Special offer</h4><div class="form-group"><uic-html-editor ng-model="item.viewsData.index.specialOffer.text[options.lang]" toolbar="basic"></uic-html-editor></div><div class="form-group"><label translate="translate">Link</label><input class="form-control" ng-model="item.viewsData.index.specialOffer.link" type="url"/></div></uice-tabset-tab></uice-tabset></fieldset><a-footer-toolbar on-save="save()" save-disabled="form.$invalid" current-user-permission-update="site_settings.change_CmsSettings"></a-footer-toolbar>' + ''),
      $pageTitle: 'Main Page'
    });
  }]);

}).call(this);
;
(function() {
  angular.module('AdminApp').controller('AdArticlesListView', ["$scope", "$controller", "$stateParams", "AdminAppConfig", function($scope, $controller, $stateParams, AdminAppConfig) {
    $scope.MODEL_NAME = 'CmsArticle';
    $scope.AdminAppConfig = AdminAppConfig;
    $scope.queryFilter || ($scope.queryFilter = {
      fields: {
        title: true,
        isPublished: true,
        created: true,
        publicationDate: true,
        mainImg: true,
        id: true
      },
      where: {}
    });
    if ($stateParams.category && $stateParams.category !== 'null') {
      $scope.queryFilter.where = {};
      $scope.queryFilter.where.category = $stateParams.category;
    }
    $controller('AdBaseListView', {
      $scope: $scope
    });
  }]).controller('AdArticlesEditView', ["$scope", "$controller", function($scope, $controller) {
    $scope.MODEL_NAME = 'CmsArticle';
    $scope.afterLoadData = function(item) {
      item.data = item.data || {};
      if (!item.data.gdprWidget) {
        item.data.gdprWidget = null;
      }
      return item;
    };
    return $controller('AdBaseEditView', {
      $scope: $scope
    });
  }]).config(["$injector", "$stateProvider", "$tableViewProvider", function($injector, $stateProvider, $tableViewProvider) {
    $tableViewProvider.setColumnsConfig('CmsArticle', {
      lg: {
        render: 'table',
        sortField: '-created',
        itemsPerPage: 25,
        columns: ['state', 'title', 'publicationDate', 'created']
      },
      sm: {
        render: 'list',
        sortField: '-created',
        itemsPerPage: 15,
        columns: {
          iconType: 'placeholder',
          iconClass: 'bg-primary',
          iconTemplate: '<span class="fas fa-font placeholder"></span>',
          cellTemplate: "<h4>{{$item.title | l10n}}</h4>"
        }
      }
    });
    return $stateProvider.state('app.modelEditors.CmsArticle', {
      url: '/articles',
      abstract: true,
      template: '<ui-view class="ui-view-animation"/>'
    }).state('app.modelEditors.CmsArticle.list', {
      url: "/?category&filter&page&sort",
      controller: 'AdArticlesListView',
      template: ('/client/admin_app/modelEditors/articles/ListView.html', '<a-table-toolbar force-lang="forceLang" search-term="itemFilter.searchTerm" options="options.toolbarOptions" model-name="{{:: MODEL_NAME}}" current-user-permission-create="{{:: DJANGO_APP_NAME}}.add_{{:: MODEL_NAME}}"><a class="btn btn-default" ng-show="AdminAppConfig.visibleModels.CmsCategoryForArticle" ui-sref="app.modelEditors.CmsCategoryForArticle.list" uic-if-on-current-user-permission="*.view_CmsCategoryForArticle"><span class="fas fa-list"></span><span translate="">Categories</span></a></a-table-toolbar><a-view-title></a-view-title><uice-table-view ng-if="ready" items="items" search-term="itemFilter.searchTerm" columns="{{:: MODEL_NAME}}" force-lang="forceLang" on-item-click="onItemEdit(item, ids)"></uice-table-view>' + ''),
      $pageTitle: 'Articles',
      $bulk: function() {
        if (!$injector.has('CmsCategoryForArticle')) {
          return;
        }
        return {
          CmsCategoryForArticle: {
            list: {
              filter: {
                fields: {
                  id: true,
                  title: true,
                  identifier: true
                }
              },
              $mergeWithArray: 'items'
            }
          }
        };
      }
    }).state('app.modelEditors.CmsArticle.edit', {
      url: '/edit/:id?ids&returnTo&returnToParams',
      controller: 'AdArticlesEditView',
      template: ('/client/admin_app/modelEditors/articles/EditView.html', '<a-view-title></a-view-title><fieldset uic-disabled-on-current-user-permission="! {{:: DJANGO_APP_NAME}}.change_{{:: MODEL_NAME}}"><form name="form"><a-cms-article-editor ng-model="item" form="form" force-lang="forceLang"></a-cms-article-editor></form></fieldset><a-footer-toolbar item="item" on-save="save()" save-disabled="form.$invalid" on-delete="delete()" delete-disabled="!item.id" current-user-permission-create="{{:: DJANGO_APP_NAME}}.add_{{:: MODEL_NAME}}" current-user-permission-update="{{:: DJANGO_APP_NAME}}.change_{{:: MODEL_NAME}}" current-user-permission-delete="{{:: DJANGO_APP_NAME}}.delete_{{:: MODEL_NAME}}"></a-footer-toolbar>' + ''),
      $pageTitle: 'Edit article'
    }).state('app.modelEditors.CmsArticle.add', {
      url: '/add/-?returnTo&returnToParams',
      controller: 'AdArticlesEditView',
      template: ('/client/admin_app/modelEditors/articles/EditView.html', '<a-view-title></a-view-title><fieldset uic-disabled-on-current-user-permission="! {{:: DJANGO_APP_NAME}}.change_{{:: MODEL_NAME}}"><form name="form"><a-cms-article-editor ng-model="item" form="form" force-lang="forceLang"></a-cms-article-editor></form></fieldset><a-footer-toolbar item="item" on-save="save()" save-disabled="form.$invalid" on-delete="delete()" delete-disabled="!item.id" current-user-permission-create="{{:: DJANGO_APP_NAME}}.add_{{:: MODEL_NAME}}" current-user-permission-update="{{:: DJANGO_APP_NAME}}.change_{{:: MODEL_NAME}}" current-user-permission-delete="{{:: DJANGO_APP_NAME}}.delete_{{:: MODEL_NAME}}"></a-footer-toolbar>' + ''),
      $pageTitle: 'Add article'
    });
  }]);

}).call(this);
;
(function() {
  angular.module('AdminApp').controller('CategoryForArticlesListView', ["$scope", "$controller", function($scope, $controller) {
    $scope.MODEL_NAME = 'CmsCategoryForArticle';
    $scope.USE_SERVER_PAGINATION = true;
    $scope.queryFilter = {
      fields: {
        id: true,
        identifier: true,
        isPublished: true,
        title: true,
        publicationDate: true
      },
      where: {}
    };
    return $controller('AdBaseListView', {
      $scope: $scope
    });
  }]).controller('CategoryForArticlesEditView', ["$scope", "$controller", function($scope, $controller) {
    $scope.MODEL_NAME = 'CmsCategoryForArticle';
    return $controller('AdBaseEditView', {
      $scope: $scope
    });
  }]).config(["$stateProvider", "$tableViewProvider", function($stateProvider, $tableViewProvider) {
    $tableViewProvider.setColumnsConfig('CmsCategoryForArticle', {
      lg: {
        render: 'table',
        sortField: '-publicationDate',
        itemsPerPage: 25,
        columns: [
          'state', {
            headerName: 'Identifier',
            field: 'identifier'
          }, 'title', 'publicationDate'
        ]
      },
      sm: {
        render: 'list',
        sortField: '-id',
        itemsPerPage: 15,
        columns: {
          iconType: 'placeholder',
          iconClass: 'bg-primary',
          iconTemplate: '<span class="fas fa-list-ul placeholder"></span>',
          cellTemplate: "<h4>{{$item.identifier}}: {{$item.title | l10n}}</h4>"
        }
      }
    });
    return $stateProvider.state('app.modelEditors.CmsCategoryForArticle', {
      url: '/categoryForArticles',
      abstract: true,
      template: '<ui-view class="ui-view-animation"/>'
    }).state('app.modelEditors.CmsCategoryForArticle.list', {
      url: '/?page&filter&sort',
      controller: 'CategoryForArticlesListView',
      template: ('/client/admin_app/modelEditors/categoryForArticles/ListView.html', '<div id="CategoryForArticlesListView"><a-table-toolbar force-lang="forceLang" search-term="itemFilter.searchTerm" options="options.toolbarOptions" model-name="{{:: MODEL_NAME}}" current-user-permission-create="{{:: DJANGO_APP_NAME}}.add_{{:: MODEL_NAME}}"></a-table-toolbar><a-view-title></a-view-title><div ng-if="ready"><uice-table-view items="items" search-term="itemFilter.searchTerm" columns="CmsCategoryForArticle" force-lang="forceLang" on-item-click="onItemEdit(item)" query-filter="queryFilter" load-items-fn="loadData()" count-items-fn="countData()"></uice-table-view></div></div>' + ''),
      $pageTitle: 'Article categories',
      $bulk: function(defaults, toParams, $models, bulk) {}
    }).state('app.modelEditors.CmsCategoryForArticle.edit', {
      url: '/edit/:id?ids&returnTo&returnToParams',
      controller: 'CategoryForArticlesEditView',
      template: ('/client/admin_app/modelEditors/categoryForArticles/EditView.html', '<div id="CategoryForArticlesEditView"><a-view-title></a-view-title><fieldset uic-disabled-on-current-user-permission="! {{:: DJANGO_APP_NAME}}.change_{{:: MODEL_NAME}}"><form name="form"><a-cms-category-for-article-editor ng-model="item" force-lang="forceLang"></a-cms-category-for-article-editor></form></fieldset><a-footer-toolbar item="item" on-save="save()" save-disabled="form.$invalid" on-delete="delete()" delete-disabled="!item.id || item.identifier == \'legal-information\'" current-user-permission-create="{{:: DJANGO_APP_NAME}}.add_{{:: MODEL_NAME}}" current-user-permission-update="{{:: DJANGO_APP_NAME}}.change_{{:: MODEL_NAME}}" current-user-permission-delete="{{:: DJANGO_APP_NAME}}.delete_{{:: MODEL_NAME}}"></a-footer-toolbar></div>' + ''),
      $pageTitle: 'Edit category for article',
      $bulk: function(defaults, toParams, $models, bulk) {}
    }).state('app.modelEditors.CmsCategoryForArticle.add', {
      url: '/add/-?returnTo&returnToParams',
      controller: 'CategoryForArticlesEditView',
      template: ('/client/admin_app/modelEditors/categoryForArticles/EditView.html', '<div id="CategoryForArticlesEditView"><a-view-title></a-view-title><fieldset uic-disabled-on-current-user-permission="! {{:: DJANGO_APP_NAME}}.change_{{:: MODEL_NAME}}"><form name="form"><a-cms-category-for-article-editor ng-model="item" force-lang="forceLang"></a-cms-category-for-article-editor></form></fieldset><a-footer-toolbar item="item" on-save="save()" save-disabled="form.$invalid" on-delete="delete()" delete-disabled="!item.id || item.identifier == \'legal-information\'" current-user-permission-create="{{:: DJANGO_APP_NAME}}.add_{{:: MODEL_NAME}}" current-user-permission-update="{{:: DJANGO_APP_NAME}}.change_{{:: MODEL_NAME}}" current-user-permission-delete="{{:: DJANGO_APP_NAME}}.delete_{{:: MODEL_NAME}}"></a-footer-toolbar></div>' + ''),
      $pageTitle: 'Add category for article',
      $bulk: function(defaults, toParams, $models, bulk) {}
    });
  }]);

}).call(this);
;
(function() {
  angular.module('AdminApp').controller('CategoryForPictureAlbumsListView', ["$scope", "$controller", function($scope, $controller) {
    $scope.MODEL_NAME = 'CmsCategoryForPictureAlbum';
    $scope.USE_SERVER_PAGINATION = true;

    /*
    $scope.queryFilter = {
        fields: {}
        where: {}
    }
     */
    return $controller('AdBaseListView', {
      $scope: $scope
    });
  }]).controller('CategoryForPictureAlbumsEditView', ["$scope", "$controller", function($scope, $controller) {
    $scope.MODEL_NAME = 'CmsCategoryForPictureAlbum';
    return $controller('AdBaseEditView', {
      $scope: $scope
    });
  }]).config(["$stateProvider", "$tableViewProvider", function($stateProvider, $tableViewProvider) {
    $tableViewProvider.setColumnsConfig('CmsCategoryForPictureAlbum', {
      lg: {
        render: 'table',
        sortField: '-state',
        itemsPerPage: 25,
        columns: [
          'state', {
            headerName: 'Identifier',
            field: 'identifier'
          }, 'title', 'publicationDate'
        ]
      },
      sm: {
        render: 'list',
        sortField: '-id',
        itemsPerPage: 15,
        columns: {
          iconType: 'placeholder',
          iconClass: 'bg-primary',
          iconTemplate: '<span class="fas fa-list-ul placeholder"></span>',
          cellTemplate: "<h4>{{$item.identifier}}: {{$item.title | l10n}}</h4>"
        }
      }
    });
    return $stateProvider.state('app.modelEditors.CmsCategoryForPictureAlbum', {
      url: '/categoryForPictureAlbums',
      abstract: true,
      template: '<ui-view class="ui-view-animation"/>'
    }).state('app.modelEditors.CmsCategoryForPictureAlbum.list', {
      url: '/?page&filter&sort',
      controller: 'CategoryForPictureAlbumsListView',
      template: ('/client/admin_app/modelEditors/categoryForPictureAlbums/ListView.html', '<div id="CategoryForPictureAlbumsListView"><a-table-toolbar force-lang="forceLang" search-term="itemFilter.searchTerm" options="options.toolbarOptions" model-name="{{:: MODEL_NAME}}" current-user-permission-create="{{:: DJANGO_APP_NAME}}.add_{{:: MODEL_NAME}}"></a-table-toolbar><a-view-title></a-view-title><div ng-if="ready"><uice-table-view items="items" search-term="itemFilter.searchTerm" columns="CmsCategoryForPictureAlbum" force-lang="forceLang" on-item-click="onItemEdit(item)" query-filter="queryFilter" load-items-fn="loadData()" count-items-fn="countData()"></uice-table-view></div></div>' + ''),
      $pageTitle: 'Album categories',
      $bulk: function(defaults, toParams, $models, bulk) {}
    }).state('app.modelEditors.CmsCategoryForPictureAlbum.edit', {
      url: '/edit/:id?ids&returnTo&returnToParams',
      controller: 'CategoryForPictureAlbumsEditView',
      template: ('/client/admin_app/modelEditors/categoryForPictureAlbums/EditView.html', '<div id="CategoryForPictureAlbumsEditView"><a-view-title></a-view-title><fieldset uic-disabled-on-current-user-permission="! {{:: DJANGO_APP_NAME}}.change_{{:: MODEL_NAME}}"><form name="form"><a-cms-category-for-picture-album-editor ng-model="item" force-lang="forceLang"></a-cms-category-for-picture-album-editor></form></fieldset><a-footer-toolbar item="item" on-save="save()" save-disabled="form.$invalid" on-delete="delete()" delete-disabled="!item.id" current-user-permission-create="{{:: DJANGO_APP_NAME}}.add_{{:: MODEL_NAME}}" current-user-permission-update="{{:: DJANGO_APP_NAME}}.change_{{:: MODEL_NAME}}" current-user-permission-delete="{{:: DJANGO_APP_NAME}}.delete_{{:: MODEL_NAME}}"></a-footer-toolbar></div>' + ''),
      $pageTitle: 'Edit category for album',
      $bulk: function(defaults, toParams, $models, bulk) {}
    }).state('app.modelEditors.CmsCategoryForPictureAlbum.add', {
      url: '/add/-?returnTo&returnToParams',
      controller: 'CategoryForPictureAlbumsEditView',
      template: ('/client/admin_app/modelEditors/categoryForPictureAlbums/EditView.html', '<div id="CategoryForPictureAlbumsEditView"><a-view-title></a-view-title><fieldset uic-disabled-on-current-user-permission="! {{:: DJANGO_APP_NAME}}.change_{{:: MODEL_NAME}}"><form name="form"><a-cms-category-for-picture-album-editor ng-model="item" force-lang="forceLang"></a-cms-category-for-picture-album-editor></form></fieldset><a-footer-toolbar item="item" on-save="save()" save-disabled="form.$invalid" on-delete="delete()" delete-disabled="!item.id" current-user-permission-create="{{:: DJANGO_APP_NAME}}.add_{{:: MODEL_NAME}}" current-user-permission-update="{{:: DJANGO_APP_NAME}}.change_{{:: MODEL_NAME}}" current-user-permission-delete="{{:: DJANGO_APP_NAME}}.delete_{{:: MODEL_NAME}}"></a-footer-toolbar></div>' + ''),
      $pageTitle: 'Add category for album',
      $bulk: function(defaults, toParams, $models, bulk) {}
    });
  }]);

}).call(this);
;
(function() {
  angular.module('AdminApp').controller('GdprRequestsListView', ["$scope", "$controller", "$state", "$stateParams", function($scope, $controller, $state, $stateParams) {
    $scope.MODEL_NAME = 'CmsGdprRequest';

    /*
    $scope.queryFilter = {
        fields: {}
        where: {}
    }
     */
    return $controller('AdBaseListView', {
      $scope: $scope
    });
  }]).controller('GdprRequestsEditView', ["$scope", "$controller", "$state", "lwGdprRequestModal", function($scope, $controller, $state, lwGdprRequestModal) {
    $scope.MODEL_NAME = 'CmsGdprRequest';
    $scope.MODEL_DEFAULT_ITEM = {};
    $controller('AdBaseEditView', {
      $scope: $scope
    });
    return $scope.runAction = function(responseType) {
      lwGdprRequestModal.open($scope.item, responseType).then(function(data) {
        return $scope.item = data;
      });
    };
  }]).config(["$stateProvider", "$tableViewProvider", function($stateProvider, $tableViewProvider) {
    $tableViewProvider.setColumnsConfig('CmsGdprRequest', {
      lg: {
        render: 'table',
        sortField: '-id',
        itemsPerPage: 25,
        columns: [
          'id', 'created', {
            headerName: 'Type',
            field: 'type',
            cellTemplate: "{{$item.type | constantToText:'CMS_GDPR_REQUEST.type'}}"
          }, {
            headerName: 'Status',
            field: 'status',
            cellTemplate: "{{$item.status | constantToText:'CMS_GDPR_REQUEST.status'}}"
          }, {
            headerName: 'Email',
            field: 'emails',
            cellTemplate: "<span ng-repeat=\"e in $item.emails\">{{e}}<span ng-show=\"!$last\">, </span></span>"
          }
        ]
      },
      sm: {
        render: 'list',
        sortField: '-id',
        itemsPerPage: 15,
        columns: {
          iconType: 'placeholder',
          iconClass: 'bg-primary',
          iconTemplate: '<span class="fas fa-unlock-alt placeholder"></span>',
          cellTemplate: "<h4>{{$item.type | constantToText:'CMS_GDPR_REQUEST.type'}}</h4>\n<h5><span translate>Status:</span> {{$item.status | constantToText:'CMS_GDPR_REQUEST.status'}}</h5>"
        }
      }
    });
    return $stateProvider.state('app.modelEditors.CmsGdprRequest', {
      url: '/gdprRequests',
      abstract: true,
      template: '<ui-view class="ui-view-animation"/>'
    }).state('app.modelEditors.CmsGdprRequest.list', {
      url: '/?page&filter',
      controller: 'GdprRequestsListView',
      template: ('/client/admin_app/modelEditors/gdprRequests/ListView.html', '<div id="GdprRequestsListView"><a-table-toolbar force-lang="forceLang" search-term="itemFilter.searchTerm" options="{hideLangPicker: true}"></a-table-toolbar><a-view-title></a-view-title><div ng-if="ready &amp;&amp; !loading"><uice-table-view items="items" search-term="itemFilter.searchTerm" columns="CmsGdprRequest" force-lang="forceLang" on-item-click="onItemEdit(item)"></uice-table-view></div></div>' + ''),
      $pageTitle: 'GDPR requests',
      $bulk: function(defaults, toParams, $models, bulk) {}
    }).state('app.modelEditors.CmsGdprRequest.edit', {
      url: '/edit/:id?returnTo&returnToParams',
      controller: 'GdprRequestsEditView',
      template: ('/client/admin_app/modelEditors/gdprRequests/EditView.html', '<a-view-title></a-view-title><fieldset uic-disabled-on-current-user-permission="! {{:: DJANGO_APP_NAME}}.change_{{:: MODEL_NAME}}"><form class="col-md-60 col-md-offset-20 nopadding" name="form"><uib-tabset active="activeTab"><uib-tab heading="{{\'Main\' | translate}}"><div class="form-group nomargin"><label translate="">Created:</label><span> {{item.created | date: \'short\'}}</span></div><div class="form-group nomargin"><label translate="">Type:</label><span> {{item.type | constantToText: \'CMS_GDPR_REQUEST.type\'}}</span></div><div class="form-group nomargin"><label translate="">Status:</label><span> {{item.status | constantToText: \'CMS_GDPR_REQUEST.status\'}}</span></div><div class="form-group nomargin"><label translate="">Emails:</label><span> </span><span ng-repeat="e in item.emails"> {{e}}<span ng-show="!$last">, </span></span></div><div class="form-group nomargin" ng-show="item.phones.length"><label translate="">Phones:</label><span> </span><span ng-repeat="e in item.phones"> {{e}}<span ng-show="!$last">, </span></span></div><div class="form-group nomargin"><label translate="">Preferred language:</label><span> {{item.preferredLang}}</span></div><div class="form-group"><label translate="">Available actions for user data:</label><div></div><div class="btn-group"><button class="btn btn-sm btn-info" ng-click="runAction(\'info\')"><span class="fas fa-info"></span><span translate="">Collect information</span></button><button class="btn btn-sm btn-default" ng-click="runAction(\'anonimization\')"><span class="fas fa-lock"></span><span translate="">Anonimize data</span></button><button class="btn btn-sm btn-danger" ng-click="runAction(\'deletion\')"><span class="fas fa-trash-alt"></span><span translate="">Delete data from database</span></button></div></div></uib-tab></uib-tabset></form></fieldset><a-footer-toolbar item="item" on-save="save()" save-disabled="form.$invalid" on-delete="delete()" delete-disabled="!item.id" current-user-permission-create="{{:: DJANGO_APP_NAME}}.add_{{:: MODEL_NAME}}" current-user-permission-update="{{:: DJANGO_APP_NAME}}.change_{{:: MODEL_NAME}}" current-user-permission-delete="{{:: DJANGO_APP_NAME}}.delete_{{:: MODEL_NAME}}"></a-footer-toolbar>' + ''),
      $pageTitle: 'Edit GDPR request',
      $bulk: function(defaults, toParams, $models, bulk) {}
    });
  }]);

}).call(this);
;
(function() {
  angular.module('AdminApp').controller('AdPermissionGroupsListView', ["$scope", "$controller", "$stateParams", function($scope, $controller, $stateParams) {
    $scope.MODEL_NAME = 'GroupPermissions';
    $controller('AdBaseListView', {
      $scope: $scope
    });
  }]).controller('AdPermissionGroupsEditView', ["$scope", "$controller", function($scope, $controller) {
    $scope.MODEL_NAME = 'GroupPermissions';
    $scope.MODEL_DEFAULT_ITEM || ($scope.MODEL_DEFAULT_ITEM = {
      name: '',
      permissions: []
    });
    $controller('AdBaseEditView', {
      $scope: $scope
    });
  }]).config(["$injector", "$stateProvider", "$tableViewProvider", function($injector, $stateProvider, $tableViewProvider) {
    $tableViewProvider.setColumnsConfig('GroupPermissions', {
      lg: {
        render: 'table',
        sortField: '-created',
        itemsPerPage: 25,
        columns: ['name']
      },
      sm: {
        render: 'list',
        sortField: '-created',
        itemsPerPage: 15,
        columns: {
          iconType: 'placeholder',
          iconClass: 'bg-primary',
          iconTemplate: '<span class="fas fa-object-group placeholder"></span>',
          cellTemplate: "<h4>{{$item.name}}</h4>"
        }
      }
    });
    return $stateProvider.state('app.modelEditors.GroupPermissions', {
      url: '/groupPermissions',
      abstract: true,
      template: '<ui-view class="ui-view-animation"/>'
    }).state('app.modelEditors.GroupPermissions.list', {
      url: "/?filter&page&sort",
      controller: 'AdPermissionGroupsListView',
      template: ('/client/admin_app/modelEditors/groupPermissions/ListView.html', '<a-table-toolbar force-lang="forceLang" search-term="itemFilter.searchTerm" options="{hideLangPicker: true}" model-name="{{:: MODEL_NAME}}" current-user-permission-create="isAdmin()"></a-table-toolbar><a-view-title></a-view-title><uice-table-view ng-if="ready" items="items" search-term="itemFilter.searchTerm" columns="{{:: MODEL_NAME}}" force-lang="forceLang" on-item-click="onItemEdit(item, ids)"></uice-table-view>' + ''),
      $pageTitle: 'Group permissions'
    }).state('app.modelEditors.GroupPermissions.edit', {
      url: '/edit/:id?ids&returnTo&returnToParams',
      controller: 'AdPermissionGroupsEditView',
      template: ('/client/admin_app/modelEditors/groupPermissions/EditView.html', '<a-view-title></a-view-title><fieldset uic-disabled-on-current-user-permission="!isAdmin()"><form class="col-md-60 col-md-offset-20 nopadding" name="form"><uib-tabset active="activeTab"><uib-tab index="0" heading="{{\'Group settings\' | translate}}"><a-group-permissions-editor ng-model="item"></a-group-permissions-editor></uib-tab></uib-tabset></form></fieldset><a-footer-toolbar item="item" on-save="save()" save-disabled="form.$invalid" on-delete="delete()" delete-disabled="!item.id" current-user-permission-create="isAdmin()" current-user-permission-update="isAdmin()" current-user-permission-delete="isAdmin()"></a-footer-toolbar>' + ''),
      $pageTitle: 'Edit group'
    }).state('app.modelEditors.GroupPermissions.add', {
      url: '/add/-?returnTo&returnToParams',
      controller: 'AdPermissionGroupsEditView',
      template: ('/client/admin_app/modelEditors/groupPermissions/EditView.html', '<a-view-title></a-view-title><fieldset uic-disabled-on-current-user-permission="!isAdmin()"><form class="col-md-60 col-md-offset-20 nopadding" name="form"><uib-tabset active="activeTab"><uib-tab index="0" heading="{{\'Group settings\' | translate}}"><a-group-permissions-editor ng-model="item"></a-group-permissions-editor></uib-tab></uib-tabset></form></fieldset><a-footer-toolbar item="item" on-save="save()" save-disabled="form.$invalid" on-delete="delete()" delete-disabled="!item.id" current-user-permission-create="isAdmin()" current-user-permission-update="isAdmin()" current-user-permission-delete="isAdmin()"></a-footer-toolbar>' + ''),
      $pageTitle: 'Add group'
    });
  }]);

}).call(this);
;
(function() {
  angular.module('AdminApp').controller('AdKeywordsListView', ["$scope", "$controller", function($scope, $controller) {
    $scope.MODEL_NAME = 'CmsKeyword';
    $scope.queryFilter = {
      fields: {
        title: true,
        isPublished: true,
        id: true
      }
    };
    return $controller('AdBaseListView', {
      $scope: $scope
    });
  }]).controller('AdKeywordsEditView', ["$scope", "$controller", function($scope, $controller) {
    $scope.MODEL_NAME = 'CmsKeyword';
    return $controller('AdBaseEditView', {
      $scope: $scope
    });
  }]).config(["$stateProvider", "$tableViewProvider", function($stateProvider, $tableViewProvider) {
    $tableViewProvider.setColumnsConfig('CmsKeyword', {
      lg: {
        render: 'table',
        sortField: '-title',
        itemsPerPage: 25,
        columns: ['state', 'title']
      },
      sm: {
        render: 'list',
        sortField: '-title',
        itemsPerPage: 15,
        columns: {
          iconType: 'placeholder',
          iconClass: 'bg-success',
          iconTemplate: '<span class="fas fa-tags placeholder"></span>',
          cellTemplate: "<h4>{{$item.title | l10n}}</h4>"
        }
      }
    });
    return $stateProvider.state('app.modelEditors.CmsKeyword', {
      url: '/keywords',
      abstract: true,
      template: '<ui-view class="ui-view-animation"/>'
    }).state('app.modelEditors.CmsKeyword.list', {
      url: "/",
      controller: 'AdKeywordsListView',
      template: ('/client/admin_app/modelEditors/keywords/ListView.html', '<a-table-toolbar force-lang="forceLang" search-term="itemFilter.searchTerm" model-name="{{:: MODEL_NAME}}" current-user-permission-create="{{:: DJANGO_APP_NAME}}.add_{{:: MODEL_NAME}}"></a-table-toolbar><a-view-title></a-view-title><uice-table-view ng-if="ready" items="items" search-term="itemFilter.searchTerm" columns="{{:: MODEL_NAME}}" force-lang="forceLang" on-item-click="onItemEdit(item, ids)"></uice-table-view>' + ''),
      $pageTitle: 'Keywords'
    }).state('app.modelEditors.CmsKeyword.edit', {
      url: '/edit/:id?ids&returnTo&returnToParams',
      controller: 'AdKeywordsEditView',
      template: ('/client/admin_app/modelEditors/keywords/EditView.html', '<a-view-title></a-view-title><fieldset uic-disabled-on-current-user-permission="! {{:: DJANGO_APP_NAME}}.change_{{:: MODEL_NAME}}"><form name="form"><a-cms-keyword-editor ng-model="item" form="form" force-lang="forceLang"></a-cms-keyword-editor></form></fieldset><a-footer-toolbar item="item" on-save="save()" save-disabled="form.$invalid" on-delete="delete()" delete-disabled="!item.id" current-user-permission-create="{{:: DJANGO_APP_NAME}}.add_{{:: MODEL_NAME}}" current-user-permission-update="{{:: DJANGO_APP_NAME}}.change_{{:: MODEL_NAME}}" current-user-permission-delete="{{:: DJANGO_APP_NAME}}.delete_{{:: MODEL_NAME}}"></a-footer-toolbar>' + ''),
      $pageTitle: 'Edit keyword'
    }).state('app.modelEditors.CmsKeyword.add', {
      url: '/add/-?returnTo&returnToParams',
      controller: 'AdKeywordsEditView',
      template: ('/client/admin_app/modelEditors/keywords/EditView.html', '<a-view-title></a-view-title><fieldset uic-disabled-on-current-user-permission="! {{:: DJANGO_APP_NAME}}.change_{{:: MODEL_NAME}}"><form name="form"><a-cms-keyword-editor ng-model="item" form="form" force-lang="forceLang"></a-cms-keyword-editor></form></fieldset><a-footer-toolbar item="item" on-save="save()" save-disabled="form.$invalid" on-delete="delete()" delete-disabled="!item.id" current-user-permission-create="{{:: DJANGO_APP_NAME}}.add_{{:: MODEL_NAME}}" current-user-permission-update="{{:: DJANGO_APP_NAME}}.change_{{:: MODEL_NAME}}" current-user-permission-delete="{{:: DJANGO_APP_NAME}}.delete_{{:: MODEL_NAME}}"></a-footer-toolbar>' + ''),
      $pageTitle: 'Add keyword'
    });
  }]);

}).call(this);
;
(function() {
  angular.module('AdminApp').controller('AdMailNotificationListView', ["$scope", "$controller", "$currentUser", function($scope, $controller, $currentUser) {
    $scope.MODEL_NAME = 'CmsMailNotification';
    $scope.options = {};
    $scope.options.isAdmin = $currentUser.isAdmin();
    $scope.queryFilter = {};
    if (!$scope.options.isAdmin) {
      $scope.queryFilter.where = {
        owner: $currentUser.id
      };
    }
    return $controller('AdBaseListView', {
      $scope: $scope
    });
  }]).controller('AdMailNotificationEditView', ["$scope", "$controller", "$injector", "$currentUser", "CmsSettings", function($scope, $controller, $injector, $currentUser, CmsSettings) {
    $scope.MODEL_NAME = 'CmsMailNotification';
    $scope.MODEL_DEFAULT_ITEM = {
      emailTemplateName: 'OrangeWave',
      to: [],
      description: {},
      subject: {},
      body: {},
      sendEcho: false,
      echoSubject: {},
      echoBody: {},
      formResponseSuccess: {}
    };
    CmsSettings.serverInfo(function(data) {
      $scope.HOSTNAME = data.hostname;
      $scope.EMAIL_TEMPLATES = data.emailTemplates;
      $scope.MODEL_DEFAULT_ITEM.emailTemplateName = $scope.EMAIL_TEMPLATES[0].value;
      if (!$scope.item.id) {
        $scope.item.emailTemplateName = $scope.EMAIL_TEMPLATES[0].value;
      }
    });
    $controller('AdBaseEditView', {
      $scope: $scope
    });
    $scope.options || ($scope.options = {});
    return $scope.options.isAdmin = $currentUser.isAdmin();
  }]).controller('AdMailNotificationSettingsView', ["$scope", "$cms", "$constants", "CmsSettings", function($scope, $cms, $constants, CmsSettings) {
    var initSettings;
    $scope.USER_ROLES = $constants.resolve('CMS_USER.userRole');
    initSettings = function(userRole, identifier) {
      $scope.settings.userNotifySettings = $scope.settings.userNotifySettings || {};
      $scope.settings.userNotifySettings.email = $scope.settings.userNotifySettings.email || {};
      $scope.settings.userNotifySettings.email[userRole] = $scope.settings.userNotifySettings.email[userRole] || {};
      $scope.settings.userNotifySettings.email[userRole].enabled = $scope.settings.userNotifySettings.email[userRole].enabled || [];
      $scope.settings.userNotifySettings.email[userRole].canDisable = $scope.settings.userNotifySettings.email[userRole].canDisable || {};
    };
    $scope.save = function() {
      var _data;
      _data = {
        id: $scope.settings.id,
        userNotifySettings: $scope.settings.userNotifySettings
      };
      return CmsSettings.updateAttributes({
        id: $scope.settings.id
      }, _data, function() {
        $cms.showNotification('Saved!');
        return $cms.loadSettings(_data);
      }, function() {
        return $cms.showNotification("Error. The object cannot be saved", {}, 'error');
      });
    };
    $scope.removeNotify = function(userRole, identifier) {
      var i;
      initSettings(userRole, identifier);
      i = $scope.settings.userNotifySettings.email[userRole].enabled.indexOf(identifier);
      if (i > -1) {
        return $scope.settings.userNotifySettings.email[userRole].enabled.splice(i, 1);
      }
    };
    $scope.addNotify = function(userRole, identifier) {
      var i;
      initSettings(userRole, identifier);
      i = $scope.settings.userNotifySettings.email[userRole].enabled.indexOf(identifier);
      if (i === -1) {
        return $scope.settings.userNotifySettings.email[userRole].enabled.push(identifier);
      }
    };
    return CmsSettings.findOne(function(data) {
      return $scope.settings = data;
    });
  }]).config(["$stateProvider", "$tableViewProvider", function($stateProvider, $tableViewProvider) {
    $tableViewProvider.setColumnsConfig('CmsMailNotification', {
      lg: {
        render: 'table',
        sortField: '-identifier',
        itemsPerPage: 25,
        columns: [
          {
            headerName: 'Unique Identifier',
            field: 'identifier'
          }, {
            headerName: 'Mail recipient',
            field: 'to',
            cellTemplate: "{{$item.to[0]}}\n<span ng-if=\"$item.to.length>1\" translate=''> (+{{$item.to.length-1}} more)</span>"
          }
        ]
      },
      sm: {
        render: 'list',
        sortField: '-identifier',
        itemsPerPage: 15,
        columns: {
          iconType: 'placeholder',
          iconClass: 'bg-danger',
          iconTemplate: '<span class="fas fa-bell placeholder"></span>',
          cellTemplate: "<h4>{{$item.identifier}}</h4>\n<p>\n    {{$item.to[0]}}\n    <span ng-if=\"$item.to.length>1\" translate=''> (+{{$item.to.length-1}} more)</span>\n</p>"
        }
      }
    });
    $tableViewProvider.setColumnsConfig('CmsMailNotification_for_nonAdmin', {
      lg: {
        render: 'table',
        sortField: '-description',
        itemsPerPage: 25,
        columns: [
          {
            headerName: 'Notification description',
            field: 'description',
            cellTemplate: "{{$item.description | l10n | cut:true:50}}"
          }
        ]
      },
      sm: {
        render: 'list',
        sortField: '-description',
        itemsPerPage: 15,
        columns: {
          iconType: 'placeholder',
          iconClass: 'bg-danger',
          iconTemplate: '<span class="fas fa-bell placeholder"></span>',
          cellTemplate: "<h4>{{$item.description | l10n}}</h4>"
        }
      }
    });
    return $stateProvider.state('app.modelEditors.CmsMailNotification', {
      url: '/mailNotification',
      abstract: true,
      template: '<ui-view class="ui-view-animation"/>'
    }).state('app.modelEditors.CmsMailNotification.list', {
      url: '/',
      controller: 'AdMailNotificationListView',
      template: ('/client/admin_app/modelEditors/mailCampaign/mailNotification/ListView.html', '<div ng-switch="options.isAdmin"><div ng-switch-when="true"><a-table-toolbar force-lang="forceLang" search-term="itemFilter.searchTerm" model-name="CmsMailNotification" options="{hideLangPicker: true}"><a class="btn btn-default" ui-sref="app.modelEditors.CmsMailNotification.settings" uic-if-on-current-user-permission="site_settings.change_CmsSettings"><div class="fas fa-user-cog"></div><span class="btn-text" translate="">User\'s email settings</span></a></a-table-toolbar></div><div ng-switch-when="false"><a-table-toolbar force-lang="forceLang" search-term="itemFilter.searchTerm" options="toolbarOptions"></a-table-toolbar></div></div><a-view-title></a-view-title><div ng-switch="options.isAdmin" ng-if="ready"><uice-table-view ng-switch-when="true" items="items" search-term="itemFilter.searchTerm" columns="CmsMailNotification" force-lang="forceLang" on-item-click="onItemEdit(item, ids)"></uice-table-view><uice-table-view ng-switch-when="false" items="items" search-term="itemFilter.searchTerm" columns="CmsMailNotification_for_nonAdmin" force-lang="forceLang" on-item-click="onItemEdit(item, ids)"></uice-table-view></div>' + ''),
      $pageTitle: 'Mail notifications'
    }).state('app.modelEditors.CmsMailNotification.edit', {
      url: '/edit/:id?ids&returnTo&returnToParams',
      controller: 'AdMailNotificationEditView',
      template: ('/client/admin_app/modelEditors/mailCampaign/mailNotification/EditView.html', '<a-view-title></a-view-title><fieldset uic-disabled-on-current-user-permission="! {{:: DJANGO_APP_NAME}}.change_{{:: MODEL_NAME}}"><form name="form"><uib-tabset active="activeTab"><uib-tab ng-if="options.isAdmin || item.send" index="0"><uib-tab-heading><span class="visible-xxs-inline" translate="">Notification mail</span><span ng-switch="!!$cms.settings.cmsLangCodes.length"><ul class="list-inline hidden-xxs" ng-switch-when="true"><li><span translate="">Notification mail</span><span>. </span><span translate="">Language</span></li><li class="nopadding-lr"><uic-lang-picker class="xs" ng-model="$parent.forceLang" size="sm"></uic-lang-picker></li></ul><span ng-switch-when="false" translate="">Notification mail</span></span></uib-tab-heading><div class="uic-lang-picker-xs-block"><ul class="list-inline"><li><span translate="">Content language</span></li><li><uic-lang-picker ng-model="forceLang" size="sm"></uic-lang-picker></li></ul></div><div class="row" ng-if="options.isAdmin"><div class="form-group" ng-class="{\'col-sm-50\': EMAIL_TEMPLATES.length &gt; 1, \'col-sm-100\': EMAIL_TEMPLATES.length == 1}"><label><span translate="translate">Identifier</span><span> </span><span class="text-danger" ng-show="form.identifier.$error.required" translate="">[required]</span></label><input class="form-control" ng-model="item.identifier" required="required" name="identifier" ng-disabled="item.id"/></div><div class="col-sm-50 form-group" ng-if="EMAIL_TEMPLATES.length &gt; 1"><label translate="">Email template</label><select class="form-control" ng-model="item.emailTemplateName" ng-options="o.value as (o.description | l10n)  for o in EMAIL_TEMPLATES"></select></div></div><div class="form-group" ng-if="options.isAdmin &amp;&amp; $cms.settings.serverIntegrations.emailBackend != \'gmail\'"><label><span translate="translate">Send from email</span><span> </span><span class="text-danger" ng-show="form.email.$error.email" translate="">[invalid email]</span></label><input class="form-control" ng-model="item.fromEmail" type="email" name="email" placeholder="{{\'Default value:\' | translate}} noreply@{{HOSTNAME}}"/></div><div class="checkbox nomargin-top" ng-if="options.isAdmin"><label><input ng-model="item.send" type="checkbox"/><span translate="">Send mail to user</span></label></div><div ng-show="item.send"><h5 class="text-success" ng-show="!options.isAdmin">{{item.description | l10n}}</h5><uice-l10n-input class="form-group" ng-model="item.subject" lang="forceLang" maxlength="75" label="Email subject"><validation-area><a-validate-notification-template ng-model="item" field-name="subject" cms-model-name="CmsMailNotification"></a-validate-notification-template></validation-area></uice-l10n-input><uice-l10n-input class="form-group" ng-model="item.body" lang="forceLang" type="html" cms-model-item="item" maxlength="57000" min-height="40em" label="Body"><validation-area><a-validate-notification-template ng-model="item" field-name="body" cms-model-name="CmsMailNotification"></a-validate-notification-template></validation-area></uice-l10n-input></div><div ng-show="item.identifier.indexOf(\'form\') == 0"><hr/><uice-l10n-input class="form-group" ng-model="item.formResponseSuccess" lang="forceLang" type="html" cms-model-item="item" maxlength="57000" min-height="40em" label="Form success response"><validation-area><a-validate-notification-template ng-model="item" field-name="formResponseSuccess" cms-model-name="CmsMailNotification"></a-validate-notification-template></validation-area></uice-l10n-input></div></uib-tab><uib-tab ng-if="options.isAdmin || item.sendEcho" index="1"><uib-tab-heading><span class="visible-xxs-inline" translate="">Echo mail</span><span ng-switch="!!$cms.settings.cmsLangCodes.length"><ul class="list-inline hidden-xxs" ng-switch-when="true"><li><span translate="">Echo mail</span><span>. </span><span translate="">Language</span></li><li class="nopadding-lr"><uic-lang-picker class="xs" ng-model="$parent.forceLang" size="sm"></uic-lang-picker></li></ul><span ng-switch-when="false" translate="">Echo mail</span></span></uib-tab-heading><div class="uic-lang-picker-xs-block"><ul class="list-inline"><li><span translate="">Content language</span></li><li><uic-lang-picker ng-model="forceLang" size="sm"></uic-lang-picker></li></ul></div><div class="form-group"><uic-checkbox ng-model="item.sendEcho"><span translate="translate">Send echo mail to admin</span></uic-checkbox></div><div ng-show="item.sendEcho"><h5 class="text-success" ng-show="!options.isAdmin">{{item.description | l10n}}</h5><div class="form-group"><label><span translate="translate">Mail recipient</span><span> </span><span class="text-danger" ng-show="!item.to.length" translate="">[required]</span></label><uice-string-list-editor ng-model="item.to" input-placeholder="{{\'Type email\' | translate}}" input-type="email" disable-move-actions="true"><span style="word-break: break-all;">{{$item}}</span></uice-string-list-editor></div><uice-l10n-input class="form-group" ng-model="item.echoSubject" lang="forceLang" maxlength="75" label="Email subject"><validation-area><a-validate-notification-template ng-model="item" field-name="echoSubject" cms-model-name="CmsMailNotification"></a-validate-notification-template></validation-area></uice-l10n-input><uice-l10n-input class="form-group" ng-model="item.echoBody" lang="forceLang" type="html" cms-model-item="item" maxlength="57000" min-height="40em" label="Body"><validation-area><a-validate-notification-template ng-model="item" field-name="echoBody" cms-model-name="CmsMailNotification"></a-validate-notification-template></validation-area></uice-l10n-input></div></uib-tab><uib-tab heading="{{\'Variables\' | translate}}" index="2" ng-if="!options.isAdmin"><div class="form-group"><label translate="">Variables that can be used in templates</label><a-variables-description-editor ng-model="item.variables" view-only="true"></a-variables-description-editor></div></uib-tab><uib-tab index="2" ng-if="options.isAdmin"><uib-tab-heading><span class="visible-xxs-inline" translate="">Description &amp; Variables</span><span ng-switch="!!$cms.settings.cmsLangCodes.length"><ul class="list-inline hidden-xxs" ng-switch-when="true"><li><span translate="">Description &amp; Variables</span><span>. </span><span translate="">Language</span></li><li class="nopadding-lr"><uic-lang-picker class="xs" ng-model="$parent.forceLang" size="sm"></uic-lang-picker></li></ul><span ng-switch-when="false" translate="">Description &amp; Variables</span></span></uib-tab-heading><div class="uic-lang-picker-xs-block"><ul class="list-inline"><li><span translate="">Content language</span></li><li><uic-lang-picker ng-model="forceLang" size="sm"></uic-lang-picker></li></ul></div><uice-l10n-input class="form-group" ng-model="item.description" lang="forceLang" type="textarea" maxlength="200" label="Description"></uice-l10n-input><div class="form-group"><label translate="">Template variables description</label><a-variables-description-editor ng-model="item.variables" lang="forceLang"></a-variables-description-editor></div></uib-tab></uib-tabset></form></fieldset><a-footer-toolbar item="item" on-save="save()" save-disabled="form.$invalid || (item.sendEcho &amp;&amp; !item.to.length)" on-delete="delete()" delete-disabled="!item.id" current-user-permission-create="{{:: DJANGO_APP_NAME}}.add_{{:: MODEL_NAME}}" current-user-permission-update="{{:: DJANGO_APP_NAME}}.change_{{:: MODEL_NAME}}" current-user-permission-delete="{{:: DJANGO_APP_NAME}}.delete_{{:: MODEL_NAME}}"></a-footer-toolbar>' + ''),
      $pageTitle: 'Edit notification',
      $bulk: function() {
        return {
          CmsSettings: {
            findOne: true
          }
        };
      }
    }).state('app.modelEditors.CmsMailNotification.add', {
      url: '/add/-?returnTo&returnToParams',
      controller: 'AdMailNotificationEditView',
      template: ('/client/admin_app/modelEditors/mailCampaign/mailNotification/EditView.html', '<a-view-title></a-view-title><fieldset uic-disabled-on-current-user-permission="! {{:: DJANGO_APP_NAME}}.change_{{:: MODEL_NAME}}"><form name="form"><uib-tabset active="activeTab"><uib-tab ng-if="options.isAdmin || item.send" index="0"><uib-tab-heading><span class="visible-xxs-inline" translate="">Notification mail</span><span ng-switch="!!$cms.settings.cmsLangCodes.length"><ul class="list-inline hidden-xxs" ng-switch-when="true"><li><span translate="">Notification mail</span><span>. </span><span translate="">Language</span></li><li class="nopadding-lr"><uic-lang-picker class="xs" ng-model="$parent.forceLang" size="sm"></uic-lang-picker></li></ul><span ng-switch-when="false" translate="">Notification mail</span></span></uib-tab-heading><div class="uic-lang-picker-xs-block"><ul class="list-inline"><li><span translate="">Content language</span></li><li><uic-lang-picker ng-model="forceLang" size="sm"></uic-lang-picker></li></ul></div><div class="row" ng-if="options.isAdmin"><div class="form-group" ng-class="{\'col-sm-50\': EMAIL_TEMPLATES.length &gt; 1, \'col-sm-100\': EMAIL_TEMPLATES.length == 1}"><label><span translate="translate">Identifier</span><span> </span><span class="text-danger" ng-show="form.identifier.$error.required" translate="">[required]</span></label><input class="form-control" ng-model="item.identifier" required="required" name="identifier" ng-disabled="item.id"/></div><div class="col-sm-50 form-group" ng-if="EMAIL_TEMPLATES.length &gt; 1"><label translate="">Email template</label><select class="form-control" ng-model="item.emailTemplateName" ng-options="o.value as (o.description | l10n)  for o in EMAIL_TEMPLATES"></select></div></div><div class="form-group" ng-if="options.isAdmin &amp;&amp; $cms.settings.serverIntegrations.emailBackend != \'gmail\'"><label><span translate="translate">Send from email</span><span> </span><span class="text-danger" ng-show="form.email.$error.email" translate="">[invalid email]</span></label><input class="form-control" ng-model="item.fromEmail" type="email" name="email" placeholder="{{\'Default value:\' | translate}} noreply@{{HOSTNAME}}"/></div><div class="checkbox nomargin-top" ng-if="options.isAdmin"><label><input ng-model="item.send" type="checkbox"/><span translate="">Send mail to user</span></label></div><div ng-show="item.send"><h5 class="text-success" ng-show="!options.isAdmin">{{item.description | l10n}}</h5><uice-l10n-input class="form-group" ng-model="item.subject" lang="forceLang" maxlength="75" label="Email subject"><validation-area><a-validate-notification-template ng-model="item" field-name="subject" cms-model-name="CmsMailNotification"></a-validate-notification-template></validation-area></uice-l10n-input><uice-l10n-input class="form-group" ng-model="item.body" lang="forceLang" type="html" cms-model-item="item" maxlength="57000" min-height="40em" label="Body"><validation-area><a-validate-notification-template ng-model="item" field-name="body" cms-model-name="CmsMailNotification"></a-validate-notification-template></validation-area></uice-l10n-input></div><div ng-show="item.identifier.indexOf(\'form\') == 0"><hr/><uice-l10n-input class="form-group" ng-model="item.formResponseSuccess" lang="forceLang" type="html" cms-model-item="item" maxlength="57000" min-height="40em" label="Form success response"><validation-area><a-validate-notification-template ng-model="item" field-name="formResponseSuccess" cms-model-name="CmsMailNotification"></a-validate-notification-template></validation-area></uice-l10n-input></div></uib-tab><uib-tab ng-if="options.isAdmin || item.sendEcho" index="1"><uib-tab-heading><span class="visible-xxs-inline" translate="">Echo mail</span><span ng-switch="!!$cms.settings.cmsLangCodes.length"><ul class="list-inline hidden-xxs" ng-switch-when="true"><li><span translate="">Echo mail</span><span>. </span><span translate="">Language</span></li><li class="nopadding-lr"><uic-lang-picker class="xs" ng-model="$parent.forceLang" size="sm"></uic-lang-picker></li></ul><span ng-switch-when="false" translate="">Echo mail</span></span></uib-tab-heading><div class="uic-lang-picker-xs-block"><ul class="list-inline"><li><span translate="">Content language</span></li><li><uic-lang-picker ng-model="forceLang" size="sm"></uic-lang-picker></li></ul></div><div class="form-group"><uic-checkbox ng-model="item.sendEcho"><span translate="translate">Send echo mail to admin</span></uic-checkbox></div><div ng-show="item.sendEcho"><h5 class="text-success" ng-show="!options.isAdmin">{{item.description | l10n}}</h5><div class="form-group"><label><span translate="translate">Mail recipient</span><span> </span><span class="text-danger" ng-show="!item.to.length" translate="">[required]</span></label><uice-string-list-editor ng-model="item.to" input-placeholder="{{\'Type email\' | translate}}" input-type="email" disable-move-actions="true"><span style="word-break: break-all;">{{$item}}</span></uice-string-list-editor></div><uice-l10n-input class="form-group" ng-model="item.echoSubject" lang="forceLang" maxlength="75" label="Email subject"><validation-area><a-validate-notification-template ng-model="item" field-name="echoSubject" cms-model-name="CmsMailNotification"></a-validate-notification-template></validation-area></uice-l10n-input><uice-l10n-input class="form-group" ng-model="item.echoBody" lang="forceLang" type="html" cms-model-item="item" maxlength="57000" min-height="40em" label="Body"><validation-area><a-validate-notification-template ng-model="item" field-name="echoBody" cms-model-name="CmsMailNotification"></a-validate-notification-template></validation-area></uice-l10n-input></div></uib-tab><uib-tab heading="{{\'Variables\' | translate}}" index="2" ng-if="!options.isAdmin"><div class="form-group"><label translate="">Variables that can be used in templates</label><a-variables-description-editor ng-model="item.variables" view-only="true"></a-variables-description-editor></div></uib-tab><uib-tab index="2" ng-if="options.isAdmin"><uib-tab-heading><span class="visible-xxs-inline" translate="">Description &amp; Variables</span><span ng-switch="!!$cms.settings.cmsLangCodes.length"><ul class="list-inline hidden-xxs" ng-switch-when="true"><li><span translate="">Description &amp; Variables</span><span>. </span><span translate="">Language</span></li><li class="nopadding-lr"><uic-lang-picker class="xs" ng-model="$parent.forceLang" size="sm"></uic-lang-picker></li></ul><span ng-switch-when="false" translate="">Description &amp; Variables</span></span></uib-tab-heading><div class="uic-lang-picker-xs-block"><ul class="list-inline"><li><span translate="">Content language</span></li><li><uic-lang-picker ng-model="forceLang" size="sm"></uic-lang-picker></li></ul></div><uice-l10n-input class="form-group" ng-model="item.description" lang="forceLang" type="textarea" maxlength="200" label="Description"></uice-l10n-input><div class="form-group"><label translate="">Template variables description</label><a-variables-description-editor ng-model="item.variables" lang="forceLang"></a-variables-description-editor></div></uib-tab></uib-tabset></form></fieldset><a-footer-toolbar item="item" on-save="save()" save-disabled="form.$invalid || (item.sendEcho &amp;&amp; !item.to.length)" on-delete="delete()" delete-disabled="!item.id" current-user-permission-create="{{:: DJANGO_APP_NAME}}.add_{{:: MODEL_NAME}}" current-user-permission-update="{{:: DJANGO_APP_NAME}}.change_{{:: MODEL_NAME}}" current-user-permission-delete="{{:: DJANGO_APP_NAME}}.delete_{{:: MODEL_NAME}}"></a-footer-toolbar>' + ''),
      $pageTitle: 'Add notification',
      $bulk: function() {
        return {
          CmsSettings: {
            findOne: true
          }
        };
      }
    }).state('app.modelEditors.CmsMailNotification.settings', {
      url: '/settings?returnTo&returnToParams',
      controller: 'AdMailNotificationSettingsView',
      template: ('/client/admin_app/modelEditors/mailCampaign/mailNotification/SettingsView.html', '<div id="AdMailNotificationSettingsView"><a-view-title></a-view-title><form name="form"><uib-tabset active="activeTab"><uib-tab ng-repeat="userRole in USER_ROLES" index="$index" heading="{{userRole.description | translate}}"><h6 class="text-primary nomargin-top" translate="">Enabled email notifications</h6><div class="input-group"><select class="form-control" ng-model="newIdentifier" ng-options="o.identifier as (o.identifier + \' \' + (o.description|l10n|cut:true:50) ) for o in $models.CmsMailNotification.items"></select><div class="input-group-btn"><button class="btn btn-success" ng-disabled="!newIdentifier" ng-click="addNotify(userRole.value, newIdentifier); newIdentifier=\'\'" translate="" translate-context="append">Add</button></div></div><div ng-show="settings.userNotifySettings.email[userRole.value].enabled.length"><hr/><div class="well well-sm"><table class="table nomargin"><thead><tr><th translate="">Notification name</th><th class="td-can-disable text-center" translate="">Can user disable it?</th><th class="td-actions" translate="">Actions</th></tr></thead><thead><tr ng-repeat="mailNotify in $models.CmsMailNotification.items | orderBy: \'identifier\'" ng-if="settings.userNotifySettings.email[userRole.value].enabled.indexOf(mailNotify.identifier)&gt;-1"><td><b>{{mailNotify.identifier}}</b> {{mailNotify.description | l10n}}</td><td class="td-can-disable text-center"><uic-checkbox ng-model="settings.userNotifySettings.email[userRole.value].canDisable[mailNotify.identifier]"></uic-checkbox></td><td class="td-actions"><button class="btn btn-danger btn-sm" ng-click="removeNotify(userRole.value, mailNotify.identifier)" translate="">Remove</button></td></tr></thead></table></div></div></uib-tab></uib-tabset></form><a-footer-toolbar on-save="save()" save-disabled="form.$invalid" current-user-permission-update="site_settings.change_CmsSettings"></a-footer-toolbar></div>' + ''),
      $pageTitle: "User's email settings",
      $bulk: function() {
        return {
          CmsMailNotification: {
            find: {
              filter: {
                fields: {
                  body: false
                }
              },
              $mergeWithArray: 'items'
            }
          }
        };
      }
    });
  }]);

}).call(this);
;
(function() {
  angular.module('AdminApp').controller('PdfDataListView', ["$scope", "$controller", function($scope, $controller) {
    $scope.MODEL_NAME = 'CmsPdfData';

    /*
    $scope.queryFilter = {
        fields: {}
        where: {}
    }
     */
    return $controller('AdBaseListView', {
      $scope: $scope
    });
  }]).controller('PdfDataEditView', ["$scope", "$controller", function($scope, $controller) {
    $scope.MODEL_NAME = 'CmsPdfData';
    $scope.MODEL_DEFAULT_ITEM = {
      title: {},
      content: {},
      description: {},
      runningTitles: {
        isHeaderActive: false,
        isFooterActive: false,
        headerHeight: 10,
        footerHeight: 10,
        header: {},
        footer: {}
      },
      margins: {
        left: 1,
        right: 1,
        top: 1,
        bottom: 1
      }
    };
    return $controller('AdBaseEditView', {
      $scope: $scope
    });
  }]).config(["$stateProvider", "$tableViewProvider", function($stateProvider, $tableViewProvider) {
    $tableViewProvider.setColumnsConfig('CmsPdfData', {
      lg: {
        render: 'table',
        sortField: '-id',
        itemsPerPage: 25,
        columns: [
          {
            headerName: 'Identifier',
            field: 'identifier'
          }
        ]
      },
      sm: {
        render: 'list',
        sortField: '-id',
        itemsPerPage: 15,
        columns: {
          iconType: 'placeholder',
          iconClass: 'bg-warning',
          iconTemplate: '<span class="fas fa-file-pdf placeholder"></span>',
          cellTemplate: "<h4>{{$item.identifier}}</h4>"
        }
      }
    });
    return $stateProvider.state('app.modelEditors.CmsPdfData', {
      url: '/pdfData',
      abstract: true,
      template: '<ui-view class="ui-view-animation"/>'
    }).state('app.modelEditors.CmsPdfData.list', {
      url: '/?page&filter',
      controller: 'PdfDataListView',
      template: ('/client/admin_app/modelEditors/pdfData/ListView.html', '<div id="PdfDataListView"><a-table-toolbar force-lang="forceLang" search-term="itemFilter.searchTerm" model-name="{{:: MODEL_NAME}}" current-user-permission-create="{{:: DJANGO_APP_NAME}}.add_{{:: MODEL_NAME}}"></a-table-toolbar><a-view-title></a-view-title><uice-table-view ng-if="ready" items="items" search-term="itemFilter.searchTerm" columns="CmsPdfData" force-lang="forceLang" on-item-click="onItemEdit(item)"></uice-table-view></div>' + ''),
      $pageTitle: 'PDF templates',
      $bulk: function(defaults, toParams, $models, bulk) {}
    }).state('app.modelEditors.CmsPdfData.edit', {
      url: '/edit/:id?ids&returnTo&returnToParams',
      controller: 'PdfDataEditView',
      template: ('/client/admin_app/modelEditors/pdfData/EditView.html', '<div id="PdfDataEditView"><fieldset uic-disabled-on-current-user-permission="! {{:: DJANGO_APP_NAME}}.change_{{:: MODEL_NAME}}"><form class="col-md-80 col-md-offset-10 nopadding" name="form"><uib-tabset active="activeTab"><uib-tab index="0"><uib-tab-heading><span class="visible-xxs-inline" translate="">Main</span><span ng-switch="!!$cms.settings.cmsLangCodes.length"><ul class="list-inline hidden-xxs" ng-switch-when="true"><li><span translate="">Main</span><span>. </span><span translate="">Language</span></li><li class="nopadding-lr"><uic-lang-picker class="xs" ng-model="$parent.forceLang" size="sm"></uic-lang-picker></li></ul><span ng-switch-when="false" translate="">Main</span></span></uib-tab-heading><div class="uic-lang-picker-xs-block"><ul class="list-inline"><li><span translate="">Content language</span></li><li><uic-lang-picker ng-model="forceLang" size="sm"></uic-lang-picker></li></ul></div><div class="form-group"><label><span translate="">Identifier</span><span class="text-danger"> *</span></label><input class="form-control" ng-model="item.identifier" ng-disabled="item.id" required="required"/></div><uice-l10n-input class="form-group" label="Title" ng-model="item.title" lang="forceLang" maxlength="150"><validation-area><a-validate-notification-template ng-model="item" field-name="title" cms-model-name="CmsPdfData"></a-validate-notification-template></validation-area></uice-l10n-input><uice-l10n-input class="form-group" label="Body" ng-model="item.content" cms-model-item="item" lang="forceLang" type="html" min-height="40em"><validation-area><a-validate-notification-template ng-model="item" field-name="content" cms-model-name="CmsPdfData"></a-validate-notification-template></validation-area></uice-l10n-input><div class="form-group nomargin"><label translate="">Document margins</label><div class="row"><div class="col-md-25 form-group"><label translate="">Top</label><div class="input-group"><input class="form-control" ng-model="item.margins.top" type="number" required="required" step="0.01" min="0"/><div class="input-group-addon" translate="" translate-context="centimeter">cm</div></div></div><div class="col-md-25 form-group"><label translate="">Bottom</label><div class="input-group"><input class="form-control" ng-model="item.margins.bottom" type="number" required="required" step="0.01" min="0"/><div class="input-group-addon" translate="" translate-context="centimeter">cm</div></div></div><div class="col-md-25 form-group"><label translate="">Left</label><div class="input-group"><input class="form-control" ng-model="item.margins.left" type="number" required="required" step="0.01" min="0"/><div class="input-group-addon" translate="" translate-context="centimeter">cm</div></div></div><div class="col-md-25 form-group"><label translate="">Right</label><div class="input-group"><input class="form-control" ng-model="item.margins.right" type="number" required="required" step="0.01" min="0"/><div class="input-group-addon" translate="" translate-context="centimeter">cm</div></div></div></div></div></uib-tab><uib-tab index="1" heading="{{\'Header &amp; footer\' | translate}}"><uic-checkbox ng-model="item.runningTitles.isHeaderActive"><span translate="">Use header</span></uic-checkbox><div ng-if="item.runningTitles.isHeaderActive"><div class="form-group"><label translate="">Header height (mm)</label><input class="form-control" ng-model="item.runningTitles.headerHeight" min="0" required="required" step="1" type="number"/></div><uice-l10n-input class="form-group" label="Document header" ng-model="item.runningTitles.header" cms-model-item="item" lang="forceLang" type="html" min-height="20em"></uice-l10n-input></div><uic-checkbox ng-model="item.runningTitles.isFooterActive"><span translate="">Use footer</span></uic-checkbox><div ng-if="item.runningTitles.isFooterActive"><div class="form-group"><label translate="">Footer height (mm)</label><input class="form-control" ng-model="item.runningTitles.footerHeight" min="0" required="required" step="1" type="number"/></div><uice-l10n-input class="form-group" label="Document footer" ng-model="item.runningTitles.footer" cms-model-item="item" lang="forceLang" type="html" min-height="20em"></uice-l10n-input></div></uib-tab></uib-tabset></form></fieldset><a-footer-toolbar item="item" on-save="save(null, true)" save-disabled="form.$invalid" on-delete="delete()" delete-disabled="!item.id" current-user-permission-create="{{:: DJANGO_APP_NAME}}.add_{{:: MODEL_NAME}}" current-user-permission-update="{{:: DJANGO_APP_NAME}}.change_{{:: MODEL_NAME}}" current-user-permission-delete="{{:: DJANGO_APP_NAME}}.delete_{{:: MODEL_NAME}}"></a-footer-toolbar></div>' + ''),
      $pageTitle: 'Edit PDF template',
      $bulk: function(defaults, toParams, $models, bulk) {}
    }).state('app.modelEditors.CmsPdfData.add', {
      url: '/add/-?returnTo&returnToParams',
      controller: 'PdfDataEditView',
      template: ('/client/admin_app/modelEditors/pdfData/EditView.html', '<div id="PdfDataEditView"><fieldset uic-disabled-on-current-user-permission="! {{:: DJANGO_APP_NAME}}.change_{{:: MODEL_NAME}}"><form class="col-md-80 col-md-offset-10 nopadding" name="form"><uib-tabset active="activeTab"><uib-tab index="0"><uib-tab-heading><span class="visible-xxs-inline" translate="">Main</span><span ng-switch="!!$cms.settings.cmsLangCodes.length"><ul class="list-inline hidden-xxs" ng-switch-when="true"><li><span translate="">Main</span><span>. </span><span translate="">Language</span></li><li class="nopadding-lr"><uic-lang-picker class="xs" ng-model="$parent.forceLang" size="sm"></uic-lang-picker></li></ul><span ng-switch-when="false" translate="">Main</span></span></uib-tab-heading><div class="uic-lang-picker-xs-block"><ul class="list-inline"><li><span translate="">Content language</span></li><li><uic-lang-picker ng-model="forceLang" size="sm"></uic-lang-picker></li></ul></div><div class="form-group"><label><span translate="">Identifier</span><span class="text-danger"> *</span></label><input class="form-control" ng-model="item.identifier" ng-disabled="item.id" required="required"/></div><uice-l10n-input class="form-group" label="Title" ng-model="item.title" lang="forceLang" maxlength="150"><validation-area><a-validate-notification-template ng-model="item" field-name="title" cms-model-name="CmsPdfData"></a-validate-notification-template></validation-area></uice-l10n-input><uice-l10n-input class="form-group" label="Body" ng-model="item.content" cms-model-item="item" lang="forceLang" type="html" min-height="40em"><validation-area><a-validate-notification-template ng-model="item" field-name="content" cms-model-name="CmsPdfData"></a-validate-notification-template></validation-area></uice-l10n-input><div class="form-group nomargin"><label translate="">Document margins</label><div class="row"><div class="col-md-25 form-group"><label translate="">Top</label><div class="input-group"><input class="form-control" ng-model="item.margins.top" type="number" required="required" step="0.01" min="0"/><div class="input-group-addon" translate="" translate-context="centimeter">cm</div></div></div><div class="col-md-25 form-group"><label translate="">Bottom</label><div class="input-group"><input class="form-control" ng-model="item.margins.bottom" type="number" required="required" step="0.01" min="0"/><div class="input-group-addon" translate="" translate-context="centimeter">cm</div></div></div><div class="col-md-25 form-group"><label translate="">Left</label><div class="input-group"><input class="form-control" ng-model="item.margins.left" type="number" required="required" step="0.01" min="0"/><div class="input-group-addon" translate="" translate-context="centimeter">cm</div></div></div><div class="col-md-25 form-group"><label translate="">Right</label><div class="input-group"><input class="form-control" ng-model="item.margins.right" type="number" required="required" step="0.01" min="0"/><div class="input-group-addon" translate="" translate-context="centimeter">cm</div></div></div></div></div></uib-tab><uib-tab index="1" heading="{{\'Header &amp; footer\' | translate}}"><uic-checkbox ng-model="item.runningTitles.isHeaderActive"><span translate="">Use header</span></uic-checkbox><div ng-if="item.runningTitles.isHeaderActive"><div class="form-group"><label translate="">Header height (mm)</label><input class="form-control" ng-model="item.runningTitles.headerHeight" min="0" required="required" step="1" type="number"/></div><uice-l10n-input class="form-group" label="Document header" ng-model="item.runningTitles.header" cms-model-item="item" lang="forceLang" type="html" min-height="20em"></uice-l10n-input></div><uic-checkbox ng-model="item.runningTitles.isFooterActive"><span translate="">Use footer</span></uic-checkbox><div ng-if="item.runningTitles.isFooterActive"><div class="form-group"><label translate="">Footer height (mm)</label><input class="form-control" ng-model="item.runningTitles.footerHeight" min="0" required="required" step="1" type="number"/></div><uice-l10n-input class="form-group" label="Document footer" ng-model="item.runningTitles.footer" cms-model-item="item" lang="forceLang" type="html" min-height="20em"></uice-l10n-input></div></uib-tab></uib-tabset></form></fieldset><a-footer-toolbar item="item" on-save="save(null, true)" save-disabled="form.$invalid" on-delete="delete()" delete-disabled="!item.id" current-user-permission-create="{{:: DJANGO_APP_NAME}}.add_{{:: MODEL_NAME}}" current-user-permission-update="{{:: DJANGO_APP_NAME}}.change_{{:: MODEL_NAME}}" current-user-permission-delete="{{:: DJANGO_APP_NAME}}.delete_{{:: MODEL_NAME}}"></a-footer-toolbar></div>' + ''),
      $pageTitle: 'Add PDF template',
      $bulk: function(defaults, toParams, $models, bulk) {}
    });
  }]);

}).call(this);
;
(function() {
  angular.module('AdminApp').controller('AdPictureAlbumsListView', ["$scope", "$controller", "AdminAppConfig", function($scope, $controller, AdminAppConfig) {
    $scope.MODEL_NAME = 'CmsPictureAlbum';
    $scope.AdminAppConfig = AdminAppConfig;
    $scope.queryFilter = {
      fields: {
        title: true,
        isPublished: true,
        created: true,
        publicationDate: true,
        id: true
      }
    };
    return $controller('AdBaseListView', {
      $scope: $scope
    });
  }]).controller('AdPictureAlbumsEditView', ["$scope", "$controller", "$state", function($scope, $controller, $state) {
    $scope.MODEL_NAME = 'CmsPictureAlbum';
    $scope.MODEL_DEFAULT_ITEM = {
      title: {},
      description: {},
      isPublished: true,
      images: []
    };
    $controller('AdBaseEditView', {
      $scope: $scope
    });
    $scope.afterSave = function(item, isNew) {
      if (isNew) {
        $state.go('app.modelEditors.CmsPictureAlbum.edit', {
          id: item.id
        });
        return item;
      }
      $scope.gotoParentState();
      return item;
    };
    return $scope.isTitleSet = function() {
      var k, ref, v;
      ref = $scope.item.title;
      for (k in ref) {
        v = ref[k];
        if (v) {
          return true;
        }
      }
      return false;
    };
  }]).config(["$stateProvider", "$tableViewProvider", function($stateProvider, $tableViewProvider) {
    $tableViewProvider.setColumnsConfig('CmsPictureAlbum', {
      lg: {
        render: 'table',
        sortField: '-created',
        itemsPerPage: 25,
        columns: ['state', 'title', 'created']
      },
      sm: {
        render: 'list',
        sortField: '-created',
        itemsPerPage: 15,
        columns: {
          iconType: 'placeholder',
          iconClass: 'bg-warning',
          iconTemplate: '<span class="fas fa-images placeholder"></span>',
          cellTemplate: "<h4>{{$item.title | l10n}}</h4>"
        }
      }
    });
    return $stateProvider.state('app.modelEditors.CmsPictureAlbum', {
      url: '/pictureAlbums',
      abstract: true,
      template: '<ui-view class="ui-view-animation"/>'
    }).state('app.modelEditors.CmsPictureAlbum.list', {
      url: '/',
      controller: 'AdPictureAlbumsListView',
      template: ('/client/admin_app/modelEditors/pictureAlbum/ListView.html', '<a-table-toolbar force-lang="forceLang" search-term="itemFilter.searchTerm" model-name="{{:: MODEL_NAME}}" current-user-permission-create="{{:: DJANGO_APP_NAME}}.add_{{:: MODEL_NAME}}"><a class="btn btn-default" ng-show="AdminAppConfig.visibleModels.CmsCategoryForPictureAlbum" ui-sref="app.modelEditors.CmsCategoryForPictureAlbum.list" uic-if-on-current-user-permission="*.view_CmsCategoryForPictureAlbum"><span class="fas fa-list"></span><span translate="">Categories</span></a></a-table-toolbar><a-view-title></a-view-title><uice-table-view ng-if="ready" items="items" search-term="itemFilter.searchTerm" columns="{{:: MODEL_NAME}}" force-lang="forceLang" on-item-click="onItemEdit(item, ids)"></uice-table-view>' + ''),
      $pageTitle: 'Picture albums'
    }).state('app.modelEditors.CmsPictureAlbum.edit', {
      url: '/edit/:id?ids&returnTo&returnToParams',
      controller: 'AdPictureAlbumsEditView',
      template: ('/client/admin_app/modelEditors/pictureAlbum/EditView.html', '<div id="adPictureAlbumsEdit"><a-view-title></a-view-title><fieldset uic-disabled-on-current-user-permission="! {{:: DJANGO_APP_NAME}}.change_{{:: MODEL_NAME}}"><form class="content-with-sidebar" name="form"><uib-tabset class="content" active="activeTab"><uib-tab><uib-tab-heading><span class="visible-xxs-inline" translate="">Media editor</span><span ng-switch="!!$cms.settings.cmsLangCodes.length"><ul class="list-inline hidden-xxs" ng-switch-when="true"><li><span translate="">Media editor</span><span>. </span><span translate="">Language</span></li><li class="nopadding-lr"><uic-lang-picker class="xs" ng-model="$parent.forceLang" size="sm"></uic-lang-picker></li></ul><span ng-switch-when="false" translate="">Media editor</span></span></uib-tab-heading><div class="uic-lang-picker-xs-block"><ul class="list-inline"><li><span translate="">Content language</span></li><li><uic-lang-picker ng-model="forceLang" size="sm"></uic-lang-picker></li></ul></div><uice-l10n-input class="form-group" ng-model="item.title" lang="forceLang" maxlength="75" name="title" required="" label="Album title"></uice-l10n-input><div ng-if="isVisibleModel(\'CmsCategoryForPictureAlbum\')"><div class="form-group" uic-if-on-current-user-permission="*.view_CmsCategoryForPictureAlbum"><label translate="">Category for album</label><a-db-relation-editor ng-model="item.category" type="belongsTo" model-name="CmsCategoryForPictureAlbum" current-user-permission-create="*.add_CmsCategoryForPictureAlbum" current-user-permission-edit="*.change_CmsCategoryForPictureAlbum"></a-db-relation-editor></div></div><uice-l10n-input class="form-group" ng-model="item.description" lang="forceLang" type="textarea" maxlength="450" label="Album description"></uice-l10n-input><div class="clearfix" ng-if="item.id"><hr/><uice-file-manager ng-model="item" field="images" cms-model="Model" accept="image/jpeg,image/png,image/gif"></uice-file-manager></div><div><hr/><small class="help-block"><span class="text-danger">*</span><span> - </span><span translate="">Required fields</span></small></div></uib-tab></uib-tabset><div class="sidebar"><a-object-edit-sidebar ng-model="item" hide-main-img="true" hide-files="true"></a-object-edit-sidebar></div></form></fieldset><a-footer-toolbar item="item" on-save="save()" save-disabled="form.$invalid || !isTitleSet()" on-delete="delete()" delete-disabled="!item.id" current-user-permission-create="{{:: DJANGO_APP_NAME}}.add_{{:: MODEL_NAME}}" current-user-permission-update="{{:: DJANGO_APP_NAME}}.change_{{:: MODEL_NAME}}" current-user-permission-delete="{{:: DJANGO_APP_NAME}}.delete_{{:: MODEL_NAME}}"></a-footer-toolbar></div>' + ''),
      $pageTitle: 'Edit picture album'
    }).state('app.modelEditors.CmsPictureAlbum.add', {
      url: '/add/-?returnTo&returnToParams',
      controller: 'AdPictureAlbumsEditView',
      template: ('/client/admin_app/modelEditors/pictureAlbum/EditView.html', '<div id="adPictureAlbumsEdit"><a-view-title></a-view-title><fieldset uic-disabled-on-current-user-permission="! {{:: DJANGO_APP_NAME}}.change_{{:: MODEL_NAME}}"><form class="content-with-sidebar" name="form"><uib-tabset class="content" active="activeTab"><uib-tab><uib-tab-heading><span class="visible-xxs-inline" translate="">Media editor</span><span ng-switch="!!$cms.settings.cmsLangCodes.length"><ul class="list-inline hidden-xxs" ng-switch-when="true"><li><span translate="">Media editor</span><span>. </span><span translate="">Language</span></li><li class="nopadding-lr"><uic-lang-picker class="xs" ng-model="$parent.forceLang" size="sm"></uic-lang-picker></li></ul><span ng-switch-when="false" translate="">Media editor</span></span></uib-tab-heading><div class="uic-lang-picker-xs-block"><ul class="list-inline"><li><span translate="">Content language</span></li><li><uic-lang-picker ng-model="forceLang" size="sm"></uic-lang-picker></li></ul></div><uice-l10n-input class="form-group" ng-model="item.title" lang="forceLang" maxlength="75" name="title" required="" label="Album title"></uice-l10n-input><div ng-if="isVisibleModel(\'CmsCategoryForPictureAlbum\')"><div class="form-group" uic-if-on-current-user-permission="*.view_CmsCategoryForPictureAlbum"><label translate="">Category for album</label><a-db-relation-editor ng-model="item.category" type="belongsTo" model-name="CmsCategoryForPictureAlbum" current-user-permission-create="*.add_CmsCategoryForPictureAlbum" current-user-permission-edit="*.change_CmsCategoryForPictureAlbum"></a-db-relation-editor></div></div><uice-l10n-input class="form-group" ng-model="item.description" lang="forceLang" type="textarea" maxlength="450" label="Album description"></uice-l10n-input><div class="clearfix" ng-if="item.id"><hr/><uice-file-manager ng-model="item" field="images" cms-model="Model" accept="image/jpeg,image/png,image/gif"></uice-file-manager></div><div><hr/><small class="help-block"><span class="text-danger">*</span><span> - </span><span translate="">Required fields</span></small></div></uib-tab></uib-tabset><div class="sidebar"><a-object-edit-sidebar ng-model="item" hide-main-img="true" hide-files="true"></a-object-edit-sidebar></div></form></fieldset><a-footer-toolbar item="item" on-save="save()" save-disabled="form.$invalid || !isTitleSet()" on-delete="delete()" delete-disabled="!item.id" current-user-permission-create="{{:: DJANGO_APP_NAME}}.add_{{:: MODEL_NAME}}" current-user-permission-update="{{:: DJANGO_APP_NAME}}.change_{{:: MODEL_NAME}}" current-user-permission-delete="{{:: DJANGO_APP_NAME}}.delete_{{:: MODEL_NAME}}"></a-footer-toolbar></div>' + ''),
      $pageTitle: 'Add picture album'
    });
  }]);

}).call(this);
;
(function() {
  angular.module('AdminApp').controller('AdSiteReviewsListView', ["$scope", "$controller", function($scope, $controller) {
    $scope.MODEL_NAME = 'CmsSiteReview';
    $scope.queryFilter = $scope.queryFilter || {
      fields: {
        isPublished: true,
        created: true,
        body: true,
        id: true,
        title: true
      }
    };
    return $controller('AdBaseListView', {
      $scope: $scope
    });
  }]).controller('AdSiteReviewsEditView', ["$scope", "$controller", function($scope, $controller) {
    $scope.MODEL_NAME = 'CmsSiteReview';
    $scope.MODEL_DEFAULT_ITEM = $scope.MODEL_DEFAULT_ITEM || {
      title: {},
      description: {},
      body: {}
    };
    return $controller('AdBaseEditView', {
      $scope: $scope
    });
  }]).config(["$stateProvider", "$tableViewProvider", function($stateProvider, $tableViewProvider) {
    $tableViewProvider.setColumnsConfig('CmsSiteReview', {
      lg: {
        render: 'table',
        sortField: '-created',
        itemsPerPage: 25,
        columns: [
          'state', 'title', {
            headerName: 'Review',
            field: 'body',
            l10n: true,
            cellTemplate: '{{$item.body | l10n:forceLang | cut:false:55}}'
          }, 'created'
        ]
      },
      sm: {
        render: 'list',
        sortField: '-created',
        itemsPerPage: 15,
        columns: {
          iconType: 'placeholder',
          iconClass: 'bg-success',
          iconTemplate: '<span class="fas fa-comments placeholder"></span>',
          cellTemplate: "<h4>{{$item.title | l10n}}</h4>\n<p>{{$item.body | l10n | cut:false:35}}</p>"
        }
      }
    });
    return $stateProvider.state('app.modelEditors.CmsSiteReview', {
      url: '/siteReviews',
      abstract: true,
      template: '<ui-view class="ui-view-animation"/>'
    }).state('app.modelEditors.CmsSiteReview.list', {
      url: '/',
      controller: 'AdSiteReviewsListView',
      template: ('/client/admin_app/modelEditors/siteReviews/ListView.html', '<a-table-toolbar force-lang="forceLang" search-term="itemFilter.searchTerm" model-name="{{:: MODEL_NAME}}" current-user-permission-create="{{:: DJANGO_APP_NAME}}.add_{{:: MODEL_NAME}}"></a-table-toolbar><uice-table-view ng-if="ready" items="items" search-term="itemFilter.searchTerm" columns="{{:: MODEL_NAME}}" force-lang="forceLang" on-item-click="onItemEdit(item, ids)"></uice-table-view>' + ''),
      $pageTitle: 'Site reviews'
    }).state('app.modelEditors.CmsSiteReview.edit', {
      url: '/edit/:id?ids&returnTo&returnToParams',
      controller: 'AdSiteReviewsEditView',
      template: ('/client/admin_app/modelEditors/siteReviews/EditView.html', '<a-view-title></a-view-title><fieldset uic-disabled-on-current-user-permission="! {{:: DJANGO_APP_NAME}}.change_{{:: MODEL_NAME}}"><form class="content-with-sidebar" name="form"><uib-tabset class="content" active="activeTab"><uib-tab index="0"><uib-tab-heading><span class="visible-xxs-inline" translate="">Editor</span><span ng-switch="!!$cms.settings.cmsLangCodes.length"><ul class="list-inline hidden-xxs" ng-switch-when="true"><li><span translate="">Editor</span><span>. </span><span translate="">Language</span></li><li class="nopadding-lr"><uic-lang-picker class="xs" ng-model="$parent.forceLang" size="sm"></uic-lang-picker></li></ul><span ng-switch-when="false" translate="">Editor</span></span></uib-tab-heading><div class="uic-lang-picker-xs-block"><ul class="list-inline"><li><span translate="">Content language</span></li><li><uic-lang-picker ng-model="forceLang" size="sm"></uic-lang-picker></li></ul></div><uice-l10n-input class="form-group" ng-model="item.title" lang="forceLang" maxlength="75" label="Title(name or email, etc.)"></uice-l10n-input><uice-l10n-input class="form-group" ng-model="item.description" lang="forceLang" maxlength="75" label="Headline(city or address, etc.)"></uice-l10n-input><uice-l10n-input class="form-group" ng-model="item.body" lang="forceLang" type="textarea" maxlength="450" label="Review(text)" min-height="11em"></uice-l10n-input></uib-tab><uib-tab class="preview-tab" heading="{{\'Preview\' | translate }}" index="2" ng-if="item.id"><div class="content-container text-center"><br/><div class="col-lg-40 col-lg-offset-30 col-md-50 col-md-offset-25 col-xs-100"><uicv-site-review item="item" force-lang="forceLang" item-class="warning"></uicv-site-review></div></div></uib-tab></uib-tabset><div class="sidebar"><a-object-edit-sidebar ng-model="item" hide-url-id="true" hide-files="true" ng-hide="item.importedFrom"></a-object-edit-sidebar><div class="panel panel-primary panel-sm" ng-show="item.importedFrom"><div class="panel-heading"><div class="panel-title" translate="">Main</div></div><div class="panel-body"><div class="form-group"><label translate="">Imported from: {{item.importedFrom}}</label><input class="form-control" ng-model="item.importedUrl" placeholder="{{\'External link\' | translate}}" disabled=""/></div></div></div></div></form></fieldset><a-footer-toolbar item="item" on-save="save()" save-disabled="form.$invalid" on-delete="delete()" delete-disabled="!item.id" current-user-permission-create="{{:: DJANGO_APP_NAME}}.add_{{:: MODEL_NAME}}" current-user-permission-update="{{:: DJANGO_APP_NAME}}.change_{{:: MODEL_NAME}}" current-user-permission-delete="{{:: DJANGO_APP_NAME}}.delete_{{:: MODEL_NAME}}"></a-footer-toolbar>' + ''),
      $pageTitle: 'Edit site review'
    }).state('app.modelEditors.CmsSiteReview.add', {
      url: '/add/-?returnTo&returnToParams',
      controller: 'AdSiteReviewsEditView',
      template: ('/client/admin_app/modelEditors/siteReviews/EditView.html', '<a-view-title></a-view-title><fieldset uic-disabled-on-current-user-permission="! {{:: DJANGO_APP_NAME}}.change_{{:: MODEL_NAME}}"><form class="content-with-sidebar" name="form"><uib-tabset class="content" active="activeTab"><uib-tab index="0"><uib-tab-heading><span class="visible-xxs-inline" translate="">Editor</span><span ng-switch="!!$cms.settings.cmsLangCodes.length"><ul class="list-inline hidden-xxs" ng-switch-when="true"><li><span translate="">Editor</span><span>. </span><span translate="">Language</span></li><li class="nopadding-lr"><uic-lang-picker class="xs" ng-model="$parent.forceLang" size="sm"></uic-lang-picker></li></ul><span ng-switch-when="false" translate="">Editor</span></span></uib-tab-heading><div class="uic-lang-picker-xs-block"><ul class="list-inline"><li><span translate="">Content language</span></li><li><uic-lang-picker ng-model="forceLang" size="sm"></uic-lang-picker></li></ul></div><uice-l10n-input class="form-group" ng-model="item.title" lang="forceLang" maxlength="75" label="Title(name or email, etc.)"></uice-l10n-input><uice-l10n-input class="form-group" ng-model="item.description" lang="forceLang" maxlength="75" label="Headline(city or address, etc.)"></uice-l10n-input><uice-l10n-input class="form-group" ng-model="item.body" lang="forceLang" type="textarea" maxlength="450" label="Review(text)" min-height="11em"></uice-l10n-input></uib-tab><uib-tab class="preview-tab" heading="{{\'Preview\' | translate }}" index="2" ng-if="item.id"><div class="content-container text-center"><br/><div class="col-lg-40 col-lg-offset-30 col-md-50 col-md-offset-25 col-xs-100"><uicv-site-review item="item" force-lang="forceLang" item-class="warning"></uicv-site-review></div></div></uib-tab></uib-tabset><div class="sidebar"><a-object-edit-sidebar ng-model="item" hide-url-id="true" hide-files="true" ng-hide="item.importedFrom"></a-object-edit-sidebar><div class="panel panel-primary panel-sm" ng-show="item.importedFrom"><div class="panel-heading"><div class="panel-title" translate="">Main</div></div><div class="panel-body"><div class="form-group"><label translate="">Imported from: {{item.importedFrom}}</label><input class="form-control" ng-model="item.importedUrl" placeholder="{{\'External link\' | translate}}" disabled=""/></div></div></div></div></form></fieldset><a-footer-toolbar item="item" on-save="save()" save-disabled="form.$invalid" on-delete="delete()" delete-disabled="!item.id" current-user-permission-create="{{:: DJANGO_APP_NAME}}.add_{{:: MODEL_NAME}}" current-user-permission-update="{{:: DJANGO_APP_NAME}}.change_{{:: MODEL_NAME}}" current-user-permission-delete="{{:: DJANGO_APP_NAME}}.delete_{{:: MODEL_NAME}}"></a-footer-toolbar>' + ''),
      $pageTitle: 'Add site review'
    });
  }]);

}).call(this);
;
(function() {
  angular.module('AdminApp').controller('AdSmsNotificationListView', ["$scope", "$controller", "$currentUser", "AdminAppConfig", function($scope, $controller, $currentUser, AdminAppConfig) {
    $scope.AdminAppConfig = AdminAppConfig;
    $scope.MODEL_NAME = 'CmsSmsNotification';
    $scope.options = $scope.options || {};
    $scope.options.isAdmin = $currentUser.isAdmin();
    $scope.queryFilter = {};
    if (!$scope.options.isAdmin) {
      $scope.queryFilter.where = {
        owner: $currentUser.id
      };
    }
    return $controller('AdBaseListView', {
      $scope: $scope
    });
  }]).controller('AdSmsNotificationEditView', ["$scope", "$controller", "$injector", "$currentUser", function($scope, $controller, $injector, $currentUser) {
    $scope.MODEL_NAME = 'CmsSmsNotification';
    $scope.MODEL_DEFAULT_ITEM = {
      to: [],
      body: {},
      sendEcho: false,
      echoBody: {},
      description: {}
    };
    $controller('AdBaseEditView', {
      $scope: $scope
    });
    $scope.options = $scope.options || {};
    return $scope.options.isAdmin = $currentUser.isAdmin();
  }]).controller('AdSmsNotificationSettingsView', ["$scope", "$cms", "$constants", "CmsSettings", function($scope, $cms, $constants, CmsSettings) {
    var initSettings;
    $scope.USER_ROLES = $constants.resolve('CMS_USER.userRole');
    initSettings = function(userRole, identifier) {
      $scope.settings.userNotifySettings = $scope.settings.userNotifySettings || {};
      $scope.settings.userNotifySettings.sms = $scope.settings.userNotifySettings.sms || {};
      $scope.settings.userNotifySettings.sms[userRole] = $scope.settings.userNotifySettings.sms[userRole] || {};
      $scope.settings.userNotifySettings.sms[userRole].enabled = $scope.settings.userNotifySettings.sms[userRole].enabled || [];
      $scope.settings.userNotifySettings.sms[userRole].canDisable = $scope.settings.userNotifySettings.sms[userRole].canDisable || {};
    };
    $scope.save = function() {
      var _data;
      _data = {
        id: $scope.settings.id,
        userNotifySettings: $scope.settings.userNotifySettings
      };
      return CmsSettings.updateAttributes({
        id: $scope.settings.id
      }, _data, function() {
        $cms.showNotification('Saved!');
        return $cms.loadSettings(_data);
      }, function() {
        return $cms.showNotification("Error. The object cannot be saved", {}, 'error');
      });
    };
    $scope.removeNotify = function(userRole, identifier) {
      var i;
      initSettings(userRole, identifier);
      i = $scope.settings.userNotifySettings.sms[userRole].enabled.indexOf(identifier);
      if (i > -1) {
        return $scope.settings.userNotifySettings.sms[userRole].enabled.splice(i, 1);
      }
    };
    $scope.addNotify = function(userRole, identifier) {
      var i;
      initSettings(userRole, identifier);
      i = $scope.settings.userNotifySettings.sms[userRole].enabled.indexOf(identifier);
      if (i === -1) {
        return $scope.settings.userNotifySettings.sms[userRole].enabled.push(identifier);
      }
    };
    return CmsSettings.findOne(function(data) {
      return $scope.settings = data;
    });
  }]).config(["$stateProvider", "$tableViewProvider", function($stateProvider, $tableViewProvider) {
    $tableViewProvider.setColumnsConfig('CmsSmsNotification', {
      lg: {
        render: 'table',
        sortField: '-identifier',
        itemsPerPage: 25,
        columns: [
          {
            headerName: 'Unique Identifier',
            field: 'identifier'
          }, {
            headerName: 'Recipient',
            field: 'to',
            cellTemplate: "{{$item.to[0]}}\n<span ng-if=\"$item.to.length>1\" translate> (+{{$item.to.length-1}} more)</span>"
          }
        ]
      },
      sm: {
        render: 'list',
        sortField: '-identifier',
        itemsPerPage: 15,
        columns: {
          iconType: 'placeholder',
          iconClass: 'bg-danger',
          iconTemplate: '<span class="fas fa-mobile-alt placeholder"></span>',
          cellTemplate: "<h4>{{$item.identifier}}</h4>\n<p>\n    {{$item.to[0]}}\n    <span ng-if=\"$item.to.length>1\" translate> (+{{$item.to.length-1}} more)</span>\n</p>"
        }
      }
    });
    $tableViewProvider.setColumnsConfig('CmsSmsNotification_for_nonAdmin', {
      lg: {
        render: 'table',
        sortField: '-description',
        itemsPerPage: 25,
        columns: [
          {
            headerName: 'Notification description',
            field: 'description',
            cellTemplate: "{{$item.description | l10n | cut:true:50}}"
          }
        ]
      },
      sm: {
        render: 'list',
        sortField: '-description',
        itemsPerPage: 15,
        columns: {
          iconType: 'placeholder',
          iconClass: 'bg-danger',
          iconTemplate: '<span class="fas fa-mobile-alt placeholder"></span>',
          cellTemplate: "<h4>{{$item.description | l10n}}</h4>"
        }
      }
    });
    return $stateProvider.state('app.modelEditors.CmsSmsNotification', {
      url: '/SmsNotification',
      abstract: true,
      template: '<ui-view class="ui-view-animation"/>'
    }).state('app.modelEditors.CmsSmsNotification.list', {
      url: '/',
      controller: 'AdSmsNotificationListView',
      template: ('/client/admin_app/modelEditors/smsCampaign/smsNotification/ListView.html', '<div ng-switch="options.isAdmin"><div ng-switch-when="true"><a-table-toolbar force-lang="forceLang" search-term="itemFilter.searchTerm" model-name="CmsSmsNotification" options="{hideLangPicker: true}"><a class="btn btn-default" ui-sref="app.modelEditors.CmsSmsNotification.settings" uic-if-on-current-user-permission="site_settings.change_CmsSettings"><span class="fas fa-user-cog"></span><span class="btn-text" translate="">User\'s sms settings</span></a></a-table-toolbar></div><div ng-switch-when="false"><a-table-toolbar force-lang="forceLang" search-term="itemFilter.searchTerm" options="toolbarOptions"></a-table-toolbar></div></div><a-view-title></a-view-title><div ng-switch="options.isAdmin" ng-if="ready"><uice-table-view ng-switch-when="true" items="items" search-term="itemFilter.searchTerm" columns="CmsSmsNotification" force-lang="forceLang" on-item-click="onItemEdit(item, ids)"></uice-table-view><uice-table-view ng-switch-when="false" items="items" search-term="itemFilter.searchTerm" columns="CmsSmsNotification_for_nonAdmin" force-lang="forceLang" on-item-click="onItemEdit(item, ids)"></uice-table-view></div>' + ''),
      $pageTitle: 'SMS notifications'
    }).state('app.modelEditors.CmsSmsNotification.edit', {
      url: '/edit/:id?ids&returnTo&returnToParams',
      controller: 'AdSmsNotificationEditView',
      template: ('/client/admin_app/modelEditors/smsCampaign/smsNotification/EditView.html', '<div id="AdSmsNotificationEditView"><a-view-title></a-view-title><fieldset uic-disabled-on-current-user-permission="! {{:: DJANGO_APP_NAME}}.change_{{:: MODEL_NAME}}"><form name="form"><uib-tabset active="activeTab"><uib-tab ng-if="options.isAdmin || item.send" index="0"><uib-tab-heading><span class="visible-xxs-inline" translate="">Notification sms</span><span ng-switch="!!$cms.settings.cmsLangCodes.length"><ul class="list-inline hidden-xxs" ng-switch-when="true"><li><span translate="">Notification sms</span><span>. </span><span translate="">Language</span></li><li class="nopadding-lr"><uic-lang-picker class="xs" ng-model="$parent.forceLang" size="sm"></uic-lang-picker></li></ul><span ng-switch-when="false" translate="">Notification sms</span></span></uib-tab-heading><div class="uic-lang-picker-xs-block"><ul class="list-inline"><li><span translate="">Content language</span></li><li><uic-lang-picker ng-model="forceLang" size="sm"></uic-lang-picker></li></ul></div><div ng-if="options.isAdmin"><div class="form-group"><label><span translate="translate">Identifier</span><span> </span><span class="text-danger" ng-show="form.identifier.$error.required" translate="">[required]</span></label><input class="form-control" ng-model="item.identifier" required="required" ng-disabled="item.id" name="identifier"/></div><div class="form-group"><uic-checkbox ng-model="item.send"><span translate="translate">Send sms to user</span></uic-checkbox></div></div><div ng-show="item.send"><h5 class="text-success" ng-show="!options.isAdmin">{{item.description | l10n}}</h5><uice-l10n-input class="form-group" ng-model="item.body" lang="forceLang" type="textarea" label="Body"><validation-area><a-validate-notification-template ng-model="item" field-name="body" cms-model-name="CmsSmsNotification"></a-validate-notification-template></validation-area></uice-l10n-input></div></uib-tab><uib-tab ng-if="options.isAdmin || item.sendEcho" index="1"><uib-tab-heading><span class="visible-xxs-inline" translate="">Echo sms</span><span ng-switch="!!$cms.settings.cmsLangCodes.length"><ul class="list-inline hidden-xxs" ng-switch-when="true"><li><span translate="">Echo sms</span><span>. </span><span translate="">Language</span></li><li class="nopadding-lr"><uic-lang-picker class="xs" ng-model="$parent.forceLang" size="sm"></uic-lang-picker></li></ul><span ng-switch-when="false" translate="">Echo sms</span></span></uib-tab-heading><div class="uic-lang-picker-xs-block"><ul class="list-inline"><li><span translate="">Content language</span></li><li><uic-lang-picker ng-model="forceLang" size="sm"></uic-lang-picker></li></ul></div><div class="form-group"><uic-checkbox ng-model="item.sendEcho"><span translate="translate">Send echo sms to admin</span></uic-checkbox></div><div ng-show="item.sendEcho"><h5 class="text-success" ng-show="!options.isAdmin">{{item.description | l10n}}</h5><div class="form-group"><label><span translate="translate">Sms recipient</span><span class="text-danger" ng-show="!item.to.length" translate="">[required]</span></label><uice-string-list-editor ng-model="item.to" input-placeholder="{{\'Type phone\' | translate}}" input-type="email" disable-move-actions="true"><span style="word-break: break-all;">{{$item}}</span></uice-string-list-editor></div><uice-l10n-input class="form-group" ng-model="item.echoBody" lang="forceLang" type="textarea" label="Body"><validation-area><a-validate-notification-template ng-model="item" field-name="echoBody" cms-model-name="CmsSmsNotification"></a-validate-notification-template></validation-area></uice-l10n-input></div></uib-tab><uib-tab heading="{{\'Variables\' | translate}}" index="2" ng-if="!options.isAdmin"><div class="form-group"><label translate="">Variables that can be used in templates</label><a-variables-description-editor ng-model="item.variables" view-only="true"></a-variables-description-editor></div></uib-tab><uib-tab index="2" ng-if="options.isAdmin"><uib-tab-heading><span class="visible-xxs-inline" translate="">Description &amp; Variables</span><span ng-switch="!!$cms.settings.cmsLangCodes.length"><ul class="list-inline hidden-xxs" ng-switch-when="true"><li><span translate="">Description &amp; Variables</span><span>. </span><span translate="">Language</span></li><li class="nopadding-lr"><uic-lang-picker class="xs" ng-model="$parent.forceLang" size="sm"></uic-lang-picker></li></ul><span ng-switch-when="false" translate="">Description &amp; Variables</span></span></uib-tab-heading><div class="uic-lang-picker-xs-block"><ul class="list-inline"><li><span translate="">Content language</span></li><li><uic-lang-picker ng-model="forceLang" size="sm"></uic-lang-picker></li></ul></div><uice-l10n-input class="form-group" ng-model="item.description" lang="forceLang" type="textarea" maxlength="200" label="Description"></uice-l10n-input><div class="form-group"><label translate="">Template variables description</label><a-variables-description-editor ng-model="item.variables" lang="forceLang"></a-variables-description-editor></div></uib-tab></uib-tabset></form></fieldset></div><a-footer-toolbar item="item" on-save="save()" save-disabled="form.$invalid || (item.sendEcho &amp;&amp; !item.to.length)" on-delete="delete()" delete-disabled="!item.id" current-user-permission-create="{{:: DJANGO_APP_NAME}}.add_{{:: MODEL_NAME}}" current-user-permission-update="{{:: DJANGO_APP_NAME}}.change_{{:: MODEL_NAME}}" current-user-permission-delete="{{:: DJANGO_APP_NAME}}.delete_{{:: MODEL_NAME}}"></a-footer-toolbar>' + ''),
      $pageTitle: 'Edit SMS notification'
    }).state('app.modelEditors.CmsSmsNotification.add', {
      url: '/add/-?returnTo&returnToParams',
      controller: 'AdSmsNotificationEditView',
      template: ('/client/admin_app/modelEditors/smsCampaign/smsNotification/EditView.html', '<div id="AdSmsNotificationEditView"><a-view-title></a-view-title><fieldset uic-disabled-on-current-user-permission="! {{:: DJANGO_APP_NAME}}.change_{{:: MODEL_NAME}}"><form name="form"><uib-tabset active="activeTab"><uib-tab ng-if="options.isAdmin || item.send" index="0"><uib-tab-heading><span class="visible-xxs-inline" translate="">Notification sms</span><span ng-switch="!!$cms.settings.cmsLangCodes.length"><ul class="list-inline hidden-xxs" ng-switch-when="true"><li><span translate="">Notification sms</span><span>. </span><span translate="">Language</span></li><li class="nopadding-lr"><uic-lang-picker class="xs" ng-model="$parent.forceLang" size="sm"></uic-lang-picker></li></ul><span ng-switch-when="false" translate="">Notification sms</span></span></uib-tab-heading><div class="uic-lang-picker-xs-block"><ul class="list-inline"><li><span translate="">Content language</span></li><li><uic-lang-picker ng-model="forceLang" size="sm"></uic-lang-picker></li></ul></div><div ng-if="options.isAdmin"><div class="form-group"><label><span translate="translate">Identifier</span><span> </span><span class="text-danger" ng-show="form.identifier.$error.required" translate="">[required]</span></label><input class="form-control" ng-model="item.identifier" required="required" ng-disabled="item.id" name="identifier"/></div><div class="form-group"><uic-checkbox ng-model="item.send"><span translate="translate">Send sms to user</span></uic-checkbox></div></div><div ng-show="item.send"><h5 class="text-success" ng-show="!options.isAdmin">{{item.description | l10n}}</h5><uice-l10n-input class="form-group" ng-model="item.body" lang="forceLang" type="textarea" label="Body"><validation-area><a-validate-notification-template ng-model="item" field-name="body" cms-model-name="CmsSmsNotification"></a-validate-notification-template></validation-area></uice-l10n-input></div></uib-tab><uib-tab ng-if="options.isAdmin || item.sendEcho" index="1"><uib-tab-heading><span class="visible-xxs-inline" translate="">Echo sms</span><span ng-switch="!!$cms.settings.cmsLangCodes.length"><ul class="list-inline hidden-xxs" ng-switch-when="true"><li><span translate="">Echo sms</span><span>. </span><span translate="">Language</span></li><li class="nopadding-lr"><uic-lang-picker class="xs" ng-model="$parent.forceLang" size="sm"></uic-lang-picker></li></ul><span ng-switch-when="false" translate="">Echo sms</span></span></uib-tab-heading><div class="uic-lang-picker-xs-block"><ul class="list-inline"><li><span translate="">Content language</span></li><li><uic-lang-picker ng-model="forceLang" size="sm"></uic-lang-picker></li></ul></div><div class="form-group"><uic-checkbox ng-model="item.sendEcho"><span translate="translate">Send echo sms to admin</span></uic-checkbox></div><div ng-show="item.sendEcho"><h5 class="text-success" ng-show="!options.isAdmin">{{item.description | l10n}}</h5><div class="form-group"><label><span translate="translate">Sms recipient</span><span class="text-danger" ng-show="!item.to.length" translate="">[required]</span></label><uice-string-list-editor ng-model="item.to" input-placeholder="{{\'Type phone\' | translate}}" input-type="email" disable-move-actions="true"><span style="word-break: break-all;">{{$item}}</span></uice-string-list-editor></div><uice-l10n-input class="form-group" ng-model="item.echoBody" lang="forceLang" type="textarea" label="Body"><validation-area><a-validate-notification-template ng-model="item" field-name="echoBody" cms-model-name="CmsSmsNotification"></a-validate-notification-template></validation-area></uice-l10n-input></div></uib-tab><uib-tab heading="{{\'Variables\' | translate}}" index="2" ng-if="!options.isAdmin"><div class="form-group"><label translate="">Variables that can be used in templates</label><a-variables-description-editor ng-model="item.variables" view-only="true"></a-variables-description-editor></div></uib-tab><uib-tab index="2" ng-if="options.isAdmin"><uib-tab-heading><span class="visible-xxs-inline" translate="">Description &amp; Variables</span><span ng-switch="!!$cms.settings.cmsLangCodes.length"><ul class="list-inline hidden-xxs" ng-switch-when="true"><li><span translate="">Description &amp; Variables</span><span>. </span><span translate="">Language</span></li><li class="nopadding-lr"><uic-lang-picker class="xs" ng-model="$parent.forceLang" size="sm"></uic-lang-picker></li></ul><span ng-switch-when="false" translate="">Description &amp; Variables</span></span></uib-tab-heading><div class="uic-lang-picker-xs-block"><ul class="list-inline"><li><span translate="">Content language</span></li><li><uic-lang-picker ng-model="forceLang" size="sm"></uic-lang-picker></li></ul></div><uice-l10n-input class="form-group" ng-model="item.description" lang="forceLang" type="textarea" maxlength="200" label="Description"></uice-l10n-input><div class="form-group"><label translate="">Template variables description</label><a-variables-description-editor ng-model="item.variables" lang="forceLang"></a-variables-description-editor></div></uib-tab></uib-tabset></form></fieldset></div><a-footer-toolbar item="item" on-save="save()" save-disabled="form.$invalid || (item.sendEcho &amp;&amp; !item.to.length)" on-delete="delete()" delete-disabled="!item.id" current-user-permission-create="{{:: DJANGO_APP_NAME}}.add_{{:: MODEL_NAME}}" current-user-permission-update="{{:: DJANGO_APP_NAME}}.change_{{:: MODEL_NAME}}" current-user-permission-delete="{{:: DJANGO_APP_NAME}}.delete_{{:: MODEL_NAME}}"></a-footer-toolbar>' + ''),
      $pageTitle: 'Add SMS notification'
    }).state('app.modelEditors.CmsSmsNotification.settings', {
      url: '/settings?returnTo&returnToParams',
      controller: 'AdSmsNotificationSettingsView',
      template: ('/client/admin_app/modelEditors/smsCampaign/smsNotification/SettingsView.html', '<div id="AdSmsNotificationSettingsView"><a-view-title></a-view-title><form name="form"><uib-tabset active="activeTab"><uib-tab ng-repeat="userRole in USER_ROLES" index="$index" heading="{{userRole.description | translate}}"><h6 class="text-primary nomargin-top" translate="">Enabled sms notifications</h6><div class="input-group"><select class="form-control" ng-model="newIdentifier" ng-options="o.identifier as (o.identifier + \' \' + (o.description|l10n|cut:true:50) ) for o in $models.CmsSmsNotification.items"></select><div class="input-group-btn"><button class="btn btn-success" ng-disabled="!newIdentifier" ng-click="addNotify(userRole.value, newIdentifier); newIdentifier=\'\'" translate="" translate-context="append">Add</button></div></div><div ng-show="settings.userNotifySettings.sms[userRole.value].enabled.length"><hr/><div class="well well-sm"><table class="table nomargin"><thead><tr><th translate="">Notification name</th><th class="td-can-disable text-center" translate="">Can user disable it?</th><th class="td-actions" translate="">Actions</th></tr></thead><thead><tr ng-repeat="smsNotify in $models.CmsSmsNotification.items | orderBy: \'identifier\'" ng-if="settings.userNotifySettings.sms[userRole.value].enabled.indexOf(smsNotify.identifier)&gt;-1"><td><b>{{smsNotify.identifier}}</b> {{smsNotify.description | l10n}}</td><td class="td-can-disable text-center"><uic-checkbox ng-model="settings.userNotifySettings.sms[userRole.value].canDisable[smsNotify.identifier]"></uic-checkbox></td><td class="td-actions"><button class="btn btn-danger btn-sm" ng-click="removeNotify(userRole.value, smsNotify.identifier)" translate="">Remove</button></td></tr></thead></table></div></div></uib-tab></uib-tabset></form><a-footer-toolbar on-save="save()" save-disabled="form.$invalid" current-user-permission-update="site_settings.change_CmsSettings"></a-footer-toolbar></div>' + ''),
      $pageTitle: "User's SMS settings",
      $bulk: function() {
        return {
          CmsSmsNotification: {
            find: {
              filter: {
                fields: {
                  body: false
                }
              },
              $mergeWithArray: 'items'
            }
          }
        };
      }
    });
  }]);

}).call(this);
;
(function() {
  angular.module('AdminApp').controller('AdTranslationsListView', ["$scope", "$controller", "$stateParams", function($scope, $controller, $stateParams) {
    $scope.MODEL_NAME = 'CmsTranslations';
    $scope.queryFilter = {
      fields: {
        created: true,
        lang: true,
        id: true
      },
      where: {
        isPublished: true
      }
    };
    return $controller('AdBaseListView', {
      $scope: $scope
    });
  }]).controller('AdTranslationsEditView', ["$scope", "$controller", "$cms", function($scope, $controller, $cms) {
    $scope.MODEL_NAME = 'CmsTranslations';
    return $controller('AdBaseEditView', {
      $scope: $scope
    });
  }]).config(["$stateProvider", "$tableViewProvider", function($stateProvider, $tableViewProvider) {
    $tableViewProvider.setColumnsConfig('CmsTranslations', {
      lg: {
        render: 'table',
        sortField: '-created',
        itemsPerPage: 25,
        columns: [
          {
            headerName: 'Language',
            field: 'lang',
            cellTemplate: '{{$item.lang | fromLangCodeToLangName}}'
          }, 'created'
        ]
      },
      sm: {
        render: 'list',
        sortField: '-created',
        itemsPerPage: 15,
        columns: {
          iconType: 'placeholder',
          iconClass: 'bg-success',
          iconTemplate: '<span class="fas fa-language placeholder"></span>',
          cellTemplate: "<h4>{{$item.lang | fromLangCodeToLangName}}</h4>"
        }
      }
    });
    return $stateProvider.state('app.modelEditors.CmsTranslations', {
      url: '/translations',
      abstract: true,
      template: '<ui-view class="ui-view-animation"/>'
    }).state('app.modelEditors.CmsTranslations.list', {
      url: '/',
      controller: 'AdTranslationsListView',
      template: ('/client/admin_app/modelEditors/translations/ListView.html', '<a-view-title></a-view-title><uice-table-view ng-if="ready" items="items" search-term="itemFilter.searchTerm" columns="{{:: MODEL_NAME}}" force-lang="forceLang" on-item-click="onItemEdit(item, ids)"></uice-table-view>' + ''),
      $pageTitle: 'Translations'
    }).state('app.modelEditors.CmsTranslations.edit', {
      url: '/edit/:id?ids&returnTo&returnToParams',
      controller: 'AdTranslationsEditView',
      template: ('/client/admin_app/modelEditors/translations/EditView.html', '<div id="TranslationsEditView"><fieldset ng-switch="!!item.id" uic-disabled-on-current-user-permission="! {{:: DJANGO_APP_NAME}}.change_CmsTranslations"><a-cms-translations-editor ng-model="item" ng-switch-when="true"></a-cms-translations-editor><div class="with-preloader" ng-switch-when="false" style="width: 10em;"><div class="preloader"></div></div></fieldset></div><a-footer-toolbar on-save="save()" save-disabled="form.$invalid || (item.sendEcho &amp;&amp; !item.to.length)" current-user-permission-update="{{:: DJANGO_APP_NAME}}.change_CmsTranslations"></a-footer-toolbar>' + ''),
      $pageTitle: 'Edit translation'
    });
  }]);

}).call(this);
;
(function() {
  angular.module('AdminApp').controller('AdUserListView', ["$scope", "$controller", function($scope, $controller) {
    $scope.MODEL_NAME = 'CmsUser';
    $scope.USE_SERVER_PAGINATION = true;
    $scope.queryFilter = $scope.queryFilter || {
      fields: {
        id: true,
        username: true,
        email: true,
        date_joined: true,
        userRole: true
      }
    };
    return $controller('AdBaseListView', {
      $scope: $scope
    });
  }]).controller('AdUserEditView', ["$scope", "$controller", function($scope, $controller) {
    $scope.MODEL_NAME = 'CmsUser';
    return $controller('AdBaseEditView', {
      $scope: $scope
    });
  }]).config(["$stateProvider", "$tableViewProvider", function($stateProvider, $tableViewProvider) {
    $tableViewProvider.setColumnsConfig('CmsUser', {
      lg: {
        render: 'table',
        sortField: '-username',
        itemsPerPage: 25,
        columns: [
          {
            headerName: 'Username',
            field: 'username'
          }, {
            headerName: 'Email',
            field: 'email'
          }, {
            headerName: 'User role',
            field: 'userRole',
            cellTemplate: "{{$item.userRole | constantToText: 'CMS_USER.userRole'}}"
          }, {
            headerName: 'Created',
            field: 'date_joined',
            cellTemplate: "{{$item.date_joined | date}}"
          }
        ]
      },
      sm: {
        render: 'list',
        sortField: '-date_joined',
        itemsPerPage: 15,
        columns: {
          iconType: 'placeholder',
          iconClass: 'bg-danger',
          iconTemplate: '<span class="fas fa-user placeholder"></span>',
          cellTemplate: "<h4>{{$item.username}}<small ng-if=\"$item.userRole\">  ({{$item.userRole | constantToText: 'CMS_USER.userRole'}})</small></h4>\n<h5 ng-if=\"$item.email\">{{$item.email}}</h5>"
        }
      }
    });
    return $stateProvider.state('app.modelEditors.CmsUser', {
      url: '/users',
      abstract: true,
      template: '<ui-view class="ui-view-animation"/>'
    }).state('app.modelEditors.CmsUser.list', {
      url: '/?page&filter&sort',
      controller: 'AdUserListView',
      template: ('/client/admin_app/modelEditors/users/ListView.html', '<div id="UsersListView"><a-table-toolbar force-lang="forceLang" search-term="itemFilter.searchTerm" options="options.toolbarOptions" model-name="{{:: MODEL_NAME}}" current-user-permission-create="{{:: DJANGO_APP_NAME}}.add_{{:: MODEL_NAME}}"><a class="btn btn-success" ui-sref="app.modelEditors.GroupPermissions.list" ng-if="$currentUser.isAdmin() &amp;&amp; $parent.AdminAppConfig.visibleModels.GroupPermissions"><div class="far fa-object-group"></div><span translate="">Groups</span></a></a-table-toolbar><a-view-title></a-view-title><div ng-if="ready"><uice-table-view search-term="itemFilter.searchTerm" columns="CmsUser" force-lang="forceLang" on-item-click="onItemEdit(item, ids)" query-filter="queryFilter" load-items-fn="loadData()" count-items-fn="countData()"></uice-table-view></div></div>' + ''),
      $pageTitle: 'Users'
    }).state('app.modelEditors.CmsUser.edit', {
      url: '/edit/:id?ids&returnTo&returnToParams',
      controller: 'AdUserEditView',
      template: ('/client/admin_app/modelEditors/users/EditView.html', '<div id="UsersEditView"><a-view-title></a-view-title><fieldset uic-disabled-on-current-user-permission="! {{:: DJANGO_APP_NAME}}.change_{{:: MODEL_NAME}}"><form name="form"><a-cms-user-editor ng-model="item" force-lang="forceLang" form="form"></a-cms-user-editor></form></fieldset><a-footer-toolbar item="item" on-save="save()" save-disabled="form.$invalid" on-delete="delete()" delete-disabled="!item.id" current-user-permission-create="main_app.add_{{:: MODEL_NAME}}" current-user-permission-update="main_app.change_{{:: MODEL_NAME}}" current-user-permission-delete="main_app.delete_{{:: MODEL_NAME}}"></a-footer-toolbar></div>' + ''),
      $pageTitle: 'Edit user'
    }).state('app.modelEditors.CmsUser.add', {
      url: '/add/-?returnTo&returnToParams',
      controller: 'AdUserEditView',
      template: ('/client/admin_app/modelEditors/users/EditView.html', '<div id="UsersEditView"><a-view-title></a-view-title><fieldset uic-disabled-on-current-user-permission="! {{:: DJANGO_APP_NAME}}.change_{{:: MODEL_NAME}}"><form name="form"><a-cms-user-editor ng-model="item" force-lang="forceLang" form="form"></a-cms-user-editor></form></fieldset><a-footer-toolbar item="item" on-save="save()" save-disabled="form.$invalid" on-delete="delete()" delete-disabled="!item.id" current-user-permission-create="main_app.add_{{:: MODEL_NAME}}" current-user-permission-update="main_app.change_{{:: MODEL_NAME}}" current-user-permission-delete="main_app.delete_{{:: MODEL_NAME}}"></a-footer-toolbar></div>' + ''),
      $pageTitle: 'Add user'
    });
  }]);

}).call(this);
;
(function() {
  angular.module('AdminApp').config(["$stateProvider", function($stateProvider) {
    return $stateProvider.state('app.modelEditors', {
      url: '/model-editors',
      abstract: true,
      template: '<ui-view/>'
    });
  }]);

}).call(this);
;
(function() {
  angular.module('AdminApp').service('aExportSiteResourcesModal', ["$uibModal", "$localStorage", "$langPicker", "CmsSettings", "$cms", "$q", function($uibModal, $localStorage, $langPicker, CmsSettings, $cms, $q) {
    var modalController;
    modalController = function($scope, $uibModalInstance, isPossible, options) {
      var base;
      $scope.options = options;
      $scope.isPossible = isPossible;
      (base = $scope.options).email || (base.email = $localStorage.aExportSiteResourcesModal_email);
      $scope.cancel = function() {
        return $uibModalInstance.dismiss('cancel');
      };
      return $scope.ok = function() {
        $localStorage.aExportSiteResourcesModal_email = $scope.options.email;
        $scope.options.loading = true;
        CmsSettings.dumpDbAndFiles({
          args: options.dumpType,
          sendToEmail: $scope.options.email,
          preferredLang: $langPicker.currentLang
        }, function(data) {
          $cms.showNotification("Check your email", null, 'warning');
          $uibModalInstance.close();
        }, function(data) {
          $cms.showNotification("Error. Try again later", null, 'error');
          $uibModalInstance.dismiss('cancel');
        });
      };
    };
    this.open = function(options) {
      var defer;
      options = options || {};
      options.dumpType = options.dumpType || 'db,files';
      defer = $q.defer();
      if (!CmsSettings.checkDumpDbAndFilesIsPossible) {
        defer.resolve();
        $cms.showNotification("Please, don't close this page!", null, 'warning');
        location.href = CmsSettings.$actionToUrl('dumpDbAndFiles', {
          args: options.dumpType
        });
        return defer.promise;
      }
      CmsSettings.checkDumpDbAndFilesIsPossible({
        args: options.dumpType
      }, function(data) {
        var result;
        if (data.isPossible && !data.emailRequired) {
          defer.resolve();
          $cms.showNotification("Please, don't close this page!", null, 'warning');
          location.href = CmsSettings.$actionToUrl('dumpDbAndFiles', {
            args: options.dumpType
          });
          return;
        }
        result = $uibModal.open({
          animation: true,
          backdrop: 'static',
          keyboard: false,
          windowClass: 'a-export-site-resources-modal',
          template: ('/client/admin_app/settings/aExportSiteResources/modal.html', '<div class="modal-header"><button class="close" ng-click="cancel()"><span>&times;</span></button><h4 class="modal-title" translate="">Dump resources</h4></div><form class="modal-body" name="form"><fieldset ng-disabled="options.loading"><p translate="">The archive with the dump is too large for you to download through the browser. Enter your email and when the archive is ready, you will be sent a link to it.</p><div class="form-group"><label translate="">Your email:</label><input class="form-control" ng-model="options.email" required="required" type="email"/></div></fieldset></form><div class="modal-footer"><button class="btn btn-success" ng-click="ok()" translate="" ng-disabled="form.$invalid || options.loading">Export</button><button class="btn btn-default" ng-click="cancel()" translate="">Cancel</button></div>' + ''),
          controller: ['$scope', '$uibModalInstance', 'isPossible', 'options', modalController],
          size: 'sm',
          resolve: {
            options: function() {
              return options;
            },
            isPossible: function() {
              return data;
            }
          }
        });
        return result.result.then(defer.resolve, defer.reject);
      }, function(data) {
        $cms.showNotification("Error. Try again later", null, 'error');
        defer.reject(data);
      });
      return defer.promise;
    };
    return this;
  }]);

}).call(this);
;
(function() {
  angular.module('AdminApp').service('aSendTestNotify', ["$uibModal", "CmsSettings", function($uibModal, CmsSettings) {
    var email, emailModalController, phone, smsModalController;
    email = void 0;
    phone = void 0;
    emailModalController = function($scope, $uibModalInstance, options) {
      $scope.options = options;
      $scope.email = email || '';
      $scope.cancel = function() {
        return $uibModalInstance.dismiss('cancel');
      };
      return $scope.ok = function() {
        email = $scope.email;
        $scope.result = 'loading';
        CmsSettings.sendTestNotify({
          to: $scope.email,
          notifyType: 'email'
        }, {
          serverIntegrations: options
        }, function() {
          $scope.result = 'ok';
        }, function(data) {
          $scope.result = 'error';
          $scope.data = data;
        });
      };
    };
    smsModalController = function($scope, $uibModalInstance, options) {
      $scope.options = options;
      $scope.phone = phone || '';
      $scope.cancel = function() {
        return $uibModalInstance.dismiss('cancel');
      };
      return $scope.ok = function() {
        phone = $scope.phone;
        $scope.result = 'loading';
        CmsSettings.sendTestNotify({
          to: $scope.phone,
          notifyType: 'sms'
        }, {
          serverIntegrations: options
        }, function() {
          $scope.result = 'ok';
        }, function(data) {
          $scope.result = 'error';
          $scope.data = data;
        });
      };
    };
    this.email = function(options) {
      var result;
      result = $uibModal.open({
        animation: true,
        windowClass: 'a-send-test-notify-email-modal',
        template: ('/client/admin_app/settings/aSendTestNotify/emailModal.html', '<div class="modal-header"><button class="close" ng-click="cancel()"><span>&times;</span></button><h4 class="modal-title" translate="">Send test email</h4></div><form class="modal-body" name="form"><div class="form-group"><label translate="">Email</label><input class="form-control" ng-model="email" type="email" required="required" ng-disabled="result == \'loading\'" autofocus=""/></div><div ng-switch="result"><div class="with-preloader" ng-switch-when="loading" style="height: 5em;"><div class="preloader"></div></div><div class="alert alert-success" ng-switch-when="ok"><b translate="">All ok!</b></div><div class="alert alert-danger" ng-switch-when="error"><b translate="">Can\'t send email:</b><div uic-bind-html="data.data.error"></div></div></div></form><div class="modal-footer"><button class="btn btn-primary" ng-click="ok()" ng-disabled="form.$invalid || result == \'loading\'"> <div class="fas fa-share"></div><span translate="">Send</span></button><button class="btn btn-default" ng-click="cancel()" translate="" ng-disabled="loading">Cancel</button></div>' + ''),
        controller: ['$scope', '$uibModalInstance', 'options', emailModalController],
        size: 'sm',
        resolve: {
          options: function() {
            return options || {};
          }
        }
      });
      return result.result;
    };
    this.sms = function(options) {
      var result;
      result = $uibModal.open({
        animation: true,
        windowClass: 'a-send-test-notify-sms-modal',
        template: ('/client/admin_app/settings/aSendTestNotify/smsModal.html', '<div class="modal-header"><button class="close" ng-click="cancel()"><span>&times;</span></button><h4 class="modal-title" translate="">Send test sms</h4></div><form class="modal-body" name="form"><div class="form-group"><label translate="">Phone</label><input class="form-control" ng-model="phone" type="text" required="required" ng-disabled="result == \'loading\'"/></div><div ng-switch="result"><div class="with-preloader" ng-switch-when="loading" style="height: 5em;"><div class="preloader"></div></div><div class="alert alert-success" ng-switch-when="ok"><b translate="">All ok!</b></div><div class="alert alert-danger" ng-switch-when="error"><b translate="">Can\'t send sms:</b><div uic-bind-html="data.data.error"></div></div></div></form><div class="modal-footer"><button class="btn btn-success" ng-click="ok()" translate="" ng-disabled="form.$invalid || result == \'loading\'">Send</button><button class="btn btn-default" ng-click="cancel()" translate="" ng-disabled="loading">Cancel</button></div>' + ''),
        controller: ['$scope', '$uibModalInstance', 'options', smsModalController],
        size: 'sm',
        resolve: {
          options: function() {
            return options || {};
          }
        }
      });
      return result.result;
    };
    return this;
  }]);

}).call(this);
;
(function() {
  angular.module('AdminApp').controller('SettingsView', ["$scope", "$cms", "$models", "$sce", "$templateRequest", "H", "$currentUser", "$state", "aIntegrationsModal", "CmsSettings", "uiceL10nFileEditorModal", "AdminAppConfig", "aSendTestNotify", "aExportSiteResourcesModal", function($scope, $cms, $models, $sce, $templateRequest, H, $currentUser, $state, aIntegrationsModal, CmsSettings, uiceL10nFileEditorModal, AdminAppConfig, aSendTestNotify, aExportSiteResourcesModal) {
    var base, base1, faviconElement, i, k, len, link, logoAltElement, logoElement, ref, ref1, resolveFilePath, uploadFileForBrand, v;
    if (!$currentUser.hasPermission('site_settings.change_CmsSettings')) {
      return $state.go('app.dashboard');
    }
    $scope.AdminAppConfig = AdminAppConfig;
    $scope.$cms = $cms;
    $scope.forceLang = CMS_DEFAULT_LANG;
    $scope.allowedCurrencySymbols = ['CZK', 'USD', 'EUR', 'UAH', 'RUB'];
    $scope.allowedCurrency = {};
    $scope.allowedLangs = {};
    $scope.item = {};
    $scope.options || ($scope.options = {
      dumpType: 'db',
      redirectionsForm: null
    });
    (base = $scope.options).brandUrls || (base.brandUrls = {});
    (base1 = $scope.options.brandUrls).disabled || (base1.disabled = {});
    $scope.CMS_ALLOWED_LANGS = CMS_ALLOWED_LANGS;
    $scope.CMS_DEFAULT_LANG = CMS_DEFAULT_LANG;
    $scope.CMS_MODEL_NAMES_FOR_WATERMARK = [];
    ref = AdminAppConfig.visibleModels;
    for (k in ref) {
      v = ref[k];
      if (k !== 'CmsTranslations' && k !== 'CmsMailNotification' && k !== 'CmsSmsNotification' && k !== 'GroupPermissions') {
        $scope.CMS_MODEL_NAMES_FOR_WATERMARK.push({
          value: k,
          description: k
        });
      }
    }
    resolveFilePath = function(name) {
      var q;
      q = name.indexOf('?');
      if (q > -1) {
        name = name.substr(0, q);
      }
      return name + "?t=" + new Date().getTime();
    };
    faviconElement = null;
    logoElement = null;
    logoAltElement = null;
    ref1 = document.head.getElementsByTagName('link');
    for (i = 0, len = ref1.length; i < len; i++) {
      link = ref1[i];
      if (!(link.href && link.rel)) {
        continue;
      }
      if (link.rel === 'icon') {
        faviconElement = link;
        $scope.options.brandUrls.favicon = resolveFilePath(link.href);
      }
      if (link.rel === 'logo') {
        logoElement = link;
        $scope.options.brandUrls.logo = resolveFilePath(link.href);
      }
      if (link.rel === 'logo-alt') {
        logoAltElement = link;
        $scope.options.brandUrls['logo-alt'] = resolveFilePath(link.href);
        $scope.options.brandUrls.hasLogoAlt = true;
      }
    }
    CmsSettings.findOne(function(data) {
      var c, j, l, len1, len2, len3, m, ref2, ref3, ref4;
      $scope.item = angular.copy(data);
      $scope.item.serverIntegrations = $scope.item.serverIntegrations || {};
      if (!$scope.item.serverIntegrations.hasOwnProperty('useMaps')) {
        $scope.item.serverIntegrations.useMaps = false;
        $scope.item.serverIntegrations.mapsBackend = 'google';
      }
      if (!$scope.item.serverIntegrations.hasOwnProperty('useTranslations')) {
        $scope.item.serverIntegrations.useTranslations = false;
        $scope.item.serverIntegrations.translationsBackend = 'google';
      }
      ref2 = $scope.allowedCurrencySymbols;
      for (j = 0, len1 = ref2.length; j < len1; j++) {
        c = ref2[j];
        $scope.allowedCurrency[c] = false;
      }
      ref3 = data.allowedCurrency || [];
      for (l = 0, len2 = ref3.length; l < len2; l++) {
        c = ref3[l];
        $scope.allowedCurrency[c] = true;
      }
      ref4 = data.cmsLangCodes || [];
      for (m = 0, len3 = ref4.length; m < len3; m++) {
        c = ref4[m];
        $scope.allowedLangs[c] = true;
      }
      $scope.$watch('allowedCurrency', function(allowedCurrency, oldValue) {
        return $scope.item.allowedCurrency = (function() {
          var results;
          results = [];
          for (k in allowedCurrency) {
            v = allowedCurrency[k];
            if (v) {
              results.push(k);
            }
          }
          return results;
        })();
      }, true);
      $scope.$watch('item.integrations', function(integrations) {
        return $scope.hasIntegrations = !angular.isEmpty(integrations);
      }, true);
    });
    $scope.addIntegration = function() {
      aIntegrationsModal.add($scope.item.integrations).then(function(data) {
        var base2;
        (base2 = $scope.item).integrations || (base2.integrations = {});
        $scope.item.integrations[data.type] = data.data;
      });
    };
    $scope.manageWatermarkImg = function() {
      var options;
      options = {
        id: $scope.item.id,
        forModelField: 'watermarkImg',
        forceCmsStorageType: 'localStorage'
      };
      return uiceL10nFileEditorModal.edit($scope.item, options).then(function(l10nFile) {
        return $scope.item.watermarkImg = l10nFile;
      });
    };
    $scope.save = function() {
      var item, ref2;
      item = angular.copy($scope.item);
      item.cmsLangCodes = [];
      ref2 = $scope.allowedLangs;
      for (k in ref2) {
        v = ref2[k];
        if (v) {
          item.cmsLangCodes.push(k);
        }
      }
      CmsSettings.updateAttributes({
        id: $scope.item.id
      }, item, function(data) {
        $scope.item = angular.copy(data);
        $cms.loadSettings(data);
        return $cms.showNotification('Settings were updated successfully!');
      }, function() {
        return $cms.showNotification("Error. Can't update settings. Try again later", null, 'error');
      });
    };
    $scope.onAddPaymentMethod = function(value) {
      var method;
      $scope.item.paymentMethods = $scope.item.paymentMethods || [];
      method = {
        title: {},
        type: 'online'
      };
      method.title[$scope.forceLang] = value;
      $scope.item.paymentMethods.push(method);
    };
    $scope.onAddSiteRedirect = function(value) {
      var j, len1, r, ref2;
      $scope.item.siteRedirections = $scope.item.siteRedirections || [];
      ref2 = $scope.item.siteRedirections;
      for (j = 0, len1 = ref2.length; j < len1; j++) {
        r = ref2[j];
        if (r.fromUrl === value) {
          return $cms.showNotification("Redirect rule already exists!", null, 'danger');
        }
      }
      $scope.item.siteRedirections.unshift({
        fromUrl: value,
        redirectCode: '301'
      });
    };
    $scope.sendTestNotify = function(notifyType, options) {
      aSendTestNotify[notifyType](options);
    };
    $scope.exportResources = function() {
      aExportSiteResourcesModal.open({
        dumpType: $scope.options.dumpType
      });
    };
    $scope.restoreResources = function() {
      $scope.options.restoreInProgress = true;
      $scope.options.restoreError = false;
      $cms.showNotification("Please, don't close this page!", null, 'warning');
      CmsSettings.restoreDbAndFiles({}, {
        file: $scope.options.restoreFromFile,
        $asFormData: true
      }, function() {
        $cms.showNotification('Restored from file');
        $scope.options.restoreInProgress = false;
        setTimeout(function() {
          return location.reload();
        }, 3000);
      }, function(data) {
        $cms.showNotification('Failed to restore from file', null, 'error');
        $scope.options.restoreInProgress = false;
        if (angular.isString(data.data) && data.data.indexOf("<!DOCTYPE html>") === 0) {
          data.type = 'html';
        } else {
          data.type = 'json';
        }
        $scope.options.restoreError = data;
      });
    };
    uploadFileForBrand = function(destination, element, fileToUpload) {
      var NAMES;
      NAMES = {
        'favicon': 'Favicon',
        'logo': 'Logo',
        'logo-alt': 'Alternate logo'
      };
      $scope.options.brandUrls.disabled[destination] = true;
      return CmsSettings.uploadFileForBrand({
        destination: destination,
        id: $scope.item.id
      }, {
        file: fileToUpload,
        $asFormData: true
      }, function(data) {
        $cms.showNotification("{{siteBrandFile}} changed", {
          siteBrandFile: NAMES[destination]
        });
        element.href = resolveFilePath(element.href) + '?t=' + new Date().getTime();
        $scope.options.brandUrls.disabled[destination] = false;
        $scope.options.brandUrls[destination] = element.href;
      }, function(data) {
        $scope.options.brandUrls.disabled[destination] = false;
        $cms.showNotification("Error on {{siteBrandFile}} upload", {
          siteBrandFile: NAMES[destination]
        }, 'error');
      });
    };
    $scope.$watch('options.newFavicon', function(newFavicon, oldValue) {
      if (newFavicon) {
        uploadFileForBrand('favicon', faviconElement, newFavicon);
        $scope.options.newFavicon = null;
      }
    });
    $scope.$watch('options.newLogo', function(newLogo, oldValue) {
      if (newLogo) {
        uploadFileForBrand('logo', logoElement, newLogo);
        $scope.options.newLogo = null;
      }
    });
    $scope.$watch('options.newLogoAlt', function(newLogo, oldValue) {
      if (newLogo) {
        uploadFileForBrand('logo-alt', logoAltElement, newLogo);
        $scope.options.newLogoAlt = null;
      }
    });
  }]).config(["$stateProvider", function($stateProvider) {
    return $stateProvider.state('app.settings', {
      url: '/settings',
      controller: 'SettingsView',
      template: ('/client/admin_app/settings/View.html', '<a-view-title></a-view-title><fieldset id="SettingsView" uic-disabled-on-current-user-permission="!isAdmin()"><uice-tabset class="sm" dropdown-btn-class="info"><uice-tabset-tab ng-repeat="tab in AdminAppConfig.settingsTabs.customTabs"><uice-tabset-tab-heading uic-bind-html="tab.heading"></uice-tabset-tab-heading><div uic-bind-html="tab.template"></div></uice-tabset-tab><uice-tabset-tab ng-if="AdminAppConfig.settingsTabs.config.maps"><uice-tabset-tab-heading><span class="fas fa-map-marked-alt"></span><span translate="" translate-context="World maps">Maps</span></uice-tabset-tab-heading><h4 class="text-primary" translate="">Maps</h4><uic-checkbox ng-model="item.serverIntegrations.useMaps"><span translate="" translate-context="World maps">Use integrations with map-provider</span></uic-checkbox><fieldset class="row" ng-disabled="!item.serverIntegrations.useMaps"><form name="options.serverIntegrations_maps"><div class="col-md-100"><div class="form-inline"><div class="form-group"><label><span translate="" translate-context="plugin backend">Backend:</span><span style="padding-right: 7px;"></span></label><select class="form-control input-sm" ng-model="item.serverIntegrations.mapsBackend"><option value="google">Google maps </option></select></div></div><hr/></div><div ng-switch="item.serverIntegrations.mapsBackend"><div ng-switch-when="google"><div class="col-md-50 form-group"><label><span translate="">Google maps API key</span><span> </span><small translate="">(for client)</small><span class="text-danger"> *</span></label><input class="form-control" ng-model="item.serverIntegrations.googleMaps.apiKey" required="required"/></div><div class="col-md-50 form-group"><label><span translate="">Google maps API key</span><span> </span><small translate="">(for server)</small></label><input class="form-control" ng-model="item.serverIntegrations.googleMaps.apiKeyServer"/></div><div class="col-md-100"><uic-alert type="info"><b translate="">Do not forget to reload page after changing settings in this tab!</b></uic-alert></div></div></div></form></fieldset></uice-tabset-tab><uice-tabset-tab ng-if="AdminAppConfig.settingsTabs.config.languages"><uice-tabset-tab-heading><span class="fas fa-language"></span><span translate="">Localization</span></uice-tabset-tab-heading><h4 class="text-primary" translate="">Languages</h4><div class="form-group"><label translate="" style="padding-right:5px;">Visible languages on landing/site:</label><div class="checkbox-inline"><label class="nomargin text-normal"><input type="checkbox" checked="checked" disabled="disabled"/>{{:: $cms.LANG_MAP[CMS_DEFAULT_LANG]}}</label></div><div class="checkbox-inline" ng-repeat="l_code in CMS_ALLOWED_LANGS" ng-if="l_code != CMS_DEFAULT_LANG"><label class="nomargin text-normal"><input type="checkbox" ng-model="allowedLangs[l_code]"/>{{:: $cms.LANG_MAP[l_code]}}</label></div></div><h4 class="text-primary" translate="">Translation service</h4><uic-checkbox ng-model="item.serverIntegrations.useTranslations"><span translate="">Use integrations with translation service (for admin page only)</span></uic-checkbox><fieldset class="row" ng-disabled="!item.serverIntegrations.useTranslations"><form name="options.serverIntegrations_translations"><div class="col-md-100"><div class="form-inline"><div class="form-group"><label><span translate="" translate-context="plugin backend">Backend:</span><span style="padding-right: 7px;"></span></label><select class="form-control input-sm" ng-model="item.serverIntegrations.translationsBackend"><option value="google">Google Translate</option></select></div></div><hr/></div><div ng-switch="item.serverIntegrations.mapsBackend"><div ng-switch-when="google"><div class="col-md-100 form-group"><label><span translate="">Google Translate API key</span><span class="text-danger"> *</span></label><input class="form-control" ng-model="item.serverIntegrations.googleTranslate.apiKey" required="required"/></div><div class="col-md-100"><uic-alert type="info"><b translate="">Do not forget to reload page after changing settings in this tab!</b></uic-alert></div></div></div></form></fieldset></uice-tabset-tab><uice-tabset-tab ng-if="AdminAppConfig.settingsTabs.config.smsNotify"><uice-tabset-tab-heading><span class="fas fa-mobile-alt"></span><span translate="">Sms</span></uice-tabset-tab-heading><h4 class="text-primary" translate="">Sms notifications</h4><uic-checkbox ng-model="item.serverIntegrations.useSmsNotifications"><span translate="">Use sms notifications for user</span></uic-checkbox><fieldset class="row" ng-disabled="!item.serverIntegrations.useSmsNotifications"><form name="options.serverIntegrations_sms"><div class="col-md-100"><div class="form-inline"><div class="form-group"><label><span translate="" translate-context="plugin backend">Backend:</span><span style="padding-right: 7px;"></span></label><div class="input-group"><select class="form-control input-sm" ng-model="item.serverIntegrations.smsBackend"><option value="smsapicom">smsapi.com </option></select><div class="input-group-btn"><button class="btn btn-sm btn-default" ng-click="sendTestNotify(\'sms\', item.serverIntegrations)" title="{{\'Send test sms\' | translate}}" ng-disabled="options.serverIntegrations_sms.$invalid"><span class="fas fa-mobile-alt"></span><span translate="">Test</span></button></div></div></div></div><hr/></div><div class="col-md-50 form-group"><label><span translate="">Username for smsapi.com</span><span class="text-danger"> *</span></label><input class="form-control" ng-model="item.serverIntegrations.smsapicom.username" required="required"/></div><div class="col-md-50 form-group"><label><span translate="">Password for smsapi.com</span><span class="text-danger"> *</span></label><input class="form-control" ng-model="item.serverIntegrations.smsapicom.password" type="password" required="required"/></div><div class="col-md-100 form-group"><label><span translate="">Sender name (see https://ssl.smsapi.com/sms_settings/sendernames)</span><span class="text-danger"> *</span></label><input class="form-control" ng-model="item.serverIntegrations.smsapicom.senderName" required="required"/></div></form></fieldset><div ng-if="item.serverIntegrations.useSmsNotifications &amp;&amp; AdminAppConfig.settingsTabs.config.smsNotifyForUsers"><h4 class="text-primary" translate="">User\'s SMS settings</h4><a-user-notify-settings ng-model="item.userNotifySettings.sms" type="sms"></a-user-notify-settings></div></uice-tabset-tab><uice-tabset-tab ng-if="AdminAppConfig.settingsTabs.config.emailNotify"><uice-tabset-tab-heading><span class="fas fa-envelope"></span><span translate="">Email</span></uice-tabset-tab-heading><h4 class="text-primary" translate="">Email notifications</h4><uic-checkbox ng-model="item.serverIntegrations.useEmailNotifications"><span translate="">Use email notifications for user</span></uic-checkbox><fieldset class="row" ng-disabled="!item.serverIntegrations.useEmailNotifications"><form name="options.serverIntegrations_email"><div class="col-md-100 form-inline"><div class="form-group"><label><span translate="" translate-context="plugin backend">Backend:</span><span class="text-danger" ng-show="item.serverIntegrations.emailBackend == \'gmail\'"> **</span><span style="padding-right: 7px;"></span></label><div class="input-group"><select class="form-control input-sm" ng-model="item.serverIntegrations.emailBackend"><option value="sendgrid">sendgrid.com</option><option value="gmail">mail.google.com (gmail)</option><option value="regular" translate="">any other SMTP-server</option></select><div class="input-group-btn"><button class="btn btn-sm btn-default" ng-click="sendTestNotify(\'email\', item.serverIntegrations)" title="{{\'Send test email\' | translate}}" ng-disabled="options.serverIntegrations_email.$invalid"><span class="fas fa-envelope"></span><span translate="">Test</span></button></div></div></div></div><div class="col-md-100"><div ng-show="item.serverIntegrations.emailBackend == \'gmail\'" style="margin-top: 5px;"><small><span class="text-danger">**</span><span> - </span><span translate="">you should allow less secure app access to your email. See <a href="https://support.google.com/accounts/answer/6010255" target=\'_blank\'>this link</a></span></small></div><hr/></div><div ng-switch="item.serverIntegrations.emailBackend"><div ng-switch-when="sendgrid"><div class="col-md-100 form-group"><label><span translate="">Sendgrid API key</span><span class="text-danger"> *</span></label><input class="form-control" ng-model="item.serverIntegrations.sendgrid.apiKey" required="required"/></div></div><div ng-switch-when="gmail"><div class="col-md-50 form-group"><label><span translate="">Real email login at mail.google.com</span><span class="text-danger"> *</span></label><input class="form-control" ng-model="item.serverIntegrations.gmail.username" required="required" placeholder="test@gmail.com"/></div><div class="col-md-50 form-group"><label><span translate="">Real email password at mail.google.com</span><span class="text-danger"> *</span></label><input class="form-control" ng-model="item.serverIntegrations.gmail.password" required="required" type="password"/></div></div><div ng-switch-when="regular"><div class="col-md-50"><div class="form-group"><label><span translate="">Host</span><span class="text-danger"> *</span></label><input class="form-control" ng-model="item.serverIntegrations.smtp.host" required="required" placeholder="localhost"/></div><div class="form-group"><label translate="">Port</label><input class="form-control" ng-model="item.serverIntegrations.smtp.port" type="number" min="0" step="1" placeholder="25"/></div><uic-checkbox ng-model="item.serverIntegrations.smtp.useTls"><span translate="">Use TLS</span></uic-checkbox></div><div class="col-md-50"><div class="form-group"><label translate="">Username</label><input class="form-control" ng-model="item.serverIntegrations.smtp.username" placeholder="{{\'Leave empty for localhost\' | translate}}"/></div><div class="form-group"><label translate="">Password</label><input class="form-control" ng-model="item.serverIntegrations.smtp.password" placeholder="{{\'Leave empty for localhost\' | translate}}"/></div></div></div></div></form></fieldset><div ng-if="item.serverIntegrations.useEmailNotifications &amp;&amp; AdminAppConfig.settingsTabs.config.emailNotifyForUsers"><h4 class="text-primary" translate="">User\'s email settings</h4><a-user-notify-settings ng-model="item.userNotifySettings.email" type="email"></a-user-notify-settings></div></uice-tabset-tab><uice-tabset-tab ng-if="AdminAppConfig.settingsTabs.config.currency"><uice-tabset-tab-heading><span class="fas fa-dollar-sign"></span><span translate="">Currency</span></uice-tabset-tab-heading><h4 class="text-primary" translate="">Main</h4><div class="row"><div class="col-md-50"><div class="form-group"><label translate="">Default currency</label><select class="form-control" ng-model="item.defaultCurrency"><option value="CZK">CZK</option><option value="USD">USD</option><option value="EUR">EUR</option></select></div></div><div class="col-md-50"><div class="form-group"><label translate="">Allowed currency</label><div><label class="checkbox-inline" ng-repeat="c in allowedCurrencySymbols"><input type="checkbox" ng-model="allowedCurrency[c]"/>{{c}}</label></div></div><div class="form-group"><uic-checkbox ng-model="item.defaultCurrencyConverter.useECB"><span translate="">Use exchange rates from ECB</span></uic-checkbox></div></div></div><div ng-hide="item.defaultCurrencyConverter.useECB"><div ng-show="item.allowedCurrency.length"><h4 class="text-primary" translate="">Currency converter data</h4><div class="row"><div class="col-md-25" ng-repeat="c in allowedCurrencySymbols" ng-hide="item.defaultCurrency==c"><div class="form-group"><label>1 {{item.defaultCurrency}} = X {{c}}</label><input class="form-control" ng-model="item.defaultCurrencyConverter[item.defaultCurrency][c]" min="0" type="number" ng-disabled="!allowedCurrency[c]"/></div></div></div></div></div></uice-tabset-tab><uice-tabset-tab ng-if="AdminAppConfig.settingsTabs.config.redirections"><uice-tabset-tab-heading><span class="fas fa-link"></span><span translate="">Redirections</span></uice-tabset-tab-heading><h4 class="text-primary" translate="">Site redirection rules</h4><uic-alert type="warning"><h5 class="nomargin-top"><b translate="">Redirect rules restrictions:</b></h5><ul><li translate="">Url to be redirected to could be specified as http://, https:// or just  /</li><li><span translate="">If you are redirecting from one domain to the base, you have to specify the full record of the base domain</span><div><b translate="">Example:</b></div><code><span> http://site1.com/url1  <b>=&gt;</b> https://mysite.com/url2</span></code><div><b translate="">Invalid example (for the target url, the prefix and domain are missing):</b></div><code><span> http://site1.com/url1  <b>=&gt;</b> /url2 <span class=\'text-warning\'>(**)</span></span></code><div></div><small><span class="text-warning">**</span><span> - </span><span translate="">it\'s not clear where to redirect the user</span></small></li><li><span translate="">If you redirect within the base domain, then prefixes or domains need not be specified</span><div><b translate="">Example:</b></div><code><span> /my/url1?param=myparam  <b>=&gt;</b> /url2  <span class=\'text-warning\'>(***)</span></span></code><div></div><small><span class="text-warning">***</span><span> - </span><span translate="">at the same time, if the user visited the site not through https, but through http, then he will be redirected to http version</span></small></li></ul></uic-alert><form name="options.redirectionsForm"><uice-list-editor ng-model="item.siteRedirections" on-add="onAddSiteRedirect(value)" input-placeholder="{{\'New redirections from url\' | translate}}" input-position="top"><item-title><label class="text-danger"><span translate="">Rule №</span> {{$index+1}}</label></item-title><item-body class="clearfix"><div class="form-group"><label><span translate="">From url</span><span class="text-danger"> *</span></label><input class="form-control input-sm" ng-model="$item.fromUrl" type="string" a-is-url="" required="required" name="redirectFromUrl{{$index}}"/></div><div class="form-group nomargin-bottom"><ul class="list-table nomargin"><li class="text-left"><label><span translate="">To url</span><span class="text-danger"> *</span></label><input class="form-control input-sm" ng-model="$item.toUrl" type="string" a-is-url="" required="required" name="redirectToUrl{{$index}}"/></li><li style="max-width:4em;"><label><span translate="">Http code</span></label><select class="form-control input-sm" ng-model="$item.redirectCode"><option value="301" translate="">301 - Moved Permanently</option><option value="302" translate="">302 - Moved Temporarily</option></select></li></ul></div></item-body></uice-list-editor></form></uice-tabset-tab><uice-tabset-tab ng-if="AdminAppConfig.settingsTabs.config.dumpAndRestore"><uice-tabset-tab-heading><span class="fas fa-database"></span><span translate="">Resources</span></uice-tabset-tab-heading><h4 class="text-primary" translate="">Application data            </h4><uic-alert type="info"><b translate="">Exporting/importing app data could take some time</b><div translate="">Do not close page while exporting/importing is in progress!</div></uic-alert><div class="row"><div class="col-md-50"><div class="form-group"><label translate="">Dump resources</label><div class="input-group"><select class="form-control" ng-model="options.dumpType"><option value="db" translate="">Database</option><option value="files" translate="">Media files</option><option value="db,files" translate="">Database and media files</option></select><div class="input-group-btn"><button class="btn btn-success" ng-click="exportResources()"><span class="fas fa-cloud-upload-alt"></span><span translate="">Export to 7zip archive</span></button></div></div></div></div><fieldset class="col-md-50" ng-disabled="options.restoreInProgress"><form name="options.restoreDbAndFiles_form"><div class="form-group"><label translate="">Restore resources</label><div class="input-group"><uic-input-file class="import-db-files nomargin" ng-model="options.restoreFromFile" max-size="500Mb" accept="application/x-7z-compressed" name="importFromFile"></uic-input-file><div class="input-group-btn"><button class="btn btn-warning" ng-click="restoreResources()" ng-disabled="options.restoreDbAndFiles_form.$invalid || !options.restoreFromFile"><span class="fas fa-cloud-download-alt"></span><span translate="">Import from 7zip archive</span></button></div></div></div><div class="form-group" ng-show="options.restoreFromFile.name"><span translate="">File {{options.restoreFromFile.name}}</span><span class="text-danger" ng-show="options.restoreDbAndFiles_form.importFromFile.$error.acceptType"><span> </span><span translate="">[invalid file type]</span></span></div><div class="with-preloader" ng-show="options.restoreInProgress" style="height: 8em;margin-top: 1em;"><div class="preloader"></div></div></form></fieldset><div class="col-md-100" ng-show="options.restoreError"><uic-alert type="danger"><div ng-switch="options.restoreError.type"><b translate="">Restore failure</b><hr/><div ng-switch-when="html"><uic-iframe class="nomargin nopadding" ng-model="options.restoreError.data"></uic-iframe></div><div ng-switch-when="json"><pre>{{options.restoreError.data | json: 4}}</pre></div></div></uic-alert></div></div></uice-tabset-tab><uice-tabset-tab><uice-tabset-tab-heading><span class="fas fa-search-plus"></span><span translate="">SEO</span></uice-tabset-tab-heading><uic-lang-picker class="pull-right sm" ng-model="forceLang"></uic-lang-picker><h4 class="text-primary" translate="">Main</h4><div class="row"><div class="col-md-30"><div class="form-group"><label><span translate="">Favicon</span><span> </span><small translate="">(png-files allowed, max 5mb)</small></label><ul class="list-table nomargin vertical-align-middle"><li class="w-2em"><img class="w-100" ng-src="{{options.brandUrls.favicon}}"/></li><li><uic-input-file class="btn btn-success btn-sm" ng-model="options.newFavicon" accept="image/png" max-size="5 MB" ng-disabled="options.brandUrls.disabled.favicon"></uic-input-file></li></ul></div><div class="form-group"><label><span translate="">Logo</span><span> </span><small translate="">(png-files allowed, max 5mb)</small></label><ul class="list-table nomargin vertical-align-middle"><li class="w-6em"><img class="w-100 thumbnail thumbnail-logo nomargin" ng-src="{{options.brandUrls.logo}}"/></li><li><uic-input-file class="btn btn-success btn-sm" ng-model="options.newLogo" accept="image/png" max-size="5 MB" ng-disabled="options.brandUrls.disabled.logo"></uic-input-file></li></ul></div><div class="form-group" ng-if="options.brandUrls.hasLogoAlt"><label><span translate="">Alternate logo</span><span> </span><small translate="">(png-files allowed, max 5mb)</small></label><ul class="list-table nomargin vertical-align-middle"><li class="w-6em"><img class="w-100 thumbnail thumbnail-logo nomargin" ng-src="{{options.brandUrls[\'logo-alt\']}}"/></li><li><uic-input-file class="btn btn-success btn-sm" ng-model="options.newLogoAlt" accept="image/png" max-size="5 MB" ng-disabled="options.brandUrls.disabled[\'logo-alt\']"></uic-input-file></li></ul></div></div><div class="col-md-70"><uice-l10n-input class="form-group" ng-model="item.siteTitle" lang="forceLang" maxlength="80" label="Site title"></uice-l10n-input><uice-l10n-input class="form-group" ng-model="item.siteDescription" lang="forceLang" type="textarea" maxlength="300" label="Site description"></uice-l10n-input></div></div><hr/><h4 class="text-primary" translate="">Robots.txt</h4><div class="form-group"><label translate="">Robots.txt content</label><textarea class="form-control" ng-model="item.robotsTxt" maxlength="20000"></textarea></div><hr/><h4 class="text-primary" translate="">Prerender for web crawlers</h4><uic-checkbox ng-model="item.serverIntegrations.usePrerender"><span translate="">Use prerender</span></uic-checkbox><div ng-hide="!item.serverIntegrations.usePrerender"><div class="form-inline"><div class="form-group"><label><span translate="" translate-context="plugin backend">Backend:</span></label><select class="form-control input-sm" ng-model="item.serverIntegrations.prerenderBackend"><option value="prerender.io" translate="">Prerender.io</option><option value="" translate="">Base self hosted prerender</option></select></div></div><div ng-switch="item.serverIntegrations.prerenderBackend == \'prerender.io\'"><div class="form-group" ng-switch-when="true"><label><span translate="">Token for prerender.io (copy token from <a href="https://prerender.io/install-token">https://prerender.io/install-token</a>)</span><span class="text-danger"> *</span></label><input class="form-control" ng-model="item.serverIntegrations.prerender.token" required="required"/></div><div class="form-group" ng-switch-when="false"><label><span translate="">Full url to prerender site</span><span class="text-danger"> *</span></label><input class="form-control" ng-model="item.serverIntegrations.prerender.url" type="url" required="required"/></div></div><div class="form-group"><label><span translate="">Web crawler\'s user agent - regular expression (see more at <a href="https://docs.python.org/2/library/re.html" target=\'_blank\'>this link</a>)</span><span class="text-danger"> *</span></label><textarea class="form-control" ng-model="item.serverIntegrations.prerender.userAgentRegex" required="required" style="min-height: 8em;"></textarea></div></div><hr/><h4 class="text-primary" translate="">Social media</h4><div><b translate="translate">Here you can add links to facebook, twitter, etc. accounts</b></div><uice-social-links-editor ng-model="item.seo.social"></uice-social-links-editor><hr/><ul class="list-table nomargin"><li class="text-left"><h4 class="text-primary" translate="">Integrations</h4></li><li></li><li class="text-right" style="vertical-align:middle;"><button class="btn btn-xs btn-primary" ng-click="addIntegration()" ng-if="hasIntegrations"><span class="fas fa-plus"></span><span translate="">Add Service</span></button></li></ul><a-integrations-editor ng-model="item.integrations"></a-integrations-editor><hr/><h4 class="text-primary" translate="">Watermark</h4><div class="clearfix row"><div class="col-lg-30 col-md-40"><div class="form-group"><label translate="translate">Upload your watermark</label><span class="text-danger">*</span><uice-main-img-manager class="watermark" ng-model="item" field="watermarkImg" options="{forceCmsStorageType: \'localStorage\', allowEditTitleDescription: false, accept: \'image/png\'}"></uice-main-img-manager></div></div><div class="col-lg-70 col-md-60"><uic-checkbox ng-model="item.watermark.autoAddWatermark"><span translate="translate">Add watermark to all images</span></uic-checkbox><div class="form-group"><label translate="translate">Watermark position</label><select class="form-control" ng-model="item.watermark.position" ng-disabled="!item.watermark.autoAddWatermark"><option translate="translate" value="top left">Top left</option><option translate="translate" value="top right">Top right</option><option translate="translate" value="bottom left">Bottom left</option><option translate="translate" value="bottom right">Bottom right</option><option translate="translate" value="center">Center</option></select></div><div class="form-group watermark-models" ng-show="item.watermark.autoAddWatermark"><label><span translate="">Apply watermark to server models</span><span class="text-danger">**</span>:</label><div></div><uic-constant ng-model="item.watermark.models" checkbox="CMS_MODEL_NAMES_FOR_WATERMARK" input-class="inline"></uic-constant></div><div><small><span class="text-danger">*</span><span translate="" style="padding-left:3px;">watermark should be in PNG format with transparent background</span></small></div><div ng-show="item.watermark.autoAddWatermark"><small><span class="text-danger">**</span><span translate="" style="padding-left:3px;">watermark will be added to all images, except for the previews</span></small></div></div></div></uice-tabset-tab></uice-tabset></fieldset><a-footer-toolbar on-save="save()" save-disabled="options.redirectionsForm.$invalid" current-user-permission-update="isAdmin()"></a-footer-toolbar>' + ''),
      $pageTitle: 'Settings'
    });
  }]);

}).call(this);
;
(function() {
  angular.module('AdminApp').controller('SiteMenuView', ["$scope", "$cms", "$bulk", "CmsSettings", function($scope, $cms, $bulk, CmsSettings) {
    $scope.$bulk = $bulk;
    $scope.activeTab = 0;
    CmsSettings.findOne(function(data) {
      $scope.item = data;
    });
    return $scope.save = function() {
      CmsSettings.findOne(function(item) {
        item.navbarConf = $scope.item.navbarConf;
        return CmsSettings.updateAttributes({
          id: item.id
        }, item, function() {
          $cms.showNotification('Saved!');
        }, function() {
          $cms.showNotification("Error. The object cannot be saved", null, 'error');
        });
      });
    };
  }]).config(["$stateProvider", "$injector", function($stateProvider, $injector) {
    var operations;
    operations = {
      CmsCategoryForArticle: {
        find: {
          $mergeWithArray: 'items'
        }
      },
      CmsArticle: {
        find: {
          filter: {
            where: {
              isPublished: true
            },
            fields: {
              title: true,
              id: true,
              urlIdPart: true,
              category: true,
              isPublished: true
            }
          },
          $mergeWithArray: 'items'
        }
      }
    };
    if ($injector.has('CmsPictureAlbumProvider')) {
      operations.CmsPictureAlbum = {
        find: {
          where: {
            isPublished: true
          },
          fields: {
            title: true,
            id: true,
            category: true
          },
          $mergeWithArray: 'items'
        }
      };
    }
    return $stateProvider.state('app.siteMenu', {
      url: '/siteMenu',
      controller: 'SiteMenuView',
      template: ('/client/admin_app/siteMenu/View.html', '<a-view-title></a-view-title><fieldset class="tab-content" id="SiteMenuView" uic-disabled-on-current-user-permission="!site_settings.change_CmsSettings"><div class="tab-pane"><label translate="translate">Preview</label><div ng-if="$bulk.ready &amp;&amp; !$bulk.loading &amp;&amp; item.id"><div class="navbar navbar-default"><div class="container-fluid"><div class="navbar-collapse"><uic-navbar-nav ng-model="item.navbarConf.main" use-url-id="true" use-hide-for-lang="false" cms-model-item="item"></uic-navbar-nav></div></div></div><div class="clearfix" style="padding-bottom:10px;"><div><label translate="translate">Menu editor</label></div><div class="col-md-70 col-md-offset-15 nopadding-xs"><uice-navbar-nav-editor ng-model="item.navbarConf.main" use-url-id="true" cms-model-item="item" ng-if="$bulk.ready &amp;&amp; !$bulk.loading"></uice-navbar-nav-editor></div></div></div></div></fieldset><a-footer-toolbar on-save="save()" current-user-permission-update="site_settings.change_CmsSettings"></a-footer-toolbar>' + ''),
      $pageTitle: 'Site menu',
      $bulk: function() {
        return operations;
      }
    });
  }]);

}).call(this);
;
angular.module('ui.cms').run(['gettextCatalog', function (gettextCatalog) {
/* jshint -W100 */
    gettextCatalog.setStrings('cz', {"(+{{item.to.length-1}} more)":"(+{{item.to.length-1}} more)","(png-files allowed, max 5mb)":"(soubory png povoleny, max. 5 MB)","301 - Moved Permanently":"301 - Trvale přesunuto","302 - Moved Temporarily":"302 - Dočasné přesunuto","<b>{{username}}</b>, уou are not allowed to access this page":"<b>{{username}}</b>, nemáte přístup na tuto stránku","Actions":"Akce","Add":{"append":"Přidat"},"Add PDF template":"Přidat šablonu PDF","Add SMS notification":"Přidejte oznámení SMS","Add Service":"Přidat službu","Add article":"Přidat článek","Add card":"Přidat kartu","Add card after current":"Přidat kartu za aktuální","Add card before current":"Přidat kartu před aktuální","Add category for album":"Přidat kategorii pro album","Add category for article":"Přidat kategorii pro článek","Add group":"Přidat skupinu","Add keyword":"Přidat klíčové slovo","Add new":"Přidat nový","Add notification":"Přidat upozornění","Add picture album":"Přidat album obrázků","Add site review":"Přidat recenzi webu","Add slide":"Přidat snímek","Add slide after current":"Přidat snímek po aktuálním","Add slide before current":"Přidat snímek před aktuálním","Add third-party service":"Přidat službu třetí strany","Add user":"Přidat uživatele","Add variable description":"Přidat popis proměnné","Add watermark to all images":"Přidat vodoznak ke všem přidaným obrázkům","Additional permissions":"Další oprávnění","Admin":"Administrátor","Admin has all permissions":"Správce má všechna oprávnění","Album categories":"Kategorie alb","Album description":"Popis alba","Album title":"Název alba","All modules":"Všechny moduly","All ok":"Vše OK","Allowed currency":"VIditelné měny na stránce","Alternate logo":"Alternativní logo","An interval to cycle through the slides(msec)":"Interval přepínání slidů (msek)","Anonimization request":"Žádost o anonymizaci","Anonimize data":"Anonymizovat údaje","Application data":"Aplikační data","Apply":"Aplikovat","Apply translation":"Uplatnit gřeklad","Apply watermark to server models":"Přidávat vodoznak k serverovým modelům","Article":"Článek","Article categories":"Kategorie článků","Articles":"Články","Available actions for user data":"Dostupné akce pro uživatelská data","Available keywords":"Dostupná klíčová slova","Backend":{"plugin backend":"Backend"},"Base self hosted prerender":"Základní předobslužný host","Body":"Text","Bottom":"Dole","Bottom left":"Vlevo dole","Bottom right":"Vpravo dole","CMS Admin":"CMS Admin","Can add {{modelName}}":{"user permission":"Může přidat {{modelName}}"},"Can change {{modelName}}":{"user permission":"Může změnit {{modelName}}"},"Can delete {{modelName}}":{"user permission":"Může smazat {{modelName}}"},"Can user disable it?":"Může to uživatel vypnout?","Can view admin page":{"user permission":"Může zobrazit stránku správce"},"Can view glossary":{"user permission":"Může zobrazit glosář"},"Can view {{modelName}}":{"user permission":"Může zobrazit {{modelName}}"},"Can't send email":"Nelze odesílat email","Can't send sms":"Nelze odesílat SMS","Cancel":"Zrušit","Card":"Karta","Card content":"Obsah karty","Cards":"Karty","Categories":"Kategorie","Categories can have a hierarchy. You might have a \"Music\" category, and under that have children categories for \"Pop\" and \"Rock\". It's optional":"Kategorie mohou mít hierarchii. Možná máte kategorii „Hudba“ a pod ní kategorie dětí pro „Pop“ a „Rock“. Je to volitelné","Category for album":"Kategorie alba","Category for article":"Kategorie článku","Category for picture album":"Kategorie pro obrázkové album","Center":"Centrum","Chat":"Chat","Check your email":"Zkontrolujte svůj e-mail","Child categories":"Kategorie dětí","Choose picture for card":"Vyberte obrázek pro kartu","Choose picture for slide":"Byberte obrázek pro slide","Click 'Add' button in this window":"Stiskněte v tomto okně 'Přidat'","Close":"Zavřít","Collect information":"Sbírat informace","Comment(notes for translators)":"Komentář(poznámky pro překladatele)","Content":"Content","Content language":"Obsah pro jazyk","Copy code from https://admin.jivosite.com":"Zkopírujte kód z https://admin.jivosite.com","Copy here chatflow code from https://chatflow.io":"Zkopírujte sem chatflow kód z https://chatflow.io","Copy here disqus universal code from https://disqus.com":"Zkopírujte zde disqus univerzální kód z https://disqus.com","Copy here facebook pixel code from Event Manager page for this site":"Zkopírujte sem facebookový pixel kód ze stránky Správce událostí pro tento web","Copy here script tag from Settings-Counter page in Yandex Metrica for site":"Zde zkopírujte script tag z stránky Nastavení - Counter v Yandex Metrice","Copy here script tag from Tracking Info page in Google Analytics for site":"Zkopírujte sem script-tag ze stránky \"Sledovací kód\" do Google Analytics pro tento web","Copy here script tag from Tracking Info page in Google Tag Manager for site":"Zde zkopírujte tag skriptu ze stránky Tracking Info ve aplikace Google Tag Manager","Copy the meta tag from <a href=\"{{integrationLink(integration.type)}}\" target=\"_blank\">this page</a>":"Zkopírujte metaznačku <a href=\"{{integrationLink(integration.type)}}\" target=\"_blank\">odtud</a>","Create":"Vytvořit","Create new":"Vytvořit nový","Created":"Vytvořeno","Currency":"Měna","Currency converter data":"Data měnového měniče","Custom integrations":"Vlastní integrace","Dashboard":"Ovládací panel","Data":"Data","Database":"Databáze","Database and media files":"Databázové a mediální soubory","Default currency":"Základní měna","Default value":"Výchozí hodnota","Delete":"Smazat","Delete all selected objects?":"Smazat všechny vybrané objekty?","Delete data from database":"Smazat data z databázy","Deletion request":"Žadost na smazání","Description":"Popis","Description &amp; Variables":"Popis & amp; Proměnné","Details":"Podrobnisti","Disable the looping of slides":"Vypnout cyklické přepínání slidů","Disable the transition animation between slides":"Vypnout animaci pri přepínání slidů","Disqus comments":"Disqus komentáře","Do not close page while exporting/importing is in progress":"Nezavírejte stránku, když probíhá export / import","Do not forget to reload page after changing settings in this tab":"Po změně nastavení na této kartě nezapomeňte stránku znovu načíst","Do not use link":"Nepoužívejte odkaz","Document footer":"Zápatí dokumentu","Document header":"Záhlaví dokumentu","Document margins":"Okraje dokumentu","Don't pause slider on mouseover":"Nezastavovat přepínání slidů při navedení myši na slider","Dump resources":"Dumpovat zdroje","Echo mail":"Echo mail","Echo sms":"Sms echo","Edit":"Upravit","Edit GDPR request":"Upravit požadavek GDPR","Edit PDF template":"Upravit šablonu PDF","Edit SMS notification":"Upravit oznámení SMS","Edit article":"Upravit článek","Edit category for album":"Upravit kategorii alba","Edit category for article":"Upravit kategorii článku","Edit group":"Upravit skupinu","Edit keyword":"Upravit klíčové slovo","Edit notification":"Upravit ozámení","Edit picture album":"Upravit album obrázků","Edit site review":"Upravit recenzi webu","Edit translation":"Upravit překlad","Edit user":"Upravit uživatele","Editor":"Editor","Email":"Email","Email alredy registered":"Email už je zaregistrován","Email notifications":"E-mailové oznámení","Email subject":"Předmět emailu","Email template":"Šablona dopisu","Emails":"E-mailové adresy","Enabled email notifications":"Povolené e-mailová upozornění","Enabled notifications":"Povolená oznámení","Enabled sms notifications":"Povolená SMS upozornění","Error on {{siteBrandFile}} upload":"Chyba při nahrávání {{siteBrandFile}}","Error. Can't update settings. Try again later":"Chyba. Nelze aktualizovat nastavení. Zkuste to později","Error. The main page cannot be saved. Try again later":"Chyba. Hlavní stránku nelze uložit. Zkuste to později","Error. The object cannot be saved":"Chyba. Objekt nelze uložit","Error. Try again later":"Chyba. Zkuste to později","Errors in template":"Chyby v šabloně","Example":"Příklad","Export":"Exportovat","Export to 7zip archive":"Exportovat do 7zip archivu","Exporting/importing app data could take some time":"Export / import dat aplikace může nějakou dobu trvat","External link":"Externí odkaz","Facebook Pixel":"Facebook Pixel","Failed to restore from file":"Obnovení ze souboru se nezdařilo","Favicon":"Favicon","Featured items":"Oblíbené","File {{options.restoreFromFile.name}}":"Soubor {{options.restoreFromFile.name}}","First Name":"Jméno","Footer height (mm)":"Výška zápatí (mm)","Form success response":"Text formy pri úspěšném odeslání","Form {{$index+1}}(e.g. \"{{gettext_form}}\")":"Forma {{$index+1}}(směr., \"{{gettext_form}}\")","From url":"Z adresy URL","Full url to prerender site":"Plná adresa URL k prerenderu","GDPR Requests":"GDPR Žádosti","GDPR requests":"GDPR Žádosti","GDPR widget":"GDPR widget","Gallery":"Galerie","Generate password":"Generovat heslo","Glossary":"Glossary","Google AdSense":"Google AdSense","Google Analytics":"Google Analytics","Google Search Console":"Google Search Console","Google Tag Manager":"Google Tag Manager","Google Translate":{"Варианты перевода от...":"Google Translate"},"Google Translate API key":"Google Translate API key","Google maps API key":"Google maps API key","Group permissions":"Skupinová oprávnění","Group settings":"Nastavení skupiny","Groups":"Skupiny","Header & footer":"Header & footer","Header &amp; footer":"Záhlaví & amp; zápatí","Header height (mm)":"Výška záhlaví (mm)","Headline(city or address, etc.)":"Nadpis (město nebo adressa, a tp.)","Here you can add links to facebook, twitter, etc. accounts":"Zde můžete přidat odkazy na facebook,twitter a jiné účty","Here you can add support for":"Zde můžete přidat podporu pro","Host":"Host","Http code":"Http kód","Icon":"Ikonka","Identifier":"Identifikátor","If you are redirecting from one domain to the base, you have to specify the full record of the base domain":"Pokud přesměrujete z jedné domény na základnu, musíte zadat úplný záznam základní domény","If you redirect within the base domain, then prefixes or domains need not be specified":"Pokud přesměrujete v základní doméně, nemusí být předpony nebo domény zadány","Import from 7zip archive":"Importovat z archivu 7zip","Imported from: {{item.importedFrom}}":"Importováno z: {{item.importedFrom}}","Information request":"Žádost o informace","Integration with":"Integrace s","Integrations":"Integrace","Invalid chars":"Neplatné znaky","Invalid chars in username":"Nepřípustné znaky ve jméně uživatele","Invalid example (for the target url, the prefix and domain are missing)":"Neplatný příklad (cílová adresa URL, předpona a doména chybí)","Is published":"Je zveřejněno","Keyword":"Klíčové slovo","Keywords":"Klíčová slova","Language":"Jazyk","Languages":"Jazyky","Last Name":"Příjmeni","Leave empty for localhost":"Nechte prázdné pro localhost","Left":"Vlevo","Link":"Odkaz","Link to article":"Odkaz na článek","Link to site. Ex.: google.com":"Odkaz na jiný web, napr. google.com","Links to facebook, twitter, etc. social accounts":"Odkazy na facebook, twitter a jiné účty","Localization":"Lokalizace","Login":"Přihlásit se","Logo":"Logo","Mail Notifications":"E-mailová oznámení","Mail notifications":"E-mailová oznámení","Mail recipient":"Příjemci dopisů","Main":"Základní","Main Page":"Základní stránka","Main page updated":"Hlavní stránka je obnovena","Maps":{"$$noContext":"Mapy","World maps":"Mapy"},"Media editor":"Editor médií","Media files":"Média soubory","Menu":"Menu","Menu editor":"Editor menu","Modules":"Moduly","Move card left":"Posunout kartu doleva","Move card right":"Posunout kartu doprava","Move slide left":"Posunout snímek doleva","Move slide right":"Posunout snímek doprava","Name for group":"Název skupiny","New redirections from url":"Nové přesměrování z adresy URL","Next Article":"Další článek","Next Product":"Další produkt","Next object":"Další object","No cards":"Žádné karty","No contacts":"Žádné kontakty","No links":"Žádné odkazy","Notification description":"Popis oznámení","Notification mail":"E-mailové upozornění","Notification name":"Název upozornění","Notification sms":"SMS upozornění","Ok":"Ok","Only a-z, 0-9, and -_ allowed":"Přípustné jsou pouze a-z, 0-9,  -_ symboly","Only a-z, 0-9, and -_. allowed":"Přípustné jsou pouze a-z, 0-9,  -_. symboly","Oops! Can't delete object. Try again later":"Ups! Nepodařilo se smazat objekt. Zkuste to později","Oops! Can't save object. Try again later":"Ups! Nepodeřilo se uložit objekt. Zkuste to později","PDF templates":"Šablony PDF","Page":"Stránka","Parent category":"Rodičovská kategorie","Password":"Heslo","Password doesn't match":"Hesla se neshodují","Password for smsapi.com":"Heslo pro smsapi.com","Paste here any html tags that you want to add to document.head":"Vložte sem všechny html tagy, které chcete přidat do document.head","Paste here html code for site":"Vložte sem html-kód pro web","Paste here meta tag":"Vložte meta-teg sem","Paste here meta tag from Alternate methods in Google Search Console for site":"Vložte sem meta-teg ze záložky \"Alternativní metoda\" v Google Search Console pro web","Paste here script tag from Google AdSense":"Vložte sem značku skriptu z Google AdSense","Permissions":"Oprávněni","Permissions disabled for this site":"Oprávnění pro tento web deaktivována","Permissions for '{{userRole}}":{"user permission group":"Oprávnění pro '{{userRole}}"},"Phone":"Telefonní číslo","Phones":"Telefonní čísla","Picked keywords":"Vybraná klíčová slova","Picture":"Obrázek","Picture Gallery":"Galerie obrázků","Picture albums":"Obrázková alba","Please specify a semantic ID, that will be used later to form the search engine-friendly URL":"Zadejte prosím sematický identifikátor,ktery pak bude sloužit k vytvoření přátelských URL adres pro odběratele, např google.com","Please wait":"Prosím, vyčkejte","Please, don't close this page":"Tuto stránku prosím nezavírejte","Plural":"Množné","Popular items":"Populární","Port":"Port","Preferred language":"Preferovaný jazyk","Prerender for web crawlers":"Prerender pro webové crawlery","Prerender.io":"Prerender.io","Preview":"Náhled","Previous Article":"Předchozí článek","Previous Product":"Předchozí produkt","Previous object":"Předchozí object","Price text":"Cena","Public contacts":"Veřejně viditelné kontakty","Publication date":"Datum publikaci","Published":"Publikováno","Real email login at mail.google.com":"Skutečné e-mailové jmeno (login) na mail.google.com","Real email password at mail.google.com":"Skutečné heslo e-mailu na adrese mail.google.com","Recipient":"Příjemce","Redirect rule already exists":"Pravidlo přesměrování již existuje","Redirect rules restrictions":"Omezení přesměrování pravidel","Redirections":"Přesměrování","Regular user":"Běžný uživatel","Remove":"Smazat","Remove current card":"Odebrat aktuální kartu","Remove current slide":"Odebrat aktuální snimek","Repeat password":"Zopakujte heslo","Required fields":"Požadovaná pole","Resources":"Zdroje","Restore failure":"Obnovit chybu","Restore resources":"Obnovovat zdroje","Restored from file":"Obnoveno ze souboru","Return to Google Search Console and click \"VERIFY\" button":"Vrat´te se do   Google Search Console a stiskněte tlačítko \"OVĚŘTE\" tlačítko","Return to Yandex Webmaster and click \"VERIFY\" button":"Vraťte se do Yandex Webmaster a klikněte na \"OVĚŘTE\" tlačítko","Review":"Komentář","Review(text)":"Komentář(text)","Right":"Vpravo","Robots.txt":"Robots.txt","Robots.txt content":"Robots.txt content","Rule №":"Pravidlo č","SEO":"SEO","SMS notifications":"SMS upozornění","Save":"Ulžit","Saved":"Uloženo","Select article category":"Vyberte kategorii článku","Select keyword":"Vyberte klíčové slovo","Send":"Odeslat","Send echo mail to admin":"Odeslat echo email administrátorovi","Send echo sms to admin":"Odeslat sms echo administrátorovi","Send from email":"Odeslat z e-mailu","Send mail to user":"Odeslat e-mail živateli","Send sms to user":"Odeslat SMS uživateli","Send test email":"Odeslat test e-mail","Send test sms":"Odeslat test SMS","Sender name (see https://ssl.smsapi.com/sms_settings/sendernames)":"Jméno uživatele (na https://ssl.smsapi.com/sms_settings/sendernames)","Sendgrid API key":"Sendgrid API klíč","Set user password":"Nastavit uživatelské heslo","Settings":"Nastavení","Settings were updated successfully":"Nastavení byla aktualiována","Singular":"Jednotné","Site Menu":"Menu webu","Site Reviews":"Recenze o webu","Site description":"Krátký textový popis webu","Site menu":"Menu","Site redirection rules":"Pravidla přesměrování stránek","Site reviews":"Recenze stránek","Site title":"Název webu","Slide":"Slide","Slide content":"Obsah snímku","Slider":"Slider","Slider is empty":"Slider je prázdný","Slider settings":"Nastavení slideru","Slides":"Slidy","Sms":"SMS","Sms Notifications":"SMS Upozornění","Sms notifications":"SMS upozornění","Sms recipient":"Příjemci SMS","Social links":"Sociální stránky","Social media":"Sociální média","Source text":"Zdrojvý text","Special offer":"Speciální nabídka","State":"Status","Static content translations":"Překlady statického obsahu","Status":"Status","Subtitle":"Titulky","Template engine output":"Produkt modulu zpracování šablon","Template variables description":"Popis proměnných šablony","Templates for Pdf generator":"Šablony pro generátor PDF","Test":"Test","Text":"Text","The archive with the dump is too large for you to download through the browser. Enter your email and when the archive is ready, you will be sent a link to it":"Archiv s výpisem paměti je příliš velký na to, abyste jej mohli stáhnout prostřednictvím prohlížeče. Zadejte svůj e-mail a až bude archiv připraven, bude vám na něj zaslán odkaz","There is nothing in here":"Tady nic není","Third-party service":"Servis třetí strany","This field is multilingual":"To to pole je vícejazyčné","Timezone":"Časové pásmo","Title":"Záhlaví","Title(name or email, etc.)":"Záhlaví(jméno, email, a t.p.)","To url":"Na adresu URL","Toggle password visibility":"Přepnout viditelnost hesla","Token for prerender.io (copy token from <a href=\"https://prerender.io/install-token\">https://prerender.io/install-token</a>)":"Token pro prerender.io (zkopírujte token z <a href=\"https://prerender.io/install-token\">https://prerender.io/install-token</a>)","Top":"Top","Top left":"Vlevo nahoře","Top right":"Vpravo nahoře","Translation":"Překlad","Translation service":"Překladatelská služba","Translations":"Překlady","Type":"Druh","Type email":"Zadejte email","Type phone":"Zadejte telefonní číslo","Type to search":"Zadejte vyhledávání","Type translation for text":"Vložte překlad pro text","Unique Identifier":"Jedinečný identifikátor","Unique identifier":"Jedinečný identifikátor","Universal Analytics tracking code":"Sledovací kód Universal Analytics","Upload your watermark":"VLožte svůj vodoznak","Url to be redirected to could be specified as http://, https:// or just":"Adresa URL, na kterou chcete být přesměrována, lze zadat jako http: //, https: // nebo jen","Use TLS":"Použit TLS","Use email notifications for user":"Používat e-mailová oznámení pro uživatele","Use exchange rates from ECB":"Použijte směnné kurzy od ECB","Use footer":"Používat footer","Use header":"Používat header","Use integrations with map-provider":{"World maps":"Použijte integrace s poskytovatelem map"},"Use integrations with translation service (for admin page only)":"Použít integrace s překladatelskou službou (pouze pro stránku správce)","Use prerender":"Používat prerender","Use sms notifications for user":"Používat SMS upozornění od uživatele","User":"Uživatel","User role":"Typ uživatele","User's SMS settings":"Nastavení SMS uživatele","User's email settings":"Nastavení emailu pro uživatele","User's sms settings":"Nastavení SMS pro uživatele","Username":"Login uživatele","Username alredy taken":"Jméno uživatele už je použito","Username for smsapi.com":"Jméno uživatele pro smsapi.com","Users":"Uživatelé","Variable":"Proměnná","Variable description":"Popis proměnné","Variable name":"Název proměnné","Variable type":"Typ proměnné","Variables":"Proměnné","Variables that can be used in templates":"Proměnné, které lze použít v šablonách","View":{"View items":"Pohled"},"View as":"Zobrazit jako","Visible languages on landing/site":"Viditelné jazyky na lendingu/webu","Watermark":"Voduynak","Watermark position":"Pozice vodoznaku","Web crawler's user agent - regular expression (see more at <a href=\"https://docs.python.org/2/library/re.html\" target=\"_blank\">this link</a>)":"Uživatelský agent webového prohledávače - regulární výraz (více na adrese <a href=\"https://docs.python.org/2/library/re.html\" target=\"_blank\">tento odkaz</a>)","Without translation":"Bez překladu","Without widget":"Bez widgetu","Yandex Metrica":"Yandex Metrics","Yandex Webmaster":"Yandex Webmaster","You've added all available integrations":"Přidali jste všechny dostupné integrace","Your email":"Váš email","any other SMTP-server":"jakýkoli jiný server SMTP","at the same time, if the user visited the site not through https, but through http, then he will be redirected to http version":"pokud uživatel navštívil web ne přes https, ale přes http, pak bude přesměrován na http verzi","click 'Edit' button to add contacts":"stiskněte tlacítko \"Upravit\" pro přidání kontaktu","click 'Edit' button to add social links":"stiskněte tlačítko \"Upravit\" pro přidání odkazů na sosiální sítě","cm":{"centimeter":"cm"},"invalid email":"špatný email","invalid file type":"neplatný typ souboru","it's not clear where to redirect the user":"není jasné, kam přesměrovat uživatele","required":"povinné pole","restore":"obnovit","restore initial data":"obnovit původní údaje","this identifier is already in use":"identifikátor už je zabraný","watermark should be in PNG format with transparent background":"vodoznakem musí být obrázek v PNG formátě s průhledným pozadím","watermark will be added to all images, except for the previews":"vodoznak bude přidán ke všem obrázkům, s výjimkou náhledů","you should allow less secure app access to your email. See <a href=\"https://support.google.com/accounts/answer/6010255\" target=\"_blank\">this link</a>":"měli byste umožnit méně bezpečný přístup aplikace k e-mailu. Viz <a href=\"https://support.google.com/accounts/answer/6010255\" target=\"_blank\">tento odkaz</a>","{{siteBrandFile}} changed":"{{siteBrandFile}} změněno"});
    gettextCatalog.setStrings('ru', {"(+{{item.to.length-1}} more)":"(+{{item.to.length-1}} и более)","(for client)":"(для браузера-клиента)","(for server)":"(для сервера)","(png-files allowed, max 5mb)":"(png файл, макс. размер - 5 мб)","301 - Moved Permanently":"301 - Перемещено навсегда","302 - Moved Temporarily":"302 - Перемещено временно","<b>{{username}}</b>, уou are not allowed to access this page":"<b>{{username}}</b>, вы не можете просматривать эту страницу","Actions":"Действия","Add":{"append":"Добавить"},"Add PDF template":"Новый Pdf-шаблон","Add SMS notification":"Добавить SMS-уведомление","Add Service":"Добавить Cервис","Add article":"Добавить статью","Add card":"Добавить карточку","Add card after current":"Добавить карточку после текущей","Add card before current":"Добавить карточку перед текущей","Add category for album":"Добавить категорию для альбома","Add category for article":"Добавить категорию для статьи","Add group":"Добавить группу","Add keyword":"Добавить ключевое слово","Add new":"Создать","Add notification":"Добавить уведомление","Add picture album":"Добавить альбом картинок","Add site review":"Добавить отзыв о сайте","Add slide":"Добавить слайд","Add slide after current":"Добавить слайд после текущего","Add slide before current":"Добавить слайд перед текущим","Add third-party service":"Добавить сторонний сервис","Add user":"Добавить пользователя","Add variable description":"Добавить описание переменной","Add watermark to all images":"Добавлять водяной знак ко всем загружаемым изображениям","Additional permissions":"Дополнительные разрешения","Admin":"Администратор","Admin has all permissions":"У админа есть все разрешения","Album categories":"Категории альбомов","Album description":"Описание альбома","Album title":"Заголовок альбома","All modules":"Все модули","All ok":"Все ОК","Allowed currency":"Видимые валюты на сайте","Alternate logo":"Альтернативный логотип","An interval to cycle through the slides(msec)":"Интервал переключения слайдов(мсек)","Anonimization request":"Запрос на анонимизацию","Anonimize data":"Анонимизировать данные","Application data":"Данные приложения","Apply":"Применить","Apply translation":"Применить перевод","Apply watermark to server models":"Добавлять водяной знак к серверным моделям","Article":"Статья","Article categories":"Категории статей","Articles":"Статьи","Available actions for user data":"Доступные действия над пользовательскими данными","Available keywords":"Доступные ключевые слова","Backend":{"plugin backend":"Бэкенд"},"Base self hosted prerender":"Self-hosted пререндер","Body":"Текст","Bottom":"Низ, см","Bottom left":"Низ-лево","Bottom right":"Низ-право","CMS Admin":"CMS Admin","Can add {{modelName}}":{"user permission":"Может создавать {{modelName}}"},"Can change {{modelName}}":{"user permission":"Может изменять {{modelName}}"},"Can delete {{modelName}}":{"user permission":"Может удалять {{modelName}}"},"Can user disable it?":"Пользователь может отключить?","Can view admin page":{"user permission":"Может просматривать админку"},"Can view glossary":{"user permission":"Может просматривать справочник"},"Can view {{modelName}}":{"user permission":"Может просматривать {{modelName}}"},"Can't send email":"Отослать письмо не получилось","Can't send sms":"Отослать смс не получилось","Cancel":"Отменить","Card":"Карточка","Card content":"Контент карточки","Cards":"Карточки","Categories":"Категории","Categories can have a hierarchy. You might have a \"Music\" category, and under that have children categories for \"Pop\" and \"Rock\". It's optional":"Категории могут иметь иерархию. Например, вы можете создать категорию \"Музыка\", внутри которой будут 2 дочерние категории \"Популярная\" и \"Рок\". Это опционально","Category for album":"Категория для альбома","Category for article":"Категория для статьи","Category for picture album":"Категория для альбома","Center":"Центр","Chat":"Чат","Check your email":"Проверьте ваш email","Child categories":"Дочерние категории","Choose picture for card":"Выберите изображение для карточки","Choose picture for slide":"Выберите изображение для слайда","Click 'Add' button in this window":"Нажмите в этом окне 'Добавить'","Close":"Закрыть","Collect information":"Собрать информацию","Comment(notes for translators)":"Комментарий(замечания для переводчиков)","Content":"Контент","Content language":"Контент для языка","Copy code from https://admin.jivosite.com":"Скопируйте html-код чата из https://admin.jivosite.com","Copy here chatflow code from https://chatflow.io":"Скопируйте html-код чата из https://chatflow.io","Copy here disqus universal code from https://disqus.com":"Скопируйте html-код чата из https://disqus.com","Copy here facebook pixel code from Event Manager page for this site":"Вставьте сюда код facebook-pixel из  Event Manager-а для этого сайта","Copy here script tag from Settings-Counter page in Yandex Metrica for site":"Скопируйте код со страницы Настройки-Счетчик на сайте с Яндекс Метрикой","Copy here script tag from Tracking Info page in Google Analytics for site":"Скопируйте сюда script-тег со страницы \"Код отслеживания\" в Google Analytics для сайта","Copy here script tag from Tracking Info page in Google Tag Manager for site":"Скопируйте сюда script-тег со страницы  в Google Tag Manager для сайта","Copy the meta tag from <a href=\"{{integrationLink(integration.type)}}\" target=\"_blank\">this page</a>":"Скопируйте meta-тег <a href=\"{{integrationLink(integration.type)}}\" target=\"_blank\">отсюда</a>","Create":"Создать","Create new":"Создать","Created":"Создано","Currency":"Валюта","Currency converter data":"Данные для конвертера валют","Custom integrations":"Сторонние сервисы, интеграции","Dashboard":"Панель управления","Data":"Данные","Database":"База данных","Database and media files":"База данных и медиа-файлы","Default currency":"Валюта по-умолчанию","Default value":"Значение по-умолчанию","Delete":"Удалить","Delete all selected objects?":"Удалить все выбранные объекты?","Delete data from database":"Удалить данные из базы данных","Deletion request":"Запрос на удаление данных","Description":"Описание","Description &amp; Variables":"Описание и переменные","Details":"Подробнее","Disable the looping of slides":"Отключить цикличное перелистывание слайдов","Disable the transition animation between slides":"Отключить анимацию при перелистывании слайдов","Disqus comments":"Комментарии Disqus","Do not close page while exporting/importing is in progress":"Не закрывайте страницу, пока происходит импорт/экспорт","Do not forget to reload page after changing settings in this tab":"Не забудьте перезагрузить страницу после внесения изменений в этом табе","Do not use link":"Не использовать ссылку","Document footer":"Футер документа","Document header":"Шапка документа","Document margins":"Отступы документа","Don't pause slider on mouseover":"Не останавливать перелистывание слайдов при наведении мышки на слайдер","Dump resources":"Архивирование ресурсов","Echo mail":"Письмо-эхо","Echo sms":"СМС-эхо","Edit":"Редактировать","Edit GDPR request":"Редактирование GDRP-запроса","Edit PDF template":"Ред. Pdf-шаблон","Edit SMS notification":"Редактирование SMS-уведомления","Edit article":"Редактирование статьи","Edit category for album":"Редактирование категории для альбома","Edit category for article":"Редактирование категории для статьи","Edit group":"Редактировать группу","Edit keyword":"Редактирование ключевого слова","Edit notification":"Редактировать уведомление","Edit picture album":"Редактирование альбома картинок","Edit site review":"Редактирование отзыва о сайте","Edit translation":"Редактировать перевод","Edit user":"Редактирование пользователя","Editor":"Редактор","Email":"Email","Email alredy registered":"Email уже зарегистрирован","Email notifications":"Уведомления по почте","Email subject":"Тема письма","Email template":"Шаблон письма","Emails":"Почтовые ящики","Enabled email notifications":"Включенные почтовые уведомления","Enabled notifications":"Включенные уведомления","Enabled sms notifications":"Включенные СМС уведомления","Error on {{siteBrandFile}} upload":"Ошибка при загрузке {{siteBrandFile}}","Error. Can't update settings. Try again later":"Ошибка. Не получилось сохранить настройки. Попытайтесь позже снова","Error. The main page cannot be saved. Try again later":"Ошибка! Не получилось сохранить основную страницу. Попытайтесь позже снова","Error. The object cannot be saved":"Ошибка! Не получилось сохранить страницу","Error. Try again later":"Ошибка. Попытайтесь позже снова","Errors in template":"Ошибки в шаблоне","Example":"Пример","Export":"Экспортировать","Export to 7zip archive":"Экспортировать в 7zip архив","Exporting/importing app data could take some time":"Импорт/экспорт данных может занять много времени","External link":"Внешняя ссылка","Failed to restore from file":"Не получилось восстановить из архива","Featured items":"Избранное","File {{options.restoreFromFile.name}}":"Файл {{options.restoreFromFile.name}}","First Name":"Имя","Footer height (mm)":"Высота футера (мм)","Form success response":"Текст формы при успешной отправке","Form {{$index+1}}(e.g. \"{{gettext_form}}\")":"Форма {{$index+1}}(напр., \"{{gettext_form}}\")","From url":"С url","Full url to prerender site":"Полный url к пререндеру","GDPR Requests":"Запросы по GDPR","GDPR requests":"Запросы по GDPR","GDPR widget":"Виджет для GDPR","Gallery":"Галерея","Generate password":"Сгенерировать пароль","Glossary":"Справочник","Google Analytics":"Google Analytics","Google Search Console":"Google Search Console","Google Translate":{"Варианты перевода от...":"Google Translate"},"Google Translate API key":"API-ключ для Google Translate","Google maps API key":"API-ключ для Google maps","Group permissions":"Групповые разрешения","Group settings":"Настройки групп","Groups":"Группы","Header & footer":"Шапка и футер","Header &amp; footer":"Шапка и футер","Header height (mm)":"Высота шапки (мм)","Headline(city or address, etc.)":"Основная строка(город или адрес, т.п.)","Here you can add links to facebook, twitter, etc. accounts":"Тут вы можете добавить ссылки на facebook, twitter и др. аккаунты","Here you can add support for":"Тут вы можете добавить поддержку для","Host":"Хост","Http code":"Http-код","Icon":"Иконка","Identifier":"Идентификатор","If you are redirecting from one domain to the base, you have to specify the full record of the base domain":"Если вы перенаправляете с одного домена на базовый, то вы должны указывать полную запись базового домена","If you redirect within the base domain, then prefixes or domains need not be specified":"Если вы перенаправляете в пределах базового домена, то указывать префиксы или домены не обязательно","Import from 7zip archive":"Импортировать из 7zip архива","Imported from: {{item.importedFrom}}":"Импортировано из: {{item.importedFrom}}","Information request":"Запрос на получение информации","Integration with":"Интеграция с","Integrations":"Сторонние сервисы, интеграции","Invalid chars":"Недопустимые символы","Invalid chars in username":"Недопустимые символы в имени пользователя","Invalid example (for the target url, the prefix and domain are missing)":"Неправильный пример (для целевого url пропущен префикс и домен)","Is published":"Опубликовано","Keyword":"Ключевое слово","Keywords":"Ключевые слова","Language":"Язык","Languages":"Языки","Last Name":"Фамилия","Leave empty for localhost":"Оставьте поле пустым для localhost","Left":"Лево, см","Link":"Ссылка","Link to article":"Ссылка на статью","Link to site. Ex.: google.com":"Ссылка на сайт. Напр.: google.com","Links to facebook, twitter, etc. social accounts":"Ссылки на facebook, twitter и др. аккаунты","Localization":"Локализация","Login":"Логин","Logo":"Логотип","Mail Notifications":"Почтовые уведомления","Mail notifications":"Почтовые уведомления","Mail recipient":"Получатели писем","Main":"Основное","Main Page":"Основная страница","Main page updated":"Основная страница обновлена","Maps":{"$$noContext":"Карты","World maps":"Карты"},"Media editor":"Медиа редактор","Media files":"Медиа-файлы","Menu":"Меню","Menu editor":"Редактор меню","Modules":"Модули","Move card left":"Передвинуть карточку влево","Move card right":"Передвинуть карточку вправо","Move slide left":"Передвинуть слайд влево","Move slide right":"Передвинуть слайд вправо","Name for group":"Название для группы","New redirections from url":"Новое перенаправление для url","Next Article":"Следующая Статья","Next Product":"Следующий Товар","Next object":"Следующий объект","No cards":"Нет карточек","No contacts":"Нет контактов","No links":"Нет ссылок","Notification description":"Описание уведомления","Notification mail":"Почтовое уведомление","Notification name":"Название уведомления","Notification sms":"СМС уведомление","Ok":"Ок","Only a-z, 0-9, and -_ allowed":"Допустимы только a-z, 0-9,  -_ символы","Only a-z, 0-9, and -_. allowed":"Допустимы только a-z, 0-9,  -_. символы","Oops! Can't delete object. Try again later":"Ой! Не получилось сохранить объект. Попытайтесь позже снова","Oops! Can't save object. Try again later":"Ой! Не получилось сохранить объект. Попытайтесь позже снова","PDF templates":"Pdf-шаблоны","Page":"Страница","Parent category":"Родительская категория","Password":"Пароль","Password doesn't match":"Пароли не совпадают","Password for smsapi.com":"Пароль для smsapi.com","Paste here any html tags that you want to add to document.head":"Вставьте сюда html-теги, которые вы хотите добавить в document.head","Paste here html code for site":"Вставьте сюда html-код для сайта","Paste here meta tag":"Вставьте meta-тег сюда","Paste here meta tag from Alternate methods in Google Search Console for site":"Вставьте сюда meta-тег из вкладки \"Альтернативный метод\" в Google Search Console для сайта","Paste here script tag from Google AdSense":"Вставьте сюда script-тег от Google AdSense","Permissions":"Разрешения","Permissions disabled for this site":"Разрешения отключены для этого сайта","Permissions for '{{userRole}}":{"user permission group":"Групповые разрешения для '{{userRole}}"},"Phone":"Телефон","Phones":"Телефоны","Picked keywords":"Выбранные ключевые слова","Picture":"Картинка","Picture Gallery":"Галерея картинок","Picture albums":"Альбомы картинок","Please specify a semantic ID, that will be used later to form the search engine-friendly URL":"Пожалуйста, укажите семантический идентификатор, который потом будет использован для формирования дружественных url для поисковиков, типа google.com","Please wait":"Пожалуйста, подождите","Please, don't close this page":"Пожалуйста, не закрывайте страницу","Plural":"Множественное","Popular items":"Популярное","Port":"Порт","Preferred language":"Предпочтительный язык","Prerender for web crawlers":"Пререндер для веб-краулеров","Preview":"Предпросмотр","Previous Article":"Предыдущая Статья","Previous Product":"Предыдущий Товар","Previous object":"Предыдущий объект","Price text":"Цена","Public contacts":"Публично видимые контактные данные","Publication date":"Дата публикации","Published":"Опубликовано","Real email login at mail.google.com":"Реальный логин на mail.google.com","Real email password at mail.google.com":"Реальный пароль на mail.google.com","Recipient":"Получатель","Redirect rule already exists":"Правило редиректа для этого url уже существует","Redirect rules restrictions":"Ограничения правил редиректа","Redirections":"Перенаправления","Regular user":"Обычный пользователь","Remove":"Удалить","Remove current card":"Удалить текущую карточку","Remove current slide":"Удалить текущий слайд","Repeat password":"Повторите пароль","Required fields":"Обязательные поля","Resources":"Ресурсы","Restore failure":"Восстановить из дампа не получилось","Restore resources":"Восстановление ресурсов","Restored from file":"Восстановлено из файла","Return to Google Search Console and click \"VERIFY\" button":"Вернитесь в  Google Search Console и нажмите кнопку \"VERIFY\"/\"ПОДТВЕРДИТЬ\"","Return to Yandex Webmaster and click \"VERIFY\" button":"Вернитесь в Яндекс Вебмастер и нажмите кнопку \"ПРОВЕРИТЬ\"","Review":"Отзыв","Review(text)":"Отзыв(текст)","Right":"Право, см","Robots.txt content":"Содержимое robots.txt","Rule №":"Правило №","SEO":"SEO","SMS notifications":"СМС уведомления","Save":"Сохранить","Saved":"Сохранено","Select article category":"Выберите категорию для статьи","Select keyword":"Выберите ключевое слово","Send":"Отправить","Send echo mail to admin":"Отправить письмо-эхо администратору","Send echo sms to admin":"Отправить СМС-эхо администратору","Send from email":"Отправить от имени почтового ящика","Send mail to user":"Отправить письмо пользователю","Send sms to user":"Отправить СМС пользователю","Send test email":"Отправить тестовый email","Send test sms":"Отправить тестовое смс","Sender name (see https://ssl.smsapi.com/sms_settings/sendernames)":"Имя отправителя (см https://ssl.smsapi.com/sms_settings/sendernames)","Sendgrid API key":"API-ключ в sendgrid","Set user password":"Установить пароль пользователя","Settings":"Настройки","Settings were updated successfully":"Настройки обновлены","Singular":"Единственное","Site Menu":"Меню сайта","Site Reviews":"Отзывы о сайте","Site description":"Короткое текстовое описание сайта","Site menu":"Меню сайта","Site redirection rules":"Правила перенаправлений url для сайта","Site reviews":"Отзывы о сайте","Site title":"Заголовок сайта","Slide":"Слайд","Slide content":"Контент слайда","Slider":"Слайдер","Slider is empty":"Слайдер пуст","Slider settings":"Настройки слайдера","Slides":"Слайды","Sms":"СМС","Sms Notifications":"СМС уведомления","Sms notifications":"СМС уведомления","Sms recipient":"Получатели СМС","Social links":"Социальные сайты","Social media":"Социальные сайты","Source text":"Исходный текст","Special offer":"Спец. предложение","State":"Статус","Static content translations":"Переводы статического контента","Status":"Статус","Subtitle":"Подзаголовок","Template engine output":"Вывод шаблонной системы","Template variables description":"Описание переменных в шаблоне","Templates for Pdf generator":"Шаблоны для Pdf-генератора","Test":"Тестировать","Text":"Текст","The archive with the dump is too large for you to download through the browser. Enter your email and when the archive is ready, you will be sent a link to it":"Архив с дампом слишком большой для того, чтоб вы могли загрузить его через браузер. Укажите ваш email и когда архив будет готов, вам будет выслана ссылка на него","There is nothing in here":"Тут ничего нет","Third-party service":"Сторонний сервис","This field is multilingual":"Это поле - мультиязычное","Timezone":"Часовой пояс","Title":"Заголовок","Title(name or email, etc.)":"Заголовок(имя, email, и т.п.)","To url":"Перенаправить на url","Toggle password visibility":"Переключить видимость пароля","Token for prerender.io (copy token from <a href=\"https://prerender.io/install-token\">https://prerender.io/install-token</a>)":"Токен для prerender.io (скопируйте токен со страницы <a href=\"https://prerender.io/install-token\">https://prerender.io/install-token</a>)","Top":"Верх, см","Top left":"Верх-лево","Top right":"Верх-Право","Translation":"Перевод","Translation service":"Сервис переводов","Translations":"Переводы","Type":"Тип","Type email":"Наберите email","Type phone":"Наберите номер телефона","Type to search":"Поиск по...","Type translation for text":"Вставьте перевод для текста","Unique Identifier":"Уникальный Идентификатор","Unique identifier":"Уникальный идентификатор","Universal Analytics tracking code":"Код отслеживания Universal Analytics","Upload your watermark":"Загрузите свой водяной знак","Url to be redirected to could be specified as http://, https:// or just":"Url, с которого нужно производить редирект может начинаться с http://, https:// или просто /","Use TLS":"Использовать TLS","Use email notifications for user":"Использовать email уведомления для пользователя","Use exchange rates from ECB":"Использовать курс валют от ЕЦБ","Use footer":"Использовать футер","Use header":"Использовать шапку","Use integrations with map-provider":{"World maps":"Использовать интеграцию с картографическими провайдерами"},"Use integrations with translation service (for admin page only)":"Использовать интеграцию с сервисом переводов строк (доступно только для админки)","Use prerender":"Использовать пререндер","Use sms notifications for user":"Использовать СМС уведомления для пользователя","User":"Пользователь","User role":"Тип пользователя","User's SMS settings":"Настройки для пользователей","User's email settings":"Настройки для пользователей","User's sms settings":"Настройки для пользователей","Username":"Логин пользователя","Username alredy taken":"Имя пользователя уже занято","Username for smsapi.com":"Имя пользователя для smsapi.com","Users":"Пользователи","Variable":"Переменные","Variable description":"Описание переменной","Variable name":"Название переменной","Variable type":"Тип переменной","Variables":"Переменные","Variables that can be used in templates":"Переменные, которые можно использовать в шаблоне","View":{"View items":"Просмотреть"},"View as":"Отобразить как","Visible languages on landing/site":"Видимые языки на лендинге/сайте","Watermark":"Водяной знак","Watermark position":"Позиция водяного знака","Web crawler's user agent - regular expression (see more at <a href=\"https://docs.python.org/2/library/re.html\" target=\"_blank\">this link</a>)":"User Agent веб-краулеров - регулярное выражение (подробности по <a href=\"https://docs.python.org/2/library/re.html\" target=\"_blank\">этой ссылке</a>)","Without translation":"Без перевода","Without widget":"Без виджета","Yandex Metrica":"Яндекс Метрика","Yandex Webmaster":"Яндекс Вебмастер","You've added all available integrations":"Вы добавили все доступные интеграции","Your email":"Ваш email","any other SMTP-server":"любой другой SMTP-сервер","at the same time, if the user visited the site not through https, but through http, then he will be redirected to http version":"при этом, если пользователь зашел на сайт не через https, а через http, то его перенаправят на http версию","click 'Edit' button to add contacts":"нажмите кнопку \"Редактировать\" чтоб добавить контакты","click 'Edit' button to add social links":"нажмите кнопку \"Редактировать\" чтоб добавить ссылки на социальные сети","cm":{"centimeter":"см"},"invalid email":"неправильный email","invalid file type":"неверный тип файла","it's not clear where to redirect the user":"неясно куда перенаправлять пользователя","required":"обязательное поле","restore":"восстановить","restore initial data":"восстановить изначальное значение","this identifier is already in use":"идентификатор уже занят","watermark should be in PNG format with transparent background":"водяным знаком должна быть картинка в PNG формате с прозрачным фоном","watermark will be added to all images, except for the previews":"водяной знак будет добавлен ко всем изображениям кроме превью","you should allow less secure app access to your email. See <a href=\"https://support.google.com/accounts/answer/6010255\" target=\"_blank\">this link</a>":"вы должны разрешить доступ небезопасных приложений к вашему email. Подробности по <a href=\"https://support.google.com/accounts/answer/6010255\" target=\"_blank\">этой сслыке</a>","{{siteBrandFile}} changed":"{{siteBrandFile}} изменен"});
/* jshint +W100 */
}]);