(function() {
    'use strict';
    var authHeader, getHost, urlBase, urlBaseHost;

    urlBase = '/api/v1';

    authHeader = 'Authorization';

    getHost = function(url) {
        var m;
        m = url.match(/^(?:https?:)?\/\/([^\/]+)/);
        if (m) {
            return m[1];
        }
        return null;
    };

    urlBaseHost = getHost(urlBase) || location.host;

    angular.module("lbServices", ['ngResource']).config([
        "$resourceProvider", "$httpProvider", function($resourceProvider, $httpProvider) {
        $resourceProvider.defaults.stripTrailingSlashes = false;
        $httpProvider.defaults.xsrfCookieName = 'csrftoken';
        $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
    }])

    
    .factory("CmsArticle", [
        "CustomResource", "lbAuth", function(Resource, lbAuth) {
            var R;
            R = Resource(urlBase + "/CmsArticle/:id/",
            { 
                forModelField: '@forModelField',
                id: '@id',
            },
            { 
                "count": {
                    url: urlBase + "/CmsArticle/count/",
                    method: "GET", 
                    cancellable: true,
                },
                "create": {
                    url: urlBase + "/CmsArticle/",
                    method: "POST", 
                },
                "deleteFile": {
                    url: urlBase + "/CmsArticle/:id/file/:forModelField/delete/",
                    method: "PATCH", 
                },
                "deleteFilesInsideList": {
                    url: urlBase + "/CmsArticle/:id/fileList/:forModelField/delete/",
                    method: "PATCH", 
                    isArray: true,
                },
                "destroy": {
                    url: urlBase + "/CmsArticle/:id/",
                    method: "DELETE", 
                },
                "findOne": {
                    url: urlBase + "/CmsArticle/find_one/",
                    method: "GET", 
                    cancellable: true,
                },
                "list": {
                    url: urlBase + "/CmsArticle/",
                    method: "GET", 
                    cancellable: true,
                    isArray: true,
                },
                "markAsRead": {
                    url: urlBase + "/CmsArticle/:id/mark_as_read/",
                    method: "PUT", 
                },
                "partialUpdate": {
                    url: urlBase + "/CmsArticle/:id/",
                    method: "PATCH", 
                },
                "retrieve": {
                    url: urlBase + "/CmsArticle/:id/",
                    method: "GET", 
                    cancellable: true,
                },
                "update": {
                    url: urlBase + "/CmsArticle/:id/",
                    method: "PUT", 
                },
                "updateFile": {
                    url: urlBase + "/CmsArticle/:id/file/:forModelField/update/",
                    method: "PUT", 
                },
                "updateFilesInsideList": {
                    url: urlBase + "/CmsArticle/:id/fileList/:forModelField/update/",
                    method: "PUT", 
                    isArray: true,
                },
                "uploadFile": {
                    url: urlBase + "/CmsArticle/:id/file/:forModelField/upload/",
                    headers: {
                        'Content-Type': undefined
                    },
                    method: "POST", 
                },
                "uploadFileToList": {
                    url: urlBase + "/CmsArticle/:id/fileList/:forModelField/upload/",
                    headers: {
                        'Content-Type': undefined
                    },
                    method: "POST", 
                    isArray: true,
                },
            });
            
            R['deleteById'] = R['destroy'];
            R['destroyById'] = R['destroy'];
            R['find'] = R['list'];
            R['findById'] = R['retrieve'];
            R['query'] = R['list'];
            R['updateAttributes'] = R['partialUpdate'];
            R.modelName = "CmsArticle";
            return R;
        }
    ])
    
    .factory("CmsCategoryForArticle", [
        "CustomResource", "lbAuth", function(Resource, lbAuth) {
            var R;
            R = Resource(urlBase + "/CmsCategoryForArticle/:id/",
            { 
                forModelField: '@forModelField',
                id: '@id',
            },
            { 
                "count": {
                    url: urlBase + "/CmsCategoryForArticle/count/",
                    method: "GET", 
                    cancellable: true,
                },
                "create": {
                    url: urlBase + "/CmsCategoryForArticle/",
                    method: "POST", 
                },
                "deleteFile": {
                    url: urlBase + "/CmsCategoryForArticle/:id/file/:forModelField/delete/",
                    method: "PATCH", 
                },
                "destroy": {
                    url: urlBase + "/CmsCategoryForArticle/:id/",
                    method: "DELETE", 
                },
                "findOne": {
                    url: urlBase + "/CmsCategoryForArticle/find_one/",
                    method: "GET", 
                    cancellable: true,
                },
                "list": {
                    url: urlBase + "/CmsCategoryForArticle/",
                    method: "GET", 
                    cancellable: true,
                    isArray: true,
                },
                "partialUpdate": {
                    url: urlBase + "/CmsCategoryForArticle/:id/",
                    method: "PATCH", 
                },
                "retrieve": {
                    url: urlBase + "/CmsCategoryForArticle/:id/",
                    method: "GET", 
                    cancellable: true,
                },
                "update": {
                    url: urlBase + "/CmsCategoryForArticle/:id/",
                    method: "PUT", 
                },
                "updateFile": {
                    url: urlBase + "/CmsCategoryForArticle/:id/file/:forModelField/update/",
                    method: "PUT", 
                },
                "uploadFile": {
                    url: urlBase + "/CmsCategoryForArticle/:id/file/:forModelField/upload/",
                    headers: {
                        'Content-Type': undefined
                    },
                    method: "POST", 
                },
            });
            
            R['deleteById'] = R['destroy'];
            R['destroyById'] = R['destroy'];
            R['find'] = R['list'];
            R['findById'] = R['retrieve'];
            R['query'] = R['list'];
            R['updateAttributes'] = R['partialUpdate'];
            R.modelName = "CmsCategoryForArticle";
            return R;
        }
    ])
    
    .factory("CmsCategoryForPictureAlbum", [
        "CustomResource", "lbAuth", function(Resource, lbAuth) {
            var R;
            R = Resource(urlBase + "/CmsCategoryForPictureAlbum/:id/",
            { 
                id: '@id',
            },
            { 
                "count": {
                    url: urlBase + "/CmsCategoryForPictureAlbum/count/",
                    method: "GET", 
                    cancellable: true,
                },
                "create": {
                    url: urlBase + "/CmsCategoryForPictureAlbum/",
                    method: "POST", 
                },
                "destroy": {
                    url: urlBase + "/CmsCategoryForPictureAlbum/:id/",
                    method: "DELETE", 
                },
                "findOne": {
                    url: urlBase + "/CmsCategoryForPictureAlbum/find_one/",
                    method: "GET", 
                    cancellable: true,
                },
                "list": {
                    url: urlBase + "/CmsCategoryForPictureAlbum/",
                    method: "GET", 
                    cancellable: true,
                    isArray: true,
                },
                "partialUpdate": {
                    url: urlBase + "/CmsCategoryForPictureAlbum/:id/",
                    method: "PATCH", 
                },
                "retrieve": {
                    url: urlBase + "/CmsCategoryForPictureAlbum/:id/",
                    method: "GET", 
                    cancellable: true,
                },
                "update": {
                    url: urlBase + "/CmsCategoryForPictureAlbum/:id/",
                    method: "PUT", 
                },
            });
            
            R['deleteById'] = R['destroy'];
            R['destroyById'] = R['destroy'];
            R['find'] = R['list'];
            R['findById'] = R['retrieve'];
            R['query'] = R['list'];
            R['updateAttributes'] = R['partialUpdate'];
            R.modelName = "CmsCategoryForPictureAlbum";
            return R;
        }
    ])
    
    .factory("CmsCeleryResult", [
        "CustomResource", "lbAuth", function(Resource, lbAuth) {
            var R;
            R = Resource(urlBase + "/CmsCeleryResult/:id/",
            { 
                id: '@id',
            },
            { 
                "findOne": {
                    url: urlBase + "/CmsCeleryResult/find_one/",
                    method: "GET", 
                    cancellable: true,
                },
                "list": {
                    url: urlBase + "/CmsCeleryResult/",
                    method: "GET", 
                    cancellable: true,
                    isArray: true,
                },
                "retrieve": {
                    url: urlBase + "/CmsCeleryResult/:id/",
                    method: "GET", 
                    cancellable: true,
                },
            });
            
            R['find'] = R['list'];
            R['findById'] = R['retrieve'];
            R['query'] = R['list'];
            R.modelName = "CmsCeleryResult";
            return R;
        }
    ])
    
    .factory("CmsCurrencyData", [
        "CustomResource", "lbAuth", function(Resource, lbAuth) {
            var R;
            R = Resource(urlBase + "/CmsCurrencyData/:id/",
            { 
                id: '@id',
            },
            { 
                "count": {
                    url: urlBase + "/CmsCurrencyData/count/",
                    method: "GET", 
                    cancellable: true,
                },
                "create": {
                    url: urlBase + "/CmsCurrencyData/",
                    method: "POST", 
                },
                "destroy": {
                    url: urlBase + "/CmsCurrencyData/:id/",
                    method: "DELETE", 
                },
                "findOne": {
                    url: urlBase + "/CmsCurrencyData/find_one/",
                    method: "GET", 
                    cancellable: true,
                },
                "list": {
                    url: urlBase + "/CmsCurrencyData/",
                    method: "GET", 
                    cancellable: true,
                    isArray: true,
                },
                "partialUpdate": {
                    url: urlBase + "/CmsCurrencyData/:id/",
                    method: "PATCH", 
                },
                "retrieve": {
                    url: urlBase + "/CmsCurrencyData/:id/",
                    method: "GET", 
                    cancellable: true,
                },
                "update": {
                    url: urlBase + "/CmsCurrencyData/:id/",
                    method: "PUT", 
                },
            });
            
            R['deleteById'] = R['destroy'];
            R['destroyById'] = R['destroy'];
            R['find'] = R['list'];
            R['findById'] = R['retrieve'];
            R['query'] = R['list'];
            R['updateAttributes'] = R['partialUpdate'];
            R.modelName = "CmsCurrencyData";
            return R;
        }
    ])
    
    .factory("CmsGdprRequest", [
        "CustomResource", "lbAuth", function(Resource, lbAuth) {
            var R;
            R = Resource(urlBase + "/CmsGdprRequest/:id/",
            { 
                id: '@id',
            },
            { 
                "anonimizeUserData": {
                    url: urlBase + "/CmsGdprRequest/:id/anonimize_user_data/",
                    method: "PUT", 
                },
                "collectUserData": {
                    url: urlBase + "/CmsGdprRequest/:id/collect_user_data/",
                    method: "PUT", 
                },
                "create": {
                    url: urlBase + "/CmsGdprRequest/",
                    method: "POST", 
                },
                "deleteUserData": {
                    url: urlBase + "/CmsGdprRequest/:id/delete_user_data/",
                    method: "PUT", 
                },
                "destroy": {
                    url: urlBase + "/CmsGdprRequest/:id/",
                    method: "DELETE", 
                },
                "findOne": {
                    url: urlBase + "/CmsGdprRequest/find_one/",
                    method: "GET", 
                    cancellable: true,
                },
                "list": {
                    url: urlBase + "/CmsGdprRequest/",
                    method: "GET", 
                    cancellable: true,
                    isArray: true,
                },
                "partialUpdate": {
                    url: urlBase + "/CmsGdprRequest/:id/",
                    method: "PATCH", 
                },
                "retrieve": {
                    url: urlBase + "/CmsGdprRequest/:id/",
                    method: "GET", 
                    cancellable: true,
                },
                "sendRequest": {
                    url: urlBase + "/CmsGdprRequest/:id/send_request/",
                    method: "PUT", 
                },
                "sendVerificationCode": {
                    url: urlBase + "/CmsGdprRequest/:id/send_verification_code/",
                    method: "PUT", 
                },
                "update": {
                    url: urlBase + "/CmsGdprRequest/:id/",
                    method: "PUT", 
                },
                "verifyCode": {
                    url: urlBase + "/CmsGdprRequest/:id/verify_code/",
                    method: "PUT", 
                },
            });
            
            R['deleteById'] = R['destroy'];
            R['destroyById'] = R['destroy'];
            R['find'] = R['list'];
            R['findById'] = R['retrieve'];
            R['query'] = R['list'];
            R['updateAttributes'] = R['partialUpdate'];
            R.modelName = "CmsGdprRequest";
            return R;
        }
    ])
    
    .factory("CmsKeyword", [
        "CustomResource", "lbAuth", function(Resource, lbAuth) {
            var R;
            R = Resource(urlBase + "/CmsKeyword/:id/",
            { 
                forModelField: '@forModelField',
                id: '@id',
            },
            { 
                "count": {
                    url: urlBase + "/CmsKeyword/count/",
                    method: "GET", 
                    cancellable: true,
                },
                "create": {
                    url: urlBase + "/CmsKeyword/",
                    method: "POST", 
                },
                "deleteFile": {
                    url: urlBase + "/CmsKeyword/:id/file/:forModelField/delete/",
                    method: "PATCH", 
                },
                "destroy": {
                    url: urlBase + "/CmsKeyword/:id/",
                    method: "DELETE", 
                },
                "findOne": {
                    url: urlBase + "/CmsKeyword/find_one/",
                    method: "GET", 
                    cancellable: true,
                },
                "list": {
                    url: urlBase + "/CmsKeyword/",
                    method: "GET", 
                    cancellable: true,
                    isArray: true,
                },
                "partialUpdate": {
                    url: urlBase + "/CmsKeyword/:id/",
                    method: "PATCH", 
                },
                "retrieve": {
                    url: urlBase + "/CmsKeyword/:id/",
                    method: "GET", 
                    cancellable: true,
                },
                "update": {
                    url: urlBase + "/CmsKeyword/:id/",
                    method: "PUT", 
                },
                "updateFile": {
                    url: urlBase + "/CmsKeyword/:id/file/:forModelField/update/",
                    method: "PUT", 
                },
                "uploadFile": {
                    url: urlBase + "/CmsKeyword/:id/file/:forModelField/upload/",
                    headers: {
                        'Content-Type': undefined
                    },
                    method: "POST", 
                },
            });
            
            R['deleteById'] = R['destroy'];
            R['destroyById'] = R['destroy'];
            R['find'] = R['list'];
            R['findById'] = R['retrieve'];
            R['query'] = R['list'];
            R['updateAttributes'] = R['partialUpdate'];
            R.modelName = "CmsKeyword";
            return R;
        }
    ])
    
    .factory("CmsMailNotification", [
        "CustomResource", "lbAuth", function(Resource, lbAuth) {
            var R;
            R = Resource(urlBase + "/CmsMailNotification/:id/",
            { 
                id: '@id',
                identifier: '@identifier',
            },
            { 
                "count": {
                    url: urlBase + "/CmsMailNotification/count/",
                    method: "GET", 
                    cancellable: true,
                },
                "create": {
                    url: urlBase + "/CmsMailNotification/",
                    method: "POST", 
                },
                "destroy": {
                    url: urlBase + "/CmsMailNotification/:id/",
                    method: "DELETE", 
                },
                "findOne": {
                    url: urlBase + "/CmsMailNotification/find_one/",
                    method: "GET", 
                    cancellable: true,
                },
                "getInitial": {
                    url: urlBase + "/CmsMailNotification/:id/get_initial/",
                    method: "GET", 
                    cancellable: true,
                },
                "list": {
                    url: urlBase + "/CmsMailNotification/",
                    method: "GET", 
                    cancellable: true,
                    isArray: true,
                },
                "partialUpdate": {
                    url: urlBase + "/CmsMailNotification/:id/",
                    method: "PATCH", 
                },
                "retrieve": {
                    url: urlBase + "/CmsMailNotification/:id/",
                    method: "GET", 
                    cancellable: true,
                },
                "send": {
                    url: urlBase + "/CmsMailNotification/send/:identifier/",
                    method: "PUT", 
                },
                "sendMultipart": {
                    headers: {
                        'Content-Type': undefined
                    },
                    url: urlBase + "/CmsMailNotification/send_multipart/:identifier/",
                    method: "PUT", 
                },
                "update": {
                    url: urlBase + "/CmsMailNotification/:id/",
                    method: "PUT", 
                },
                "validate": {
                    url: urlBase + "/CmsMailNotification/validate/",
                    method: "PUT", 
                },
            });
            
            R['deleteById'] = R['destroy'];
            R['destroyById'] = R['destroy'];
            R['find'] = R['list'];
            R['findById'] = R['retrieve'];
            R['query'] = R['list'];
            R['updateAttributes'] = R['partialUpdate'];
            R.modelName = "CmsMailNotification";
            return R;
        }
    ])
    
    .factory("CmsPdfData", [
        "CustomResource", "lbAuth", function(Resource, lbAuth) {
            var R;
            R = Resource(urlBase + "/CmsPdfData/:id/",
            { 
                forModelField: '@forModelField',
                id: '@id',
            },
            { 
                "count": {
                    url: urlBase + "/CmsPdfData/count/",
                    method: "GET", 
                    cancellable: true,
                },
                "create": {
                    url: urlBase + "/CmsPdfData/",
                    method: "POST", 
                },
                "deleteFilesInsideList": {
                    url: urlBase + "/CmsPdfData/:id/fileList/:forModelField/delete/",
                    method: "PATCH", 
                    isArray: true,
                },
                "destroy": {
                    url: urlBase + "/CmsPdfData/:id/",
                    method: "DELETE", 
                },
                "getInitial": {
                    url: urlBase + "/CmsPdfData/:id/get_initial/",
                    method: "GET", 
                    cancellable: true,
                },
                "list": {
                    url: urlBase + "/CmsPdfData/",
                    method: "GET", 
                    cancellable: true,
                    isArray: true,
                },
                "partialUpdate": {
                    url: urlBase + "/CmsPdfData/:id/",
                    method: "PATCH", 
                },
                "retrieve": {
                    url: urlBase + "/CmsPdfData/:id/",
                    method: "GET", 
                    cancellable: true,
                },
                "update": {
                    url: urlBase + "/CmsPdfData/:id/",
                    method: "PUT", 
                },
                "updateFilesInsideList": {
                    url: urlBase + "/CmsPdfData/:id/fileList/:forModelField/update/",
                    method: "PUT", 
                    isArray: true,
                },
                "uploadFileToList": {
                    url: urlBase + "/CmsPdfData/:id/fileList/:forModelField/upload/",
                    headers: {
                        'Content-Type': undefined
                    },
                    method: "POST", 
                    isArray: true,
                },
                "validate": {
                    url: urlBase + "/CmsPdfData/validate/",
                    method: "PUT", 
                },
            });
            
            R['deleteById'] = R['destroy'];
            R['destroyById'] = R['destroy'];
            R['find'] = R['list'];
            R['findById'] = R['retrieve'];
            R['query'] = R['list'];
            R['updateAttributes'] = R['partialUpdate'];
            R.modelName = "CmsPdfData";
            return R;
        }
    ])
    
    .factory("CmsPictureAlbum", [
        "CustomResource", "lbAuth", function(Resource, lbAuth) {
            var R;
            R = Resource(urlBase + "/CmsPictureAlbum/:id/",
            { 
                forModelField: '@forModelField',
                id: '@id',
            },
            { 
                "count": {
                    url: urlBase + "/CmsPictureAlbum/count/",
                    method: "GET", 
                    cancellable: true,
                },
                "create": {
                    url: urlBase + "/CmsPictureAlbum/",
                    method: "POST", 
                },
                "deleteFilesInsideList": {
                    url: urlBase + "/CmsPictureAlbum/:id/fileList/:forModelField/delete/",
                    method: "PATCH", 
                    isArray: true,
                },
                "destroy": {
                    url: urlBase + "/CmsPictureAlbum/:id/",
                    method: "DELETE", 
                },
                "findOne": {
                    url: urlBase + "/CmsPictureAlbum/find_one/",
                    method: "GET", 
                    cancellable: true,
                },
                "list": {
                    url: urlBase + "/CmsPictureAlbum/",
                    method: "GET", 
                    cancellable: true,
                    isArray: true,
                },
                "partialUpdate": {
                    url: urlBase + "/CmsPictureAlbum/:id/",
                    method: "PATCH", 
                },
                "retrieve": {
                    url: urlBase + "/CmsPictureAlbum/:id/",
                    method: "GET", 
                    cancellable: true,
                },
                "update": {
                    url: urlBase + "/CmsPictureAlbum/:id/",
                    method: "PUT", 
                },
                "updateFilesInsideList": {
                    url: urlBase + "/CmsPictureAlbum/:id/fileList/:forModelField/update/",
                    method: "PUT", 
                    isArray: true,
                },
                "uploadFileToList": {
                    url: urlBase + "/CmsPictureAlbum/:id/fileList/:forModelField/upload/",
                    headers: {
                        'Content-Type': undefined
                    },
                    method: "POST", 
                    isArray: true,
                },
            });
            
            R['deleteById'] = R['destroy'];
            R['destroyById'] = R['destroy'];
            R['find'] = R['list'];
            R['findById'] = R['retrieve'];
            R['query'] = R['list'];
            R['updateAttributes'] = R['partialUpdate'];
            R.modelName = "CmsPictureAlbum";
            return R;
        }
    ])
    
    .factory("CmsSettings", [
        "CustomResource", "lbAuth", function(Resource, lbAuth) {
            var R;
            R = Resource(urlBase + "/CmsSettings/:id/",
            { 
                destination: '@destination',
                forModelField: '@forModelField',
                id: '@id',
                provider: '@provider',
            },
            { 
                "checkDumpDbAndFilesIsPossible": {
                    url: urlBase + "/CmsSettings/check_dump_db_and_files_is_possible/",
                    method: "GET", 
                    cancellable: true,
                },
                "create": {
                    url: urlBase + "/CmsSettings/",
                    method: "POST", 
                },
                "deleteFile": {
                    url: urlBase + "/CmsSettings/:id/file/:forModelField/delete/",
                    method: "PATCH", 
                },
                "deleteFilesInsideList": {
                    url: urlBase + "/CmsSettings/:id/fileList/:forModelField/delete/",
                    method: "PATCH", 
                    isArray: true,
                },
                "destroy": {
                    url: urlBase + "/CmsSettings/:id/",
                    method: "DELETE", 
                },
                "dumpDbAndFiles": {
                    url: urlBase + "/CmsSettings/dump_db_and_files/",
                    method: "GET", 
                    cancellable: true,
                },
                "findOne": {
                    url: urlBase + "/CmsSettings/find_one/",
                    method: "GET", 
                    cancellable: true,
                },
                "list": {
                    url: urlBase + "/CmsSettings/",
                    method: "GET", 
                    cancellable: true,
                    isArray: true,
                },
                "parseFile": {
                    url: urlBase + "/CmsSettings/parse_file/",
                    headers: {
                        'Content-Type': undefined
                    },
                    method: "PUT", 
                },
                "partialUpdate": {
                    url: urlBase + "/CmsSettings/:id/",
                    method: "PATCH", 
                },
                "restoreDbAndFiles": {
                    url: urlBase + "/CmsSettings/restore_db_and_files/",
                    headers: {
                        'Content-Type': undefined
                    },
                    method: "PUT", 
                },
                "retrieve": {
                    url: urlBase + "/CmsSettings/:id/",
                    method: "GET", 
                    cancellable: true,
                },
                "sendTestNotify": {
                    url: urlBase + "/CmsSettings/send_test_notify/",
                    method: "PUT", 
                },
                "serverInfo": {
                    url: urlBase + "/CmsSettings/server_info/",
                    method: "GET", 
                    cancellable: true,
                },
                "translateText": {
                    url: urlBase + "/CmsSettings/translate_text/",
                    method: "GET", 
                    cancellable: true,
                },
                "update": {
                    url: urlBase + "/CmsSettings/:id/",
                    method: "PUT", 
                },
                "updateFile": {
                    url: urlBase + "/CmsSettings/:id/file/:forModelField/update/",
                    method: "PUT", 
                },
                "updateFilesInsideList": {
                    url: urlBase + "/CmsSettings/:id/fileList/:forModelField/update/",
                    method: "PUT", 
                    isArray: true,
                },
                "uploadFile": {
                    url: urlBase + "/CmsSettings/:id/file/:forModelField/upload/",
                    headers: {
                        'Content-Type': undefined
                    },
                    method: "POST", 
                },
                "uploadFileForBrand": {
                    url: urlBase + "/CmsSettings/:id/brand/:destination/upload/",
                    headers: {
                        'Content-Type': undefined
                    },
                    method: "POST", 
                },
                "uploadFileForHtmlEditors": {
                    url: urlBase + "/CmsSettings/file_for_html_editors/:provider/upload/",
                    method: "POST", 
                },
                "uploadFileToList": {
                    url: urlBase + "/CmsSettings/:id/fileList/:forModelField/upload/",
                    headers: {
                        'Content-Type': undefined
                    },
                    method: "POST", 
                    isArray: true,
                },
            });
            
            R['deleteById'] = R['destroy'];
            R['destroyById'] = R['destroy'];
            R['find'] = R['list'];
            R['findById'] = R['retrieve'];
            R['query'] = R['list'];
            R['updateAttributes'] = R['partialUpdate'];
            R.modelName = "CmsSettings";
            return R;
        }
    ])
    
    .factory("CmsSiteReview", [
        "CustomResource", "lbAuth", function(Resource, lbAuth) {
            var R;
            R = Resource(urlBase + "/CmsSiteReview/:id/",
            { 
                forModelField: '@forModelField',
                id: '@id',
            },
            { 
                "count": {
                    url: urlBase + "/CmsSiteReview/count/",
                    method: "GET", 
                    cancellable: true,
                },
                "create": {
                    url: urlBase + "/CmsSiteReview/",
                    method: "POST", 
                },
                "deleteFile": {
                    url: urlBase + "/CmsSiteReview/:id/file/:forModelField/delete/",
                    method: "PATCH", 
                },
                "destroy": {
                    url: urlBase + "/CmsSiteReview/:id/",
                    method: "DELETE", 
                },
                "findOne": {
                    url: urlBase + "/CmsSiteReview/find_one/",
                    method: "GET", 
                    cancellable: true,
                },
                "list": {
                    url: urlBase + "/CmsSiteReview/",
                    method: "GET", 
                    cancellable: true,
                    isArray: true,
                },
                "partialUpdate": {
                    url: urlBase + "/CmsSiteReview/:id/",
                    method: "PATCH", 
                },
                "retrieve": {
                    url: urlBase + "/CmsSiteReview/:id/",
                    method: "GET", 
                    cancellable: true,
                },
                "update": {
                    url: urlBase + "/CmsSiteReview/:id/",
                    method: "PUT", 
                },
                "updateFile": {
                    url: urlBase + "/CmsSiteReview/:id/file/:forModelField/update/",
                    method: "PUT", 
                },
                "uploadFile": {
                    url: urlBase + "/CmsSiteReview/:id/file/:forModelField/upload/",
                    headers: {
                        'Content-Type': undefined
                    },
                    method: "POST", 
                },
            });
            
            R['deleteById'] = R['destroy'];
            R['destroyById'] = R['destroy'];
            R['find'] = R['list'];
            R['findById'] = R['retrieve'];
            R['query'] = R['list'];
            R['updateAttributes'] = R['partialUpdate'];
            R.modelName = "CmsSiteReview";
            return R;
        }
    ])
    
    .factory("CmsSmsNotification", [
        "CustomResource", "lbAuth", function(Resource, lbAuth) {
            var R;
            R = Resource(urlBase + "/CmsSmsNotification/:id/",
            { 
                id: '@id',
            },
            { 
                "count": {
                    url: urlBase + "/CmsSmsNotification/count/",
                    method: "GET", 
                    cancellable: true,
                },
                "create": {
                    url: urlBase + "/CmsSmsNotification/",
                    method: "POST", 
                },
                "destroy": {
                    url: urlBase + "/CmsSmsNotification/:id/",
                    method: "DELETE", 
                },
                "findOne": {
                    url: urlBase + "/CmsSmsNotification/find_one/",
                    method: "GET", 
                    cancellable: true,
                },
                "getInitial": {
                    url: urlBase + "/CmsSmsNotification/:id/get_initial/",
                    method: "GET", 
                    cancellable: true,
                },
                "list": {
                    url: urlBase + "/CmsSmsNotification/",
                    method: "GET", 
                    cancellable: true,
                    isArray: true,
                },
                "partialUpdate": {
                    url: urlBase + "/CmsSmsNotification/:id/",
                    method: "PATCH", 
                },
                "retrieve": {
                    url: urlBase + "/CmsSmsNotification/:id/",
                    method: "GET", 
                    cancellable: true,
                },
                "update": {
                    url: urlBase + "/CmsSmsNotification/:id/",
                    method: "PUT", 
                },
                "validate": {
                    url: urlBase + "/CmsSmsNotification/validate/",
                    method: "PUT", 
                },
            });
            
            R['deleteById'] = R['destroy'];
            R['destroyById'] = R['destroy'];
            R['find'] = R['list'];
            R['findById'] = R['retrieve'];
            R['query'] = R['list'];
            R['updateAttributes'] = R['partialUpdate'];
            R.modelName = "CmsSmsNotification";
            return R;
        }
    ])
    
    .factory("CmsTranslations", [
        "CustomResource", "lbAuth", function(Resource, lbAuth) {
            var R;
            R = Resource(urlBase + "/CmsTranslations/:id/",
            { 
                id: '@id',
            },
            { 
                "create": {
                    url: urlBase + "/CmsTranslations/",
                    method: "POST", 
                },
                "destroy": {
                    url: urlBase + "/CmsTranslations/:id/",
                    method: "DELETE", 
                },
                "getByLangCodeForGettext": {
                    url: urlBase + "/CmsTranslations/get_by_lang_code_for_gettext/",
                    method: "GET", 
                    cancellable: true,
                },
                "list": {
                    url: urlBase + "/CmsTranslations/",
                    method: "GET", 
                    cancellable: true,
                    isArray: true,
                },
                "partialUpdate": {
                    url: urlBase + "/CmsTranslations/:id/",
                    method: "PATCH", 
                },
                "retrieve": {
                    url: urlBase + "/CmsTranslations/:id/",
                    method: "GET", 
                    cancellable: true,
                },
                "update": {
                    url: urlBase + "/CmsTranslations/:id/",
                    method: "PUT", 
                },
            });
            
            R['deleteById'] = R['destroy'];
            R['destroyById'] = R['destroy'];
            R['find'] = R['list'];
            R['findById'] = R['retrieve'];
            R['query'] = R['list'];
            R['updateAttributes'] = R['partialUpdate'];
            R.modelName = "CmsTranslations";
            return R;
        }
    ])
    
    .factory("CmsUser", [
        "CustomResource", "lbAuth", function(Resource, lbAuth) {
            var R;
            R = Resource(urlBase + "/CmsUser/:id/",
            { 
                forModelField: '@forModelField',
                id: '@id',
            },
            { 
                "count": {
                    url: urlBase + "/CmsUser/count/",
                    method: "GET", 
                    cancellable: true,
                },
                "create": {
                    url: urlBase + "/CmsUser/",
                    method: "POST", 
                },
                "deleteFile": {
                    url: urlBase + "/CmsUser/:id/file/:forModelField/delete/",
                    method: "PATCH", 
                },
                "deleteFilesInsideList": {
                    url: urlBase + "/CmsUser/:id/fileList/:forModelField/delete/",
                    method: "PATCH", 
                    isArray: true,
                },
                "destroy": {
                    url: urlBase + "/CmsUser/:id/",
                    method: "DELETE", 
                },
                "forceSetPassword": {
                    url: urlBase + "/CmsUser/:id/force_set_password/",
                    method: "PUT", 
                },
                "list": {
                    url: urlBase + "/CmsUser/",
                    method: "GET", 
                    cancellable: true,
                    isArray: true,
                },
                "login": {
                    url: urlBase + "/CmsUser/login/",
                    method: "POST", 
                    interceptor: {
                        response: function(response) {
                            var data, params;
                            data = response.data;
                            lbAuth.setUser(data.token, data.userId, data.user);
                            params = response.config.params || {};
                            lbAuth.rememberMe = !!params.rememberMe;
                            lbAuth.save();
                            return response.resource;
                        }
                    },
                },
                "logout": {
                    url: urlBase + "/CmsUser/logout/",
                    method: "DELETE", 
                    interceptor: {
                        response: function(response) {
                            lbAuth.clearUser();
                            lbAuth.clearStorage();
                            return response.resource;
                        }
                    }
                },
                "me": {
                    url: urlBase + "/CmsUser/me/",
                    method: "GET", 
                    cancellable: true,
                },
                "partialUpdate": {
                    url: urlBase + "/CmsUser/:id/",
                    method: "PATCH", 
                },
                "register": {
                    url: urlBase + "/CmsUser/register/",
                    method: "POST", 
                    interceptor: {
                        response: function(response) {
                            var data, params;
                            data = response.data;
                            lbAuth.setUser(data.token, data.userId, data.user);
                            params = response.config.params || {};
                            lbAuth.rememberMe = !!params.rememberMe;
                            lbAuth.save();
                            return response.resource;
                        }
                    },
                },
                "reset": {
                    url: urlBase + "/CmsUser/reset/",
                    method: "POST", 
                },
                "retrieve": {
                    url: urlBase + "/CmsUser/:id/",
                    method: "GET", 
                    cancellable: true,
                },
                "setPassword": {
                    url: urlBase + "/CmsUser/set_password/",
                    method: "POST", 
                },
                "update": {
                    url: urlBase + "/CmsUser/:id/",
                    method: "PUT", 
                },
                "updateFile": {
                    url: urlBase + "/CmsUser/:id/file/:forModelField/update/",
                    method: "PUT", 
                },
                "updateFilesInsideList": {
                    url: urlBase + "/CmsUser/:id/fileList/:forModelField/update/",
                    method: "PUT", 
                    isArray: true,
                },
                "updateMe": {
                    url: urlBase + "/CmsUser/update_me/",
                    method: "PUT", 
                },
                "uploadFile": {
                    url: urlBase + "/CmsUser/:id/file/:forModelField/upload/",
                    headers: {
                        'Content-Type': undefined
                    },
                    method: "POST", 
                },
                "uploadFileToList": {
                    url: urlBase + "/CmsUser/:id/fileList/:forModelField/upload/",
                    headers: {
                        'Content-Type': undefined
                    },
                    method: "POST", 
                    isArray: true,
                },
            });
            
            R['deleteById'] = R['destroy'];
            R['destroyById'] = R['destroy'];
            R['find'] = R['list'];
            R['findById'] = R['retrieve'];
            R['query'] = R['list'];
            R['updateAttributes'] = R['partialUpdate'];
            R.modelName = "CmsUser";
            return R;
        }
    ])
    
    .factory("GroupPermissions", [
        "CustomResource", "lbAuth", function(Resource, lbAuth) {
            var R;
            R = Resource(urlBase + "/GroupPermissions/:id/",
            { 
                id: '@id',
            },
            { 
                "create": {
                    url: urlBase + "/GroupPermissions/",
                    method: "POST", 
                },
                "destroy": {
                    url: urlBase + "/GroupPermissions/:id/",
                    method: "DELETE", 
                },
                "list": {
                    url: urlBase + "/GroupPermissions/",
                    method: "GET", 
                    cancellable: true,
                    isArray: true,
                },
                "listAllPermissions": {
                    url: urlBase + "/GroupPermissions/list_all_permissions/",
                    method: "GET", 
                    cancellable: true,
                    isArray: true,
                },
                "partialUpdate": {
                    url: urlBase + "/GroupPermissions/:id/",
                    method: "PATCH", 
                },
                "retrieve": {
                    url: urlBase + "/GroupPermissions/:id/",
                    method: "GET", 
                    cancellable: true,
                },
                "update": {
                    url: urlBase + "/GroupPermissions/:id/",
                    method: "PUT", 
                },
            });
            
            R['deleteById'] = R['destroy'];
            R['destroyById'] = R['destroy'];
            R['find'] = R['list'];
            R['findById'] = R['retrieve'];
            R['query'] = R['list'];
            R['updateAttributes'] = R['partialUpdate'];
            R.modelName = "GroupPermissions";
            return R;
        }
    ])
    
    .factory("Helpers", [
        "CustomResource", "lbAuth", function(Resource, lbAuth) {
            var R;
            R = Resource(urlBase + "/Helpers/",
            { 
            },
            { 
                "bulk": {
                    url: urlBase + "/Helpers/bulk/",
                    method: "GET", 
                    cancellable: true,
                },
                "postBulk": {
                    url: urlBase + "/Helpers/post_bulk/",
                    method: "POST", 
                },
            });
            
            R.modelName = "Helpers";
            return R;
        }
    ])
    

    .factory('lbAuth', ["$injector", function($injector) {
        var lbAuth, load, props, propsPrefix, save, storage, useGdprStorage = false;
        props = ['accessTokenId', 'currentUserId', 'rememberMe'];
        propsPrefix = '$lb$';
        if ($injector.has('$gdprStorage')){
            storage = $injector.get('$gdprStorage');
            useGdprStorage = true;
        } else {
            storage = localStorage;
        }
        lbAuth = function() {
            var self;
            self = this;
            props.forEach(function(name) {
                self[name] = load(name);
            });
            this.currentUserData = null;
        };
        save = function(name, value) {
            if (useGdprStorage){
                storage[name] = value;
            } else {
                var key;
                key = propsPrefix + name;
                if (value === null) {
                    value = '';
                }
                storage[key] = value;
            }
        };
        load = function(name) {
            var key;
            if (useGdprStorage){
                return storage[name] || null;
            } else {
                key = propsPrefix + name;
                return storage[key] || null;
            }
        };
        lbAuth.prototype.save = function() {
            var self;
            self = this;
            props.forEach(function(name) {
                save(name, self[name]);
            });
            if (useGdprStorage){
                storage.$apply(true);
            }
        };
        lbAuth.prototype.setUser = function(accessTokenId, userId, userData) {
            this.accessTokenId = accessTokenId;
            this.currentUserId = userId;
            this.currentUserData = userData;
        };
        lbAuth.prototype.clearUser = function() {
            this.accessTokenId = null;
            this.currentUserId = null;
            this.currentUserData = null;
        };
        lbAuth.prototype.clearStorage = function() {
            props.forEach(function(name) {
                save(name, null);
            });
        };
        return new lbAuth;
    }])
    .config([
        '$httpProvider', function($httpProvider) {
            $httpProvider.interceptors.push('lbAuthRequestInterceptor');
        }
    ])
    .factory('lbAuthRequestInterceptor', [
        '$q', 'lbAuth', function($q, lbAuth) {
            return {
                request: function(config) {
                    var host, res;
                    host = getHost(config.url);
                    if (host && host !== urlBaseHost) {
                        return config;
                    }
                    if (lbAuth.accessTokenId) {
                        config.headers[authHeader] = "Token " + lbAuth.accessTokenId;
                    } else if (config.__isGetCurrentUser__) {
                        res = {
                            body: {
                                error: {
                                    status: 401
                                }
                            },
                            status: 401,
                            config: config,
                            headers: function() {
                                return void 0;
                            }
                        };
                        return $q.reject(res);
                    }
                    return config || $q.when(config);
                }
            };
        }
    ])
    .provider('CustomResource', function() {

        /**
         * @ngdoc method
         * @name lbServices.CustomResourceProvider#setAuthHeader
         * @methodOf lbServices.CustomResourceProvider
         * @param {string} header The header name to use, e.g. `X-Access-Token`
         * @description
         * Configure the REST transport to use a different header for sending
         * the authentication token. It is sent in the `Authorization` header
         * by default.
         */
        this.setAuthHeader = function(header) {
            authHeader = header;
        };

        /**
         * @ngdoc method
         * @name lbServices.CustomResourceProvider#setUrlBase
         * @methodOf lbServices.CustomResourceProvider
         * @param {string} url The URL to use, e.g. `/api` or `//example.com/api`.
         * @description
         * Change the URL of the REST API server. By default, the URL provided
         * to the code generator (`lb-ng` or `grunt-loopback-sdk-angular`) is used.
         */
        this.setUrlBase = function(url) {
            urlBase = url;
            urlBaseHost = getHost(urlBase) || location.host;
        };

        /**
         * @ngdoc method
         * @name lbServices.CustomResourceProvider#getUrlBase
         * @methodOf lbServices.CustomResourceProvider
         * @description
         * Get the URL of the REST API server. The URL provided
         * to the code generator (`lb-ng` or `grunt-loopback-sdk-angular`) is used.
         */
        this.getUrlBase = function() {
            return urlBase;
        };
        this.$get = [
            '$resource', function($resource) {
                return function(url, params, actions) {
                    var resource;
                    resource = $resource(url, params, actions);
                    // Angular always calls POST on $save()
                    // This hack is based on
                    // http://kirkbushell.me/angular-js-using-ng-resource-in-a-more-restful-manner/

                    resource.prototype.$origSave = resource.prototype.$save;

                    resource.prototype.$save = function(success, error) {
                        var result;
                        // create new object
                        if (!this.hasOwnProperty('id') && !this.hasOwnProperty('pk')) {
                            if (resource.create) {
                                result = resource.create.call(this, {}, this, success, error);
                                return result.$promise || result;
                            }
                            return resource.$origSave(success, error);
                        }
                        //  update old object
                        result = resource.update.call(this, {}, this, success, error);
                        return result.$promise || result;
                    };
                    return resource;
                };
            }
        ];
    });

}).call(this);


angular.module('lbServices').factory('LoopBackAuth', [
    'lbAuth',
    function(lbAuth){
        return lbAuth;
    }
]);

angular.module('facebook', []).provider('Facebook', function(){
    this.init = angular.noop;
    this.$get = function(){
        return {
            init: angular.noop
        }
    }
});
