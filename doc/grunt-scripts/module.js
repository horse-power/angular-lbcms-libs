angular.module('DocExampleApp', [
    'gettext',
    'ui.bootstrap',
    'ui.gettext.langPicker',
    'ui.router',
    'ngAnimate',
    'lbServices',
    'ngResource',
    'ui.cms',
    'ui.cms.editable',
    'cms.models',
    'ui.landingWidgets',
    'AdminApp',
    //'ui.landingWidgets.editable'
])

.controller('defaultCtrl', function(){})

.config(["$constantsProvider", "$modelsProvider", "$uicIntegrationsProvider", "$injector", function($constantsProvider, $modelsProvider, $uicIntegrationsProvider, $injector){
    $modelsProvider.defaultModels = {};
    $constantsProvider.set('TEST_MODEL', {
        myProperty:[
            {
                value: 'hello',
                description: 'Hello!'
            },{
                value: 11,
                description: 'Integer: 11'
            },{
                value: false,
                description: 'My Boolean: false'
            },{
                value: 'world!!',
                description: 'world'
            }
        ]
    });
    $constantsProvider.set('$REPRESENTATION', {
        CmsArticle: {
            html: '{{$item.title | l10n}}',
            placeholder: 'Выберите статью'
        }
    });
    // переопределяем внешние возможные зависимости, чтоб можно было их отображать на reference-сервере
    $uicIntegrationsProvider.registerThirdPartyLibrary('googlemaps-infobox', [
        "./external/infobox-lib.js"
    ])
    $uicIntegrationsProvider.registerThirdPartyLibrary('aggrid', [
        "./external/aggrid/15/ag-grid.min.js"
    ])
    $uicIntegrationsProvider.registerThirdPartyLibrary('fontawesome4-json', [
        "./external/FontAwesomeIcons4.json"
    ])
    $uicIntegrationsProvider.registerThirdPartyLibrary('fontawesome5-json', [
        "./external/FontAwesomeIcons5.json"
    ])
    $uicIntegrationsProvider.registerThirdPartyLibrary('ckeditor4', [
        "./external/ckeditor/4/ckeditor.js"
    ]);
}])
.run(["$langPicker", "$cms", function($langPicker, $cms){
    $langPicker.setLanguageList({
        ru: 'Русский',
        en: 'English'
    });
    $langPicker.detectLanguage();
    $cms.settings = {
        cmsLangCodesJson: {
            'Русский': 'ru',
            'English': 'en'
        }
    };
}]);


angular.module('ui.cms.editable').config(["$injector", "$provide", function($injector, $provide){
    var $stateProvider = $injector.get('$stateProvider');
    // отключаем возможность либам регистрировать новые view и переходить в них
    $stateProvider.state = function(){
        return $stateProvider;
    }
    $provide.decorator('$state', ["$delegate", function($delegate) {
        $delegate.go = angular.noop;
        return $delegate;
    }]);

}]);


mkDocCtrl = function(ctrlName, scope, func) {
    // создает контроллер для документации прямо в рантайме браузера,
    // так можно определять переменные прямо в док-строках, а не создавать отдельные файлы
    angular.module('DocExampleApp').controller(ctrlName, function($scope, $cms, $injector) {
        $scope.$cms = $cms;
        angular.extend($scope, scope);
        if (func) {
            func($scope, $injector);
        }
    });
};
