NG_DOCS={
  "sections": {
    "api": "Main API Reference"
  },
  "pages": [
    {
      "section": "api",
      "id": "AdminApp.AdminAppConfigProvider",
      "shortName": "AdminApp.AdminAppConfigProvider",
      "type": "object",
      "moduleName": "AdminApp",
      "shortDescription": "провайдер для настройки приложения admin-app (админка цмс-ки)",
      "keywords": "$representation $state a-dashboard-admin a-sidebar-admin add addhref admin admin-app adminapp adminappconfigprovider angular api app bottom change_cmssettings class cms cmsarticle cmscategoryforarticle cmscategoryforpicturealbum cmsgdprrequest cmskeyword cmsmailnotification cmsmymodel cmspdfdata cmspicturealbum cmssitereview cmssmsnotification cmstranslations cmsuser conf config content copyright copyrightnotice creator creatorurl creditline css- cssclass danger dashboard data destination directive display en english fa-github fa-youtube fab fields font-awesome heading hello html- iconclass inline-block iso- l10nfile label langs list listhref mainpage menu method middle modeleditors modelname models module object overridemyapp primary redirections registercustomsettingstab registercustomsidebartemplate registerdashboardcard registerglossarycard rest- setallowedlanguages setcopyrightmetadataeditorformodels setcopyrightmetadatafields setloginpagetitle setnavbarconfig setsettingstabsconfig setsidebartitle settings setvisiblemodels setvisiblepages sidebar site site_settings sitemenu style template templateurl test-directive title top true type-hint type-hint-string url view_glossary world"
    },
    {
      "section": "api",
      "id": "AdminApp.directive:aCardsEditor",
      "shortName": "aCardsEditor",
      "type": "directive",
      "moduleName": "AdminApp",
      "shortDescription": "редактор карточек, которые можно отображать через ui.landingWidgets.directive:lwCards",
      "keywords": "$scope a-cards-editor acardseditorctrl adminapp api article bg-primary bg-warning cardsblock class cms-model-item cmsarticle cmsmodelitem data directive docexampleapp en files findone h3 html html- icon icon- image js json l10n landingwidgets lang link lw-cards mkdocctrl module ng-controller ng-model ngdisabled ngmodel padding price px script small style subtitle test text text-center title type ui"
    },
    {
      "section": "api",
      "id": "AdminApp.directive:aCarouselEditor",
      "shortName": "aCarouselEditor",
      "type": "directive",
      "moduleName": "AdminApp",
      "shortDescription": "редактор слайдера (отображать можно с помощью ui.landingWidgets.directive:lwCarousel)",
      "keywords": "$scope a-carousel-editor acarouseleditorctrl acorouseleditor adminapp api article bg-primary bg-warning class cms-model-item cmsarticle cmsmodelitem data directive docexampleapp en files findone h3 html image js json landingwidgets lang lw-carousel md mkdocctrl module ng-controller ng-model ngdisabled ngmodel options px script slider test type ui xs"
    },
    {
      "section": "api",
      "id": "AdminApp.directive:aCmsTranslationsEditor",
      "shortName": "aCmsTranslationsEditor",
      "type": "directive",
      "moduleName": "AdminApp",
      "shortDescription": "редактор переводов для CmsTranslations",
      "keywords": "$scope a-cms-translations-editor acmstranslationseditorctrl adminapp api cmstranslations context directive disable-auto-scroll docexampleapp greeting hello html js lang mkdocctrl module msgcomm msgctxt msgid msgidplural msgs msgstr ng-controller ng-model ngmodel pug ru script textarea translation translations true user world"
    },
    {
      "section": "api",
      "id": "AdminApp.directive:aDashboardAdmin",
      "shortName": "aDashboardAdmin",
      "type": "directive",
      "moduleName": "AdminApp",
      "shortDescription": "",
      "keywords": "$scope adashboardadminctrl adminapp api directive docexampleapp html js mkdocctrl module ng-controller ngdisabled ngmodel pug script"
    },
    {
      "section": "api",
      "id": "AdminApp.directive:aDbRelationEditor",
      "shortName": "aDbRelationEditor",
      "type": "directive",
      "moduleName": "AdminApp",
      "shortDescription": "директива для редактирования ссылок на другие объекты в бд. Поддерживаются типы отношений hasOne, hasMany, belongsTo. Подробнее см примеры использования.",
      "keywords": "$representation $scope a-db-relation-editor adbrelationeditorctrl add_cmsarticle adminapp api article articleid articleids articles belongsto belongsto- change_cmsarticle class cms cmsarticle current-user-permission-create current-user-permission-edit directive docexampleapp edit fake fakearticleids false filterfields filterwhere form-group getnewitem hasmany hasmany- hasone haspermission html id__gte items js json main_app mkdocctrl model-name modelname module myfakearticles ng-controller ng-model ngdisabled ngmodel ngrequired script service sorted text-danger three title true type ui uiceitemeditor"
    },
    {
      "section": "api",
      "id": "AdminApp.directive:adminApp",
      "shortName": "adminApp",
      "type": "directive",
      "moduleName": "AdminApp",
      "shortDescription": "",
      "keywords": "$scope adminapp api directive ngdisabled ngmodel pug"
    },
    {
      "section": "api",
      "id": "AdminApp.directive:aFooterToolbar",
      "shortName": "aFooterToolbar",
      "type": "directive",
      "moduleName": "AdminApp",
      "shortDescription": "",
      "keywords": "$scope adminapp afootertoolbarctrl api directive docexampleapp html js mkdocctrl module ng-controller ngdisabled ngmodel script"
    },
    {
      "section": "api",
      "id": "AdminApp.directive:aKeywordsIdListEditor",
      "shortName": "aKeywordsIdListEditor",
      "type": "directive",
      "moduleName": "AdminApp",
      "shortDescription": "",
      "keywords": "$scope adminapp akeywordsidlisteditorctrl api directive docexampleapp html js mkdocctrl module ng-controller ngdisabled ngmodel script"
    },
    {
      "section": "api",
      "id": "AdminApp.directive:aObjectEditSidebar",
      "shortName": "aObjectEditSidebar",
      "type": "directive",
      "moduleName": "AdminApp",
      "shortDescription": "директива с правым сайдбаром редактора свойств объекта типа главной картинки, идентификатора,",
      "keywords": "$scope adminapp aobjecteditsidebarctrl api directive docexampleapp html js mkdocctrl module ng-controller ngdisabled ngmodel pug script"
    },
    {
      "section": "api",
      "id": "AdminApp.directive:aValidateNotificationTemplate",
      "shortName": "aValidateNotificationTemplate",
      "type": "directive",
      "moduleName": "AdminApp",
      "shortDescription": "валидация текстовых шаблонов экземпляров CmsSmsNotification, CmsMailNotification",
      "keywords": "$scope adminapp api avalidatenotificationtemplatectrl body cmsmailnotification cmsmodelname cmssmsnotification directive docexampleapp echosubject fieldname html js mkdocctrl module ng-controller ngmodel pug script"
    },
    {
      "section": "api",
      "id": "AdminApp.directive:aVariablesDescriptionEditor",
      "shortName": "aVariablesDescriptionEditor",
      "type": "directive",
      "moduleName": "AdminApp",
      "shortDescription": "директива, которая предназначена для редактирования описаний переменных на основе VariableDescription модели",
      "keywords": "$scope adminapp api avariablesdescriptioneditorctrl directive docexampleapp html js mkdocctrl module ng-controller ngdisabled ngmodel pug script variabledescription"
    },
    {
      "section": "api",
      "id": "angular.element.prototype.contains",
      "shortName": "angular.element.prototype.contains",
      "type": "function",
      "moduleName": "ng",
      "shortDescription": "добавляет к jqLite метод определения, принадлежит ли HTML элемент родителю .usage, h2#usage&#123;display: none;&#125;",
      "keywords": "angular api el element function getelementbyid h2 hello html jqlite prototype true usage"
    },
    {
      "section": "api",
      "id": "angular.getValue",
      "shortName": "angular.getValue",
      "type": "function",
      "moduleName": "ng",
      "shortDescription": "возвращает значение пути к полю объекта",
      "keywords": "angular api array custom function getvalue hello myobject obj object path prop1 prop2 prop3 text world"
    },
    {
      "section": "api",
      "id": "angular.getValueOr",
      "shortName": "angular.getValueOr",
      "type": "function",
      "moduleName": "ng",
      "shortDescription": "похожая на angular.getValue функция, только 1 обязательный аргумент - объект с которого считываются свойства,",
      "keywords": "angular api array custom function getvalue getvalueor hello myobject object pro1p1 prop1 prop2 prop3 text world"
    },
    {
      "section": "api",
      "id": "angular.isEmpty",
      "shortName": "angular.isEmpty",
      "type": "function",
      "moduleName": "ng",
      "shortDescription": "проверяет является ли переданный объект пустым.",
      "keywords": "angular api arr array false function hello isempty length obj object test true undefined-"
    },
    {
      "section": "api",
      "id": "angular.setValue",
      "shortName": "angular.setValue",
      "type": "function",
      "moduleName": "ng",
      "shortDescription": "добавляет значение свойства в объект согласно пути к свойству",
      "keywords": "angular api array console custom function hello log myobject object prop1 setvalue text world"
    },
    {
      "section": "api",
      "id": "angular.tr",
      "shortName": "angular.tr",
      "type": "function",
      "moduleName": "ng",
      "shortDescription": "функция, которая предназначена для маркирования строковых литералов",
      "keywords": "angular api function gettext h2 text tr usage"
    },
    {
      "section": "api",
      "id": "GLOBAL",
      "shortName": "GLOBAL",
      "type": "overview",
      "moduleName": "GLOBAL",
      "shortDescription": "в этом разделе описаны функции, которые добавлены в базовые js объекты.",
      "keywords": "api global js overview"
    },
    {
      "section": "api",
      "id": "LESS",
      "shortName": "LESS",
      "type": "overview",
      "moduleName": "LESS",
      "shortDescription": "Классы css и хаки, которые идут в поставке с цмс-ными библиотеками. Пакет CmsLibs.",
      "keywords": "api blank cmslibs css href http js org overview target"
    },
    {
      "section": "api",
      "id": "LESS.Hacks",
      "shortName": "LESS.Hacks",
      "type": "service",
      "moduleName": "LESS",
      "shortDescription": "хаки используемые библиотекой.",
      "keywords": "api bootstrap class datepicker-a firefox firefox- font-size fontawesome form-control glyphicon-chevron-left glyphicon-chevron-right glyphicon-menu-left glyphicon-menu-right glyphicon-menu-up hacks https jpg net rgb select selet- service src style text-danger ui"
    },
    {
      "section": "api",
      "id": "ui.cms",
      "shortName": "ui.cms",
      "type": "overview",
      "moduleName": "ui.cms",
      "shortDescription": "Основной модуль cms. Дает обертки над функциями ангулара,",
      "keywords": "amazon api bucket cms cms- cms_allowed_langs cms_db_bucket cms_default_lang cms_storage digitaloceanspaces en google html http local localhost my_super_bucket overview ru server type ui var"
    },
    {
      "section": "api",
      "id": "ui.cms.$cmsProvider",
      "shortName": "ui.cms.$cmsProvider",
      "type": "object",
      "moduleName": "ui.cms",
      "shortDescription": "провайдер для настроек $cms",
      "keywords": "$cms $cmsprovider $scope action ae anchor api app btn btn-default button cms config console controller description directive fancy hello helpers indexapp log method my-directive myaction mydirective myservice navbar navbar- object override property pug registercustomsiteaction registercustomsiteanchor registernavbarcustomdirective replace restrict runsiteaction service state-a template test true ui uisref view world"
    },
    {
      "section": "api",
      "id": "ui.cms.$constantsProvider",
      "shortName": "ui.cms.$constantsProvider",
      "type": "object",
      "moduleName": "ui.cms",
      "shortDescription": "провайдер для определения констант. В основном используется django-командами для генерации разных схем и экспорта choices из полей моделей в бд.",
      "keywords": "$constantsprovider admin angular api choices cms cms_user config default description django- en method module my_value myapp object ru set ui upper-case user userrole"
    },
    {
      "section": "api",
      "id": "ui.cms.$directiveOverrideProvider",
      "shortName": "ui.cms.$directiveOverrideProvider",
      "type": "object",
      "moduleName": "ui.cms",
      "shortDescription": "провайдер для переопределения объявления директив.",
      "keywords": "$directiveoverrideprovider $scope angular angular_inline_template angularjs api body cms coffee config console controller directivename directivenewname directiveobject directiveoriginalname duplicate grunt hello html https link log method module mynew myspecialsmth myspecialsmth2 ngannotate object org override overrideaftermyapp someapp template templateurl ui uiceloginform uiceloginformcustom"
    },
    {
      "section": "api",
      "id": "ui.cms.directive:uicAlert",
      "shortName": "uicAlert",
      "type": "directive",
      "moduleName": "ui.cms",
      "shortDescription": "плашка-уведомление",
      "keywords": "alert api cms css- danger directive docexampleapp html info js mkdocctrl module ng-controller pug script success test type ui uic-alert uicalertctrl warning"
    },
    {
      "section": "api",
      "id": "ui.cms.directive:uicAutoHeight",
      "shortName": "uicAutoHeight",
      "type": "directive",
      "moduleName": "ui.cms",
      "shortDescription": "директива для подстраивания высоты виджета под контент.",
      "keywords": "api class cms css data directive docexampleapp enter form-control html js mkdocctrl module ng-controller ng-model ngmodel script textarea textarea- ui uic-auto-height uicautoheightctrl"
    },
    {
      "section": "api",
      "id": "ui.cms.directive:uicBindHtml",
      "shortName": "uicBindHtml",
      "type": "directive",
      "moduleName": "ui.cms",
      "shortDescription": "директива подобная ng-bind-html,",
      "keywords": "$scope angularjs api bind- bind-item class cms directive div docexampleapp form-control html https js mkdocctrl module myangular2template myangulartemplate ng-bind-html ng-controller ng-hide ng-model oncompiledone org pug script somefunc text ui uic-bind-html uicbindhtml uicbindhtmlctrl variable variablenameinscope"
    },
    {
      "section": "api",
      "id": "ui.cms.directive:uicCaret",
      "shortName": "uicCaret",
      "type": "directive",
      "moduleName": "ui.cms",
      "shortDescription": "директива-обертка для легкого отображения кареток",
      "keywords": "$scope api caret class cms direction directive dropup false margin-top mydirection pug style true ui uic-caret"
    },
    {
      "section": "api",
      "id": "ui.cms.directive:uicCenterBlock",
      "shortName": "uicCenterBlock",
      "type": "directive",
      "moduleName": "ui.cms",
      "shortDescription": "директива для центрирования по вертикали и горизонтали контента внутри блока,",
      "keywords": "api bg-danger bg-primary bg-success black class cms color directive docexampleapp ea height html module padding pug span style ui uic-center-block"
    },
    {
      "section": "api",
      "id": "ui.cms.directive:uicCheckbox",
      "shortName": "uicCheckbox",
      "type": "directive",
      "moduleName": "ui.cms",
      "shortDescription": "директива-обертка для легкого отображения input(type=&quot;checkbox&quot;)",
      "keywords": "$scope api bootstrap-e button checkbox cms directive div input label mycheckboxval ngchange ngdisabled ngmodel pug span super switch true ui uic-checkbox"
    },
    {
      "section": "api",
      "id": "ui.cms.directive:uicColsGrid",
      "shortName": "uicColsGrid",
      "type": "directive",
      "moduleName": "ui.cms",
      "shortDescription": "директива, позволяющая генерировать сетку адаптивной верстки, на основе размеров колонок для разных размеров экранов.",
      "keywords": "$item $origscope $scope api bg-primary class cms css directive docexampleapp flexbox function hello hr html item items js json lg margin-bottom md mkdocctrl module myarray ng-controller padding pug return row sayhi scope script sm small style text ui uic-cols-grid uiccolsgridctrl value2 world xs"
    },
    {
      "section": "api",
      "id": "ui.cms.directive:uicConstant",
      "shortName": "uicConstant",
      "type": "directive",
      "moduleName": "ui.cms",
      "shortDescription": "директива, которая работает в паре с $constantsProvider. Объявите в блоке config с помощью $constantsProvider",
      "keywords": "$constants $constantsprovider $scope angular api array boolean checkbox checkbox-inline class cms col-xs-100 config controller css- description directive docexampleapp false hello html inline input input-class input-sm inputclass integer js mkdocctrl module mycomplexarrayvalue mycomplexvalue myctrl mymodule myproperty mysimplevalue ng-controller ng-model ng-model-type ngdisabled ngmodel object placeholder pug radio radio-inline row script select set skipvalues test_model text-success true ui uic-constant uicconstantctrl well-sm world"
    },
    {
      "section": "api",
      "id": "ui.cms.directive:uicContact",
      "shortName": "uicContact",
      "type": "directive",
      "moduleName": "ui.cms",
      "shortDescription": "директива для отображения L10nContact объектов(рендерит ссылки для почты, возможности позвонить по номеру). Обязательны поля value и type у ngModel",
      "keywords": "$scope api cms contact description directive docexampleapp ea email fax html js l10ncontact mkdocctrl module ng-controller ng-model ngmodel pug script skype tel type ui uic-contact uiccontactctrl"
    },
    {
      "section": "api",
      "id": "ui.cms.directive:uicCountryListPicker",
      "shortName": "uicCountryListPicker",
      "type": "directive",
      "moduleName": "ui.cms",
      "shortDescription": "пикер массива стран (сохраняются двухзначные iso-коды)",
      "keywords": "$scope africa allowed api asia australiaandoceania cms columns directive docexampleapp europe html iso- js json md mkdocctrl module mycountries ng-controller ng-model ngdisabled ngmodel northamerica pug script sm southamerica ua ui uic-country-list-picker uiccountrylistpickerctrl world xs"
    },
    {
      "section": "api",
      "id": "ui.cms.directive:uicCountryPicker",
      "shortName": "uicCountryPicker",
      "type": "directive",
      "moduleName": "ui.cms",
      "shortDescription": "пикер iso-кода страны",
      "keywords": "$scope africa allowed api asia australiaandoceania cms css- directive docexampleapp europe html input-class input-sm inputclass iso- js mkdocctrl module mycountry ng-controller ng-model ngdisabled ngmodel ngrequired northamerica placeholder pug script southamerica ua ui uic-country-picker uiccountrypickerctrl world"
    },
    {
      "section": "api",
      "id": "ui.cms.directive:uicDatepicker",
      "shortName": "uicDatepicker",
      "type": "directive",
      "moduleName": "ui.cms",
      "shortDescription": "обертка над http://angular-ui.github.io/bootstrap/versioned-docs/1.3.3/#/datepicker",
      "keywords": "$scope allow-invalid api bg-danger bootstrap class cms col-md-33 date2 date3 date4 datedisabled datepicker datepicker-a datepickeroptions directive docexampleapp false formatyear github height html http https io js max-date maxdate min-date mindate mkdocctrl mode module ng-controller ng-model ng-model-type ngdisabled ngmodel ngmodeltype offset proxymodel pug row script showweeks startingday string style text-danger today ui uib-datepicker uib-datepicker-popup uic-datepicker uicdatepickerctrl utc well-sm yyyy"
    },
    {
      "section": "api",
      "id": "ui.cms.directive:uicDatepickerInput",
      "shortName": "uicDatepickerInput",
      "type": "directive",
      "moduleName": "ui.cms",
      "shortDescription": "обертка над http://angular-ui.github.io/bootstrap/versioned-docs/1.3.3/#/datepicker",
      "keywords": "$scope allow-invalid api choose class cms col-md-33 date2 date3 date4 datedisabled datepicker datepicker-a datepickeroptions dd directive docexampleapp fa-calendar-alt fa-car false form-control formatyear github height html http icon-class icon-position input-a io js left max-date maxdate min-date mindate mkdocctrl mmmm mode module ng-controller ng-model ng-model-type ngdisabled ngmodel ngmodeltype offset pug row script showweeks startingday string style today ui uib-datepicker uic-datepicker-input uicdatepickerctrl utc yyyy"
    },
    {
      "section": "api",
      "id": "ui.cms.directive:uicDbModel",
      "shortName": "uicDbModel",
      "type": "directive",
      "moduleName": "ui.cms",
      "shortDescription": "директива для выбора id элементов коллекций из бд (можно выбирать разными стилями, как по 1 id - belongsTo, так и много - hasMany). Пример: в бд есть CmsArticle объекты, с полями id, title и т.п. (см примеры).",
      "keywords": "$constants $representation $scope api array articles belongsto checkbox checkbox- checkbox-inline chekbox class cms cmsarticle coffee css- directive django- docexampleapp drf_lbcms drfs find hasmany html inline input input-class input-sm inputclass items js json margin-top mkdocctrl model-name module mymodel mymodel2 ng-controller ng-model ng-model-type ngdisabled ngmodel null object placeholder pug python radio radio- radio-inline script select style text-danger title todo true ui uic-db-model uicdbmodelctrl"
    },
    {
      "section": "api",
      "id": "ui.cms.directive:uicDisabledOnCurrentUserPermission",
      "shortName": "uicDisabledOnCurrentUserPermission",
      "type": "directive",
      "moduleName": "ui.cms",
      "shortDescription": "",
      "keywords": "$currentuser $scope api cms directive disabled resolvepermissionsstring true ui uicdisabledoncurrentuserpermission"
    },
    {
      "section": "api",
      "id": "ui.cms.directive:uicDisqusComments",
      "shortName": "uicDisqusComments",
      "type": "directive",
      "moduleName": "ui.cms",
      "shortDescription": "виджет, отображающий кол-во комментариев в disqus",
      "keywords": "$location $scope absurl api cms directive disqus docexampleapp html identifier js mkdocctrl module ng-controller pug script split title ui uicdisquscommentsctrl url"
    },
    {
      "section": "api",
      "id": "ui.cms.directive:uicDisqusCommentsCount",
      "shortName": "uicDisqusCommentsCount",
      "type": "directive",
      "moduleName": "ui.cms",
      "shortDescription": "виджет, отображающий кол-во комментариев в disqus",
      "keywords": "$location $scope absurl api cms directive disqus docexampleapp html identifier js mkdocctrl module ng-controller placeholder pug script split ui uicdisquscommentscountctrl url"
    },
    {
      "section": "api",
      "id": "ui.cms.directive:uicEnterPress",
      "shortName": "uicEnterPress",
      "type": "directive",
      "moduleName": "ui.cms",
      "shortDescription": "директива для обработки события нажатия Enter внутри элемента",
      "keywords": "$scope alert api class cms directive docexampleapp enter form-control function hello html js mkdocctrl module ng-controller onenterfunc pug script textarea ui uic-enter-press uicenterpress uicenterpressctrl"
    },
    {
      "section": "api",
      "id": "ui.cms.directive:uicFeedbackForm",
      "shortName": "uicFeedbackForm",
      "type": "directive",
      "moduleName": "ui.cms",
      "shortDescription": "Форма обратной связи. Отправляет письмо админу с полями email, name, text через CmsMailNotification с идентификатором &#39;form.feedback&#39; или указанным через параметры.",
      "keywords": "$form $formdata api class clearfix cms cmsmailnotification col-md-50 custom directive docexampleapp email feedback feedback-form form form-control form-group h3 html input input- int js label label- mkdocctrl module myint ng-controller ng-model number placeholder- pug required row script show-placeholder step text text-danger transclude translate true type ui uic-feedback-form uicfeedbackformctrl"
    },
    {
      "section": "api",
      "id": "ui.cms.directive:uicFileThumb",
      "shortName": "uicFileThumb",
      "type": "directive",
      "moduleName": "ui.cms",
      "shortDescription": "директива для отображения превью мультиязычных объектов(для картинок - превью, для остальных - иконка типа файла).",
      "keywords": "$scope api cms cms_storage cz de default default-url defaulturl description directive ea en en-5789de3dbb9a43029fb38021 file height hidename image jpg l10nanyfile mainimg meta mycmsobject null originalname praha pug ru size thumbs title type ua ui uic-file-thumb url width"
    },
    {
      "section": "api",
      "id": "ui.cms.directive:uicFooterNav",
      "shortName": "uicFooterNav",
      "type": "directive",
      "moduleName": "ui.cms",
      "shortDescription": "директива, которая генерирует элементы для navbar-nav, который можно размещать в footer-е.",
      "keywords": "$scope _blank action api app attrs cms data directive docexampleapp dropdown- en footer- google href html items js link menu mkdocctrl module nav navbar-nav ng-controller ng-model ngmodel pug ru script someactionservice title type ui ui-lang-picker-for-navbar ui-sref uic-footer-nav uicfooternavctrl uicnavbarnav yandex"
    },
    {
      "section": "api",
      "id": "ui.cms.directive:uicHref",
      "shortName": "uicHref",
      "type": "directive",
      "moduleName": "ui.cms",
      "shortDescription": "устанавливает атрибут href для элемента согласно переданному l10nObject",
      "keywords": "api cms directive fileid href l10nobject ui uichref"
    },
    {
      "section": "api",
      "id": "ui.cms.directive:uicIfOnCurrentUserPermission",
      "shortName": "uicIfOnCurrentUserPermission",
      "type": "directive",
      "moduleName": "ui.cms",
      "shortDescription": "",
      "keywords": "$currentuser $scope api cms directive html resolvepermissionsstring ui uicifoncurrentuserpermission"
    },
    {
      "section": "api",
      "id": "ui.cms.directive:uicIframe",
      "shortName": "uicIframe",
      "type": "directive",
      "moduleName": "ui.cms",
      "shortDescription": "",
      "keywords": "$scope api cms directive docexampleapp html js mkdocctrl module ng-controller ngdisabled ngmodel script ui uiciframectrl"
    },
    {
      "section": "api",
      "id": "ui.cms.directive:uicInputFile",
      "shortName": "uicInputFile",
      "type": "directive",
      "moduleName": "ui.cms",
      "shortDescription": "директива-замена стандартного тега input(type=&#39;file&#39;),",
      "keywords": "$error accept api audio btn btn-success cms cms_app coffee directive file input kb max-size maxsize mb mime my_file myfile ng-model ngdisabled ngfile ngmodel pug true ui uic-input-file video"
    },
    {
      "section": "api",
      "id": "ui.cms.directive:uicLangPicker",
      "shortName": "uicLangPicker",
      "type": "directive",
      "moduleName": "ui.cms",
      "shortDescription": "Пикер языка. Языки - не все перечисленные в CMS_ALLOWED_LANGS, а только те, которые",
      "keywords": "$scope api class cms cms_allowed_langs cmslangcodes cmssettings directive disabled display docexampleapp dropdown form-control html input input-a input-group input-group-btn iso- js lang lg margin-bottom md mkdocctrl module ng-controller ng-model ngdisabled ngmodel placeholder pug ru script search select size sm style text type ui uic-lang-picker uiclangpickerctrl xs"
    },
    {
      "section": "api",
      "id": "ui.cms.directive:uicLocalFilePicker",
      "shortName": "uicLocalFilePicker",
      "type": "directive",
      "moduleName": "ui.cms",
      "shortDescription": "надстройка над uicInputFile отображает превью для картинки, выбранной на компьютере пользователя, или если это файл другого типа - иконку типа файла.",
      "keywords": "$error api btn btn-success class clearfix cms col-md-30 directive docexampleapp file forcecmsstoragetype html js kb l10nfile max-size maxsize mb mime mkdocctrl module my_file myfile ng-controller ng-model ngdisabled ngmodel null oldl10nfile pug script true ui uic-local-file-picker uicinputfile uicinputfilepreviewctrl"
    },
    {
      "section": "api",
      "id": "ui.cms.directive:uicMapCoordinatePicker",
      "shortName": "uicMapCoordinatePicker",
      "type": "directive",
      "moduleName": "ui.cms",
      "shortDescription": "Пикер координаты на основе Google Maps. Его стоит использовать, т.к. библиотеки от гуглекарт подгружаются только тогда, когда виджет видим, а не всегда по умолчанию(что только увеличивает размер загружаемой пользователем старницы)",
      "keywords": "$scope add_autocomplete allow-searchbox allowsearchbox api autocompletetypes cms directive docexampleapp edit geopoint google green hidesetlocationbtn html https js json location maps markercolor markericon mkdocctrl module mylocation ng-controller ng-model ngdisabled ngmodel null options placeholder pug required script search searchbox searchbox- select svg- true type types ui uic-map-coordinate-picker uicmapcoordinatepickerctrl view"
    },
    {
      "section": "api",
      "id": "ui.cms.directive:uicNavbarNav",
      "shortName": "uicNavbarNav",
      "type": "directive",
      "moduleName": "ui.cms",
      "shortDescription": "директива, которая генерирует элементы для navbar-nav. Подробнее см. пример.",
      "keywords": "$cms $scope _blank action align api app attrs button center class cms collapse collapsed container-fluid data directive docexampleapp editable en google href html html- icon-bar isnavbarcollapsed iteminnertemplate items js left link menu mkdocctrl module nav navbar navbar-brand navbar-collapse navbar-default navbar-header navbar-nav navbar-toggle ng- ng-click ng-controller ng-model ngmodel pug router ru script site someactionservice span title togglenavbar type ui ui-lang-picker-for-navbar ui-sref uib-collapse uic-navbar-nav uicenavbarnaveditor uicnavbarnavctrl yandex"
    },
    {
      "section": "api",
      "id": "ui.cms.directive:uicNoItems",
      "shortName": "uicNoItems",
      "type": "directive",
      "moduleName": "ui.cms",
      "shortDescription": "плашка с указанием о том, что объектов нет, или о том, что",
      "keywords": "$scope alert api class cms col-md-50 directive docexampleapp false filter function hello html is-filtered isfiltered js mkdocctrl module myisfiltered mysearchterm mysearchterm2 ng-controller ngdisabled pug reset reset-filter resetfilter row script search-term searchterm true ui uice-no-items uicnoitemsctrl"
    },
    {
      "section": "api",
      "id": "ui.cms.directive:uicPagination",
      "shortName": "uicPagination",
      "type": "directive",
      "moduleName": "ui.cms",
      "shortDescription": "директива подобная uibPagination, только без кучи обязательных параметров.",
      "keywords": "$scope api cms directive docexampleapp html js mkdocctrl module ng-controller ng-model ngdisabled ngmodel pug script string total-pages totalpages ui uibpagination uic-pagination uicpaginationctrl"
    },
    {
      "section": "api",
      "id": "ui.cms.directive:uicParallax",
      "shortName": "uicParallax",
      "type": "directive",
      "moduleName": "ui.cms",
      "shortDescription": "директива, которая добавляет эффект параллакса, при этом если передать ссылку на L10nImage файл, то этот файл будет установлен как background-image у всего тега",
      "keywords": "$scope additionalbackgroundimage api background-image bg-primary br class clearfix cms col-md-50 css directive docexampleapp fileid height html https js l10nfile l10nimage md mkdocctrl module myfile ng-controller org png pug python screensize script style thumb ui uic-parallax uicparallax uicparralaxctrl url xs"
    },
    {
      "section": "api",
      "id": "ui.cms.directive:uicPdfViewer",
      "shortName": "uicPdfViewer",
      "type": "directive",
      "moduleName": "ui.cms",
      "shortDescription": "",
      "keywords": "$scope api cms directive docexampleapp html js mkdocctrl module ng-controller ngdisabled ngmodel pug script ui uicpdfviewerctrl"
    },
    {
      "section": "api",
      "id": "ui.cms.directive:uicPicture",
      "shortName": "uicPicture",
      "type": "directive",
      "moduleName": "ui.cms",
      "shortDescription": "директива, которая заменяет uicImg*. Должно быть указано ИЛИ file ИЛИ fileList и fileId",
      "keywords": "api cms directive docexampleapp file fileid filelist hello html js mkdocctrl module ng-controller pug script searchterm size thumbs ui uicimg uicpicturectrl uicsearchinputctrl"
    },
    {
      "section": "api",
      "id": "ui.cms.directive:uicScrollToTopButton",
      "shortName": "uicScrollToTopButton",
      "type": "directive",
      "moduleName": "ui.cms",
      "shortDescription": "директива для прокрутки страницы вверх. Укажите дополнительные",
      "keywords": "api bottom bottom-left bottom-right class cms directive docexampleapp html module ngdisabled pug scrollto top ui uic-scroll-to-top-button"
    },
    {
      "section": "api",
      "id": "ui.cms.directive:uicSearchInput",
      "shortName": "uicSearchInput",
      "type": "directive",
      "moduleName": "ui.cms",
      "shortDescription": "виджет для ввода строки под поиск",
      "keywords": "$scope api cms directive docexampleapp hello html input-sm inputclass js mkdocctrl module ng-controller ng-model ngdisabled ngmodel pug script search searchterm type ui uic-search-input uicsearchinputctrl"
    },
    {
      "section": "api",
      "id": "ui.cms.directive:uicSelect",
      "shortName": "uicSelect",
      "type": "directive",
      "moduleName": "ui.cms",
      "shortDescription": "директива-аналог ui-select от ангулара. но она избавлена от необходимости тащить с собой левые css",
      "keywords": "$scope api class cms coffee coffeescript col-xs-100 css css- directive docexampleapp fa-check fas hello html input-a input-sm inputclass item items js mkdocctrl module mychoicestemplate myitems mymodel myselectedtemplate ng-controller ng-model ng-required ngdisabled ngmodel ngrequired null placeholder pug row script selected span text true ui ui-select uic-select uicselectctrl word world"
    },
    {
      "section": "api",
      "id": "ui.cms.directive:uicSiteBrandA",
      "shortName": "uicSiteBrandA",
      "type": "directive",
      "moduleName": "ui.cms",
      "shortDescription": "круглая кнопка с логотипом сайта.",
      "keywords": "$scope _blank api aribnb booking cms directive docexampleapp facebook href html http js margin-right mkdocctrl module mysites ng-controller ng-repeat odnoklassniki pug ru script site style tripadvisor twitter ui uic-site-brand-a uicsitebrandactrl vk youtube"
    },
    {
      "section": "api",
      "id": "ui.cms.directive:uicSiteReviewForm",
      "shortName": "uicSiteReviewForm",
      "type": "directive",
      "moduleName": "ui.cms",
      "shortDescription": "Форма отзыва о сайте/работе. Создает CmsSiteReview с полями title, body, description.",
      "keywords": "$form $formdata api body class clearfix cms cmssitereview col-md-50 custom description directive docexampleapp email form form-control form-group h3 html input input- int js label label- mkdocctrl module myint ng-controller ng-model number placeholder- pug required row script show-placeholder site-review-form step title transclude translate true type ui uic-site-review-form uicsitereviewformctrl"
    },
    {
      "section": "api",
      "id": "ui.cms.directive:uicState",
      "shortName": "uicState",
      "type": "directive",
      "moduleName": "ui.cms",
      "shortDescription": "директива-обертка для легкого отображения состояний",
      "keywords": "$scope api class cms directive fail false font-size icon-ban-circle icon-ok-sign mystate pug style text-error text-success true ui uic-state"
    },
    {
      "section": "api",
      "id": "ui.cms.directive:uicStickySidebar",
      "shortName": "uicStickySidebar",
      "type": "directive",
      "moduleName": "ui.cms",
      "shortDescription": "обертка над плагином создающим прикрепленный сайдбар. Подробнее на https://github.com/abouolia/sticky-sidebar",
      "keywords": "ad adipisicing aliqua aliquip amet anim api aute bg-primary cillum class clearfix cms col-md-30 col-md-70 commodo consectetur consequat container-selector containerselector content content-container culpa cupidatat deserunt directive docexampleapp dolor dolore duis ea eiusmod elit enim esse est eu excepteur exercitation fugiat h1 h3 hello html https incididunt ipsum irure js labore laboris laborum lorem magna minim mkdocctrl module mollit ng-controller nisi nostrud nulla occaecat officia padding pariatur proident pug qui quis reprehenderit row script sed sidebar sint sit sticky style sunt tempor top-spacing ui uic-sticky-sidebar uicstickysidebarctrl ullamco ut velit veniam voluptate"
    },
    {
      "section": "api",
      "id": "ui.cms.directive:uicTelephoneInput",
      "shortName": "uicTelephoneInput",
      "type": "directive",
      "moduleName": "ui.cms",
      "shortDescription": "инпут для телефонных номеров. Загрузится вот этот виджет https://github.com/jackocnr/intl-tel-input",
      "keywords": "$error $scope allowed-countries allowedcountries api background class cms cms_allowed_langs css- directive docexampleapp europe form html https initialcountry input-sm inputclass js json mkdocctrl module ng-controller ng-model ng-show ngdisabled ngmodel ngrequired padding phone phonenumber preferred-countries preferredcountries ru script service style telephone text-danger ua ui uic-telephone-input uictelephoneinputctrl uk well-sm white"
    },
    {
      "section": "api",
      "id": "ui.cms.directive:uicTimepicker",
      "shortName": "uicTimepicker",
      "type": "directive",
      "moduleName": "ui.cms",
      "shortDescription": "пикер времени. Может работать как с текстовыми представлениями в формате &quot;XX:YY&quot;, так и с js-объектом Date",
      "keywords": "$scope api class cms col-sm-25 datetimes directive docexampleapp dt false function html inputclass js js- length max-time maxdate maxtime mindate mintime mkdocctrl module mydate mystringdate mytimedisabled ng-controller ng-model ng-model-type ngdisabled ngmodel ngmodeltype promise pug return row script select- step string time-disabled timedisabled ui uic-timepicker uictimepickerctrl xx"
    },
    {
      "section": "api",
      "id": "ui.cms.directive:uicTimezonePicker",
      "shortName": "uicTimezonePicker",
      "type": "directive",
      "moduleName": "ui.cms",
      "shortDescription": "пикер названия таймзоны",
      "keywords": "$scope africa allowed america api asia australia cms css- directive docexampleapp europe html input-class input-sm inputclass js mkdocctrl module mytimezone ng-controller ng-model ngdisabled ngmodel ngrequired placeholder pug script ui uic-timezone-picker uictimezonepickerctrl world"
    },
    {
      "section": "api",
      "id": "ui.cms.directive:uicvArticle",
      "shortName": "uicvArticle",
      "type": "directive",
      "moduleName": "ui.cms",
      "shortDescription": "отображает один объект CmsArticle. Для",
      "keywords": "$directiveoverrideprovider api cms cmsarticle de directive en forcelang item ru ui"
    },
    {
      "section": "api",
      "id": "ui.cms.directive:uicvSiteReview",
      "shortName": "uicvSiteReview",
      "type": "directive",
      "moduleName": "ui.cms",
      "shortDescription": "отображает один объект CmsSiteReview. Для",
      "keywords": "$directiveoverrideprovider api cms cmssitereview css de directive en forcelang item itemclass lw-reviews-object ru ui"
    },
    {
      "section": "api",
      "id": "ui.cms.editable",
      "shortName": "ui.cms.editable",
      "type": "overview",
      "moduleName": "ui.cms.editable",
      "shortDescription": "модуль содержит директивы для базового редактирования объектов",
      "keywords": "api cms editable models overview ui"
    },
    {
      "section": "api",
      "id": "ui.cms.editable.$stateOverrideProvider",
      "shortName": "ui.cms.editable.$stateOverrideProvider",
      "type": "object",
      "moduleName": "ui.cms.editable",
      "shortDescription": "провайдер для переопределения объявления views из ui.router.",
      "keywords": "$controller $scope $stateoverrideprovider $stateprovider angular api app body bootstrap cms config controller editable element github hello html http io list login method model module myapp mynewfunc news newslistctrl object override overrideloginview overridemyapp overridenewslist overridenewslistctrl ready return router stateconfig statename templateurl ui views"
    },
    {
      "section": "api",
      "id": "ui.cms.editable.$tableViewProvider",
      "shortName": "ui.cms.editable.$tableViewProvider",
      "type": "object",
      "moduleName": "ui.cms.editable",
      "shortDescription": "провайдер для переопределения aggrid или других рендеров таблиц",
      "keywords": "$tableviewprovider aggrid api cms editable object ui"
    },
    {
      "section": "api",
      "id": "ui.cms.editable.$translationService",
      "shortName": "ui.cms.editable.$translationService",
      "type": "object",
      "moduleName": "ui.cms.editable",
      "shortDescription": "Сервис для получения переводов с google translate. Кеширует переводы",
      "keywords": "$translationservice api cms editable google localstorage object sessionstorage translate ui"
    },
    {
      "section": "api",
      "id": "ui.cms.editable.directive:uiceAvatarManager",
      "shortName": "uiceAvatarManager",
      "type": "directive",
      "moduleName": "ui.cms.editable",
      "shortDescription": "виджет для загрузки/редактирования/удаления аватарки mainImg у объекта CmsUser",
      "keywords": "$scope api app circle class cms cmsuser col-sm-50 defaultctrl directive django docexampleapp editable findbyid html int mainimg module mongodb ng-controller ng-model ngdisabled ngmodel objectid pug row sm text-danger ui uice-avatar-manager user"
    },
    {
      "section": "api",
      "id": "ui.cms.editable.directive:uiceColorPicker",
      "shortName": "uiceColorPicker",
      "type": "directive",
      "moduleName": "ui.cms.editable",
      "shortDescription": "выбирает цвета в hex. Для браузеров, которые не поддерживают input[type=&#39;color&#39;]",
      "keywords": "$scope api class cms col-md-50 color css- directive docexampleapp editable form-group hex hex- html html5 https input input- input-class input-sm inputclass js js- mkdocctrl module ng-controller ng-model ngdisabled ngmodel placeholder pug rgb rgba rgbacolor rgbcolor row script show-text showtext true type ui uice-color-picker uicecolorpickerctrl without-html5"
    },
    {
      "section": "api",
      "id": "ui.cms.editable.directive:uiceContactsEditor",
      "shortName": "uiceContactsEditor",
      "type": "directive",
      "moduleName": "ui.cms.editable",
      "shortDescription": "Редактор контактов для SiteSettings",
      "keywords": "$scope api cms contacts description directive docexampleapp editable email en hello html js mkdocctrl module ng-controller ng-model ngdisabled ngmodel pug script sitesettings tel ui uice-contacts-editor uicecontactseditorctrl"
    },
    {
      "section": "api",
      "id": "ui.cms.editable.directive:uiceDbModelList",
      "shortName": "uiceDbModelList",
      "type": "directive",
      "moduleName": "ui.cms.editable",
      "shortDescription": "директива, которая позволяет выбирать id объектов моделей из бд в массив. При этом можно менять позиции id в массиве, создавать новые элементы из бд (отрисовывается кнопка +).",
      "keywords": "$constants $injector $representation $scope add api app cms cmsarticle coffee directive docexampleapp editable function html items js json length mkdocctrl model-empty model-name modeleditors modelname module myidsarray ng-controller ng-if ng-model ngdisabled ngmodel oncreateclick pug script test title ui ui-sref uice-db-model-list uicedbmodellistctrl uisrefcreate uisrefedit update var"
    },
    {
      "section": "api",
      "id": "ui.cms.editable.directive:uiceDurationpicker",
      "shortName": "uiceDurationpicker",
      "type": "directive",
      "moduleName": "ui.cms.editable",
      "shortDescription": "",
      "keywords": "$scope api cms coffee directive djangoproject docexampleapp duration durationfield editable html https inputclass js json mkdocctrl module ng-controller ng-model ngdisabled ngmodel pug script select- ui uice-durationpicker uicedurationpickerctrl uuuuuu"
    },
    {
      "section": "api",
      "id": "ui.cms.editable.directive:uiceFaIconPicker",
      "shortName": "uiceFaIconPicker",
      "type": "directive",
      "moduleName": "ui.cms.editable",
      "shortDescription": "директива для выбора иконок из сета FontAwesome",
      "keywords": "$scope api class cms col-md-50 css directive docexampleapp editable fa fa-plus fahtml fas fontawesome html icon js json mkdocctrl module ng-controller ng-model ngdisabled ngmodel pug row script type ui uice-fa-icon-picker uicefaiconpickerrctrl"
    },
    {
      "section": "api",
      "id": "ui.cms.editable.directive:uiceFileDropzone",
      "shortName": "uiceFileDropzone",
      "type": "directive",
      "moduleName": "ui.cms.editable",
      "shortDescription": "виджет для поддержки зоны дропдауна файла. Виджет ничего не загружает на сервер,",
      "keywords": "$scope ae alert api cms console directive div docexampleapp dropzone-alert dropzone-content editable files html js log maxcount mkdocctrl module mycb myerror ng-controller ng-if on-upload onupload pug script ui uicefiledropzonectrl upload"
    },
    {
      "section": "api",
      "id": "ui.cms.editable.directive:uiceFileIdPicker",
      "shortName": "uiceFileIdPicker",
      "type": "directive",
      "moduleName": "ui.cms.editable",
      "shortDescription": "",
      "keywords": "$scope accept api article cms cms-model-item cmsarticle cmsmodelitem css- directive editable files findone image input-lg input-size input-sm inputclass item mime- myid ngdisabled ngmodel null pug resource- sm ui uice-file-id-picker video"
    },
    {
      "section": "api",
      "id": "ui.cms.editable.directive:uiceFileManager",
      "shortName": "uiceFileManager",
      "type": "directive",
      "moduleName": "ui.cms.editable",
      "shortDescription": "виджет для загрузки/редактирования/удаления файлов из списка",
      "keywords": "$scope api article bootstrap-panel class cms cmsarticle col-md-50 directive docexampleapp editable field files files-label finone html image item js l10nanyfile main manage-main-img mime- mkdocctrl module ng-controller ng-model ngmodel panel pug row script set true type ui uice-file-manager uicefilemanagerdocctrl upload"
    },
    {
      "section": "api",
      "id": "ui.cms.editable.directive:uiceItemIdListEditor",
      "shortName": "uiceItemIdListEditor",
      "type": "directive",
      "moduleName": "ui.cms.editable",
      "shortDescription": "редактор массива id элементов",
      "keywords": "addbuttontext api array cms directive editable items ngdisabled ngmodel onchange ui"
    },
    {
      "section": "api",
      "id": "ui.cms.editable.directive:uiceL10nInput",
      "shortName": "uiceL10nInput",
      "type": "directive",
      "moduleName": "ui.cms.editable",
      "shortDescription": "директива для ввода значений в l10nObject, string, number переменные.",
      "keywords": "$scope api cms cms_default_lang directive docexampleapp editable email en form-control hello html input- input-a input-class input-sm js l10nobject l10nobject- lang max maxlength min minlength mkdocctrl module ng-controller ng-model ngdisabled ngmodel nomodel number object password pug required ru script span-label string text textarea translate type ui uice-l10n-input uicel10ninputctrl url"
    },
    {
      "section": "api",
      "id": "ui.cms.editable.directive:uiceListEditor",
      "shortName": "uiceListEditor",
      "type": "directive",
      "moduleName": "ui.cms.editable",
      "shortDescription": "",
      "keywords": "$item $scope add api bottom class cms directive docexampleapp editable elem form-control form-group function hello html input inputplaceholder item-body js label mkdocctrl module myarr ng-controller ng-model ngdisabled ngmodel on-add onadd placeholder pug push required script text title top ui uice-list-editor uicelisteditorctrl"
    },
    {
      "section": "api",
      "id": "ui.cms.editable.directive:uiceLoginForm",
      "shortName": "uiceLoginForm",
      "type": "directive",
      "moduleName": "ui.cms.editable",
      "shortDescription": "директива формы логина пользователя. Поддерживает",
      "keywords": "admin api cms col-lg-30 col-lg-offset-35 directive editable form-inline formclass h2 input- labels ngdisabled onerror onlogin placeholders pug showplaceholder true ui uice-login-form"
    },
    {
      "section": "api",
      "id": "ui.cms.editable.directive:uiceLogout",
      "shortName": "uiceLogout",
      "type": "directive",
      "moduleName": "ui.cms.editable",
      "shortDescription": "директива кнопки разлогина пользователя.",
      "keywords": "api cms directive editable ngdisabled onerror onlogout pug ui uice-logout"
    },
    {
      "section": "api",
      "id": "ui.cms.editable.directive:uiceMainImgManager",
      "shortName": "uiceMainImgManager",
      "type": "directive",
      "moduleName": "ui.cms.editable",
      "shortDescription": "виджет для загрузки/редактирования/удаления поля mainImg(или любого другого указаного в атрибуте field) у объекта",
      "keywords": "$scope api app cms cmsarticle directive django editable field findbyid image int item l10nimagefile main mainimg mongodb ngdisabled ngmodel objectid pug ui uice-main-img-manager"
    },
    {
      "section": "api",
      "id": "ui.cms.editable.directive:uiceNavbarNavEditor",
      "shortName": "uiceNavbarNavEditor",
      "type": "directive",
      "moduleName": "ui.cms.editable",
      "shortDescription": "редактор меню",
      "keywords": "$scope _blank api app attrs cms data directive docexampleapp editable en google href html items js link menu mkdocctrl module nav ng-controller ng-model ngmodel pug ru script title type ui ui-lang-picker-for-navbar ui-sref uice-navbar-nav-editor uicenavbarnaveditorctrl yandex"
    },
    {
      "section": "api",
      "id": "ui.cms.editable.directive:uiceNoItems",
      "shortName": "uiceNoItems",
      "type": "directive",
      "moduleName": "ui.cms.editable",
      "shortDescription": "плашка с указанием о том, что объектов нет, или о том, что",
      "keywords": "$scope alert api class cms col-md-50 directive docexampleapp editable false filter function hello html is-filtered isfiltered js mkdocctrl module myisfiltered mysearchterm mysearchterm2 ng-controller ngdisabled pug reset reset-filter resetfilter row script search-term searchterm true ui uice-no-items uicenoitemsctrl"
    },
    {
      "section": "api",
      "id": "ui.cms.editable.directive:uiceRegisterForm",
      "shortName": "uiceRegisterForm",
      "type": "directive",
      "moduleName": "ui.cms.editable",
      "shortDescription": "",
      "keywords": "$scope api cms directive docexampleapp editable html js mkdocctrl module ng-controller ngdisabled ngmodel pug script ui uiceregisterformctrl"
    },
    {
      "section": "api",
      "id": "ui.cms.editable.directive:uiceSocialLinksEditor",
      "shortName": "uiceSocialLinksEditor",
      "type": "directive",
      "moduleName": "ui.cms.editable",
      "shortDescription": "редактор массива ссылок на социальные сети",
      "keywords": "api array cms directive editable facebook ngmodel onchange ui vk"
    },
    {
      "section": "api",
      "id": "ui.cms.editable.directive:uiceStringListEditor",
      "shortName": "uiceStringListEditor",
      "type": "directive",
      "moduleName": "ui.cms.editable",
      "shortDescription": "редактор массива строк(или других данных)",
      "keywords": "addbuttontext api array cms directive editable email hello inputplaceholder inputtype maxlength ngdisabled ngmodel onchange text ui world"
    },
    {
      "section": "api",
      "id": "ui.cms.editable.directive:uiceTableView",
      "shortName": "uiceTableView",
      "type": "directive",
      "moduleName": "ui.cms.editable",
      "shortDescription": "",
      "keywords": "$scope api cms directive docexampleapp editable html items js mkdocctrl module ng-controller pug script ui uicetableviewctrl"
    },
    {
      "section": "api",
      "id": "ui.cms.editable.directive:uiceTabset",
      "shortName": "uiceTabset",
      "type": "directive",
      "moduleName": "ui.cms.editable",
      "shortDescription": "Директива подобная uib-tabset, также содержит",
      "keywords": "$index $scope active alert alertme api callback class cms content cool directive disable disabled docexampleapp dynamic editable fa-car fas function heading html js mkdocctrl module ng-controller ng-repeat pretty pug script select sm static tab tabs title true ui uib-tabset uibtabset uice-tabset uice-tabset-tab uice-tabset-tab-heading uicetabset uicetabsetctrl ve"
    },
    {
      "section": "api",
      "id": "ui.cms.editable.service:uiceL10nFileEditorModal",
      "shortName": "uiceL10nFileEditorModal",
      "type": "service",
      "moduleName": "ui.cms.editable",
      "shortDescription": "сервис дает возможность вызывать модальное окно для загрузки",
      "keywords": "$q $scope accept allowedittitledescription angularjs api audio avatarimg boolean cms cmsarticle cmskeyword delete edit editable false file formodelfield https item kb l10nfile l10nfile- lbservices mainimg maxsize mb mime null options org promise property service string successcb title ui uicel10nfileeditormodal video view"
    },
    {
      "section": "api",
      "id": "ui.cms.filter:chunk",
      "shortName": "chunk",
      "type": "filter",
      "moduleName": "ui.cms",
      "shortDescription": "фильтр разбивает массив объектов на несколько массивов размером size-объектов",
      "keywords": "$scope api array chunk cms div filter horiz item items myarray pug size size- ui vert"
    },
    {
      "section": "api",
      "id": "ui.cms.filter:constantToText",
      "shortName": "constantToText",
      "type": "filter",
      "moduleName": "ui.cms",
      "shortDescription": "фильтр переводит константное значение (определенное через $constantsProvider) в текст. Если найденное значение - объект, то он будет пропущен",
      "keywords": "$constants $constantsprovider $scope api cms cms_order constantpath constanttotext filter l10n order pug status translate ui"
    },
    {
      "section": "api",
      "id": "ui.cms.filter:countryCodeToText",
      "shortName": "countryCodeToText",
      "type": "filter",
      "moduleName": "ui.cms",
      "shortDescription": "возвращает название страны по ее 2-х значному коду",
      "keywords": "$langpicker $scope api cms code country countrycode countrycodetotext currentlang de filter forcelang invalid pug ua ui"
    },
    {
      "section": "api",
      "id": "ui.cms.filter:cut",
      "shortName": "cut",
      "type": "filter",
      "moduleName": "ui.cms",
      "shortDescription": "фильтр красиво обрезает строку в размер, добавляя в конец троеточие",
      "keywords": "$scope api cms cut filter hello max mytext pug true ui wordwise world"
    },
    {
      "section": "api",
      "id": "ui.cms.filter:duration",
      "shortName": "duration",
      "type": "filter",
      "moduleName": "ui.cms",
      "shortDescription": "Formats duration to a humanized string based on the requested format.",
      "keywords": "$scope api based cms description djangoproject duration duration1 durationfield filter format formats formatted formatting hours https humanized info medium minutes predefined pug requested rules seconds short short-dot string ui uuuuuu"
    },
    {
      "section": "api",
      "id": "ui.cms.filter:humanBytes",
      "shortName": "humanBytes",
      "type": "filter",
      "moduleName": "ui.cms",
      "shortDescription": "фильтр отображает размер файла в b, kb, mb, tb, pb",
      "keywords": "$scope api cms filter humanbytes kb mb mysize1 mysize2 pb pug tb ui"
    },
    {
      "section": "api",
      "id": "ui.cms.filter:humanCurrency",
      "shortName": "humanCurrency",
      "type": "filter",
      "moduleName": "ui.cms",
      "shortDescription": "фильтр форматирует суммы с знаком валюты",
      "keywords": "$scope api cms currencysymbol czk eur filter humancurrency mycurrency myvalue pug ui usd"
    },
    {
      "section": "api",
      "id": "ui.cms.filter:humanDatetime",
      "shortName": "humanDatetime",
      "type": "filter",
      "moduleName": "ui.cms",
      "shortDescription": "Formats Date to a humanized string based on the requested format.",
      "keywords": "$scope api based cms day description filter format formats formatted formatting hours humandatetime humanized medium object predefined pug requested rules short short-dot string ui"
    },
    {
      "section": "api",
      "id": "ui.cms.filter:l10n",
      "shortName": "l10n",
      "type": "filter",
      "moduleName": "ui.cms",
      "shortDescription": "фильтр для работы с отображением L10n* объектов из cms типа L10nStringXs и т.п.",
      "keywords": "$langpicker $scope api cms cms_default_lang currentlang cz de en filter forcelang hello js l10n l10nobject l10nstringxs mycmsobject pug ru title ua ui"
    },
    {
      "section": "api",
      "id": "ui.cms.filter:time",
      "shortName": "time",
      "type": "filter",
      "moduleName": "ui.cms",
      "shortDescription": "Formats time to a string based on the requested format.",
      "keywords": "$scope api as-is based characters clock cms composed datetime day description elements en_us equivalent escape escaped filter format formats formatted formatting hh hour input iso literal locale marker medium millisecond milliseconds minute mm morning number order output padded pm predefined pug quote quotes recognized requested rules second sequence short shorter single ss sss string surrounding time time1 ui values versions"
    },
    {
      "section": "api",
      "id": "ui.cms.filter:toUrlId",
      "shortName": "toUrlId",
      "type": "filter",
      "moduleName": "ui.cms",
      "shortDescription": "фильтр для конвертирования объектов из cms типа CmsArticle и т.п. в url-пригодные строки с id объекта в начале",
      "keywords": "$scope api cms cms_default_lang cmsarticle cz de en filter forcelang hello l10nobject mycmsobject pug ru title tourlid ua ui url- world"
    },
    {
      "section": "api",
      "id": "ui.cms.filter:toYesNo",
      "shortName": "toYesNo",
      "type": "filter",
      "moduleName": "ui.cms",
      "shortDescription": "фильтр для конвертирования переданного булевого значения в текст Yes/No. Результат переводится сразу фильтром translate.",
      "keywords": "$scope api cms cms_default_lang cz en false filter mytrue pug toyesno translate true ua ui"
    },
    {
      "section": "api",
      "id": "ui.cms.service:$cms",
      "shortName": "$cms",
      "type": "service",
      "moduleName": "ui.cms",
      "shortDescription": "сервис, который содержит вспомогательные функции для работы с сайтом,",
      "keywords": "$bulk $cms $cmsprovider $state alert amazon api bootstrap- brand btn btn-default bucket-a button cb chunk class click cloud cms cmssettings collapsenavbar context delay detectidle error eventtypes false faviconurl filter gettextcatalog google hello idle isnavbarcollapsed js keypress kirill label lg loadsettings logoalturl logourl md mousedown myaction notification notifytype null obj options primary property pug registercustomsiteaction resolvevalueforscreensize rmdetectidle runsiteaction s3 savesettings screensize service settings shownotification sm stoponstatenamechange success template togglenavbar touchend true type-hint type-hint-boolean type-hint-object type-hint-string ui ui-notifications warning xs"
    },
    {
      "section": "api",
      "id": "ui.cms.service:$constants",
      "shortName": "$constants",
      "type": "service",
      "moduleName": "ui.cms",
      "shortDescription": "сервис, который дает возможность работать с константами, которые были определены с помощью $constantsProvider",
      "keywords": "$constantsprovider api cms constantpath l10n property resolve service totext translate ui"
    },
    {
      "section": "api",
      "id": "ui.cms.service:$countries",
      "shortName": "$countries",
      "type": "service",
      "moduleName": "ui.cms",
      "shortDescription": "сервис, который содержит вспомогательные функции для работы с",
      "keywords": "$countries ad af afghanistan africa ag al albania algeria andorra angola antigua ao api armenia asia au australia australiaandoceania austria az azerbaijan bahamas barbados barbuda bb benin bj bs class cms code codetotext country de dz europe fiji fj forcelang getlist invalid ki kiribati label northamerica p1 p2 pn property service southamerica type-hint type-hint-array ua ui world"
    },
    {
      "section": "api",
      "id": "ui.cms.service:$currentUser",
      "shortName": "$currentUser",
      "type": "service",
      "moduleName": "ui.cms",
      "shortDescription": "сервис, который содержит вспомогательные функции для работы с текущим залогиненным пользователем",
      "keywords": "$currentuser admin api avatar canviewpage class cleardata cms cmsuser console controller create_post data directive django- edit_post editable editor false get_all_permissions haspermission isadmin isinrole isuserrole jpg label log my_app myctrl pageobject permission permission_codename permissions property resolvepermissionsstring service setdata sm true type-hint type-hint-number type-hint-object type-hint-string ui uice-avatar-manager url username userrole xs your-user-role"
    },
    {
      "section": "api",
      "id": "ui.cms.service:$timezones",
      "shortName": "$timezones",
      "type": "service",
      "moduleName": "ui.cms",
      "shortDescription": "сервис, который содержит вспомогательные функции для работы с",
      "keywords": "$timezones africa america api asia australia class cms code codetotext europe forcelang getlist invalid label p1 p2 pn property pytz service timezone type-hint type-hint-array ui world"
    },
    {
      "section": "api",
      "id": "ui.cms.service:$worldMaps",
      "shortName": "$worldMaps",
      "type": "service",
      "moduleName": "ui.cms",
      "shortDescription": "сервис, который содержит вспомогательные функции для работы с картами",
      "keywords": "api class cms getfactory google label maps null property provider service type-hint type-hint-string ui uicgooglemapclass"
    },
    {
      "section": "api",
      "id": "ui.cms.service:H",
      "shortName": "H",
      "type": "service",
      "moduleName": "ui.cms",
      "shortDescription": "сервис, который содержит вспомогательные функции для работы с примитивами данных;",
      "keywords": "_blank _self angular api application arraytoobj audio browser_compatibility callback- cb ccs clipboard cms cmssettings console content convert copytoclipboard css css- currency czk data date1 date2 dateend daten datestart days defaultcurrency defaultcurrencyconverter description directive djangoproject downloaddataasfile dump duration durationfield durationobj durationstring element en-5789de3dbb9a43029fb38021 enddate eur execurl extname fa-file-archive-o fa-file-image-o fa-file-text-o fa-file-word-o false filename filetype fontawesome formatsupport fromcurrency generaterange getboundingclientrect getelementposition getfilethumbs getfilethumburl getfiletype getfiletypeicon getfileurl getoriginalname getscrollposition getsize gettypeiconnamebyfiletype gmt height hello hours https htts image info isdateinrange isdaterangeinrange isfilehasthumbs issamedate issameday issamemonth isvalidfiletype items jpg js js- json l10nbasefile l10nfile l10nobject left loaded loadonce localhost log mainimg mar max meta milliseconds mime- min minutes mon mozilla mp3 myfile mytext null objtoarray org originalname param1 param2 param3 param4 parse path pos praha property query_params range rangeend rangestart resolvel10nfilepath resolvel10nfilethumbpath scrollposition seconds service settings size start startdate sun target test text thumbs thumbsize timezoneoffset title todate tofloat toint true tue txt type ui uicimg uicimgthumb undefined url urlparams uuuuuu video viewport webp width world wtf"
    },
    {
      "section": "api",
      "id": "ui.cms.service:UicGoogleMapClass",
      "shortName": "UicGoogleMapClass",
      "type": "service",
      "moduleName": "ui.cms",
      "shortDescription": "UicGoogleMapClass фабрика которая возвращает класс для работы с google maps. Конструктор",
      "keywords": "api avoidhighways avoidtolls center class cms color config coordinate data directionsrenderer directionsservice display distancematrixservice driving element en false findanddisplayroute fitbounds fixme generatestaticimgurl getdistancematrix getlatlng gm google green hidemarkertooltiponinit htmlelement icon infobox infobox- initmap inline-block js js- label lat latlng lng maps mapsize marker marker1 marker2 markers metric null options property red respone ru service setmarker static-map style template text title travelmode true type-hint type-hint-array type-hint-boolean type-hint-number type-hint-object type-hint-string ui uicgooglemapclass undefined unitsystem updatemarkers url useinfobox width yellow zoom"
    },
    {
      "section": "api",
      "id": "ui.landingWidgets.$lwFilterViewProvider",
      "shortName": "ui.landingWidgets.$lwFilterViewProvider",
      "type": "object",
      "moduleName": "ui.landingWidgets",
      "shortDescription": "провайдер для определения фильтровочных объектов",
      "keywords": "$lwfilterviewprovider api landingwidgets object ui"
    },
    {
      "section": "api",
      "id": "ui.landingWidgets.directive:lwCards",
      "shortName": "lwCards",
      "type": "directive",
      "moduleName": "ui.landingWidgets",
      "shortDescription": "директива, которая может генерировать карточки от AdminApp.directive:aCardsEditor",
      "keywords": "$scope _blank adminapp api article articles bg-primary btn btn-default card cards cardsblock class cmsmodelitem data details directive div docexampleapp files fontawesome google hello href html html- inline-block js l10n l10nobject label landingwidgets lg link lw-cards lwcardsctrl md mkdocctrl module ng-controller ng-href ng-model ngmodel padding ru script sm string style target text-center title type type-hint type-hint-array type-hint-number type-hint-object type-hint-string ui ui-sref xs yandex"
    },
    {
      "section": "api",
      "id": "ui.landingWidgets.directive:lwCarousel",
      "shortName": "lwCarousel",
      "type": "directive",
      "moduleName": "ui.landingWidgets",
      "shortDescription": "обертка над страндартной каруселью в бутстрапе, позволяет определять слайды через описание в ng-model переменную, а",
      "keywords": "$scope acorouseleditor adminapp animation api bg-primary carouseldata class cms-model-item cmsgallery cmsmodelitem coffee directive disable disables docexampleapp false files findone gallery html image img-size imgid inline-block interval item jpg js label landingwidgets looping lw-carousel lwcarouselctrl md mkdocctrl module mouseover ng-controller ng-model ngmodel pause pauses pug px script setting slide slide1 slides string transition truthy type type-hint type-hint-array type-hint-boolean type-hint-number type-hint-object type-hint-string ui xs"
    },
    {
      "section": "api",
      "id": "ui.landingWidgets.directive:lwFilterClearView",
      "shortName": "lwFilterClearView",
      "type": "directive",
      "moduleName": "ui.landingWidgets",
      "shortDescription": "",
      "keywords": "$scope api directive docexampleapp html js landingwidgets lwfilterclearviewctrl mkdocctrl module ng-controller pug script ui"
    },
    {
      "section": "api",
      "id": "ui.landingWidgets.directive:lwFilterEditView",
      "shortName": "lwFilterEditView",
      "type": "directive",
      "moduleName": "ui.landingWidgets",
      "shortDescription": "",
      "keywords": "$scope api directive docexampleapp html items js landingwidgets lwfiltereditviewctrl mkdocctrl module ng-controller pug script ui"
    },
    {
      "section": "api",
      "id": "ui.landingWidgets.directive:lwGallery",
      "shortName": "lwGallery",
      "type": "directive",
      "moduleName": "ui.landingWidgets",
      "shortDescription": "",
      "keywords": "$scope api directive ea landingwidgets ngdisabled ngmodel pug ui"
    },
    {
      "section": "api",
      "id": "ui.landingWidgets.directive:lwHeaderScrollDownClass",
      "shortName": "lwHeaderScrollDownClass",
      "type": "directive",
      "moduleName": "ui.landingWidgets",
      "shortDescription": "",
      "keywords": "api directive div landingwidgets lwheaderscrolldownclass myanimation pug ui"
    },
    {
      "section": "api",
      "id": "ui.landingWidgets.directive:lwLegalInformationLinks",
      "shortName": "lwLegalInformationLinks",
      "type": "directive",
      "moduleName": "ui.landingWidgets",
      "shortDescription": "отображает плашку, в которой находятся все ссылки на статьи для категории с identifier==&#39;legal-information&#39;, если они доступны. Если таких статей нет, то виджет невидим",
      "keywords": "api app articles details directive docexampleapp html identifier js landingwidgets legal-information lwlegalinformationlinksctrl mkdocctrl module ng-controller script ui ui-router url"
    },
    {
      "section": "api",
      "id": "ui.landingWidgets.directive:lwLegalInformationRequest",
      "shortName": "lwLegalInformationRequest",
      "type": "directive",
      "moduleName": "ui.landingWidgets",
      "shortDescription": "",
      "keywords": "anonimization api deletion directive docexampleapp html info js landingwidgets lwlegalinformationrequestctrl mkdocctrl module ng-controller ngdisabled script ui"
    },
    {
      "section": "api",
      "id": "ui.landingWidgets.directive:lwMap",
      "shortName": "lwMap",
      "type": "directive",
      "moduleName": "ui.landingWidgets",
      "shortDescription": "",
      "keywords": "$scope api directive landingwidgets ngmodel pug ui"
    },
    {
      "section": "api",
      "id": "ui.landingWidgets.directive:lwQuoteBubble",
      "shortName": "lwQuoteBubble",
      "type": "directive",
      "moduleName": "ui.landingWidgets",
      "shortDescription": "директива для отображения цитат в виде баллонов с подписью и аватаром внизу.",
      "keywords": "api blockquote directive http imgl10nfile imgurl jpg l10nimage landingwidgets lbcms lw-quote-bubble ng-href nghref pug quote sub-title subtitle text title ui url"
    },
    {
      "section": "api",
      "id": "ui.landingWidgets.directive:lwVideo",
      "shortName": "lwVideo",
      "type": "directive",
      "moduleName": "ui.landingWidgets",
      "shortDescription": "",
      "keywords": "$scope api directive landingwidgets ngmodel pug ui"
    },
    {
      "section": "api",
      "id": "ui.landingWidgets.directive:lwViewportEnterClass",
      "shortName": "lwViewportEnterClass",
      "type": "directive",
      "moduleName": "ui.landingWidgets",
      "shortDescription": "",
      "keywords": "api directive div landingwidgets lwviewportenterclass myanimation pug ui"
    },
    {
      "section": "api",
      "id": "ui.landingWidgets.directive:lwViewportEnterFunction",
      "shortName": "lwViewportEnterFunction",
      "type": "directive",
      "moduleName": "ui.landingWidgets",
      "shortDescription": "",
      "keywords": "api directive div landingwidgets lwviewportenterfunction myanimation pug ui"
    },
    {
      "section": "api",
      "id": "window.Array",
      "shortName": "window.Array",
      "type": "object",
      "moduleName": "window",
      "shortDescription": "дополнительные функции для работы с массивами",
      "keywords": "api arr array console findbyproperty function hello insert item log move nan null object property remove removeduplicates removevalue text undefined window world"
    },
    {
      "section": "api",
      "id": "window.Date",
      "shortName": "window.Date",
      "type": "object",
      "moduleName": "window",
      "shortDescription": "дополнительные функции для работы c датами",
      "keywords": "api day endof feb function gmt iso- iso-date iso-time js moment month object sat startof sun test thu todateisostring totimeisostring week window"
    },
    {
      "section": "api",
      "id": "window.Object",
      "shortName": "window.Object",
      "type": "object",
      "moduleName": "window",
      "shortDescription": "дополнительные функции для работы со Object",
      "keywords": "api arraymap callback function hello https mozilla object org other_key test thisarg window world"
    },
    {
      "section": "api",
      "id": "window.String",
      "shortName": "window.String",
      "type": "object",
      "moduleName": "window",
      "shortDescription": "дополнительные функции для работы со строками",
      "keywords": "api function hel hello input match matchall null object prototype regexp replaceall replacement search string test whoa window world"
    }
  ],
  "apis": {
    "api": true
  },
  "html5Mode": false,
  "editExample": false,
  "startPage": "/api/ui.cms",
  "scripts": [
    "angular.js",
    "ui-bootstrap-tpls.js",
    "angular-ui-router.js",
    "angular-animate.js",
    "angular-gettext.js",
    "angular-scroll.js",
    "loading-bar.js",
    "uiFlagCss16.min.js",
    "ui.gettext.langPicker.js",
    "angular-resource.js",
    "angular-pageslide-directive.js",
    "angular-ui-notification.js",
    "gdprStorage-withTranslations.js",
    "config.js",
    "resources.js",
    "cms.js",
    "cms-editable.js",
    "cms-lw.js",
    "admin.js",
    "module.js"
  ]
};