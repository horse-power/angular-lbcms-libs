(function() {
  var ATTR_NAME, getLinkCardElement;

  ATTR_NAME = 'uic-special-ckeditor-card';

  getLinkCardElement = function(element) {
    var attrs;
    if (!element) {
      return null;
    }
    attrs = element.getAttributes();
    if (attrs.hasOwnProperty(ATTR_NAME)) {
      return element;
    }
    return getLinkCardElement(element.getParent());
  };

  CKEDITOR.plugins.add('linkcard', {
    requires: 'widget,lbcmshelpers',
    icons: 'linkcard',
    lang: 'en,ru',
    onLoad: function() {
      var borderRad, padding;
      borderRad = 3;
      padding = 5;
      return CKEDITOR.addCss("div[" + ATTR_NAME + "]{\n    position: relative;\n    background-color:#ff0;\n    border-radius: " + borderRad + "px;\n    padding: " + padding + "px;\n    margin-bottom: " + padding + "px;\n}\ndiv[" + ATTR_NAME + "]:before{\n    line-height: 1;\n    display: block;\n    padding: " + padding + "px;\n    margin-left: -" + padding + "px;\n    margin-right: -" + padding + "px;\n    margin-top: -" + padding + "px;\n    margin-bottom: " + padding + "px;\n    border-top-left-radius: " + borderRad + "px;\n    border-top-right-radius: " + borderRad + "px;\n    background: #91850b;\n    color: white;\n    content: '';\n    cursor: default;\n}\ndiv[" + ATTR_NAME + "] > *:last-child{\n    margin-bottom: 0;\n}\ndiv[" + ATTR_NAME + "] > *:first-child{\n    margin-top: 0;\n}");
    },
    init: function(editor) {
      var lang;
      lang = editor.lang.linkcard;
      editor.addCommand('linkcard', new CKEDITOR.dialogCommand('linkcardDialog'));
      editor.ui.addButton('Linkcard', {
        label: lang.dialogTitle,
        command: 'linkcard',
        toolbar: 'insert'
      });
      if (editor.contextMenu) {
        editor.addMenuGroup('linkcardGroup');
        editor.addMenuItem('linkcardItem', {
          label: lang.menuItemEdit,
          icon: this.path + 'icons/linkcard.png',
          command: 'linkcard',
          group: 'linkcardGroup'
        });
        editor.contextMenu.addListener(function(element) {
          if (getLinkCardElement(element)) {
            return {
              linkcardItem: CKEDITOR.TRISTATE_OFF
            };
          }
        });
      }
      CKEDITOR.dialog.add('linkcardDialog', this.path + 'dialogs/linkcard.js');
      CKEDITOR.addCss("div[" + ATTR_NAME + "]:before{\n    content: '" + lang.dialogTitle + " (" + lang.rmcHelpText + ")';\n}");
    }
  });

  CKEDITOR.plugins.linkcard = {
    ATTR_NAME: ATTR_NAME,
    getLinkCardElement: getLinkCardElement,
    parseAttributes: function(element) {
      var attrs, e;
      element = getLinkCardElement(element);
      attrs = {};
      attrs[ATTR_NAME] = {};
      try {
        attrs[ATTR_NAME] = JSON.parse(element.getAttribute(ATTR_NAME));
      } catch (error) {
        e = error;
      }
      return attrs;
    }
  };

}).call(this);
