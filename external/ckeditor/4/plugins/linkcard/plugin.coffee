ATTR_NAME = 'uic-special-ckeditor-card'

getLinkCardElement = (element)->
    if !element then return null
    attrs = element.getAttributes()
    if attrs.hasOwnProperty(ATTR_NAME)
        return element
    getLinkCardElement(element.getParent())


CKEDITOR.plugins.add 'linkcard',
    requires: 'widget,lbcmshelpers'
    icons: 'linkcard'
    lang: 'en,ru'
    onLoad: ()->
        borderRad = 3
        padding = 5
        CKEDITOR.addCss("""
            div[#{ATTR_NAME}]{
                position: relative;
                background-color:#ff0;
                border-radius: #{borderRad}px;
                padding: #{padding}px;
                margin-bottom: #{padding}px;
            }
            div[#{ATTR_NAME}]:before{
                line-height: 1;
                display: block;
                padding: #{padding}px;
                margin-left: -#{padding}px;
                margin-right: -#{padding}px;
                margin-top: -#{padding}px;
                margin-bottom: #{padding}px;
                border-top-left-radius: #{borderRad}px;
                border-top-right-radius: #{borderRad}px;
                background: #91850b;
                color: white;
                content: '';
                cursor: default;
            }
            div[#{ATTR_NAME}] > *:last-child{
                margin-bottom: 0;
            }
            div[#{ATTR_NAME}] > *:first-child{
                margin-top: 0;
            }
        """)
    init: (editor) ->
        lang = editor.lang.linkcard
        editor.addCommand 'linkcard', new (CKEDITOR.dialogCommand)('linkcardDialog')
        editor.ui.addButton 'Linkcard', {
            label: lang.dialogTitle
            command: 'linkcard'
            toolbar: 'insert'
        }
        if editor.contextMenu
            editor.addMenuGroup('linkcardGroup')
            editor.addMenuItem('linkcardItem', {
                label: lang.menuItemEdit
                icon: @path + 'icons/linkcard.png'
                command: 'linkcard'
                group: 'linkcardGroup'
            })
            editor.contextMenu.addListener (element) ->
                if getLinkCardElement(element)
                    return { linkcardItem: CKEDITOR.TRISTATE_OFF }
                return
        CKEDITOR.dialog.add('linkcardDialog', @path + 'dialogs/linkcard.js')
        # локализируем строку помощи
        CKEDITOR.addCss("""
            div[#{ATTR_NAME}]:before{
                content: '#{lang.dialogTitle} (#{lang.rmcHelpText})';
            }
        """)
        return


CKEDITOR.plugins.linkcard = {
    ATTR_NAME
    getLinkCardElement
    parseAttributes: (element)->
        element = getLinkCardElement(element)
        attrs = {}
        attrs[ATTR_NAME] = {}
        try
            attrs[ATTR_NAME] = JSON.parse(element.getAttribute(ATTR_NAME))
        catch e
        attrs
}
