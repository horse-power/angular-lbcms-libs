CKEDITOR.dialog.add 'linkcardDialog', (editor) ->
    plugin = CKEDITOR.plugins.linkcard
    lang = editor.lang.linkcard
    lbcmsLang = editor.lang.lbcmshelpers
    lbcmshelpers = CKEDITOR.plugins.lbcmshelpers(editor)

    iconPickerWidget = lbcmshelpers.configWidgets.getAngularHtmlWidget({
        propName: plugin.ATTR_NAME
        html: """
            <#{plugin.ATTR_NAME}-editor ng-model="ngModel"></#{plugin.ATTR_NAME}-editor>
        """
    }, (widget, value, filepath)->
        widget.data[plugin.ATTR_NAME] = JSON.stringify(value)
        widget.fire('data', widget.data)
        widget.setAttribute(plugin.ATTR_NAME, widget.data[plugin.ATTR_NAME])
        widget.setAttribute('type', value.type || '')
        return
    )

    {
        title: lang.dialogTitle
        minWidth: 400
        minHeight: 400
        contents: [
            {
                id: 'tab-basic'
                label: lbcmsLang.settings
                elements: [
                    iconPickerWidget
                ]
            }
        ]
        onShow: ->
            selection = editor.getSelection()
            element = selection.getStartElement()
            @insertMode = false
            if !plugin.getLinkCardElement(element)
                element = editor.document.createElement('div')
                element.setAttribute(plugin.ATTR_NAME, '')
                p = editor.document.createElement('p')
                p.$.innerHTML = "&nbsp;"
                element.append(p)
                @insertMode = true

            @element = plugin.getLinkCardElement(element)
            data = plugin.parseAttributes(@element)
            # хак для lbcmshelpers
            @$widget = {
                data: data
            }
            if !@insertMode
                @setupContent(@element)
            return
        onOk: ->
            dialog = this
            @commitContent(@element)
            if @insertMode
                editor.insertElement(@element)
                range = editor.createRange()
                range.setStart(@element, 0)
                range.setEnd(@element, 1)
                editor.getSelection().selectRanges([range])
            return

    }
