(function() {
  CKEDITOR.dialog.add('linkcardDialog', function(editor) {
    var iconPickerWidget, lang, lbcmsLang, lbcmshelpers, plugin;
    plugin = CKEDITOR.plugins.linkcard;
    lang = editor.lang.linkcard;
    lbcmsLang = editor.lang.lbcmshelpers;
    lbcmshelpers = CKEDITOR.plugins.lbcmshelpers(editor);
    iconPickerWidget = lbcmshelpers.configWidgets.getAngularHtmlWidget({
      propName: plugin.ATTR_NAME,
      html: "<" + plugin.ATTR_NAME + "-editor ng-model=\"ngModel\"></" + plugin.ATTR_NAME + "-editor>"
    }, function(widget, value, filepath) {
      widget.data[plugin.ATTR_NAME] = JSON.stringify(value);
      widget.fire('data', widget.data);
      widget.setAttribute(plugin.ATTR_NAME, widget.data[plugin.ATTR_NAME]);
      widget.setAttribute('type', value.type || '');
    });
    return {
      title: lang.dialogTitle,
      minWidth: 400,
      minHeight: 400,
      contents: [
        {
          id: 'tab-basic',
          label: lbcmsLang.settings,
          elements: [iconPickerWidget]
        }
      ],
      onShow: function() {
        var data, element, p, selection;
        selection = editor.getSelection();
        element = selection.getStartElement();
        this.insertMode = false;
        if (!plugin.getLinkCardElement(element)) {
          element = editor.document.createElement('div');
          element.setAttribute(plugin.ATTR_NAME, '');
          p = editor.document.createElement('p');
          p.$.innerHTML = "&nbsp;";
          element.append(p);
          this.insertMode = true;
        }
        this.element = plugin.getLinkCardElement(element);
        data = plugin.parseAttributes(this.element);
        this.$widget = {
          data: data
        };
        if (!this.insertMode) {
          this.setupContent(this.element);
        }
      },
      onOk: function() {
        var dialog, range;
        dialog = this;
        this.commitContent(this.element);
        if (this.insertMode) {
          editor.insertElement(this.element);
          range = editor.createRange();
          range.setStart(this.element, 0);
          range.setEnd(this.element, 1);
          editor.getSelection().selectRanges([range]);
        }
      }
    };
  });

}).call(this);
