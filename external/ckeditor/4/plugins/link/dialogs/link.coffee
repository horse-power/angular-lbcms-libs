###*
# @license Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
# For licensing, see LICENSE.md or http://ckeditor.com/license
###

CKEDITOR.dialog.add 'link', (editor) ->
    lbcmshelpers = CKEDITOR.plugins.lbcmshelpers(editor)
    plugin = CKEDITOR.plugins.link
    initialLinkText = undefined
    # Handles the event when the "Target" selection box is changed.

    targetChanged = ->
        dialog = @getDialog()
        popupFeatures = dialog.getContentElement('advanced', 'popupFeatures')
        targetName = dialog.getContentElement('advanced', 'linkTargetName')
        value = @getValue()
        if !popupFeatures or !targetName
            return
        popupFeatures = popupFeatures.getElement()
        popupFeatures.hide()
        targetName.setValue ''
        switch value
            when 'frame'
                targetName.setLabel editor.lang.link.targetFrameName
                targetName.getElement().show()
            when 'popup'
                popupFeatures.show()
                targetName.setLabel editor.lang.link.targetPopupName
                targetName.getElement().show()
            else
                targetName.setValue value
                targetName.getElement().hide()
                break
        return

    # Handles the event when the "Type" selection box is changed.
    articles_loaded = false
    linkTypeChanged = ->
        dialog = @getDialog()
        partIds = [
            'urlOptions'
            'anchorOptions'
            'mediaOptions'
            'uiSrefArticleOptions'
            'telephoneOptions'
            'emailOptions'
            'actionOptions'
        ]
        typeValue = @getValue()
        if typeValue == 'uiSrefArticle' and !articles_loaded
            # загружаем статьи, если это не было сделано ранее
            lbcmshelpers.$models.CmsArticle.loadItems({filter:{fields:{id:true, title:true, isPublished:true}}})
            articles_loaded = true
        if typeValue == 'uiSrefArticle'
            dialog.hidePage('advanced')
        else
            dialog.showPage('advanced')

        i = 0
        while i < partIds.length
            element = dialog.getContentElement('info', partIds[i])
            if !element
                i++
                continue
            element = element.getElement().getParent().getParent()
            if partIds[i] == typeValue + 'Options'
                element.show()
            else
                element.hide()
            i++
        dialog.layout()
        return

    setupParams = (page, data) ->
        if data[page]
            @setValue data[page][@id] or ''
        return

    setupPopupParams = (data) ->
        setupParams.call this, 'target', data

    setupAdvParams = (data) ->
        setupParams.call this, 'advanced', data

    commitParams = (page, data) ->
        if !data[page]
            data[page] = {}
        data[page][@id] = @getValue() or ''
        return

    commitPopupParams = (data) ->
        commitParams.call this, 'target', data

    commitAdvParams = (data) ->
        commitParams.call this, 'advanced', data

    commonLang = editor.lang.common
    linkLang = editor.lang.link
    anchors = undefined
    mediaFileIdPickerWidget = lbcmshelpers.configWidgets.getFilePickerForFileId({
        accept: '*'
        'allow-preview': true
        propName: 'media.fileId'
    }, (widget, value, filepath)->
        # устанавливаем полную ссылку на файл в href
        # это сделано для fallback-a, если html будет отображаться там, где нет js
        widget.media.url = filepath
        return
    )

    uiSrefArticlePickerWidget = lbcmshelpers.configWidgets.getAngularHtmlWidget({
        propName: 'uiSrefArticle.id'
        html: """
        <div style='margin-bottom:2px;'>#{linkLang.pickArticle}</div>
        <select class='form-control input-sm'
            ng-model="ngModel"
            ng-options="a_item.id as (a_item.title | l10n) for a_item in options.$models.CmsArticle.items | filter: {isPublished:true}">
        </select>
        """
        $models: lbcmshelpers.$models
    })

    telephonePickerWidget = lbcmshelpers.configWidgets.getAngularHtmlWidget({
        propName: 'telephone.telephone'
        html: """
        <div style='margin-bottom:2px;'>#{linkLang.telephoneNumber}</div>
        <input class='form-control input-sm' ng-model="ngModel" />
        """
    })
    emailPickerWidget = lbcmshelpers.configWidgets.getAngularHtmlWidget({
        propName: 'email.email'
        html: """
        <div style='margin-bottom:2px;'>#{linkLang.emailValue}</div>
        <input class='form-control input-sm' ng-model="ngModel" />
        """
    })


    siteActions = []
    for k,v of lbcmshelpers.$cms.siteActions
        title = k
        if v.description
            title = "#{k}: #{v.description}"
        siteActions.push([
            title
            k
        ])
    linkTypes = [
        [
            linkLang.toUrl
            'url'
        ],[
            linkLang.toMedia
            'media'
        ],[
            linkLang.toArticle
            'uiSrefArticle'
        ],[
            linkLang.toAnchor
            'anchor'
        ],[
            linkLang.toTelephone
            'telephone'
        ],[
            linkLang.toEmail
            'email'
        ]
    ]
    if siteActions.length
        linkTypes.push([
            linkLang.toSiteAction
            'action'
        ])


    {
        title: linkLang.title
        minWidth: if (CKEDITOR.skinName or editor.config.skin) == 'moono-lisa' then 450 else 350
        contents: [
            {
                id: 'info'
                label: linkLang.info
                title: linkLang.info
                elements: [
                    {
                        type: 'text'
                        id: 'linkDisplayText'
                        label: linkLang.displayText
                        setup: ->
                            @enable()
                            @setValue editor.getSelection().getSelectedText()
                            # Keep inner text so that it can be compared in commit function. By obtaining value from getData()
                            # we get value stripped from new line chars which is important when comparing the value later on.
                            initialLinkText = @getValue()
                            return
                        commit: (data) ->
                            data.linkText = if @isEnabled() then @getValue() else ''
                            return

                    }
                    {
                        id: 'linkType'
                        type: 'select'
                        label: linkLang.type
                        'default': 'url'
                        items: linkTypes
                        onChange: linkTypeChanged
                        setup: (data) ->
                            @setValue data.type or 'url'
                            return
                        commit: (data) ->
                            data.type = @getValue()
                            return
                    }

                    {
                        type: 'vbox'
                        id: 'urlOptions'
                        children: [{
                            type: 'text'
                            id: 'url'
                            label: commonLang.url
                            required: true
                            validate: ->
                                dialog = @getDialog()
                                if dialog.getContentElement('info', 'linkType') and dialog.getValueOf('info', 'linkType') != 'url'
                                    return true
                                if !editor.config.linkJavaScriptLinksAllowed and /javascript\:/.test(@getValue())
                                    alert commonLang.invalidValue
                                    # jshint ignore:line
                                    return false
                                if @getDialog().fakeObj
                                    return true
                                func = CKEDITOR.dialog.validate.notEmpty(linkLang.noUrl)
                                func.apply(this)
                            setup: (data) ->
                                if data.url
                                    @setValue data.url.url
                                return
                            commit: (data) ->
                                if !data.url
                                    data.url = {}
                                data.url.url = @getValue()
                                return
                        }]
                        setup: ->
                            if !@getDialog().getContentElement('info', 'linkType')
                                @getElement().show()
                            return
                    }


                    {
                        type: 'vbox'
                        id: 'mediaOptions'
                        children: [
                            mediaFileIdPickerWidget
                        ]
                        setup: ->
                            if !@getDialog().getContentElement('info', 'linkType')
                                @getElement().hide()
                            return
                    }


                    {
                        type: 'vbox'
                        id: 'uiSrefArticleOptions'
                        children: [
                            uiSrefArticlePickerWidget
                        ]
                        setup: ->
                            if !@getDialog().getContentElement('info', 'linkType')
                                @getElement().hide()
                            return
                    }

                    {
                        type: 'vbox'
                        id: 'telephoneOptions'
                        children: [
                            telephonePickerWidget
                        ]
                        setup: ->
                            if !@getDialog().getContentElement('info', 'linkType')
                                @getElement().show()
                            return
                    }

                    {
                        type: 'vbox'
                        id: 'emailOptions'
                        children: [
                            emailPickerWidget
                        ]
                        setup: ->
                            if !@getDialog().getContentElement('info', 'linkType')
                                @getElement().show()
                            return
                    }
                    {
                        id: 'actionOptions'
                        type: 'select'
                        label: linkLang.toSiteAction
                        items: siteActions
                        setup: (data)->
                            if !@getDialog().getContentElement('info', 'linkType')
                                @getElement().hide()
                            if data.action and data.action.id
                                @setValue(data.action.id)
                            return
                        commit: (data) ->
                            if !data.action
                                data.action = {}
                            data.action.id = @getValue()
                            return
                    }

                    {
                        type: 'vbox'
                        id: 'anchorOptions'
                        children: [
                            {
                                type: 'fieldset'
                                id: 'selectAnchorText'
                                label: linkLang.selectAnchor
                                setup: ->
                                    anchors = plugin.getEditorAnchors(editor)
                                    @getElement()[if anchors and anchors.length then 'show' else 'hide']()
                                    return
                                children: [ {
                                    type: 'hbox'
                                    id: 'selectAnchor'
                                    children: [
                                        {
                                            type: 'select'
                                            id: 'anchorName'
                                            'default': ''
                                            label: linkLang.anchorName
                                            style: 'width: 100%;'
                                            items: [ [ '' ] ]
                                            setup: (data) ->
                                                @clear()
                                                @add ''
                                                if anchors
                                                    i = 0
                                                    while i < anchors.length
                                                        if anchors[i].name
                                                            @add anchors[i].name
                                                        i++
                                                if data.anchor
                                                    @setValue data.anchor.name
                                                return
                                            commit: (data) ->
                                                if !data.anchor
                                                    data.anchor = {}
                                                data.anchor.name = @getValue()
                                                return
                                        }
                                        {
                                            type: 'select'
                                            id: 'anchorId'
                                            'default': ''
                                            label: linkLang.anchorId
                                            style: 'width: 100%;'
                                            items: [ [ '' ] ]
                                            setup: (data) ->
                                                @clear()
                                                @add ''
                                                if anchors
                                                    i = 0
                                                    while i < anchors.length
                                                        if anchors[i].id
                                                            @add anchors[i].id
                                                        i++
                                                if data.anchor
                                                    @setValue data.anchor.id
                                                return
                                            commit: (data) ->
                                                if !data.anchor
                                                    data.anchor = {}
                                                data.anchor.id = @getValue()
                                                return
                                        }
                                    ]
                                    setup: ->
                                        @getElement()[if anchors and anchors.length then 'show' else 'hide']()
                                        return
                                } ]
                            }
                            {
                                type: 'html'
                                id: 'noAnchors'
                                style: 'text-align: center;'
                                html: '<div role="note" tabIndex="-1">' + CKEDITOR.tools.htmlEncode(linkLang.noAnchors) + '</div>'
                                focus: true
                                setup: ->
                                    @getElement()[if anchors and anchors.length then 'hide' else 'show']()
                                    return
                            }
                        ]
                        setup: ->
                            if !@getDialog().getContentElement('info', 'linkType')
                                @getElement().hide()
                            return
                    }


                ]
            }
            {
                id: 'advanced'
                requiredContent: 'a[target]'
                label: linkLang.advanced
                title: linkLang.advanced
                elements: [
                    # {
                    #    type: 'checkbox'
                    #    id: 'download'
                    #    requiredContent: 'a[download]'
                    #    label: linkLang.download
                    #    setup: (data) ->
                    #        if data.download != undefined
                    #            @setValue 'checked', 'checked'
                    #        return
                    #    commit: (data) ->
                    #        if @getValue()
                    #            data.download = @getValue()
                    #        return
                    # }
                    {
                        type: 'select'
                        id: 'linkTargetType'
                        label: commonLang.target
                        'default': '_blank'
                        style: 'width : 100%;'
                        'items': [
                            [
                                commonLang.notSet
                                'notSet'
                            ]
                            [
                                linkLang.targetFrame
                                'frame'
                            ]
                            [
                                linkLang.targetPopup
                                'popup'
                            ]
                            [
                                commonLang.targetNew
                                '_blank'
                            ]
                            [
                                commonLang.targetTop
                                '_top'
                            ]
                            [
                                commonLang.targetSelf
                                '_self'
                            ]
                            [
                                commonLang.targetParent
                                '_parent'
                            ]
                        ]
                        onChange: targetChanged
                        setup: (data) ->
                            if data.target
                                @setValue data.target.type or 'notSet'
                            targetChanged.call this
                            return
                        commit: (data) ->
                            if !data.target
                                data.target = {}
                            data.target.type = @getValue()
                            return
                    }
                    {
                        type: 'text'
                        id: 'linkTargetName'
                        label: linkLang.targetFrameName
                        'default': ''
                        setup: (data) ->
                            if data.target
                                @setValue data.target.name
                            return
                        commit: (data) ->
                            if !data.target
                                data.target = {}
                            data.target.name = @getValue().replace(/([^\x00-\x7F]|\s)/gi, '')
                            return

                    }

                    {
                        type: 'fieldset'
                        id: 'popupFeatures'
                        label: linkLang.popupFeatures
                        children: [
                            {
                                type: 'hbox'
                                children: [
                                    {
                                        type: 'checkbox'
                                        id: 'resizable'
                                        label: linkLang.popupResizable
                                        setup: setupPopupParams
                                        commit: commitPopupParams
                                    }
                                    {
                                        type: 'checkbox'
                                        id: 'status'
                                        label: linkLang.popupStatusBar
                                        setup: setupPopupParams
                                        commit: commitPopupParams
                                    }
                                ]
                            }
                            {
                                type: 'hbox'
                                children: [
                                    {
                                        type: 'checkbox'
                                        id: 'location'
                                        label: linkLang.popupLocationBar
                                        setup: setupPopupParams
                                        commit: commitPopupParams
                                    }
                                    {
                                        type: 'checkbox'
                                        id: 'toolbar'
                                        label: linkLang.popupToolbar
                                        setup: setupPopupParams
                                        commit: commitPopupParams
                                    }
                                ]
                            }
                            {
                                type: 'hbox'
                                children: [
                                    {
                                        type: 'checkbox'
                                        id: 'menubar'
                                        label: linkLang.popupMenuBar
                                        setup: setupPopupParams
                                        commit: commitPopupParams
                                    }
                                    {
                                        type: 'checkbox'
                                        id: 'fullscreen'
                                        label: linkLang.popupFullScreen
                                        setup: setupPopupParams
                                        commit: commitPopupParams
                                    }
                                ]
                            }
                            {
                                type: 'hbox'
                                children: [
                                    {
                                        type: 'checkbox'
                                        id: 'scrollbars'
                                        label: linkLang.popupScrollBars
                                        setup: setupPopupParams
                                        commit: commitPopupParams
                                    }
                                    {
                                        type: 'checkbox'
                                        id: 'dependent'
                                        label: linkLang.popupDependent
                                        setup: setupPopupParams
                                        commit: commitPopupParams
                                    }
                                ]
                            }
                            {
                                type: 'hbox'
                                children: [
                                    {
                                        type: 'text'
                                        widths: [
                                            '50%'
                                            '50%'
                                        ]
                                        labelLayout: 'horizontal'
                                        label: commonLang.width
                                        id: 'width'
                                        setup: setupPopupParams
                                        commit: commitPopupParams
                                    }
                                    {
                                        type: 'text'
                                        labelLayout: 'horizontal'
                                        widths: [
                                            '50%'
                                            '50%'
                                        ]
                                        label: linkLang.popupLeft
                                        id: 'left'
                                        setup: setupPopupParams
                                        commit: commitPopupParams
                                    }
                                ]
                            }
                            {
                                type: 'hbox'
                                children: [
                                    {
                                        type: 'text'
                                        labelLayout: 'horizontal'
                                        widths: [
                                            '50%'
                                            '50%'
                                        ]
                                        label: commonLang.height
                                        id: 'height'
                                        setup: setupPopupParams
                                        commit: commitPopupParams
                                    }
                                    {
                                        type: 'text'
                                        labelLayout: 'horizontal'
                                        label: linkLang.popupTop
                                        widths: [
                                            '50%'
                                            '50%'
                                        ]
                                        id: 'top'
                                        setup: setupPopupParams
                                        commit: commitPopupParams
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ]
        onShow: ->
            `var editor`
            editor = @getParentEditor()
            selection = editor.getSelection()
            selectedElement = selection.getSelectedElement()
            displayTextField = @getContentElement('info', 'linkDisplayText').getElement().getParent().getParent()
            element = plugin.getSelectedLink(editor)
            # Fill in all the relevant fields if there's already one link selected.
            if element
                # Don't change selection if some element is already selected.
                # For example - don't destroy fake selection.
                if !selectedElement
                    selection.selectElement(element)
                    selectedElement = element
            # Here we'll decide whether or not we want to show Display Text field.
            if plugin.showDisplayTextForElement(selectedElement, editor)
                displayTextField.show()
            else
                displayTextField.hide()
            data = plugin.parseLinkAttributes(editor, element)
            # Record down the selected element in the dialog.
            @_.selectedElement = element
            # хак для lbcmshelpers
            @$widget = {
                data: data
            }
            @setupContent(data)
            if data.type == 'uiSrefArticle'
                @hidePage('advanced')
            return
        onOk: ->
            data = {}
            # Collect data from fields.
            @commitContent data
            selection = editor.getSelection()
            attributes = plugin.getLinkAttributes(editor, data)
            bm = undefined
            nestedLinks = undefined
            if !@_.selectedElement
                range = selection.getRanges()[0]
                text = undefined
                # Use link URL as text with a collapsed cursor.
                if range.collapsed
                    # Short mailto link text view (#5736).
                    text = new (CKEDITOR.dom.text)(data.linkText or attributes.set['data-cke-saved-href'], editor.document)
                    range.insertNode text
                    range.selectNodeContents text
                else if initialLinkText != data.linkText
                    text = new (CKEDITOR.dom.text)(data.linkText, editor.document)
                    # Shrink range to preserve block element.
                    range.shrink CKEDITOR.SHRINK_TEXT
                    # Use extractHtmlFromRange to remove markup within the selection. Also this method is a little
                    # smarter than range#deleteContents as it plays better e.g. with table cells.
                    editor.editable().extractHtmlFromRange range
                    range.insertNode text
                # Editable links nested within current range should be removed, so that the link is applied to whole selection.
                nestedLinks = range._find('a')
                i = 0
                while i < nestedLinks.length
                    nestedLinks[i].remove true
                    i++
                # Apply style.
                style = new (CKEDITOR.style)(
                    element: 'a'
                    attributes: attributes.set)
                style.type = CKEDITOR.STYLE_INLINE
                # need to override... dunno why.
                style.applyToRange range, editor
                range.select()
            else
                # We're only editing an existing link, so just overwrite the attributes.
                element = @_.selectedElement
                href = element.data('cke-saved-href')
                textView = element.getHtml()
                newText = undefined
                element.setAttributes attributes.set
                element.removeAttributes attributes.removed
                if data.linkText and initialLinkText != data.linkText
                    # Display text has been changed.
                    newText = data.linkText
                else if href == textView and textView.indexOf('@') != -1
                    # Update text view when user changes protocol (#4612).
                    # Short mailto link text view (#5736).
                    newText = attributes.set['data-cke-saved-href']
                if newText
                    element.setText newText
                    # We changed the content, so need to select it again.
                    selection.selectElement element
                delete @_.selectedElement
            return
        onLoad: ->
            if !editor.config.linkShowAdvancedTab
                @hidePage('advanced')
            #Hide Advanded tab.
            return
        onFocus: ->
            linkType = @getContentElement('info', 'linkType')
            urlField = undefined
            if linkType and linkType.getValue() == 'url'
                urlField = @getContentElement('info', 'url')
                urlField.select()
            return

    }
# jscs:disable maximumLineLength
