# out: false
{
    type: 'vbox'
    id: 'emailOptions'
    padding: 1
    children: [
        {
            type: 'text'
            id: 'emailAddress'
            label: linkLang.emailAddress
            required: true
            validate: ->
                dialog = @getDialog()
                if !dialog.getContentElement('info', 'linkType') or dialog.getValueOf('info', 'linkType') != 'email'
                    return true
                func = CKEDITOR.dialog.validate.notEmpty(linkLang.noEmail)
                func.apply this
            setup: (data) ->
                if data.email
                    @setValue data.email.address
                linkType = @getDialog().getContentElement('info', 'linkType')
                if linkType and linkType.getValue() == 'email'
                    @select()
                return
            commit: (data) ->
                if !data.email
                    data.email = {}
                data.email.address = @getValue()
                return

        }
        {
            type: 'text'
            id: 'emailSubject'
            label: linkLang.emailSubject
            setup: (data) ->
                if data.email
                    @setValue data.email.subject
                return
            commit: (data) ->
                if !data.email
                    data.email = {}
                data.email.subject = @getValue()
                return

        }
        {
            type: 'textarea'
            id: 'emailBody'
            label: linkLang.emailBody
            rows: 3
            'default': ''
            setup: (data) ->
                if data.email
                    @setValue data.email.body
                return
            commit: (data) ->
                if !data.email
                    data.email = {}
                data.email.body = @getValue()
                return

        }
    ]
    setup: ->
        if !@getDialog().getContentElement('info', 'linkType')
            @getElement().hide()
        return

}
