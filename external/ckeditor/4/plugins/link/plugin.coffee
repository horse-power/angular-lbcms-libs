###*
# @license Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
# For licensing, see LICENSE.md or http://ckeditor.com/license
###

unescapeSingleQuote = (str) ->
    str.replace /\\'/g, '\''

escapeSingleQuote = (str) ->
    str.replace /'/g, '\\$&'



CKEDITOR.plugins.add 'link',
    requires: 'dialog,fakeobjects'
    lang: 'af,ar,az,bg,bn,bs,ca,cs,cy,da,de,de-ch,el,en,en-au,en-ca,en-gb,eo,es,et,eu,fa,fi,fo,fr,fr-ca,gl,gu,he,hi,hr,hu,id,is,it,ja,ka,km,ko,ku,lt,lv,mk,mn,ms,nb,nl,no,oc,pl,pt,pt-br,ro,ru,si,sk,sl,sq,sr,sr-latn,sv,th,tr,tt,ug,uk,vi,zh,zh-cn'
    icons: 'anchor,anchor-rtl,link,unlink'
    hidpi: true
    onLoad: ->
        # Add the CSS styles for anchor placeholders.
        iconPath = CKEDITOR.getUrl(@path + 'images' + (if CKEDITOR.env.hidpi then '/hidpi' else '') + '/anchor.png')
        baseStyle = 'background:url(' + iconPath + ') no-repeat %1 center;border:1px dotted #00f;background-size:16px;'
        template = '.%2 a.cke_anchor,' + '.%2 a.cke_anchor_empty' + ',.cke_editable.%2 a[name]' + ',.cke_editable.%2 a[data-cke-saved-name]' + '{' + baseStyle + 'padding-%1:18px;' + 'cursor:auto;' + '}' + '.%2 img.cke_anchor' + '{' + baseStyle + 'width:16px;' + 'min-height:15px;' + 'height:1.15em;' + 'vertical-align:text-bottom;' + '}'
        css = template.replace( /%1/g, 'left' ).replace( /%2/g, 'cke_contents_ltr')
        CKEDITOR.addCss(css)
        return

    init: (editor) ->
        allowed = 'a[!href,uic-rel,uic-href,file-id,uic-sref-for-ckeditor,du-smooth-scroll,ng-click]'
        required = 'a[href]'
        if CKEDITOR.dialog.isTabEnabled(editor, 'link', 'advanced')
            allowed = allowed.replace(']', ',accesskey,charset,id,lang,name,rel,tabindex,title,type,download]{*}(*)')
        if CKEDITOR.dialog.isTabEnabled(editor, 'link', 'target')
            allowed = allowed.replace(']', ',target,onclick]')
        # Add the link and unlink buttons.
        editor.addCommand 'link', new (CKEDITOR.dialogCommand)('link',
            allowedContent: allowed
            requiredContent: required)
        editor.addCommand 'anchor', new (CKEDITOR.dialogCommand)('anchor',
            allowedContent: 'a[!name,id]'
            requiredContent: 'a[name]')
        editor.addCommand 'unlink', new (CKEDITOR.unlinkCommand)
        editor.addCommand 'removeAnchor', new (CKEDITOR.removeAnchorCommand)
        editor.setKeystroke CKEDITOR.CTRL + 76, 'link'
        if editor.ui.addButton
            editor.ui.addButton 'Link',
                label: editor.lang.link.toolbar
                command: 'link'
                toolbar: 'links,10'
            editor.ui.addButton 'Unlink',
                label: editor.lang.link.unlink
                command: 'unlink'
                toolbar: 'links,20'
            editor.ui.addButton 'Anchor',
                label: editor.lang.link.anchor.toolbar
                command: 'anchor'
                toolbar: 'links,30'
        CKEDITOR.dialog.add 'link', @path + 'dialogs/link.js'
        CKEDITOR.dialog.add 'anchor', @path + 'dialogs/anchor.js'
        editor.on 'doubleclick', ((evt) ->
            # If the link has descendants and the last part of it is also a part of a word partially
            # unlinked, clicked element may be a descendant of the link, not the link itself. (#11956)
            element = CKEDITOR.plugins.link.getSelectedLink(editor) or evt.data.element.getAscendant('a', 1)
            if element and !element.isReadOnly()
                if element.is('a')
                    evt.data.dialog = if element.getAttribute('name') and (!element.getAttribute('href') or !element.getChildCount()) then 'anchor' else 'link'
                    # Pass the link to be selected along with event data.
                    evt.data.link = element
                else if CKEDITOR.plugins.link.tryRestoreFakeAnchor(editor, element)
                    evt.data.dialog = 'anchor'
            return
        ), null, null, 0
        # If event was cancelled, link passed in event data will not be selected.
        editor.on 'doubleclick', ((evt) ->
            # Make sure both links and anchors are selected (#11822).
            if evt.data.dialog in {
                    link: 1
                    anchor: 1} and evt.data.link
                editor.getSelection().selectElement evt.data.link
            return
        ), null, null, 20
        # If the "menu" plugin is loaded, register the menu items.
        if editor.addMenuItems
            editor.addMenuItems({
                anchor:
                    label: editor.lang.link.anchor.menu
                    command: 'anchor'
                    group: 'anchor'
                    order: 1
                removeAnchor:
                    label: editor.lang.link.anchor.remove
                    command: 'removeAnchor'
                    group: 'anchor'
                    order: 5
                link:
                    label: editor.lang.link.menu
                    command: 'link'
                    group: 'link'
                    order: 1
                unlink:
                    label: editor.lang.link.unlink
                    command: 'unlink'
                    group: 'link'
                    order: 5
            })
        # If the "contextmenu" plugin is loaded, register the listeners.
        if editor.contextMenu
            editor.contextMenu.addListener (element) ->
                if !element or element.isReadOnly()
                    return null
                anchor = CKEDITOR.plugins.link.tryRestoreFakeAnchor(editor, element)
                if !anchor and !(anchor = CKEDITOR.plugins.link.getSelectedLink(editor))
                    return null
                menu = {}
                if anchor.getAttribute('href') and anchor.getChildCount()
                    menu =
                        link: CKEDITOR.TRISTATE_OFF
                        unlink: CKEDITOR.TRISTATE_OFF
                if anchor and anchor.hasAttribute('name')
                    menu.anchor = menu.removeAnchor = CKEDITOR.TRISTATE_OFF
                menu
        return
    afterInit: (editor) ->
        # Empty anchors upcasting to fake objects.
        editor.dataProcessor.dataFilter.addRules elements: a: (element) ->
            if !element.attributes.name
                return null
            if !element.children.length
                return editor.createFakeParserElement(element, 'cke_anchor', 'anchor')
            null
        pathFilters = editor._.elementsPath and editor._.elementsPath.filters
        if pathFilters
            pathFilters.push (element, name) ->
                if name == 'a'
                    if CKEDITOR.plugins.link.tryRestoreFakeAnchor(editor, element) or element.getAttribute('name') and (!element.getAttribute('href') or !element.getChildCount())
                        return 'anchor'
                return
        return
# Loads the parameters in a selected link to the link dialog fields.
anchorRegex = /^#(.*)$/
urlRegex = /^((?:http|https|ftp|news):\/\/)?(.*)$/
selectableTargets = /^(_(?:self|top|parent|blank))$/
popupRegex = /\s*window.open\(\s*this\.href\s*,\s*(?:'([^']*)'|null)\s*,\s*'([^']*)'\s*\)\s*;\s*return\s*false;*\s*/
popupFeaturesRegex = /(?:^|,)([^=]+)=(\d+|yes|no)/gi
advAttrNames = {
    id: 'advId'
    accessKey: 'advAccessKey'
    name: 'advName'
    lang: 'advLangCode'
    tabindex: 'advTabIndex'
    title: 'advTitle'
    type: 'advContentType'
    'class': 'advCSSClasses'
    charset: 'advCharset'
    style: 'advStyles'
    rel: 'advRel'
}

###*
# Set of Link plugin helpers.
#
# @class
# @singleton
###

CKEDITOR.plugins.link =
    getSelectedLink: (editor) ->
        selection = editor.getSelection()
        selectedElement = selection.getSelectedElement()
        if selectedElement and selectedElement.is('a')
            return selectedElement
        range = selection.getRanges()[0]
        if range
            range.shrink CKEDITOR.SHRINK_TEXT
            return editor.elementPath(range.getCommonAncestor()).contains('a', 1)
        null
    getEditorAnchors: (editor) ->
        editable = editor.editable()
        scope = if editable.isInline() and !editor.plugins.divarea then editor.document else editable
        links = scope.getElementsByTag('a')
        imgs = scope.getElementsByTag('img')
        anchors = []
        i = 0
        item = undefined
        # Retrieve all anchors within the scope.
        while item = links.getItem(i++)
            if item.data('cke-saved-name') or item.hasAttribute('name')
                anchors.push
                    name: item.data('cke-saved-name') or item.getAttribute('name')
                    id: item.getAttribute('id')
        # Retrieve all "fake anchors" within the scope.
        i = 0
        while item = imgs.getItem(i++)
            if item = @tryRestoreFakeAnchor(editor, item)
                anchors.push
                    name: item.getAttribute('name')
                    id: item.getAttribute('id')
        anchors
    fakeAnchor: true
    tryRestoreFakeAnchor: (editor, element) ->
        if element and element.data('cke-real-element-type') and element.data('cke-real-element-type') == 'anchor'
            link = editor.restoreRealElement(element)
            if link.data('cke-saved-name')
                return link
        return
    parseLinkAttributes: (editor, element) ->
        href = element and (element.data('cke-saved-href') or element.getAttribute('href')) or ''
        javascriptMatch = undefined
        anchorMatch = undefined
        urlMatch = undefined
        retval = {}
        if href.indexOf('tel:') == 0
            retval.type = 'telephone'
            retval.telephone = {telephone: href.replace('tel:', '')}
        else if href.indexOf('mailto:') == 0
            retval.type = 'email'
            retval.email = {email: href.replace('mailto:', '')}
        else if anchorMatch = href.match(anchorRegex)
            retval.type = 'anchor'
            retval.anchor = {}
            retval.anchor.name = retval.anchor.id = anchorMatch[1]
        else if element and element.getAttribute('ng-click')
            ngClick = element.getAttribute('ng-click')
            ngClick = ngClick.split('runSiteAction(')[1]
            retval.type = 'action'
            retval.action = {
                id: ngClick.replace(')','').replace(/"/g, '')
            }
        else if element and element.getAttribute('uic-href') and element.getAttribute('file-id')
            retval.type = 'media'
            retval.media = {
                fileId: element.getAttribute('file-id')
            }
        else if element and element.getAttribute('uic-sref-for-ckeditor')
            retval.type = 'uiSrefArticle'
            id = element.getAttribute('uic-sref-for-ckeditor').split('{')[1].split('}')[0]
            id = id.replace('id', '').replaceAll(' ','')
                   .replaceAll("'",'').replaceAll('"', '')
                   .replaceAll(':','')
            if CMS_VERSION >= 31
                id = parseInt(id)
            retval.uiSrefArticle = {
                id: id
            }
        else if href
            retval.type = 'url'
            retval.url = {}
            retval.url.url = href
        # Load target and popup settings.
        if element
            target = element.getAttribute('target')
            # IE BUG: target attribute is an empty string instead of null in IE if it's not set.
            if !target
                onclick = element.data('cke-pa-onclick') or element.getAttribute('onclick')
                onclickMatch = onclick and onclick.match(popupRegex)
                if onclickMatch
                    retval.target =
                        type: 'popup'
                        name: onclickMatch[1]
                    featureMatch = undefined
                    while featureMatch = popupFeaturesRegex.exec(onclickMatch[2])
                        # Some values should remain numbers (#7300)
                        if (featureMatch[2] == 'yes' or featureMatch[2] == '1') and !(featureMatch[1] in
                                height: 1
                                width: 1
                                top: 1
                                left: 1)
                            retval.target[featureMatch[1]] = true
                        else if isFinite(featureMatch[2])
                            retval.target[featureMatch[1]] = featureMatch[2]
                else
                    retval.target = {
                        type: '_blank'
                        name: '_blank'
                    }
            else
                retval.target = {
                    type: if target.match(selectableTargets) then target else 'frame'
                    name: target
                }
            download = element.getAttribute('download')
            if download != null
                retval.download = true
            advanced = {}
            for a of advAttrNames
                val = element.getAttribute(a)
                if val
                    advanced[advAttrNames[a]] = val
            advName = element.data('cke-saved-name') or advanced.advName
            if advName
                advanced.advName = advName
            if !CKEDITOR.tools.isEmpty(advanced)
                retval.advanced = advanced
        retval
    getLinkAttributes: (editor, data) ->
        set = {}
        set['uic-rel'] = ''
        # Compose the URL.
        switch data.type
            when 'telephone'
                set['data-cke-saved-href'] = "tel:#{data.telephone.telephone}"
            when 'email'
                set['data-cke-saved-href'] = "mailto:#{data.email.email}"
            when 'url'
                if !data.url
                    set['data-cke-saved-href'] = ''
                else
                    url = data.url.url
                    if !data.url.url.match(urlRegex) and url.indexOf('/')==-1
                        url = "http://" + url
                    set['data-cke-saved-href'] = url
            when 'action'
                set['ng-click'] = """$cms.collapseNavbar();$cms.runSiteAction("#{data.action.id}")"""
            when 'media'
                set['uic-href'] = "item.files"
                set['file-id'] = data.media.fileId
                set['href'] = data.media.url
            when 'uiSrefArticle'
                set['uic-sref-for-ckeditor'] = """app.articles.details({id: #{data.uiSrefArticle.id}})"""
                set['href'] = ''
            when 'anchor'
                name = data.anchor and data.anchor.name
                id = data.anchor and data.anchor.id
                set['data-cke-saved-href'] = '#' + (name or id or '')
                set['du-smooth-scroll'] = ''

        # Popups and target.
        if data.target
            if data.target.type == 'popup'
                onclickList = [
                    'window.open(this.href, \''
                    data.target.name or ''
                    '\', \''
                ]
                featureList = [
                    'resizable'
                    'status'
                    'location'
                    'toolbar'
                    'menubar'
                    'fullscreen'
                    'scrollbars'
                    'dependent'
                ]
                featureLength = featureList.length

                addFeature = (featureName) ->
                    if data.target[featureName]
                        featureList.push featureName + '=' + data.target[featureName]
                    return

                i = 0
                while i < featureLength
                    featureList[i] = featureList[i] + (if data.target[featureList[i]] then '=yes' else '=no')
                    i++
                addFeature 'width'
                addFeature 'left'
                addFeature 'height'
                addFeature 'top'
                onclickList.push featureList.join(','), '\'); return false;'
                set['data-cke-pa-onclick'] = onclickList.join('')
            else if data.target.type != 'notSet' and data.target.name
                set.target = data.target.name
        # Force download attribute.
        if data.download
            set.download = ''
        # Advanced attributes.
        if data.advanced
            for a of advAttrNames
                val = data.advanced[advAttrNames[a]]
                if val
                    set[a] = val
            if set.name
                set['data-cke-saved-name'] = set.name
        # Browser need the "href" fro copy/paste link to work. (#6641)
        if set['data-cke-saved-href']
            set.href = set['data-cke-saved-href']
        removed = {
            target: 1
            onclick: 1
            'data-cke-pa-onclick': 1
            'data-cke-saved-name': 1
            'download': 1
        }
        if data.advanced
            CKEDITOR.tools.extend removed, advAttrNames
        # Remove all attributes which are not currently set.
        for s of set
            delete removed[s]
        if data.type != 'anchor'
            removed['du-smooth-scroll'] = 1
        if data.type != 'media'
            removed['uic-href'] = 1
            removed['file-id'] = 1
        if data.type not in  ['url', 'telephone', 'email']
            removed['data-cke-saved-href'] = 1
            removed['href'] = 1

        if data.type == 'action'
            removed['uic-href'] = 1
            removed['file-id'] = 1
            removed['href'] = 1
            removed['onclick'] = 1
            removed['target'] = 1
            removed['data-cke-saved-href'] = 1
        else
            removed['ng-click'] = 1
        if data.type == 'uiSrefArticle'
            removed.href = 1
            removed.onclick = 1
            removed.target = 1
            removed['data-cke-saved-href'] = 1
        else
            removed['uic-sref-for-ckeditor'] = 1
        {
            set: set
            removed: CKEDITOR.tools.objectKeys(removed)
        }
    showDisplayTextForElement: (element, editor) ->
        undesiredElements =
            img: 1
            table: 1
            tbody: 1
            thead: 1
            tfoot: 1
            input: 1
            select: 1
            textarea: 1
        # Widget duck typing, we don't want to show display text for widgets.
        if editor.widgets and editor.widgets.focused
            return false
        !element or !element.getName or !element.is(undesiredElements)
# TODO Much probably there's no need to expose these as public objects.

CKEDITOR.unlinkCommand = ->

CKEDITOR.unlinkCommand.prototype =
    exec: (editor) ->
        # IE/Edge removes link from selection while executing "unlink" command when cursor
        # is right before/after link's text. Therefore whole link must be selected and the
        # position of cursor must be restored to its initial state after unlinking. (#13062)
        if CKEDITOR.env.ie
            range = editor.getSelection().getRanges()[0]
            link = range.getPreviousEditableNode() and range.getPreviousEditableNode().getAscendant('a', true) or range.getNextEditableNode() and range.getNextEditableNode().getAscendant('a', true)
            bookmark = undefined
            if range.collapsed and link
                bookmark = range.createBookmark()
                range.selectNodeContents link
                range.select()
        style = new (CKEDITOR.style)(
            element: 'a'
            type: CKEDITOR.STYLE_INLINE
            alwaysRemoveElement: 1)
        editor.removeStyle style
        if bookmark
            range.moveToBookmark bookmark
            range.select()
        return
    refresh: (editor, path) ->
        # Despite our initial hope, document.queryCommandEnabled() does not work
        # for this in Firefox. So we must detect the state by element paths.
        element = path.lastElement and path.lastElement.getAscendant('a', true)
        if element and element.getName() == 'a' and element.getAttribute('href') and element.getChildCount()
            @setState CKEDITOR.TRISTATE_OFF
        else
            @setState CKEDITOR.TRISTATE_DISABLED
        return
    contextSensitive: 1
    startDisabled: 1
    requiredContent: 'a[href]'
    editorFocus: 1

CKEDITOR.removeAnchorCommand = ->

CKEDITOR.removeAnchorCommand.prototype = {
    exec: (editor) ->
        sel = editor.getSelection()
        bms = sel.createBookmarks()
        anchor = undefined
        if sel and (anchor = sel.getSelectedElement()) and (if !anchor.getChildCount() then CKEDITOR.plugins.link.tryRestoreFakeAnchor(editor, anchor) else anchor.is('a'))
            anchor.remove 1
        else
            if anchor = CKEDITOR.plugins.link.getSelectedLink(editor)
                if anchor.hasAttribute('href')
                    anchor.removeAttributes({
                        name: 1
                        'data-cke-saved-name': 1
                    })
                    anchor.removeClass('cke_anchor')
                else
                    anchor.remove(1)
        sel.selectBookmarks bms
        return
    requiredContent: 'a[name]'
}


CKEDITOR.tools.extend CKEDITOR.config, {
    linkShowAdvancedTab: true
    linkShowTargetTab: true
}
