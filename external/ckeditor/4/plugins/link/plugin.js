
/**
 * @license Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

(function() {
  var advAttrNames, anchorRegex, escapeSingleQuote, popupFeaturesRegex, popupRegex, selectableTargets, unescapeSingleQuote, urlRegex,
    indexOf = [].indexOf || function(item) { for (var i = 0, l = this.length; i < l; i++) { if (i in this && this[i] === item) return i; } return -1; };

  unescapeSingleQuote = function(str) {
    return str.replace(/\\'/g, '\'');
  };

  escapeSingleQuote = function(str) {
    return str.replace(/'/g, '\\$&');
  };

  CKEDITOR.plugins.add('link', {
    requires: 'dialog,fakeobjects',
    lang: 'af,ar,az,bg,bn,bs,ca,cs,cy,da,de,de-ch,el,en,en-au,en-ca,en-gb,eo,es,et,eu,fa,fi,fo,fr,fr-ca,gl,gu,he,hi,hr,hu,id,is,it,ja,ka,km,ko,ku,lt,lv,mk,mn,ms,nb,nl,no,oc,pl,pt,pt-br,ro,ru,si,sk,sl,sq,sr,sr-latn,sv,th,tr,tt,ug,uk,vi,zh,zh-cn',
    icons: 'anchor,anchor-rtl,link,unlink',
    hidpi: true,
    onLoad: function() {
      var baseStyle, css, iconPath, template;
      iconPath = CKEDITOR.getUrl(this.path + 'images' + (CKEDITOR.env.hidpi ? '/hidpi' : '') + '/anchor.png');
      baseStyle = 'background:url(' + iconPath + ') no-repeat %1 center;border:1px dotted #00f;background-size:16px;';
      template = '.%2 a.cke_anchor,' + '.%2 a.cke_anchor_empty' + ',.cke_editable.%2 a[name]' + ',.cke_editable.%2 a[data-cke-saved-name]' + '{' + baseStyle + 'padding-%1:18px;' + 'cursor:auto;' + '}' + '.%2 img.cke_anchor' + '{' + baseStyle + 'width:16px;' + 'min-height:15px;' + 'height:1.15em;' + 'vertical-align:text-bottom;' + '}';
      css = template.replace(/%1/g, 'left').replace(/%2/g, 'cke_contents_ltr');
      CKEDITOR.addCss(css);
    },
    init: function(editor) {
      var allowed, required;
      allowed = 'a[!href,uic-rel,uic-href,file-id,uic-sref-for-ckeditor,du-smooth-scroll,ng-click]';
      required = 'a[href]';
      if (CKEDITOR.dialog.isTabEnabled(editor, 'link', 'advanced')) {
        allowed = allowed.replace(']', ',accesskey,charset,id,lang,name,rel,tabindex,title,type,download]{*}(*)');
      }
      if (CKEDITOR.dialog.isTabEnabled(editor, 'link', 'target')) {
        allowed = allowed.replace(']', ',target,onclick]');
      }
      editor.addCommand('link', new CKEDITOR.dialogCommand('link', {
        allowedContent: allowed,
        requiredContent: required
      }));
      editor.addCommand('anchor', new CKEDITOR.dialogCommand('anchor', {
        allowedContent: 'a[!name,id]',
        requiredContent: 'a[name]'
      }));
      editor.addCommand('unlink', new CKEDITOR.unlinkCommand);
      editor.addCommand('removeAnchor', new CKEDITOR.removeAnchorCommand);
      editor.setKeystroke(CKEDITOR.CTRL + 76, 'link');
      if (editor.ui.addButton) {
        editor.ui.addButton('Link', {
          label: editor.lang.link.toolbar,
          command: 'link',
          toolbar: 'links,10'
        });
        editor.ui.addButton('Unlink', {
          label: editor.lang.link.unlink,
          command: 'unlink',
          toolbar: 'links,20'
        });
        editor.ui.addButton('Anchor', {
          label: editor.lang.link.anchor.toolbar,
          command: 'anchor',
          toolbar: 'links,30'
        });
      }
      CKEDITOR.dialog.add('link', this.path + 'dialogs/link.js');
      CKEDITOR.dialog.add('anchor', this.path + 'dialogs/anchor.js');
      editor.on('doubleclick', (function(evt) {
        var element;
        element = CKEDITOR.plugins.link.getSelectedLink(editor) || evt.data.element.getAscendant('a', 1);
        if (element && !element.isReadOnly()) {
          if (element.is('a')) {
            evt.data.dialog = element.getAttribute('name') && (!element.getAttribute('href') || !element.getChildCount()) ? 'anchor' : 'link';
            evt.data.link = element;
          } else if (CKEDITOR.plugins.link.tryRestoreFakeAnchor(editor, element)) {
            evt.data.dialog = 'anchor';
          }
        }
      }), null, null, 0);
      editor.on('doubleclick', (function(evt) {
        var ref;
        if ((ref = evt.data.dialog, indexOf.call({
          link: 1,
          anchor: 1
        }, ref) >= 0) && evt.data.link) {
          editor.getSelection().selectElement(evt.data.link);
        }
      }), null, null, 20);
      if (editor.addMenuItems) {
        editor.addMenuItems({
          anchor: {
            label: editor.lang.link.anchor.menu,
            command: 'anchor',
            group: 'anchor',
            order: 1
          },
          removeAnchor: {
            label: editor.lang.link.anchor.remove,
            command: 'removeAnchor',
            group: 'anchor',
            order: 5
          },
          link: {
            label: editor.lang.link.menu,
            command: 'link',
            group: 'link',
            order: 1
          },
          unlink: {
            label: editor.lang.link.unlink,
            command: 'unlink',
            group: 'link',
            order: 5
          }
        });
      }
      if (editor.contextMenu) {
        editor.contextMenu.addListener(function(element) {
          var anchor, menu;
          if (!element || element.isReadOnly()) {
            return null;
          }
          anchor = CKEDITOR.plugins.link.tryRestoreFakeAnchor(editor, element);
          if (!anchor && !(anchor = CKEDITOR.plugins.link.getSelectedLink(editor))) {
            return null;
          }
          menu = {};
          if (anchor.getAttribute('href') && anchor.getChildCount()) {
            menu = {
              link: CKEDITOR.TRISTATE_OFF,
              unlink: CKEDITOR.TRISTATE_OFF
            };
          }
          if (anchor && anchor.hasAttribute('name')) {
            menu.anchor = menu.removeAnchor = CKEDITOR.TRISTATE_OFF;
          }
          return menu;
        });
      }
    },
    afterInit: function(editor) {
      var pathFilters;
      editor.dataProcessor.dataFilter.addRules({
        elements: {
          a: function(element) {
            if (!element.attributes.name) {
              return null;
            }
            if (!element.children.length) {
              return editor.createFakeParserElement(element, 'cke_anchor', 'anchor');
            }
            return null;
          }
        }
      });
      pathFilters = editor._.elementsPath && editor._.elementsPath.filters;
      if (pathFilters) {
        pathFilters.push(function(element, name) {
          if (name === 'a') {
            if (CKEDITOR.plugins.link.tryRestoreFakeAnchor(editor, element) || element.getAttribute('name') && (!element.getAttribute('href') || !element.getChildCount())) {
              return 'anchor';
            }
          }
        });
      }
    }
  });

  anchorRegex = /^#(.*)$/;

  urlRegex = /^((?:http|https|ftp|news):\/\/)?(.*)$/;

  selectableTargets = /^(_(?:self|top|parent|blank))$/;

  popupRegex = /\s*window.open\(\s*this\.href\s*,\s*(?:'([^']*)'|null)\s*,\s*'([^']*)'\s*\)\s*;\s*return\s*false;*\s*/;

  popupFeaturesRegex = /(?:^|,)([^=]+)=(\d+|yes|no)/gi;

  advAttrNames = {
    id: 'advId',
    accessKey: 'advAccessKey',
    name: 'advName',
    lang: 'advLangCode',
    tabindex: 'advTabIndex',
    title: 'advTitle',
    type: 'advContentType',
    'class': 'advCSSClasses',
    charset: 'advCharset',
    style: 'advStyles',
    rel: 'advRel'
  };


  /**
   * Set of Link plugin helpers.
   *
   * @class
   * @singleton
   */

  CKEDITOR.plugins.link = {
    getSelectedLink: function(editor) {
      var range, selectedElement, selection;
      selection = editor.getSelection();
      selectedElement = selection.getSelectedElement();
      if (selectedElement && selectedElement.is('a')) {
        return selectedElement;
      }
      range = selection.getRanges()[0];
      if (range) {
        range.shrink(CKEDITOR.SHRINK_TEXT);
        return editor.elementPath(range.getCommonAncestor()).contains('a', 1);
      }
      return null;
    },
    getEditorAnchors: function(editor) {
      var anchors, editable, i, imgs, item, links, scope;
      editable = editor.editable();
      scope = editable.isInline() && !editor.plugins.divarea ? editor.document : editable;
      links = scope.getElementsByTag('a');
      imgs = scope.getElementsByTag('img');
      anchors = [];
      i = 0;
      item = void 0;
      while (item = links.getItem(i++)) {
        if (item.data('cke-saved-name') || item.hasAttribute('name')) {
          anchors.push({
            name: item.data('cke-saved-name') || item.getAttribute('name'),
            id: item.getAttribute('id')
          });
        }
      }
      i = 0;
      while (item = imgs.getItem(i++)) {
        if (item = this.tryRestoreFakeAnchor(editor, item)) {
          anchors.push({
            name: item.getAttribute('name'),
            id: item.getAttribute('id')
          });
        }
      }
      return anchors;
    },
    fakeAnchor: true,
    tryRestoreFakeAnchor: function(editor, element) {
      var link;
      if (element && element.data('cke-real-element-type') && element.data('cke-real-element-type') === 'anchor') {
        link = editor.restoreRealElement(element);
        if (link.data('cke-saved-name')) {
          return link;
        }
      }
    },
    parseLinkAttributes: function(editor, element) {
      var a, advName, advanced, anchorMatch, download, featureMatch, href, id, javascriptMatch, ngClick, onclick, onclickMatch, ref, retval, target, urlMatch, val;
      href = element && (element.data('cke-saved-href') || element.getAttribute('href')) || '';
      javascriptMatch = void 0;
      anchorMatch = void 0;
      urlMatch = void 0;
      retval = {};
      if (href.indexOf('tel:') === 0) {
        retval.type = 'telephone';
        retval.telephone = {
          telephone: href.replace('tel:', '')
        };
      } else if (href.indexOf('mailto:') === 0) {
        retval.type = 'email';
        retval.email = {
          email: href.replace('mailto:', '')
        };
      } else if (anchorMatch = href.match(anchorRegex)) {
        retval.type = 'anchor';
        retval.anchor = {};
        retval.anchor.name = retval.anchor.id = anchorMatch[1];
      } else if (element && element.getAttribute('ng-click')) {
        ngClick = element.getAttribute('ng-click');
        ngClick = ngClick.split('runSiteAction(')[1];
        retval.type = 'action';
        retval.action = {
          id: ngClick.replace(')', '').replace(/"/g, '')
        };
      } else if (element && element.getAttribute('uic-href') && element.getAttribute('file-id')) {
        retval.type = 'media';
        retval.media = {
          fileId: element.getAttribute('file-id')
        };
      } else if (element && element.getAttribute('uic-sref-for-ckeditor')) {
        retval.type = 'uiSrefArticle';
        id = element.getAttribute('uic-sref-for-ckeditor').split('{')[1].split('}')[0];
        id = id.replace('id', '').replaceAll(' ', '').replaceAll("'", '').replaceAll('"', '').replaceAll(':', '');
        if (CMS_VERSION >= 31) {
          id = parseInt(id);
        }
        retval.uiSrefArticle = {
          id: id
        };
      } else if (href) {
        retval.type = 'url';
        retval.url = {};
        retval.url.url = href;
      }
      if (element) {
        target = element.getAttribute('target');
        if (!target) {
          onclick = element.data('cke-pa-onclick') || element.getAttribute('onclick');
          onclickMatch = onclick && onclick.match(popupRegex);
          if (onclickMatch) {
            retval.target = {
              type: 'popup',
              name: onclickMatch[1]
            };
            featureMatch = void 0;
            while (featureMatch = popupFeaturesRegex.exec(onclickMatch[2])) {
              if ((featureMatch[2] === 'yes' || featureMatch[2] === '1') && !(ref = featureMatch[1], indexOf.call({
                height: 1,
                width: 1,
                top: 1,
                left: 1
              }, ref) >= 0)) {
                retval.target[featureMatch[1]] = true;
              } else if (isFinite(featureMatch[2])) {
                retval.target[featureMatch[1]] = featureMatch[2];
              }
            }
          } else {
            retval.target = {
              type: '_blank',
              name: '_blank'
            };
          }
        } else {
          retval.target = {
            type: target.match(selectableTargets) ? target : 'frame',
            name: target
          };
        }
        download = element.getAttribute('download');
        if (download !== null) {
          retval.download = true;
        }
        advanced = {};
        for (a in advAttrNames) {
          val = element.getAttribute(a);
          if (val) {
            advanced[advAttrNames[a]] = val;
          }
        }
        advName = element.data('cke-saved-name') || advanced.advName;
        if (advName) {
          advanced.advName = advName;
        }
        if (!CKEDITOR.tools.isEmpty(advanced)) {
          retval.advanced = advanced;
        }
      }
      return retval;
    },
    getLinkAttributes: function(editor, data) {
      var a, addFeature, featureLength, featureList, i, id, name, onclickList, ref, removed, s, set, url, val;
      set = {};
      set['uic-rel'] = '';
      switch (data.type) {
        case 'telephone':
          set['data-cke-saved-href'] = "tel:" + data.telephone.telephone;
          break;
        case 'email':
          set['data-cke-saved-href'] = "mailto:" + data.email.email;
          break;
        case 'url':
          if (!data.url) {
            set['data-cke-saved-href'] = '';
          } else {
            url = data.url.url;
            if (!data.url.url.match(urlRegex) && url.indexOf('/') === -1) {
              url = "http://" + url;
            }
            set['data-cke-saved-href'] = url;
          }
          break;
        case 'action':
          set['ng-click'] = "$cms.collapseNavbar();$cms.runSiteAction(\"" + data.action.id + "\")";
          break;
        case 'media':
          set['uic-href'] = "item.files";
          set['file-id'] = data.media.fileId;
          set['href'] = data.media.url;
          break;
        case 'uiSrefArticle':
          set['uic-sref-for-ckeditor'] = "app.articles.details({id: " + data.uiSrefArticle.id + "})";
          set['href'] = '';
          break;
        case 'anchor':
          name = data.anchor && data.anchor.name;
          id = data.anchor && data.anchor.id;
          set['data-cke-saved-href'] = '#' + (name || id || '');
          set['du-smooth-scroll'] = '';
      }
      if (data.target) {
        if (data.target.type === 'popup') {
          onclickList = ['window.open(this.href, \'', data.target.name || '', '\', \''];
          featureList = ['resizable', 'status', 'location', 'toolbar', 'menubar', 'fullscreen', 'scrollbars', 'dependent'];
          featureLength = featureList.length;
          addFeature = function(featureName) {
            if (data.target[featureName]) {
              featureList.push(featureName + '=' + data.target[featureName]);
            }
          };
          i = 0;
          while (i < featureLength) {
            featureList[i] = featureList[i] + (data.target[featureList[i]] ? '=yes' : '=no');
            i++;
          }
          addFeature('width');
          addFeature('left');
          addFeature('height');
          addFeature('top');
          onclickList.push(featureList.join(','), '\'); return false;');
          set['data-cke-pa-onclick'] = onclickList.join('');
        } else if (data.target.type !== 'notSet' && data.target.name) {
          set.target = data.target.name;
        }
      }
      if (data.download) {
        set.download = '';
      }
      if (data.advanced) {
        for (a in advAttrNames) {
          val = data.advanced[advAttrNames[a]];
          if (val) {
            set[a] = val;
          }
        }
        if (set.name) {
          set['data-cke-saved-name'] = set.name;
        }
      }
      if (set['data-cke-saved-href']) {
        set.href = set['data-cke-saved-href'];
      }
      removed = {
        target: 1,
        onclick: 1,
        'data-cke-pa-onclick': 1,
        'data-cke-saved-name': 1,
        'download': 1
      };
      if (data.advanced) {
        CKEDITOR.tools.extend(removed, advAttrNames);
      }
      for (s in set) {
        delete removed[s];
      }
      if (data.type !== 'anchor') {
        removed['du-smooth-scroll'] = 1;
      }
      if (data.type !== 'media') {
        removed['uic-href'] = 1;
        removed['file-id'] = 1;
      }
      if ((ref = data.type) !== 'url' && ref !== 'telephone' && ref !== 'email') {
        removed['data-cke-saved-href'] = 1;
        removed['href'] = 1;
      }
      if (data.type === 'action') {
        removed['uic-href'] = 1;
        removed['file-id'] = 1;
        removed['href'] = 1;
        removed['onclick'] = 1;
        removed['target'] = 1;
        removed['data-cke-saved-href'] = 1;
      } else {
        removed['ng-click'] = 1;
      }
      if (data.type === 'uiSrefArticle') {
        removed.href = 1;
        removed.onclick = 1;
        removed.target = 1;
        removed['data-cke-saved-href'] = 1;
      } else {
        removed['uic-sref-for-ckeditor'] = 1;
      }
      return {
        set: set,
        removed: CKEDITOR.tools.objectKeys(removed)
      };
    },
    showDisplayTextForElement: function(element, editor) {
      var undesiredElements;
      undesiredElements = {
        img: 1,
        table: 1,
        tbody: 1,
        thead: 1,
        tfoot: 1,
        input: 1,
        select: 1,
        textarea: 1
      };
      if (editor.widgets && editor.widgets.focused) {
        return false;
      }
      return !element || !element.getName || !element.is(undesiredElements);
    }
  };

  CKEDITOR.unlinkCommand = function() {};

  CKEDITOR.unlinkCommand.prototype = {
    exec: function(editor) {
      var bookmark, link, range, style;
      if (CKEDITOR.env.ie) {
        range = editor.getSelection().getRanges()[0];
        link = range.getPreviousEditableNode() && range.getPreviousEditableNode().getAscendant('a', true) || range.getNextEditableNode() && range.getNextEditableNode().getAscendant('a', true);
        bookmark = void 0;
        if (range.collapsed && link) {
          bookmark = range.createBookmark();
          range.selectNodeContents(link);
          range.select();
        }
      }
      style = new CKEDITOR.style({
        element: 'a',
        type: CKEDITOR.STYLE_INLINE,
        alwaysRemoveElement: 1
      });
      editor.removeStyle(style);
      if (bookmark) {
        range.moveToBookmark(bookmark);
        range.select();
      }
    },
    refresh: function(editor, path) {
      var element;
      element = path.lastElement && path.lastElement.getAscendant('a', true);
      if (element && element.getName() === 'a' && element.getAttribute('href') && element.getChildCount()) {
        this.setState(CKEDITOR.TRISTATE_OFF);
      } else {
        this.setState(CKEDITOR.TRISTATE_DISABLED);
      }
    },
    contextSensitive: 1,
    startDisabled: 1,
    requiredContent: 'a[href]',
    editorFocus: 1
  };

  CKEDITOR.removeAnchorCommand = function() {};

  CKEDITOR.removeAnchorCommand.prototype = {
    exec: function(editor) {
      var anchor, bms, sel;
      sel = editor.getSelection();
      bms = sel.createBookmarks();
      anchor = void 0;
      if (sel && (anchor = sel.getSelectedElement()) && (!anchor.getChildCount() ? CKEDITOR.plugins.link.tryRestoreFakeAnchor(editor, anchor) : anchor.is('a'))) {
        anchor.remove(1);
      } else {
        if (anchor = CKEDITOR.plugins.link.getSelectedLink(editor)) {
          if (anchor.hasAttribute('href')) {
            anchor.removeAttributes({
              name: 1,
              'data-cke-saved-name': 1
            });
            anchor.removeClass('cke_anchor');
          } else {
            anchor.remove(1);
          }
        }
      }
      sel.selectBookmarks(bms);
    },
    requiredContent: 'a[name]'
  };

  CKEDITOR.tools.extend(CKEDITOR.config, {
    linkShowAdvancedTab: true,
    linkShowTargetTab: true
  });

}).call(this);
