(function() {
  var $cms, $compile, $injector, $models, H, colSizes, generateNgWidgetClass;

  $injector = null;

  $compile = null;

  $models = null;

  $cms = null;

  H = null;

  colSizes = [['25%', '25'], ['30%', '30'], ['35%', '35'], ['40%', '40'], ['45%', '45'], ['50%', '50'], ['55%', '55'], ['60%', '60'], ['65%', '65'], ['70%', '70'], ['75%', '75'], ['100%', '100']];

  CKEDITOR.plugins.add('lbcmshelpers', {
    lang: 'en,ru',
    init: function(editor) {
      $injector = CKEDITOR.$injector;
      $compile = $injector.get('$compile');
      $models = $injector.get('$models');
      $cms = $injector.get('$cms');
      return H = $injector.get('H');
    }
  });

  generateNgWidgetClass = function(editor) {
    var NgHtmlWidget;
    return NgHtmlWidget = (function() {
      function NgHtmlWidget(options, onCommit, onChange) {
        if (typeof options === 'function') {
          onCommit = options;
          onChange = onCommit;
          options = {};
        }
        if (!options.propName) {
          throw "Expect property 'propName' in options for NgHtmlWidget. Got undefined.";
        }
        this.options = options;
        this._onCommit = onCommit || angular.noop;
        this._onChange = onChange || angular.noop;
        this.id = ("ngWidget" + (options.widgetName || '') + "-") + new Date().getTime() + '-' + Math.random().toString(36).substring(7);
      }

      NgHtmlWidget.prototype.resolveL10nFilePath = function(cmsModelItem, id) {
        var f, i, len, ref;
        if (!cmsModelItem || !cmsModelItem.files || !id) {
          return '';
        }
        ref = cmsModelItem.files || [];
        for (i = 0, len = ref.length; i < len; i++) {
          f = ref[i];
          if (f.id === id) {
            return H.path.resolveL10nFilePath(f);
          }
        }
        return '';
      };

      NgHtmlWidget.prototype.onCommit = function(widget, value, $scope) {
        this._onCommit(widget, value, $scope);
      };

      NgHtmlWidget.prototype.onChange = function(widget, value, $scope) {
        this._onChange(widget, value, $scope);
      };

      NgHtmlWidget.prototype.getDefinition = function() {
        var ngWidget, setupData;
        ngWidget = this;
        setupData = null;
        return {
          id: ngWidget.id,
          type: 'html',
          html: "<div class='noreset' id='" + this.id + "'>" + ngWidget.options.html + "</div>",
          onLoad: function(event) {},
          setup: function(data) {
            if (data.data) {
              setupData = data.data;
            } else {
              setupData = data;
            }
            if (ngWidget.$scope) {
              ngWidget.$scope.ngModel = angular.getValue(setupData, ngWidget.options.propName);
            }
          },
          onShow: function(widget) {
            var compileElement, dialogWidget, element, parentScope;
            dialogWidget = widget.sender.widget || widget.sender.$widget;
            parentScope = editor.$scope;
            if (!ngWidget.$scope) {
              ngWidget.$scope = parentScope.$new();
              compileElement = true;
            }
            ngWidget.$scope.$apply(function() {
              ngWidget.$scope.options = ngWidget.options;
              ngWidget.$scope.ngModel = angular.getValue(dialogWidget.data || setupData, ngWidget.options.propName);
            });
            ngWidget.$scope.$watch('ngModel', function(ngModel, oldValue) {
              if (ngModel !== oldValue) {
                ngWidget.onChange(widget, ngModel, ngWidget.$scope);
              }
            });

            /*
             * компилируем директиву ангулара.
             * поскольку оригинальный widget не дает нам ссылку на html-элемент,
             * то ищем его вручную
            for page in @_.dialog.definition.contents
                _widget = @_.dialog.getContentElement(page.id, ngWidget.id)
                if _widget
                    element = _widget.getElement()
             */
            element = document.getElementById(this.id);
            if (!element) {
              throw "lbcmshelpers:: Can't find element in ckeditor dialog with id '" + ngWidget.id + "'";
            }
            if (compileElement) {
              $compile(element)(ngWidget.$scope);
            } else {
              setTimeout(function() {
                return ngWidget.$scope.$apply();
              }, 10);
            }
          },
          commit: function(widget) {
            if (widget.hasOwnProperty('data')) {
              angular.setValue(widget.data, ngWidget.options.propName, ngWidget.$scope.ngModel);
            } else {
              angular.setValue(widget, ngWidget.options.propName, ngWidget.$scope.ngModel);
            }
            ngWidget.onCommit(widget, ngWidget.$scope.ngModel, ngWidget.$scope);
          }
        };
      };

      return NgHtmlWidget;

    })();
  };

  CKEDITOR.plugins.lbcmshelpers = function(editor) {
    var NgHtmlWidgetBase, lang;
    lang = editor.lang.lbcmshelpers;
    NgHtmlWidgetBase = generateNgWidgetClass(editor);
    return {
      $injector: $injector,
      $compile: $compile,
      $models: $models,
      $cms: $cms,
      H: H,
      colsGrid: {
        cleanWidgetClasses: function(element) {
          var allowed, cl, classes, i, len;
          if (element.getClasses) {
            classes = Object.keys(element.getClasses());
          } else if (element.$) {
            classes = element.$.className || "";
            classes = classes.split(' ');
          } else {
            throw "Got unusual element. Neither HtmlElement, nor $-element. " + element;
          }
          allowed = [];
          for (i = 0, len = classes.length; i < len; i++) {
            cl = classes[i];
            if (cl.indexOf('col-') === -1 && cl !== "cke_widget_element" && cl.indexOf('pull-') === -1) {
              allowed.push(cl);
            } else {
              element.removeClass(cl);
            }
          }
          return allowed;
        },
        setWidgetClasses: function(element, data, allowedClasses) {
          var cl, i, j, len, len1, ref, s, screenSize;
          allowedClasses = allowedClasses || [];
          ref = ['lg', 'md', 'sm', 'xs'];
          for (i = 0, len = ref.length; i < len; i++) {
            screenSize = ref[i];
            s = data["colWidthClass_" + screenSize];
            allowedClasses.push("col-" + screenSize + "-" + s);
          }
          if (element.addClass) {
            for (j = 0, len1 = allowedClasses.length; j < len1; j++) {
              cl = allowedClasses[j];
              element.addClass(cl);
            }
          } else {
            element.className = allowedClasses.join(' ');
          }
        }
      },
      configWidgets: {
        getColsWidth: function() {
          return {
            type: 'vbox',
            padding: 0,
            children: [
              {
                type: 'select',
                id: 'colWidthClass_lg',
                label: lang.colWidthClass_lg,
                items: colSizes,
                setup: function(widget) {
                  this.setValue(widget.data.colWidthClass_lg);
                },
                commit: function(widget) {
                  widget.setData('colWidthClass_lg', this.getValue());
                }
              }, {
                type: 'select',
                id: 'colWidthClass_md',
                label: lang.colWidthClass_md,
                items: colSizes,
                setup: function(widget) {
                  this.setValue(widget.data.colWidthClass_md);
                },
                commit: function(widget) {
                  widget.setData('colWidthClass_md', this.getValue());
                }
              }, {
                type: 'select',
                id: 'colWidthClass_sm',
                label: lang.colWidthClass_sm,
                items: colSizes,
                setup: function(widget) {
                  this.setValue(widget.data.colWidthClass_sm);
                },
                commit: function(widget) {
                  widget.setData('colWidthClass_sm', this.getValue());
                }
              }, {
                type: 'select',
                id: 'colWidthClass_xs',
                label: lang.colWidthClass_xs,
                items: colSizes,
                setup: function(widget) {
                  this.setValue(widget.data.colWidthClass_xs);
                },
                commit: function(widget) {
                  widget.setData('colWidthClass_xs', this.getValue());
                }
              }
            ]
          };
        },
        getFilePickerForFileId: function(options, onCommit, onChange) {
          var _ngWidget, attrs;
          if (typeof options === 'function') {
            onCommit = options;
            onChange = onCommit;
            options = {};
          }
          if (!options.propName) {
            options.propName = 'fileId';
          }
          options['input-size'] = options['input-size'] || 'sm';
          attrs = H.convert.objToHtmlProps(options);
          options.html = "<uice-file-id-picker " + attrs + "\n    ng-model='ngModel'\n    cms-model-item='cmsModelItem'\n    allow-dnd='false'>\n</uice-file-id-picker>";
          if (!options.widgetName) {
            options.widgetName = 'UiceFileIdPicker';
          }
          _ngWidget = new NgHtmlWidgetBase(options, function(widget, value, $scope) {
            if (onCommit) {
              onCommit(widget, value, _ngWidget.resolveL10nFilePath($scope.cmsModelItem || $scope.$parent.cmsModelItem, value));
            }
          }, function(widget, value, $scope) {
            if (onChange) {
              onChange(widget, value, _ngWidget.resolveL10nFilePath($scope.cmsModelItem || $scope.$parent.cmsModelItem, value));
            }
          });
          return _ngWidget.getDefinition();
        },
        getAngularHtmlWidget: function(options, onCommit, onChange) {
          var _ngWidget;
          _ngWidget = new NgHtmlWidgetBase(options, onCommit, onChange);
          return _ngWidget.getDefinition();
        }
      }
    };
  };

}).call(this);
