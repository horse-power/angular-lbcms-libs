
$injector = null
$compile = null
$models = null
$cms = null
H = null

colSizes = [
    ['25%', '25']
    ['30%', '30']
    ['35%', '35']
    ['40%', '40']
    ['45%', '45']
    ['50%', '50']
    ['55%', '55']
    ['60%', '60']
    ['65%', '65']
    ['70%', '70']
    ['75%', '75']
    ['100%', '100']
]



CKEDITOR.plugins.add 'lbcmshelpers', {
    lang: 'en,ru'
    init: (editor) ->
        $injector = CKEDITOR.$injector
        $compile = $injector.get('$compile')
        $models = $injector.get('$models')
        $cms = $injector.get('$cms')
        H = $injector.get('H')
}



generateNgWidgetClass = (editor)->
    class NgHtmlWidget
        constructor: (options, onCommit, onChange)->
            if typeof(options) == 'function'
                onCommit = options
                onChange = onCommit
                options = {}
            if !options.propName
                throw "Expect property 'propName' in options for NgHtmlWidget. Got undefined."
            @options = options
            @_onCommit = onCommit or angular.noop
            @_onChange = onChange or angular.noop

            @id = "ngWidget#{options.widgetName || ''}-" + new Date().getTime() + '-' + Math.random().toString(36).substring(7)

        resolveL10nFilePath: (cmsModelItem, id)->
            if !cmsModelItem or !cmsModelItem.files or !id then return ''
            for f in cmsModelItem.files or []
                if f.id == id
                    return H.path.resolveL10nFilePath(f)
            ''

        onCommit: (widget, value, $scope)->
            @_onCommit(widget, value, $scope)
            return
        onChange: (widget, value, $scope)->
            @_onChange(widget, value, $scope)
            return

        getDefinition: ()->
            ngWidget = this
            setupData = null
            #
            id: ngWidget.id
            type: 'html'
            html: "<div class='noreset' id='#{@id}'>#{ngWidget.options.html}</div>"
            onLoad: (event) ->
                return
            setup: (data)->
                if data.data
                    setupData = data.data
                else
                    setupData = data
                if ngWidget.$scope
                    ngWidget.$scope.ngModel = angular.getValue(
                        setupData,
                        ngWidget.options.propName
                    )
                return
            onShow: (widget) ->
                # widget.sender.widget - оригинальный виджет от ckeditor
                # ------.------.$widget - хак, для поддерки ангулара из бажных плагинов
                dialogWidget = widget.sender.widget or widget.sender.$widget

                # создаем дочерний скоуп и заполняем его
                parentScope = editor.$scope
                if not ngWidget.$scope
                    ngWidget.$scope = parentScope.$new()
                    compileElement = true

                # устанавливаем значения в $scope
                ngWidget.$scope.$apply ()->
                    ngWidget.$scope.options = ngWidget.options
                    ngWidget.$scope.ngModel = angular.getValue(
                        dialogWidget.data or setupData,
                        ngWidget.options.propName
                    )
                    return
                # запускаем слежение за моделью
                ngWidget.$scope.$watch 'ngModel', (ngModel, oldValue)->
                    if ngModel!=oldValue
                        ngWidget.onChange(widget, ngModel, ngWidget.$scope)
                    return

                ###
                # компилируем директиву ангулара.
                # поскольку оригинальный widget не дает нам ссылку на html-элемент,
                # то ищем его вручную
                for page in @_.dialog.definition.contents
                    _widget = @_.dialog.getContentElement(page.id, ngWidget.id)
                    if _widget
                        element = _widget.getElement()
                ###

                # гадкий ckeditor таки теряет виджеты, больше не полагаюсь на реализацию
                # выше. ищем тупо по id
                element = document.getElementById(@id)
                if !element
                    # что тут могло бы случиться, я не знаю,
                    # но на всякий случай:
                    throw "lbcmshelpers:: Can't find element in ckeditor dialog with id '#{ngWidget.id}'"
                # подсоединяем $scope к верстке
                if compileElement
                    $compile(element)(ngWidget.$scope)
                else
                    # если $scope уже подсоединен, то только запускаем $digest чтоб
                    # перерисовать верстку согласно переменным
                    setTimeout(
                        ()-> ngWidget.$scope.$apply()
                        ,
                        10
                    )
                return

            commit: (widget)->
                # снова разночтения в плагинах.
                # иногда widget - это экземляр ckeditor-виджета, а иногда
                # это просто данные которые должно редактировать модальное окно=(
                if widget.hasOwnProperty('data')
                    angular.setValue(widget.data, ngWidget.options.propName, ngWidget.$scope.ngModel)
                else
                    angular.setValue(widget, ngWidget.options.propName, ngWidget.$scope.ngModel)
                ngWidget.onCommit(widget, ngWidget.$scope.ngModel, ngWidget.$scope)
                # скоуп больше не нужен
                # ngWidget.$scope.$destroy()
                return









CKEDITOR.plugins.lbcmshelpers = (editor)->
    lang = editor.lang.lbcmshelpers
    NgHtmlWidgetBase = generateNgWidgetClass(editor)
    # возвращаем объект с helper-ами

    $injector: $injector
    $compile: $compile
    $models: $models
    $cms: $cms
    H: H
    colsGrid: {
        cleanWidgetClasses: (element)->
            if element.getClasses
                classes = Object.keys(element.getClasses())
            else if element.$
                classes = element.$.className or ""
                classes = classes.split(' ')
            else
                throw "Got unusual element. Neither HtmlElement, nor $-element. " + element
            allowed = []
            for cl in classes
                if cl.indexOf('col-')==-1 and cl!="cke_widget_element" and cl.indexOf('pull-')==-1
                    allowed.push(cl)
                else
                    element.removeClass(cl)
            allowed
        setWidgetClasses: (element, data, allowedClasses)->
            allowedClasses = allowedClasses or []
            for screenSize in ['lg', 'md', 'sm', 'xs']
                s = data["colWidthClass_#{screenSize}"]
                allowedClasses.push("col-#{screenSize}-#{s}")
            if element.addClass
                for cl in allowedClasses
                    element.addClass(cl)
            else
                element.className = allowedClasses.join(' ')
            return
    }
    configWidgets: {
        getColsWidth: ()->
            {
                type: 'vbox'
                padding: 0
                children: [{
                    type: 'select'
                    id: 'colWidthClass_lg'
                    label: lang.colWidthClass_lg
                    items: colSizes
                    setup: (widget) ->
                        @setValue(widget.data.colWidthClass_lg)
                        return
                    commit: (widget) ->
                        widget.setData 'colWidthClass_lg', @getValue()
                        return
                }, {
                    type: 'select'
                    id: 'colWidthClass_md'
                    label: lang.colWidthClass_md
                    items: colSizes
                    setup: (widget) ->
                        @setValue(widget.data.colWidthClass_md)
                        return
                    commit: (widget) ->
                        widget.setData 'colWidthClass_md', @getValue()
                        return
                }, {
                    type: 'select'
                    id: 'colWidthClass_sm'
                    label: lang.colWidthClass_sm
                    items: colSizes
                    setup: (widget) ->
                        @setValue(widget.data.colWidthClass_sm)
                        return
                    commit: (widget) ->
                        widget.setData 'colWidthClass_sm', @getValue()
                        return
                }, {
                    type: 'select'
                    id: 'colWidthClass_xs'
                    label: lang.colWidthClass_xs
                    items: colSizes
                    setup: (widget) ->
                        @setValue(widget.data.colWidthClass_xs)
                        return
                    commit: (widget) ->
                        widget.setData 'colWidthClass_xs', @getValue()
                        return
                }]
            }
        getFilePickerForFileId: (options, onCommit, onChange)->
            if typeof(options) == 'function'
                onCommit = options
                onChange = onCommit
                options = {}
            if !options.propName
                options.propName = 'fileId'
            options['input-size'] = options['input-size'] or 'sm'
            attrs = H.convert.objToHtmlProps(options)
            options.html = """
                <uice-file-id-picker #{attrs}
                    ng-model='ngModel'
                    cms-model-item='cmsModelItem'
                    allow-dnd='false'>
                </uice-file-id-picker>
            """
            if !options.widgetName
                options.widgetName = 'UiceFileIdPicker'

            _ngWidget = new NgHtmlWidgetBase options, \
                (widget, value, $scope)->
                    if onCommit
                        onCommit(widget, value, _ngWidget.resolveL10nFilePath(
                            $scope.cmsModelItem or $scope.$parent.cmsModelItem,
                            value
                        ))
                    return
                ,
                (widget, value, $scope)->
                    if onChange
                        onChange(widget, value, _ngWidget.resolveL10nFilePath(
                            $scope.cmsModelItem or $scope.$parent.cmsModelItem,
                            value
                        ))
                    return

            return _ngWidget.getDefinition()

        getAngularHtmlWidget: (options, onCommit, onChange)->
            _ngWidget = new NgHtmlWidgetBase(options, onCommit, onChange)
            _ngWidget.getDefinition()

    }
