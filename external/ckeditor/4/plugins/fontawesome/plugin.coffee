CKEDITOR.dtd.$removeEmpty['span'] = false
DEFAULT_FA_ICONS_VERSION = 'v4'

findFontAwesomeHref = (links)->
    for link in links when link.href and link.rel == 'stylesheet'
        if link.href.includes('font-awesome') and link.href.includes('.css')
            return link.href
    null

addFontAwesomeToEditor = do()->
    v5Fix = document.createElement('style')
    v5Fix.innerHTML = """
        .fa.fad{
            font-family: "Font Awesome 5 Duotone" !important;
        }
        .fa.fab{
            font-family: "Font Awesome 5 Brands" !important;
        }
        .fa.fal, .fa.far {
            font-family: "Font Awesome 5 Free" !important;
        }
        .fa.fas{
            font-family: "Font Awesome 5 Free" !important;
        }
    """
    (ckeditorHead, faLink)->
        ckeditorHead.appendChild(faLink)
        ckeditorHead.appendChild(v5Fix)
        return


CKEDITOR.plugins.add 'fontawesome',
    requires: 'widget'
    lang: 'en,ru'
    icons: 'fontawesome'
    init: (editor) ->
        lang = editor.lang.fontawesome
        editor.widgets.add('FontAwesome', {
            button: lang.dialogTitle
            template: '<span class="fa" data-font-awesome></span>'
            dialog: 'fontawesomeDialog'
            allowedContent: 'span[class,!data-font-awesome](fa)'
            upcast: (element) ->
                element.name == 'span' and element.hasClass('fa')
            init: ->
                data = CKEDITOR.plugins.fontawesome.parseAttributes(@element.$)
                for k,v of data
                    @setData(k, v)
                return
            data: ->
                classes = ['fa']
                if @data.iconClass
                    classes.push(@data.iconClass)
                position = getPositionClass(@data.position)
                if position
                    classes.push(position)
                @element.$.className = classes.join(' ')
                if @data.size
                    @element.$.style['font-size'] = @data.size + 'px'
                if @data.color
                    @element.$.style.color = @data.color
                return
        })
        CKEDITOR.dialog.add('fontawesomeDialog', @path + 'dialogs/fontawesome.js')
        # пытаемся найти стилевик шрифта на странице
        # если ничего не нашли, то грузим с cdn
        faHref = findFontAwesomeHref(document.getElementsByTagName('link')) or 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css'
        if faHref.indexOf('/5.') > -1
            DEFAULT_FA_ICONS_VERSION = 'v5'
            CKEDITOR.plugins.fontawesome.DEFAULT_FA_ICONS_VERSION = 'v5'
        link = document.createElement('link')
        link.type = 'text/css'
        link.rel = 'stylesheet'
        link.href = faHref

        # HACK: ckeditor не дает нормально загружать левые стилевики
        editor.on 'instanceReady', ()->
            setTimeout \
                ()->
                    addFontAwesomeToEditor(editor.document.$.head, link)
                    return
                    #editor.document.$.head.appendChild(link)
                ,
                100
        # каждый раз при изменении типа отображения ckeditor будет избавляться от нужного стилевика
        editor.on 'mode', ()->
            if @mode != 'source'
                addFontAwesomeToEditor(editor.document.$.head, link)
                #editor.document.$.head.appendChild(link)
            return


###*
# Set of FontAwesome plugin helpers.
#
# @class
# @singleton
###
POSITION_CLASSES = [
    {
        id: 'normalPosition'
        class: ''
    },{
        id: 'rotate90'
        class: 'fa-rotate-90'
    },{
        id: 'rotate180'
        class: 'fa-rotate-180'
    },{
        id: 'rotate270'
        class: 'fa-rotate-270'
    },{
        id: 'flipHorizontal'
        class: 'fa-flip-horizontal'
    },{
        id: 'flipVertical'
        class: 'fa-flip-vertical'
    }
]

getPositionId = (cl)->
    for p in POSITION_CLASSES when p.class == cl
        return p.id
    null

getPositionClass = (id)->
    for p in POSITION_CLASSES when p.id == id
        return p.class
    null

CKEDITOR.plugins.fontawesome = {
    DEFAULT_FA_ICONS_VERSION
    POSITION_CLASSES
    getSelectedElement: (editor) ->
        selection = editor.getSelection()
        selectedElement = selection.getSelectedElement()
        if selectedElement and selectedElement.is('fa-icon')
            return selectedElement
        range = selection.getRanges()[0]
        if range
            range.shrink CKEDITOR.SHRINK_TEXT
            return editor.elementPath(range.getCommonAncestor()).contains('fa-icon', 1)
        null
    parseAttributes: (element) ->
        _classes = ''
        _styles = {}
        if element
            _classes = element.getAttribute('class') or ''
            _styles = element.style or {}

        _classes = _classes.split(' ')
        retval = {
            position: 'normalPosition'
            iconClass: ''
            size: _styles['font-size'] or ''
            color: _styles.color or ''
            ###
            effects:{
                spinning: false
                fixedWidth: false
                border: false
            }
            ###
        }
        primaryClass = 'fa'
        for cl in _classes when cl != 'fa' and cl.indexOf('fa-') == -1 and cl.indexOf('cke_widget_element') == -1
            primaryClass = cl
            break
        if _classes.includes('fa')
            for cl in _classes when cl.indexOf('fa-') == 0
                pcl = getPositionId(cl)
                if pcl
                    retval.position = pcl
                    continue
                ###
                if cl == 'fa-spin'
                    retval.effects.spinning = true
                    continue
                if cl == 'fa-fw'
                    retval.effects.fixedWidth = true
                    continue
                if cl == 'fa-border'
                    retval.effects.border = true
                    continue
                ###
                retval.iconClass = primaryClass + ' ' + cl
        retval

}
