(function() {
  var DEFAULT_FA_ICONS_VERSION, POSITION_CLASSES, addFontAwesomeToEditor, findFontAwesomeHref, getPositionClass, getPositionId;

  CKEDITOR.dtd.$removeEmpty['span'] = false;

  DEFAULT_FA_ICONS_VERSION = 'v4';

  findFontAwesomeHref = function(links) {
    var i, len, link;
    for (i = 0, len = links.length; i < len; i++) {
      link = links[i];
      if (link.href && link.rel === 'stylesheet') {
        if (link.href.includes('font-awesome') && link.href.includes('.css')) {
          return link.href;
        }
      }
    }
    return null;
  };

  addFontAwesomeToEditor = (function() {
    var v5Fix;
    v5Fix = document.createElement('style');
    v5Fix.innerHTML = ".fa.fad{\n    font-family: \"Font Awesome 5 Duotone\" !important;\n}\n.fa.fab{\n    font-family: \"Font Awesome 5 Brands\" !important;\n}\n.fa.fal, .fa.far {\n    font-family: \"Font Awesome 5 Free\" !important;\n}\n.fa.fas{\n    font-family: \"Font Awesome 5 Free\" !important;\n}";
    return function(ckeditorHead, faLink) {
      ckeditorHead.appendChild(faLink);
      ckeditorHead.appendChild(v5Fix);
    };
  })();

  CKEDITOR.plugins.add('fontawesome', {
    requires: 'widget',
    lang: 'en,ru',
    icons: 'fontawesome',
    init: function(editor) {
      var faHref, lang, link;
      lang = editor.lang.fontawesome;
      editor.widgets.add('FontAwesome', {
        button: lang.dialogTitle,
        template: '<span class="fa" data-font-awesome></span>',
        dialog: 'fontawesomeDialog',
        allowedContent: 'span[class,!data-font-awesome](fa)',
        upcast: function(element) {
          return element.name === 'span' && element.hasClass('fa');
        },
        init: function() {
          var data, k, v;
          data = CKEDITOR.plugins.fontawesome.parseAttributes(this.element.$);
          for (k in data) {
            v = data[k];
            this.setData(k, v);
          }
        },
        data: function() {
          var classes, position;
          classes = ['fa'];
          if (this.data.iconClass) {
            classes.push(this.data.iconClass);
          }
          position = getPositionClass(this.data.position);
          if (position) {
            classes.push(position);
          }
          this.element.$.className = classes.join(' ');
          if (this.data.size) {
            this.element.$.style['font-size'] = this.data.size + 'px';
          }
          if (this.data.color) {
            this.element.$.style.color = this.data.color;
          }
        }
      });
      CKEDITOR.dialog.add('fontawesomeDialog', this.path + 'dialogs/fontawesome.js');
      faHref = findFontAwesomeHref(document.getElementsByTagName('link')) || 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css';
      if (faHref.indexOf('/5.') > -1) {
        DEFAULT_FA_ICONS_VERSION = 'v5';
        CKEDITOR.plugins.fontawesome.DEFAULT_FA_ICONS_VERSION = 'v5';
      }
      link = document.createElement('link');
      link.type = 'text/css';
      link.rel = 'stylesheet';
      link.href = faHref;
      editor.on('instanceReady', function() {
        return setTimeout(function() {
          addFontAwesomeToEditor(editor.document.$.head, link);
        }, 100);
      });
      return editor.on('mode', function() {
        if (this.mode !== 'source') {
          addFontAwesomeToEditor(editor.document.$.head, link);
        }
      });
    }
  });


  /**
   * Set of FontAwesome plugin helpers.
   *
   * @class
   * @singleton
   */

  POSITION_CLASSES = [
    {
      id: 'normalPosition',
      "class": ''
    }, {
      id: 'rotate90',
      "class": 'fa-rotate-90'
    }, {
      id: 'rotate180',
      "class": 'fa-rotate-180'
    }, {
      id: 'rotate270',
      "class": 'fa-rotate-270'
    }, {
      id: 'flipHorizontal',
      "class": 'fa-flip-horizontal'
    }, {
      id: 'flipVertical',
      "class": 'fa-flip-vertical'
    }
  ];

  getPositionId = function(cl) {
    var i, len, p;
    for (i = 0, len = POSITION_CLASSES.length; i < len; i++) {
      p = POSITION_CLASSES[i];
      if (p["class"] === cl) {
        return p.id;
      }
    }
    return null;
  };

  getPositionClass = function(id) {
    var i, len, p;
    for (i = 0, len = POSITION_CLASSES.length; i < len; i++) {
      p = POSITION_CLASSES[i];
      if (p.id === id) {
        return p["class"];
      }
    }
    return null;
  };

  CKEDITOR.plugins.fontawesome = {
    DEFAULT_FA_ICONS_VERSION: DEFAULT_FA_ICONS_VERSION,
    POSITION_CLASSES: POSITION_CLASSES,
    getSelectedElement: function(editor) {
      var range, selectedElement, selection;
      selection = editor.getSelection();
      selectedElement = selection.getSelectedElement();
      if (selectedElement && selectedElement.is('fa-icon')) {
        return selectedElement;
      }
      range = selection.getRanges()[0];
      if (range) {
        range.shrink(CKEDITOR.SHRINK_TEXT);
        return editor.elementPath(range.getCommonAncestor()).contains('fa-icon', 1);
      }
      return null;
    },
    parseAttributes: function(element) {
      var _classes, _styles, cl, i, j, len, len1, pcl, primaryClass, retval;
      _classes = '';
      _styles = {};
      if (element) {
        _classes = element.getAttribute('class') || '';
        _styles = element.style || {};
      }
      _classes = _classes.split(' ');
      retval = {
        position: 'normalPosition',
        iconClass: '',
        size: _styles['font-size'] || '',
        color: _styles.color || ''

        /*
        effects:{
            spinning: false
            fixedWidth: false
            border: false
        }
         */
      };
      primaryClass = 'fa';
      for (i = 0, len = _classes.length; i < len; i++) {
        cl = _classes[i];
        if (!(cl !== 'fa' && cl.indexOf('fa-') === -1 && cl.indexOf('cke_widget_element') === -1)) {
          continue;
        }
        primaryClass = cl;
        break;
      }
      if (_classes.includes('fa')) {
        for (j = 0, len1 = _classes.length; j < len1; j++) {
          cl = _classes[j];
          if (!(cl.indexOf('fa-') === 0)) {
            continue;
          }
          pcl = getPositionId(cl);
          if (pcl) {
            retval.position = pcl;
            continue;
          }

          /*
          if cl == 'fa-spin'
              retval.effects.spinning = true
              continue
          if cl == 'fa-fw'
              retval.effects.fixedWidth = true
              continue
          if cl == 'fa-border'
              retval.effects.border = true
              continue
           */
          retval.iconClass = primaryClass + ' ' + cl;
        }
      }
      return retval;
    }
  };

}).call(this);
