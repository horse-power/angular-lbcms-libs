
/**
 * @license Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */


/**
 * @fileOverview Image plugin based on Widgets API
 */

(function() {
  CKEDITOR.dialog.add('image2', function(editor) {
    var colWidthConfigWidget, commonLang, config, createPreLoader, doc, domHeight, domWidth, features, fileIdPickerWidget, getNatural, hasFileBrowser, heightField, helpers, image, imgResizerConfigWidget, lang, lbcmshelpers, lockButton, lockButtonId, lockRatio, lockResetHtml, lockResetStyle, natural, onChangeDimension, onChangeSrc, onLoadLockReset, preLoadedHeight, preLoadedWidth, preLoader, regexGetSizeOrEmpty, resetButton, resetButtonId, srcChanged, toggleDimensions, toggleLockRatio, userDefinedLock, validateDimension, widget, widthField;
    regexGetSizeOrEmpty = /(^\s*(\d+)(px)?\s*$)|^$/i;
    lockButtonId = CKEDITOR.tools.getNextId();
    resetButtonId = CKEDITOR.tools.getNextId();
    lang = editor.lang.image2;
    commonLang = editor.lang.common;
    lockResetStyle = 'margin-top:18px;width:40px;height:20px;';
    lockResetHtml = new CKEDITOR.template('<div>' + '<a href="javascript:void(0)" tabindex="-1" title="' + lang.lockRatio + '" class="cke_btn_locked" id="{lockButtonId}" role="checkbox">' + '<span class="cke_icon"></span>' + '<span class="cke_label">' + lang.lockRatio + '</span>' + '</a>' + '<a href="javascript:void(0)" tabindex="-1" title="' + lang.resetSize + '" class="cke_btn_reset" id="{resetButtonId}" role="button">' + '<span class="cke_label">' + lang.resetSize + '</span>' + '</a>' + '</div>').output({
      lockButtonId: lockButtonId,
      resetButtonId: resetButtonId
    });
    helpers = CKEDITOR.plugins.image2;
    config = editor.config;
    hasFileBrowser = !!(config.filebrowserImageBrowseUrl || config.filebrowserBrowseUrl);
    features = editor.widgets.registered.image.features;
    getNatural = helpers.getNatural;
    lbcmshelpers = CKEDITOR.plugins.lbcmshelpers(editor);
    doc = void 0;
    widget = void 0;
    image = void 0;
    preLoader = void 0;
    domWidth = void 0;
    domHeight = void 0;
    preLoadedWidth = void 0;
    preLoadedHeight = void 0;
    srcChanged = void 0;
    lockRatio = void 0;
    userDefinedLock = void 0;
    lockButton = void 0;
    resetButton = void 0;
    widthField = void 0;
    heightField = void 0;
    natural = void 0;
    validateDimension = function() {
      var isValid, match;
      match = this.getValue().match(regexGetSizeOrEmpty);
      isValid = !!(match && parseInt(match[1], 10) !== 0);
      if (!isValid) {
        alert(commonLang['invalid' + CKEDITOR.tools.capitalize(this.id)]);
      }
      return isValid;
    };
    createPreLoader = function() {
      var image;
      var addListener, listeners, removeListeners;
      image = doc.createElement('img');
      listeners = [];
      addListener = function(event, callback) {
        listeners.push(image.once(event, function(evt) {
          removeListeners();
          callback(evt);
        }));
      };
      removeListeners = function() {
        var l;
        l = void 0;
        while (l = listeners.pop()) {
          l.removeListener();
        }
      };
      return function(src, callback, scope) {
        addListener('load', function() {
          var dimensions;
          dimensions = getNatural(image);
          callback.call(scope, image, dimensions.width, dimensions.height);
        });
        addListener('error', function() {
          callback(null);
        });
        addListener('abort', function() {
          callback(null);
        });
        image.setAttribute('src', (config.baseHref || '') + src + '?' + Math.random().toString(16).substring(2));
      };
    };
    onChangeSrc = function(value) {
      if (config.image2_disableResizer) {
        return;
      }
      value = value || this.getValue();
      toggleDimensions(false);
      if (value !== widget.data.src) {
        preLoader(value, function(image, width, height) {
          toggleDimensions(true);
          if (!image) {
            return toggleLockRatio(false);
          }
          widthField.setValue(editor.config.image2_prefillDimensions === false ? 0 : width);
          heightField.setValue(editor.config.image2_prefillDimensions === false ? 0 : height);
          preLoadedWidth = width;
          preLoadedHeight = height;
          toggleLockRatio(helpers.checkHasNaturalRatio(image));
        });
        srcChanged = true;
      } else if (srcChanged) {
        toggleDimensions(true);
        widthField.setValue(domWidth);
        heightField.setValue(domHeight);
        srcChanged = false;
      } else {
        toggleDimensions(true);
      }
    };
    onChangeDimension = function() {
      var height, isWidth, value, width;
      if (!lockRatio) {
        return;
      }
      value = this.getValue();
      if (!value) {
        return;
      }
      if (!value.match(regexGetSizeOrEmpty)) {
        toggleLockRatio(false);
      }
      if (value === '0') {
        return;
      }
      isWidth = this.id === 'width';
      width = domWidth || preLoadedWidth;
      height = domHeight || preLoadedHeight;
      if (isWidth) {
        value = Math.round(height * value / width);
      } else {
        value = Math.round(width * value / height);
      }
      if (!isNaN(value)) {
        (isWidth ? heightField : widthField).setValue(value);
      }
    };
    onLoadLockReset = function() {
      var dialog, setupMouseClasses;
      dialog = this.getDialog();
      setupMouseClasses = function(el) {
        el.on('mouseover', (function() {
          this.addClass('cke_btn_over');
        }), el);
        el.on('mouseout', (function() {
          this.removeClass('cke_btn_over');
        }), el);
      };
      lockButton = doc.getById(lockButtonId);
      resetButton = doc.getById(resetButtonId);
      if (lockButton) {
        dialog.addFocusable(lockButton, 4 + hasFileBrowser);
        lockButton.on('click', (function(evt) {
          toggleLockRatio();
          evt.data && evt.data.preventDefault();
        }), this.getDialog());
        setupMouseClasses(lockButton);
      }
      if (resetButton) {
        dialog.addFocusable(resetButton, 5 + hasFileBrowser);
        resetButton.on('click', (function(evt) {
          if (srcChanged) {
            widthField.setValue(preLoadedWidth);
            heightField.setValue(preLoadedHeight);
          } else {
            widthField.setValue(domWidth);
            heightField.setValue(domHeight);
          }
          evt.data && evt.data.preventDefault();
        }), this);
        setupMouseClasses(resetButton);
      }
    };
    toggleLockRatio = function(enable) {
      var height, icon, width;
      if (!lockButton) {
        return;
      }
      if (typeof enable === 'boolean') {
        if (userDefinedLock) {
          return;
        }
        lockRatio = enable;
      } else {
        width = widthField.getValue();
        height = void 0;
        userDefinedLock = true;
        lockRatio = !lockRatio;
        if (lockRatio && width) {
          height = domHeight / domWidth * width;
          if (!isNaN(height)) {
            heightField.setValue(Math.round(height));
          }
        }
      }
      lockButton[lockRatio ? 'removeClass' : 'addClass']('cke_btn_unlocked');
      lockButton.setAttribute('aria-checked', lockRatio);
      if (CKEDITOR.env.hc) {
        icon = lockButton.getChild(0);
        icon.setHtml(lockRatio ? (CKEDITOR.env.ie ? '■' : '▣') : CKEDITOR.env.ie ? '□' : '▢');
      }
    };
    toggleDimensions = function(enable) {
      var method;
      method = enable ? 'enable' : 'disable';
      widthField[method]();
      heightField[method]();
    };
    if (!config.image2_disableResizer) {
      imgResizerConfigWidget = {
        type: 'hbox',
        widths: ['25%', '25%', '50%'],
        requiredContent: features.dimension.requiredContent,
        children: [
          {
            type: 'text',
            width: '45px',
            id: 'width',
            label: commonLang.width,
            validate: validateDimension,
            onKeyUp: onChangeDimension,
            onLoad: function() {
              widthField = this;
            },
            setup: function(widget) {
              this.setValue(widget.data.width);
            },
            commit: function(widget) {
              widget.setData('width', this.getValue());
            }
          }, {
            type: 'text',
            id: 'height',
            width: '45px',
            label: commonLang.height,
            validate: validateDimension,
            onKeyUp: onChangeDimension,
            onLoad: function() {
              heightField = this;
            },
            setup: function(widget) {
              this.setValue(widget.data.height);
            },
            commit: function(widget) {
              widget.setData('height', this.getValue());
            }
          }, {
            id: 'lock',
            type: 'html',
            style: lockResetStyle,
            onLoad: onLoadLockReset,
            setup: function(widget) {
              toggleLockRatio(widget.data.lock);
            },
            commit: function(widget) {
              widget.setData('lock', lockRatio);
            },
            html: lockResetHtml
          }
        ]
      };
    } else {
      imgResizerConfigWidget = {
        type: 'html',
        html: ''
      };
    }

    /*
    {
        id: 'alt'
        type: 'text'
        label: lang.alt
        setup: (widget) ->
            @setValue widget.data.alt
            return
        commit: (widget) ->
            widget.setData 'alt', @getValue()
            return
        validate: if editor.config.image2_altRequired == true then CKEDITOR.dialog.validate.notEmpty(lang.altMissing) else null
    }
     */
    colWidthConfigWidget = lbcmshelpers.configWidgets.getColsWidth();
    fileIdPickerWidget = lbcmshelpers.configWidgets.getFilePickerForFileId({
      accept: 'image/jpg,image/png,image/gif,image/jpeg',
      'allow-preview': true
    }, function(widget, value, filepath) {
      widget.setData('src', filepath);
      return onChangeSrc(filepath);
    }, function(widget, value, filepath) {
      return onChangeSrc(filepath);
    });
    return {
      title: lang.title,
      minWidth: 250,
      minHeight: 100,
      onLoad: function() {
        doc = this._.element.getDocument();
        preLoader = createPreLoader();
      },
      onShow: function() {
        var editorInstance;
        editorInstance = this._.editor;
        widget = this.widget;
        image = widget.parts.image;
        srcChanged = userDefinedLock = lockRatio = false;
        natural = getNatural(image);
        preLoadedWidth = domWidth = natural.width;
        preLoadedHeight = domHeight = natural.height;
      },
      contents: [
        {
          id: 'upload',
          label: lang.infoTab,
          elements: [
            fileIdPickerWidget, {
              type: 'hbox',
              id: 'alignment',
              requiredContent: features.align.requiredContent,
              children: [
                {
                  id: 'align',
                  type: 'radio',
                  items: [[commonLang.alignLeft, 'left'], [commonLang.alignCenter, 'center'], [commonLang.alignRight, 'right']],
                  label: commonLang.align,
                  setup: function(widget) {
                    this.setValue(widget.data.align);
                  },
                  commit: function(widget) {
                    widget.setData('align', this.getValue());
                  }
                }
              ]
            }
          ]
        }, {
          id: 'info',
          label: lang.size,
          elements: [
            {
              type: 'html',
              html: "<div style='font-weight:bold;'>" + lang.sizeInP + "</div>"
            }, colWidthConfigWidget, imgResizerConfigWidget
          ]
        }
      ]
    };
  });

}).call(this);
