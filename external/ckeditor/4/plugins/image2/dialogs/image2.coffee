###*
# @license Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
# For licensing, see LICENSE.md or http://ckeditor.com/license
###

###*
# @fileOverview Image plugin based on Widgets API
###

CKEDITOR.dialog.add 'image2', (editor) ->
    # RegExp: 123, 123px, empty string ""
    regexGetSizeOrEmpty = /(^\s*(\d+)(px)?\s*$)|^$/i
    lockButtonId = CKEDITOR.tools.getNextId()
    resetButtonId = CKEDITOR.tools.getNextId()
    lang = editor.lang.image2
    commonLang = editor.lang.common
    lockResetStyle = 'margin-top:18px;width:40px;height:20px;'
    lockResetHtml = new (CKEDITOR.template)('<div>' + '<a href="javascript:void(0)" tabindex="-1" title="' + lang.lockRatio + '" class="cke_btn_locked" id="{lockButtonId}" role="checkbox">' + '<span class="cke_icon"></span>' + '<span class="cke_label">' + lang.lockRatio + '</span>' + '</a>' + '<a href="javascript:void(0)" tabindex="-1" title="' + lang.resetSize + '" class="cke_btn_reset" id="{resetButtonId}" role="button">' + '<span class="cke_label">' + lang.resetSize + '</span>' + '</a>' + '</div>').output(
        lockButtonId: lockButtonId
        resetButtonId: resetButtonId)
    helpers = CKEDITOR.plugins.image2
    config = editor.config
    hasFileBrowser = ! !(config.filebrowserImageBrowseUrl or config.filebrowserBrowseUrl)
    features = editor.widgets.registered.image.features
    getNatural = helpers.getNatural
    lbcmshelpers = CKEDITOR.plugins.lbcmshelpers(editor)

    doc = undefined
    widget = undefined
    image = undefined
    preLoader = undefined
    domWidth = undefined
    domHeight = undefined
    preLoadedWidth = undefined
    preLoadedHeight = undefined
    srcChanged = undefined
    lockRatio = undefined
    userDefinedLock = undefined
    lockButton = undefined
    resetButton = undefined
    widthField = undefined
    heightField = undefined
    natural = undefined
    # Render the "Browse" button on demand to avoid an "empty" (hidden child)
    # space in dialog layout that distorts the UI.
    # Validates dimension. Allowed values are:
    # "123px", "123", "" (empty string)

    validateDimension = ->
        match = @getValue().match(regexGetSizeOrEmpty)
        isValid = ! !(match and parseInt(match[1], 10) != 0)
        if !isValid
            alert commonLang['invalid' + CKEDITOR.tools.capitalize(@id)]
        # jshint ignore:line
        isValid

    # Creates a function that pre-loads images. The callback function passes
    # [image, width, height] or null if loading failed.
    #
    # @returns {Function}

    createPreLoader = ->
        `var image`
        image = doc.createElement('img')
        listeners = []
        # @param {String} src.
        # @param {Function} callback.

        addListener = (event, callback) ->
            listeners.push image.once(event, (evt) ->
                removeListeners()
                callback evt
                return
            )
            return

        removeListeners = ->
            l = undefined
            while l = listeners.pop()
                l.removeListener()
            return

        (src, callback, scope) ->
            addListener 'load', ->
                # Don't use image.$.(width|height) since it's buggy in IE9-10 (#11159)
                dimensions = getNatural(image)
                callback.call scope, image, dimensions.width, dimensions.height
                return
            addListener 'error', ->
                callback null
                return
            addListener 'abort', ->
                callback null
                return
            image.setAttribute 'src', (config.baseHref or '') + src + '?' + Math.random().toString(16).substring(2)
            return

    # This function updates width and height fields once the
    # "src" field is altered. Along with dimensions, also the
    # dimensions lock is adjusted.

    onChangeSrc = (value) ->
        if config.image2_disableResizer
            return
        value = value or @getValue()
        toggleDimensions false
        # Remember that src is different than default.
        if value != widget.data.src
            # Update dimensions of the image once it's preloaded.
            preLoader value, (image, width, height) ->
                # Re-enable width and height fields.
                toggleDimensions true
                # There was problem loading the image. Unlock ratio.
                if !image
                    return toggleLockRatio(false)
                # Fill width field with the width of the new image.
                widthField.setValue if editor.config.image2_prefillDimensions == false then 0 else width
                # Fill height field with the height of the new image.
                heightField.setValue if editor.config.image2_prefillDimensions == false then 0 else height
                # Cache the new width.
                preLoadedWidth = width
                # Cache the new height.
                preLoadedHeight = height
                # Check for new lock value if image exist.
                toggleLockRatio helpers.checkHasNaturalRatio(image)
                return
            srcChanged = true
        else if srcChanged
            # Re-enable width and height fields.
            toggleDimensions true
            # Restore width field with cached width.
            widthField.setValue domWidth
            # Restore height field with cached height.
            heightField.setValue domHeight
            # Src equals default one back again.
            srcChanged = false
        else
            # Re-enable width and height fields.
            toggleDimensions true
        return

    onChangeDimension = ->
        # If ratio is un-locked, then we don't care what's next.
        if !lockRatio
            return
        value = @getValue()
        # No reason to auto-scale or unlock if the field is empty.
        if !value
            return
        # If the value of the field is invalid (e.g. with %), unlock ratio.
        if !value.match(regexGetSizeOrEmpty)
            toggleLockRatio false
        # No automatic re-scale when dimension is '0'.
        if value == '0'
            return
        isWidth = @id == 'width'
        width = domWidth or preLoadedWidth
        height = domHeight or preLoadedHeight
        # If changing width, then auto-scale height.
        if isWidth
            value = Math.round(height * value / width)
        else
            value = Math.round(width * value / height)
        # If the value is a number, apply it to the other field.
        if !isNaN(value)
            (if isWidth then heightField else widthField).setValue value
        return

    # Set-up function for lock and reset buttons:
    # 	* Adds lock and reset buttons to focusables. Check if button exist first
    # 	    because it may be disabled e.g. due to ACF restrictions.
    # 	* Register mouseover and mouseout event listeners for UI manipulations.
    # 	* Register click event listeners for buttons.

    onLoadLockReset = ->
        dialog = @getDialog()
        # Create references to lock and reset buttons for this dialog instance.

        setupMouseClasses = (el) ->
            el.on 'mouseover', (->
                @addClass 'cke_btn_over'
                return
            ), el
            el.on 'mouseout', (->
                @removeClass 'cke_btn_over'
                return
            ), el
            return

        lockButton = doc.getById(lockButtonId)
        resetButton = doc.getById(resetButtonId)
        # Activate (Un)LockRatio button
        if lockButton
            # Consider that there's an additional focusable field
            # in the dialog when the "browse" button is visible.
            dialog.addFocusable lockButton, 4 + hasFileBrowser
            lockButton.on 'click', ((evt) ->
                toggleLockRatio()
                evt.data and evt.data.preventDefault()
                return
            ), @getDialog()
            setupMouseClasses lockButton
        # Activate the reset size button.
        if resetButton
            # Consider that there's an additional focusable field
            # in the dialog when the "browse" button is visible.
            dialog.addFocusable resetButton, 5 + hasFileBrowser
            # Fills width and height fields with the original dimensions of the
            # image (stored in widget#data since widget#init).
            resetButton.on 'click', ((evt) ->
                # If there's a new image loaded, reset button should revert
                # cached dimensions of pre-loaded DOM element.
                if srcChanged
                    widthField.setValue preLoadedWidth
                    heightField.setValue preLoadedHeight
                else
                    widthField.setValue domWidth
                    heightField.setValue domHeight
                evt.data and evt.data.preventDefault()
                return
            ), this
            setupMouseClasses resetButton
        return

    toggleLockRatio = (enable) ->
        # No locking if there's no radio (i.e. due to ACF).
        if !lockButton
            return
        if typeof enable == 'boolean'
            # If user explicitly wants to decide whether
            # to lock or not, don't do anything.
            if userDefinedLock
                return
            lockRatio = enable
        else
            width = widthField.getValue()
            height = undefined
            userDefinedLock = true
            lockRatio = !lockRatio
            # Automatically adjust height to width to match
            # the original ratio (based on dom- dimensions).
            if lockRatio and width
                height = domHeight / domWidth * width
                if !isNaN(height)
                    heightField.setValue Math.round(height)
        lockButton[if lockRatio then 'removeClass' else 'addClass'] 'cke_btn_unlocked'
        lockButton.setAttribute 'aria-checked', lockRatio
        # Ratio button hc presentation - WHITE SQUARE / BLACK SQUARE
        if CKEDITOR.env.hc
            icon = lockButton.getChild(0)
            icon.setHtml if lockRatio then (if CKEDITOR.env.ie then '■' else '▣') else if CKEDITOR.env.ie then '□' else '▢'
        return

    toggleDimensions = (enable) ->
        method = if enable then 'enable' else 'disable'
        widthField[method]()
        heightField[method]()
        return


    if !config.image2_disableResizer
        imgResizerConfigWidget = {
            type: 'hbox'
            widths: [
                '25%'
                '25%'
                '50%'
            ]
            requiredContent: features.dimension.requiredContent
            children: [
                {
                    type: 'text'
                    width: '45px'
                    id: 'width'
                    label: commonLang.width
                    validate: validateDimension
                    onKeyUp: onChangeDimension
                    onLoad: ->
                        widthField = this
                        return
                    setup: (widget) ->
                        @setValue widget.data.width
                        return
                    commit: (widget) ->
                        widget.setData 'width', @getValue()
                        return

                }
                {
                    type: 'text'
                    id: 'height'
                    width: '45px'
                    label: commonLang.height
                    validate: validateDimension
                    onKeyUp: onChangeDimension
                    onLoad: ->
                        heightField = this
                        return
                    setup: (widget) ->
                        @setValue widget.data.height
                        return
                    commit: (widget) ->
                        widget.setData 'height', @getValue()
                        return

                }
                {
                    id: 'lock'
                    type: 'html'
                    style: lockResetStyle
                    onLoad: onLoadLockReset
                    setup: (widget) ->
                        toggleLockRatio widget.data.lock
                        return
                    commit: (widget) ->
                        widget.setData 'lock', lockRatio
                        return
                    html: lockResetHtml
                }
            ]
        }
    else
        imgResizerConfigWidget = {
            type: 'html'
            html: ''
        }

    ###
    {
        id: 'alt'
        type: 'text'
        label: lang.alt
        setup: (widget) ->
            @setValue widget.data.alt
            return
        commit: (widget) ->
            widget.setData 'alt', @getValue()
            return
        validate: if editor.config.image2_altRequired == true then CKEDITOR.dialog.validate.notEmpty(lang.altMissing) else null
    }
    ###
    colWidthConfigWidget = lbcmshelpers.configWidgets.getColsWidth()
    fileIdPickerWidget = lbcmshelpers.configWidgets.getFilePickerForFileId({
            accept: 'image/jpg,image/png,image/gif,image/jpeg'
            'allow-preview': true
        },
        (widget, value, filepath)->
            widget.setData('src', filepath)
            onChangeSrc(filepath)
        ,
        (widget, value, filepath)->
            onChangeSrc(filepath)
    )

    {
        title: lang.title
        minWidth: 250
        minHeight: 100
        onLoad: ->
            # Create a "global" reference to the document for this dialog instance.
            doc = @_.element.getDocument()
            # Create a pre-loader used for determining dimensions of new images.
            preLoader = createPreLoader()
            return
        onShow: ->
            editorInstance = this._.editor
            # Create a "global" reference to edited widget.
            widget = @widget
            # Create a "global" reference to widget's image.
            image = widget.parts.image
            # Reset global variables.
            srcChanged = userDefinedLock = lockRatio = false
            # Natural dimensions of the image.
            natural = getNatural(image)
            # Get the natural width of the image.
            preLoadedWidth = domWidth = natural.width
            # Get the natural height of the image.
            preLoadedHeight = domHeight = natural.height
            return

        contents: [
            {
                id: 'upload'
                label: lang.infoTab
                elements: [

                    fileIdPickerWidget

                    {
                        type: 'hbox'
                        id: 'alignment'
                        requiredContent: features.align.requiredContent
                        children: [ {
                            id: 'align'
                            type: 'radio'
                            items: [
                                #[
                                #    commonLang.alignNone
                                #    'none'
                                #]
                                [
                                    commonLang.alignLeft
                                    'left'
                                ]
                                [
                                    commonLang.alignCenter
                                    'center'
                                ]
                                [
                                    commonLang.alignRight
                                    'right'
                                ]
                            ]
                            label: commonLang.align
                            setup: (widget) ->
                                @setValue widget.data.align
                                return
                            commit: (widget) ->
                                widget.setData 'align', @getValue()
                                return

                        } ]
                    }
                    # {
                    #    id: 'hasCaption'
                    #    type: 'checkbox'
                    #    label: lang.captioned
                    #    requiredContent: features.caption.requiredContent
                    #    setup: (widget) ->
                    #        @setValue widget.data.hasCaption
                    #        return
                    #    commit: (widget) ->
                    #        widget.setData 'hasCaption', @getValue()
                    #        return
                    # }
                ]
            }
            {
                id: 'info'
                label: lang.size
                elements: [
                    {
                        type: 'html'
                        html: "<div style='font-weight:bold;'>#{lang.sizeInP}</div>"
                    }
                    colWidthConfigWidget
                    imgResizerConfigWidget
                ]
            }
        ]
    }
