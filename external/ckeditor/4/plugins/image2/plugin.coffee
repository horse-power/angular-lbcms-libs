###*
* @license Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
* For licensing, see LICENSE.md or http://ckeditor.com/license
###

'use strict'
do ->
  template = '<img alt="" src="" />'
  templateBlock = new (CKEDITOR.template)('<figure class="{captionedClass}">' + template + '<figcaption class="col-xs-100">{captionPlaceholder}</figcaption>' + '</figure>')
  alignmentsObj =
    left: 0
    center: 1
    right: 2
  regexPercent = /^\s*(\d+\%)\s*$/i
  # Wiget states (forms) depending on alignment and configuration.
  #
  # Non-captioned widget (inline styles)
  # 		┌──────┬───────────────────────────────┬─────────────────────────────┐
  # 		│Align │Internal form                  │Data                         │
  # 		├──────┼───────────────────────────────┼─────────────────────────────┤
  # 		│none  │<wrapper>                      │<img />                      │
  # 		│      │ <img />                       │                             │
  # 		│      │</wrapper>                     │                             │
  # 		├──────┼───────────────────────────────┼─────────────────────────────┤
  # 		│left  │<wrapper style=”float:left”>   │<img style=”float:left” />   │
  # 		│      │ <img />                       │                             │
  # 		│      │</wrapper>                     │                             │
  # 		├──────┼───────────────────────────────┼─────────────────────────────┤
  # 		│center│<wrapper>                      │<p style=”text-align:center”>│
  # 		│      │ <p style=”text-align:center”> │  <img />                    │
  # 		│      │   <img />                     │</p>                         │
  # 		│      │ </p>                          │                             │
  # 		│      │</wrapper>                     │                             │
  # 		├──────┼───────────────────────────────┼─────────────────────────────┤
  # 		│right │<wrapper style=”float:right”>  │<img style=”float:right” />  │
  # 		│      │ <img />                       │                             │
  # 		│      │</wrapper>                     │                             │
  # 		└──────┴───────────────────────────────┴─────────────────────────────┘
  #
  # Non-captioned widget (config.image2_alignClasses defined)
  # 		┌──────┬───────────────────────────────┬─────────────────────────────┐
  # 		│Align │Internal form                  │Data                         │
  # 		├──────┼───────────────────────────────┼─────────────────────────────┤
  # 		│none  │<wrapper>                      │<img />                      │
  # 		│      │ <img />                       │                             │
  # 		│      │</wrapper>                     │                             │
  # 		├──────┼───────────────────────────────┼─────────────────────────────┤
  # 		│left  │<wrapper class=”left”>         │<img class=”left” />         │
  # 		│      │ <img />                       │                             │
  # 		│      │</wrapper>                     │                             │
  # 		├──────┼───────────────────────────────┼─────────────────────────────┤
  # 		│center│<wrapper>                      │<p class=”center”>           │
  # 		│      │ <p class=”center”>            │ <img />                     │
  # 		│      │   <img />                     │</p>                         │
  # 		│      │ </p>                          │                             │
  # 		│      │</wrapper>                     │                             │
  # 		├──────┼───────────────────────────────┼─────────────────────────────┤
  # 		│right │<wrapper class=”right”>        │<img class=”right” />        │
  # 		│      │ <img />                       │                             │
  # 		│      │</wrapper>                     │                             │
  # 		└──────┴───────────────────────────────┴─────────────────────────────┘
  #
  # Captioned widget (inline styles)
  # 		┌──────┬────────────────────────────────────────┬────────────────────────────────────────┐
  # 		│Align │Internal form                           │Data                                    │
  # 		├──────┼────────────────────────────────────────┼────────────────────────────────────────┤
  # 		│none  │<wrapper>                               │<figure />                              │
  # 		│      │ <figure />                             │                                        │
  # 		│      │</wrapper>                              │                                        │
  # 		├──────┼────────────────────────────────────────┼────────────────────────────────────────┤
  # 		│left  │<wrapper style=”float:left”>            │<figure style=”float:left” />           │
  # 		│      │ <figure />                             │                                        │
  # 		│      │</wrapper>                              │                                        │
  # 		├──────┼────────────────────────────────────────┼────────────────────────────────────────┤
  # 		│center│<wrapper style=”text-align:center”>     │<div style=”text-align:center”>         │
  # 		│      │ <figure style=”display:inline-block” />│ <figure style=”display:inline-block” />│
  # 		│      │</wrapper>                              │</p>                                    │
  # 		├──────┼────────────────────────────────────────┼────────────────────────────────────────┤
  # 		│right │<wrapper style=”float:right”>           │<figure style=”float:right” />          │
  # 		│      │ <figure />                             │                                        │
  # 		│      │</wrapper>                              │                                        │
  # 		└──────┴────────────────────────────────────────┴────────────────────────────────────────┘
  #
  # Captioned widget (config.image2_alignClasses defined)
  # 		┌──────┬────────────────────────────────────────┬────────────────────────────────────────┐
  # 		│Align │Internal form                           │Data                                    │
  # 		├──────┼────────────────────────────────────────┼────────────────────────────────────────┤
  # 		│none  │<wrapper>                               │<figure />                              │
  # 		│      │ <figure />                             │                                        │
  # 		│      │</wrapper>                              │                                        │
  # 		├──────┼────────────────────────────────────────┼────────────────────────────────────────┤
  # 		│left  │<wrapper class=”left”>                  │<figure class=”left” />                 │
  # 		│      │ <figure />                             │                                        │
  # 		│      │</wrapper>                              │                                        │
  # 		├──────┼────────────────────────────────────────┼────────────────────────────────────────┤
  # 		│center│<wrapper class=”center”>                │<div class=”center”>                    │
  # 		│      │ <figure />                             │ <figure />                             │
  # 		│      │</wrapper>                              │</p>                                    │
  # 		├──────┼────────────────────────────────────────┼────────────────────────────────────────┤
  # 		│right │<wrapper class=”right”>                 │<figure class=”right” />                │
  # 		│      │ <figure />                             │                                        │
  # 		│      │</wrapper>                              │                                        │
  # 		└──────┴────────────────────────────────────────┴────────────────────────────────────────┘
  #
  # @param {CKEDITOR.editor}
  # @returns {Object}

  widgetDef = (editor) ->
    alignClasses = editor.config.image2_alignClasses
    captionedClass = editor.config.image2_captionedClass
    lbcmshelpers = CKEDITOR.plugins.lbcmshelpers(editor)

    deflate = ->
      if @deflated
        return
      # Remember whether widget was focused before destroyed.
      if editor.widgets.focused == @widget
        @focused = true
      editor.widgets.destroy @widget
      # Mark widget was destroyed.
      @deflated = true
      return

    inflate = ->
      editable = editor.editable()
      doc = editor.document
      # Create a new widget. This widget will be either captioned
      # non-captioned, block or inline according to what is the
      # new state of the widget.
      if @deflated
        @widget = editor.widgets.initOn(@element, 'image', @widget.data)
        # Once widget was re-created, it may become an inline element without
        # block wrapper (i.e. when unaligned, end not captioned). Let's do some
        # sort of autoparagraphing here (#10853).
        if @widget.inline and !new (CKEDITOR.dom.elementPath)(@widget.wrapper, editable).block
          block = doc.createElement(if editor.activeEnterMode == CKEDITOR.ENTER_P then 'p' else 'div')
          block.replace @widget.wrapper
          @widget.wrapper.move block
        # The focus must be transferred from the old one (destroyed)
        # to the new one (just created).
        if @focused
          @widget.focus()
          delete @focused
        delete @deflated
      else
        setWrapperAlign @widget, alignClasses
      return

    {
      allowedContent: getWidgetAllowedContent(editor)
      requiredContent: 'img[src,alt]'
      features: getWidgetFeatures(editor)
      styleableElements: 'img figure'
      contentTransformations: [ [ 'img[width]: sizeToAttribute' ] ]
      editables: caption:
        selector: 'figcaption'
        allowedContent: 'br em strong sub sup u s; a[!href,target]'
      parts:
        image: 'img'
        caption: 'figcaption'
      dialog: 'image2'
      template: template
      data: ->
        features = @features
        # Image can't be captioned when figcaption is disallowed (#11004).
        if @data.hasCaption and !editor.filter.checkFeature(features.caption)
          @data.hasCaption = false
        # Image can't be aligned when floating is disallowed (#11004).
        if @data.align != 'none' and !editor.filter.checkFeature(features.align)
          @data.align = 'left'
        # Convert the internal form of the widget from the old state to the new one.
        @shiftState
          widget: this
          element: @element
          oldData: @oldData
          newData: @data
          deflate: deflate
          inflate: inflate
        # Update widget.parts.link since it will not auto-update unless widget
        # is destroyed and re-inited.
        if !@data.link
          if @parts.link
            delete @parts.link
        else
          if !@parts.link
            @parts.link = @parts.image.getParent()

        @parts.image.setAttributes({
          src: @data.src
          'data-cke-saved-src': @data.src
          alt: @data.alt
          'file-id': @data.fileId
          'uic-img-list': ''
          'file-list': 'item.files'
        })
        # If shifting non-captioned -> captioned, remove classes
        # related to styles from <img/>.
        if @oldData and !@oldData.hasCaption and @data.hasCaption
          for c of @data.classes
            @parts.image.removeClass c
        # Set dimensions of the image according to gathered data.
        # Do it only when the attributes are allowed (#11004).
        if editor.filter.checkFeature(features.dimension)
          setDimensions this

        # очищаем виджет от классов col- и pull-
        allowed_classes = lbcmshelpers.colsGrid.cleanWidgetClasses(this)
        allowed_classes.push("pull-#{@data.align}")

        # устанавливаем классы для виджета согласно @data
        if @data.hasCaption
          lbcmshelpers.colsGrid.cleanWidgetClasses(@parts.image)
          lbcmshelpers.colsGrid.setWidgetClasses(
                @parts.image.$.parentNode,
                @data,
                allowed_classes
            )
        else
          lbcmshelpers.colsGrid.setWidgetClasses(this, @data, allowed_classes)
        # Cache current data.
        @oldData = CKEDITOR.tools.extend({}, @data)
        return

      init: ->
        helpers = CKEDITOR.plugins.image2
        image = @parts.image
        data = {
          hasCaption: ! !@parts.caption
          src: image.getAttribute('src')
          alt: image.getAttribute('alt') or ''
          width: image.getAttribute('width') or ''
          height: image.getAttribute('height') or ''
          fileId: image.getAttribute('file-id') or undefined
          lock: if @ready then helpers.checkHasNaturalRatio(image) else true
          colWidthClass_lg: '40'
          colWidthClass_md: '50'
          colWidthClass_sm: '60'
          colWidthClass_xs: '100'
        }
        if data.hasCaption
          className = @parts.caption.$.parentNode.className or ''
        else
          className = @parts.image.$.className or ''
        for cl in className.split(' ')
            if cl.indexOf('col-') == 0
              [screenSize, colSize] = cl.replace('col-', '').split('-')
              data["colWidthClass_#{screenSize}"] = colSize

        # If we used 'a' in widget#parts definition, it could happen that
        # selected element is a child of widget.parts#caption. Since there's no clever
        # way to solve it with CSS selectors, it's done like that. (#11783).
        link = image.getAscendant('a')
        if link and @wrapper.contains(link)
          @parts.link = link
        # Depending on configuration, read style/class from element and
        # then remove it. Removed style/class will be set on wrapper in #data listener.
        # Note: Center alignment is detected during upcast, so only left/right cases
        # are checked below.
        if !data.align
          alignElement = if data.hasCaption then @element else image
          # Read the initial left/right alignment from the class set on element.
          if alignClasses
            if alignElement.hasClass(alignClasses[0])
              data.align = 'left'
            else if alignElement.hasClass(alignClasses[2])
              data.align = 'right'
            if data.align
              alignElement.removeClass alignClasses[alignmentsObj[data.align]]
            else
              data.align = 'left'
          else
            data.align = alignElement.getStyle('float') or 'left'
            alignElement.removeStyle 'float'
        # Update data.link object with attributes if the link has been discovered.
        if editor.plugins.link and @parts.link
          data.link = helpers.getLinkAttributesParser()(editor, @parts.link)
          # Get rid of cke_widget_* classes in data. Otherwise
          # they might appear in link dialog.
          advanced = data.link.advanced
          if advanced and advanced.advCSSClasses
            advanced.advCSSClasses = CKEDITOR.tools.trim(advanced.advCSSClasses.replace(/cke_\S+/, ''))
        # Get rid of extra vertical space when there's no caption.
        # It will improve the look of the resizer.
        @wrapper[(if data.hasCaption then 'remove' else 'add') + 'Class'] 'cke_image_nocaption'
        @setData data
        # Setup dynamic image resizing with mouse.
        # Don't initialize resizer when dimensions are disallowed (#11004).
        if editor.filter.checkFeature(@features.dimension) and editor.config.image2_disableResizer != true
          setupResizer this
        @shiftState = helpers.stateShifter(@editor)
        # Add widget editing option to its context menu.
        @on 'contextMenu', (evt) ->
          evt.data.image = CKEDITOR.TRISTATE_OFF
          # Integrate context menu items for link.
          # Note that widget may be wrapped in a link, which
          # does not belong to that widget (#11814).
          if @parts.link or @wrapper.getAscendant('a')
            evt.data.link = evt.data.unlink = CKEDITOR.TRISTATE_OFF
          return
        # Pass the reference to this widget to the dialog.
        @on 'dialog', ((evt) ->
          evt.data.widget = this
          return
        ), this
        return
      addClass: (className) ->
        getStyleableElement(this).addClass className
        return
      hasClass: (className) ->
        getStyleableElement(this).hasClass className
      removeClass: (className) ->
        getStyleableElement(this).removeClass className
        return
      getClasses: do ->
        classRegex = new RegExp('^(' + [].concat(captionedClass, alignClasses).join('|') + ')$')
        ->
          classes = @repository.parseElementClasses(getStyleableElement(this).getAttribute('class'))
          # Neither config.image2_captionedClass nor config.image2_alignClasses
          # do not belong to style classes.
          for c of classes
            if classRegex.test(c)
              delete classes[c]
          classes
      upcast: upcastWidgetElement(editor)
      downcast: downcastWidgetElement(editor)
      getLabel: ->
        label = (@data.alt or '') + ' ' + @pathName
        @editor.lang.widget.label.replace /%1/, label

    }

  setWrapperAlign = (widget, alignClasses) ->
    wrapper = widget.wrapper
    align = widget.data.align
    hasCaption = widget.data.hasCaption
    wrapper.removeStyle('float')
    widget.parts.image.addClass('pull-'+ align)
    ###
    #  этот хак из оригинального плагина работает бажно
    if alignClasses
      # Remove all align classes first.
      i = 3
      while i--
        wrapper.removeClass alignClasses[i]
      if align == 'center'
        # Avoid touching non-captioned, centered widgets because
        # they have the class set on the element instead of wrapper:
        #
        # 	<div class="cke_widget_wrapper">
        # 		<p class="center-class">
        # 			<img />
        # 		</p>
        # 	</div>
        if hasCaption
          wrapper.addClass alignClasses[1]
      else if align != 'none'
        wrapper.addClass alignClasses[alignmentsObj[align]]
    else
      if align == 'center'
        if hasCaption
          wrapper.setStyle 'text-align', 'center'
        else
          wrapper.removeStyle 'text-align'
        wrapper.removeStyle 'float'
      else
        if align == 'none'
          wrapper.removeStyle 'float'
        else
          wrapper.setStyle 'float', align
        wrapper.removeStyle 'text-align'
    ###
    return

  # Returns a function that creates widgets from all <img> and
  # <figure class="{config.image2_captionedClass}"> elements.
  #
  # @param {CKEDITOR.editor} editor
  # @returns {Function}

  upcastWidgetElement = (editor) ->
    isCenterWrapper = centerWrapperChecker(editor)
    captionedClass = editor.config.image2_captionedClass
    # @param {CKEDITOR.htmlParser.element} el
    # @param {Object} data
    (el, data) ->
      dimensions =
        width: 1
        height: 1
      name = el.name
      image = undefined
      # #11110 Don't initialize on pasted fake objects.
      if el.attributes['data-cke-realelement']
        return
      # If a center wrapper is found, there are 3 possible cases:
      #
      # 1. <div style="text-align:center"><figure>...</figure></div>.
      #    In this case centering is done with a class set on widget.wrapper.
      #    Simply replace centering wrapper with figure (it's no longer necessary).
      #
      # 2. <p style="text-align:center"><img/></p>.
      #    Nothing to do here: <p> remains for styling purposes.
      #
      # 3. <div style="text-align:center"><img/></div>.
      #    Nothing to do here (2.) but that case is only possible in enterMode different
      #    than ENTER_P.
      if isCenterWrapper(el)
        if name == 'div'
          figure = el.getFirst('figure')
          # Case #1.
          if figure
            el.replaceWith figure
            el = figure
        # Cases #2 and #3 (handled transparently)
        # If there's a centering wrapper, save it in data.
        data.align = 'center'
        # Image can be wrapped in link <a><img/></a>.
        image = el.getFirst('img') or el.getFirst('a').getFirst('img')
      else if name == 'figure' and el.hasClass(captionedClass)
        image = el.getFirst('img') or el.getFirst('a').getFirst('img')
        # Upcast linked image like <a><img/></a>.
      else if isLinkedOrStandaloneImage(el)
        image = if el.name == 'a' then el.children[0] else el
      if !image
        return
      # If there's an image, then cool, we got a widget.
      # Now just remove dimension attributes expressed with %.
      for d of dimensions
        dimension = image.attributes[d]
        if dimension and dimension.match(regexPercent)
          delete image.attributes[d]
      el

  # Returns a function which transforms the widget to the external format
  # according to the current configuration.
  #
  # @param {CKEDITOR.editor}

  downcastWidgetElement = (editor) ->
    alignClasses = editor.config.image2_alignClasses
    # @param {CKEDITOR.htmlParser.element} el
    (el) ->
      # In case of <a><img/></a>, <img/> is the element to hold
      # inline styles or classes (image2_alignClasses).
      attrsHolder = if el.name == 'a' then el.getFirst() else el
      attrs = attrsHolder.attributes
      align = @data.align
      # De-wrap the image from resize handle wrapper.
      # Only block widgets have one.
      if !@inline
        resizeWrapper = el.getFirst('span')
        if resizeWrapper
          resizeWrapper.replaceWith resizeWrapper.getFirst(
            img: 1
            a: 1)
      if align and align != 'none'
        styles = CKEDITOR.tools.parseCssText(attrs.style or '')
        # When the widget is captioned (<figure>) and internally centering is done
        # with widget's wrapper style/class, in the external data representation,
        # <figure> must be wrapped with an element holding an style/class:
        #
        # 	<div style="text-align:center">
        # 		<figure class="image" style="display:inline-block">...</figure>
        # 	</div>
        # or
        # 	<div class="some-center-class">
        # 		<figure class="image">...</figure>
        # 	</div>
        #
        if align == 'center' and el.name == 'figure'
          el = el.wrapWith(new (CKEDITOR.htmlParser.element)('div', if alignClasses then 'class': alignClasses[1] else style: 'text-align:center'))
        else if align of {left: 1, right: 1}
          if alignClasses
            attrsHolder.addClass alignClasses[alignmentsObj[align]]
          else
            styles['float'] = align
        # Update element styles.
        if !alignClasses and !CKEDITOR.tools.isEmpty(styles)
          attrs.style = CKEDITOR.tools.writeCssText(styles)
      el

  # Returns a function that checks if an element is a centering wrapper.
  #
  # @param {CKEDITOR.editor} editor
  # @returns {Function}

  centerWrapperChecker = (editor) ->
    captionedClass = editor.config.image2_captionedClass
    alignClasses = editor.config.image2_alignClasses
    validChildren =
      figure: 1
      a: 1
      img: 1
    (el) ->
      # Wrapper must be either <div> or <p>.
      if !(el.name of
          div: 1
          p: 1)
        return false
      children = el.children
      # Centering wrapper can have only one child.
      if children.length != 1
        return false
      child = children[0]
      # Only <figure> or <img /> can be first (only) child of centering wrapper,
      # regardless of its type.
      if !(child.name of validChildren)
        return false
      # If centering wrapper is <p>, only <img /> can be the child.
      #   <p style="text-align:center"><img /></p>
      if el.name == 'p'
        if !isLinkedOrStandaloneImage(child)
          return false
      else
        # If a <figure> is the first (only) child, it must have a class.
        #   <div style="text-align:center"><figure>...</figure><div>
        if child.name == 'figure'
          if !child.hasClass(captionedClass)
            return false
        else
          # Centering <div> can hold <img/> or <a><img/></a> only when enterMode
          # is ENTER_(BR|DIV).
          #   <div style="text-align:center"><img /></div>
          #   <div style="text-align:center"><a><img /></a></div>
          if editor.enterMode == CKEDITOR.ENTER_P
            return false
          # Regardless of enterMode, a child which is not <figure> must be
          # either <img/> or <a><img/></a>.
          if !isLinkedOrStandaloneImage(child)
            return false
      # Centering wrapper got to be... centering. If image2_alignClasses are defined,
      # check for centering class. Otherwise, check the style.
      if (if alignClasses then el.hasClass(alignClasses[1]) else CKEDITOR.tools.parseCssText(el.attributes.style or '', true)['text-align'] == 'center')
        return true
      false

  # Checks whether element is <img/> or <a><img/></a>.
  #
  # @param {CKEDITOR.htmlParser.element}

  isLinkedOrStandaloneImage = (el) ->
    if el.name == 'img'
      return true
    else if el.name == 'a'
      return el.children.length == 1 and el.getFirst('img')
    false

  # Sets width and height of the widget image according to current widget data.
  #
  # @param {CKEDITOR.plugins.widget} widget

  setDimensions = (widget) ->
    data = widget.data
    dimensions =
      width: data.width
      height: data.height
    image = widget.parts.image
    for d of dimensions
      if dimensions[d]
        image.setAttribute d, dimensions[d]
      else
        image.removeAttribute d
    return

  # Defines all features related to drag-driven image resizing.
  #
  # @param {CKEDITOR.plugins.widget} widget

  setupResizer = (widget) ->
    editor = widget.editor
    editable = editor.editable()
    doc = editor.document
    resizer = widget.resizer = doc.createElement('span')
    resizer.addClass 'cke_image_resizer'
    resizer.setAttribute 'title', editor.lang.image2.resizer
    resizer.append new (CKEDITOR.dom.text)('\u200b', doc)
    # Inline widgets don't need a resizer wrapper as an image spans the entire widget.
    if !widget.inline
      imageOrLink = widget.parts.link or widget.parts.image
      oldResizeWrapper = imageOrLink.getParent()
      resizeWrapper = doc.createElement('span')
      resizeWrapper.addClass 'cke_image_resizer_wrapper'
      resizeWrapper.append imageOrLink
      resizeWrapper.append resizer
      widget.element.append resizeWrapper, true
      # Remove the old wrapper which could came from e.g. pasted HTML
      # and which could be corrupted (e.g. resizer span has been lost).
      if oldResizeWrapper.is('span')
        oldResizeWrapper.remove()
    else
      widget.wrapper.append resizer
    # Calculate values of size variables and mouse offsets.
    resizer.on 'mousedown', (evt) ->
      image = widget.parts.image
      factor = if widget.data.align == 'right' then -1 else 1
      startX = evt.data.$.screenX
      startY = evt.data.$.screenY
      startWidth = image.$.clientWidth
      startHeight = image.$.clientHeight
      ratio = startWidth / startHeight
      listeners = []
      cursorClass = 'cke_image_s' + (if ! ~factor then 'w' else 'e')
      nativeEvt = undefined
      newWidth = undefined
      newHeight = undefined
      updateData = undefined
      moveDiffX = undefined
      moveDiffY = undefined
      moveRatio = undefined
      # Save the undo snapshot first: before resizing.
      # Attaches an event to a global document if inline editor.
      # Additionally, if classic (`iframe`-based) editor, also attaches the same event to `iframe`'s document.

      attachToDocuments = (name, callback, collection) ->
        `var listeners`
        globalDoc = CKEDITOR.document
        listeners = []
        if !doc.equals(globalDoc)
          listeners.push globalDoc.on(name, callback)
        listeners.push doc.on(name, callback)
        if collection
          i = listeners.length
          while i--
            collection.push listeners.pop()
        return

      # Calculate with first, and then adjust height, preserving ratio.

      adjustToX = ->
        newWidth = startWidth + factor * moveDiffX
        newHeight = Math.round(newWidth / ratio)
        return

      # Calculate height first, and then adjust width, preserving ratio.

      adjustToY = ->
        newHeight = startHeight - moveDiffY
        newWidth = Math.round(newHeight * ratio)
        return

      # This is how variables refer to the geometry.
      # Note: x corresponds to moveOffset, this is the position of mouse
      # Note: o corresponds to [startX, startY].
      #
      # 	+--------------+--------------+
      # 	|              |              |
      # 	|      I       |      II      |
      # 	|              |              |
      # 	+------------- o -------------+ _ _ _
      # 	|              |              |      ^
      # 	|      VI      |     III      |      | moveDiffY
      # 	|              |         x _ _ _ _ _ v
      # 	+--------------+---------|----+
      # 	               |         |
      # 	                <------->
      # 	                moveDiffX

      onMouseMove = (evt) ->
        nativeEvt = evt.data.$
        # This is how far the mouse is from the point the button was pressed.
        moveDiffX = nativeEvt.screenX - startX
        moveDiffY = startY - (nativeEvt.screenY)
        # This is the aspect ratio of the move difference.
        moveRatio = Math.abs(moveDiffX / moveDiffY)
        # Left, center or none-aligned widget.
        if factor == 1
          if moveDiffX <= 0
            # Case: IV.
            if moveDiffY <= 0
              adjustToX()
            else
              if moveRatio >= ratio
                adjustToX()
              else
                adjustToY()
          else
            # Case: III.
            if moveDiffY <= 0
              if moveRatio >= ratio
                adjustToY()
              else
                adjustToX()
            else
              adjustToY()
        else
          if moveDiffX <= 0
            # Case: IV.
            if moveDiffY <= 0
              if moveRatio >= ratio
                adjustToY()
              else
                adjustToX()
            else
              adjustToY()
          else
            # Case: III.
            if moveDiffY <= 0
              adjustToX()
            else
              if moveRatio >= ratio
                adjustToX()
              else
                adjustToY()
        # Don't update attributes if less than 10.
        # This is to prevent images to visually disappear.
        if newWidth >= 15 and newHeight >= 15
          image.setAttributes
            width: newWidth
            height: newHeight
          updateData = true
        else
          updateData = false
        return

      onMouseUp = ->
        l = undefined
        while l = listeners.pop()
          l.removeListener()
        # Restore default cursor by removing special class.
        editable.removeClass cursorClass
        # This is to bring back the regular behaviour of the resizer.
        resizer.removeClass 'cke_image_resizing'
        if updateData
          widget.setData
            width: newWidth
            height: newHeight
          # Save another undo snapshot: after resizing.
          editor.fire 'saveSnapshot'
        # Don't update data twice or more.
        updateData = false
        return

      editor.fire 'saveSnapshot'
      # Mousemove listeners are removed on mouseup.
      attachToDocuments 'mousemove', onMouseMove, listeners
      # Clean up the mousemove listener. Update widget data if valid.
      attachToDocuments 'mouseup', onMouseUp, listeners
      # The entire editable will have the special cursor while resizing goes on.
      editable.addClass cursorClass
      # This is to always keep the resizer element visible while resizing.
      resizer.addClass 'cke_image_resizing'
      return
    # Change the position of the widget resizer when data changes.
    widget.on 'data', ->
      resizer[if widget.data.align == 'right' then 'addClass' else 'removeClass'] 'cke_image_resizer_left'
      return
    return

  # Integrates widget alignment setting with justify
  # plugin's commands (execution and refreshment).
  # @param {CKEDITOR.editor} editor
  # @param {String} value 'left', 'right', 'center' or 'block'

  alignCommandIntegrator = (editor) ->
    execCallbacks = []
    enabled = undefined
    (value) ->
      command = editor.getCommand('justify' + value)
      # Most likely, the justify plugin isn't loaded.
      if !command
        return
      # This command will be manually refreshed along with
      # other commands after exec.
      execCallbacks.push ->
        command.refresh editor, editor.elementPath()
        return
      if value of {right: 1, left: 1, center: 1}
        command.on 'exec', (evt) ->
          widget = getFocusedWidget(editor)
          if widget
            widget.setData 'align', value
            # Once the widget changed its align, all the align commands
            # must be refreshed: the event is to be cancelled.
            i = execCallbacks.length
            while i--
              execCallbacks[i]()
            evt.cancel()
          return
      command.on 'refresh', (evt) ->
        widget = getFocusedWidget(editor)
        allowed =
          right: 1
          left: 1
          center: 1
        if !widget
          return
        # Cache "enabled" on first use. This is because filter#checkFeature may
        # not be available during plugin's afterInit in the future — a moment when
        # alignCommandIntegrator is called.
        if enabled == undefined
          enabled = editor.filter.checkFeature(editor.widgets.registered.image.features.align)
        # Don't allow justify commands when widget alignment is disabled (#11004).
        if !enabled
          @setState CKEDITOR.TRISTATE_DISABLED
        else
          @setState if widget.data.align == value then CKEDITOR.TRISTATE_ON else if value of allowed then CKEDITOR.TRISTATE_OFF else CKEDITOR.TRISTATE_DISABLED
        evt.cancel()
        return
      return

  linkCommandIntegrator = (editor) ->
    # Nothing to integrate with if link is not loaded.
    if !editor.plugins.link
      return
    CKEDITOR.on 'dialogDefinition', (evt) ->
      dialog = evt.data
      if dialog.name == 'link'
        def = dialog.definition
        onShow = def.onShow
        onOk = def.onOk

        def.onShow = ->
          widget = getFocusedWidget(editor)
          displayTextField = @getContentElement('info', 'linkDisplayText').getElement().getParent().getParent()
          # Widget cannot be enclosed in a link, i.e.
          #		<a>foo<inline widget/>bar</a>
          if widget and (if widget.inline then !widget.wrapper.getAscendant('a') else 1)
            @setupContent widget.data.link or {}
            # Hide the display text in case of linking image2 widget.
            displayTextField.hide()
          else
            # Make sure that display text is visible, as it might be hidden by image2 integration
            # before.
            displayTextField.show()
            onShow.apply this, arguments
          return

        # Set widget data if linking the widget using
        # link dialog (instead of default action).
        # State shifter handles data change and takes
        # care of internal DOM structure of linked widget.

        def.onOk = ->
          widget = getFocusedWidget(editor)
          # Widget cannot be enclosed in a link, i.e.
          #		<a>foo<inline widget/>bar</a>
          if widget and (if widget.inline then !widget.wrapper.getAscendant('a') else 1)
            data = {}
            # Collect data from fields.
            @commitContent data
            # Set collected data to widget.
            widget.setData 'link', data
          else
            onOk.apply this, arguments
          return

      return
    # Overwrite default behaviour of unlink command.
    editor.getCommand('unlink').on 'exec', (evt) ->
      widget = getFocusedWidget(editor)
      # Override unlink only when link truly belongs to the widget.
      # If wrapped inline widget in a link, let default unlink work (#11814).
      if !widget or !widget.parts.link
        return
      widget.setData 'link', null
      # Selection (which is fake) may not change if unlinked image in focused widget,
      # i.e. if captioned image. Let's refresh command state manually here.
      @refresh editor, editor.elementPath()
      evt.cancel()
      return
    # Overwrite default refresh of unlink command.
    editor.getCommand('unlink').on 'refresh', (evt) ->
      widget = getFocusedWidget(editor)
      if !widget
        return
      # Note that widget may be wrapped in a link, which
      # does not belong to that widget (#11814).
      @setState if widget.data.link or widget.wrapper.getAscendant('a') then CKEDITOR.TRISTATE_OFF else CKEDITOR.TRISTATE_DISABLED
      evt.cancel()
      return
    return

  # Returns the focused widget, if of the type specific for this plugin.
  # If no widget is focused, `null` is returned.
  #
  # @param {CKEDITOR.editor}
  # @returns {CKEDITOR.plugins.widget}

  getFocusedWidget = (editor) ->
    widget = editor.widgets.focused
    if widget and widget.name == 'image'
      return widget
    null

  # Returns a set of widget allowedContent rules, depending
  # on configurations like config#image2_alignClasses or
  # config#image2_captionedClass.
  #
  # @param {CKEDITOR.editor}
  # @returns {Object}

  getWidgetAllowedContent = (editor) ->
    alignClasses = editor.config.image2_alignClasses
    rules =
      div: match: centerWrapperChecker(editor)
      p: match: centerWrapperChecker(editor)
      img: attributes: '!src,alt,width,height,file-id,file-list,uic-img-list'
      figure: classes: '!' + editor.config.image2_captionedClass
      figcaption: true
    if alignClasses
      # Centering class from the config.
      rules.div.classes = alignClasses[1]
      rules.p.classes = rules.div.classes
      # Left/right classes from the config.
      rules.img.classes = alignClasses[0] + ',' + alignClasses[2]
      rules.figure.classes += ',' + rules.img.classes
    else
      # Centering with text-align.
      rules.div.styles = 'text-align,text-indent,padding'
      rules.p.styles = 'text-align,text-indent,padding'
      rules.img.styles = 'float'
      rules.figure.styles = 'float,display'
    rules

  # Returns a set of widget feature rules, depending
  # on editor configuration. Note that the following may not cover
  # all the possible cases since requiredContent supports a single
  # tag only.
  #
  # @param {CKEDITOR.editor}
  # @returns {Object}

  getWidgetFeatures = (editor) ->
    alignClasses = editor.config.image2_alignClasses
    features =
      dimension: requiredContent: 'img[width,height]'
      align: requiredContent: 'img' + (if alignClasses then '(' + alignClasses[0] + ')' else '{float}')
      caption: requiredContent: 'figcaption'
    features

  # Returns element which is styled, considering current
  # state of the widget.
  #
  # @see CKEDITOR.plugins.widget#applyStyle
  # @param {CKEDITOR.plugins.widget} widget
  # @returns {CKEDITOR.dom.element}

  getStyleableElement = (widget) ->
    if widget.data.hasCaption then widget.element else widget.parts.image

  CKEDITOR.plugins.add 'image2',
    lang: 'af,ar,az,bg,bn,bs,ca,cs,cy,da,de,de-ch,el,en,en-au,en-ca,en-gb,eo,es,et,eu,fa,fi,fo,fr,fr-ca,gl,gu,he,hi,hr,hu,id,is,it,ja,ka,km,ko,ku,lt,lv,mk,mn,ms,nb,nl,no,oc,pl,pt,pt-br,ro,ru,si,sk,sl,sq,sr,sr-latn,sv,th,tr,tt,ug,uk,vi,zh,zh-cn'
    requires: 'widget,dialog,lbcmshelpers'
    icons: 'image'
    hidpi: true
    onLoad: ->
      CKEDITOR.addCss("""
            .cke_image_nocaption{
                line-height:0;
                display:block;
            }
            .cke_widget_wrapper.cke_widget_inline.cke_widget_image{
                width: 100%;
            }
            .cke_editable.cke_image_sw, .cke_editable.cke_image_sw *{
                cursor:sw-resize !important;
            }
            .cke_editable.cke_image_se, .cke_editable.cke_image_se *{
                cursor:se-resize !important;
            }
            .cke_image_resizer{
                display:none;
                position:absolute;
                width:10px;
                height:10px;
                bottom:-5px;
                right:-5px;
                background:#000;
                outline:1px solid #fff;
                line-height:0;
                cursor:se-resize;
            }
            .cke_image_resizer_wrapper{
                position:relative;
                display:inline-block;
                line-height:0;
            }
            .cke_image_resizer.cke_image_resizer_left{
                right:auto;
                left:-5px;
                cursor:sw-resize;
            }
            .cke_widget_wrapper:hover .cke_image_resizer, .cke_image_resizer.cke_image_resizing{
                display:block;
            }
            .cke_widget_wrapper>a{
                display:inline-block;
            }""")
      return
    init: (editor) ->
      # Adapts configuration from original image plugin. Should be removed
      # when we'll rename image2 to image.
      config = editor.config
      lang = editor.lang.image2
      image = widgetDef(editor)
      # Since filebrowser plugin discovers config properties by dialog (plugin?)
      # names (sic!), this hack will be necessary as long as Image2 is not named
      # Image. And since Image2 will never be Image, for sure some filebrowser logic
      # got to be refined.
      config.filebrowserImage2BrowseUrl = config.filebrowserImageBrowseUrl
      config.filebrowserImage2UploadUrl = config.filebrowserImageUploadUrl
      # Add custom elementspath names to widget definition.
      image.pathName = lang.pathName
      image.editables.caption.pathName = lang.pathNameCaption
      # Register the widget.
      editor.widgets.add 'image', image
      # Add toolbar button for this plugin.
      editor.ui.addButton and editor.ui.addButton('Image',
        label: editor.lang.common.image
        command: 'image'
        toolbar: 'insert,10')
      # Register context menu option for editing widget.
      if editor.contextMenu
        editor.addMenuGroup 'image', 10
        editor.addMenuItem 'image',
          label: lang.menu
          command: 'image'
          group: 'image'
      CKEDITOR.dialog.add 'image2', @path + 'dialogs/image2.js'
      return
    afterInit: (editor) ->
      # Integrate with align commands (justify plugin).
      align =
        left: 1
        right: 1
        center: 1
        block: 1
      integrate = alignCommandIntegrator(editor)
      for value of align
        integrate value
      # Integrate with link commands (link plugin).
      linkCommandIntegrator editor
      return

  ###*
  # A set of Enhanced Image (image2) plugin helpers.
  #
  # @class
  # @singleton
  ###

  CKEDITOR.plugins.image2 =
    stateShifter: (editor) ->
      # Tag name used for centering non-captioned widgets.
      doc = editor.document
      alignClasses = editor.config.image2_alignClasses
      captionedClass = editor.config.image2_captionedClass
      editable = editor.editable()
      shiftables = [
        'hasCaption'
        'align'
        'link'
      ]
      # Atomic procedures, one per state variable.
      stateActions =
        align: (shift, oldValue, newValue) ->
          el = shift.element
          # Alignment changed.
          if shift.changed.align
            # No caption in the new state.
            if !shift.newData.hasCaption
              # Changed to "center" (non-captioned).
              if newValue == 'center'
                shift.deflate()
                shift.element = wrapInCentering(editor, el)
              # Changed to "non-center" from "center" while caption removed.
              if !shift.changed.hasCaption and oldValue == 'center' and newValue != 'center'
                shift.deflate()
                shift.element = unwrapFromCentering(el)
          else if newValue == 'center' and shift.changed.hasCaption and !shift.newData.hasCaption
            shift.deflate()
            shift.element = wrapInCentering(editor, el)
          # Finally set display for figure.
          if !alignClasses and el.is('figure')
            if newValue == 'center'
              el.setStyle 'display', 'inline-block'
            else
              el.removeStyle 'display'
          return
        hasCaption: (shift, oldValue, newValue) ->
          # This action is for real state change only.
          if !shift.changed.hasCaption
            return
          # Get <img/> or <a><img/></a> from widget. Note that widget element might itself
          # be what we're looking for. Also element can be <p style="text-align:center"><a>...</a></p>.
          imageOrLink = undefined
          if shift.element.is(
              img: 1
              a: 1)
            imageOrLink = shift.element
          else
            imageOrLink = shift.element.findOne('a,img')
          # Switching hasCaption always destroys the widget.
          shift.deflate()
          # There was no caption, but the caption is to be added.
          if newValue
            # Create new <figure> from widget template.
            figure = CKEDITOR.dom.element.createFromHtml(templateBlock.output(
              captionedClass: captionedClass
              captionPlaceholder: editor.lang.image2.captionPlaceholder), doc)
            # Replace element with <figure>.
            replaceSafely figure, shift.element
            # Use old <img/> or <a><img/></a> instead of the one from the template,
            # so we won't lose additional attributes.
            imageOrLink.replace figure.findOne('img')
            # Update widget's element.
            shift.element = figure
          else
            # Unwrap <img/> or <a><img/></a> from figure.
            imageOrLink.replace shift.element
            # Update widget's element.
            shift.element = imageOrLink
          return
        link: (shift, oldValue, newValue) ->
          if shift.changed.link
            img = if shift.element.is('img') then shift.element else shift.element.findOne('img')
            link = if shift.element.is('a') then shift.element else shift.element.findOne('a')
            needsDeflate = shift.element.is('a') and !newValue or shift.element.is('img') and newValue
            newEl = undefined
            if needsDeflate
              shift.deflate()
            # If unlinked the image, returned element is <img>.
            if !newValue
              newEl = unwrapFromLink(link)
            else
              # If linked the image, returned element is <a>.
              if !oldValue
                newEl = wrapInLink(img, shift.newData.link)
              # Set and remove all attributes associated with this state.
              attributes = CKEDITOR.plugins.image2.getLinkAttributesGetter()(editor, newValue)
              if !CKEDITOR.tools.isEmpty(attributes.set)
                (newEl or link).setAttributes attributes.set
              if attributes.removed.length
                (newEl or link).removeAttributes attributes.removed
            if needsDeflate
              shift.element = newEl
          return

      wrapInCentering = (editor, element) ->
        attribsAndStyles = {styles: {}}
        if alignClasses
          attribsAndStyles.attributes = 'class': alignClasses[1]
        else
          attribsAndStyles.styles = 'text-align': 'center'
        # т.к. запаковывается картинка в тег p или div, нужно быть уверенным,
        # что у тега нет отступов и паддинга
        attribsAndStyles.styles['text-indent'] = '0'
        attribsAndStyles.styles.padding = '0'
        # There's no gentle way to center inline element with CSS, so create p/div
        # that wraps widget contents and does the trick either with style or class.
        tag = if editor.activeEnterMode == CKEDITOR.ENTER_P then 'p' else 'div'
        center = doc.createElement(tag, attribsAndStyles)
        # Replace element with centering wrapper.
        replaceSafely center, element
        element.move center
        center

      unwrapFromCentering = (element) ->
        imageOrLink = element.findOne('a,img')
        imageOrLink.replace element
        imageOrLink

      # Wraps <img/> -> <a><img/></a>.
      # Returns reference to <a>.
      #
      # @param {CKEDITOR.dom.element} img
      # @param {Object} linkData
      # @returns {CKEDITOR.dom.element}

      wrapInLink = (img, linkData) ->
        link = doc.createElement('a', attributes: href: linkData.url)
        link.replace img
        img.move link
        link

      # De-wraps <a><img/></a> -> <img/>.
      # Returns the reference to <img/>
      #
      # @param {CKEDITOR.dom.element} link
      # @returns {CKEDITOR.dom.element}

      unwrapFromLink = (link) ->
        img = link.findOne('img')
        img.replace link
        img

      replaceSafely = (replacing, replaced) ->
        if replaced.getParent()
          range = editor.createRange()
          range.moveToPosition replaced, CKEDITOR.POSITION_BEFORE_START
          # Remove old element. Do it before insertion to avoid a case when
          # element is moved from 'replaced' element before it, what creates
          # a tricky case which insertElementIntorRange does not handle.
          replaced.remove()
          editable.insertElementIntoRange replacing, range
        else
          replacing.replace replaced
        return

      (shift) ->
        name = undefined
        i = undefined
        shift.changed = {}
        i = 0
        while i < shiftables.length
          name = shiftables[i]
          shift.changed[name] = if shift.oldData then shift.oldData[name] != shift.newData[name] else false
          i++
        # Iterate over possible state variables.
        i = 0
        while i < shiftables.length
          name = shiftables[i]
          data = if shift.oldData then shift.oldData[name] else null
          stateActions[name](shift, data, shift.newData[name])
          i++
        shift.inflate()
        return
    checkHasNaturalRatio: (image) ->
      $ = image.$
      natural = @getNatural(image)
      # The reason for two alternative comparisons is that the rounding can come from
      # both dimensions, e.g. there are two cases:
      # 	1. height is computed as a rounded relation of the real height and the value of width,
      #	2. width is computed as a rounded relation of the real width and the value of heigh.
      Math.round($.clientWidth / natural.width * natural.height) == $.clientHeight or Math.round($.clientHeight / natural.height * natural.width) == $.clientWidth
    getNatural: (image) ->
      dimensions = undefined
      if image.$.naturalWidth
        dimensions =
          width: image.$.naturalWidth
          height: image.$.naturalHeight
      else
        img = new Image
        img.src = image.getAttribute('src')
        dimensions =
          width: img.width
          height: img.height
      dimensions
    getLinkAttributesGetter: ->
      # #13885
      CKEDITOR.plugins.link.getLinkAttributes
    getLinkAttributesParser: ->
      # #13885
      CKEDITOR.plugins.link.parseLinkAttributes
  return

###*
# A CSS class applied to the `<figure>` element of a captioned image.
#
# Read more in the [documentation](#!/guide/dev_captionedimage) and see the
# [SDK sample](http://sdk.ckeditor.com/samples/captionedimage.html).
#
#		// Changes the class to "captionedImage".
#		config.image2_captionedClass = 'captionedImage';
#
# @cfg {String} [image2_captionedClass='image']
# @member CKEDITOR.config
###

CKEDITOR.config.image2_captionedClass = 'image'

###*
# Determines whether dimension inputs should be automatically filled when the image URL changes in the Enhanced Image
# plugin dialog window.
#
# Read more in the [documentation](#!/guide/dev_captionedimage) and see the
# [SDK sample](http://sdk.ckeditor.com/samples/captionedimage.html).
#
#		config.image2_prefillDimensions = false;
#
# @since 4.5
# @cfg {Boolean} [image2_prefillDimensions=true]
# @member CKEDITOR.config
###

###*
# Disables the image resizer. By default the resizer is enabled.
#
# Read more in the [documentation](#!/guide/dev_captionedimage) and see the
# [SDK sample](http://sdk.ckeditor.com/samples/captionedimage.html).
#
#		config.image2_disableResizer = true;
#
# @since 4.5
# @cfg {Boolean} [image2_disableResizer=false]
# @member CKEDITOR.config
###

###*
# CSS classes applied to aligned images. Useful to take control over the way
# the images are aligned, i.e. to customize output HTML and integrate external stylesheets.
#
# Classes should be defined in an array of three elements, containing left, center, and right
# alignment classes, respectively. For example:
#
#		config.image2_alignClasses = [ 'align-left', 'align-center', 'align-right' ];
#
# **Note**: Once this configuration option is set, the plugin will no longer produce inline
# styles for alignment. It means that e.g. the following HTML will be produced:
#
#		<img alt="My image" class="custom-center-class" src="foo.png" />
#
# instead of:
#
#		<img alt="My image" style="float:left" src="foo.png" />
#
# **Note**: Once this configuration option is set, corresponding style definitions
# must be supplied to the editor:
#
# * For [classic editor](#!/guide/dev_framed) it can be done by defining additional
# styles in the {@link CKEDITOR.config#contentsCss stylesheets loaded by the editor}. The same
# styles must be provided on the target page where the content will be loaded.
# * For [inline editor](#!/guide/dev_inline) the styles can be defined directly
# with `<style> ... <style>` or `<link href="..." rel="stylesheet">`, i.e. within the `<head>`
# of the page.
#
# For example, considering the following configuration:
#
#		config.image2_alignClasses = [ 'align-left', 'align-center', 'align-right' ];
#
# CSS rules can be defined as follows:
#
#		.align-left {
#			float: left;
#		}
#
#		.align-right {
#			float: right;
#		}
#
#		.align-center {
#			text-align: center;
#		}
#
#		.align-center > figure {
#			display: inline-block;
#		}
#
# Read more in the [documentation](#!/guide/dev_captionedimage) and see the
# [SDK sample](http://sdk.ckeditor.com/samples/captionedimage.html).
#
# @since 4.4
# @cfg {String[]} [image2_alignClasses=null]
# @member CKEDITOR.config
###

###*
# Determines whether alternative text is required for the captioned image.
#
#		config.image2_altRequired = true;
#
# Read more in the [documentation](#!/guide/dev_captionedimage) and see the
# [SDK sample](http://sdk.ckeditor.com/samples/captionedimage.html).
#
# @since 4.6.0
# @cfg {Boolean} [image2_altRequired=false]
# @member CKEDITOR.config
###

# ---
# generated by js2coffee 2.2.0
