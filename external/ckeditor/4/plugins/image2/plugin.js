
/**
* @license Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
* For licensing, see LICENSE.md or http://ckeditor.com/license
 */

(function() {
  'use strict';
  (function() {
    var alignCommandIntegrator, alignmentsObj, centerWrapperChecker, downcastWidgetElement, getFocusedWidget, getStyleableElement, getWidgetAllowedContent, getWidgetFeatures, isLinkedOrStandaloneImage, linkCommandIntegrator, regexPercent, setDimensions, setWrapperAlign, setupResizer, template, templateBlock, upcastWidgetElement, widgetDef;
    template = '<img alt="" src="" />';
    templateBlock = new CKEDITOR.template('<figure class="{captionedClass}">' + template + '<figcaption class="col-xs-100">{captionPlaceholder}</figcaption>' + '</figure>');
    alignmentsObj = {
      left: 0,
      center: 1,
      right: 2
    };
    regexPercent = /^\s*(\d+\%)\s*$/i;
    widgetDef = function(editor) {
      var alignClasses, captionedClass, deflate, inflate, lbcmshelpers;
      alignClasses = editor.config.image2_alignClasses;
      captionedClass = editor.config.image2_captionedClass;
      lbcmshelpers = CKEDITOR.plugins.lbcmshelpers(editor);
      deflate = function() {
        if (this.deflated) {
          return;
        }
        if (editor.widgets.focused === this.widget) {
          this.focused = true;
        }
        editor.widgets.destroy(this.widget);
        this.deflated = true;
      };
      inflate = function() {
        var block, doc, editable;
        editable = editor.editable();
        doc = editor.document;
        if (this.deflated) {
          this.widget = editor.widgets.initOn(this.element, 'image', this.widget.data);
          if (this.widget.inline && !new CKEDITOR.dom.elementPath(this.widget.wrapper, editable).block) {
            block = doc.createElement(editor.activeEnterMode === CKEDITOR.ENTER_P ? 'p' : 'div');
            block.replace(this.widget.wrapper);
            this.widget.wrapper.move(block);
          }
          if (this.focused) {
            this.widget.focus();
            delete this.focused;
          }
          delete this.deflated;
        } else {
          setWrapperAlign(this.widget, alignClasses);
        }
      };
      return {
        allowedContent: getWidgetAllowedContent(editor),
        requiredContent: 'img[src,alt]',
        features: getWidgetFeatures(editor),
        styleableElements: 'img figure',
        contentTransformations: [['img[width]: sizeToAttribute']],
        editables: {
          caption: {
            selector: 'figcaption',
            allowedContent: 'br em strong sub sup u s; a[!href,target]'
          }
        },
        parts: {
          image: 'img',
          caption: 'figcaption'
        },
        dialog: 'image2',
        template: template,
        data: function() {
          var allowed_classes, c, features;
          features = this.features;
          if (this.data.hasCaption && !editor.filter.checkFeature(features.caption)) {
            this.data.hasCaption = false;
          }
          if (this.data.align !== 'none' && !editor.filter.checkFeature(features.align)) {
            this.data.align = 'left';
          }
          this.shiftState({
            widget: this,
            element: this.element,
            oldData: this.oldData,
            newData: this.data,
            deflate: deflate,
            inflate: inflate
          });
          if (!this.data.link) {
            if (this.parts.link) {
              delete this.parts.link;
            }
          } else {
            if (!this.parts.link) {
              this.parts.link = this.parts.image.getParent();
            }
          }
          this.parts.image.setAttributes({
            src: this.data.src,
            'data-cke-saved-src': this.data.src,
            alt: this.data.alt,
            'file-id': this.data.fileId,
            'uic-img-list': '',
            'file-list': 'item.files'
          });
          if (this.oldData && !this.oldData.hasCaption && this.data.hasCaption) {
            for (c in this.data.classes) {
              this.parts.image.removeClass(c);
            }
          }
          if (editor.filter.checkFeature(features.dimension)) {
            setDimensions(this);
          }
          allowed_classes = lbcmshelpers.colsGrid.cleanWidgetClasses(this);
          allowed_classes.push("pull-" + this.data.align);
          if (this.data.hasCaption) {
            lbcmshelpers.colsGrid.cleanWidgetClasses(this.parts.image);
            lbcmshelpers.colsGrid.setWidgetClasses(this.parts.image.$.parentNode, this.data, allowed_classes);
          } else {
            lbcmshelpers.colsGrid.setWidgetClasses(this, this.data, allowed_classes);
          }
          this.oldData = CKEDITOR.tools.extend({}, this.data);
        },
        init: function() {
          var advanced, alignElement, cl, className, colSize, data, helpers, image, j, len, link, ref, ref1, screenSize;
          helpers = CKEDITOR.plugins.image2;
          image = this.parts.image;
          data = {
            hasCaption: !!this.parts.caption,
            src: image.getAttribute('src'),
            alt: image.getAttribute('alt') || '',
            width: image.getAttribute('width') || '',
            height: image.getAttribute('height') || '',
            fileId: image.getAttribute('file-id') || void 0,
            lock: this.ready ? helpers.checkHasNaturalRatio(image) : true,
            colWidthClass_lg: '40',
            colWidthClass_md: '50',
            colWidthClass_sm: '60',
            colWidthClass_xs: '100'
          };
          if (data.hasCaption) {
            className = this.parts.caption.$.parentNode.className || '';
          } else {
            className = this.parts.image.$.className || '';
          }
          ref = className.split(' ');
          for (j = 0, len = ref.length; j < len; j++) {
            cl = ref[j];
            if (cl.indexOf('col-') === 0) {
              ref1 = cl.replace('col-', '').split('-'), screenSize = ref1[0], colSize = ref1[1];
              data["colWidthClass_" + screenSize] = colSize;
            }
          }
          link = image.getAscendant('a');
          if (link && this.wrapper.contains(link)) {
            this.parts.link = link;
          }
          if (!data.align) {
            alignElement = data.hasCaption ? this.element : image;
            if (alignClasses) {
              if (alignElement.hasClass(alignClasses[0])) {
                data.align = 'left';
              } else if (alignElement.hasClass(alignClasses[2])) {
                data.align = 'right';
              }
              if (data.align) {
                alignElement.removeClass(alignClasses[alignmentsObj[data.align]]);
              } else {
                data.align = 'left';
              }
            } else {
              data.align = alignElement.getStyle('float') || 'left';
              alignElement.removeStyle('float');
            }
          }
          if (editor.plugins.link && this.parts.link) {
            data.link = helpers.getLinkAttributesParser()(editor, this.parts.link);
            advanced = data.link.advanced;
            if (advanced && advanced.advCSSClasses) {
              advanced.advCSSClasses = CKEDITOR.tools.trim(advanced.advCSSClasses.replace(/cke_\S+/, ''));
            }
          }
          this.wrapper[(data.hasCaption ? 'remove' : 'add') + 'Class']('cke_image_nocaption');
          this.setData(data);
          if (editor.filter.checkFeature(this.features.dimension) && editor.config.image2_disableResizer !== true) {
            setupResizer(this);
          }
          this.shiftState = helpers.stateShifter(this.editor);
          this.on('contextMenu', function(evt) {
            evt.data.image = CKEDITOR.TRISTATE_OFF;
            if (this.parts.link || this.wrapper.getAscendant('a')) {
              evt.data.link = evt.data.unlink = CKEDITOR.TRISTATE_OFF;
            }
          });
          this.on('dialog', (function(evt) {
            evt.data.widget = this;
          }), this);
        },
        addClass: function(className) {
          getStyleableElement(this).addClass(className);
        },
        hasClass: function(className) {
          return getStyleableElement(this).hasClass(className);
        },
        removeClass: function(className) {
          getStyleableElement(this).removeClass(className);
        },
        getClasses: (function() {
          var classRegex;
          classRegex = new RegExp('^(' + [].concat(captionedClass, alignClasses).join('|') + ')$');
          return function() {
            var c, classes;
            classes = this.repository.parseElementClasses(getStyleableElement(this).getAttribute('class'));
            for (c in classes) {
              if (classRegex.test(c)) {
                delete classes[c];
              }
            }
            return classes;
          };
        })(),
        upcast: upcastWidgetElement(editor),
        downcast: downcastWidgetElement(editor),
        getLabel: function() {
          var label;
          label = (this.data.alt || '') + ' ' + this.pathName;
          return this.editor.lang.widget.label.replace(/%1/, label);
        }
      };
    };
    setWrapperAlign = function(widget, alignClasses) {
      var align, hasCaption, wrapper;
      wrapper = widget.wrapper;
      align = widget.data.align;
      hasCaption = widget.data.hasCaption;
      wrapper.removeStyle('float');
      widget.parts.image.addClass('pull-' + align);

      /*
       *  этот хак из оригинального плагина работает бажно
      if alignClasses
         * Remove all align classes first.
        i = 3
        while i--
          wrapper.removeClass alignClasses[i]
        if align == 'center'
           * Avoid touching non-captioned, centered widgets because
           * they have the class set on the element instead of wrapper:
           *
           * 	<div class="cke_widget_wrapper">
           * 		<p class="center-class">
           * 			<img />
           * 		</p>
           * 	</div>
          if hasCaption
            wrapper.addClass alignClasses[1]
        else if align != 'none'
          wrapper.addClass alignClasses[alignmentsObj[align]]
      else
        if align == 'center'
          if hasCaption
            wrapper.setStyle 'text-align', 'center'
          else
            wrapper.removeStyle 'text-align'
          wrapper.removeStyle 'float'
        else
          if align == 'none'
            wrapper.removeStyle 'float'
          else
            wrapper.setStyle 'float', align
          wrapper.removeStyle 'text-align'
       */
    };
    upcastWidgetElement = function(editor) {
      var captionedClass, isCenterWrapper;
      isCenterWrapper = centerWrapperChecker(editor);
      captionedClass = editor.config.image2_captionedClass;
      return function(el, data) {
        var d, dimension, dimensions, figure, image, name;
        dimensions = {
          width: 1,
          height: 1
        };
        name = el.name;
        image = void 0;
        if (el.attributes['data-cke-realelement']) {
          return;
        }
        if (isCenterWrapper(el)) {
          if (name === 'div') {
            figure = el.getFirst('figure');
            if (figure) {
              el.replaceWith(figure);
              el = figure;
            }
          }
          data.align = 'center';
          image = el.getFirst('img') || el.getFirst('a').getFirst('img');
        } else if (name === 'figure' && el.hasClass(captionedClass)) {
          image = el.getFirst('img') || el.getFirst('a').getFirst('img');
        } else if (isLinkedOrStandaloneImage(el)) {
          image = el.name === 'a' ? el.children[0] : el;
        }
        if (!image) {
          return;
        }
        for (d in dimensions) {
          dimension = image.attributes[d];
          if (dimension && dimension.match(regexPercent)) {
            delete image.attributes[d];
          }
        }
        return el;
      };
    };
    downcastWidgetElement = function(editor) {
      var alignClasses;
      alignClasses = editor.config.image2_alignClasses;
      return function(el) {
        var align, attrs, attrsHolder, resizeWrapper, styles;
        attrsHolder = el.name === 'a' ? el.getFirst() : el;
        attrs = attrsHolder.attributes;
        align = this.data.align;
        if (!this.inline) {
          resizeWrapper = el.getFirst('span');
          if (resizeWrapper) {
            resizeWrapper.replaceWith(resizeWrapper.getFirst({
              img: 1,
              a: 1
            }));
          }
        }
        if (align && align !== 'none') {
          styles = CKEDITOR.tools.parseCssText(attrs.style || '');
          if (align === 'center' && el.name === 'figure') {
            el = el.wrapWith(new CKEDITOR.htmlParser.element('div', alignClasses ? {
              'class': alignClasses[1]
            } : {
              style: 'text-align:center'
            }));
          } else if (align in {
            left: 1,
            right: 1
          }) {
            if (alignClasses) {
              attrsHolder.addClass(alignClasses[alignmentsObj[align]]);
            } else {
              styles['float'] = align;
            }
          }
          if (!alignClasses && !CKEDITOR.tools.isEmpty(styles)) {
            attrs.style = CKEDITOR.tools.writeCssText(styles);
          }
        }
        return el;
      };
    };
    centerWrapperChecker = function(editor) {
      var alignClasses, captionedClass, validChildren;
      captionedClass = editor.config.image2_captionedClass;
      alignClasses = editor.config.image2_alignClasses;
      validChildren = {
        figure: 1,
        a: 1,
        img: 1
      };
      return function(el) {
        var child, children;
        if (!(el.name in {
          div: 1,
          p: 1
        })) {
          return false;
        }
        children = el.children;
        if (children.length !== 1) {
          return false;
        }
        child = children[0];
        if (!(child.name in validChildren)) {
          return false;
        }
        if (el.name === 'p') {
          if (!isLinkedOrStandaloneImage(child)) {
            return false;
          }
        } else {
          if (child.name === 'figure') {
            if (!child.hasClass(captionedClass)) {
              return false;
            }
          } else {
            if (editor.enterMode === CKEDITOR.ENTER_P) {
              return false;
            }
            if (!isLinkedOrStandaloneImage(child)) {
              return false;
            }
          }
        }
        if ((alignClasses ? el.hasClass(alignClasses[1]) : CKEDITOR.tools.parseCssText(el.attributes.style || '', true)['text-align'] === 'center')) {
          return true;
        }
        return false;
      };
    };
    isLinkedOrStandaloneImage = function(el) {
      if (el.name === 'img') {
        return true;
      } else if (el.name === 'a') {
        return el.children.length === 1 && el.getFirst('img');
      }
      return false;
    };
    setDimensions = function(widget) {
      var d, data, dimensions, image;
      data = widget.data;
      dimensions = {
        width: data.width,
        height: data.height
      };
      image = widget.parts.image;
      for (d in dimensions) {
        if (dimensions[d]) {
          image.setAttribute(d, dimensions[d]);
        } else {
          image.removeAttribute(d);
        }
      }
    };
    setupResizer = function(widget) {
      var doc, editable, editor, imageOrLink, oldResizeWrapper, resizeWrapper, resizer;
      editor = widget.editor;
      editable = editor.editable();
      doc = editor.document;
      resizer = widget.resizer = doc.createElement('span');
      resizer.addClass('cke_image_resizer');
      resizer.setAttribute('title', editor.lang.image2.resizer);
      resizer.append(new CKEDITOR.dom.text('\u200b', doc));
      if (!widget.inline) {
        imageOrLink = widget.parts.link || widget.parts.image;
        oldResizeWrapper = imageOrLink.getParent();
        resizeWrapper = doc.createElement('span');
        resizeWrapper.addClass('cke_image_resizer_wrapper');
        resizeWrapper.append(imageOrLink);
        resizeWrapper.append(resizer);
        widget.element.append(resizeWrapper, true);
        if (oldResizeWrapper.is('span')) {
          oldResizeWrapper.remove();
        }
      } else {
        widget.wrapper.append(resizer);
      }
      resizer.on('mousedown', function(evt) {
        var adjustToX, adjustToY, attachToDocuments, cursorClass, factor, image, listeners, moveDiffX, moveDiffY, moveRatio, nativeEvt, newHeight, newWidth, onMouseMove, onMouseUp, ratio, startHeight, startWidth, startX, startY, updateData;
        image = widget.parts.image;
        factor = widget.data.align === 'right' ? -1 : 1;
        startX = evt.data.$.screenX;
        startY = evt.data.$.screenY;
        startWidth = image.$.clientWidth;
        startHeight = image.$.clientHeight;
        ratio = startWidth / startHeight;
        listeners = [];
        cursorClass = 'cke_image_s' + (!~factor ? 'w' : 'e');
        nativeEvt = void 0;
        newWidth = void 0;
        newHeight = void 0;
        updateData = void 0;
        moveDiffX = void 0;
        moveDiffY = void 0;
        moveRatio = void 0;
        attachToDocuments = function(name, callback, collection) {
          var listeners;
          var globalDoc, i;
          globalDoc = CKEDITOR.document;
          listeners = [];
          if (!doc.equals(globalDoc)) {
            listeners.push(globalDoc.on(name, callback));
          }
          listeners.push(doc.on(name, callback));
          if (collection) {
            i = listeners.length;
            while (i--) {
              collection.push(listeners.pop());
            }
          }
        };
        adjustToX = function() {
          newWidth = startWidth + factor * moveDiffX;
          newHeight = Math.round(newWidth / ratio);
        };
        adjustToY = function() {
          newHeight = startHeight - moveDiffY;
          newWidth = Math.round(newHeight * ratio);
        };
        onMouseMove = function(evt) {
          nativeEvt = evt.data.$;
          moveDiffX = nativeEvt.screenX - startX;
          moveDiffY = startY - nativeEvt.screenY;
          moveRatio = Math.abs(moveDiffX / moveDiffY);
          if (factor === 1) {
            if (moveDiffX <= 0) {
              if (moveDiffY <= 0) {
                adjustToX();
              } else {
                if (moveRatio >= ratio) {
                  adjustToX();
                } else {
                  adjustToY();
                }
              }
            } else {
              if (moveDiffY <= 0) {
                if (moveRatio >= ratio) {
                  adjustToY();
                } else {
                  adjustToX();
                }
              } else {
                adjustToY();
              }
            }
          } else {
            if (moveDiffX <= 0) {
              if (moveDiffY <= 0) {
                if (moveRatio >= ratio) {
                  adjustToY();
                } else {
                  adjustToX();
                }
              } else {
                adjustToY();
              }
            } else {
              if (moveDiffY <= 0) {
                adjustToX();
              } else {
                if (moveRatio >= ratio) {
                  adjustToX();
                } else {
                  adjustToY();
                }
              }
            }
          }
          if (newWidth >= 15 && newHeight >= 15) {
            image.setAttributes({
              width: newWidth,
              height: newHeight
            });
            updateData = true;
          } else {
            updateData = false;
          }
        };
        onMouseUp = function() {
          var l;
          l = void 0;
          while (l = listeners.pop()) {
            l.removeListener();
          }
          editable.removeClass(cursorClass);
          resizer.removeClass('cke_image_resizing');
          if (updateData) {
            widget.setData({
              width: newWidth,
              height: newHeight
            });
            editor.fire('saveSnapshot');
          }
          updateData = false;
        };
        editor.fire('saveSnapshot');
        attachToDocuments('mousemove', onMouseMove, listeners);
        attachToDocuments('mouseup', onMouseUp, listeners);
        editable.addClass(cursorClass);
        resizer.addClass('cke_image_resizing');
      });
      widget.on('data', function() {
        resizer[widget.data.align === 'right' ? 'addClass' : 'removeClass']('cke_image_resizer_left');
      });
    };
    alignCommandIntegrator = function(editor) {
      var enabled, execCallbacks;
      execCallbacks = [];
      enabled = void 0;
      return function(value) {
        var command;
        command = editor.getCommand('justify' + value);
        if (!command) {
          return;
        }
        execCallbacks.push(function() {
          command.refresh(editor, editor.elementPath());
        });
        if (value in {
          right: 1,
          left: 1,
          center: 1
        }) {
          command.on('exec', function(evt) {
            var i, widget;
            widget = getFocusedWidget(editor);
            if (widget) {
              widget.setData('align', value);
              i = execCallbacks.length;
              while (i--) {
                execCallbacks[i]();
              }
              evt.cancel();
            }
          });
        }
        command.on('refresh', function(evt) {
          var allowed, widget;
          widget = getFocusedWidget(editor);
          allowed = {
            right: 1,
            left: 1,
            center: 1
          };
          if (!widget) {
            return;
          }
          if (enabled === void 0) {
            enabled = editor.filter.checkFeature(editor.widgets.registered.image.features.align);
          }
          if (!enabled) {
            this.setState(CKEDITOR.TRISTATE_DISABLED);
          } else {
            this.setState(widget.data.align === value ? CKEDITOR.TRISTATE_ON : value in allowed ? CKEDITOR.TRISTATE_OFF : CKEDITOR.TRISTATE_DISABLED);
          }
          evt.cancel();
        });
      };
    };
    linkCommandIntegrator = function(editor) {
      if (!editor.plugins.link) {
        return;
      }
      CKEDITOR.on('dialogDefinition', function(evt) {
        var def, dialog, onOk, onShow;
        dialog = evt.data;
        if (dialog.name === 'link') {
          def = dialog.definition;
          onShow = def.onShow;
          onOk = def.onOk;
          def.onShow = function() {
            var displayTextField, widget;
            widget = getFocusedWidget(editor);
            displayTextField = this.getContentElement('info', 'linkDisplayText').getElement().getParent().getParent();
            if (widget && (widget.inline ? !widget.wrapper.getAscendant('a') : 1)) {
              this.setupContent(widget.data.link || {});
              displayTextField.hide();
            } else {
              displayTextField.show();
              onShow.apply(this, arguments);
            }
          };
          def.onOk = function() {
            var data, widget;
            widget = getFocusedWidget(editor);
            if (widget && (widget.inline ? !widget.wrapper.getAscendant('a') : 1)) {
              data = {};
              this.commitContent(data);
              widget.setData('link', data);
            } else {
              onOk.apply(this, arguments);
            }
          };
        }
      });
      editor.getCommand('unlink').on('exec', function(evt) {
        var widget;
        widget = getFocusedWidget(editor);
        if (!widget || !widget.parts.link) {
          return;
        }
        widget.setData('link', null);
        this.refresh(editor, editor.elementPath());
        evt.cancel();
      });
      editor.getCommand('unlink').on('refresh', function(evt) {
        var widget;
        widget = getFocusedWidget(editor);
        if (!widget) {
          return;
        }
        this.setState(widget.data.link || widget.wrapper.getAscendant('a') ? CKEDITOR.TRISTATE_OFF : CKEDITOR.TRISTATE_DISABLED);
        evt.cancel();
      });
    };
    getFocusedWidget = function(editor) {
      var widget;
      widget = editor.widgets.focused;
      if (widget && widget.name === 'image') {
        return widget;
      }
      return null;
    };
    getWidgetAllowedContent = function(editor) {
      var alignClasses, rules;
      alignClasses = editor.config.image2_alignClasses;
      rules = {
        div: {
          match: centerWrapperChecker(editor)
        },
        p: {
          match: centerWrapperChecker(editor)
        },
        img: {
          attributes: '!src,alt,width,height,file-id,file-list,uic-img-list'
        },
        figure: {
          classes: '!' + editor.config.image2_captionedClass
        },
        figcaption: true
      };
      if (alignClasses) {
        rules.div.classes = alignClasses[1];
        rules.p.classes = rules.div.classes;
        rules.img.classes = alignClasses[0] + ',' + alignClasses[2];
        rules.figure.classes += ',' + rules.img.classes;
      } else {
        rules.div.styles = 'text-align,text-indent,padding';
        rules.p.styles = 'text-align,text-indent,padding';
        rules.img.styles = 'float';
        rules.figure.styles = 'float,display';
      }
      return rules;
    };
    getWidgetFeatures = function(editor) {
      var alignClasses, features;
      alignClasses = editor.config.image2_alignClasses;
      features = {
        dimension: {
          requiredContent: 'img[width,height]'
        },
        align: {
          requiredContent: 'img' + (alignClasses ? '(' + alignClasses[0] + ')' : '{float}')
        },
        caption: {
          requiredContent: 'figcaption'
        }
      };
      return features;
    };
    getStyleableElement = function(widget) {
      if (widget.data.hasCaption) {
        return widget.element;
      } else {
        return widget.parts.image;
      }
    };
    CKEDITOR.plugins.add('image2', {
      lang: 'af,ar,az,bg,bn,bs,ca,cs,cy,da,de,de-ch,el,en,en-au,en-ca,en-gb,eo,es,et,eu,fa,fi,fo,fr,fr-ca,gl,gu,he,hi,hr,hu,id,is,it,ja,ka,km,ko,ku,lt,lv,mk,mn,ms,nb,nl,no,oc,pl,pt,pt-br,ro,ru,si,sk,sl,sq,sr,sr-latn,sv,th,tr,tt,ug,uk,vi,zh,zh-cn',
      requires: 'widget,dialog,lbcmshelpers',
      icons: 'image',
      hidpi: true,
      onLoad: function() {
        CKEDITOR.addCss(".cke_image_nocaption{\n    line-height:0;\n    display:block;\n}\n.cke_widget_wrapper.cke_widget_inline.cke_widget_image{\n    width: 100%;\n}\n.cke_editable.cke_image_sw, .cke_editable.cke_image_sw *{\n    cursor:sw-resize !important;\n}\n.cke_editable.cke_image_se, .cke_editable.cke_image_se *{\n    cursor:se-resize !important;\n}\n.cke_image_resizer{\n    display:none;\n    position:absolute;\n    width:10px;\n    height:10px;\n    bottom:-5px;\n    right:-5px;\n    background:#000;\n    outline:1px solid #fff;\n    line-height:0;\n    cursor:se-resize;\n}\n.cke_image_resizer_wrapper{\n    position:relative;\n    display:inline-block;\n    line-height:0;\n}\n.cke_image_resizer.cke_image_resizer_left{\n    right:auto;\n    left:-5px;\n    cursor:sw-resize;\n}\n.cke_widget_wrapper:hover .cke_image_resizer, .cke_image_resizer.cke_image_resizing{\n    display:block;\n}\n.cke_widget_wrapper>a{\n    display:inline-block;\n}");
      },
      init: function(editor) {
        var config, image, lang;
        config = editor.config;
        lang = editor.lang.image2;
        image = widgetDef(editor);
        config.filebrowserImage2BrowseUrl = config.filebrowserImageBrowseUrl;
        config.filebrowserImage2UploadUrl = config.filebrowserImageUploadUrl;
        image.pathName = lang.pathName;
        image.editables.caption.pathName = lang.pathNameCaption;
        editor.widgets.add('image', image);
        editor.ui.addButton && editor.ui.addButton('Image', {
          label: editor.lang.common.image,
          command: 'image',
          toolbar: 'insert,10'
        });
        if (editor.contextMenu) {
          editor.addMenuGroup('image', 10);
          editor.addMenuItem('image', {
            label: lang.menu,
            command: 'image',
            group: 'image'
          });
        }
        CKEDITOR.dialog.add('image2', this.path + 'dialogs/image2.js');
      },
      afterInit: function(editor) {
        var align, integrate, value;
        align = {
          left: 1,
          right: 1,
          center: 1,
          block: 1
        };
        integrate = alignCommandIntegrator(editor);
        for (value in align) {
          integrate(value);
        }
        linkCommandIntegrator(editor);
      }
    });

    /**
     * A set of Enhanced Image (image2) plugin helpers.
     *
     * @class
     * @singleton
     */
    CKEDITOR.plugins.image2 = {
      stateShifter: function(editor) {
        var alignClasses, captionedClass, doc, editable, replaceSafely, shiftables, stateActions, unwrapFromCentering, unwrapFromLink, wrapInCentering, wrapInLink;
        doc = editor.document;
        alignClasses = editor.config.image2_alignClasses;
        captionedClass = editor.config.image2_captionedClass;
        editable = editor.editable();
        shiftables = ['hasCaption', 'align', 'link'];
        stateActions = {
          align: function(shift, oldValue, newValue) {
            var el;
            el = shift.element;
            if (shift.changed.align) {
              if (!shift.newData.hasCaption) {
                if (newValue === 'center') {
                  shift.deflate();
                  shift.element = wrapInCentering(editor, el);
                }
                if (!shift.changed.hasCaption && oldValue === 'center' && newValue !== 'center') {
                  shift.deflate();
                  shift.element = unwrapFromCentering(el);
                }
              }
            } else if (newValue === 'center' && shift.changed.hasCaption && !shift.newData.hasCaption) {
              shift.deflate();
              shift.element = wrapInCentering(editor, el);
            }
            if (!alignClasses && el.is('figure')) {
              if (newValue === 'center') {
                el.setStyle('display', 'inline-block');
              } else {
                el.removeStyle('display');
              }
            }
          },
          hasCaption: function(shift, oldValue, newValue) {
            var figure, imageOrLink;
            if (!shift.changed.hasCaption) {
              return;
            }
            imageOrLink = void 0;
            if (shift.element.is({
              img: 1,
              a: 1
            })) {
              imageOrLink = shift.element;
            } else {
              imageOrLink = shift.element.findOne('a,img');
            }
            shift.deflate();
            if (newValue) {
              figure = CKEDITOR.dom.element.createFromHtml(templateBlock.output({
                captionedClass: captionedClass,
                captionPlaceholder: editor.lang.image2.captionPlaceholder
              }), doc);
              replaceSafely(figure, shift.element);
              imageOrLink.replace(figure.findOne('img'));
              shift.element = figure;
            } else {
              imageOrLink.replace(shift.element);
              shift.element = imageOrLink;
            }
          },
          link: function(shift, oldValue, newValue) {
            var attributes, img, link, needsDeflate, newEl;
            if (shift.changed.link) {
              img = shift.element.is('img') ? shift.element : shift.element.findOne('img');
              link = shift.element.is('a') ? shift.element : shift.element.findOne('a');
              needsDeflate = shift.element.is('a') && !newValue || shift.element.is('img') && newValue;
              newEl = void 0;
              if (needsDeflate) {
                shift.deflate();
              }
              if (!newValue) {
                newEl = unwrapFromLink(link);
              } else {
                if (!oldValue) {
                  newEl = wrapInLink(img, shift.newData.link);
                }
                attributes = CKEDITOR.plugins.image2.getLinkAttributesGetter()(editor, newValue);
                if (!CKEDITOR.tools.isEmpty(attributes.set)) {
                  (newEl || link).setAttributes(attributes.set);
                }
                if (attributes.removed.length) {
                  (newEl || link).removeAttributes(attributes.removed);
                }
              }
              if (needsDeflate) {
                shift.element = newEl;
              }
            }
          }
        };
        wrapInCentering = function(editor, element) {
          var attribsAndStyles, center, tag;
          attribsAndStyles = {
            styles: {}
          };
          if (alignClasses) {
            attribsAndStyles.attributes = {
              'class': alignClasses[1]
            };
          } else {
            attribsAndStyles.styles = {
              'text-align': 'center'
            };
          }
          attribsAndStyles.styles['text-indent'] = '0';
          attribsAndStyles.styles.padding = '0';
          tag = editor.activeEnterMode === CKEDITOR.ENTER_P ? 'p' : 'div';
          center = doc.createElement(tag, attribsAndStyles);
          replaceSafely(center, element);
          element.move(center);
          return center;
        };
        unwrapFromCentering = function(element) {
          var imageOrLink;
          imageOrLink = element.findOne('a,img');
          imageOrLink.replace(element);
          return imageOrLink;
        };
        wrapInLink = function(img, linkData) {
          var link;
          link = doc.createElement('a', {
            attributes: {
              href: linkData.url
            }
          });
          link.replace(img);
          img.move(link);
          return link;
        };
        unwrapFromLink = function(link) {
          var img;
          img = link.findOne('img');
          img.replace(link);
          return img;
        };
        replaceSafely = function(replacing, replaced) {
          var range;
          if (replaced.getParent()) {
            range = editor.createRange();
            range.moveToPosition(replaced, CKEDITOR.POSITION_BEFORE_START);
            replaced.remove();
            editable.insertElementIntoRange(replacing, range);
          } else {
            replacing.replace(replaced);
          }
        };
        return function(shift) {
          var data, i, name;
          name = void 0;
          i = void 0;
          shift.changed = {};
          i = 0;
          while (i < shiftables.length) {
            name = shiftables[i];
            shift.changed[name] = shift.oldData ? shift.oldData[name] !== shift.newData[name] : false;
            i++;
          }
          i = 0;
          while (i < shiftables.length) {
            name = shiftables[i];
            data = shift.oldData ? shift.oldData[name] : null;
            stateActions[name](shift, data, shift.newData[name]);
            i++;
          }
          shift.inflate();
        };
      },
      checkHasNaturalRatio: function(image) {
        var $, natural;
        $ = image.$;
        natural = this.getNatural(image);
        return Math.round($.clientWidth / natural.width * natural.height) === $.clientHeight || Math.round($.clientHeight / natural.height * natural.width) === $.clientWidth;
      },
      getNatural: function(image) {
        var dimensions, img;
        dimensions = void 0;
        if (image.$.naturalWidth) {
          dimensions = {
            width: image.$.naturalWidth,
            height: image.$.naturalHeight
          };
        } else {
          img = new Image;
          img.src = image.getAttribute('src');
          dimensions = {
            width: img.width,
            height: img.height
          };
        }
        return dimensions;
      },
      getLinkAttributesGetter: function() {
        return CKEDITOR.plugins.link.getLinkAttributes;
      },
      getLinkAttributesParser: function() {
        return CKEDITOR.plugins.link.parseLinkAttributes;
      }
    };
  })();


  /**
   * A CSS class applied to the `<figure>` element of a captioned image.
   *
   * Read more in the [documentation](#!/guide/dev_captionedimage) and see the
   * [SDK sample](http://sdk.ckeditor.com/samples/captionedimage.html).
   *
   *		// Changes the class to "captionedImage".
   *		config.image2_captionedClass = 'captionedImage';
   *
   * @cfg {String} [image2_captionedClass='image']
   * @member CKEDITOR.config
   */

  CKEDITOR.config.image2_captionedClass = 'image';


  /**
   * Determines whether dimension inputs should be automatically filled when the image URL changes in the Enhanced Image
   * plugin dialog window.
   *
   * Read more in the [documentation](#!/guide/dev_captionedimage) and see the
   * [SDK sample](http://sdk.ckeditor.com/samples/captionedimage.html).
   *
   *		config.image2_prefillDimensions = false;
   *
   * @since 4.5
   * @cfg {Boolean} [image2_prefillDimensions=true]
   * @member CKEDITOR.config
   */


  /**
   * Disables the image resizer. By default the resizer is enabled.
   *
   * Read more in the [documentation](#!/guide/dev_captionedimage) and see the
   * [SDK sample](http://sdk.ckeditor.com/samples/captionedimage.html).
   *
   *		config.image2_disableResizer = true;
   *
   * @since 4.5
   * @cfg {Boolean} [image2_disableResizer=false]
   * @member CKEDITOR.config
   */


  /**
   * CSS classes applied to aligned images. Useful to take control over the way
   * the images are aligned, i.e. to customize output HTML and integrate external stylesheets.
   *
   * Classes should be defined in an array of three elements, containing left, center, and right
   * alignment classes, respectively. For example:
   *
   *		config.image2_alignClasses = [ 'align-left', 'align-center', 'align-right' ];
   *
   * **Note**: Once this configuration option is set, the plugin will no longer produce inline
   * styles for alignment. It means that e.g. the following HTML will be produced:
   *
   *		<img alt="My image" class="custom-center-class" src="foo.png" />
   *
   * instead of:
   *
   *		<img alt="My image" style="float:left" src="foo.png" />
   *
   * **Note**: Once this configuration option is set, corresponding style definitions
   * must be supplied to the editor:
   *
   * * For [classic editor](#!/guide/dev_framed) it can be done by defining additional
   * styles in the {@link CKEDITOR.config#contentsCss stylesheets loaded by the editor}. The same
   * styles must be provided on the target page where the content will be loaded.
   * * For [inline editor](#!/guide/dev_inline) the styles can be defined directly
   * with `<style> ... <style>` or `<link href="..." rel="stylesheet">`, i.e. within the `<head>`
   * of the page.
   *
   * For example, considering the following configuration:
   *
   *		config.image2_alignClasses = [ 'align-left', 'align-center', 'align-right' ];
   *
   * CSS rules can be defined as follows:
   *
   *		.align-left {
   *			float: left;
   *		}
   *
   *		.align-right {
   *			float: right;
   *		}
   *
   *		.align-center {
   *			text-align: center;
   *		}
   *
   *		.align-center > figure {
   *			display: inline-block;
   *		}
   *
   * Read more in the [documentation](#!/guide/dev_captionedimage) and see the
   * [SDK sample](http://sdk.ckeditor.com/samples/captionedimage.html).
   *
   * @since 4.4
   * @cfg {String[]} [image2_alignClasses=null]
   * @member CKEDITOR.config
   */


  /**
   * Determines whether alternative text is required for the captioned image.
   *
   *		config.image2_altRequired = true;
   *
   * Read more in the [documentation](#!/guide/dev_captionedimage) and see the
   * [SDK sample](http://sdk.ckeditor.com/samples/captionedimage.html).
   *
   * @since 4.6.0
   * @cfg {Boolean} [image2_altRequired=false]
   * @member CKEDITOR.config
   */

}).call(this);
